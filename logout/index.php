<?php

session_start();

$_SESSION = array();
if (isset($_COOKIE[session_name()])) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
}
unset($_SESSION['uid']);
unset($_SESSION['name']);
unset($_SESSION['email']);
unset($_SESSION['logged_in']);
unset($_SESSION['user']);
unset($_SESSION['access_token']);
session_destroy();

header('Location:http://accounts.silveroakhealth.com/login/?target_system=MINDNET');
?>