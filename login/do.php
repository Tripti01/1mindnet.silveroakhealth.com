<?php

include 'do_api_call.php';
include 'soh-config.php';

# Take access token from URL
if (isset($_REQUEST['access_token']) && $_REQUEST['access_token'] != '') {
    $access_token = $_REQUEST['access_token'];
    $request_uri = 'token_verify?access_token=' . $access_token;
    $response = make_api_call($request_uri, "");
    $status = $response["response"];

    if (isset($_REQUEST['callback_url']) && $_REQUEST['callback_url'] != '') {
        $callback_url = $_REQUEST['callback_url'];
    } else {
        $callback_url = "https://ewap.silveroakhealth.com/v4/home/";
    }

    if ($status == '200') {

        if (isset($_REQUEST['uid']) && $_REQUEST['uid'] != '') {
            $uid = $_REQUEST['uid'];
            $emp_id = $uid;
            $corp_id = $_REQUEST['corp_id'];
            $email = $_REQUEST['email'];
            $name = $_REQUEST['name'];

            session_start();
            $_SESSION['user'] = 'logged_in';
            $_SESSION['uid'] = $uid;
            $_SESSION['name'] = $name;
            $_SESSION['email'] = $email;
            $_SESSION['corp_id'] = $corp_id;
            $_SESSION['access_token'] = $access_token;

            #collect the user agent details of the users browser and store it into user_agent variable 
            if (isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT'])) {
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            } else {
                # if its not available in case disabled by user set the user_agent as empty ensure no error messages as this page is called as an image source in the article page
                $user_agent = "Empty";
            }
            # Collect the ip_Address from the user and store it in the ip variable
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            date_default_timezone_set('Asia/Kolkata');
            $timestamp = date('Y-m-d H:i:s');

            $stmt00 = $dbh->prepare("INSERT INTO visitor_log VALUES(?,?,?,?,?,?,?,?,?,?)");
            $stmt00->execute(array('', $corp_id, $uid, 'LOGIN', '', $user_agent, $ip, $timestamp, 'WEB', ''));

            #Before redirecting to home page, check if the user is SilverOak Health employee. The domain should be @silveroakhealth.com
            $prvg_counter = 0;
            $prvg_list = array();

            $stmt04 = $dbh->prepare("SELECT prvg_id from user_prvg where uid=?");
            $stmt04->execute(array($uid));
            while ($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)) {
                $this_prvg = $row04['prvg_id'];
                $prvg_list[$prvg_counter] = $this_prvg;
                $prvg_counter++;
            }
            $_SESSION['$prvg_list'] = $prvg_list;
            session_write_close();

            header("Location:" . $callback_url . "");
            exit();
        } else {
            echo "Some error occurred. Please try again. If the error persists please contact us at help@silveroakhealth.com. Error Code : EWAP_LOGIN_UMSNG ";
            ## UID is missing we cant procced.
        }
    } else if ($status == '440') {
        # Access token is not valid
        echo "Some error occurred. Please try again. If the error persists please contact us at help@silveroakhealth.com. Error Code : EWAP_LOGIN_ATNV";
    } else {
        echo "Some error occurred. Please try again. If the error persists please contact us at help@silveroakhealth.com. Error Code : EWAP_LOGIN_OTHER";
    }
} else {
    ## Access token is missing.  
    echo "Some error occurred. Please try again. If the error persists please contact us at help@silveroakhealth.com. Error Code : EWAP_LOGIN_ATMSNG";
}
?>
