<?php

session_start();
$target_system = "MINDNET";

if (isset($_SESSION['user'])) {

    $uid = $_SESSION['uid'];
    $tid=$uid;

    $corp_id = $_SESSION['corp_id'];
    $corp_logo = $_SESSION['corp_logo'];

    $email = $_SESSION['email'];
    $name = $_SESSION['name'];

    $access_token = $_SESSION['access_token'];
} else {
    $callback_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header("Location:http://accounts.silveroakhealth.com/login/?target_system=" . $target_system . "&callback_url=" . $callback_url);

    exit();
}
?>