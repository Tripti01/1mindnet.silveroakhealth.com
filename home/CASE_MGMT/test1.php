<?php
# Including functions and other files
include '../../../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'get_name.php';
include 'do_api_call.php';

#getting tid from session
$tid = $_SESSION['tid'];

//to check if status are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = "";
}

 $err_msg = '';

#on click of new caller button
if (isset($_REQUEST['create_new_btn'])) {
    if (isset($_REQUEST['caller_name']) && !empty($_REQUEST['caller_name']) && $_REQUEST['caller_name'] !== '' && isset($_REQUEST['caller_company']) && !empty($_REQUEST['caller_company']) && $_REQUEST['caller_company'] !== '' && isset($_REQUEST['caller_mobile']) && !empty($_REQUEST['caller_mobile']) && $_REQUEST['caller_mobile'] !== '' && isset($_REQUEST['caller_yob']) && !empty($_REQUEST['caller_yob']) && $_REQUEST['caller_yob'] !== '' && isset($_REQUEST['caller_gender']) && !empty($_REQUEST['caller_gender']) && $_REQUEST['caller_gender'] !== '' ) {

            $name = urlencode($_REQUEST['caller_name']); //to get encrypted name
            $mobile = $_REQUEST['caller_mobile']; //to get mobile
            $corp_id = $_REQUEST['caller_company']; //to get yob
            $platform = 'WEB';
            $email = $_REQUEST['caller_email'];
            $yob = $_REQUEST['caller_yob'];
            $gender = $_REQUEST['caller_gender'];
            

            $assigned_therapist = $_REQUEST['assigned_therapist'];

            #Check if email already exist using login api(set the dummy password as 123456 to make login API work)
            $password = '123456';
            $request_uri01 = 'login?email=' . $email . '&platform='. $platform . '&password='. $password ;
            $response01 = make_api_call($request_uri01);
            $status01 = $response01["response"];

            if($status01 != 200 and strpos($response01["error"], 'exists') !== false){

                $request_uri02 = 'user_create?email=' . $email . '&platform='. $platform . '&corp_id='. $corp_id ;
                $response02 = make_api_call($request_uri02);
                $status02 = $response02["response"];

                if($status02 == 200){

                    $client_uid = $response02["uid"];
                    $request_uri03 = 'user_profile?name=' . $name . '&mobile='. $mobile . '&uid='. $client_uid .'&yob='. $yob .'&gender='. $gender;
                    $response03 = make_api_call($request_uri03);
                    $status03 = $response03["response"];
                    if($status03 == 200){

                        $stmt10 = $dbh->prepare("UPDATE user_therapist SET tid=? WHERE uid = ? LIMIT 1");
                        $stmt10->execute(array($assigned_therapist, $client_uid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1011].");

                        
                        # Redirecting to the caller_Details
                        header('Location:caller_details.php?client_uid=' . $client_uid);
                    }
                }else{
                    $err_msg = $status02['error'];
                }
            }else{
                $err_msg = "User account already exists!";
            }
         
    } else {
        //  something is wrong the client side validation 
        //  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_NEW_CALLS_JS_MSNG");
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>New EWAP Client</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <style>
            .form-control {
                height: 35px;
            }
            article{
                border: 1px solid #a8aaaf99;
            }
            article .title {
                top: -21px;
                font-size: 16px;
            }
            .btn{
                background-color:#002143;
            }
            label {
                font-weight: 500;
                color: #002143;
            }
        </style>

    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include '../top_navbar.php'; ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="card-box">
                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="form-horizontal">
                                        <div id="err_name" class="msg_disp" style="color:red;font-weight: bold"><center>
                                            <?php if($err_msg !=''){
                                                echo $err_msg;
                                            }?></center>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <article>
                                                    <h5 class="title">
                                                        <b>Name</b>
                                                    </h5>
                                                    <div class="row">
                                                        <div class=" col-sm-12">
                                                            <br/>
                                                            <div class="form-group">
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label">Name</label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" placeholder="Name" name="caller_name"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                                 <article>
                                                    <h5 class="title">
                                                        <b>Corporate Details</b>
                                                    </h5>
                                                    <div class="row" style="margin-top:0.5%;">
                                                        <div class=" col-sm-12">
                                                            <BR/>
                                                            <div class="form-group">	
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label">Corporate Name</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <select id="caller_company" name="caller_company" title="" class="form-control">
                                                                        <option value="0" disabled selected> SELECT </option>
                                                                        <?php              
                                                                            $w = 0; //counter to fetch all notes
                                                                            $stmt20 = $dbh->prepare("SELECT corp_id, corp_name FROM corp_master ORDER BY corp_name ASC");
                                                                            $stmt20->execute(array())or die("Some error occured. Please try again. ErrorCode: [CORP DETAILS-122001].");
                                                                            if ($stmt20->rowCount() != 0) {
                                                                                while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC))
                                                                                {
                                                                                    $corp_id_list[$w] = $row20['corp_id']; //to fetch reason
                                                                                    $corp_name_list[$w] = $row20['corp_name'];
                                                                                    echo '<option value="' . $corp_id_list[$w] . '">' . $corp_name_list[$w] . '</option>';
                                                                                    $w++;
                                                                                }
                                                                            }
                                                                        ?> 
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                                <article>
                                                    <h5 class="title">
                                                        <b>Contact Details</b>
                                                    </h5>
                                                    <div class="row" style="margin-top:0.5%;">
                                                        <div class=" col-sm-12">
                                                            <br/>
                                                            <div class="form-group">
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label">Mobile</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" placeholder="Mobile" name="caller_mobile"/>
                                                                </div>
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label" style="float:right">Email</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" placeholder="Email" name="caller_email"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>


                                                <article>
                                                    <h5 class="title">
                                                        <b>Other Details</b>
                                                    </h5>
                                                    <div class="row" style="margin-top:0.5%;">
                                                        <div class=" col-sm-12">
                                                            <BR/>
                                                            <div class="form-group">														
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label">Year of Birth</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <select id="yob" name="caller_yob" title="" class="form-control">
                                                                        <option value="2010">2010</option>
                                                                        <option value="2009">2009</option>
                                                                        <option value="2008">2008</option>
                                                                        <option value="2007">2007</option>
                                                                        <option value="2006">2006</option>
                                                                        <option value="2005">2005</option>
                                                                        <option value="2004">2004</option>
                                                                        <option value="2003">2003</option>
                                                                        <option value="2002">2002</option>
                                                                        <option value="2001">2001</option>
                                                                        <option value="2000">2000</option>
                                                                        <option value="1999" selected="">1999</option>
                                                                        <option value="1998">1998</option>
                                                                        <option value="1997">1997</option>
                                                                        <option value="1996">1996</option>
                                                                        <option value="1995">1995</option>
                                                                        <option value="1994">1994</option>
                                                                        <option value="1993">1993</option>
                                                                        <option value="1992">1992</option>
                                                                        <option value="1991">1991</option>
                                                                        <option value="1990">1990</option>
                                                                        <option value="1989">1989</option>
                                                                        <option value="1988">1988</option>
                                                                        <option value="1987">1987</option>
                                                                        <option value="1986">1986</option>
                                                                        <option value="1985">1985</option>
                                                                        <option value="1984">1984</option>
                                                                        <option value="1983">1983</option>
                                                                        <option value="1982">1982</option>
                                                                        <option value="1981">1981</option>
                                                                        <option value="1980">1980</option>
                                                                        <option value="1979">1979</option>
                                                                        <option value="1978">1978</option>
                                                                        <option value="1977">1977</option>
                                                                        <option value="1976">1976</option>
                                                                        <option value="1975">1975</option>
                                                                        <option value="1974">1974</option>
                                                                        <option value="1973">1973</option>
                                                                        <option value="1972">1972</option>
                                                                        <option value="1971">1971</option>
                                                                        <option value="1970">1970</option>
                                                                        <option value="1969">1969</option>
                                                                        <option value="1968">1968</option>
                                                                        <option value="1967">1967</option>
                                                                        <option value="1966">1966</option>
                                                                        <option value="1965">1965</option>
                                                                        <option value="1964">1964</option>
                                                                        <option value="1963">1963</option>
                                                                        <option value="1962">1962</option>
                                                                        <option value="1961">1961</option>

                                                                    </select>
                                                                </div>
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label" style="float:right">Gender</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <select id="gender" name="caller_gender" title="" class="form-control">
                                                                        <option value="" disabled="" selected="">Select</option>
                                                                        <option value="M"> Male </option>
                                                                        <option value="F"> Female </option>
                                                                        <option value="O">Other</option>
                                                                        <option value="W">Would rather not say</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                               
                                                <article> 
                                                    <h5 class="title">
                                                        <b>Assign To</b>
                                                    </h5>
                                                    <div class="row" style="margin-top:0.5%;">
                                                        <div class=" col-sm-12">
                                                            <BR/>
                                                            <div class="form-group">
                                                                <div class=" col-sm-2">
                                                                    <label class="control-label">Therapist Name</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <select id="caller_assign_to" name="assigned_therapist" title="" class="form-control">
                                                                        <option value="0" disabled selected> SELECT </option>
                                                                        <?php
                                                                        
                                                                        $w = 0; 
                                                                        $stmt21 = $dbh->prepare("SELECT user_login.uid, name FROM user_login, user_prvg WHERE user_login.uid = user_prvg.uid AND prvg_id=?");
                                                                        $stmt21->execute(array("CASE_MGMT"))or die("Some error occured. Please try again. ErrorCode: [CORP DETAILS-1001].");
                                                                        if ($stmt21->rowCount() != 0) {
                                                                            while ($row21 = $stmt21->fetch(PDO::FETCH_ASSOC)) {
                                                                                $tid_list[$w] = $row21['uid']; 
                                                                                $thrp_name_list[$w] = $row21['name'];
                                                                                echo '<option value="' . $tid_list[$w] . '">' . $thrp_name_list[$w] . '</option>';
                                                                                $w++;
                                                                            }
                                                                        }
                                                                        ?> 
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>

                                                <div class="row">
                                                    <div class="col-sm-12" style="text-align: center;margin-top:2%;">
                                                        <input type="submit" value="Create New Client" name="create_new_btn" class="btn btn-primary"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
        </div>



        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiQbVEMemZgEE5gdyI50TxyIUK0Ba9PBI&libraries=places&autocomplete=true"></script>
        <script src="../assets2/global/scripts/jquery.geocomplete.min.js"></script>
        <script>
            $(function () {
                $("#location").geocomplete({
                    details: ".geo-details",
                    detailsAttribute: "data-geo"
                });
            });
        </script>
        <!---Form validation---->
        <script>
            $("#form-horizontal_1").validate({
                rules: {
                    req: {
                        required: true,
                    },
                },
                messages: {
                    req: {
                        required: "Please Enter Email or Mobile Number",
                    },
                }

            });
            $("#form-horizontal").validate({
                rules: {
                    caller_name: {
                        required: true,
                    },
                    caller_mobile: {
                        required: true,
                        phoneUS: true
                    },
                    caller_company: {
                        required: true,
                    },
                    caller_yob: {
                        required: true,
                    },
                    caller_gender: {
                        required: true,
                    },
                    location: {
                        required: true,
                    },
                    caller_emp_id: {
                        required: true,
                    },
                    assigned_therapist: {
                        required: true,
                    }
                },
                messages: {
                    caller_name: {
                        required: "Please Enter Name",
                    },
                    caller_mobile: {
                        required: "Please Enter Mobile",
                    },
                    caller_company: {
                        required: "Please Select Company",
                    },
                    caller_yob: {
                        required: "Please Select Year of Birth",
                    },
                    caller_gender: {
                        required: "Please Select Gender",
                    },
                    location: {
                        required: "Please Enter Location",
                    },
                    caller_emp_id: {
                        required: "Please Enter Employee ID",
                    },
                    assigned_therapist: {
                        required: "Please Select Therapist",
                    }
                }
            });
        </script>
        <script type="text/javascript">
<?php if ($status == 1) { ?>
                $(document).ready(function () {
                    $("#err_name").addClass("alert");
                    $("#err_name").addClass("alert-danger");
                    $("#err_name").html("Employee id already exsists.");
                    $("#new_call").modal("show");
                });
<?php } ?>
        </script>
    </body>
</html>