<?php 

include 'soh-config.php';

// $i=0;
if(isset($_REQUEST['uid']) && $_REQUEST['uid']!=''){
  $uid = $_REQUEST['uid'];
  $stmt03 = $dbh->prepare("SELECT * from user_login where uid=?");
  $stmt03->execute(array($uid));
  if($stmt03->rowCount() != 0){
    while( $row03 = $stmt03->fetch(PDO::FETCH_ASSOC)){
      $name = $row03['name'];
      $corp_id = $row03['corp_id'];
      $mobile = $row03['mobile'];
      $email = $row03['email'];

      $stmt04 = $dbh->prepare("SELECT corp_name from corp_master where corp_id=?");
      $stmt04->execute(array($corp_id));
      if($stmt04->rowCount() != 0){
        while($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)){
          $corp_name = $row04['corp_name'];
        
      }
      
    }
      
  }
  $stmt00 = $dbh->prepare("SELECT name from user_login where uid=?");
  $stmt00->execute(array($tid));
  if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $therapist_name = $row00['name'];
       
    }
}
  // $i++;
}
}else{
  echo "some error occured";
  die();
}


    // $stmt05 = $dbh->prepare("SELECT taken_by, taken_at from notes_master WHERE uid=? ORDER BY taken_at DESC LIMIT 1");
    // $stmt05->execute(array($uid[$i]));
    // if($stmt05->rowCount() != 0){
    //     $row05 = $stmt05->fetch(PDO::FETCH_ASSOC);
    //     $taken_at[$i] = strval(strtotime($row05['taken_at']));
    //     $last_call[$i] = date("d-m-Y",$taken_at[$i]);
    // }else{
    //     $last_call[$i] = '-';
    // }

    // To get corp_name
  

?>

<aside class="aside aside-fixed" style="width:280px;" >
    <div class="aside-header">
        <a href="../../index.html" class="aside-logo" style="font-size: 20px;">Case Management System</a>
        
    </div>
    <div class="aside-body">
        <ul class="nav nav-aside">
            <div class="profile-sidebar pd-lg-r-25">
            <div class="row">
              <div class="col-sm-3 col-md-2 col-lg">
                <div class="avatar avatar-xxl"><img src="update.jpg" class="rounded-circle" alt=""></div>
              </div><!-- col -->
              <div class="col-sm-8 col-md-7 col-lg mg-t-20 mg-sm-t-0 mg-lg-t-25">
                <h5 class="mg-b-2 tx-spacing--1"><?php echo $name; ?></h5>
                <p class="tx-color-03 mg-b-25"><?php echo $corp_name; ?></p>
                <p class="tx-color-03 mg-b-25">SOH User Id - <?php echo $uid; ?></p>
                <p class="tx-color-03 mg-b-25"><?php echo $email; ?></p>
              </div><!-- col -->
			</div>
			
			<div class="row">
              <div class="col-sm-6 col-md-5 col-lg mg-t-40" style="margin-top: 5px !important;">
                <label class="tx-sans tx-10 tx-semibold tx-uppercase tx-color-01 tx-spacing-1 mg-b-15" style="color:#0148ae;">COORDINATOR :</label>&nbsp;<b>Abc</b>
              </div><!-- col -->
            </div>
			
			<div class="row">
              <div class="col-sm-6 col-md-5 col-lg mg-t-40" style="margin-top: 5px !important;">
                <label class="tx-sans tx-10 tx-semibold tx-uppercase tx-color-01 tx-spacing-1 mg-b-15" style="color:#0148ae;">COUNSELLOR :</label>&nbsp;<b><?php echo $therapist_name; ?> </b>
              </div><!-- col -->
            </div>
			
			<div class="row">
              <div class="col-sm-6 col-md-5 col-lg mg-t-40" style="margin-top: 5px !important;">
                <label class="tx-sans tx-10 tx-semibold tx-uppercase tx-color-01 tx-spacing-1 mg-b-15" style="color:#0148ae;">ACTIONS</label>
                <ul class="list-unstyled profile-info-list">
                  <li><i data-feather="file"></i><a href="view_corp_copy.php?uid=<?php echo $uid; ?>">Counselling Notes</a></li>


                  <li><i data-feather="home"></i> <a href="">Peer Support Group</a></li>
                  <li><i data-feather="coffee"></i> <a href="">Diet</a></li>
                  <li><i data-feather="package"></i> <a href="">SOH Activity</a></li>
                </ul>
              </div><!-- col -->
              
            </div><!-- row -->
			
			<div class="row">
              <div class="col-sm-6 col-md-5 col-lg mg-t-40" style="margin-top: 20px !important;">
                <label class="tx-sans tx-10 tx-semibold tx-uppercase tx-color-01 tx-spacing-1 mg-b-15" style="color:#0148ae;">Administration</label>
                <ul class="list-unstyled profile-info-list">
                  <li><i data-feather="menu"></i><a href="">Edit Profile</a></li>
                  <li><i data-feather="menu"></i> <a href="">Reset Password</a></li>
                </ul>
              </div><!-- col -->
            </div>
			
			<div class="row">
              <div class="col-sm-8 col-md-7 col-lg mg-t-20 mg-sm-t-0 mg-lg-t-25" style="margin-top: 20px !important;">
                <div class="d-flex">
                  <div class="profile-skillset flex-fill">
                    <h5><a href="" class="link-01">21 Jul,21</a></h5>
                    <label>First Seen</label>
                  </div>
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <div class="profile-skillset flex-fill">
                    <h5><a href="" class="link-01">7 Aug, 21</a></h5>
                    <label>Last Seen</label>
                  </div>
                </div>
			  </div>
            </div>

          </div>
        </ul>
    </div>
</aside>