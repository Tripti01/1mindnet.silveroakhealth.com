<?php

include 'soh-config.php';

$i=0;
if(isset($_REQUEST['uid']) && $_REQUEST['uid']!=''){
  $uid = $_REQUEST['uid'];
  $stmt03 = $dbh->prepare("SELECT * from notes_master WHERE uid=?");
  $stmt03->execute(array($uid));
  if($stmt03->rowCount() != 0){
    while( $row03 = $stmt03->fetch(PDO::FETCH_ASSOC)){
      $notes_id[$i] = $row03['notes_id'];
      $duration[$i]  = $row03['duration'];
      $reason[$i]  = $row03['reason'];
      $source[$i]  = $row03['source'];
      $taken_at[$i] = strval(strtotime($row03['taken_at']));
      $model_time[$i] = $row03['taken_at'];
      $date[$i] = date("d M, Y",$taken_at[$i]);
      $time[$i] = date("h:i A",$taken_at[$i]);

      $stmt04 = $dbh->prepare("SELECT * from notes_text where notes_id=?");
      $stmt04->execute(array($notes_id[$i]));
      if($stmt04->rowCount() != 0){
        while($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)){
          $old_notes[$i] = $row04['old_notes'];
        }
      }

      $stmt05 = $dbh->prepare("SELECT * from list_calltype where calltype_code=?");
      $stmt05->execute(array($source[$i]));
      if($stmt05->rowCount() != 0){
        while($row05 = $stmt05->fetch(PDO::FETCH_ASSOC)){
          $calltype_text[$i] = $row05['calltype_text'];
        }
      }

      $stmt06 = $dbh->prepare("SELECT * from list_subreason where subreason_code=?");
      $stmt06->execute(array($reason[$i]));
      if($stmt06->rowCount() != 0){
        while($row06 = $stmt06->fetch(PDO::FETCH_ASSOC)){
          $reason_code[$i] = $row06['reason_code'];
          $subreason_text[$i] = $row06['subreason_text'];

          $stmt07 = $dbh->prepare("SELECT * from list_reason where reason_code=?");
          $stmt07->execute(array($reason_code[$i]));
          if($stmt07->rowCount() != 0){
            while($row07 = $stmt07->fetch(PDO::FETCH_ASSOC)){
              $reason_text[$i] = $row07['reason_text'];
            }
          }
        }
      }

      // echo $notes_id[$i]."-".$taken_at[$i]."-".$duration[$i]."-".$reason[$i]."-".$source[$i]."-".$calltype_text[$i]."--".$reason_code[$i]."--".$reason_text[$i]."--".$subreason_text[$i]."<br>";
    $i++;
    }
}else{
  echo "some error occured";
  // die();
}
}
// exit();
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

        <title>Corporate Information System</title>

        <!-- vendor css -->
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">


        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/typicons.font/typicons.css" rel="stylesheet">
        <link href="../../assets/lib/prismjs/themes/prism-vs.css" rel="stylesheet">
        <link href="../../assets/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../../assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
        <link href="../../assets/lib/select2/css/select2.min.css" rel="stylesheet">
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
		<link rel="stylesheet" href="../../assets/css/dashforge.demo.css">
    </head>
    <body>

        <?php include 'header_profile.php'; ?>
        <div class="content-body">
            <div class="container pd-x-0">
                <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                    <div class="content content-components">
                        <div class="container">
                        <div style="text-align:right"><button type="button" class="btn btn-outline-primary" href="#modal5" data-toggle="modal" style="padding: 5px; margin-bottom:10px;">Add Notes</button></div>

                        
                            <div data-label="" class="df-example demo-table" style="width:800px;">
                                <div id="example4_filter" class="dataTables_filter" >
                                    <table id="example4" class="table dataTable no-footer" role="grid" aria-describedby="example4_info" style="">
                                        <thead>
                                        <tr role="row">
                                            <th class="wd-20p sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" aria-sort="ascending" style="width: 132px;">Session No.</th>
                                            <th class="wd-25p sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 177px;">Reason</th>
                                            <th class="wd-20p sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 130px;">Date</th>
                                            <th class="wd-15p sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Extn: activate to sort column ascending" style="width: 92px;">Time</th>
                                            <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for ($g = 0; $g <= $i; $g++) {
                                          if (isset($reason[$g]) && $reason[$g] !== '' && isset($date[$g]) && $date[$g] !== '' && isset($time[$g]) && $time[$g] !== '') {
                                        ?>
                                            <tr role="row" class="odd">
                                                    <td class="sorting_1">SESSION <?php echo $g+1; ?></td>
                                                    <td><?php echo $reason_text[$g]; ?> </td>
                                                    <td><?php echo $date[$g]; ?> </td>
                                                    <td><?php echo $time[$g]; ?></td>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-outline-secondary" href=""  data-target="#viewmodal_<?php echo $g; ?>" data-toggle="modal" style="padding: 5px;">View</button>
                                                        <button type="button" class="btn btn-outline-primary" href="#modal3" data-toggle="modal" style="padding: 5px;">Edit</button>
                                                    </td>
                                                </tr>
                                        <?php 
                                        
                                            } 

                                          }
                                        
                                        
                                        ?>
                                        </tbody>
                                    </table>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <!-- model4  -->
    <?php
    for ($l = 0; $l <= $i; $l++) {
        if (isset($reason[$l]) && $reason[$l] !== '' && isset($date[$l]) && $date[$l] !== '' && isset($time[$l]) && $time[$l] !== '') {
    ?>
    <div class="modal fade" id="viewmodal_<?php echo $l; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content tx-14">
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel4">Notes for Call Number 1</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
			<div class="nk-content-inner">
                            <div class="nk-content-body">
								<div class="nk-block">
                                <div class="card">
                                            <div id="faqs" class="accordion">
                                                <div class="accordion-item">
                                                    <a href="#" class="accordion-head" data-toggle="collapse" data-target="#faq-q1">
                                                        <h6 class="title">Basic Details</h6>
                                                        <span class="accordion-icon"></span>
                                                    </a>
                                                    <div class="accordion-body collapse show" id="faq-q1" data-parent="#faqs">
                                                        <div class="accordion-inner">
															<table style="border-collapse: collapse; width: 100%;">
                                  <tr>
                                    <th>Call Type</th>
                                    <th>Reason</th>
                                    <th>Time</th>
                                    <th>Duration</th>
                                  </tr>
                                    <tr>
                                      <td><?php echo $calltype_text[$l] ; ?></td>
                                      <td><?php echo $subreason_text[$l]; ?></td>
                                      <td><?php echo $model_time[$l]; ?></td>
                                      <td><?php echo $duration[$l]; ?></td>
                                    </tr>
															</table>                                                        
														</div>
                          </div>
                      </div><!-- .accordion-item -->
                    <br>
                      <div class="accordion-item">
                          <a href="#" class="accordion-head collapsed" data-toggle="collapse" data-target="#faq-q3">
                              <h6 class="title">Notes</h6>
                              <span class="accordion-icon"></span>
                          </a>
                          <div class="accordion-body collapse" id="faq-q3" data-parent="#faqs">
                              <div class="accordion-inner">
                                  <p><?php echo $old_notes[$l]; ?></p>
                              </div>
                          </div>
                      </div><!-- .accordion-item -->
                  </div><!-- .accordion -->
              </div>
          </div>
      </div>
      </div>          
			</div>
        </div>
      </div>
    </div>
    <?php 
  } 
  
}
  
  ?>
	
    <!-- model5 -->
    
    <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document" >
          
        <div class="modal-content tx-14" >
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel4">Add Notes for Call Number 1</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <table style="border-collapse: collapse; width: 100%;">
            <tr>
                <th>Date</th>
                <th>Time</th>
            </tr>
            <tr>
                <td><input type="date" id="datepicker1" class="form-control" ></td>
                <td><input id="timepicker2" type="text" class="form-control" name="time" /></td>
                </tr>
				
				<tr>
				<td>&nbsp;</td>
				</tr>
				
			<tr>
                <th>Duration</th>
                <th>Reason</th>
                <th>Sub Reason</th>
                <th>Type</th>
                <th>Employee type</th>
            </tr>
			<tr>
                <td>
                    <select class="form-control" name="duration">
					<option value="0" selected disabled>Select Duration</option> 
                        <option value="5">5 minutes</option>
                        <option value="10">10 minutes</option>
                        <option value="20">20 minutes</option>
                        <option value="30">30 minutes</option>
                        <option value="40">40 minutes</option>
                        <option value="50">50 minutes</option>
                        <option value="60">60 minutes</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="reason">
					<option value="0" selected disabled>Select Reason</option> 
                        <option value="Healthy issue" name="healthy_issues">Healthy issue</option>
                        <option value="Family issue" name="family_isses">Family issue</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="healthy_issues">
					<option value="0" selected disabled>Select Sub Reason</option> 
                        <option value="Covid 19">Covid 19</option>
                        <option value="Cancer">Cancer</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="type">
					<option value="0" selected disabled>Select Type</option> 
                        <option value="F2F onsite">F2F onsite</option>
                        <option value=">F2F SOH">F2F SOH</option>
                    </select>
                </td>
                <td>
                  <input type="radio" name="Employee_type" value="employee">
                  <label for="html">Employee</label><br>
                  <input type="radio" name="Employee_type" value="dependent">
                  <label for="html">Dependent</label>
                </td>
            </tr>
			<tr>
				<td>&nbsp;</td>
				</tr>
        </table>
        <label for="notes"><b>Notes</b></label>
        <!-- <textarea id="notes" name="notes" rows="4" cols="50" style="width:98%; height:150px;"> -->
        <textarea class="form-control" id="notes" rows="1" id="notes" name="notes" wrap="virtual" style="min-height:6px;height:300px;">
        </textarea>
        
          <div class="modal-footer">
            <input type="hidden" name="corp_id" value=""/>
            <input type="hidden" name="sr_no" value=""/>
            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_add_domain">Save</button>
        </div>
        </div>
        </div>
      </div>
    </div>

    <!-- model3 -->
	<div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document" >
          
        <div class="modal-content tx-14" >
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel4">Edit Notes for Call Number 1</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <table style="border-collapse: collapse; width: 100%;">
            <tr>
                <th>Date</th>
                <th>Time</th>
            </tr>
            <tr>
                <td><input type="date" id="datepicker1" class="form-control" ></td>
                <td><input id="timepicker2" type="text" class="form-control" name="time" /></td>
                </tr>
				
				<tr>
				<td>&nbsp;</td>
				</tr>
				
			<tr>
                <th>Duration</th>
                <th>Reason</th>
                <th>Sub Reason</th>
                <th>Type</th>
                <th>Employee type</th>
            </tr>
			<tr>
                <td>
                    <select class="form-control" name="duration">
					<option value="0" selected disabled>Select Duration</option> 
                        <option value="5">5 minutes</option>
                        <option value="10">10 minutes</option>
                        <option value="20">20 minutes</option>
                        <option value="30">30 minutes</option>
                        <option value="40">40 minutes</option>
                        <option value="50">50 minutes</option>
                        <option value="60">60 minutes</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="reason">
					<option value="0" selected disabled>Select Reason</option> 
                        <option value="Healthy issue" name="healthy_issues">Healthy issue</option>
                        <option value="Family issue" name="family_isses">Family issue</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="healthy_issues">
					<option value="0" selected disabled>Select Sub Reason</option> 
                        <option value="Covid 19">Covid 19</option>
                        <option value="Cancer">Cancer</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="type">
					<option value="0" selected disabled>Select Type</option> 
                        <option value="F2F onsite">F2F onsite</option>
                        <option value=">F2F SOH">F2F SOH</option>
                    </select>
                </td>
                <td>
                  <input type="radio" name="Employee_type" value="employee">
                  <label for="html">Employee</label><br>
                  <input type="radio" name="Employee_type" value="dependent">
                  <label for="html">Dependent</label>
                </td>
            </tr>
			<tr>
				<td>&nbsp;</td>
				</tr>
        </table>
        <label for="notes"><b>Notes</b></label>
        <!-- <textarea id="notes" name="notes" rows="4" cols="50" style="width:98%; height:150px;"> -->
        <textarea class="form-control" id="notes" rows="1" id="notes" name="notes" wrap="virtual" style="min-height:6px;height:300px;">
        </textarea>
        
          <div class="modal-footer">
            <input type="hidden" name="corp_id" value=""/>
            <input type="hidden" name="sr_no" value=""/>
            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_add_domain">Save</button>
        </div>
        </div>
        </div>
      </div>
    </div>
        
        

    <script type="text/javascript">
        function formValidate()
        {
            var val = document.frmDomin;
            if (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value) || /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value)) {
            } else
            {
                $("#err_name").html("Invalid Domain");
                val.domain.focus();
                return false;
            }
        }
    </script>


    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
    <script src="../../assets/js/dashforge.js"></script>
    <script src="../../assets/js/dashforge.aside.js"></script>

    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/prismjs/prism.js"></script>
    <script src="../../assets/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../../assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../../assets/lib/select2/js/select2.min.js"></script>

    <script src="../../assets/js/dashforge.js"></script>

    
    
    
    
    <script>
      $(function(){
        'use strict'

        $('#example4').DataTable({
        //   'ajax': '../../assets/data/datatable-objects.txt',
          "columns": [
            { "data": "name" },
            { "data": "position" },
            { "data": "office" },
            { "data": "extn" },
            { "data": "action" },
          ],
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
</body>
</html>
