<?php 
include 'if_loggedin.php';
include 'soh-config.php'; 

// To get client detais
$i=0;

$stmt02 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=?");
$stmt02->execute(array($tid));
if ($stmt02->rowCount() != 0) {
    while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
        $uid[$i] = $row02['uid'];

        $stmt03 = $dbh->prepare("SELECT user_login.uid, name, email, mobile, corp_id, yob, gender from user_login, user_profile WHERE user_login.uid=user_profile.uid AND user_login.uid=?");
        $stmt03->execute(array($uid[$i]));
        if($stmt03->rowCount() != 0){
            $row03 = $stmt03->fetch(PDO::FETCH_ASSOC);
            $name[$i] = $row03['name'];
            $corp_id[$i] = $row03['corp_id'];
            $mobile[$i] = $row03['mobile'];
            $email[$i] = $row03['email'];
            $yob[$i] = $row03['yob'];
            $gender[$i] = $row03['gender'];

            $year = date("Y");
            $age[$i] = $year - $yob[$i];

            $last_call[$i] = '';
            // $stmt05 = $dbh->prepare("SELECT taken_by, taken_at from notes_master WHERE uid=? ORDER BY taken_at DESC LIMIT 1");
            // $stmt05->execute(array($uid[$i]));
            // if($stmt05->rowCount() != 0){
            //     $row05 = $stmt05->fetch(PDO::FETCH_ASSOC);
            //     $taken_at[$i] = strval(strtotime($row05['taken_at']));
            //     $last_call[$i] = date("d-m-Y",$taken_at[$i]);
            // }else{
            //     $last_call[$i] = '-';
            // }

            // To get corp_name
            $stmt04 = $dbh->prepare("SELECT corp_name from corp_master where corp_id=?");
            $stmt04->execute(array($corp_id[$i]));
            if($stmt04->rowCount() != 0){
                $row04 = $stmt04->fetch(PDO::FETCH_ASSOC);
                $corp_name[$i] = $row04['corp_name'];

            // echo $name[$i]."-".$corp_name[$i]."-".$mobile[$i]."-".$email[$i]."-".$yob[$i]."-".$gender[$i]."<br>";
            // echo count($name);
            $i++;
    }
    
        
    }
}

}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">
        <title>Case Management System</title>
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    </head>
    <body>

<?php include '../header.php'; ?>
        <div class="content pd-0">
            <div class="content-body">
                <div class="container pd-x-0">
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                        <div>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                                    <li class="breadcrumb-item active" style="font-size:15px;" aria-current="page">ALL CLIENTS</li>
                                </ol>
                            </nav>
                           
                        </div>
                      
                    </div>
                    
                    <div class="row row-xs mg-t-10">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class=" card-body">
                                <table class="table table-striped mg-b-0">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Sr&nbsp;no.</th>
                                            <th>Name</th> 
                                            <th>Company</th>                                       
                                            <th>Mobile</th>
                                            <th>Email</th> 
                                            <th>Age/Gender</th>                                       
                                            <th>Last&nbsp;Call&nbsp;Time</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0; $i<count($uid); $i++) { ?>
                                                <tr>
                                                    <td><?php echo $i+1; ?> </td>
                                                    <td><?php echo $name[$i]; ?> </td>
                                                    <td><?php echo $corp_name[$i]; ?> </td>
                                                    <td><?php echo $mobile[$i]; ?> </td>
                                                    <td><?php echo $email[$i]; ?> </td>
                                                    <td><?php echo $age[$i]."/".$gender[$i]; ?> </td>
                                                    <!-- <td><?php #echo $last_call[$i]; ?> </td> -->

                                                    <td><a href="dashboard.php?uid=<?php echo $uid[$i]; ?> " class="btn btn-xs btn-secondary">View&nbsp;&&nbsp;Edit</a></td>

                                                </tr>   

                                        <?php } ?>
                                                								
                                    </tbody>   
                                </table>

                            </div>
                        </div>
                    </div>
                    </div>
   
                </div><!-- container -->
            </div><!-- content -->
        </div>
        <script src="../../assets/lib/jquery/jquery.min.js"></script>
        <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/lib/feather-icons/feather.min.js"></script>
        <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
        <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
        <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
        <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
        <script src="../../assets/js/dashforge.js"></script>
        <script src="../../assets/js/dashforge.aside.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    </body>
</html>
