<?php

?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="description" content="Silver Oak Health - HR Dashboard">
        <meta name="author" content="Silver Oak Health">

        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

        <title>Client Finder</title>

        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
        <link href="../../assets/css/dashforge.demo.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">

        <script src="lib/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">
        <style>
            .daterangepicker .ranges li.active {
                background-color: #fff; 
                color: #000; 
            }
        </style>
        <style>
            table.dataTable thead .sorting::after {
                display:none;
            }
            table.dataTable thead .sorting::before {
                display:none;
            }
            table.dataTable thead .sorting_asc::after {
                display:none;
            }
            table.dataTable thead .sorting_desc::after {
                display:none;
            }
        </style>
    </head>
    <body class="page-profile">

        <?php include '../header.php'; ?>

        <div class="content content-fixed">
            <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <div>
                        <h5 class=" tx-spacing--1"> Counselling Raw Data </h5>
                        <nav aria-label="breadcrumb mg-t-5 ">
                            <ol class="breadcrumb breadcrumb-style1 mg-b-10">

                                <li class="breadcrumb-item active " aria-current="page"> <span class="link-03">Showing  from</span> <?php echo $ui_from_date; ?> <span class="link-03">to </span><?php echo $ui_to_date; ?>   </li>
                            </ol>
                        </nav>

                    </div>
                    <div>
                        <div class="input-group mg-b-10">
                            <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="GET">
                                <div class="row ">
                                    <div class="col-lg-10" style="padding-right:5px">
                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span id="get_date"></span> <i class="fa fa-caret-down"></i>
                                        </div>                                      
                                    </div>
                                    <div class="col-lg-1" style="padding-left:0px">
                                        <input type="hidden" name="daterange" id="daterange" />
                                        <button class="btn btn-sm pd-x-8 btn-primary btn-uppercase" onclick="get_daterange()"> Show </button>
                                    </div>
                                </div>
                            </form>                        
                        </div>
                    </div>
                </div> 
                <div class="row row-xs">
                    <div class="col-lg-12">
                        <div>
                            <table id="tb1" class="display nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Date & Time </th>
                                        <th>User ID</th> 
                                        <th>Call Type</th>
                                        <th>Reason</th>
                                        <th>Location</th>
                                        
                                        <th>Vertical</th>
                                       
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                          <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>  <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>  <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>  <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>  <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>  <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>  <tr>
                                        <td>Date & Time </td>
                                        <td>User ID</td> 
                                        <td>Call Type</td>
                                        <td>Reason</td>
                                        <td>Location</td>
                                        
                                        <td>Vertical</td>
                                       
                                        <td>Type</td>
                                    </tr>          
                                </tbody>
                            </table>
                        </div><!-- card -->
                    </div><!-- col -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- content -->



        <footer class="footer">
            <div>

            </div>
            <div>
                <nav class="nav">

                </nav>
            </div>
        </footer>

        <script src="../../assets/lib/jqueryui/jquery-ui.min.js"></script>
        <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/lib/feather-icons/feather.min.js"></script>
        <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
        <script src="../../assets/lib/flot.curvedlines/curvedLines.js"></script>
        <script src="../../assets/lib/peity/jquery.peity.min.js"></script>
        <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
        <script src="../../assets/lib/bootstrap/js/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
        <script src="../../assets/js/dashforge.js"></script>
        <script src="../../assets/js/nav-active.js"></script>
        <script type="text/javascript">
                                    $(function () {

                                        var start = moment().subtract(29, 'days');
                                        var end = moment();

                                        show_date = '<?php echo date("F d, Y", strtotime($from_date)); ?>';
                                        end_date = '<?php echo date("F d, Y", strtotime($to_date)); ?>';

                                        function cb(start, end) {
                                            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                        }

                                        $('#reportrange').daterangepicker({
                                            startDate: start,
                                            endDate: end,
                                            maxDate: end,
                                            ranges: {
                                                'Today': [moment(), moment()],
                                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                            }
                                        }, cb);

                                        cb(moment(show_date), moment(end_date));

                                    });
        </script>
        <script>
            function get_daterange() {
                var get_date_range = $("#get_date").text();
                $("#daterange").val(get_date_range);

            }
        </script>
        <script>

            //date.setDate(date.getDate());
            $('#dateFrom').datepicker({
                format: 'dd-mm-yyyy'
            });
            $('#dateTo').datepicker({

                format: 'dd-mm-yyyy'
            });
        </script>
        <script>
            //var corp_name = "<?php echo $corp_name; ?>";
            $(document).ready(function () {
                $('#tb1').DataTable({
                    dom: 'Bfrtip',
                    "bSort": false,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: '<?php echo $corp_name; ?> Data export'
                        }
                    ]
                });
            });
        </script>     
    </body>
</html>
