<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            .internal-card-img{
                width: 43%;
                height: 185px;
                border-radius: 50%;
                margin-left: 29%;
                margin-top: 2%;
            }
            .internal-table{
                padding-bottom: 40px;
            }
            .internal-card{
                border: None !important;
            }
            .internal-hr{
                border-bottom: 1px solid #dbd9d9;
            }
            .internal-icon{
                font-size: 50px;
                color: #0168fa;
                /* padding-right: 10px; */
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

        <title>Corporate Information System</title>

        <!-- vendor css -->
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
    </head>
    <body>

        <?php include '../header.php'; ?>

        <div class="content pd-0">
            <div class="content-body">
                <div class="container pd-x-0">
                    
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                          
                    
                        <div>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-style1 mg-b-10">

                                    <li class="breadcrumb-item "><a href="index.php"><i data-feather="arrow-left" width="10%"></i> All Clients</a></li>

                                </ol>
                            </nav>
                        </div>
                    </div>

                </div><!-- df-example -->
				
				
				
				

            </div><!-- container -->
        </div><!-- content -->
        <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="domain">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Notes</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Date</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="domain" placeholder="Date" name="Date" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Time</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Time" name="Time" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Duration</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Duration" name="Duration" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Reason</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Reason" name="Reason" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Sub Reason</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Sub Reason" name="add_domain" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Type" name="add_domain" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Employee Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Employee Type" name="add_domain" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="corp_id" value=""/>
							<input type="hidden" name="sr_no" value=""/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_add_domain">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
                                                            
        <div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="domain">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Domain</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Domain Name(s)</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Domain Name(s)" name="new_domain" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="corp_id" value=""/>
                            <input type="hidden" name="sr_no" value=""/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_edit_domain">Modify</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="modal03" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="domain">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Add Notes</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Date</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="domain" placeholder="Date" name="Date" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Time</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Time" name="Time" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Duration</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Duration" name="Duration" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Reason</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Reason" name="Reason" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Sub Reason</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Sub Reason" name="add_domain" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Type" name="add_domain" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Employee Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Employee Type" name="add_domain" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="corp_id" value=""/>
							<input type="hidden" name="sr_no" value=""/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_add_domain">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Details</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Registered Office Address</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Registered Office Address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">About</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="About">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">Industry type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Industry type">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary tx-13">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Contract Details</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contract Start Date</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="inputEmail3" placeholder="Contract Start Date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">Contract Duration</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Contract Duration">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">Contract End Date</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="inputPassword3" placeholder="Contract End Date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">Launch Date</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="inputPassword3" placeholder="Launch Date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">Notice Period</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Notice Period">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary tx-13">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="post" id="service">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Service Details</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Service Plan</label>
                                <div class="col-sm-7">
                                    <select name="new_service_plan" id="service" type="text" class="form-control" list="service-plan" >
                                        <option value="-1" disabled="">Select Plan</option>
                                        <option value="EWAPLITE" >EWAP LITE</option>
                                        <option value="EWAPCLASSIC" >EWAP CLASSIC</option>
                                        <option value="EWAPPLUS"  >EWAP PLUS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label">SOH Account Manager</label>
                                <div class="col-sm-7">
                                    <select type="text" name="new_account_manager" class="form-control" placeholder="SOH Account Manager">
                                        <option value="-1" disabled="">Select Account Manager</option>
                                        <option value="USER100" >USER100 </option>
                                        <option value="USER200" >USER200 </option>
                                        <option value="USER300" >USER300 </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        
                            <input type="hidden" name="corp_id" value=""/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_edit_contract"> Modify </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Locations</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Location(s)</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Location(s)">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Any Manfacturing/plant locations</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Any Manfacturing/plant locations">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary tx-13">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="" method="post"  id="contact" name="contact">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Add Contact</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact" placeholder="Contact Name" name="contact" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Phone</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact" placeholder="Contact Phone" name="contact" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Email</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact" placeholder="Contact Email" name="contact" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="corp_id" value=""/>
                            <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary tx-13" name="add_contact">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Contact Details</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Primary Contact Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Primary Contact Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Phone</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Contact Phone">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Email</label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Contact Email">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary tx-13">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal101" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Details</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Corporate Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Corporate Name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Plan</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Plan" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">Domain</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Domain" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-5 col-form-label">SOH Account Manager</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="inputPassword3" placeholder="SOH Account Manager" required>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary tx-13">Activate Account</button>
                    </div>
                </div>




            </div><!-- container -->
        </div>
    </div>

    <script type="text/javascript">
        function formValidate()
        {
            var val = document.frmDomin;
            if (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value) || /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value)) {
            } else
            {
                $("#err_name").html("Invalid Domain");
                val.domain.focus();
                return false;
            }
        }
    </script>


    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
    <script src="../../assets/js/dashforge.js"></script>
    <script src="../../assets/js/dashforge.aside.js"></script>
</body>
</html>
