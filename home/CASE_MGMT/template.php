<?php ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">
        <title>Corporate Information System</title>
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
    </head>
    <body>

<?php include '../header.php'; ?>
        <div class="content pd-0">
            <div class="content-body">
                <div class="container pd-x-0">
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                        <div>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Page Name</li>
                                </ol>
                            </nav>
                            <h4 class="mg-b-0 tx-spacing--1"> Page Heading</h4>
                        </div>
                        <div class="d-none d-md-block">
                            <button class="btn btn-sm pd-x-15 btn-white btn-uppercase"><i data-feather="mail" class="wd-10 mg-r-5"></i> 1 </button>
                            <button class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="file" class="wd-10 mg-r-5"></i> 2 </button>
                        </div>
                    </div>

                    <div class="row row-xs">
                        
                    </div>

                </div><!-- container -->
            </div><!-- content -->
        </div>
        <script src="../../assets/lib/jquery/jquery.min.js"></script>
        <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/lib/feather-icons/feather.min.js"></script>
        <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
        <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
        <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
        <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
        <script src="../../assets/js/dashforge.js"></script>
        <script src="../../assets/js/dashforge.aside.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    </body>
</html>
