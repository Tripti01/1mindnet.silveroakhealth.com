<?php
include 'soh-config.php';

$corp_id = $_REQUEST['corp_id'];
$sr_no = $_REQUEST['sr_no'];

$success_redirect_link = "view_corp.php?corp_id=" . $corp_id . "&result=SUCCESS";
$failed_redirect_link = "view_corp.php?corp_id=" . $corp_id . "&result=FAILURE";

##################### For updating corp name and alt names ####################
if (isset($_REQUEST['btn_edit_corp_name']) && !empty($_REQUEST['btn_edit_corp_name'])) {
   $new_corp_name= $_REQUEST['new_corp_name'];
   
  $stmt100 = $dbh->prepare("UPDATE corp_master SET corp_name=? WHERE corp_id=? LIMIT 1");
    $stmt100->execute(array($new_corp_name, $corp_id));
    if ($stmt100->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}

##################### For updating contract details ##########################
if (isset($_REQUEST['btn_edit_contract']) && !empty($_REQUEST['btn_edit_contract'])) {
   $new_service_plan= $_REQUEST['new_service_plan'];
   $new_account_manager= $_REQUEST['new_account_manager'];
    
  $stmt200 = $dbh->prepare("UPDATE corp_contract SET account_manager=? WHERE corp_id=? LIMIT 1");
  $stmt200->execute(array($new_account_manager, $corp_id));
  
  $stmt220 = $dbh->prepare("UPDATE corp_service_plan SET service_plan=? WHERE corp_id=? LIMIT 1");
  $stmt220->execute(array($new_service_plan, $corp_id));
   
 
  if ($stmt200->rowCount() or $stmt220->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}


##################### For updating domains ####################
if (isset($_REQUEST['btn_edit_domain']) && !empty($_REQUEST['btn_edit_domain'])) {
   $new_domain = $_REQUEST['new_domain'];
   
    $stmt300 = $dbh->prepare("UPDATE corp_domain SET domain=? WHERE sr_no=? LIMIT 1");
    $stmt300->execute(array($new_domain, $sr_no));
    if ($stmt300->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}


################### for adding domains ####################
if (isset($_REQUEST['btn_add_domain']) && !empty($_REQUEST['btn_add_domain'])) {
   $add_domain= $_REQUEST['add_domain'];
   
    $stmt400 = $dbh->prepare("INSERT into corp_domain(corp_id, domain ) values(?,?)");
    $stmt400->execute(array($corp_id, $add_domain));
    if ($stmt400->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}


?>

