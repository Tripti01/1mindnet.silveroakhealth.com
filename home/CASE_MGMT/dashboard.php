<!DOCTYPE html>
<html lang="en">
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

        <title>Corporate Information System</title>

        <!-- vendor css -->
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">


        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/typicons.font/typicons.css" rel="stylesheet">
        <link href="../../assets/lib/prismjs/themes/prism-vs.css" rel="stylesheet">
        <link href="../../assets/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../../assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
        <link href="../../assets/lib/select2/css/select2.min.css" rel="stylesheet">
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
		<link rel="stylesheet" href="../../assets/css/dashforge.demo.css">
    </head>
    <body>
	
<?php include 'header_profile.php'; ?>



<script type="text/javascript">
        function formValidate()
        {
            var val = document.frmDomin;
            if (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value) || /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value)) {
            } else
            {
                $("#err_name").html("Invalid Domain");
                val.domain.focus();
                return false;
            }
        }
    </script>


    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
    <script src="../../assets/js/dashforge.js"></script>
    <script src="../../assets/js/dashforge.aside.js"></script>

    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/prismjs/prism.js"></script>
    <script src="../../assets/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../../assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../../assets/lib/select2/js/select2.min.js"></script>

    <script src="../../assets/js/dashforge.js"></script>

    
    
    
    
    <script>
      $(function(){
        'use strict'

        $('#example4').DataTable({
        //   'ajax': '../../assets/data/datatable-objects.txt',
          "columns": [
            { "data": "name" },
            { "data": "position" },
            { "data": "office" },
            { "data": "extn" },
            { "data": "action" },
          ],
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
</body>
</html>
