<?php
include 'asmt-config.php';

$dbh = new PDO($dsn_asmt, $berlin_public, $berlin_pass);
$dbh->query("use berlin");

if(isset($_REQUEST["corp_id"]) && $_REQUEST["corp_id"] != "") {	
	$corp_id = $_REQUEST["corp_id"];
	
	$i = 0;

	$stmt01 = $dbh->prepare("SELECT uid FROM user_type WHERE corp_id=?");
	$stmt01->execute(array($corp_id));

	if ($stmt01->rowCount() != 0) {

			while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
				$uid[$i] = $row01['uid'];

					$stmt200 = $dbh->prepare("SELECT * FROM asmt_scale_score WHERE uid=? AND scale=?");
					$stmt200->execute(array($uid[$i], "A"));
					$row200 = $stmt200->fetch(PDO::FETCH_ASSOC);
					$A_score[$i] = $row200['score'];
					$A_score_level[$i] = $row200['level'];
					$A_score_level_fullname[$i] = score_level_fullname($A_score_level[$i]);
					
					$stmt300 = $dbh->prepare("SELECT * FROM asmt_scale_score WHERE uid=? AND scale=?");
					$stmt300->execute(array($uid[$i], "D"));
					$row300 = $stmt300->fetch(PDO::FETCH_ASSOC);
					$D_score[$i] = $row300['score'];
					$D_score_level[$i] = $row300['level'];
					$D_score_level_fullname[$i] = score_level_fullname($D_score_level[$i]);

					$stmt400 = $dbh->prepare("SELECT * FROM asmt_scale_score WHERE uid=? AND scale=?");
					$stmt400->execute(array($uid[$i], "S"));
					$row400 = $stmt400->fetch(PDO::FETCH_ASSOC);
					$S_score[$i] = $row400['score'];
					$S_score_level[$i] = $row400['level'];
					$S_score_level_fullname[$i] = score_level_fullname($S_score_level[$i]);
					
					$stmt500 = $dbh->prepare("SELECT * FROM user_profile WHERE uid=?");
					$stmt500->execute(array($uid[$i]));
					$row500 = $stmt500->fetch(PDO::FETCH_ASSOC);
					$gender[$i] = $row500['gender'];
					$age[$i]= 2021-$row500['yob'];

					
					$i++;                  
				}
	}
}

else {
	echo "CORP ID is missing.";
}

function score_level_fullname($score_level) {

    if ($score_level == "N") {
        $score_level_fullname = "Normal";
    } 
    else if ($score_level == "MI") {
        $score_level_fullname = "Mild";
    }
        else if ($score_level == "MO") {
        $score_level_fullname = "Moderate";
    }
    else if ($score_level == "S") {
        $score_level_fullname = "Severe";
    }
    else if ($score_level == "ES") {
        $score_level_fullname = "Extremely Severe";
    }
    return $score_level_fullname;
    }


?>

<html>
    <body>
	<?php 
	if(isset($_REQUEST["corp_id"]) && $_REQUEST["corp_id"] != "") {	
	$corp_id = $_REQUEST["corp_id"];
	?>
        <table border="1">
            <tr>
                <td>Sr No.</td>
                <td>User Id</td>
                <td>Gender</td>
                <td>Age</td>
                <td>Anxity Score</td>
                <td>Anxity Level</td>
                <td>Depression Score</td>
                <td>Depression Level</td>
                <td>Stress Score</td>
                <td>Stress Level </td>
                </tr>

            <?php
			
				for ($j = 0; $j < $i; $j++) {

					$sr_no= $j+1;
					echo "<tr>";
					echo "<td>" . $sr_no . "</td>";
					echo "<td>" . $uid[$j] . "</td>";
					echo "<td>" . $gender[$j] . "</td>";
					echo "<td>" . $age[$j] . "</td>";
					echo "<td>" . $A_score[$j] . "</td>";
					echo "<td>" . $A_score_level_fullname[$j] . "</td>";
					echo "<td>" . $D_score[$j] . "</td>";
					echo "<td>" . $D_score_level_fullname[$j] . "</td>";
					echo "<td>" . $S_score[$j] . "</td>";
					echo "<td>" . $S_score_level_fullname[$j] . "</td>";
					echo "</tr>";
				}
			}
            ?>
        </table>
    </body>
</html>