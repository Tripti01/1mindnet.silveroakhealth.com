<?php

include 'soh-config.php';

$corp_id = "CORP99999999";
$corp_name = "Silver Oak Health B2C";

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh->beginTransaction();
try {

    $stmt02 = $dbh->prepare("INSERT INTO corp_master VALUES(?,?,?,?,?,?,?,?,?);");
    $stmt02->execute(array($corp_id, $corp_name, '','','','','','',''));

    $stmt03 = $dbh->prepare("INSERT INTO corp_profile VALUES (?,?,?,?,?,?,?,?)");
    $stmt03->execute(array($corp_id, '', $address, '', $name, '', '', ''));

    $stmt04 = $dbh->prepare("INSERT INTO corp_dept VALUES (?,?,?)");
    $stmt04->execute(array('', $corp_id, $dept));

    $stmt05 = $dbh->prepare("INSERT INTO corp_domain VALUES (?,?)");
    $stmt05->execute(array($corp_id, ''));

    $stmt08 = $dbh->prepare("INSERT INTO corp_vertical VALUES (?,?)");
    $stmt08->execute(array($corp_id, $vertical_id));

    $stmt08 = $dbh->prepare("INSERT INTO corp_role VALUES (?,?,?)");
    $stmt08->execute(array('', $corp_id, $role));

    $stmt09 = $dbh->prepare("INSERT INTO corp_type VALUES (?,?)");
    $stmt09->execute(array($corp_id, $reg_type));

    $stmt10 = $dbh->prepare("INSERT INTO corp_users_count VALUES (?,?)");
    $stmt10->execute(array($corp_id, '0'));

    $status_code = 2; //all done successfully
    $dbh->commit();
    
} catch (PDOException $e) {
    $dbh->rollBack();
    echo $dbh->errorInfo();
    die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code :CRET_CORP_2");
}
?>
