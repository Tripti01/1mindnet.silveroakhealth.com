<?php
include 'qol_config.php';

$dbh = new PDO($dsn, $end_user, $end_pass);
$dbh->query("use qoldb");

$corp_id = "CORP10006891";

$stmt01 = $dbh->prepare("SELECT uid FROM user_type WHERE sub_type=?");
$stmt01->execute(array($corp_id));

while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
    $uid[$i] = $row01['uid'];

    $stmt20 = $dbh->prepare("SELECT * FROM user_profile WHERE uid=?");
    $stmt20->execute(array($uid[$i]));
    if ($stmt20->rowCount() != 0) {
        $row20 = $stmt20->fetch();
        $gender[$i] = $row20['gender'];
        $yob[$i] = $row20['yob'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "PN"));
        $row200 = $stmt200->fetch();
        $PN_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "EN"));
        $row200 = $stmt200->fetch();
        $EN_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "EW"));
        $row200 = $stmt200->fetch();
        $EW_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "GH"));
        $row200 = $stmt200->fetch();
        $GH_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "SF"));
        $row200 = $stmt200->fetch();
        $SF_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "PF"));
        $row200 = $stmt200->fetch();
        $PF_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "RE"));
        $row200 = $stmt200->fetch();
        $RE_score[$i] = $row200['score'];

        $stmt200 = $dbh->prepare("SELECT score FROM user_scale_score WHERE uid=? AND scale=?");
        $stmt200->execute(array($uid[$i], "RP"));
        $row200 = $stmt200->fetch();
        $RP_score[$i] = $row200['score'];
    }
    $i++;
}
?>

<html>
    <body>
        <table border="1">
            <?php
            for ($j = 0; $j <= $i; $j++) {
                echo "<tr>";
                echo "<td>" . $uid[$j] . "</td>";
                echo "<td>" . $gender[$j] . "</td>";
                echo "<td>" . $yob[$j] . "</td>";
                echo "<td>" . $PN_score[$j] . "</td>";
                echo "<td>" . $EN_score[$j] . "</td>";
                echo "<td>" . $EW_score[$j] . "</td>";
                echo "<td>" . $GH_score[$j] . "</td>";
                echo "<td>" . $SF_score[$j] . "</td>";
                echo "<td>" . $PF_score[$j] . "</td>";
                echo "<td>" . $RE_score[$j] . "</td>";
                echo "<td>" . $RP_score[$j] . "</td>";
                echo "</tr>";
            }
            ?>
        </table>
    </body>
</html>