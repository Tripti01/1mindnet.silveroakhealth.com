
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="https://cdn.silveroakhealth.com/images/favicon.png">
        <title>Corporate Information System</title>
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
        <script src="../../assets/lib/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    </head>
    <body class="page-profile">

        <?php include '../header.php'; ?>

        <div class="content content-fixed">
            <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
                <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-30">
                    <div>
                        <nav aria-label="breadcrumb">

                        </nav>
                        <h5 class="mg-b-0 tx-spacing--1"> Stress Control Online Sessions  </h5>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                                <li class="breadcrumb-item active " aria-current="page"> <span class="link-03">Showing  from</span> <?php echo $ui_from_date; ?> <span class="link-03">to </span><?php echo $ui_to_date; ?>   </li>
                            </ol>
                        </nav>
                    </div>
                    <div>
                        <div class="input-group mg-b-10">
                            <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="GET">
                                <div class="row ">
                                    <div class="col-lg-10" style="padding-right:5px">
                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span id="get_date"></span> <i class="fa fa-caret-down"></i>
                                        </div>										
                                    </div>
                                    <div class="col-lg-1" style="padding-left:0px">
                                        <input type="hidden" name="daterange" id="daterange" />
                                        <button class="btn btn-sm pd-x-8 btn-primary btn-uppercase" onclick="get_daterange()"> Show </button>
                                    </div>
                                </div>
                            </form>                        
                        </div>
                    </div>
                </div>
                <div class="row row-xs mg-t-10">
                    <div class="col-lg-12">

                    </div>
                </div>


                <div class="row row-xs mg-t-10">   
                    <div class="col-lg-12">
                        

                    </div>
                </div>

                <div class="row row-xs mg-t-10">   
                    <div class="col-lg-12">
                       

                    </div>
                </div>
            </div>
        </div>
        <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/lib/feather-icons/feather.min.js"></script>
        <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
        <script src="../../assets/lib/flot.curvedlines/curvedLines.js"></script>
        <script src="../../assets/lib/peity/jquery.peity.min.js"></script>
        <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
        <script src="../../assets/js/nav-active.js"></script>
        <script src="../../assets/js/dashforge.js"></script>
    </body>
</html>
