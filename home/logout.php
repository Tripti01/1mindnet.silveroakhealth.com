<?php

session_start();
$_SESSION = array();

if (isset($_COOKIE[session_name()])) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
}
//unset the session 
unset($_SESSION['corp_id']);
session_destroy();//destroying the session
//redirect to login page
 header("Location:http://silveroakhealth.com/HRDashboard");

?>