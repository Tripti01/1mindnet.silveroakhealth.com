<?php
include 'if_loggedin.php';
include 'host.php';

$prvg_list_array = $_SESSION['$prvg_list'];

if (!in_array("CASE_MGMT", $prvg_list_array)) {
    echo 'You dont have access to Case Management System, Please contact the Silver Oak Health Administrator for the Case Management System Access.';
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title> Silver Oak Health Case Management System</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include 'top_navbar.php'; ?>
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card-box bg-white">
                                    <h4 class="header-title mt-0 "><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Stress Control Online </h4>
                                    <ul class="list-group user-list">
                                        <li class="list-group-item">
                                            <a href="sco/" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-bullseye" aria-hidden="true"></i>  My SCO Clients </h5>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card-box bg-white">
                                    <h4 class="header-title mt-0 "><i class="fa fa-ellipsis-h" aria-hidden="true"></i> EWAP </h4>
                                    <ul class="list-group user-list">
                                        <li class="list-group-item">
                                            <a href="ewap/my_clients.php" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"> <i class="fa fa-user" aria-hidden="true"></i> My EWAP Clients </h5>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="ewap/search_user.php" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-search" aria-hidden="true"></i>  Search all EWAP Clients </h5>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="ewap/new_client.php" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-user-plus" aria-hidden="true"></i>  Create New EWAP Client Profile </h5>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="https://silveroakhealth.as.me/schedule.php" target="_blank" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-calendar" aria-hidden="true"></i> Schedule an appointment for EWAP User </h5>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="https://silveroakhealth.sharepoint.com/:x:/s/SOHPSY/EW3krJYZe5NCgrBfIBQ6R7sBHE0CJBuaVzXQR0Nx6Z7KGg?e=Gf9CO6" target="_blank" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-braille" aria-hidden="true"></i> View Corporate Offerings </h5> 
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card-box bg-white">
                                    <h4 class="header-title mt-0 "><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Other Links </h4>
                                    <ul class="list-group user-list">

                                        <li class="list-group-item">
                                            <a href="https://secure.acuityscheduling.com/admin/calendars" target="_blank" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>  Set my Availability Calender </h5>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="https://resiliencehealthprivatelimited.freshchat.com/a/343333287964217/dashboard/app" target="_blank" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-comments" aria-hidden="true"></i> Login to Chat App </h5>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <a href="https://help.acuityscheduling.com/hc/en-us/articles/219149487-Using-Our-Mobile-App" target="_blank" class="user-list-item">
                                                <div class="user-desc overflow-hidden">
                                                    <h5 class="name mt-0"><i class="fa fa-archive" aria-hidden="true"></i> Download Appointment App </h5>
                                                </div>
                                            </a>
                                        </li>


                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </div> 
            </div> 
        </div>
    </div>
    <script>
        var resizefunc = [];
    </script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Form wizard -->
    <script src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
    <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script src="assets/js/app.min.js"></script>

</body>
</html>