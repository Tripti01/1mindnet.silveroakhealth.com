<?php
#including functions and files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';


#to check if uid,snn and pageid from url passed into iframe, and perform accordingly else set all as blank
if (isset($_REQUEST['uid'])) {
    $uid = $_REQUEST['uid'];
} else {
    $uid = "";
}


#on-click of case-history-submit-btn
if (isset($_POST['case-history-submit-btn'])) {

    #to get details
    $uid = $_REQUEST['uid'];
    $history_text_encrypt = $_REQUEST['history_text'];
    $history_text = encrypt_notes($history_text_encrypt, $encryption_key_notes); //encrypt case history to save in database

    $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");

    //insert case history into thrp_case_history
    $stmt890 = $dbh->prepare("INSERT into thrp_case_history VALUES ('$tid','$uid','$history_text','1','') ;");
    $stmt890->execute(array());

    echo '<script src="../assets/js/jquery.min.js"></script>
              <script type="text/javascript"> 
                 $(document).ready(function(){
                    $("#disp_msg").addClass("alert");
                    $("#disp_msg").addClass("alert-success");
                    $("#disp_msg").html("Case history has been added.");
                });
             </script>';
}
?>

<!DOCTYPE html>
<html>

    <!-- Mirrored from coderthemes.com/adminto_1.3/light/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Jul 2016 08:36:07 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets2/global/plugins/jquery-ui/jquery-ui.min.css"/>  
        <style>
            html,body{
                background-color: white;
            }

            .modal{
                background-color: white;
            }
        </style>
    </head>
    <body>
        <form class="case-history-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" onsubmit=" return case_history();">
            <div class="modal-body">
                <div id="disp_msg" class="msg_disp" style="margin-bottom:12px;"></div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea class="form-control" rows="12" id="history_text" name="history_text" wrap="type"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                <button type="button" class="btn btn-danger waves-effect"  id="discard-close-btn" name="discard-close-btn"  >Discard Changes</button>
                <input type="submit" id="notes-submit-btn" name="case-history-submit-btn" value="Save Changes" class="btn btn-success"/>
            </div>
        </form>
        <!--Modal to confirm dismiss --> 
        <div id="myModal" class="modal fade" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                        <!-- dialog body -->
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            Are you sure you want to discard changes? 
                        </div>
                        <!-- dialog buttons -->
                        <input type="submit" value="Yes" id="close-btn" name="close-btn" class="btn btn-danger" />
                        <?php
#on-click of 'yes' close-btn
                        if (isset($_REQUEST["close-btn"])) {
                            if (isset($_REQUEST['uid'])) {
                                $uid = $_REQUEST['uid'];
                            } else {
                                echo "Unable to fetch user details. Error COde : UIDNIU";
                            }
                        }
                        ?>
                        <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                        <input type="submit" value="No" id="no-btn" name="no-btn" class="btn btn-success" data-dismiss="modal"/>
                    </form>
                </div>
            </div>
        </div>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <!--confirmation of case history -->
        <script>
            $("#discard-close-btn").click(function () {
                var old_text = " ";
                var old_text_length = old_text.length;

                var new_text = $("#history_text").val();
                var new_text_length = new_text.length;

                if (new_text_length != old_text_length) {
                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            //console.log("button pressed");  
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                } else if (new_text_length == " " && old_text_length == " ") {
                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            //console.log("button pressed");  
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                }
            });
        </script> 
    </body>
</html>