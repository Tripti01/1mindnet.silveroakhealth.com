<?php
#including files anf functions
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/get_user_name.php';
include 'functions/coach/get_session_name.php';

#GET DATE 
date_default_timezone_set("Asia/Kolkata");
$timestamp = date('Y-m-d H:i:s');

# Start the database
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#to get details regarding pending notes
#first, we find the uids of users whose call have been taken place.
#we check if notes have been taken place for that session for that particular uid or not.
#if notes are not pesent then we display the user details, else display nothing

$i = 0; //counter to fecth details of uids whose notes are pending
#to fetch uids of clients whose call status=1
$stmt10 = $dbh->prepare("SELECT uid,ssn FROM thrp_scheduled_calls WHERE tid=? AND call_status=? AND display=?");
$stmt10->execute(array($tid, 1, 1))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-PENDING-NOTES-1000].");
if ($stmt10->rowCount() != 0) {
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $uid_call_done = $row10['uid']; //pass all uids whose call has been done into variable for further calculations
        $ssn_list = $row10['ssn']; //pass all ssn into variable for further calculations
        //if the ssn is "cbtstart", then check if case history has been done or not, else check in thrp_nites
        if ($ssn_list == "CBTSTART") {
            $stmt21 = $dbh->prepare("SELECT * FROM thrp_case_history WHERE tid=? AND uid=? LIMIT 1");
            $stmt21->execute(array($tid, $uid_call_done))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-PENDING-NOTES-1001].");
            if ($stmt21->rowCount() != 0) {
                //if case history exsists,display nothing
            } else {
                $uid_list = $uid_call_done; //paasing only the uids whose notes have been taken from the above list and also use it as a flag.if isset display else dislay nothing.
                $uid[$i] = $uid_list; //passing into array
                $name[$i] = get_user_name($uid[$i]); //to fecth name
                $ssn[$i] = $ssn_list; //passing sessions of those uids whose notes have been taken
                $ssn_full_name[$i] = get_ssn_fullname($ssn_list); //to fetch the session name
                $i++;
            }
        } else {
            #to verify if the notes are taken for that particular session or not
            $stmt20 = $dbh->prepare("SELECT * FROM thrp_notes WHERE tid=? AND uid=? AND ssn=?");
            $stmt20->execute(array($tid, $uid_call_done, $ssn_list))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-PENDING-NOTES-1001].");
            if ($stmt20->rowCount() != 0) {
                //if notes exsists, display nothing
            } else {
                //if there are notes for the uid
                $uid_list = $uid_call_done; //paasing only the uids whose notes have been taken from the above list and also use it as a flag.if isset display else dislay nothing.
                $uid[$i] = $uid_list; //passing into array
                $name[$i] = get_user_name($uid[$i]); //to fecth name
                $ssn[$i] = $ssn_list; //passing sessions of those uids whose notes have been taken
                $ssn_full_name[$i] = get_ssn_fullname($ssn_list); //to fetch the session name
                $i++;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Pending Notes</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .heading{
                color:black;
            }
            .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
                background-color: #9c9c9b;
                border-color: #9c9c9b;
            }
            .btn-primary[disabled]:hover, .btn-primary.disabled:focus, .btn-primary.disabled:active, .btn-primary.active, .btn-primary.focus, .btn-primary.disabled:active, .btn-primary.disabled:focus, .btn-primary.disabled:hover, .open > .dropdown-toggle.btn-primary {
                background-color: #9c9c9b;
            }
            th{
                color:black;
            }
            td{
                vertical-align: middle;
            }
        </style>
    </head>
    
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="inbox-widget nicescroll" style="height: 600px;overflow-y:auto;">
                                        <table class="table m-0">
                                            <thead>
                                                <tr>
                                                    <th style="width:80%;">Client Name</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo '<script src="../assets/js/jquery.min.js"></script>';
                                                if (isset($uid_list)) {//only if uid is set display, else display message
                                                    for ($i = 0; $i < count($uid); $i++) {//counter to print details for all uids
                                                        ?>
                                                        <tr>
                                                            <td class="res_td"><a href="view_client.php?uid=<?php echo $uid[$i]; ?>"><?php echo $name[$i]; ?></a><br/><span style="font-size:12px;"><?php echo $ssn_full_name[$i]; ?></span></td>
                                                            <td>
                                                                <?php
                                                                //to check if the session is cbt start or not,if cbt start then open modal for cbt start,else open take notes
                                                                if ($ssn[$i] == "CBTSTART") {
                                                                    echo'<a><button id="btn_case_history_' . $i . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#case_history_' . $i . '" >Case History</button></a>';
                                                                } else {
                                                                    #take notes btn
                                                                    echo'<a><button id="btn_pending_notes_' . $i . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#take_notes_' . $i . '" >Take Notes</button></a>';
                                                                }echo'</td>';

                                                                #btn for dismisal
                                                                echo'<td ><a><button id="btn_' . $i . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#display_schedule_' . $i . '"><i class="fa fa-times"></i></button></a>';
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <!--Modal to dismissal --> 
                                                    <div id='display_schedule_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload()">x</button>
                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                </div>
                                                                <div class="modal-body" style="margin-top:5%;">
                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                        Are you sure you want to dismiss this ? 
                                                                        <div style="text-align:center;margin-top:10%;">
                                                                            <input type="submit"  name="confirm-dismiss-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                        </div>
                                                                    </form>
                                                                    <?php
                                                                    #on click 'yes' confirm btn,proceed
                                                                    if (isset($_REQUEST["confirm-dismiss-btn_$i"])) {
                                                                        #if session is cbt start then insert into casehistory else in take notes and mark diaplay as 0
                                                                        if ($ssn[$i] == "CBTSTART") {
                                                                            //to insert in casehistory with blank values but set display as 0.
                                                                            $stmt901 = $dbh->prepare("INSERT INTO thrp_case_history VALUES (?,?,?,?,?)");
                                                                            $stmt901->execute(array($tid, $uid[$i], '', '0', ''));
                                                                        } else {
                                                                            //to dissmiss it, insert into thrp_notes with blank values but set display as 0.
                                                                            $stmt400 = $dbh->prepare("INSERT INTO thrp_notes VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                                                            $stmt400->execute(array('', $tid, $uid[$i], $timestamp, $ssn[$i], '', '', '', '', '', '', '', '', '0', '', '', ''));
                                                                        }
                                                                        echo '<script>window.location.href="pending_notes.php";</script>';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                    <script>
                                                        $("#btn_pending_notes_<?php echo $i; ?>").click(function () {
                                                            var iframe = $("#take_notes_frame_<?php echo $i; ?>");//defining id
                                                            //defining src for iframe  
                                                            iframe.attr("src", "take_notes.php?uid=<?php echo $uid[$i]; ?>&ssn=<?php echo $ssn[$i]; ?>&page_id=pending_notes");
                                                        });
                                                    </script>
                                                    <!--Modal to open take notes uniquiuely -->
                                                    <div id='take_notes_<?php echo $i; ?>' class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload()">x</button>
                                                                    <h4 class="modal-title"><?php echo $ssn_full_name[$i]; ?></h4>
                                                                </div>
                                                                <div class="modal-body" style="padding-top:0%;">
                                                                    <!-- id defines frames src from the above script--> 
                                                                    <iframe id="take_notes_frame_<?php echo $i; ?>" frameborder="0" scrolling="no"  height="520px" width="100%"></iframe> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Modal to open case history uniquiuely -->
                                                    <div id='case_history_<?php echo $i; ?>' class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload()">x</button>
                                                                    <h4 class="modal-title">Case History</h4>
                                                                </div>
                                                                <div class="modal-body" style="padding-top:0%;">
                                                                    <!-- id defines frames src from the above script--> 
                                                                    <iframe src="case_history.php?uid=<?php echo $uid[$i]; ?>&page_id=pending_notes" frameborder="0" scrolling="no"  height="400px" width="100%"></iframe> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            } else {
                                                //if no clients are completed ,then display message as no clients to schedule.
                                                echo "No Clients Pending Notes.";
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/sco/pending_notes.php";
            }
        </script>
    </body>
</html>



