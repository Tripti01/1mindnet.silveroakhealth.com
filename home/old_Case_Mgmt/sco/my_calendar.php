<?php
#including functions and files
include '../if_loggedin.php';
include 'host.php';
include 'functions/coach/thrp_cal.php';
include 'functions/coach/get_user_name.php';
include 'functions/coach/get_session_name.php';

#to get year and week
$year = (isset($_GET['year'])) ? $_GET['year'] : date("Y");
$current_week = (isset($_GET['week'])) ? $_GET['week'] : date('W');

#calculation for week number
#if current week is greter than 52,set current_week as 1 and also increment year.Else if current week is less than 1 ,then decrement year and set current week as 52.
if ($current_week > 52) {
    $year++;
    $current_week = 1;
} elseif ($current_week < 1) {
    $year--;
    $current_week = 52;
}

#appending 0 to single digit week number for further calculations
if ($current_week < 10) {
    $current_week = '0' . $current_week;
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schedule Calls</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .heading{
                text-align:center;
                color: black;
            }
            .tooltip.bottom {
                margin-left:-12px;
            }
        </style>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">					
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:75%;margin-top:3%;width:35%;">
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">SCO Schedule Calls</h4>
                            </li>
                            <li>
                                <a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Switch to Table View" style="padding-bottom: 2%;
                                   padding-top: 2px;
                                   margin-top: 30%;margin-left:-30%;" href="my_schedule.php"><span style="font-size:11px;">Switch View</span></a>                                
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav pull-right">

                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li>  
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <?php
                //based on the thrp_type, sidebar changes according to privilages.
                if ($_SESSION['type'] == "CLNC") {
                    include 'sidebar_clnc.php';
                } else if ($_SESSION['type'] == "THRP") {
                    include 'sidebar_coach.php';
                }
                ?>
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    &nbsp; <!-- nbsp; is needed to compensate for the margin mis-aligned due to the fact that we added two new spans and no content inside card box !-->
                                    <div style="float:left;">
                                        Showing calendar events for the week <B> <?php echo date_weekly('1', $current_week, $year); ?> to <?php echo date_weekly('7', $current_week, $year); ?> </B>
                                    </div>
                                    <!--to see details of next or previous week.by default it is set to current week-->
                                    <div style="float:right;">
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?week=' . ($current_week == 1 ? 52 : $current_week - 1) . '&year=' . ($current_week == 1 ? $year - 1 : $year); ?>">
                                            <button class="btn btn-icon waves-effect waves-light btn-primary m-b-5">
                                                <b><font color="white"><i class="fa fa-chevron-left" style="font-size:14px"></font></i></b>
                                            </button>
                                        </a>
                                        <a href="<?php echo $_SERVER['PHP_SELF'] . '?week=' . ($current_week == 52 ? 1 : 1 + $current_week) . '&year=' . ($current_week == 52 ? 1 + $year : $year); ?>">
                                            <button class="btn btn-icon waves-effect waves-light btn-primary m-b-5">
                                                <b><font color="white"><i class="fa fa-chevron-right" style="font-size:14px;"></font></i></b>
                                            </button>
                                        </a>
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="inbox-widget nicescroll" style="height: 650px;overflow-y:auto;">
                                        <table class="table table-bordered m-0"  >
                                            <thead >
                                                <!-- to display name of day with date-->
                                                <tr style="text-align:center;">
                                                    <th></th>
                                                    <th class="heading">Monday<br/><?php echo date_weekly('1', $current_week, $year); ?></th>
                                                    <th class="heading">Tuesday<br/><?php echo date_weekly('2', $current_week, $year); ?></th>
                                                    <th class="heading">Wednesday<br/><?php echo date_weekly('3', $current_week, $year); ?></th>
                                                    <th class="heading">Thursday<br/><?php echo date_weekly('4', $current_week, $year); ?></th>
                                                    <th class="heading">Friday<br/><?php echo date_weekly('5', $current_week, $year); ?></th>
                                                    <th class="heading">Saturday<br/><?php echo date_weekly('6', $current_week, $year); ?></th>
                                                    <th class="heading">Sunday<br/><?php echo date_weekly('7', $current_week, $year); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- to display user scheduled at 7 am for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">7 AM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '07 AM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '07 AM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '07 AM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '07 AM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '07 AM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '07 AM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '07 AM');
                                                    ?>
                                                </tr>
                                                <!-- to display user scheduled at 8am for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">8 AM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '08 AM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '08 AM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '08 AM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '08 AM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '08 AM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '08 AM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '08 AM');
                                                    ?>
                                                </tr>
                                                <!-- to display user scheduled at 9 am for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">9 AM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '09 AM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '09 AM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '09 AM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '09 AM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '09 AM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '09 AM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '09 AM');
                                                    ?>
                                                </tr>
                                                <!-- to display user scheduled at 10 am for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">10 AM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '10 AM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '10 AM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '10 AM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '10 AM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '10 AM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '10 AM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '10 AM');
                                                    ?>
                                                </tr>
                                                <!-- to display user scheduled at 11 am for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">11 AM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '11 AM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '11 AM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '11 AM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '11 AM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '11 AM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '11 AM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '11 AM');
                                                    ?>
                                                </tr>
                                                <!-- to display user scheduled at 12 am for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">12 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '12 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '12 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '12 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '12 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '12 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '12 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '12 PM');
                                                    ?>
                                                </tr>
                                                <!-- to display user scheduled at  1pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">1 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '01 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '01 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '01 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '01 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '01 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '01 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '01 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  2pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">2 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '02 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '02 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '02 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '02 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '02 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '02 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '02 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  3pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">3 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '03 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '03 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '03 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '03 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '03 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '03 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '03 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  4pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">4 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '04 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '04 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '04 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '04 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '04 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '04 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '04 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  5 pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">5 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '05 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '05 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '05 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '05 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '05 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '05 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '05 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  6pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">6 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '06 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '06 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '06 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '06 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '06 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '06 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '06 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  7pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">7 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '07 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '07 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '07 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '07 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '07 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '07 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '07 PM');
                                                    ?>
                                                </tr>

                                                <!-- to display user scheduled at  8pm for all days for that particualr week select previously-->
                                                <tr>
                                                    <th class="heading" scope="row">8 PM</th>
                                                    <?php
                                                    show_user_scheduled($tid, '1', $current_week, $year, '08 PM');
                                                    show_user_scheduled($tid, '2', $current_week, $year, '08 PM');
                                                    show_user_scheduled($tid, '3', $current_week, $year, '08 PM');
                                                    show_user_scheduled($tid, '4', $current_week, $year, '08 PM');
                                                    show_user_scheduled($tid, '5', $current_week, $year, '08 PM');
                                                    show_user_scheduled($tid, '6', $current_week, $year, '08 PM');
                                                    show_user_scheduled($tid, '7', $current_week, $year, '08 PM');
                                                    ?>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>

