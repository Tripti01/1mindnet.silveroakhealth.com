<?php
# Including functions and other files
include '../if_loggedin.php';
include 'soh-config.php';
include 'host.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/view_notes.php';
include 'functions/coach/get_profile_pic.php';
include 'functions/coach/get_session_name.php';

# Check if UID is there in URL
if (isset($_REQUEST['uid'])) {
    $uid = $_REQUEST['uid'];
} else {
    echo "Unable to fetch user details. Error COde : UIDNIU";
}
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = 0;
}

#to fetch the current year
$current_year = date('Y');
#session list array
$ssn_list_name = array("SSN1", "SSN2", "SSN3", "SSN4", "SSN5", "SSN6", "SSN7", "SSN8");

#database connection
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Check if user can start CBT or not
$stmt01 = $dbh->prepare("SELECT can_start FROM user_info WHERE uid=? LIMIT 1");
$stmt01->execute(array($uid));
#if exists then dispaly as no else diaplsy as yes
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    $can_start = $row01['can_start'];
    $can_start = 1;
    if ($can_start == 0) {
        $start_cbt_status = 'NO';
    } else {
        $start_cbt_status = 'YES';
    }
} else {
#Show an error
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code : VWUS00");
}

//to fetch the product type to find the user type 
#the product type is found from user_type by fetching product code
$stmt110 = $dbh->prepare("SELECT type FROM user_type WHERE uid=? LIMIT 1");
$stmt110->execute(array($uid));
if ($stmt110->rowCount() != 0) {
    $row110 = $stmt110->fetch();
    $product_status = $row110['type']; //to find the product type
} else {
#Show an error
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code : VWUS02");
}


#TO fetch all the deatils for the given uid
$stmt30 = $dbh->prepare("SELECT * FROM user_login, user_profile WHERE user_login.uid=user_profile.uid AND user_login.uid=? LIMIT 1");
$stmt30->execute(array($uid));
if ($stmt30->rowCount() != 0) {
#Retrieve user profile and login details
    $row30 = $stmt30->fetch();
    $name = strtoupper(decrypt($row30['name'], $encryption_key)); //to display name in capital letters
    $name_normal = decrypt($row30['name'], $encryption_key); //to display name on top of modal for take naotes
    $email = $row30['email']; //to fetch email
    $yob = $row30['yob']; //to fecth year of bith to calculate age.
    $age = $current_year - $yob; //to calculate the age 
    $gender = $row30['gender']; //to fetch gender
    $marital_status = $row30['marital_status']; //to fetch martial status
    $emp_status = $row30['emp_status']; //to fetch employee status
} else {
#Show an error
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code : VWUS03");
}

#fetch location from user_location
$stmt40 = $dbh->prepare("SELECT country, state, city FROM user_location WHERE uid=? LIMIT 1");
$stmt40->execute(array($uid));
#if the location is present then fecth the details, else set as blank.
if ($stmt40->rowCount() != 0) {
    #fetch all the location details
    $user_location = $stmt40->fetch();
    $user_country = $user_location['country'];
    $user_state = $user_location['state'];
    $user_city = $user_location['city'];
} else {
    #since location is not set, then set all as blank
    $user_country = "";
    $user_state = "";
    $user_city = "";
}
$location = $user_city . " " . $user_state . " " . $user_country . ""; //dispaly location as combining user_city, user_state and user_country
# Fetch start date and End date of CBT
$stmt60 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time, user_schedule.due_datetime AS due_datetime FROM user_time_creation,user_schedule WHERE user_time_creation.uid=? AND user_schedule.uid=user_time_creation.uid AND user_schedule.ssn=? LIMIT 1 ;");
$stmt60->execute(array($uid, "CBTEND"));
if ($stmt60->rowCount() != 0) {
    $row60 = $stmt60->fetch();
    $CBTEND_datetime_unformatted = $row60['due_datetime']; //to fecth unformatted date for further calculations in extend cbt
    $CBTEND_datetime = date('d M Y', strtotime($row60['due_datetime'])); //to display CBT end date
    $CBTSTART_datetime = date('d M Y', strtotime($row60['creation_time'])); //to display CBT start date
} else {
    #if datetime is not present ,display error message
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code : VWUS04");
}

#to fetch the mobile no. from user_comm_pref
$stmt80 = $dbh->prepare("SELECT user_contact FROM user_comm_pref WHERE uid=? LIMIT 1");
$stmt80->execute(array($uid));
#if user_contact is present, then fecth mobile_no else display error
if ($stmt80->rowCount() != 0) {
    #to fetch mobile number
    $row80 = $stmt80->fetch();
    $mobile_no = $row80['user_contact'];
} else {
    #if usr_contct is not present ,display error message
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code : VWUS05");
}

## client notes tab details
#to fetch all sessions for which the notes has been taken
for ($k = 0; $k < 8; $k++) {
    $stmt190 = $dbh->prepare("SELECT * FROM thrp_notes WHERE uid=? AND ssn=? AND display=? LIMIT 8 ");
    $stmt190->execute(array($uid, $ssn_list_name[$k], 1));
    #if notes is present fetch session and also set status_notes as 1 else set ssn as blank and set status_notes as 0.
    if ($stmt190->rowCount() != 0) {
        while ($row190 = $stmt190->fetch(PDO::FETCH_ASSOC)) {
            $ssn_list[$k] = $row190['ssn']; //fecth session
            $status_notes[$k] = 1; //set status as 1
        }
    } else {
        $ssn_list[$k] = ''; //set session as blank
        $status_notes[$k] = 0; //since no notes available, set status as 0
    }
}

//to fetch all the details for case history
$stmt891 = $dbh->prepare("SELECT history_text FROM thrp_case_history WHERE uid=? LIMIT 1 ;");
$stmt891->execute(array($uid));
//if case history is written previously then fecth data for same, else set as blank
if ($stmt891->rowCount() != 0) {
    $row891 = $stmt891->fetch();
    if ($row891['history_text'] != "") {
        //since case history is not null , decrypt
        $text_history = decrypt_notes($row891['history_text'], $encryption_key_notes); //decrypting case history to display
        $text_status = 1;
    } else {
        //since case history is not there , set as blank
        $text_history = "";
        $text_status = 1;
    }
} else {
    //since case history is not there , set as blank
    $text_history = "";
    $text_status = 0;
}

#on-click of case-history-submit-btn
if (isset($_POST['case-history-submit-btn'])) {

    #to get details
    $uid = $_REQUEST['uid'];
    $text_status = $_REQUEST['text_status'];
    $history_text_encrypt = $_REQUEST['history_text'];
    $history_text = encrypt_notes($history_text_encrypt, $encryption_key_notes); //encrypt case history to save in database

    $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");

    //if case history is already there,then update.else insert into thrp_case_history
    if ($text_status == 1) {

        //update casehistory
        $stmt892 = $dbh->prepare("UPDATE thrp_case_history SET history_text=? WHERE tid=? AND uid=? LIMIT 1 ;");
        $stmt892->execute(array($history_text, $tid, $uid));
    } else {

        //insert case history into thrp_case_history
        $stmt890 = $dbh->prepare("INSERT into thrp_case_history VALUES ('$tid','$uid','$history_text','1','') ;");
        $stmt890->execute(array());
    }

    header('Location:' . $host . '/sco/view_client.php?uid=' . $uid . '&status=1'); //reload page
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>View Clients</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />        
        <link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <!--link href="../assets2/global/plugins/jquery-ui/jquery-ui.min.css"/-->  
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            th{
                color:#8896a0;
                font-size: 14px;
                font-weight: 600px;
                padding: 8px;
                line-height: 1.42857143;
            }
            .td_notes{
                width:15%
            }
            .td_notes1{
                width:35%;
                text-align: center;

            }
            .td_notes2{
                width:15%;
                text-align: center;

            }
        </style>
    </head> 
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include '../top_navbar.php'; ?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-9 col-lg-10 col-md-10 col-xs-9">
                                <div class="bg-picture card-box" >
                                    <div class="profile-info-name">
                                        <div class="inbox-item-img"><img alt="" class="img-circle" src="../assets2/user/profile_pic/<?php get_profile_pic($uid); ?>"></div>
                                        <div class="profile-info-detail">
                                            <table border="0" width="100%" style="cell-spacing:10px;">
                                                <tr>
                                                    <td colspan="3"><span class="client-name"><?php echo $name; ?></span> </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        <table>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"> <i class="zmdi zmdi-account-box-mail"></i> </span></td>
                                                                <td> <span class="profile-field-value"><?php
                                                                        if (isset($gender) && ($gender != "")) {
                                                                            #display gender messages based on the data fetched and also displaying age
                                                                            if ($gender == 'F') {//Female
                                                                                echo " Female, $age years ";
                                                                            } else if ($gender == 'M') {//Male
                                                                                echo " Male, $age years";
                                                                            } else {//Would rather not say
                                                                                echo "Would rather not say, $age years";
                                                                            }
                                                                        } else {
                                                                            //if gender is not stated, display as balnk.
                                                                            echo "";
                                                                        }
                                                                        ?> </span></td>
                                                            </tr>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"> <i class="zmdi zmdi-balance"></i></span></td>
                                                                <td>  <span class="profile-field-value">

                                                                        <?php
//display employee status as messages based on the values fetched
                                                                        switch ($emp_status) {
                                                                            case 10 : $emp_status = "Full time paid work";

                                                                                break;
                                                                            case 11 : $emp_status = "Part time paid work";

                                                                                break;
                                                                            case 12 : $emp_status = "Studying";

                                                                                break;
                                                                            case 13 : $emp_status = "Unemployed";

                                                                                break;
                                                                            case 14 : $emp_status = "Permanently sick or disabled";

                                                                                break;
                                                                            case 15 : $emp_status = "Retired";

                                                                                break;
                                                                            case 99 : $emp_status = "Other";

                                                                                break;
                                                                            case 0:$emp_status = " ";
                                                                                break;
                                                                        }
                                                                        ?> 
                                                                        <?php
                                                                        #if employee status is set then echo the message else display blank message.
                                                                        if (isset($emp_status) && ($emp_status != '0')) {
                                                                            echo $emp_status;
                                                                        } else {
                                                                            //if employment status is not set,then display blank message
                                                                            echo "";
                                                                        }
                                                                        ?> </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"><i class="zmdi zmdi-pin"></i></span></td>
                                                                <td> <span class="profile-field-value"><?php
                                                                        //if location is set then display location else display blank message
                                                                        if (isset($location)) {
                                                                            echo $location;
                                                                        } else {
                                                                            //if location is not set,display balnk message
                                                                            echo "";
                                                                        }
                                                                        ?></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"><i class="zmdi zmdi-phone"></i> </span></td>
                                                                <td> <span class="profile-field-value"><?php
                                                                        //if mobile_no is st then display mobile_no, else diaply balnk messgae
                                                                        if (isset($mobile_no)) {
                                                                            echo $mobile_no;
                                                                        } else {
                                                                            echo "";
                                                                            //if mobile number is not set, display blank message
                                                                        }
                                                                        ?> </span></td>
                                                            </tr>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"><i class="zmdi zmdi-email"></i></span></td>
                                                                <td><span class="profile-field-value">   <?php
                                                                        //if email is set then display email,else dipaly blank message.
                                                                        if (isset($email)) {
                                                                            echo $email;
                                                                        } else {
                                                                            echo "";
                                                                            //if email is not set, displayblank messgae
                                                                        }
                                                                        ?> </span></td>
                                                            </tr>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"><i class="zmdi zmdi-account-box-o"></i> </span></td>
                                                                <td> <span class="profile-field-value"><?php
                                                                        //if product staua is set then display accoringly,else dipaly as blank
                                                                        if (isset($product_status)) {
                                                                            if ($product_status == 'CR') {//Corporate
                                                                                echo 'Corporate';
                                                                            } else if ($product_status == 'INID') {//Individual
                                                                                echo 'Individual';
                                                                            } else if ($product_status == 'TH') {//Individual
                                                                                echo 'Individual';
                                                                            }
                                                                        } else {
                                                                            //if product status is not set then display blank message
                                                                            echo '';
                                                                        }
                                                                        ?> </span>
                                                                </td>																
                                                            </tr>
                                                        </table>  
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>  <span class="profile-field-icon"><i class="zmdi zmdi-timer"></i></span></td>
                                                                <td><span class="profile-field-value"><?php echo $CBTSTART_datetime; ?></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td> <span class="profile-field-icon"> <i class="zmdi zmdi-timer-off"></i></span></td>
                                                                <td><span class="profile-field-value"><?php echo $CBTEND_datetime; ?></span></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2"> 
                                                                    <?php
#if start_cbt_status is 'no',then display cannot start cbt else display can start
                                                                    if (strcmp($start_cbt_status, 'NO') == 0) {
                                                                        #since start_cbt_status is 'no',click btn to open a modal wchich helps to grant access to start cbt
                                                                        echo '<span class="profile-field-icon"> <span class="label label-danger" id="start" style="cursor:pointer;">
									Cannot Start CBT 
									</span>';
                                                                    } else {
                                                                        #since start_cbt_status is not 'no',then display as can start cbt
                                                                        echo '<span class="profile-field-icon"> <span class="label label-primary">
									Can Start CBT 
									</span></span>';
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-2 col-xs-3 col-lg-2" style="padding-right:1.41%;padding-left:0.46%;">
                                <div class="card-box" style="padding-top:18%;padding-bottom: 15%;padding-left:12%;padding-right:12%;">
                                    <!--Case history btn ---->
                                    <a href="#">
                                        <button class="btn btn-primary waves-effect waves-light" data-toggle="modal"  data-target="#con-close-modal_1" style="padding-left:5px;padding-right:7px;"><i class="ti-book m-r-5"></i> <span>Case History</span></button>    
                                    </a>
                                    <!--Schedule call btn---->
                                    <a href="#" >
                                        <?php
                                        echo '<button class="btn btn-primary waves-effect waves-light" data-toggle="modal"  data-target="#schedule_call" style="margin-top:6%;padding-left:5px;padding-right:5px;"><i class="fa fa-phone m-r-5"></i> <span>Schedule Call</span></button>  ';
                                        ?>
                                    </a>
                                    <!-- Extend cbt btn---->
                                    <a href="#">
                                        <button class="btn btn-primary waves-effect waves-light" data-toggle="modal"  data-target="#con-close-modal_2" style="margin-top:6%;padding-left:5px;padding-right:13px;"><i class="ti-alarm-clock m-r-5"></i> <span>Extend CBT</span></button>    
                                    </a>

                                </div>
                            </div>
                            <!---modal to open case history ---> 
                            <div id="con-close-modal_1" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <form class="case-history-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" onsubmit=" return case_history();">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                                <h4 class="modal-title">Case History</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div id="reschedule_msg" class="msg_disp"></div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <textarea class="form-control" rows="12" id="history_text" name="history_text" wrap="type"><?php
//if case history is not balnk, then display the case history.else diaplay blank
                                                                if ($text_history != "") {
                                                                    echo htmlspecialchars($text_history);
                                                                } else if ($text_history == "") {//since casehistory is not there,display blank
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                                                <input type="hidden" name="text_status" value="<?php echo $text_status; ?>"/> 
                                                <button type="button" class="btn btn-danger waves-effect" style="margin-right: 560px;" id="discard-close-btn" name="discard-close-btn"  >Discard Changes</button>
                                                <input type="submit" id="notes-submit-btn" name="case-history-submit-btn" value="Save Changes" class="btn btn-success"/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Modal to confirm dismiss --> 
                        <div id="myModal" class="modal fade" data-keyboard="false" data-backdrop="static">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                        <!-- dialog body -->
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            Are you sure you want to discard changes? 
                                        </div>
                                        <!-- dialog buttons -->
                                        <input type="submit" value="Yes" id="close-btn" name="close-btn" class="btn btn-danger" />
                                        <?php
#on-click of 'yes' close-btn,close the modal and reload the page
                                        if (isset($_REQUEST["close-btn"])) {
                                            if (isset($_REQUEST['uid'])) {
                                                $uid = $_REQUEST['uid'];
                                            } else {
                                                echo "Unable to fetch user details. Error COde : UIDNIU";
                                            }
                                            echo '<script>window.location.href="view_client.php?uid=' . $uid . '";</script>'; //reload page
                                        }
                                        ?>
                                        <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                                        <input type="submit" value="No" id="no-btn" name="no-btn" class="btn btn-success" data-dismiss="modal"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--- Modal to schedule call for future sessions,i.e scheudle call for sessions whcih are not yet been completed by client
                        <div id="schedule_call" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload();">x</button>
                                        <h4 class="modal-title">Schedule Call</h4>
                                    </div>
                                    <div class="modal-body" style="margin-top:0%;padding-top: 5%;">
                                        <iframe src="schedule_date.php?uid= echo $uid; ?>&page_id=view" frameborder="0" scrolling="no"  height="390px" width="100%"></iframe> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        !---> 
                    </div>
                    <!-- modal to give permission to start cbt or not-->
                    <div id="start_cbt_modal" class="modal fade" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                    <!-- dialog body -->
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        Are you sure user can start cbt? 
                                    </div>
                                    <input type="submit" value="Yes" id="close-btn" name="can-start-btn" class="btn btn-success" />
                                    <?php
#on click 'yes-btn'
                                    if (isset($_REQUEST["can-start-btn"])) {
                                        #check if uid is set or not.if set then proceed
                                        if (isset($_REQUEST['uid'])) {
                                            #if uid is set , update can_start as 1.
                                            $uid = $_REQUEST['uid'];
                                            $stmt4 = $dbh->prepare("UPDATE user_info SET can_start = '1' WHERE uid=? LIMIT 1");
                                            $stmt4->execute(array($uid));
                                            echo '<script>window.location.href="view_client.php?uid=' . $uid . '";</script>'; //reload page
                                        } else {
                                            #if uid is not set then ,then display unable to fetch user details
                                            echo "Unable to fetch user details. Error COde : UIDNIU";
                                        }
                                    }
                                    ?>
                                    <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                                    <input type="submit" value="No" id="no-btn" name="no-btn" class="btn btn-danger" data-dismiss="modal"/>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---modal to open cextend cbt ---> 
                    <div id="con-close-modal_2" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content" style="height:25%;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload()">x</button>
                                    <h4 class="modal-title">Extend CBT</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="extend_msg" class="msg_disp"></div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12" >
                                                <form action="update_expiry_date.php" method="POST" >
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="datepicker" value="<?php echo date('d-m-Y', strtotime($CBTEND_datetime_unformatted)); ?>" name="expiry_date">
                                                        <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                                                        <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                                    </div><!-- input-group -->
                                                    <div align="center" style="margin-top:30px;">
                                                        <input type="submit" name="btn-submit-cbt" class="btn btn-success" value="Update" >
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Tabs-->
                    <div class="row"> 
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                            <div class="panel panel-color panel-tabs panel-info"> 
                                <div class="panel-heading">
                                    <ul class="nav nav-pills">
                                        <li class="active">
                                            <a href="#navpills-1" data-toggle="tab" aria-expanded="true">Assessments</a>
                                        </li>
                                        <li class="">
                                            <a href="#navpills-2" data-toggle="tab" aria-expanded="false">Client-Activity</a>
                                        </li>
                                        <li class="">
                                            <a href="#navpills-3" data-toggle="tab" aria-expanded="false">Client-Notes</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-body"> 
                                    <div class="tab-content"> 
                                        <!-- Assessments--->
                                        <div id="navpills-1" class="tab-pane fade in active"> 
                                            <div class="row"> 
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12"> 
                                                    <div class="card-box card-tabs" style="padding-top: 0px"> 
                                                        <ul class="nav nav-pills pull-right"> 
                                                            <li class="active"> 
                                                                <a href="#cardpills-1" data-toggle="tab" aria-expanded="true">Pre-Post Assessments</a> 
                                                            </li> 
                                                            <li class=""> 
                                                                <a href="#cardpills-2" data-toggle="tab" aria-expanded="false">Mood Survey</a> 
                                                            </li> 
                                                            <li class=""> 
                                                                <a href="#cardpills-3" data-toggle="tab" aria-expanded="false">Analytics</a> 
                                                            </li> 
                                                        </ul> 
                                                        <div class="tab-content"> 
                                                            <!-- Pre-Post Assessments tab--->
                                                            <div id="cardpills-1" class="tab-pane fade in active"> 
                                                                <div class="row"> 
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12"> 
                                                                        <!--open in a fame --->
                                                                        <iframe frameborder="0" scrolling="no"  src="frames/asmt_report.php?uid=<?php echo $uid; ?>" height="360px" width="100%"></iframe> 
                                                                    </div> 
                                                                </div> 
                                                            </div> 
                                                            <!-- Mood survey tab--->
                                                            <div id="cardpills-2" class="tab-pane fade"> 
                                                                <div class="row"> 
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">  
                                                                        <!--open in a frame --->
                                                                        <iframe frameborder="0" scrolling="no" src="frames/client_mood.php?uid=<?php echo $uid; ?>" height="360px" width="100%"></iframe> 
                                                                    </div> 
                                                                </div> 
                                                            </div> 
                                                            <div id="cardpills-3" class="tab-pane fade"> 
                                                                <div class="row"> 
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12"> 
                                                                        <!--open in a frame --->
                                                                        <iframe frameborder="0" scrolling="no" src="frames/anaytics.php?uid=<?php echo $uid; ?>" height="600px" width="100%"></iframe> 
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div> 
                                        <!-- Tab for user activity-->
                                        <div id="navpills-2" class="tab-pane fade"> 
                                            <div class="row"> 
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">  
                                                    <div class="card-box card-tabs" style="padding-top: 0px"> 
                                                        <div class="tab-content"> 
                                                            <div id="cardpills-11" class="tab-pane fade in active"> 
                                                                <div class="row"> 
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">  
                                                                        <iframe frameborder="0" scrolling="no"  src="frames/user_activity.php?uid=<?php echo $uid; ?>" height="420px" width="100%"></iframe> 
                                                                    </div> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div>
                                        <!--tab for client notes --->
                                        <div id="navpills-3" class="tab-pane fade"> 
                                            <div class="row"> 
                                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">  
                                                    <div class="card-box card-tabs" style="padding:0px;"> 
                                                        <div class="tab-content"> 
                                                            <div id="cardpills-11" class="tab-pane fade in active"> 
                                                                <div class="row"> 
                                                                    <div class="col-md-12 col-sm-12 col-xs-10 col-lg-12"> 
                                                                        <div class="card-box" style="margin-bottom:0%;padding-top: 0%;padding-bottom: 0%;">
                                                                            <div class="table-responsive">
                                                                                <table class="table" style="margin-top: 0px;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th class="td_notes">SESSIONS</th>
                                                                                            <th class="td_notes1">NOTES STATUS</th>
                                                                                            <th></th>
                                                                                            <th></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody >
                                                                                        <?php
                                                                                        for ($k = 0; $k < 8; $k++) {//counter for all 8 sessions
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="td_notes">
                                                                                                    <b><?php
                                                                                                        $ssn_name[$k] = get_ssn_fullname($ssn_list_name[$k]); //to fetch session name
                                                                                                        echo $ssn_name[$k]; //to display session full name
                                                                                                        ?></b>
                                                                                                </td>
                                                                                                <td class="td_notes1">
                                                                                                    <?php
                                                                                                    if ($status_notes[$k] == 0) {//if notes are not taken, color is set as grey
                                                                                                        echo '<font color=#9e9e9e size=2px><i class="fa fa-edit"></i></font>';
                                                                                                    } else if ($status_notes[$k] == 1) {//if notes are taken, color is set as blue
                                                                                                        echo '<font color=#223C80 size=2px><i class="fa fa-edit"></i></font>';
                                                                                                    } else {//by default, color is set as grey
                                                                                                        echo '<font color=#9e9e9e size=2px><i class="fa fa-edit"></i></font>';
                                                                                                    }
                                                                                                    ?>
                                                                                                </td>
                                                                                                <td class="td_notes2">
                                                                                                    <?php
                                                                                                    #if notes_status is set as 1 ,then display edit and view notes.else display only take notes.
                                                                                                    if ($status_notes[$k] == 1) {
                                                                                                        #diaplsying edit and view notes
                                                                                                        echo '<button id="btn_notes_' . $k . '" class="btn btn-primary waves-effect waves-light" data-toggle="modal"  data-target="#take_notes_' . $k . '">Edit Notes</button>';
                                                                                                        echo '</a></td>';
                                                                                                        echo ' <td class="td_notes2">';
                                                                                                        echo '<a href="#" >';
                                                                                                        echo '<button id="btn_notes_' . $k . '" class="btn btn-primary waves-effect waves-light" data-toggle="modal"  data-target="#navpills-4_' . $k . '" >View Notes</button>';

                                                                                                        echo '</td>';
                                                                                                    } else {
                                                                                                        #display take notes
                                                                                                        echo '<button id="btn_notes_' . $k . '" class="btn btn-primary waves-effect waves-light" data-toggle="modal"  data-target="#take_notes_' . $k . '">Take Notes</button>';
                                                                                                        echo '   </td><td class="td_notes2">';
                                                                                                        echo '</td>';
                                                                                                    }
                                                                                                    ?>

                                                                                                    <script src="../assets/js/jquery.min.js"></script>
                                                                                                    <!-- Script to identify unique source for each take notes-->
                                                                                                    <script>
                                            $("#btn_notes_<?php echo $k; ?>").click(function () {
                                                var iframe = $("#take_notes_frame_<?php echo $k; ?>");//defining id 
                                                //defining src for iframe
                                                iframe.attr("src", "take_notes.php?uid=<?php echo $uid; ?>&ssn=<?php echo $ssn_list_name[$k]; ?>&page_id=view");
                                            });
                                                                                                    </script>
                                                                                                    <!-- Modal to open taken notes-->
                                                                                                    <div id='take_notes_<?php echo $k; ?>' class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                                                        <div class="modal-dialog modal-lg">
                                                                                                            <div class="modal-content">
                                                                                                                <div class="modal-header">
                                                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload()">x</button>
                                                                                                                    <h4 class="modal-title"><?php echo $name_normal; ?> - <?php echo $ssn_name[$k]; ?></h4>
                                                                                                                </div>
                                                                                                                <div class="modal-body" style="padding-top:0%;">
                                                                                                                    <!-- id defines src for frame defined in script above-->
                                                                                                                    <iframe id="take_notes_frame_<?php echo $k; ?>" frameborder="0" scrolling="no"  height="520px" width="100%"></iframe> 
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <!-- View notes--> 
                                                                                                    <div id="navpills-4_<?php echo $k; ?>" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                                                        <div class="modal-dialog modal-lg">
                                                                                                            <div class="modal-content">
                                                                                                                <div class="row"> 
                                                                                                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">  
                                                                                                                        <div class="panel-group"  role="tablist">
                                                                                                                            <div class="modal-header">
                                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                                                                                                <h4 class="modal-title"><?php echo $ssn_name[$k]; ?></h4>
                                                                                                                            </div>
                                                                                                                            <div class="panel panel-default bx-shadow-none">
                                                                                                                                <div class="panel-body">
                                                                                                                                    <?php view_notes($uid, $tid, $ssn_list_name[$k]); ?>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div><!-- end col -->    
                                                                                                        </div> 
                                                                                                    </div>
                                                                                                    </div>
                                                                                            </tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- end col -->
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- Plugins Js -->
        <script src="../assets/plugins/moment/moment.js"></script>
        <script src="../assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <!-- App js -->
        <script src="../assets/js/app.min.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script>
        <script>
            var date = new Date();
            date.setDate(date.getDate());

            $('#datepicker').datepicker({
                startDate: date
            });
        </script>
        <script>
            $(".case-history-form").validate({
                rules: {
                    history_text: {
                        required: true,
                    }
                },
                messages: {
                    history_text: {
                        required: "Please enter some text",
                    }
                }
            });
        </script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/sco/view_client.php?uid=<?php echo $uid; ?>";
                    }
        </script>
        <script>
            // Date Picker
            jQuery('#datepicker').datepicker();
            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        </script>
        <script>
            $("#start").click(function () {

                $("#start_cbt_modal").modal({// wire up the actual modal functionality and show the dialog
                    "backdrop": "static",
                    "keyboard": true,
                    "show": true                     // ensure the modal is shown immediately
                });
            });
        </script>

        <script>
            $(function () {
                $('#myModal').someDialogPlug({some: options});
            })
        </script>
        <!--confirmation of case history -->
        <script>
            $("#discard-close-btn").click(function () {
                var old_text = "<?php echo $text_history; ?>";
                var old_text_length = old_text.length;

                var new_text = $("#history_text").val();
                var new_text_length = new_text.length;

                if (new_text_length != old_text_length) {
                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            //console.log("button pressed");  
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                } else if (new_text_length == " " && old_text_length == " ") {
                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            //console.log("button pressed");  
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                }
            });
        </script> 
        <?php if ($status == 1) { ?>
            <script type="text/javascript">
                $("#reschedule_msg").addClass("alert");
                $("#reschedule_msg").addClass("alert-success");
                $("#reschedule_msg").html("Case history has been added.");
                $("#con-close-modal_1").modal({"backdrop": "static"});
            </script>
        <?php } else if ($status == 2) { ?>
            <script type="text/javascript">
                $("#extend_msg").addClass("alert");
                $("#extend_msg").addClass("alert-success");
                $("#extend_msg").html("CBT has been extended.");
                $("#con-close-modal_2").modal({"backdrop": "static"});
            </script> 
        <?php } ?>
    </body>
</html>