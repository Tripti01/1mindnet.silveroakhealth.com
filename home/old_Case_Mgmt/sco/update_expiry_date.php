<?php

#this page is used to update expiry date 
#this page is used in 2 pages, View_client.php and view_Client_report.php
#in view_client.php, in btn-submit-sbt, the update occurs
#in view_client_report.php, since the request comes from ajax, we check if the page_id is set or not and then update

if ((isset($_REQUEST["btn-submit-cbt"])) || (isset($_REQUEST['page_id']))) {
    
    # Including functions and other files
    include 'soh-config.php';
	include 'host.php';

    #check if uid is set or not.if set then proceed
    if (isset($_REQUEST['uid'])) {
        #fetch uid
        $uid = $_REQUEST['uid'];

        //fecth expiry date
        $update_expiry_date = $_REQUEST['expiry_date'];
        $update_expiry_date = str_replace('/', '-', $update_expiry_date); //replacing / by - for calculation 
        $cbt_endtime = date('Y-m-d 23:59:59', strtotime($update_expiry_date));

        # Start the database 
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");
        //updating the extended cbt date
        $stmt03 = $dbh->prepare("UPDATE user_schedule set due_datetime =?  WHERE uid=? and ssn=?");
        $stmt03->execute(array($cbt_endtime, $uid, 'CBTEND'));

        if (isset($_REQUEST['page_id'])) {
            if ($_REQUEST['page_id'] == 'view_report') {
                $tid = $_REQUEST['tid'];
                echo "1";
                //header("Location:" . $host . "/sco/view_client_report.php?tid=" . $tid . "&type=end_date_reached&status=2&modal_id=" . $modal_id); //reloading back to view client report page
            } else {
                header("Location:" . $host . "/sco/view_client.php?uid=" . $uid . '&status=2'); //reloading back to view client page
            }
        } else {
            header("Location:" . $host . "/sco/view_client.php?uid=" . $uid . '&status=2'); //reloading back to view client page
        }
    } else {
        #if uid is not set then ,then display unable to fetch user details
        echo "Unable to fetch user details. Error Code : UIDNIU";
    }
}
?>