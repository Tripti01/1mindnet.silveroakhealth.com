<?php
#includinf functions and files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/get_session_name.php';
include 'functions/coach/get_user_name.php';

//to check if uid,ssn and page_id are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['uid'])) {
    $uid = $_REQUEST['uid'];
} else {
    $uid = "";
}
if (isset($_REQUEST['ssn'])) {
    $ssn_prev = $_REQUEST['ssn'];
    $ssn_fullname = get_ssn_fullname($ssn_prev);
} else {
    $ssn_prev = "";
}
if (isset($_REQUEST['page_id'])) {
    $page_id = $_REQUEST['page_id'];
} else {
    $page_id = "";
}

#getting tid from session
$tid = $_SESSION['tid'];

#session list
$ssn_list = array("INTRODUCTION", "SESSION-1", "SESSION-2", "SESSION-3", "SESSION-4", "SESSION-5", "SESSION-6", "SESSION-7", "SESSION-8");
#status error is set as 0 by default
$status_error = 0;

# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#to fetch name
$name_decrypt = get_user_name($uid);

#only if ssn is set, i.e if we pass ssn it means we are re-scheduling or scheduling call for that particular session
if (isset($_REQUEST['ssn'])) {
#first if user has completed session or not,if yes thn set status as 1 els 0
    $stmt200 = $dbh->prepare("SELECT completed_on FROM thrp_user_ssn_completed WHERE tid=? AND uid=? AND ssn=? LIMIT 1");
    $stmt200->execute(array($tid, $uid, $ssn_prev))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-DATE-1001].");
    if ($stmt200->rowCount() != 0) {
        $row200 = $stmt200->fetch();
        $ssn_endtime = date('d-M-Y h:i A', strtotime($row200['completed_on'])); //to display completed_on datetime
        $completed_on = $row200['completed_on']; //to fecth compketed_on for further calculations
#if the call has already been scheduled for the particular ssn, uid and tid then re-schedule else schedule
        $stmt300 = $dbh->prepare("SELECT * FROM thrp_scheduled_calls WHERE tid=? AND uid=? AND ssn=? LIMIT 1");
        $stmt300->execute(array($tid, $uid, $ssn_prev))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-DATE-1002].");
        if ($stmt300->rowCount() != 0) {
            $scheduled_status = 1; //re-scheduling call
        } else {
            $scheduled_status = 0; //scheduling call
        }
    } else {
        $ssn_endtime = "Not Yet Completed."; //for future calls,i.e even befor client completes session scheduling call
        $completed_on = "";
        $scheduled_status = 1; //scheduling call
    }
} else {
    $scheduled_status = 0; //scheduling call
}

#to disable all the sessions for wchich the calls have been scheduled
$ssn_list = array();
#to fetch all sessions for which the notes has been taken
$stmt190 = $dbh->prepare("SELECT ssn FROM thrp_scheduled_calls WHERE uid=? AND tid=? LIMIT 8 ");
$stmt190->execute(array($uid, $tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-DATE-1003].");
while ($row190 = $stmt190->fetch(PDO::FETCH_ASSOC)) {
    if ($stmt190->rowCount() != 0) {
        $ssn_list[] = $row190['ssn'];
    } else {
        $ssn_list[] = '';
    }
}

#on click of schedule call btn
if (isset($_POST['schedule-submit-btn'])) {

#to fecth tid from session
    $tid = $_SESSION['tid'];

#to fetch details from form
    $uid = $_POST['uid'];
    $scheduled_status = $_REQUEST['scheduled_status'];
#GET current date
    date_default_timezone_set("Asia/Kolkata");
    $date_current = date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s')));
    if (isset($_POST['page_id'])) {//for scheduling through view client only
        $page_id = $_POST['page_id'];
    } else {
        $page_id = "";
    }
    $time = $_POST['time'];
    $date = $_POST['date'];
    $date = str_replace('/', '-', $date);
    $date = date("Y-m-d ", strtotime($date)); //date from date picker
    $time = date("H:i:s ", strtotime($time)); //time from time picker
    $scheduled_timestamp = $date . $time; //combining both date and time
    #first we check if the datetime is greater than cureent time, if not then we dispaly message as past datetime schedule cant be set, else we continue.
    if ($date_current < $scheduled_timestamp) {

#if the ssn is set from url then fetch the ssn ,else fetch from from dropdown,i.e ssn_schedule
        if (isset($_POST['ssn'])) {
            $ssn = $_POST['ssn'];
        } else {
            $ssn = $_POST['ssn_schedule'];
        }

#if the session is been completed then set completed_on else leave it blank
        if (isset($_POST['completed_on'])) {
            $completed_on = $_POST['completed_on'];
        } else {
            $completed_on = "";
        }

        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        if ($page_id == "view") {
            #to insert into thrp_scheduled_calls table
            $stmt10 = $dbh->prepare("INSERT into thrp_scheduled_calls VALUES ('','$tid','$uid','$ssn','$completed_on','$scheduled_timestamp','0','1','') ");
            $stmt10->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-DATE-1005].");

            echo'<script src="../assets/js/jquery.min.js"></script>
                         <script type="text/javascript"> 
                            $(document).ready(function(){
                                 $("#disp_msg").addClass("alert");
                                 $("#disp_msg").addClass("alert-success"); 
                                 $("#disp_msg").html("Call is Scheduled.");
                            });
                    </script>';
        } else if ($scheduled_status == 1) {//re-schedule_status=1 is for re-scheduling
            $stmt100 = $dbh->prepare("UPDATE thrp_scheduled_calls SET scheduled_date=? WHERE tid=? AND uid=? AND ssn=? LIMIT 1");
            $stmt100->execute(array($scheduled_timestamp, $tid, $uid, $ssn))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-DATE-1004].");

            echo'<script src="../assets/js/jquery.min.js"></script>
                         <script type="text/javascript"> 
                            $(document).ready(function(){
                                 $("#disp_msg").addClass("alert");
                                 $("#disp_msg").addClass("alert-success");     
                                 $("#disp_msg").html("Call is Re-Scheduled.");
                            });
                    </script>';
        } else if ($scheduled_status == 0) {//for scheduling call
            #to update thrp_scheduled_calls table
            $stmt10 = $dbh->prepare("INSERT into thrp_scheduled_calls VALUES ('','$tid','$uid','$ssn','$completed_on','$scheduled_timestamp','0','1','') ");
            $stmt10->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-DATE-1005].");


            $stmt400 = $dbh->prepare("UPDATE thrp_user_ssn_completed SET scheduled_status='1' WHERE uid=? AND tid=? AND completed_on=? LIMIT 1");
            $stmt400->execute(array($uid, $tid, $completed_on))or die(print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[THRP-SCHEDULE-DATE-1006]."));

            echo'<script src="../assets/js/jquery.min.js"></script>
                         <script type="text/javascript"> 
                            $(document).ready(function(){
                                 $("#disp_msg").addClass("alert");
                                 $("#disp_msg").addClass("alert-success"); 
                                 $("#disp_msg").html("Call is Scheduled.");
                            });
                    </script>';
        }
    } else {
        //if datetime schedueled is past time,display message
        $status_error = 1;
    }
}
?>

<!DOCTYPE html>
<html>

    <!-- Mirrored from coderthemes.com/adminto_1.3/light/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Jul 2016 08:36:07 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

        <!-- App title -->
        <title>Schedule Date</title>

        <!-- Plugins css-->
        <link href="../assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <script src="../assets/js/modernizr.min.js"></script>

        <style>
            th{
                color:black;
            }
            .dataTables_info{
                color:#C0C0C0;
            }
            .smallscreen{
                background-color: white;
                padding-bottom: 0px;
            }
            td{
                vertical-align: middle;
                margin-bottom: 20px;
            }
            tr{
                padding-bottom: 20px;
            }
            .control-label{
                color:#9E9E9E;

            }
            html{
                background-color: white;
            }
            option:disabled, select[disabled] > option {
                color: grey;
                background-color: rgba(128, 128, 128, 0.25);
            }
        </style>
    </head>
    <body>
        <div class="row">

            <!-- To schedule the call for particular client-->
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-color panel-info">
                    <!--div class="panel-heading">
                        <h3 class="panel-title">Schedule This Call</h3>
                    </div-->
                    <div class="panel-body" style="margin-top:0%;padding-top: 0%;">
                        <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" >
                            <div id="disp_msg" class="msg_disp" style="margin-left:-20px;margin-bottom:12px;"></div>
                            <div class="row" >
                                <?php
                                if ($status_error == 1) {
                                    echo '<div id="err_time" style="margin-left:-10px;margin-bottom:20px;">Your scheduling call for past time.Please select another date and time.</div>';
                                } else {
                                    //display nothing
                                }
                                ?>
                                <table style="width:100%;">
                                    <thead>
                                    <th style="width:43%;text-align: center;"></th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><label class="control-label" style="margin-left:-10px;">Client Name</label></td>
                                            <td><label class="control-label"><?php echo $name_decrypt; ?></label></td>
                                        </tr>
                                        <tr><?php if (isset($_REQUEST['ssn'])) { ?>
                                                <td><label class="control-label" style="margin-left:-10px;">For Session</label></td>
                                                <td><?php echo $ssn_fullname; ?></td>
                                            <?php } else { ?>
                                                <td></td>
                                            <?php }
                                            ?>
                                        </tr>
                                        <tr>
                                            <?php if ($ssn_prev == "CBTSTART") { ?>
                                                <td> </td>
                                                <td><br/></td>
                                            <?php } else if (isset($_REQUEST['ssn'])) { ?>
                                                <td><label class="control-label" style="margin-left:-10px;">Session Completed on</label></td>
                                                <td><?php echo $ssn_endtime; ?><br/></td>

                                            <?php } else { ?>
                                                <td><label class="control-label" style="margin-left:-10px;">Session Number</label></td><td>
                                                    <select name="ssn_schedule" class="form-control">
                                                        <!--- to disable the sessions for wchich call has been scheduled already-->
                                                        <option value="SSN1" <?php
                                                if (in_array('SSN1', $ssn_list)) {
                                                    echo "disabled=\"disabled\"";
                                                } else {
                                                    echo "";
                                                }
                                                ?>>1</option> 
                                                        <option  value="SSN2" <?php
                                                    if (in_array('SSN2', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>2</option> 
                                                        <option value="SSN3" <?php
                                                    if (in_array('SSN3', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>3</option> 
                                                        <option value="SSN4" <?php
                                                    if (in_array('SSN4', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>4</option> 
                                                        <option value="SSN5" <?php
                                                    if (in_array('SSN5', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>5</option> 
                                                        <option  value="SSN6" <?php
                                                    if (in_array('SSN6', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>6</option> 
                                                        <option  value="SSN7" <?php
                                                    if (in_array('SSN7', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>7</option> 
                                                        <option  value="SSN8" <?php
                                                    if (in_array('SSN8', $ssn_list)) {
                                                        echo "disabled=\"disabled\"";
                                                    } else {
                                                        echo "";
                                                    }
                                                ?>>8</option> 
                                                    </select></td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                </table><br/>
                            </div>
                            <div class="row">
                                <div class="input-group" style="width:100%;">

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-4 col-lg-4 col-md-4 col-xs-5">
                                                <?php
//change of display label as schedule or reschedule based scheduled_status
//if scheduled_status is 1 then display as re-schedule date else display as schedule date
                                                if ($scheduled_status == 1) {
                                                    echo '<label class="control-label ">Re-Schedule Date</label>';
                                                } else {
                                                    echo '<label class="control-label ">Schedule Date</label>';
                                                }
                                                ?>
                                            </div>
                                            <div class="col-sm-8 col-lg-8 col-md-8 col-xs-7">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="datepicker" placeholder="Select date here " name="date">
                                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                                </div><!-- input-group -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-4 col-lg-4 col-md-4 col-xs-5">
                                                <?php
//change of display label as schedule or reschedule
//if scheduled_status is 1 then display as re-schedule time else display as schedule time
                                                if ($scheduled_status == 1) {
                                                    echo '<label class="control-label ">Re-Schedule Time</label>';
                                                } else {
                                                    echo '<label class="control-label ">Schedule Time</label>';
                                                }
                                                ?>
                                            </div>
                                            <div class="col-sm-8 col-lg-8 col-md-8 col-xs-7">
                                                <div class="input-group m-b-15">
                                                    <div class="bootstrap-timepicker">
                                                        <input id="timepicker2" type="text" class="form-control" name="time" />
                                                    </div>
                                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                                </div><!-- input-group -->
                                            </div>

                                        </div>
                                    </div>
                                    <?php
//display message only when scheduling call for not yet completed ssn
                                    if ($page_id == "view") {
                                        echo '<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Calls can be scheduled for sessions whose telephonic calls have not been scheduled.</div>';
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                    <div style="text-align:center;margin-top:40px;">

                                        <?php
                                        echo '<input type="hidden" name="uid" value="' . $uid . '"/>';

#only if session is set in url pass the session else dont pass anything
                                        if (isset($_REQUEST['ssn'])) {
                                            echo '<input type="hidden" name="ssn" value="' . $ssn_prev . '"/>';
                                            #only if completed_on is set in url pass the session else dont pass anything
                                            if ($completed_on != "") {
                                                echo '<input type="hidden" name="completed_on" value="' . $completed_on . '"/>';
                                            }
                                        }
#only if pageid is set in url pass the session else dont pass anything
                                        if ($page_id == 'view') {
                                            echo '<input type="hidden" name="page_id" value="' . $page_id . '"/>';
                                        }
                                        echo '<input type="hidden" name="scheduled_status" value="' . $scheduled_status . '"/>';

//based on schedules_status,display btn as schedule call or re-schedule call
                                        if ($scheduled_status == 1) {
                                            echo '<input type="submit"  name="schedule-submit-btn" id="schedule-submit-btn" class="btn btn-success" value="Re-Schedule Call"  />';
                                        } else {
                                            echo '<input type="submit"  name="schedule-submit-btn" id="schedule-submit-btn" class="btn btn-success" value="Schedule Call" />';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- Plugins Js -->
        <script src="../assets/plugins/moment/moment.js"></script>
        <script src="../assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script> 
        <script>
            var date = new Date();
            date.setDate(date.getDate());

            $('#datepicker').datepicker({
                startDate: date
            });
        </script>

        <script>
            //script to able to set time from 8 am to 8 pm
            //if time is below or exceeds, set back to 8 am
            // Time Picker
            jQuery('#timepicker2').timepicker({
                showMeridian: true,
                minuteStep: 1,
                showInputs: true,
            }).on('changeTime.timepicker', function (e) {
                var h = e.time.hours;
                var m = e.time.minutes;
                var mer = e.time.meridian;
                //convert hours into minutes
                m += h * 60;
                //480 minutes = 8h*60m 
                // alert(m);
                if ((mer == 'AM' && m < 480) || (mer == 'PM' && m > 481)) {
                    if (mer == 'PM' && m >= 720 && m <= 779) {
                        //  jQuery('#timepicker2').timepicker('setTime', '12:00 AM');
                    } else {
                        jQuery('#timepicker2').timepicker('setTime', '08:00 AM');
                    }
                }
            });

        </script>
        <script>
            $("form").validate({
                rules: {
                    time: {
                        required: true,
                    },
                    date: {
                        required: true,
                    },
                },
                messages: {
                    time: {
                        required: "please enter a time",
                    },
                    date: {
                        required: "please enter a date",
                    },
                }
            });
        </script> 
        <script>
            $("#err_time").addClass("alert");
            $("#err_time").addClass("alert-danger");
        </script>
    </body>
</html>