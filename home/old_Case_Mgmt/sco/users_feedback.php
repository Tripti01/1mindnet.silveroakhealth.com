<?php
include 'soh-config.php';
include '../if_loggedin.php';
include 'host.php';

# Start the database 
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

$q1 = [];
$q2 = [];
$q3 = [];
$q4 = [];
$q5 = [];
$q6 = [];
$q7 = [];
$q8 = [];
$i = 0;
$count_very_satisfied = 0;
$count_satisfied = 0;
$count_neither_satisfied = 0;
$count_dissatisfied = 0;
$count_very_dissatisfied = 0;
#query to fetch all the responses of this therapist users
$stmt1 = $dbh->prepare("SELECT q1,q5,q6,q7 from survey_feedback,user_therapist WHERE user_therapist.uid = survey_feedback.uid and user_therapist.tid = ?");
$stmt1->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER_FDBK-1000].");
if ($stmt1->rowCount() != 0) {
    while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
        $q1[$i] = $row1['q1'];
        $q5[$i] = $row1['q5'];
        $q6[$i] = $row1['q6'];
        $q7[$i] = $row1['q7'];
        $i++;
    }
} else {
    die(" Please wait until one of the user completes the feedback ");
}

$stmt2 = $dbh->prepare("SELECT avg(q3) as q3,avg(q4) as q4,avg(8) as q8 from survey_feedback,user_therapist WHERE user_therapist.uid = survey_feedback.uid and user_therapist.tid = ?");
$stmt2->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER_FDBK-1000].");
if ($stmt2->rowCount() != 0) {
    $row2 = $stmt2->fetch();
    $q3_text = round($row2['q3']);
    $q3 = $row2['q3'] * 10;
    $q4_text = round($row2['q4']);
    $q4 = $row2['q4'] * 10;
    $q8_text = round($row2['q8']);
    $q8 = $row2['q8'] * 10;
}
#calculating the percentage of each responses given by the users of question no.2
$count_rel = 0;
$count_qual = 0;
$count_use = 0;
$count_uni = 0;
$count_good = 0;
$count_over = 0;
$count_in = 0;
$count_poor = 0;
$count_unr = 0;
$i = 0;
$stmt3 = $dbh->prepare("SELECT q2 from survey_feedback,user_therapist WHERE user_therapist.uid = survey_feedback.uid and user_therapist.tid = ?");
$stmt3->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER_FDBK-1000].");
if ($stmt3->rowCount() != 0) {
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {

        $q2 = $row3['q2'];
        $word = explode(",", $q2);

        # Count the no. of. users who said SCO is reliable
        for ($i = 0; $i < count($word); $i++) {
            if ($word[$i] == 'Reliable') {
                $count_rel = $count_rel + 1;
                # $count the no. of. users who said SCO is high quality
            } else if ($word[$i] == 'High Quality') {
                $count_qual = $count_qual + 1;
                # $count the no. of. users who said SCO is useful
            } else if ($word[$i] == 'Useful') {
                $count_use = $count_use + 1;
                # $count the no. of. users who said SCO is unique
            } else if ($word[$i] == 'Unique') {
                $count_uni = $count_uni + 1;
                # $count the no. of. users who said SCO is good vlaue for money
            } else if ($word[$i] == 'Good Value for money') {
                $count_good = $count_good + 1;
                # $count the no. of. users who said SCO is overpriced
            } else if ($word[$i] == 'Overpriced') {
                $count_over = $count_over + 1;
                # $count the no. of. users who said SCO is ineffective
            } else if ($word[$i] == 'Ineffective') {
                $count_in = $count_in + 1;
                # $count the no. of. users who said SCO is poor quality
            } else if ($word[$i] == 'Poor Quality') {
                $count_poor = $count_poor + 1;
                # $count the no. of. users who said SCO is Unreliable
            } else if ($word[$i] == 'Unreliable') {
                $count_unr = $count_unr + 1;
            }
        }
    }

    #calulate the $percenatges of each responses
    $total_responses = $count_rel + $count_qual + $count_use + $count_uni + $count_good + $count_over + $count_in + $count_poor + $count_unr;
    $perc_rel = ($count_rel * 100) / $total_responses;
    $perc_qual = ($count_qual * 100) / $total_responses;
    $perc_use = ($count_use * 100) / $total_responses;
    $perc_uni = ($count_uni * 100) / $total_responses;
    $perc_good = ($count_good * 100) / $total_responses;
    $perc_over = ($count_over * 100) / $total_responses;
    $perc_in = ($count_in * 100) / $total_responses;
    $perc_poor = ($count_poor * 100) / $total_responses;
    $perc_unr = ($count_unr * 100) / $total_responses;

    #putting the percentages roundo the error
    $q2_perc = [round($perc_rel), round($perc_qual), round($perc_use), round($perc_uni), round($perc_good), round($perc_over), round($perc_in), round($perc_poor), round($perc_unr)];
    $array_q2 = [$count_rel, $count_qual, $count_use, $count_uni, $count_good, $count_over, $count_in, $count_poor, $count_unr];
}
$total = count($q1);
# $count the no. of. users who said SCO is very Satisfied
for ($i = 0; $i < count($q1); $i++) {
    if ($q1[$i] == 0) {
        $count_very_satisfied = $count_very_satisfied + 1;
    }# $count the no. of. users who said SCO is Satisfied
    else if ($q1[$i] == 1) {
        $count_satisfied = $count_satisfied + 1;
        # $count the no. of. users who said SCO is neither Satisfied
    } else if ($q1[$i] == 2) {
        $count_nethier_satisfied = $count_nethier_satisfied + 1;
        # $count the no. of. users who said SCO is dissatisfied
    } else if ($q1[$i] == 3) {
        $count_dissatisfied = $count_dissatisfied + 1;
        # $count the no. of. users who said SCO is very dissatisfied
    } else if ($q1[$i] == 4) {
        $count_very_dissatisfied = $count_very_dissatisfied + 1;
    }
}
# Calculating $percentages of each responses of question no.1 
$perc_very_satisfied1 = round(($count_very_satisfied / $total) * 100);
$perc_satisfied1 = round(($count_satisfied * 100) / $total);
$perc_neither_satisfied1 = round(($count_neither_satisfied * 100) / $total);
$perc_dissatisfied1 = round(($count_dissatisfied * 100) / $total);
$perc_very_dissatisfied1 = round(($count_very_dissatisfied * 100) / $total);

#Putting the $percentages in to an array
$array_q1 = [$perc_very_satisfied1, $perc_satisfied1, $perc_neither_satisfied1, $perc_dissatisfied1, $perc_very_dissatisfied1];


$count_extremely_well = 0;
$count_very_well = 0;
$count_some_well = 0;
$count_not_well = 0;
$count_not_at_all_well = 0;

# $count the no. of. users who said SCO is extremely_well
for ($i = 0; $i < count($q1); $i++) {
    if ($q5[$i] == 0) {
        $count_extremely_well = $count_extremely_well + 1;
        # $count the no. of. users who said SCO is very well
    } else if ($q5[$i] == 1) {
        $count_very_well = $count_very_well + 1;
        # $count the no. of. users who said SCO is somewhat well
    } else if ($q5[$i] == 2) {
        $count_some_well = $count_some_well + 1;
        # $count the no. of. users who said SCO is not well
    } else if ($q5[$i] == 3) {
        $count_not_well = $count_not_well + 1;
        # $count the no. of. users who said SCO is not at all well
    } else if ($q5[$i] == 4) {
        $count_not_at_all_well = $count_not_at_all_well + 1;
    }
}
# Calculating $percentages of each responses of question no.5
$perc_extremely_well = ($count_extremely_well * 100) / $total;
$perc_very_well = ($count_very_well * 100) / $total;
$perc_some_well = ($count_some_well * 100) / $total;
$perc_not_well = ($count_not_well * 100) / $total;
$perc_not_at_all_well = ($count_not_at_all_well * 100) / $total;
#Putting it in to the aaray
$array_q5 = [round($perc_extremely_well), round($perc_very_well), round($perc_some_well), round($perc_not_well), round($perc_not_at_all_well)];


$count_very_satisfied = 0;
$count_satisfied = 0;
$count_neither_satisfied = 0;
$count_dissatisfied = 0;
$count_very_dissatisfied = 0;
$count_not_apply = 0;
# $count the no. of. users who said SCO is very satisfied	
for ($i = 0; $i < count($q1); $i++) {
    if ($q6[$i] == 0) {
        $count_very_satisfied = $count_very_satisfied + 1;
        # $count the no. of. users who said SCO is not satisfied
    } else if ($q6[$i] == 1) {
        $count_satisfied = $count_satisfied + 1;
        # $count the no. of. users who said SCO is neither satisfied
    } else if ($q6[$i] == 2) {
        $count_nethier_satisfied = $count_nethier_satisfied + 1;
        # $count the no. of. users who said SCO is dissatisfied
    } else if ($q6[$i] == 3) {
        $count_dissatisfied = $count_dissatisfied + 1;
        # $count the no. of. users who said SCO is very dissatisfied
    } else if ($q6[$i] == 4) {
        $count_very_dissatisfied = $count_very_dissatisfied + 1;
        # $count the no. of. users who said SCO is not apply
    } else if ($q6[$i] == 5) {
        $count_not_apply = $count_not_apply + 1;
    }
}
$perc_very_satisfied = ($count_very_satisfied * 100) / $total;
$perc_satisfied = ($count_satisfied * 100) / $total;
$perc_neither_satisfied = ($count_neither_satisfied * 100) / $total;
$perc_dissatisfied = ($count_dissatisfied * 100) / $total;
$perc_very_dissatisfied = ($count_very_dissatisfied * 100) / $total;
$perc_not_apply = ($count_not_apply * 100) / $total;
#putting in to the aaray
$array_q6 = [round($perc_very_satisfied), round($perc_satisfied), round($perc_neither_satisfied), round($perc_dissatisfied), round($perc_very_dissatisfied), round($perc_not_apply)];


#calculating the $percentage of each responses given by the users of question no.7

$count_extremely_resp = 0;
$count_very_resp = 0;
$count_some_resp = 0;
$count_not_so_resp = 0;
$count_not_at_all_resp = 0;
$count_not_apply = 0;
if ($q7[0] == 0) {
    $count_extremely_resp = $count_extremely_resp + 1;
} else if ($q7[0] == 1) {
    $count_very_resp = $count_very_resp + 1;
} else if ($q7[0] == 2) {
    $count_some_resp = $count_some_resp + 1;
} else if ($q7[0] == 3) {
    $count_not_so_resp = $count_not_so_resp + 1;
} else if ($q7[0] == 4) {
    $count_not_at_all_resp = $count_not_at_all_resp + 1;
} else if ($q7[0] == 5) {
    $count_not_apply = $count_not_apply + 1;
}

$perc_extremely_resp = ($count_extremely_resp * 100) / $total;
$perc_very_resp = ($count_very_resp * 100) / $total;
$perc_some_resp = ($count_some_resp * 100) / $total;
$perc_not_so_resp = ($count_not_so_resp * 100) / $total;
$perc_not_at_all_resp = ($count_not_at_all_resp * 100) / $total;
$perc_not_apply = ($count_not_apply * 100) / $total;
#putting roundo the aaray
$array_q7 = [round($perc_extremely_resp), round($perc_very_resp), round($perc_some_resp), round($perc_not_so_resp), round($perc_not_at_all_resp), round($perc_not_apply)];
?>
<!DOCTYPE html>
<html>

    <!-- Mirrored from coderthemes.com/adminto_1.3/light/page-starter.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Jul 2016 08){36){50 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="{% static 'images/favicon.ico">

        <!-- App title -->
        <title>User Satisfaction Report</title>

        <link rel="shortcut icon" href="https){//s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <!--Chartist Chart CSS -->
        <link rel="stylesheet" href="../assets/css/jQcloud.css">
        <link rel="stylesheet" href="../assets/css/users_feedback.css">
        <link rel="stylesheet" href="../assets/plugins/chartist/dist/chartist.min.css">
        <link href="../assets/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="https){//s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">	



        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING){ Respond.js doesn't work if you view the page via file){// -->
        <!--[if lt IE 9]>
        <script src="https){//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https){//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{% static 'js/modernizr.min.js' %}"></script>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper" >

            <!-- Top Bar Start -->
            <div class="topbar"  style="border-top:0;background-color:rgb(222, 225, 227); border-bottom:0px solid #223c80;" >
                <div class="row">
                    <div class = "col-lg-1"></div>
                    <div class = "col-lg-10">
                        <div class="row">
                            <div class = "col-lg-2 col-xs-4">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" height="60" width="150" style="margin-top:10px;margin-left:10px;margin-bottom:10px;"> 
                            </div>
                            <div class = "col-lg-8 col-xs-8">
                                <div class="title" style="border:0px solid blue;padding:5px 0px 10px 0px;color:#615d5d;">
                                    <center><b>User Satisfaction Report</b></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>

            <!-- Top Bar End -->

            <div class="content-page" style="margin-left:0px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class = "row">
                            <div class = "col-lg-1"></div>
                            <div class = "col-lg-10">
                                <div class="row" style="margin-top:30px; ">
                                    <div class="col-lg-6">
                                        <div class="panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Overall, how satisfied are you with "Stress Control Online"?</h3>
                                            </div>
                                            <div class="panel-body">
                                                <span class="nace">
                                                    <div class="barchart" id="chart_div_1" style="width: 100%; height: 350px;"></div>
                                                </span>
                                                <span>
                                                    <div class="row">	
                                                        <div class="col-md-7">
                                                            <table border="0">
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#1C5D99"></div></td>
                                                                    <td  style="padding-left:10px;" align="left"> Very satisfied </td>
                                                                    <td width="10%" align="center"> : </td>
                                                                    <td id="q1_perc1">  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#E06666"></div> </td>
                                                                    <td style="padding-left:10px;" align="left"> Satisfied </td>
                                                                    <td width="10%" align="center"> : </td>
                                                                    <td id="q1_perc2">  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#FFC332"></div> </td>
                                                                    <td  style="padding-left:10px;"align="left"> Neither satisfied nor dissatisfied </td>
                                                                    <td width="10%" align="center"> : </td>
                                                                    <td id="q1_perc3"></td>
                                                                </tr>															
                                                            </table>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <table id="q1">
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#6AA84F"></div> </td>
                                                                    <td  style="padding-left:5px;" align="left"> Dissatisfied </td>
                                                                    <td width="10%" align="center"> : </td>
                                                                    <td id="q1_perc4"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#3BB9B9;"></div> </td>
                                                                    <td  style="padding-left:5px;" align="left"> Very  dissatisfied </td>
                                                                    <td width="10%" align="center"> : </td>
                                                                    <td id="q1_perc5"> </td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                    </div>

                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 verticalLine">
                                        <div class="panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Which of the following words would you use to describe "Stress Control Online"?</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="bs-example">
                                                    <div id="demo-update" class="demo"></div>
                                                </div><br/>
                                                <hr style="color:grey;margin-bottom:20px !important;">
                                                <div class="row">	
                                                    <div class="col-md-6">
                                                        <table  width="85%">
                                                            <tr>
                                                                <td id="word1">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc1">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word2">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc2">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word3">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc3">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word4">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc4">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word5">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc5">  </td>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <table width="70%" id="q2">
                                                            <tr>
                                                                <td id="word6">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc6">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word7">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc7">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word8">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc8">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="word9">  </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id = "perc9">  </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="style18">
                                <div class="row">
                                    <div class="col-md-3"> 
                                        <div class="panel panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">How satisfied were you with your "Coach"?</h3>
                                            </div>
                                            <div class="panel-body" style="padding-top:0px;padding-bottom:5px;">
                                                <div id="donutchart" style="margin-top:-20px;height: 200px;background:transparent"></div>
                                                <div class="row" style="margin-top:-20px;margin-bottom:2px">	
                                                    <div class="col-md-12">
                                                        <table border="0">
                                                            <tr>
                                                                <td> <div class="legend-key" style="background-color:#1C5D99"></div></td>
                                                                <td  style="padding-left:10px;" align="left"> Very satisfied </td>
                                                                <td width="5%" align="center"> : </td>
                                                                <td id="pie_perc1">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td> <div class="legend-key" style="background-color:#E06666"></div> </td>
                                                                <td style="padding-left:10px;" align="left"> Satisfied </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id="pie_perc2">  </td>
                                                            </tr>
                                                            <tr>
                                                                <td> <div class="legend-key" style="background-color:#FFC332;margin-top:-15px;"></div> </td>
                                                                <td  style="padding-left:10px;"align="left"> Neither satisfied nor dissatisfied </td>
                                                                <td  align="center"><div  style="margin-top:-15px;"> : </div></td>
                                                                <td><div id="pie_perc3" style="margin-top:-15px;"></div></td>
                                                            </tr>
                                                            <tr>
                                                                <td> <div class="legend-key" style="background-color:#6AA84F"></div> </td>
                                                                <td  style="padding-left:10px;" align="left"> Dissatisfied </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id="pie_perc4"></td>
                                                            </tr>
                                                            <tr>
                                                                <td> <div class="legend-key" style="background-color:#3BB9B9;"></div> </td>
                                                                <td  style="padding-left:10px;" align="left"> Very  dissatisfied </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id="pie_perc5"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td> <div class="legend-key" style="background-color:#83221e;"></div> </td>
                                                                <td  style="padding-left:10px;" align="left"> Not applicable </td>
                                                                <td width="10%" align="center"> : </td>
                                                                <td id="pie_perc6"> </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="panel panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Please rate the ease of the using "Stress Control Online"</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row text-center">
                                                    <div class="col-sm-12 col-lg-12" >
                                                        <br/><br>
                                                        <div data-plugin="circliful" class="circliful-chart" data-dimension="180"
                                                             data-text="<?php echo $q8_text ?>" data-info="" data-width="25" data-fontsize="24"
                                                             data-percent="<?php echo $q8 ?>" data-fgcolor="#3BB9B9" data-bgcolor="#ebeff2"
                                                             data-type="half" data-fill="#f4f8fb">
                                                        </div>
                                                        <div style="text-align:center;color:#223c80"> 0 = Not at all easy to use <br/> 10 = Very easy to use</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="panel panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">How likely is it that you would recommend this to a friend or colleague?</h3>
                                            </div>
                                            <div class="panel-body">

                                                <div class="row text-center">
                                                    <div class="col-sm-12 col-lg-12" >
                                                        <br>
                                                        <div data-plugin="circliful" class="circliful-chart" data-dimension="180"
                                                             data-text="<?php echo $q3_text ?>" data-info="" data-width="25" data-fontsize="24"
                                                             data-percent="<?php echo $q3 ?>" data-fgcolor="#FFC332" data-bgcolor="#ebeff2"
                                                             data-type="half" data-fill="#f4f8fb">
                                                        </div>
                                                        <div style="text-align:center;color:#223c80"> 0 = Not at all likely <br/> 10 = Extremely likely</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="panel panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">How likely are you to apply the learnings from "Stress Control Online" in future?</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row text-center">
                                                    <div class="col-sm-12 col-lg-12" >
                                                        <br/>
                                                        <div data-plugin="circliful" class="circliful-chart" data-dimension="180"
                                                             data-text="<?php echo $q4_text ?>" data-info="" data-width="25" data-fontsize="24"
                                                             data-percent="<?php echo $q4 ?>" data-fgcolor="#674ea7" data-bgcolor="#ebeff2"
                                                             data-type="half" data-fill="#f4f8fb">
                                                        </div>
                                                        <div style="text-align:center;color:#223c80">0 = Not at all likely <br/> 10 = Extremely likely</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="style18">
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-md-6">
                                        <div class="panel-color panel-info">
                                            <div class="panel-heading" >
                                                <h3 class="panel-title">How responsive was the "Coach" in answering to your questions/concerns?</h3>
                                            </div>
                                            <div class="panel-body">
                                                <span class="nace">
                                                    <div class="barchart" id="chart_div_3" style="width: 100%; height: 350px;"></div>
                                                </span>
                                                <span>
                                                    <div class="row">	 
                                                        <div class="col-md-6">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#1C5D99;"></div></td>
                                                                    <td  style="padding-left:10px;" align="left"> Extremely responsive </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q2_perc1">   </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#E06666;"></div> </td>
                                                                    <td style="padding-left:10px;" align="left"> Very responsive</td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q2_perc2">   </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#FFC332; "></div> </td>
                                                                    <td  style="padding-left:10px;"align="left"> Somewhat responsive</td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q2_perc3">    </td>
                                                                </tr>															
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <table width="90%" id="q6">
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#6AA84F"></div> </td>
                                                                    <td  style="padding-left:10px;" align="left"> Not so responsive </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q2_perc4">    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#3BB9B9;"></div> </td>
                                                                    <td  style="padding-left:10px;" align="left"> Not at all responsive </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q2_perc5">    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#83221e;"></div> </td>
                                                                    <td  style="padding-left:10px;" align="left"> Not applicable </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q2_perc6">    </td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 verticalLine">
                                        <div class="panel-color panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">How well did "Stress Control Online" meet your needs?</h3>
                                            </div>
                                            <div class="panel-body" style="margin-top:-22px;">
                                                <br/>
                                                <span class="nace">
                                                    <div class="barchart" id="chart_div_2" style="width: 100%; height: 350px;"></div>
                                                </span>
                                                <span>
                                                    <div class="row">	 
                                                        <div class="col-md-5">
                                                            <table width="90%">
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#1C5D99;"></div></td>
                                                                    <td  style="padding-left:10px;" align="left"> Extremely well </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q3_perc1">  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#E06666;"></div> </td>
                                                                    <td style="padding-left:10px;" align="left"> Very well</td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q3_perc2">  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#FFC332;"></div> </td>
                                                                    <td  style="padding-left:10px;"align="left"> Somewhat well</td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q3_perc3">   </td>
                                                                </tr>															
                                                            </table>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <table width="80%" id="q7">
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#6AA84F"></div> </td>
                                                                    <td  style="padding-left:10px;" align="left"> Not so well </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q3_perc4">  </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> <div class="legend-key" style="background-color:#3BB9B9;"></div> </td>
                                                                    <td  style="padding-left:10px;" align="left"> Not at all well </td>
                                                                    <td width="10%" align="left"> : </td>
                                                                    <td id="q3_perc5">   </td>
                                                                </tr>															
                                                            </table>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 

                <footer class="footer" style="left:0; ">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6 col-xs-6"> 
                                    <script type="text/javascript">
                                        var dt = new Date();
                                        document.write(dt.getFullYear());
                                    </script> © Silver Oak Health.</div>
                                <div class="col-md-6 col-xs-6">
                                    <div style="float:right;">total number of reponses, n = <?php echo $total; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </div>

        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>

        <script src="../assets/plugins/jquery-circliful/js/jquery.circliful.min.js "></script>

        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jQcloud.js "></script>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">

                                        var dis1_values = <?php echo json_encode($array_q1) ?>;
                                        var dis2_values = <?php echo json_encode($array_q5) ?>;
                                        var hori_values = <?php echo json_encode($array_q7) ?>;
                                        var pie_values = <?php echo json_encode($array_q6) ?>;

                                        google.charts.load("current", {packages: ["corechart"]});
                                        google.charts.setOnLoadCallback(drawChart);
                                        function drawChart() {
                                            var data = google.visualization.arrayToDataTable([
                                                ['', ''],
                                                ['Very Satisfied', pie_values[0]],
                                                ['Satisfied', pie_values[1]],
                                                ['Neither satisfied nor dissatisfied', pie_values[2]],
                                                ['Dissatisfied', pie_values[3]],
                                                ['Very dissatisfied', pie_values[4]],
                                                ['Not Applicable', pie_values[5]]
                                            ]);

                                            var data1 = google.visualization.arrayToDataTable([
                                                ['', 'Very satisfied', 'Satisfied', 'Neither satisfied nor dissatisfied', 'Dissatisfied', 'Very dissatisfied'],
                                                ['', dis1_values[0], dis1_values[1], dis1_values[2], dis1_values[3], dis1_values[4]],
                                            ]);


                                            var data2 = google.visualization.arrayToDataTable([
                                                ['', 'Extremely well', 'Very well', 'Somewhat well', 'Not so well', 'Not at all well'],
                                                ['', dis2_values[0], dis2_values[1], dis2_values[2], dis2_values[3], dis2_values[4]],
                                            ]);

                                            var data3 = google.visualization.arrayToDataTable([
                                                ['', 'Extremely responsive', 'Very responsive', 'Somewhat responsive', 'Not so responsive', 'Not at all responsive', 'Not applicable'],
                                                ['', hori_values[0], hori_values[1], hori_values[2], hori_values[3], hori_values[4], hori_values[5]],
                                            ]);

                                            var options1 = {
                                                width: "50%",
                                                height: "50%",
                                                bar: {groupWidth: "30%"},
                                                colors: ['#1C5D99', '#E06666', '#FFC332', '#6AA84F', '#3BB9B9', '#83221e', '#674ea7'],
                                                legend: {position: 'none', maxLines: 3, textStyle: {color: 'black', fontSize: 16}},
                                                isStacked: true,
                                            };

                                            var options2 = {
                                                pieHole: 0.4,
                                                legend: {position: 'none'},
                                                colors: ['#1C5D99', '#E06666', '#FFC332', '#6AA84F', '#3BB9B9', '#83221e', '#674ea7'],
                                            };
                                            var view1 = new google.visualization.DataView(data1);
                                            var view2 = new google.visualization.DataView(data2);
                                            var view3 = new google.visualization.DataView(data3);

                                            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                                            chart.draw(data, options2);

                                            var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_1"));
                                            chart.draw(view1, options1);

                                            var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_2"));
                                            chart.draw(view2, options1);

                                            var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_3"));
                                            chart.draw(view3, options1);
                                        }
        </script>
        <script>
            var jcloud_values = <?php echo json_encode($array_q2); ?>;
            var q2_words = ["Reliable", "High quality", "Useful", "Unique", "Good value for money", "Overpriced", "Ineffective", "Poor quality", "Unreliable"]
            var jcloud_perc = <?php echo json_encode($q2_perc); ?>;

            var basic_words = [
                {text: "Reliable", weight: jcloud_values[0]},
                {text: "High quality", weight: jcloud_values[1]},
                {text: "Useful", weight: jcloud_values[2]},
                {text: "Unique", weight: jcloud_values[3]},
                {text: "Good value for money", weight: jcloud_values[4]},
                {text: "Overpriced", weight: jcloud_values[5]},
                {text: "Ineffective", weight: jcloud_values[6]},
                {text: "Poor quality", weight: jcloud_values[7]},
                {text: "Unreliable", weight: jcloud_values[8]}
            ];

            $('#demo-autosize').jQCloud(basic_words, {
                autoResize: true
            });

            $('#demo-colors-size').jQCloud(basic_words, {
                classPattern: null,
                colors: ["#800026", "#bd0026", "#e31a1c", "#fc4e2a", "#fd8d3c", "#feb24c", "#fed976", "#ffeda0", "#ffffcc"],
                fontSize: {
                    from: 0.05,
                    to: 0.02
                }
            });
            $('#demo-update').jQCloud(basic_words, {
                delay: 50
            });
        </script>
        <script>
            // Sorting of q2 responses in desecnding order
            for (a = 0; a < jcloud_perc.length; a++) {
                for (b = a + 1; b < jcloud_perc.length; b++) {
                    if (jcloud_perc[a] < jcloud_perc[b]) {
                        temp1 = jcloud_perc[a];
                        jcloud_perc[a] = jcloud_perc[b];
                        jcloud_perc[b] = temp1;

                        temp2 = q2_words[a];
                        q2_words[a] = q2_words[b];
                        q2_words[b] = temp2;
                    }
                }
            }
        </script>
        <script>

            var dis1_values = <?php echo json_encode($array_q1) ?>;
            var dis2_values = <?php echo json_encode($array_q5) ?>;
            var hori_values = <?php echo json_encode($array_q7) ?>;
            var pie_values = <?php echo json_encode($array_q6) ?>;

            for (var i = 1; i <= dis1_values.length; i++) {
                $('#q1_perc' + i).html(dis1_values[i - 1] + '%');
            }
            for (var i = 1; i <= dis2_values.length; i++) {
                $('#q3_perc' + i).html(dis2_values[i - 1] + '%');
            }
            for (var i = 1; i <= hori_values.length; i++) {
                $('#q2_perc' + i).html(hori_values[i - 1] + '%');
            }
            for (var i = 1; i <= pie_values.length; i++) {
                $('#pie_perc' + i).html(pie_values[i - 1] + '%');
            }
            for (var j = 1; j <= q2_words.length; j++) {
                $('#word' + j).html(q2_words[j - 1]);
                $('#perc' + j).html(jcloud_perc[j - 1] + '%');
            }

        </script>
    </body>
</html>