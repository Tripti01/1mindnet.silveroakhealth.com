<?php
#including functions and files
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/get_user_status.php';
include 'functions/coach/get_session_name.php';

# Check if UID is there in URL
#if uid is present,set uid else display error message
if (isset($_REQUEST['uid'])) {
    $uid = $_REQUEST['uid'];
} else {
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code :THRP_USAC00");
}

#session list array
$ssn_list_name = array("SSN0", "SSN1", "SSN2", "SSN3", "SSN4", "SSN5", "SSN6", "SSN7", "SSN8");

$dbh = new PDO($dsn, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Getting user session comletion status 
#user status is fetch from user_progress
#limit 9 is used to get status from SSN0 to SSN8
$i = 0; // Counter for status list
$stmt10 = $dbh->prepare("SELECT status FROM user_progress WHERE uid=? LIMIT 9");
$stmt10->execute(array($uid));
if ($stmt10->rowCount() != 0) {
# Retrive status for the 8 sessions for this user
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $status_list[$i] = $row10['status'];
        $i++;
    }
} else {
#Show an error
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code :THRP_USAC01");
}

#to get start_time and end_time of each session
$j = 0; // Counter for starttime,endtime
$stmt20 = $dbh->prepare("SELECT ssn_starttime,ssn_endtime FROM user_time_ssn WHERE uid=?  LIMIT 9");
$stmt20->execute(array($uid));
if ($stmt20->rowCount() != 0) {
# Retrive starttime,endtime for the 8 sessions for this user
    while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC)) {
        $ssn_starttime_list[$j] = $row20['ssn_starttime']; //to fecth start time
        $ssn_endtime_list[$j] = $row20['ssn_endtime']; //to fetch end time
        $j++;
    }
} else {
#Show an error
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code :THRP_USAC02");
}

#to fecth details of deu_date of each session and also the scheduled_Calls date and time
#to fetch due_Date for each session, we pass the session one by one from session list and store details in schedule_date array
#to fetch details of calls scheduled for each session , we pass the session we pass the session one by one from session list and fetch schedule_call_Date array
for ($l = 0; $l <= 8; $l++) { // Counter for session list
# Getting user schedule
    $stmt10 = $dbh->prepare("SELECT due_datetime FROM user_schedule WHERE uid =? AND ssn=? LIMIT 1");
    $stmt10->execute(array($uid, $ssn_list_name[$l]));
    if ($stmt10->rowCount() != 0) {
        #to retrive the due dates fo sessions
        $row10 = $stmt10->fetch();
        $schedule_date[$l] = $row10['due_datetime'];
    } else {
#Show an error
        die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code :THRP_USAC06");
    }

    #to find the calls scheduled for each sesssions
    #since thrp_scheduled_Calls doesnt contain ssn0, we replace ssn by CBTSTART, to fetch details.
    if ($ssn_list_name[$l] == "SSN0") {
        $ssn_list_name[$l] = "CBTSTART";
    } else {
        //$ssn_list_name[$l] remains same
    }
    $stmt11 = $dbh->prepare("SELECT scheduled_date FROM thrp_scheduled_calls WHERE uid =? AND ssn=? LIMIT 1");
    $stmt11->execute(array($uid, $ssn_list_name[$l]));
    if ($stmt11->rowCount() != 0) {
        $row11 = $stmt11->fetch();
        $schedule_call_date[$l] = $row11['scheduled_date']; //to fecth scheduled call date
    } else {
        $schedule_call_date[$l] = ' ';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <title>Time-Line</title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
        <link href="../../assets2/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="../../assets2/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/social-share-kit-master/dist/css/social-share-kit.css" type="text/css" rel="stylesheet" >		
        <link href="../../assets2/admin/layout3/css/asmt_popup.css" rel="stylesheet">
        <link href="../../assets2/admin/layout3/css/number-pb.css" type="text/css" rel="stylesheet" >	
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            th{
                color:#8896a0;
                font-size: 14px;
                font-weight: 600px;
                padding: 8px;
                line-height: 1.42857143;
            }
        </style>
    </head>
    <div class="col-lg-8" style="padding-left: 0%;padding-right: 0%;">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" style="align: center;margin-top: 0px;">
                    <thead>
                        <tr>
                            <th>SESSIONS</th>
                            <th>SCHEDULED ON</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th style="text-align: center;">SESSION STATUS</th>
                            <th>CALL SCHEDULED ON</th>
                        </tr>
                    </thead>
                    <tbody >
                        <?php
                        for ($k = 0; $k <= 8; $k++) {//counter for 8 sessions
                            ?>
                            <tr>
                                <td>
                                    <b><?php
                                        //if session is cbtstart display as introduction, else get session name
                                        if ($ssn_list_name[$k] == "CBTSTART") {
                                            //set session name as introduction
                                            $session_full_name = "Introduction";
                                        } else {
                                            //get session name from function
                                            $session_full_name = get_ssn_fullname($ssn_list_name[$k]);
                                        }
                                        echo $session_full_name;
                                        ?></b>
                                </td>
                                <td>
                                    <?php
                                    //if schdule date is null, set as null else display date
                                    if ($schedule_date[$k] == null) {
                                        //display nothing
                                        echo ' ';
                                    } else {
                                        //display date
                                        echo date("d-m-Y", strtotime($schedule_date[$k]));
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    //if ssn_starttime_list is set as 0000-00-00 00:00:00,display nothing,else display date
                                    if ($ssn_starttime_list[$k] == "0000-00-00 00:00:00") {
                                        //display nothing
                                        echo ' ';
                                    } else {
                                        //display date
                                        echo date("d-m-Y", strtotime($ssn_starttime_list[$k]));
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($ssn_endtime_list[$k] == "0000-00-00 00:00:00") {
                                        echo ' ';
                                    } else {
                                        echo date("d-m-Y", strtotime($ssn_endtime_list[$k]));
                                    }
                                    ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                    if ($status_list[$k] == 0) {//if session not yet started
                                        //display nothing
                                        echo ' ';
                                    }
                                    if ($status_list[$k] == 1) {//if session is completed
                                        //display in blue color check icon
                                        echo '<font color=#223C80 size=2px><i class="fa fa-check"></i></font>';
                                    }
                                    if ($status_list[$k] == 9) {//if session is in progress
                                        //display in grey spinner icon
                                        echo '<font color=#9e9e9e><i class="fa fa-spinner"></i></font>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($schedule_call_date[$k] == null) {
                                        echo ' ';
                                    } else {
                                        echo date("d-m-Y H:i A", strtotime($schedule_call_date[$k]));
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/detect.js"></script>
    <script src="../assets/js/fastclick.js"></script>
    <script src="../assets/js/jquery.slimscroll.js"></script>
    <script src="../assets/js/jquery.blockUI.js"></script>
    <script src="../assets/js/waves.js"></script>
    <script src="../assets/js/jquery.nicescroll.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../assets/js/jquery.core.js"></script>
    <script src="../assets/js/jquery.app.js"></script>
</body>
</html>