<?php
include 'soh-config.php';
# scale 4-40 in range of 0 to 100
$ranges_from = 4;
$ranges_to = 40;
 
$uid = $_GET['uid'];

$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//to fetch the deatils for observations from take notes
$stmt01 = $dbh->prepare("SELECT ssn,q5,q6,q7,q8 FROM thrp_notes WHERE  uid=? ORDER BY ssn LIMIT 8 ");
$stmt01->execute(array($uid));
if ($stmt01->rowCount() != 0) {
    while ($row = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $ssn_list=$row['ssn'];
        $sno[] = substr($row['ssn'], -1, 1);
        $q5[] = $row['q5'];
        $q6[] = $row['q6'];
        $q7[] = $row['q7'];
        $q8[] = $row['q8'];
    }
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Stress Control Online</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

    </head>
    <body>
        <?php 
        //if session list is set then display graph else display message
        if(isset($ssn_list)){ ?>
        <div class="row">
            <div class="col-md-5"  >
                <div style="z-index:2;position: absolute;margin-top:11%;margin-left:20px;">High</div>
                <div id="chart_div1" style="z-index:1;position: absolute;">                        
                </div>
                <div style="z-index:2;position: absolute;margin-top:45%;margin-left:20px;">Low</div>
            </div>  
            <div class="col-md-7" >
                <div style="z-index:2;position: absolute;margin-top:8%;margin-left:20px;">High</div>
                <div id="chart_div2" style="z-index:1;position: absolute;">                        
                </div>
                <div style="z-index:2;position: absolute;margin-top:32%;margin-left:20px;">Low</div>
            </div> 
        </div> 
        <div class="row">
            <div class="col-md-5">

                <div style="z-index:2;position: absolute;margin-top:350px;;margin-left:20px;">High</div>
                <div id="chart_div3" style="margin-top:300px;z-index:1;position: absolute;" >                       
                </div>
                <div style="z-index:2;position: absolute;margin-top:495px;margin-left:20px;">Low</div>
            </div>  
            <div class="col-md-7" >

                <div style="z-index:2;position: absolute;margin-top:350px;;margin-left:20px;">High</div>
                <div id="chart_div4" style="margin-top:300px;z-index:1;position: absolute;" >                       
                </div>

                <div style="z-index:2;position: absolute;margin-top:495px;margin-left:20px;">Low</div>
            </div> 
        </div>
        <?php }else { 
            echo 'No data to display.';
        }?>
        <!--[if lt IE 9]>
    <script src="../assets2/global/plugins/respond.min.js"></script>
    <script src="../assets2/global/plugins/excanvas.min.js"></script> 
    <![endif]-->
        <script src="../../assets2/global/plugins/jquery.min.js" type="text/javascript"></script>        
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/back-to-top.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="../../assets2/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="../../assets2/admin/pages/scripts/tasks.js" type="text/javascript"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/back-to-top.js" type="text/javascript"></script>


        <script src="https://www.google.com/jsapi" type="text/javascript"></script>


        <script type="text/javascript">

            // Load the Visualization API and the piechart package.
            google.load('visualization', '1.0', {'packages': ['corechart']});

            // Set a callback to run when the Google Visualization API is loaded.
            google.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table,
            // instantiates the pie chart, passes in the data and
            // draws it.
            function drawChart() {
                var data1 = google.visualization.arrayToDataTable([
                    ['x', 'Score'],
<?php
for ($i = 0; $i < count($sno) - 1; $i++) {
    echo "[" . intval($sno[$i]) . "," . intval($q5[$i]) . "],";
}
echo
"[" . intval($sno[$i]) . "," . intval($q5[$i]) . "]";
?>
                ]);

                // Create the data table.
                var data2 = google.visualization.arrayToDataTable([
                    ['x', 'Score'],
<?php
for ($i = 0; $i < count($sno) - 1; $i++) {
    echo "[" . intval($sno[$i]) . "," . intval($q6[$i]) . "],";
}
echo
"[" . intval($sno[$i]) . "," . intval($q6[$i]) . "]";
?>
                ]);


                var data3 = google.visualization.arrayToDataTable([
                    ['x', 'Score'],
<?php
for ($i = 0; $i < count($sno) - 1; $i++) {
    echo "[" . intval($sno[$i]) . "," . intval($q7[$i]) . "],";
}
echo
"[" . intval($sno[$i]) . "," . intval($q7[$i]) . "]";
?>
                ]);

                var data4 = google.visualization.arrayToDataTable([
                    ['x', 'Score'],
<?php
for ($i = 0; $i < count($sno) - 1; $i++) {
    echo "[" . intval($sno[$i]) . "," . intval($q8[$i]) . "],";
}
echo
"[" . intval($sno[$i]) . "," . intval($q8[$i]) . "]";
?>
                ]);


                var options1 = {
                    title: 'Client motivation level',
                    fontName: 'Open Sans',
                    titlePosition: 'right',
                    legend: 'none',
                    lineWidth: 0,
                    height: 300,
                    width: 400,
                    series: {
                        0: {color: '#3AB149'}
                    },
                    hAxis: {
                        title: 'Sessions',
                        ticks: [1, 2, 3, 4, 5, 6, 7, 8],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 8,
                            min: 0
                        }
                    },
                    vAxis: {
                        title: 'User Score',
                        ticks: [1, 2, 3, 4, 5],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 5,
                            min: 0
                        }
                    }
                };
                var options2 = {
                   
                    title: 'Understanding of session material',
                    fontName: 'Open Sans',
                    titlePosition: 'right',
                    legend: 'none',
                    lineWidth: 0,
                    height: 300,
                    width: 400,
                    series: {
                        0: {color: '#3AB149'}
                    },
                    hAxis: {
                        title: 'Sessions',
                        ticks: [1, 2, 3, 4, 5, 6, 7, 8],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 8,
                            min: 0
                        }
                    },
                    vAxis: {
                        title: 'User Score',
                        ticks: [1, 2, 3, 4, 5],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 5,
                            min: 0
                        }
                    }
                };
                var options3 = {
                    title: 'Completion of session tasks',
                    fontName: 'Open Sans',
                    titlePosition: 'right',
                    legend: 'none',
                    lineWidth: 0,
                    height: 300,
                    width: 400,
                    series: {
                        0: {color: '#3AB149'}
                    },
                    hAxis: {
                        title: 'Sessions',
                        ticks: [1, 2, 3, 4, 5, 6, 7, 8],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 8,
                            min: 0
                        }
                    },
                    vAxis: {
                        title: 'User Score',
                        ticks: [1, 2, 3, 4, 5],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 5,
                            min: 0
                        }
                    }
                };
                var options4 = {
                    title: 'Satisfaction with sessions',
                    fontName: 'Open Sans',
                    titlePosition: 'right',
                    legend: 'none',
                    lineWidth: 0,
                    height: 300,
                    width: 400,
                    series: {
                        0: {color: '#3AB149'}
                    },
                    hAxis: {
                        title: 'Session',
                        ticks: [1, 2, 3, 4, 5, 6, 7, 8],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 8,
                            min: 0
                        }
                    },
                    vAxis: {
                        title: 'User Score',
                        ticks: [1, 2, 3, 4, 5],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 5,
                            min: 0
                        }
                    }
                };
                // Instantiate and draw our chart, passing in some options.
                var chart1 = new google.visualization.ScatterChart(document.getElementById('chart_div1'));
                chart1.draw(data1, options1);
                var chart2 = new google.visualization.ScatterChart(document.getElementById('chart_div2'));
                chart2.draw(data2, options2);
                var chart3 = new google.visualization.ScatterChart(document.getElementById('chart_div3'));
                chart3.draw(data3, options3);
                var chart4 = new google.visualization.ScatterChart(document.getElementById('chart_div4'));
                chart4.draw(data4, options4);

            }
        </script>

    </body>
</html>
