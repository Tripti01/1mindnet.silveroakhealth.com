<?php
#including files
include 'soh-config.php';

$uid = $_GET['uid'];

$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#Checkinng if Gad Assessment is taken ,if yes score is displayed
$stmt01 = $dbh->prepare("SELECT score_gad, score_phq FROM asmt_score WHERE uid=? and type='PREA' LIMIT 1");
$stmt01->execute(array($uid));
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    $score_gad_pre = $row01['score_gad'];
    $score_phq_pre = $row01['score_phq'];
} else {
    $score_gad_pre = 0;
    $score_phq_pre = 0;
}
#Checkinng if Gad Assessment is taken ,if yes score is displayed.
$stmt02 = $dbh->prepare("SELECT score_gad, score_phq FROM asmt_score WHERE uid=? and type='POST' LIMIT 1");
$stmt02->execute(array($uid));
if ($stmt02->rowCount() != 0) {
    $row02 = $stmt02->fetch();

    $score_gad_post = $row02['score_gad'];
    $score_phq_post = $row02['score_phq'];
} else {
    $score_gad_post = 0;
    $score_phq_post = 0;
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Stress Control Online</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
        <link href="../../assets2/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="../../assets2/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/social-share-kit-master/dist/css/social-share-kit.css" type="text/css" rel="stylesheet" >		
        <link href="../../assets2/admin/layout3/css/asmt_popup.css" rel="stylesheet">
        <link href="../../assets2/admin/layout3/css/number-pb.css" type="text/css" rel="stylesheet" >	
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
    </head>
    <body>
        <div class="row" >
            <div class="col-xs-5" style="margin-left:30px">
                <article>
                    <h4 class="title"><b>Anxiety Scale</b></h4>
                    <section id="basic-pre-a" > 
                        <div class="number-pb" style=" margin-top:20px;">
                            <div align="left" style="padding-top: 10px;"> Pre Stress Control Online</div>
                            <div class="number-pb-shown" id = "pre-a"></div>
                            <div class="number-pb-num">0</div>
                        </div>
                    </section>                                            
                    <section id="basic-post-a" > 
                        <div class="number-pb">
                            <div align="left" style="padding-top: 10px; margin-top:20px;"> Post Stress Control Online</div>
                            <div class="number-pb-shown" id = "post-a"></div>
                            <div class="number-pb-num">0</div>
                        </div>
                    </section>
                </article>

                <article>
                    <section id="basic-pre-d" >
                        <h4 class="title"><b>Depression Scale</b></h4>
                        <div class="number-pb" style=" margin-top:20px;"">
                            <div align="left" style="padding-top: 10px; "> Pre Stress Control Online</div>
                            <div class="number-pb-shown" id="pre-d"></div>
                            <div class="number-pb-num">0</div>
                        </div>												
                    </section>
                    <section id="basic-post-d" >                                            
                        <div class="number-pb">
                            <div align="left" style="padding-top: 10px;"> Post Stress Control Online</div>
                            <div class="number-pb-shown" id="post-d"></div>
                            <div class="number-pb-num" >0</div>
                        </div>
                    </section>
                </article>
            </div> 
            <div class="col-xs-6" style="margin-left: 35px;">
                <ul class="scale-ranges horizontal-list" style="margin-top:25px;">
                    <li id="min">
                        <p class="min scale scale-font-color">Minimum Anxiety</p>
                        <p class="scale-range">0-5</p>
                    </li>
                    <li id="milda">
                        <p class="milda scale scale-font-color">Mild <br/>Anxiety</p>
                        <p class="scale-range">6-10</p>
                    </li>
                    <li id="mod">
                        <p class="mod scale scale-font-color">Moderate Anxiety</p>
                        <p class="scale-range">11-15</p>
                    </li>
                    <li id="sev">
                        <p class="sev scale scale-font-color">Severe <BR/> Anxiety</p>
                        <p class="scale-range">16-21</p>
                    </li>
                </ul>
                <div class="scale" style="margin-top:165px;">
                    <ul class="scale-ranges-d horizontal-list" >
                        <li id="none">
                            <p class="none-min scale scale-font-color">None / <br/>Minimal</p>
                            <p class="scale-range">0 – 4 </p>
                        </li>
                        <li id="mild">
                            <p class="mild scale scale-font-color">Mild </p>
                            <p class="scale-range">5 - 9</p>
                        </li>
                        <li id="moderate">
                            <p class="moderate scale scale-font-color">Moderate </p>
                            <p class="scale-range">10 - 14</p>
                        </li>
                        <li id="mod-sev">
                            <p class="mod-sev scale scale-font-color" style=" padding-left:2px; padding-right:2px;">Moderately Severe</p>
                            <p class="scale-range">15 - 19</p>
                        </li>
                        <li id="severe">
                            <p class="severe scale scale-font-color">Severe</p>
                            <p class="scale-range">20-27</p>
                        </li>
                    </ul>
                </div>
            </div>

        </div>


        <!--[if lt IE 9]>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/respond.min.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/excanvas.min.js"></script> 
        <![endif]-->
        <script src="../../assets2/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="../../assets2/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="../../assets2/admin/layout3/scripts/jquery.velocity.min.js"></script>
        <script src="../../assets2/admin/layout3/scripts/number-pb.js"></script>		
        <script type="text/javascript">

            /* pre assessment anixety report */
            var $basic1 = $('#basic-pre-a');
            var basicBar1 = $basic1.find('.number-pb').NumberProgressBar({
                style: 'basic',
                min: 0,
                max: 21
            })

            var basicLoop1 = function () {
                basicBar1.reach(<?php echo $score_gad_pre; ?>);
            }
            basicLoop1();

            /* post assessment anixety report */
            var $basic2 = $('#basic-post-a');
            var basicBar = $basic2.find('.number-pb').NumberProgressBar({
                style: 'basic',
                min: 0,
                max: 21
            })

            var basicLoop2 = function () {
                basicBar.reach(<?php echo $score_gad_post; ?>);
            }
            basicLoop2();

            /* pre assessment depression report */
            var $basic3 = $('#basic-pre-d');
            var basicBar3 = $basic3.find('.number-pb').NumberProgressBar({
                style: 'basic',
                min: 0,
                max: 27
            })

            var basicLoop3 = function () {
                basicBar3.reach(<?php echo $score_phq_pre; ?>);
            }
            basicLoop3();

            /* post assessment depression report */
            var $basic4 = $('#basic-post-d');
            var basicBar4 = $basic4.find('.number-pb').NumberProgressBar({
                style: 'basic',
                min: 0,
                max: 27
            })

            var basicLoop4 = function () {
                basicBar4.reach(<?php echo $score_phq_post; ?>);
            }
            basicLoop4();
        </script>
        <script type="text/javascript">
            var score_gad_pre = '<?php echo $score_gad_pre; ?>';
            var score_phq_pre = '<?php echo $score_phq_pre; ?>';
            var score_gad_post = '<?php echo $score_gad_post; ?>';
            var score_phq_post = '<?php echo $score_phq_post; ?>';

            /* pre gad and phq color bar */
            if (score_gad_pre >= 0 && score_gad_pre <= 5) {
                $('#pre-a').css('background-color', '#2ecc71');
            } else if (score_gad_pre >= 6 && score_gad_pre <= 10) {
                $('#pre-a').css('background-color', '#ffeb3b');
            } else if (score_gad_pre >= 11 && score_gad_pre <= 15) {
                $('#pre-a').css('background-color', '#ff9800');
            } else {
                $('#pre-a').css('background-color', 'red');
            }

            if (score_phq_pre >= 0 && score_phq_pre <= 4) {
                $('#pre-d').css('background-color', '#2ecc71');
            } else if (score_phq_pre >= 5 && score_phq_pre <= 9) {
                $('#pre-d').css('background-color', '#ffeb3b');
            } else if (score_phq_pre >= 10 && score_phq_pre <= 14) {
                $('#pre-d').css('background-color', '#ff9800');
            } else if (score_phq_pre >= 15 && score_phq_pre <= 19) {
                $('#pre-d').css('background-color', 'red');
            } else {
                $('#pre-d').css('background-color', '#7e423d');
            }

            /* post gad and phq color bar */
            if (score_gad_post >= 0 && score_gad_post <= 5) {
                $('#post-a').css('background-color', '#2ecc71');
            } else if (score_gad_post >= 6 && score_gad_post <= 10) {
                $('#post-a').css('background-color', '#ffeb3b');
            } else if (score_gad_post >= 11 && score_gad_post <= 15) {
                $('#post-a').css('background-color', '#ff9800');
            } else {
                $('#post-a').css('background-color', 'red');
            }

            if (score_phq_post >= 0 && score_phq_post <= 4) {
                $('#post-d').css('background-color', '#2ecc71');
            } else if (score_phq_post >= 5 && score_phq_post <= 9) {
                $('#post-d').css('background-color', '#ffeb3b');
            } else if (score_phq_post >= 10 && score_phq_post <= 14) {
                $('#post-d').css('background-color', '#ff9800');
            } else if (score_phq_post >= 15 && score_phq_post <= 19) {
                $('#post-d').css('background-color', 'red');
            } else {
                $('#post-d').css('background-color', '#7e423d');
            }
        </script>
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layou
            });
        </script>        
    </body>
</html>