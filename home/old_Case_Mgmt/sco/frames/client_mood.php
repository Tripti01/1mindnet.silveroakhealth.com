<?php
include 'soh-config.php';
# scale 4-40 in range of 0 to 100
$ranges_from = 4;
$ranges_to = 40;
        
$uid = $_GET['uid'];

$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//to fetch the deatils for mood survey graph
$stmt01 = $dbh->prepare("SELECT ssn,q1,q2,q3,q4,total FROM user_mood_survey WHERE  uid=? and done=1;");
$stmt01->execute(array($uid));

if ($stmt01->rowCount() != 0) {
    while ($row = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $ssn_list=$row['ssn'];
        $sno[] = substr($row['ssn'], -1, 1);
        $q1[] = $row['q1'];
        $q2[] = $row['q2'];
        $q3[] = $row['q3'];
        $q4[] = $row['q4'];
        $user_mood_total_score = $row['total'];
        $total_score = round(($user_mood_total_score - $ranges_from) * ((100 - 0) / ($ranges_to - $ranges_from)));
        $total[] = $total_score;
    }
}else{
    //do show graph
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>View Clients</title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
        <link href="../../assets2/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="../../assets2/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
        <link href="../../assets2/global/social-share-kit-master/dist/css/social-share-kit.css" type="text/css" rel="stylesheet" >	
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

    </head>
    <body>
        <div class="col-md-9" >
            <?php 
            //if session is set display graph else display message
            if(isset($ssn_list)){ ?>
            <div class="grid">
                <div class="col-9"   >
                    <div style="z-index:2;position: absolute;width:10px;margin-top:58px;margin-left:57px;background: linear-gradient(to top, #F82E12 , #2FE123);height: 185px;"></div>
                    <div id="chart_div1" style="z-index:1;position: absolute;" >                        
                    </div>
                    <!--div style="z-index:2;position: absolute;margin-top:233px;margin-left: 45px;">Low</div-->
                </div>
            </div>
            <?php }else { 
            echo 'No data to display.';
        }?>
        </div>  
        <!--[if lt IE 9]>
    <script src="../assets2/global/plugins/respond.min.js"></script>
    <script src="../assets2/global/plugins/excanvas.min.js"></script> 
    <![endif]-->
        <script src="../../assets2/global/plugins/jquery.min.js" type="text/javascript"></script>        
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/back-to-top.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../../assets2/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="../../assets2/admin/layout3/scripts/layout.js" type="text/javascript"></script>
        <script src="../../assets2/admin/pages/scripts/tasks.js" type="text/javascript"></script>
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/back-to-top.js" type="text/javascript"></script>
        <script src="https://www.google.com/jsapi" type="text/javascript"></script>
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layou
            });
        </script>
        <script>
            google.load('visualization', '1', {packages: ['corechart', 'line']});
            google.setOnLoadCallback(drawChart1);
            function drawChart1() {
                var data = google.visualization.arrayToDataTable([
                    ['x', 'Score'],
            <?php
            for ($i = 0; $i < count($sno) - 1; $i++) {
                echo "[" . intval($sno[$i]) . "," . intval($total[$i]) . "],";
            }
            echo "[" . intval($sno[$i]) . "," . intval($total[$i]) . "]";
            ?>
                ]);
                var options = {
                    // title: 'Client Mood Survey Report',
                    fontName: 'Open Sans',
                    titlePosition: 'right',
                    legend: 'none',
                    curveType: 'function',
                    lineWidth: 2,
                    height: 300,
                    width: 790,
                    series: {
                        0: {color: '#3AB149'}
                    },
                    hAxis: {
                        title: 'Sessions',
                        ticks: [1, 2, 3, 4, 5, 6, 7, 8],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 8,
                            min: 0
                        }
                    },
                    vAxis: {
                        title: 'Client Mood Scale',
                        ticks: [0, 10, 20, 30, 30, 40, 50, 60, 70, 80, 90, 100],
                        gridlines: {color: 'transparent'},
                        viewWindow: {
                            max: 103,
                            min: 0
                        }
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));

                chart.draw(data, options);
            }

            $(window).resize(function () {
                drawChart1();
            });
        </script>
    </body>
</html>
