<?php
#including functions and files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/get_user_status.php';
include 'functions/coach/get_session_name.php';

#to check if uid,snn and pageid from url passed into iframe, and perform accordingly else set all as blank
if (isset($_REQUEST['uid'])) {
    $uid = $_REQUEST['uid'];
} else {
    $uid = "";
}
if (isset($_REQUEST['ssn'])) {
    $ssn = $_REQUEST['ssn'];
} else {
    $ssn = "";
}
//to check if status are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = "";
}

# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#to fetch notes for particaular ssn, display notes to enable editing else leave blank
$stmt891 = $dbh->prepare("SELECT * FROM thrp_notes WHERE uid=? AND ssn=? LIMIT 1 ;");
$stmt891->execute(array($uid, $ssn))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-TAKE-NOTES-1000].");
//to fetch all details of notes
if ($stmt891->rowCount() != 0) {
    $row891 = $stmt891->fetch();
    $notes_id = $row891['notes_id'];
    if ($row891['q1'] != NULL) {//if q1 was not set to blank previously,then decrypt
        $q1_old = decrypt_notes($row891['q1'], $encryption_key_notes);
    } else {//if q1 was set to blank previously,then set as blank
        $q1_old = "";
    }
    if ($row891['q2'] != NULL) {//if q2 was not set to blank previously,then decrypt
        $q2_old = decrypt_notes($row891['q2'], $encryption_key_notes);
    } else {//if q2 was set to blank previously,then set as blank
        $q2_old = "";
    }
    if ($row891['q3'] != NULL) {//if q3 was not set to blank previously,then decrypt
        $q3_old = decrypt_notes($row891['q3'], $encryption_key_notes);
    } else {//if q3 was set to blank previously,then set as blank
        $q3_old = "";
    }
    if ($row891['q4'] != NULL) {//if q4 was not set to blank previously,then decrypt
        $q4_old = decrypt_notes($row891['q4'], $encryption_key_notes);
    } else {//if q4 was set to blank previously,then set as blank
        $q4_old = "";
    }
    $q5_old = $row891['q5'];
    $q6_old = $row891['q6'];
    $q7_old = $row891['q7'];
    $q8_old = $row891['q8'];
    $notes_status = 1; //notes present, hence set as 1
} else {
    //if notes not present,leave it balnk
    $q1_old = "";
    $q2_old = "";
    $q3_old = "";
    $q4_old = "";
    $q5_old = "";
    $q6_old = "";
    $q7_old = "";
    $q8_old = "";
    $notes_id = "";
    $notes_status = 0; //notes not present, hence set as 0
}

#On SUBMIT Button of take notes
if (isset($_POST['notes-submit-btn'])) {

#to get all the data from the form 
    $ssn = $_POST['ssn'];
    $notes_id = $_POST['notes_id'];
    $notes_status = $_POST['notes_status'];
    $uid = $_POST['uid'];
    $q1 = $_POST['q1'];
    $q2 = $_POST['q2'];
    $q3 = $_POST['q3'];
    $q4 = $_POST['q4'];
    $q5 = $_POST['q5'];
    $q6 = $_POST['q6'];
    $q7 = $_POST['q7'];
    $q8 = $_POST['q8'];

    #to encrypt notes to save in database
    $encrpyt_q1 = encrypt_notes($q1, $encryption_key_notes);
    $encrpyt_q2 = encrypt_notes($q2, $encryption_key_notes);
    $encrpyt_q3 = encrypt_notes($q3, $encryption_key_notes);
    $encrpyt_q4 = encrypt_notes($q4, $encryption_key_notes);

#GET current date
    date_default_timezone_set("Asia/Kolkata");
    $date_current = date('Y-m-d H:i:s');

    $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");

    if ($notes_status == 1) {
#update notes, since edit btn ws clicked
        $stmt190 = $dbh->prepare("UPDATE thrp_notes SET tid=?, uid=?, ssn=?, q1=?, q2=?, q3=?, q4=?, q5=?, q6=?, q7=?, q8=?, display=? WHERE notes_id=?");
        $stmt190->execute(array($tid, $uid, $ssn, $encrpyt_q1, $encrpyt_q2, $encrpyt_q3, $encrpyt_q4, $q5, $q6, $q7, $q8, '1', $notes_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-TAKE-NOTES-1001].");

        header('Location:' . $host . '/sco/take_notes.php?status=1');

    } else {
        #to insert into table as take notes was clicked
        #to insert into thrp_notes table
        $stmt90 = $dbh->prepare("INSERT into thrp_notes VALUES ('','$tid','$uid','$date_current','$ssn','$encrpyt_q1','$encrpyt_q2','$encrpyt_q3','$encrpyt_q4','$q5','$q6','$q7','$q8','1','','','') ");
        $stmt90->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-TAKE-NOTES-1001].");

        header('Location:' . $host . '/sco/take_notes.php?status=2');

    }
}
?>

<!DOCTYPE html>
<html>

    <!-- Mirrored from coderthemes.com/adminto_1.3/light/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Jul 2016 08:36:07 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets2/global/plugins/jquery-ui/jquery-ui.min.css"/>  
        <style>
            html,body{
                background-color: white;
            }

            /*.label-btn{
                margin-left: 50px;
            }*/
            /*.radio-btn{
                padding-bottom: 25px;
            }*/
            .row{
                margin-top: 5px;
            }
            .radio-info{
                margin-top: 0px;
                /* padding-left: 39px;*/
            }
            .panel-default>.panel-heading{
                background-color:#3498db;
                color:white;
            }
            th{
                color:black;
                text-align: center;
            }
            .td_notes{
                text-align: center;
            }
            .modal{
                background-color: white;
            }
            .tooltip fade bottom in{
                left:0px;
            }
        </style>
    </head>
    <body >

        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12" style="margin-top:0%;">
                <form class="notes-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" onsubmit='return test();'>

                    <div id="disp_msg" class="msg_disp" style="margin-bottom:12px;"></div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5">
                                <label for="q1" class="control-label">Observations <span style="color:red;">*</span> :
                                </label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <textarea class="form-control" rows="1" id="q1" name="q1" wrap="virtual" style="min-height:6px;height:105px;" ><?php
//to check if already notes is present or not, if notes are present then display them,to enable editing    
                                    if ($q1_old != "") {
                                        echo htmlspecialchars($q1_old);
                                    } else {
                                        
                                    }
                                    ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5">
                                <label for="q2" class="control-label">Goals accomplished <span style="color:red;">*</span> :</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <textarea class="form-control" rows="1" id="q2" name="q2" wrap="virtual" style="min-height:1px;"><?php
                                    //to check if already notes is present or not, if notes are present then display them,to enable editing    
                                    if ($q2_old != "") {
                                        echo htmlspecialchars($q2_old);
                                    } else {
                                        
                                    }
                                    ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5">
                                <label for="q3" class="control-label">Homework assigned <span style="color:red;">*</span> :
                                </label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <textarea class="form-control" rows="2" id="q3" name="q3" wrap="virtual" style="min-height:2px;height:45px;"><?php
                                    //to check if already notes is present or not, if notes are present then display them,to enable editing    
                                    if ($q3_old != "") {
                                        echo htmlspecialchars($q3_old);
                                    } else {
                                        
                                    }
                                    ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5">
                                <label for="q4" class="control-label">Remarks <span style="color:red;">*</span> :</label>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <textarea class="form-control" rows="2" id="q4" name="q4" wrap="virtual" style="min-height:1px;height:45px;"><?php
                                    //to check if already notes is present or not, if notes are present then display them,to enable editing    
                                    if ($q4_old != "") {
                                        echo htmlspecialchars($q4_old);
                                    } else {
                                        
                                    }
                                    ?></textarea>
                            </div>
                        </div>
                    </div>
                    <!--Start of questions and radio buttons --->
                    <div class="row">
                        <div class="form-group" style="margin-top:10px;">
                            <div class="col-md-5 col-sm-5">
                                <label class="control-label">Observations <span style="color:red;">*</span> :</label>
                            </div>

                            <div class="col-md-7 col-sm-7">
                                <label>
                                    <span style="padding-left:10px;">LOW</span> <span style="margin-left:179px;">HIGH</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5 ">
                                <label  class="control-label">Client motivation level</label>
                            </div>
                            <div class="col-md-5 col-sm-5" style="height:25px;">
                                <!-- if the radio button is seletcted previously, mark as checked--->
                                <div class="radio radio-info" >
                                    <div class="row" >
                                        <div class="col-md-2 col-sm-2 col-md-2 col-sm-2 radio-btn" style="margin-left:10px;">
                                            <input type="radio" name="q5" id="q6" value="1" <?php echo ($q5_old == 1) ? "checked" : ""; ?>>
                                            <label for="q6" class="label-btn">
                                                1
                                            </label></div><div class="col-md-2 col-sm-2 col-md-2 col-sm-2 radio-btn" style="margin-left:-3px;">
                                            <input type="radio" name="q5" id="q7" value="2" <?php echo ($q5_old == 2) ? "checked" : ""; ?>>
                                            <label for="q7" class="label-btn" >
                                                2
                                            </label></div><div class="col-md-2 col-sm-2 col-md-2 col-sm-2 radio-btn" style="margin-left:-4px;">
                                            <input type="radio" name="q5" id="q8" value="3" <?php echo ($q5_old == 3) ? "checked" : ""; ?>>
                                            <label for="q8" class="label-btn">
                                                3
                                            </label></div><div class="col-md-2 col-sm-2 col-md-2 col-sm-2 radio-btn" style="margin-left:-3px;">
                                            <input type="radio" name="q5" id="q9" value="4" <?php echo ($q5_old == 4) ? "checked" : ""; ?>>
                                            <label for="q9" class="label-btn">
                                                4
                                            </label></div><div class="col-md-2 col-sm-2 col-md-2 col-sm-2 radio-btn" style="margin-left:-3px;">
                                            <input type="radio" name="q5" id="q10" value="5" <?php echo ($q5_old == 5) ? "checked" : ""; ?>>
                                            <label for="q10" class="label-btn">
                                                5
                                            </label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5 ">
                                <label for="field-4" class="control-label">  Understanding of session material </label>
                            </div>
                            <div class="col-md-5 col-sm-5" >
                                <!-- if the radio button is seletcted previously, mark as checked--->
                                <div class="radio radio-info">
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q6" id="q11" value="1" <?php echo ($q6_old == 1) ? "checked" : ""; ?>>
                                        <label for="q11" class="label-btn">
                                            1
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q6" id="q12" value="2" <?php echo ($q6_old == 2) ? "checked" : ""; ?>>
                                        <label for="q12" class="label-btn" >
                                            2
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q6" id="q13" value="3" <?php echo ($q6_old == 3) ? "checked" : ""; ?>>
                                        <label for="q13" class="label-btn">
                                            3
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q6" id="q14" value="4" <?php echo ($q6_old == 4) ? "checked" : ""; ?>>
                                        <label for="q14" class="label-btn">
                                            4
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q6" id="q15" value="5" <?php echo ($q6_old == 5) ? "checked" : ""; ?>>
                                        <label for="q15" class="label-btn">
                                            5
                                        </label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5 ">
                                <label for="field-4" class="control-label"> Completion of session tasks </label>
                            </div>
                            <div class="col-md-5 col-sm-5" >
                                <!-- if the radio button is seletcted previously, mark as checked--->
                                <div class="radio radio-info">
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q7" id="q21" value="1" <?php echo ($q7_old == 1) ? "checked" : ""; ?>>
                                        <label for="q21" class="label-btn">
                                            1
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q7" id="q22" value="2" <?php echo ($q7_old == 2) ? "checked" : ""; ?>>
                                        <label for="q22" class="label-btn" >
                                            2
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q7" id="q23" value="3" <?php echo ($q7_old == 3) ? "checked" : ""; ?>>
                                        <label for="q23" class="label-btn">
                                            3
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q7" id="q24" value="4" <?php echo ($q7_old == 4) ? "checked" : ""; ?>>
                                        <label for="q24" class="label-btn">
                                            4
                                        </label></div><div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q7" id="q25" value="5" <?php echo ($q7_old == 5) ? "checked" : ""; ?>>
                                        <label for="q25" class="label-btn">
                                            5
                                        </label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5 col-sm-5 ">
                                <label for="field-4" class="control-label">Satisfaction with sessions </label>
                            </div>
                            <div class="col-md-5 col-sm-5" >
                                <!-- if the radio button is seletcted previously, mark as checked--->
                                <div class="radio radio-info">
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q8" id="q31" value="1" <?php echo ($q8_old == 1) ? "checked" : ""; ?>>
                                        <label for="q31" class="label-btn">
                                            1
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q8" id="q32" value="2" <?php echo ($q8_old == 2) ? "checked" : ""; ?>>
                                        <label for="q32" class="label-btn" >
                                            2
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q8" id="q33" value="3" <?php echo ($q8_old == 3) ? "checked" : ""; ?>>
                                        <label for="q33" class="label-btn">
                                            3
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q8" id="q34" value="4" <?php echo ($q8_old == 4) ? "checked" : ""; ?>>
                                        <label for="q34" class="label-btn">
                                            4
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-2 radio-btn">
                                        <input type="radio" name="q8" id="q35" value="5" <?php echo ($q8_old == 5) ? "checked" : ""; ?>>
                                        <label for="q35" class="label-btn">
                                            5
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='error' style='color:red;text-align:center;'></div>                        
                    <div class="modal-footer" >
                        <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                        <input type="hidden" name="ssn" value="<?php echo $ssn; ?>">
                        <input type="hidden" name="notes_id" value="<?php echo $notes_id; ?>"> 
                        <input type="hidden" name="notes_status" value="<?php echo $notes_status; ?>"> 
                        <button type="button" class="btn btn-danger waves-effect" style="margin-right: 2%;" id="notes-no-btn" name="notes-no-btn"  >Discard & Close</button>
                        <input type="submit" id="notes-submit-btn" name="notes-submit-btn" value="Save" class="btn btn-success"/>
                    </div>
                </form>
            </div>
            <div id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                            <!-- dialog body for discard changes -->
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                Are you sure you want to discard changes? 
                            </div>
                            <!-- dialog buttons -->
                            <input type="submit" value="Yes" id="close-btn" name="close-btn" class="btn btn-danger" />
                            <?php
#on click of 'yes',reload page
                            if (isset($_POST['close-btn'])) {
                                if (isset($_REQUEST['uid'])) {
                                    $uid = $_REQUEST['uid'];
                                } else {
                                    echo "Unable to fetch user details. Error COde : UIDNIU";
                                }

                                echo '<script>parent.location.reload();</script>';
                            }
                            ?>
                            <input type="hidden" name="uid" value="<?php echo $uid; ?>"/> 
                            <input type="submit" value="No" id="no-btn" name="no-btn" class="btn btn-success" data-dismiss="modal"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="../assets/js/jquery.min.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/js/bootstrap.min.js"></script>

        <script>
                    $(".notes-form").validate({
                        rules: {
                            q1: {
                                required: true,
                            },
                            q2: {
                                required: true,
                            },
                            q3: {
                                required: true,
                            },
                            q4: {
                                required: true,
                            }
                        },
                        messages: {
                            q1: {
                                required: "Please enter some text",
                            },
                            q2: {
                                required: "Please enter some text",
                            },
                            q3: {
                                required: "Please enter some text",
                            },
                            q4: {
                                required: "Please enter some text",
                            }
                        }
                    });
                    function validateRadio(obj, error_no) {
                        var result = 0;
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i].checked == true) {
                                result = 1;

                                break;
                            }
                        }
                        return result;
                    }

                    function test() {
                        var err = '';
                        var q5 = document.getElementsByName("q5");
                        var q6 = document.getElementsByName("q6");
                        var q7 = document.getElementsByName("q7");
                        var q8 = document.getElementsByName("q8");

                        if (!validateRadio(q5, 5)) {
                            $("#error5").addClass("red-error");
                            err += '\n 2';
                        }
                        if (!validateRadio(q6, 6)) {
                            $("#error6").addClass("red-error");
                            err += '\n 2';
                        }
                        if (!validateRadio(q7, 7)) {
                            $("#error7").addClass("red-error");
                            err += '\n 2';
                        }
                        if (!validateRadio(q8, 8)) {
                            $("#error8").addClass("red-error");
                            err += '\n 2';
                        }


                        if (err.length) {
                            $("div.error").html("Please mark all the options");
                            return false;
                        } else {

                            return true;
                        }

                    }

        </script>
        <script>
            $("#notes-no-btn").click(function () {

                var old_text_q1 = "<?php echo $q1_old; ?>";
                var old_text_q1_length = old_text_q1.length;
                var old_text_q2 = "<?php echo $q2_old; ?>";
                var old_text_q2_length = old_text_q2.length;
                var old_text_q3 = "<?php echo $q3_old; ?>";
                var old_text_q3_length = old_text_q3.length;
                var old_text_q4 = "<?php echo $q4_old; ?>";
                var old_text_q4_length = old_text_q4.length;

                var new_text_q1 = $("#q1").val();
                var new_text_q1_length = new_text_q1.length;
                var new_text_q2 = $("#q2").val();
                var new_text_q2_length = new_text_q2.length;
                var new_text_q3 = $("#q3").val();
                var new_text_q3_length = new_text_q3.length;
                var new_text_q4 = $("#q4").val();
                var new_text_q4_length = new_text_q4.length;

                if ((new_text_q1_length != old_text_q1_length) || (new_text_q2_length != old_text_q2_length) || (new_text_q3_length != old_text_q3_length) || (new_text_q4_length != old_text_q4_length)) {

                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            //console.log("button pressed");  
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        // "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                } else if (((new_text_q1_length == " ") && (old_text_q1_length == " ")) || ((new_text_q2_length == " ") && (old_text_q2_length == " ")) || ((new_text_q3_length == " ") && (old_text_q3_length == " "))
                        || ((new_text_q4_length == " ") && (old_text_q4_length == " "))) {

                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            //console.log("button pressed");  
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                }
            });

        </script>
        <script type="text/javascript">
<?php if ($status == 1) { ?>
                $(document).ready(function () {
                    $("#disp_msg").addClass("alert");
                    $("#disp_msg").addClass("alert-success");
                    $("#disp_msg").html("Notes has been updated.");
                });
<?php } else if ($status == 2) { ?>
                $(document).ready(function () {
                    $("#disp_msg").addClass("alert");
                    $("#disp_msg").addClass("alert-success");
                    $("#disp_msg").html("Notes has been added.");
                });
<?php } ?>
        </script>
    </body>
</html>