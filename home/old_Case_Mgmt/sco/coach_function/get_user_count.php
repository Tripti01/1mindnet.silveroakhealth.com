<?php

//this function is used to fecth the status of the user
//uid is passed as parameter to the function
//the function returns the following:
//1.returns 1, when user has not yet started cbt
//2. returns 2, when user has completed pre-assessment and user is in progress
//3. returns 3, when post-assessment has been completed by user and user has finished cbt
//4. returns 4, when user has not yet completed the post-assessment but the end date has been reached
//5. return 5, when user is inactive

function get_user_count($uid) {
    global $dbh;

    //including files
    require_once 'is_active_count.php';
    //declaring variables
    $return_value = 0;
    #to fetch current datetime
    $cur_datetime = strtotime(date('Y-m-d H:i:s'));
    //the number of days to complete the session 
    $add_days = 14;

    //to check if pre-assessment has been done or not
    $stmt01 = $dbh->prepare("SELECT * FROM asmt_score WHERE uid=? and type=? LIMIT 1");
    $stmt01->execute(array($uid, 'PREA')) or die(print_r($stmt01->errorInfo(), true));
    if ($stmt01->rowCount() != 0) {

        //to fetch the status of user from user_progress
        $stmt = $dbh->prepare("SELECT * FROM user_progress WHERE uid=? ORDER BY seqn DESC LIMIT 9");
        $stmt->execute(array($uid)) or die(print_r($stmt->errorInfo(), true));
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

            # When ordered by descending order The last ones will be 0 till we encounter 1 or 9
            if ($row['status'] == '0') {
                $active = is_active_count($uid); //to check if user is active or not
                if ($active == 1) {
                    #if status is 0 then return cbt_progress_count
                    $return_value = 2;
                } else {
                    $return_value = 4; //cbt end date reacched
                }
            } else {
                if ($row['status'] == '9') {

                    # We found 9, That means this is in Progres,then check if he is an activer user or not
                    $active = is_active_count($uid); //to check if user is active or not
                    if ($active == 1) {//user is active
                        $ssn = $row['ssn']; // Get the session 
                        #to fecth the start date of session
                        $stmt11 = $dbh->prepare("SELECT ssn_starttime FROM user_time_ssn WHERE uid=? AND ssn=? LIMIT 1");
                        $stmt11->execute(array($uid, $ssn)) or die(print_r($stmt11->errorInfo(), true));
                        if ($stmt11->rowCount() != 0) {
                            $row11 = $stmt11->fetch();
                            //we fetch the session starttime and add $add_days to get the reminder check datetime
                            $rem_datetime = strtotime(date('Y-m-d H:i:s', strtotime($row11['ssn_starttime']) + (24 * 3600 * $add_days)));
                        }

                        # Now we have to find if reminder check datetime is less than the current datetime or not
                        //if reminder check datetime is more than the current datetime,then proceed
                        if ($rem_datetime < $cur_datetime) {//if reminder check datetime is more than the current datetime,set as below
                            $return_value = 5;
                        } else {//reminder check datetime is less than the current datetime,set all as 0
                            $return_value = 2;
                        }
                    } else {
                        $return_value = 4; //cbt end date reacched
                    }
                    break;
                } else if ($row['status'] == '1') {
                    # We found 1, That means this is Completed, Now found what is the Sequnce Number of this 
                    $seqn = $row['seqn'];
                    if ($seqn == 9) {
                        #if post asmt is done then display message post asmt completed else nothing
                        $stmt01 = $dbh->prepare("SELECT * FROM asmt_score WHERE uid=? and type=? LIMIT 1");
                        $stmt01->execute(array($uid, 'POST')) or die(print_r($stmt01->errorInfo(), true));
                        if ($stmt01->rowCount() != 0) {
                            #if post assessment is completed
                            $return_value = 3; //return cbt finished
                        } else {
                            $active = is_active_count($uid); //to check if user is active or not
                            if ($active == 1) {
                                $return_value = 2; //in-progress 
                            } else {
                                $return_value = 4; //cbt end date reacched
                            }
                        }
                    } else {

                        $active = is_active_count($uid); //to check if user is active or not
                        if ($active == 1) {

                            $ssn = $row['ssn'];
                            #to fecth the end date of session
                            $stmt11 = $dbh->prepare("SELECT ssn_endtime FROM user_time_ssn WHERE uid=? AND ssn=? LIMIT 1");
                            $stmt11->execute(array($uid, $ssn)) or die(print_r($stmt11->errorInfo(), true));
                            if ($stmt11->rowCount() != 0) {
                                $row11 = $stmt11->fetch();
                                //we fetch the session end datetime and add $add_days to get the reminder check datetime
                                $rem_datetime = strtotime(date('Y-m-d H:i:s', strtotime($row11['ssn_endtime']) + (24 * 3600 * $add_days)));
                            }

                            #to get next sequence 
                            $ssn_num = substr($ssn, -1);
                            $ssn_next = 'SSN' . ($ssn_num + 1);

                            #to chech if next session has been started or not
                            $stmt21 = $dbh->prepare("SELECT ssn_starttime FROM user_time_ssn WHERE uid=? AND ssn=? LIMIT 1");
                            $stmt21->execute(array($uid, $ssn_next)) or die(print_r($stmt21->errorInfo(), true));
                            if ($stmt21->rowCount() != 0) {
                                $row21 = $stmt21->fetch();
                                $strt_datetime = $row21['ssn_starttime'];
                            }

                            #proceed only if ssn_starttime is null
                            if ($strt_datetime == '0000-00-00 00:00:00') {

                                # Now we have to find if reminder check datetime is less than the current datetime or not
                                //if reminder check datetime is more than the current datetime,then proceed
                                if ($rem_datetime < $cur_datetime) {
                                    $return_value = 5;
                                } else {//reminder check datetime is less than the current datetime,set all as 0
                                    $return_value = 2; //in-progress
                                }
                            } else {//next session is started hence set all as 0
                                $return_value = 2; //in-progress
                            }
                        } else {
                            $return_value = 4; //cbt end date reacched
                        }
                    }
                    break;
                }
            }
        }
    } else {
        $active = is_active_count($uid); //to check if user is active or not
        if ($active == 1) {
            $return_value = 1; //not yet done pre-assessment
        } else {
            $return_value = 4; //cbt end date reacched
        }
    }
    # Reurning both values 
    return ($return_value);
}
?>