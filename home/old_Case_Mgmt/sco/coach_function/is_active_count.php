<?php

#this function is used to check if the uid passed is active or not
#this check is done by comparing the due_datetime for the CBTEND with the current datetime
#if the current datetime is less than the due_datetime then user is active else not active
#if the user is active then return 1 else return as 0

function is_active_count($uid) {
    ini_set('max_execution_time', 300);
    #including functions
    include 'soh-config.php';

    #intially active is 0
    $active = 0;
#to fetch current datetime
    date_default_timezone_set("Asia/Kolkata");
    $cur_datetime = strtotime(date('Y-m-d H:i:s'));

    $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");

    #to get details of due_date, to display only those client details who have not yet completed 10 weeks in cbt
    $stmt11 = $dbh->prepare("SELECT due_datetime FROM user_schedule WHERE uid=? AND ssn=? LIMIT 1");
    $stmt11->execute(array($uid, "CBTEND"))or die(print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[THRP-MY_CLIENT-1001]."));
    if ($stmt11->rowCount() != 0) {
        $row11 = $stmt11->fetch();
        $due_datetime = strtotime($row11['due_datetime']);

        //we fetch the due_datetime and compare it with current datetime
        //if due_Datetime is greater than current datetime, then the user is active, set status as 1, else set status as 0
        if ($due_datetime > $cur_datetime) {
            //if the user is active, then set status as 1
            $active = 1;
        } else {
            //if user cbt has been expired, ten set as 0
            $active = 0;
        }
    }
    return $active;
}

?>