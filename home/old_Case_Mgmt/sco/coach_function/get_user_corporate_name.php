<?php

# Get the user id and find the corporate name of the user

function get_corporate_name_of_user($uid) {

    include 'soh-config.php';

    $corp_name = "";

    $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");

    $stmt11 = $dbh->prepare("SELECT name FROM corp_login, corp_users_list WHERE corp_login.corp_id = corp_users_list.corp_id AND corp_users_list.uid=?");
    $stmt11->execute(array($uid))or die(print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[THRP-MY_CLIENT-100133]."));
    if ($stmt11->rowCount() != 0) {
        $row11 = $stmt11->fetch();
        $corp_name = $row11['name'];

        return $corp_name;
    } else {
        $corp_name = "Not a Corporate User";
    }
    return $corp_name;
}

?>