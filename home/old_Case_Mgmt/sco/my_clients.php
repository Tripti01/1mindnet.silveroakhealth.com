<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/get_user_status_page.php';
include 'functions/coach/get_session_name.php';
include 'coach_function/get_user_count.php';
include 'coach_function/is_active.php';


#getting tid from session
$tid = $_SESSION['tid'];

#By default client status is set as 1
$client_status = 1;
#declaring variables
$cbt_started_count = 0;
$cbt_end_count = 0;
$cbt_progress_count = 0;
$cbt_not_started_count = 0;
$uid_count = 0;

# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Query database and fetch all the users assigned to this therapist
$i = 0; //counter for users assigned
$j = 0; //counter for total useres
$stmt1 = $dbh->prepare("SELECT user_therapist.uid,user_login.name, user_login.email FROM user_therapist,user_login WHERE tid=? AND user_therapist.uid=user_login.uid ORDER BY uid");
$stmt1->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY_CLIENT-1000].");
if ($stmt1->rowCount() != 0) {
    while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $uid = $row1['uid'];

        #to check if the uid is active or not
        $active = is_active($uid);
        //function to get status of the clients assigned 
        $return_value = get_user_count($uid);
        #to get count of different statuese of user assigned to this therapist/coach

        if ($return_value == 1) {//user has not yet started cbt
            $cbt_not_started_count++;
        } else if ($return_value == 2) {//user cbt is in progress
            $cbt_progress_count++;
        } else if ($return_value == 3) {//user cbt is finished
            $cbt_end_count++;
        } else if ($return_value == 5) {
            $cbt_progress_count++;
        }

        if ($active == 1) {
            $uid_list[$i] = $uid; //to put uids into array
            $name[$i] = strtoupper(decrypt($row1['name'], $encryption_key)); //to fetch name
            $email_list[$i] = $row1['email']; //to fetch email
            //to check if the given uid is corporate or not
            $stmt10 = $dbh->prepare("SELECT name FROM corp_users_list, corp_login WHERE corp_users_list.uid = ? AND corp_users_list.corp_id = corp_login.corp_id LIMIT 1");
            $stmt10->execute(array($uid_list[$i]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY_CLIENT-1001].");
            if ($stmt10->rowCount() != 0) {//if the user is a corporate user, set company_name as its name
                $row2 = $stmt10->fetch();
                $company_name[$i] = decrypt($row2['name'], $encryption_key);
            } else {//else set company_name as blank
                $company_name[$i] = "";
            }
			
			$stmt12 = $dbh->prepare("SELECT creation_time FROM user_time_creation WHERE uid=?");
			$stmt12->execute(array($uid_list[$i]));
			if ($stmt12->rowCount() != 0) {//if the user is a corporate user, set company_name as its name
                $row12 = $stmt12->fetch();
                $reg_date[$i] = date("d-M-Y", strtotime($row12['creation_time']));
            } else {//else set reg date as blank
                $reg_date[$i] = "";
            }
			
			$stmt13 = $dbh->prepare("SELECT login_time FROM user_time_login WHERE uid=?");
			$stmt13->execute(array($uid_list[$i]));
			if ($stmt13->rowCount() != 0) {//if the user is a corporate user, set company_name as its name
                $row13 = $stmt13->fetch();
                $login_date[$i] = date("d-M-Y H:i", strtotime($row13['login_time']));
            } else {//else set login date as blank
                $login_date[$i] = "";
            }
			
            # Get the current status of the user
            list($user_ssn_seqn[$i], $user_ssn_status[$i], $user_page_number[$i], $user_page_total[$i]) = get_user_status($uid_list[$i]);
            $i++;
        } else {
            //dont display the names those who have completed 10 weeks
            
        }
        $j++;
    }//end of while
} else {
    //if there are no clients assigned,then a message will be displayed
    $client_status = 0;
}
$total_users = $j; //total number of clients assigned to this therapist/coach
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>SCO Clients</title>
        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <style>
            .side_text span{
                margin: -1px 1px 1px -1px;
                line-height: 5%;
                border-left: 2px solid blue;
                padding-left: 10px;
            }
            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #ebeff2;
            }
            tbody {
                color: #797979;
            }
            th {
                color: #666666;
                font-weight: 600;
            }
            .title_color{
                color:#223C80;
                font-size: 35px;
                margin-bottom: 20px;
                padding-top:15px;
            }
            .dataTables_info{
                display: none;
            }
            .dataTables_length{
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .title_color{
                color:#223C80;
                font-size: 35px;
                margin-bottom: 20px;
                padding-top:15px;
            }
            .table-striped > tbody > tr:nth-of-type(odd), .table-hover > tbody > tr:hover, .table > thead > tr > td.active, .table > tbody > tr > td.active, .table > tfoot > tr > td.active, .table > thead > tr > th.active, .table > tbody > tr > th.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > tbody > tr.active > td, .table > tfoot > tr.active > td, .table > thead > tr.active > th, .table > tbody > tr.active > th, .table > tfoot > tr.active > th {
                background-color: #fff !important;
            }
            .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
                background-color: #3498db;
                border-color:  #3498db;
            }
        </style>

    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-3 col-md-3" style="padding-left: 0px;">
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $total_users; ?></h1>
                                        <h4>Total</h4>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3" style="padding-left: 0px;">
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $cbt_not_started_count; ?></h1>
                                        <h4>Not Yet Started</h4>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3" >
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $cbt_progress_count; ?></h1>
                                        <h4>In-Progress</h4>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3" style="padding-right: 0px;">
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $cbt_end_count; ?></h1>
                                        <h4>Completed</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card-box">
                                    <div class="table-responsive">
                                        <?php
                                        if ($client_status == 0) {
                                            echo '<div class="alert alert-danger" >';
                                            echo '<b>No Client to show.</b>';
                                            echo '</div>';
                                        } else {
                                            if(isset($uid_list)) {
                                            ?>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <table  id='datatable' class='table table-striped table-bordered'>
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th style="color:#666666;">Client Name</th>
                                                            <th>Email</th>
                                                            <th>Company Name</th>
                                                            <th>Status</th>
															<th>Registration Date</th>
															<th>Recent Activity<br/><span style="font-size:9px">(Last Login Time)</span></th>
                                                            <th>View</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        # Now we have the list of all the active uid assigned to this coach/therapist
                                                        # We will take out UID one by one and show their current and last three sessions status
                                                        for ($k = 0; $k < count($uid_list); $k++) {
                                                            #taking out the uid
                                                            $uid = $uid_list[$k];

                                                            # Taking out user status
                                                            $status = $user_ssn_status[$k];

                                                            # Calculate Serail Number
                                                            $sr_no = $k + 1;

                                                            echo '<tr>';
                                                            echo '<td>' . $sr_no . '</td>';
                                                            echo '<td><a href="view_client.php?uid=' . $uid . '">' . $name[$k] . '</a></td>';
                                                            echo '<td>' . $email_list[$k] . '</td>';
                                                            echo '<td>' . $company_name[$k] . '</td>';
                                                            echo '<td style="align-content: center;">' . $status . '</td>';
															echo '<td>' . $reg_date[$k] . '</td>';
															echo '<td>' . $login_date[$k] . '</td>';
															
                                                        ?> 
                                                            <!---View button --->
                                                        <td><a href="view_client.php?uid=<?php echo $uid_list[$k]; ?>"><button type="button" class="btn btn-primary btn-bordred waves-effect btn-xs waves-light">View</button></a></td>
                                                        </tr>
                                                    <?php } } //ending for loop
                                                    else{
                                                        echo '<div class="alert alert-danger" >';
														echo '<b>No Client to show.</b>';
														echo '</div>';
                                                        }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> 
                </div> 
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>

        <script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/detect.js"></script>
		<script src="../assets/js/fastclick.js"></script>
		<script src="../assets/js/jquery.slimscroll.js"></script>
		<script src="../assets/js/jquery.blockUI.js"></script>
		<script src="../assets/js/waves.js"></script>
		<script src="../assets/js/jquery.nicescroll.js"></script>
		<script src="../assets/js/jquery.scrollTo.min.js"></script>

		<!-- Form wizard -->
		<script src="../assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
		<!-- App js -->
		<script src="../assets/js/jquery.core.js"></script>
		<script src="../assets/js/jquery.app.js"></script>

		<script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

		<!-- App js -->
		<script src="../assets/js/app.min.js"></script>
        <!-- Datatables-->
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.print.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
		
		<!-- App js -->
        <script src="../assets/js/app.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

            $(".dataTables_info").replaceWith("<h2>New heading</h2>");
        </script>
    </body>
</html>