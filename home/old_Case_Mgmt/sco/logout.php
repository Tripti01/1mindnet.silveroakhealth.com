<?php
//including files and functions
include 'host.php';
include 'soh-config.php';

session_start();
$_SESSION = array();

if (isset($_COOKIE[session_name()])) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
}
//unset the session 
unset($_SESSION['tid']);
session_destroy();//destroying the session
//redirect to login page
header('Location:' . $host . '/../login');
?>