<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/get_session_name.php';
include 'functions/coach/get_user_name.php';
include 'coach_function/get_user_count.php';

#getting tid from session
$tid = $_SESSION['tid'];

#to fetch the current datetime
date_default_timezone_set("Asia/Kolkata");
$cur_date = date('Y-m-d');
$cur_datetime = strtotime(date('Y-m-d H:i:s'));

//intializ counter to 0
$count_not_yet_started = 0;
$count_cbt_progress = 0;
$count_cbt_finished = 0;
$count_cbt_end_date_reached = 0;
$count_inactive = 0;
$m = 0;

# Start the database 
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Getting data for - Calls Today -
# query to fetch recent 6 uids who are scheduled for call today from thrp_scheduled_Calls whose display is 1
$i = 0; //counter for calls today
$stmt100 = $dbh->prepare("SELECT * FROM thrp_scheduled_calls WHERE tid=? AND DATE(scheduled_date)=? AND call_status=? AND display=? ORDER BY scheduled_date LIMIT 6");
$stmt100->execute(array($tid, $cur_date, 0, 1))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1100].");
if ($stmt100->rowCount() != 0) {
    while ($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)) {
        $uid_today_list = $row100['uid']; #acts as flag to check if uids are set or not,if set then display the calls for today else dont display anything
        $uid_today[$i] = $row100['uid']; //to put uids into array
        $ssn_today[$i] = $row100['ssn']; //to fetch session for further calculations
        $ssn_fullname_today[$i] = get_ssn_fullname($row100['ssn']); //to get the session fullname
        $schedule_datetime[$i] = $row100['scheduled_date']; //to get scheduled date for further calculations
        $schedule_time[$i] = date('h:i A', strtotime($row100['scheduled_date'])); // Taking out the time part from the full date time
        $name_today[$i] = get_user_name($uid_today[$i]); #to fetch name for this UID
        $i++;
    }
}

##completed session--
#tofetch details of 6 users wh have completed sessions and calls have to scheduled for them
$j = 0; //counter for completed session
$stmt200 = $dbh->prepare("SELECT * FROM thrp_user_ssn_completed WHERE tid=? AND scheduled_status=? ORDER BY completed_on ASC LIMIT 6");
$stmt200->execute(array($tid, '0'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1101].");
if ($stmt200->rowCount() != 0) {
    while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $uid_completed_list = $row200['uid']; #flag to check if uids are set or not,if set then display the uids who have to be scheduled else dont display anything
        $uid_completed[$j] = $row200['uid']; //to put uids into array
        $ssn_completed[$j] = $row200['ssn']; //to fetch session for further calculations
        $ssn_fullname_completed[$j] = get_ssn_fullname($row200['ssn']); //to get the session fullname
        $ssn_endtime_completed[$j] = date('d-M-Y h:i A', strtotime($row200['completed_on'])); //to dispaly completed_on datetime as (25-Nov-2016 12:00 PM) format.
        $completed_on[$j] = $row200['completed_on']; //to fetch completed_on for further calculation
        $name_completed[$j] = get_user_name($uid_completed[$j]); #to fetch name for this UID
        $j++;
    }
}

##Client Reminder--
#to fetch all details of clients who have to be reminded from thrp_remind
$k = 0; //counter for client 
$stmt300 = $dbh->prepare("SELECT * FROM thrp_remind WHERE tid=? AND display=? LIMIT 6");
$stmt300->execute(array($tid, '1'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1102].");
if ($stmt300->rowCount() != 0) {
    while ($row300 = $stmt300->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $uid_remindlist = $row300['uid']; #flag to check if uids are set or not,if set then display the uids else dont display anything
        $uid_remind[$k] = $row300['uid']; //to put uids into array
        $name_remind[$k] = decrypt($row300['name'], $encryption_key); #to fetch name for this UID
        $ssn_remind[$k] = $row300['ssn']; //to fetch session for further calculations
#if ssn is "CBTSTART", then display as 'CBT' else as session_fullname.
        if ($ssn_remind[$k] == "CBTSTART") {
            $ssn_fullname_remind[$k] = "CBT";
        } else {
            $ssn_fullname_remind[$k] = get_ssn_fullname($ssn_remind[$k]);
        }

        $text_remind[$k] = $row300['text'];
        $date_db[$k] = strtotime(date('Y-m-d H:i:s', strtotime($row300['date']))); //to diaplay datetime of date when reminder has to be sent
        $days_remind[$k] = intval((abs($cur_datetime - $date_db[$k])) / 86400); //to calculate number of days
        $k++;
    }
}

#to get details regarding pending notes
#first, we find the uids of users whose call have been taken place.
#we check if notes have been taken place for that session for that particular uid or not.
#if notes are not pesent then we display the user details, else display nothing

$l = 0; //counter to fecth details of uids whose notes are pending
#to fetch uids of clients whose call status=1
$stmt400 = $dbh->prepare("SELECT uid,ssn FROM thrp_scheduled_calls WHERE tid=? AND call_status=? AND display=? ");
$stmt400->execute(array($tid, 1, 1))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1103].");
if ($stmt400->rowCount() != 0) {
    while ($row400 = $stmt400->fetch(PDO::FETCH_ASSOC)) {
        $uid_call_done = $row400['uid']; //pass all uids whose call has been done into variable for further calculations
        $ssn_list = $row400['ssn']; //pass all ssn into variable for further calculations
//if the ssn is "cbtstart", then check if case history has been done or not, else check in thrp_nites
        if ($ssn_list == "CBTSTART") {
            $stmt21 = $dbh->prepare("SELECT * FROM thrp_case_history WHERE tid=? AND uid=? LIMIT 1");
            $stmt21->execute(array($tid, $uid_call_done))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1104].");
            if ($stmt21->rowCount() != 0) {
//if case history exsists,display nothing
            } else {
//to get details only for 6 users
                if ($l >= 6) {
//do nothing
                    break;
                } else {
//if there are notes for the uid
                    $uid_pending_list = $uid_call_done; //paasing only the uids whose notes have been taken from the above list and also use it as a flag.if isset display else dislay nothing.
                    $uid_pending[$l] = $uid_pending_list; //passing into array
                    $name_pending[$l] = get_user_name($uid_pending[$l]); //to fecth name
                    $ssn_pending[$l] = $ssn_list; //passing sessions of those uids whose notes have been taken
                    $ssn_fullname_pending[$l] = get_ssn_fullname($ssn_list); //to fetch the session name
                    $l++;
                }
            }
        } else {
#to verify if the notes are taken for that particular session or not
            $stmt20 = $dbh->prepare("SELECT uid FROM thrp_notes WHERE tid=? AND uid=? AND ssn=? LIMIT 1");
            $stmt20->execute(array($tid, $uid_call_done, $ssn_list))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-PENDING-NOTES-1001].");
            if ($stmt20->rowCount() != 0) {
//if notes exsists
            } else {
//to get details only for 6 users
                if ($l >= 6) {
//do nothing
                    break;
                } else {
//if there are notes for the uid
                    $uid_pending_list = $uid_call_done; //paasing only the uids whose notes have been taken from the above list and also use it as a flag.if isset display else dislay nothing.
                    $uid_pending[$l] = $uid_pending_list; //passing into array
                    $name_pending[$l] = get_user_name($uid_pending[$l]); //to fecth name
                    $ssn_pending[$l] = $ssn_list; //passing sessions of those uids whose notes have been taken
                    $ssn_fullname_pending[$l] = get_ssn_fullname($ssn_list); //to fetch the session name
                    $l++;
                }
            }
        }
    }
}

$m = 0;
//to fetch details for my_Activity
//to fetch number of user counts for not_yet_started,in-progress,nd completed
$stmt100 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=? ORDER BY uid;");
$stmt100->execute(array($tid));
if ($stmt100->rowCount() != 0) {
    while ($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)) {
        $uid_activity[$m] = $row100['uid'];
//function to get status of the clients assigned 
        $return_value = get_user_count($uid_activity[$m]);

        if ($return_value == 1) {//user has not yet started cbt
            $count_not_yet_started++;
        } else if ($return_value == 2) {//user cbt is in progress
            $count_cbt_progress++;
        } else if ($return_value == 3) {//user cbt is finished
            $count_cbt_finished++;
        } else if ($return_value == 4) {//user cbt end date reached
            $count_cbt_end_date_reached++;
        } else if ($return_value == 5) {
            $count_inactive++;
        }
        $m++;
    }
}
//fetch all the uids approriately into arrays
$cbt_finished = $count_cbt_finished; //to fetch uids finished cbt into cbt_finished array
$total_users = $m - $cbt_finished; //to fecth all uids into total_users array
$not_yet_started = $count_not_yet_started; //to fetch uids nt yet started cbt into not_yet_started array
$cbt_progress = $count_cbt_progress; //to fetch uids nt yet started cbt into cbt_progress array
$cbt_end_date_reached = $count_cbt_end_date_reached; //to fetch uids whose end date reached into cbt_end_Date_reached array
$in_active = $count_inactive;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <!-- App title -->
        <title>Dashboard</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- Plugins css-->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <style>
            html,body {
                width:100%;
                height:100%
            }
            .panel-default > .panel-heading {
                background-color:#3498db;
                color:white;
            }
            .res_td{
                width:35%;
            }
            .res_td1{
                width:35%;
            }
            .notes{
                width:58.7%
            }
            .res_td2{
                width:25%;
                vertical-align: middle;
            }
            .res_td3{
                width: 79%;
            }
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
                border-bottom:1px solid #ddd;
            }
            .modal-title{
                text-align: left;
            }

        </style>
    </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                           <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <a href="my_clients.php"><button class="btn btn-primary waves-effect waves-light m-b-5" > My All SCO Clients </button></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="portlet-body">
                                    <div class="panel-group accordion" id="accordion3" aria-multiselectable="true">

                                        <!-- ============================================================== -->
                                        <!-- Calls Today -->
                                        <!-- ============================================================== -->

                                        <!--
                                        <div class="panel panel-default bx-shadow-none">
                                            <div class="panel-heading" id="SSN0">
                                                <h7 class="panel-title">
                                                    <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ssn_0">
                                                        SCO Activity</a>
                                                </h7>
                                            </div>
                                            <div id="ssn_0" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="doc" align="center" id="doc_0"> 
                                                        <div class="table-responsive">
                                                            <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%;">
                                                                <thead>
                                                                    <tr>
                                                                        <th rowspan="2" style="color:#188ae2;text-align:center;">Total Users<br/>&nbsp;</th>
                                                                        <th rowspan="2" style="color:#188ae2;text-align:center;">Users Not Yet Started<br/>&nbsp;</th>
                                                                        <th colspan="3" style="color:#188ae2;text-align:center;">Users In Progress</th>
                                                                        <th rowspan="2" style="color:#188ae2;text-align:center;">Users Completed<br/>&nbsp;</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th style="color:#188ae2;text-align:center;">Users Active</th>
                                                                        <th style="color:#188ae2;text-align:center">Users In-Active<span style="color:#223c80"> *</span></th>
                                                                        <th style="color:#188ae2;text-align:center">Users Date Reached<span style="color:#223c80"> #</span></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody style="font-family:'Open Sans';">
                                                                    <tr>
                                                                        <td align="center"><a href="view_client_report.php?tid=<?php echo $tid; ?>&type=total" style="font-weight:bold;color:black;"><?php echo $total_users; ?></a></td>
                                                                        <td align="center">
                                                                            <a href="view_client_report.php?tid=<?php echo $tid; ?>&type=not_yet_started" style="font-weight:bold;color:#d66a00;" ><?php echo $not_yet_started; ?></a>
                                                                        </td>
                                                                        <td align="center">
                                                                            <a href="view_client_report.php?tid=<?php echo $tid; ?>&type=active" style="font-weight:bold;"><?php echo $cbt_progress; ?></a>
                                                                        </td>
                                                                        <td align="center">
                                                                            <a href="view_client_report.php?tid=<?php echo $tid; ?>&type=in_active" style="font-weight:bold;color:red;"><?php echo $in_active; ?></a>
                                                                        </td>
                                                                        <td align="center">
                                                                            <a href="view_client_report.php?tid=<?php echo $tid; ?>&type=end_date_reached" style="font-weight:bold;color:red;"><?php echo $cbt_end_date_reached; ?></a>
                                                                        </td>
                                                                        <td align="center">
                                                                            <a href="view_client_report.php?tid=<?php echo $tid; ?>&type=finished" style="font-weight:bold;color:#00be29;"><span style="align:center"><?php echo $cbt_finished; ?></span></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div style="text-align: left;margin-top: 2%;">(<span style="color:#223c80;">*</span>)&nbsp;<span style="margin-right:2px;">Not logged-in from past 2 weeks.</span>
                                                            (<span style="color:#223c80">#</span>)&nbsp;<span>CBT end date is reached.</span></div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
!-->

                                        <!-- ============================================================== -->
                                        <!-- Calls Today -->
                                        <!-- ============================================================== -->
                                        <div class="panel panel-default bx-shadow-none">
                                            <div class="panel-heading" id="SSN1">
                                                <h7 class="panel-title">
                                                    <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ssn_1">
                                                        Calls Today</a>
                                                </h7>
                                            </div>
                                            <div id="ssn_1" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="doc" align="center" id="doc_1"> 

                                                        <table class="table m-0" border="0">
                                                            <tbody>
                                                                <?php
                                                                if (isset($uid_today_list)) {//if the uid_today_list are set then proceed
                                                                    for ($i = 0; $i < count($uid_today); $i++) {//counter to pass all uids
                                                                        ?>
                                                                        <tr>
                                                                            <td class="res_td"><a href="view_client.php?uid=<?php echo $uid_today[$i]; ?>"><?php echo $name_today[$i]; ?></a><br/><span style="font-size:12px;"><?php echo $ssn_fullname_today[$i]; ?></span></td>
                                                                            <td class="res_td2">Scheduled at <?php echo $schedule_time[$i]; ?></td>
                                                                            <td>
                                                                                <?php
                                                                                #re-schedule buttton
                                                                                echo '<a><button id="btn_re_schedule_' . $i . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#re-schedule_call_' . $i . '" >Re-Schedule</button></a>';
                                                                                echo '</td>';
                                                                                #mark call as done btn
                                                                                echo '<td ><a><button id="btn_call_done_' . $i . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#call_done_' . $i . '">Mark As Done</button></a>';
                                                                                echo '</td>';
                                                                                #dismissal btn
                                                                                echo '<td class="td_dismiss"><a><button id="btn_dsmiss' . $i . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#display_call_' . $i . '" ></i></button></a>';
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <!--Script to get unique source to open unique modal on click of unique reshecule btn---->
                                                                    <script>
                                                                        $("#btn_re_schedule_<?php echo $i; ?>").click(function () {
                                                                            var iframe = $("#re-schedule_call_frame_<?php echo $i; ?>");//defining id
                                                                            //definig src for iframe
                                                                            iframe.attr("src", "schedule_date.php?uid=<?php echo $uid_today[$i]; ?>&ssn=<?php echo $ssn_today[$i]; ?>");
                                                                        });
                                                                    </script>
                                                                    <!-- Modal to open re-schedule--> 
                                                                    <div id="re-schedule_call_<?php echo $i; ?>" data-keyboard="false" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload()">x</button>
                                                                                    <h4 class="modal-title">You are re-scheduling call for</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <!-- id defines src for iframe which is got from above script-->
                                                                                    <iframe id="re-schedule_call_frame_<?php echo $i; ?>" frameborder="0" scrolling="no"  height="390px" width="100%"></iframe> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--Modal to mark call as done -->
                                                                    <div id='call_done_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="reload()">x</button>
                                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                </div>
                                                                                <div>

                                                                                </div>
                                                                                <div class="modal-body" style="margin-top:3%;">
                                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                        Are you sure you want to mark call as done ?
                                                                                        <div style="text-align:center;margin-top:10%;">
                                                                                            <input type="submit"  name="confirm-call-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                        </div>
                                                                                    </form>
                                                                                    <?php
                                                                                    if (isset($_REQUEST["confirm-call-btn_$i"])) {
                                                                                        //update once the call has been done.Set call_stauts as 1
                                                                                        $stmt500 = $dbh->prepare("UPDATE thrp_scheduled_calls SET call_status=1 WHERE uid=? AND tid=? AND scheduled_date=? AND ssn=? LIMIT 1");
                                                                                        $stmt500->execute(array($uid_today[$i], $tid, $schedule_datetime[$i], $ssn_today[$i]));

                                                                                        echo '<script>window.location.href="index.php";</script>';
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                    </div><!-- /.modal -->
                                                                    <!-- Modal to dissmisal-->
                                                                    <div id='display_call_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="reload()">x</button>
                                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                </div>
                                                                                <div>
                                                                                </div>
                                                                                <div class="modal-body" style="margin-top:3%;">
                                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                        Are you sure you want to dismiss this ?
                                                                                        <div style="text-align:center;margin-top: 3%;">
                                                                                            <input type="submit"  name="confirm-display-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No" />
                                                                                        </div>
                                                                                    </form>
                                                                                    <?php
                                                                                    //to dismiss call just set display as 0
                                                                                    if (isset($_REQUEST["confirm-display-btn_$i"])) {
                                                                                        $stmt600 = $dbh->prepare("UPDATE thrp_scheduled_calls SET display=0 WHERE uid=? AND tid=? AND scheduled_date=? AND ssn=? LIMIT 1");
                                                                                        $stmt600->execute(array($uid_today[$i], $tid, $schedule_datetime[$i], $ssn_today[$i]));

                                                                                        echo '<script>window.location.href="index.php";</script>';
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                    </div><!-- /.modal -->
                                                                    <?php
                                                                }
                                                            } else {//when uid_today is not set
                                                                echo "No client calls today.";
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                        <div class="dropdown pull-right">
                                                            <a href="my_schedule.php">
                                                                <button class="btn btn-primary waves-effect waves-light btn-xs m-b-5" >View All</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>

                                        <!-- ============================================================== -->
                                        <!-- Calls to scheudle -->
                                        <!-- ============================================================== -->
                                        <div class="panel panel-default" >
                                            <div class="panel-heading" id="SSN2">
                                                <h7 class="panel-title">
                                                    <a class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ssn_2">
                                                        Calls To Schedule</a>
                                                </h7>
                                            </div>
                                            <div id="ssn_2" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="doc" align="center" id="doc_2">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <?php
                                                                if (isset($uid_completed_list)) {//if the uid_completed_list are set then proceed
                                                                    for ($j = 0; $j < count($uid_completed); $j++) {//counter for uid completed
                                                                        ?>
                                                                        <tr>
                                                                            <td style="width:35%;"><a href="view_client.php?uid=<?php echo $uid_completed[$j]; ?>"><?php echo $name_completed[$j]; ?></a></td>
                                                                            <td style="width:45%;" ><?php
                                                                                #ssn is CBTSTART display as 'started on' else display as 'completed on'.
                                                                                if ($ssn_completed[$j] == "CBTSTART") {
                                                                                    echo 'Started ' . $ssn_fullname_completed[$j] . ' on<span style="font-size:12px;"> ' . $ssn_endtime_completed[$j] . '</span>';
                                                                                } else {
                                                                                    echo 'Completed ' . $ssn_fullname_completed[$j] . ' on<span style="font-size:12px;"> ' . $ssn_endtime_completed[$j] . '</span>';
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                            <td >
                                                                                <?php
                                                                                #btn to schedule call
                                                                                echo'<a><button id="btn_schedule_' . $j . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#schedule_call_' . $j . '" >Schedule</button></a>';
                                                                                echo'</td>';
                                                                                #btn for dismissal
                                                                                echo'<td class="td_dismiss"><a><button id="btn_dis_schedule_' . $j . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#display_schedule_' . $j . '" ></i></button></a>';
                                                                                ?>
                                                                            </td>
                                                                            <!--Script to get unique source to open unique modal on click of unique shecdule btn---->
                                                                    <script>
                                                                        $("#btn_schedule_<?php echo $j; ?>").click(function () {
                                                                            var iframe = $("#schedule_call_frame_<?php echo $j; ?>");//defining id
                                                                            //defing frame src
                                                                            iframe.attr("src", "schedule_date.php?uid=<?php echo $uid_completed[$j]; ?>&ssn=<?php echo $ssn_completed[$j]; ?>");
                                                                        });
                                                                    </script>
                                                                    <!-- Modal to open schedule call-->
                                                                    <div id="schedule_call_<?php echo $j; ?>" class="modal fade"  data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload()">x</button>
                                                                                    <h4 class="modal-title">You are scheduling call for</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <!-- id defines the src of the frame got from the above script-->
                                                                                    <iframe id="schedule_call_frame_<?php echo $j; ?>" frameborder="0" scrolling="no"  height="390px" width="100%"></iframe> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--Modal for dissmissal -->
                                                                    <div id='display_schedule_<?php echo $j; ?>' class="modal fade bs-example-modal-sm" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="reload()">x</button>
                                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                </div>
                                                                                <div>
                                                                                </div>
                                                                                <div class="modal-body" style="margin-top:3%;">
                                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                        Are you sure you want to dismiss this ?
                                                                                        <div style="text-align:center;margin-top:10%;">
                                                                                            <input type="submit"  name="confirm-schedule-call-btn_<?php echo $j; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                        </div>
                                                                                    </form>
                                                                                    <?php
                                                                                    #to dismiss schedule call,insert into thrp_Scheduled_calls but mark display as 0 a
                                                                                    //also update thrp_user_ssn_completed as 1
                                                                                    if (isset($_REQUEST["confirm-schedule-call-btn_$j"])) {
                                                                                        //insert into thrp_scheduled_calls
                                                                                        $stmt700 = $dbh->prepare("INSERT into thrp_scheduled_calls VALUES ('','$tid','$uid_completed[$j]','$ssn_completed[$j]','','','0','0','') ");
                                                                                        $stmt700->execute(array());
                                                                                        //update thrp_user_Ssn_completed
                                                                                        $stmt70 = $dbh->prepare("UPDATE thrp_user_ssn_completed SET scheduled_status='1' WHERE tid=? AND uid=? AND completed_on=? LIMIT 1");
                                                                                        $stmt70->execute(array($tid, $uid_completed[$j], $completed_on[$j]));
                                                                                        echo '<script>window.location.href="index.php";</script>';
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                    </div><!-- /.modal -->
                                                                    </tr><?php
                                                                }
                                                            } else {
                                                                echo "No clients to schedule.";
                                                            }
                                                            ?>

                                                            </tbody>
                                                        </table>	
                                                        <div class="dropdown pull-right">
                                                            <a href="client_schedule.php">
                                                                <button class="btn btn-primary waves-effect waves-light btn-xs m-b-5" >View All</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- ============================================================== -->
                                        <!-- Reminder to send -->
                                        <!-- ============================================================== -->
                                        <div class="panel panel-default" >
                                            <div class="panel-heading" id="SSN3">
                                                <h7 class="panel-title">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ssn_3">
                                                        Reminder To Send</a>
                                                </h7>
                                            </div>
                                            <div id="ssn_3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="doc" align="center" id="doc_3">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <?php
                                                                if (isset($uid_remindlist)) {//if the uid_remind_list are set then proceed
                                                                    for ($k = 0; $k < count($uid_remind); $k++) {//counter for uid remind
                                                                        ?>
                                                                        <tr>
                                                                            <td class="res_td1"><a href="view_client.php?uid=<?php echo $uid_remind[$k]; ?>"><?php echo $name_remind[$k]; ?></a></td>
                                                                            <td class="notes"><?php echo $ssn_fullname_remind[$k] . ' ' . $text_remind[$k] . ' ' . $days_remind[$k] . ' days.'; ?></td>
                                                                            <td class="td_dismiss">
                                                                                <?php
                                                                                #btn for dismissal
                                                                                echo '<a><button id="btn_' . $k . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#display_remind_' . $k . '" ></i></button></a>';
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- Modal to dismissal-->
                                                                    <div id='display_remind_<?php echo $k; ?>' class="modal fade bs-example-modal-sm" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="reload()">x</button>
                                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                </div>
                                                                                <div>

                                                                                </div>
                                                                                <div class="modal-body" style="margin-top:3%;">
                                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                        Are you sure you want to dismiss this ?
                                                                                        <div style="text-align:center;margin-top:10%;">
                                                                                            <input type="submit"  name="confirm-dismiss-btn_<?php echo $k; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                        </div>
                                                                                    </form>
                                                                                    <?php
                                                                                    if (isset($_REQUEST["confirm-dismiss-btn_$k"])) {
                                                                                        //to dismiss reminder.set display as 0
                                                                                        $stmt800 = $dbh->prepare("UPDATE thrp_remind SET display=0 WHERE uid=? AND tid=? AND ssn=? LIMIT 1");
                                                                                        $stmt800->execute(array($uid_remind[$k], $tid, $ssn_remind[$k]));

                                                                                        echo '<script>window.location.href="index.php";</script>';
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                    </div><!-- /.modal -->
                                                                    <?php
                                                                }
                                                            } else {
                                                                echo "No clients to remind.";
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                        <div class="dropdown pull-right">
                                                            <a href="remind_client.php">
                                                                <button class="btn btn-primary waves-effect waves-light btn-xs m-b-5" >View All</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- ============================================================== -->
                                        <!-- Pending notes -->
                                        <!-- ============================================================== -->
                                        <div class="panel panel-default" >
                                            <div class="panel-heading" id="SSN4">
                                                <h7 class="panel-title">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ssn_4">
                                                        Pending Notes</a>
                                                </h7>
                                            </div>
                                            <div id="ssn_4" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="doc" align="center" id="doc_4">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <?php
                                                                if (isset($uid_pending_list)) {//if the uid_pending_list are set then proceed
                                                                    for ($l = 0; $l < count($uid_pending); $l++) {//counter for pending list uid
                                                                        ?>
                                                                        <tr>
                                                                            <td class="res_td3"><a href="view_client.php?uid=<?php echo $uid_pending[$l]; ?>"><?php echo $name_pending[$l]; ?></a><br/><span style="font-size:12px;"><?php echo $ssn_fullname_pending[$l]; ?></span></td>

                                                                            <td >
                                                                                <?php
                                                                                //to check if the session is cbt start or not,if cbt start then open modal for cbt start,else open take notes
                                                                                if ($ssn_pending[$l] == "CBTSTART") {
                                                                                    echo'<a><button id="btn_case_history_' . $l . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#case_history_' . $l . '" >Case History</button></a>';
                                                                                } else {
                                                                                    #take notes btn
                                                                                    echo'<a><button id="btn_pending_notes_' . $l . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#take_notes_' . $l . '" >Take Notes</button></a>';
                                                                                }
                                                                                echo'</td>';
                                                                                #dismissal btn
                                                                                echo'<td class="td_dismiss"><a><button id="btn_pending_dismiss_' . $l . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#display_pending_' . $l . '" ></i></button></a>';
                                                                                ?>
                                                                            </td>
                                                                            <!--Script to get unique source to open unique modal on click of unique pending notes btn---->
                                                                    <script>
                                                                        $("#btn_pending_notes_<?php echo $l; ?>").click(function () {
                                                                            var iframe = $("#take_notes_frame_<?php echo $l; ?>");//defining id
                                                                            //defining iframe src
                                                                            iframe.attr("src", "take_notes.php?uid=<?php echo $uid_pending[$l]; ?>&ssn=<?php echo $ssn_pending[$l]; ?>");
                                                                        });
                                                                    </script>
                                                                    <div id='take_notes_<?php echo $l; ?>' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-lg">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload()">x</button>
                                                                                    <h4 class="modal-title"><?php echo $name_pending[$l]; ?> - <?php echo $ssn_fullname_pending[$l]; ?></h4>
                                                                                </div>
                                                                                <div class="modal-body" style="padding-top:0%;">
                                                                                    <!-- defining src for iframe from id defined in above script-->
                                                                                    <iframe id="take_notes_frame_<?php echo $l; ?>" frameborder="0" scrolling="no"  height="520px" width="100%"></iframe> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--Modal to open case history--->
                                                                    <div id='case_history_<?php echo $l; ?>' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-lg">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload()"onClick="window.location.reload()">x</button>
                                                                                    <h4 class="modal-title"><?php echo $name_pending[$l]; ?> - Case History</h4>
                                                                                </div>
                                                                                <div class="modal-body" style="padding-top:0%;">
                                                                                    <!-- defining src for iframe from id defined in above script-->
                                                                                    <iframe src="case_history.php?uid=<?php echo $uid_pending[$l]; ?>" frameborder="0" scrolling="no"  height="400px" width="100%"></iframe> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Modal to open dismissal-->
                                                                    <div id='display_pending_<?php echo $l; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                        <div class="modal-dialog modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="window.location.reload()">x</button>
                                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                </div>
                                                                                <div>
                                                                                </div>
                                                                                <div class="modal-body" style="margin-top:3%;">
                                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                        Are you sure you want to dismiss this ?
                                                                                        <div style="text-align:center;margin-top:10%;">

                                                                                            <input type="submit"  name="confirm-calldismiss-btn_<?php echo $l; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                        </div>
                                                                                    </form>
                                                                                    <?php
                                                                                    //on click of dismoss btn, proceed
                                                                                    if (isset($_REQUEST["confirm-calldismiss-btn_$l"])) {
                                                                                        echo $ssn_pending[$l];

                                                                                        //if session is cbtstart, insert into casehistory and mark as 0 
                                                                                        //else insert into take notes and mark as 0
                                                                                        if ($ssn_pending[$l] == "CBTSTART") {
                                                                                            //to dismiss pending notes notfication,insert into thrp_case_history as blank but set dispaly as 0
                                                                                            $stmt901 = $dbh->prepare("INSERT INTO thrp_case_history VALUES (?,?,?,?,?)");
                                                                                            $stmt901->execute(array($tid, $uid_pending[$l], '', '0', '')) or print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1105].");
                                                                                        } else {
                                                                                            //to dismiss pending notes notfication,insert into thrp_notes as blank but set dispaly as 0
                                                                                            $stmt900 = $dbh->prepare("INSERT INTO thrp_notes VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                                                                            $stmt900->execute(array('', $tid, $uid_pending[$l], $cur_datetime, $ssn_pending[$l], '', '', '', '', '', '', '', '', '0', '', '', '')) or print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-DASHBOARD-1106].");
                                                                                        }
                                                                                        echo '<script>window.location.href="index.php";</script>';
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                    </div><!-- /.modal -->
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            } else {
                                                                echo "No pending notes.";
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                        <div class="dropdown pull-right">
                                                            <a href="pending_notes.php">
                                                                <button class="btn btn-primary waves-effect waves-light btn-xs m-b-5" >View All</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>

        </div>
        <script>
            var resizefunc = [];
        </script> 
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/sco/dashboard.php";
            }
        </script>
    </body>
</html>