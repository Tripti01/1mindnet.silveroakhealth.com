<!--this side_bar_clnc is the side bar for clinicians .
the side bar varies according to the thrp_type saved in the session.
this side bar has extra features like create client,send referral link and view details of those links.also license details of the clincians can also be viewed.
Style of side_bar !-->

<style>
    .side_icon{color: #223C80;}
    .side_text{font-size: 14px;color: #223C80;font-weight: normal;font-family:"Open Sans", sans-serif;}
    .ol,ul{padding-left:0px;}
    .red-tooltip + .tooltip > .tooltip-inner {
    background-color: black;
    color:white;
    padding:5px 10px 5px 5px;;
    left: 27px;
    }
    .tooltip.bottom .tooltip-arrow{
    border-bottom-color:black;
    }
</style>
<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu" style="margin-top:-7%;">
        <hr/>
        <ul>

            <!--dashboard-->
            <li>
                <a href="index.php" class="waves-effect"><i class="fa fa-home side_icon" ></i> <span class="side_text" >Dashboard </span> </a>
            </li>

            <!--My clients-->
            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user side_icon "></i> <span class="side_text">Clients </span> <span class="menu-arrow side_icon"></span></a>
                <ul class="list-unstyled">
                    <li><a href="create_client.php" class="waves-effect"><i class=" fa fa-user-plus side_icon"></i> <span class=" side_text"  >Create Client</span></a></li>
                    <hr style="border:0px solid #337ab7;"/>
                    <li style="margin-top:-6%;"><a href="my_clients.php" class="waves-effect"><i class="fa fa-users side_icon"></i> <span class="side_text"  >View Clients</span></a></li>
                </ul>
            </li>

            <!--my referrals-->
            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-share side_icon "></i> <span class=" side_text" > Referrals </span> <span class="menu-arrow side_icon"></span></a>
                <ul class="list-unstyled">
                    <li><a href="create_ref.php" class="waves-effect"><i class=" fa fa-user-plus side_icon"></i><span class=" side_text">Send Referral</span></a></li>
                    <hr style="border:0px solid #337ab7;"/>
                    <li style="margin-top:-6%;"><a href="view_ref.php" class="waves-effect"><i class="fa fa-users side_icon"></i><span class="side_text">View Referrals</span></a></li>
                </ul>
            </li>

            <!--my schedule-->
            <li>
                <a href="my_schedule.php" class="waves-effect"><i class="fa fa-calendar-o side_icon"></i> <span class=" side_text">Schedules </span> </a>
            </li>


        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div></div>
