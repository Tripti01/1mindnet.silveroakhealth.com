<?php
#including functions and files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/coach/get_user_name.php';
include 'functions/coach/get_session_name.php';

# Declaring variables and arrays
$schedule_dates_array = array(); //to store scheduled dates 
# Start the database 
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#query to fetch all the details from thrp_scheduled_calls grouped by scheduled_date and ordered by datetime
$i = 0; // Counter for $schedule_dates_array
$stmt1 = $dbh->prepare("SELECT DISTINCT(DATE(scheduled_date)) AS scheduled_date FROM thrp_scheduled_calls WHERE tid=? AND call_status=? AND display=?  GROUP BY scheduled_date ORDER BY scheduled_date ");
$stmt1->execute(array($tid, 0, 1))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY_SCHEDULE-1000].");
if ($stmt1->rowCount() != 0) {
    while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
        $schedule_dates_array[$i] = date('Y-m-d', strtotime($row1['scheduled_date'])); //to fecth  date part of schedlued_date into array for further calculatons
        $schedule_dates_array_formatted[$i] = date('d-M-Y', strtotime($row1['scheduled_date'])); //formatted date to diaplay
        $i++;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schedule Calls</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .heading{
                color:black;
            }
            .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
                background-color: #9c9c9b;
                border-color: #9c9c9b;
            }
            .btn-primary[disabled]:hover, .btn-primary.disabled:focus, .btn-primary.disabled:active, .btn-primary.active, .btn-primary.focus, .btn-primary.disabled:active, .btn-primary.disabled:focus, .btn-primary.disabled:hover, .open > .dropdown-toggle.btn-primary {
                background-color: #9c9c9b;
            }
            th{
                color:black;
                text-align: left;
            }
            td{
                vertical-align: middle;
                text-align: left;
            }
            .tooltip.bottom {
                margin-left:-8px;
            }
        </style>
    </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="inbox-widget nicescroll" style="height: 600px;overflow-y:auto;">
                                        <table class="table m-0">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Client Name</th>
                                                    <th style="text-align:center;">Notes Status</th>
                                                    <th style="text-align:center;">Call Status</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo '<script src="../assets/js/jquery.min.js"></script>';
                                                $p = 0; //counter to fecth details for the scheduled dates
                                                for ($i = 0; $i < count($schedule_dates_array); $i++) {//i acts as counter for all dates in schedule_dates_Array
                                                    echo '<tr>';
                                                    //to fetch all the details of all the people who have been scheduled
                                                    $stmt2 = $dbh->prepare("SELECT * FROM thrp_scheduled_calls WHERE tid=? AND display=? AND call_status=? AND DATE(scheduled_date)=? ORDER BY scheduled_date");
                                                    $stmt2->execute(array($tid, '1', '0', $schedule_dates_array[$i]));
                                                    if ($stmt2->rowCount() != 0) {
                                                        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                                            $uid_list[$p] = $row2['uid']; //to fetch uid into array
                                                            $ssn[$p] = $row2['ssn']; //to fecth session for further calculations
                                                            $ssn_full_name = get_ssn_fullname($ssn[$p]); //to get session full name
                                                            $schedule_times[$p] = $row2['scheduled_date']; //to fecth the schedule date
                                                            $scheduled_date_time = DATE("h:i A", strtotime($schedule_times[$p])); //to fetch time part of schedule date
                                                            $clientname = get_user_name($uid_list[$p]); //to fecth user name

                                                            echo '<tr>';
                                                            //to display date part
                                                            echo '<td>' . $schedule_dates_array_formatted[$i] . '<br/></td>';

                                                            //to display time part
                                                            echo '<td>';
                                                            echo $scheduled_date_time;
                                                            echo '</td>';

                                                            echo '<td ><a href="view_client.php?uid=' . $uid_list[$p] . '">';
                                                            echo $clientname . '</a><br/>( for ' . $ssn_full_name . ')';
                                                            echo '</td>';

                                                            #to display the notes status
                                                            echo '<td style="text-align:center;">';

                                                            #if session is CBTSTART, then we will check for case history.else check for notes.
                                                            if ($ssn[$p] == "CBTSTART") {
                                                                $stmt411 = $dbh->prepare("SELECT * FROM thrp_case_history WHERE uid=? AND tid=? LIMIT 1");
                                                                $stmt411->execute(array($uid_list[$p], $tid));
                                                                #if casehistory is taken then blue color else grey.and also set notes_status=1 for casehistory taken else 0.
                                                                if ($stmt411->rowCount() != 0) {
                                                                    echo '<font color=#223C80 size=2px><i class="ti-book"></i></font>';
                                                                    $notes_status[$p] = 1;
                                                                } else {
                                                                    echo '<font color=#9e9e9e size=2px><i class="ti-book"></i></font>';
                                                                    $notes_status[$p] = 0;
                                                                }
                                                            } else {
                                                                $stmt401 = $dbh->prepare("SELECT * FROM thrp_notes WHERE uid=? AND tid=? AND ssn=? LIMIT 1");
                                                                $stmt401->execute(array($uid_list[$p], $tid, $ssn[$p]));
                                                                #if notes is taken then blue color else grey.and also set notes_status=1 for notes taken else 0.
                                                                if ($stmt401->rowCount() != 0) {
                                                                    echo '<font color=#223C80 size=4px><i class="fa fa-edit"></i></font>';
                                                                    $notes_status[$p] = 1;
                                                                } else {
                                                                    echo '<font color=#9e9e9e size=4px><i class="fa fa-edit"></i></font>';
                                                                    $notes_status[$p] = 0;
                                                                }
                                                            }
                                                            echo '</td>';

                                                            #to fetch the call status
                                                            echo '<td style="text-align:center;">';
                                                            $stmt412 = $dbh->prepare("SELECT * FROM thrp_scheduled_calls WHERE uid=? AND tid=? AND ssn=? AND call_status=? LIMIT 1");
                                                            $stmt412->execute(array($uid_list[$p], $tid, $ssn[$p], '1')) or print_r($stmt412->errorInfo());
                                                            #if call is taken then blue color else grey.and also set call_status=1 for call done else 0.
                                                            if ($stmt412->rowCount() != 0) {
                                                                echo '<font color=#223C80 size=2px><i class="fa fa-phone" style="margin-left:5px;"></i></font>';
                                                                $call_status[$p] = 1;
                                                            } else {
                                                                echo '<font color=#9e9e9e size=2px><i class="fa fa-phone" style="margin-left:5px;"></i></font>';
                                                                $call_status[$p] = 0;
                                                            }
                                                            echo '</td>';

                                                            #eif call_status is 1, then display re-scheduled btn else display nothing
                                                            echo '<td >';
                                                            if ($call_status[$p] == 0) {
                                                                echo '<a><button id="btn_re_schedule_' . $p . '" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#re_schedule_call_' . $p . '" >Re-Schedule</button></a>';
                                                            } else if ($call_status[$p] == 1) {
                                                                //since call has been done, display nothing
                                                            }
                                                            ?>
                                                            <!--Script to open modal uniquely on click of reschedule btn with unique source code-->
                                                        <script>
                                                            $("#btn_re_schedule_<?php echo $p; ?>").click(function () {
                                                                var iframe = $("#re_schedule_call_frame_<?php echo $p; ?>");//defining iframe id
                                                                //definign src for iframe
                                                                iframe.attr("src", "schedule_date.php?uid=<?php echo $uid_list[$p]; ?>&ssn=<?php echo $ssn[$p]; ?>&page_id=my_schedule");
                                                            });
                                                        </script>
                                                        <!--- This modal open uniquely for re-scheduling of call--->
                                                        <div id="re_schedule_call_<?php echo $p; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload();">x</button>
                                                                        <h4 class="modal-title">Edit Call</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <!--id is used to reterive the src from the above script--->
                                                                        <iframe id="re_schedule_call_frame_<?php echo $p; ?>" frameborder="0" scrolling="no"  height="430px" width="100%"></iframe> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        echo '</td>';

                                                        #the call done button
                                                        echo '<td>';
                                                        //if call has not been done, display 'mark as done' btn else display nothing
                                                        if ($call_status[$p] == 0) {
                                                            echo '<button id="btn_' . $p . '" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#call_done_' . $p . '">Mark As Done</button>';
                                                        } else if ($call_status[$p] == 1) {
                                                            //since call has been done, do nothing
                                                        }
                                                        ?>
                                                        <!--- This modal open uniquely to mark call as done--->
                                                        <div id='call_done_<?php echo $p; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                                        <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                    </div>
                                                                    <div>

                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                            Are you sure you want to mark call as done ?
                                                                            <div style="text-align:center;margin-top:10%;">
                                                                                <input type="submit"  name="confirm-call-btn_<?php echo $p; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                            </div>
                                                                        </form>
                                                                        <?php
                                                                        #on click of 'yes' confirm-call-btn  
                                                                        if (isset($_REQUEST["confirm-call-btn_$p"])) {
                                                                            //to update once the call has been done.set call_status as 1
                                                                            $stmt400 = $dbh->prepare("UPDATE thrp_scheduled_calls SET call_status=1 WHERE uid=? AND tid=? AND scheduled_date=? AND ssn=? LIMIT 1");
                                                                            $stmt400->execute(array($uid_list[$p], $tid, $schedule_times[$p], $ssn[$p])) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-My-SCHEDULE-1001].");

                                                                            echo '<script>window.location.href="my_schedule.php";</script>'; //reload page
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                        <?php
                                                        echo '</td>';

                                                        #if call has not been done, display dismiss btn
                                                        echo '<td >';
                                                        if ($call_status[$p] == 0) {
                                                            echo '<a><button id="btn_' . $p . '" class="btn btn-danger waves-effect waves-light btn-md m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#completed_' . $p . '" ></i></button></a>';
                                                        } else {
                                                            //since call has been done, dont display anything
                                                        }
                                                        ?>
                                                        <!---to display modal when dismiss btn is clicked --->
                                                        <div id='completed_<?php echo $p; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                                        <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                    </div>
                                                                    <div>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                            Are you sure you want to dismiss this ?
                                                                            <div style="text-align:center;margin-top:10%;">
                                                                                <input type="submit"  name="confirm-submit-btn_<?php echo $p; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                <button type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true">NO</button>
                                                                            </div>
                                                                        </form>
                                                                        <?php
                                                                        #on click of 'yes' confirm-submit -btn
                                                                        if (isset($_REQUEST["confirm-submit-btn_$p"])) {
                                                                            //to dismiss call, update display as 0.
                                                                            $stmt402 = $dbh->prepare("UPDATE thrp_scheduled_calls SET display=0 WHERE uid=? AND tid=? AND ssn=? LIMIT 1");
                                                                            $stmt402->execute(array($uid_list[$p], $tid, $ssn[$p])) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY-SCHEDULE-1001].");
                                                                            echo '<script>window.location.href="my_schedule.php";</script>'; //reload
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                        <?php
                                                        $p++;
                                                    }
                                                }
                                                echo '</td></tr>';
                                            } //ending for loop for schedule_date
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/sco/my_schedule.php";
                    }
        </script>
    </body>
</html>



