<?php
#including functions and files
include 'if_loggedin.php';
include 'host.php';
include 'soh-config.php';

$gad_first_limit = [0, 6, 11, 16];
$gad_last_limit = [5, 10, 15, 21];

$phq_first_limit = [0, 5, 10, 15, 20];
$phq_last_limit = [4, 9, 14, 19, 27];


$count_prea_gad_first_bucket = 0;
$count_prea_gad_second_bucket = 0;
$count_prea_gad_third_bucket = 0;
$count_prea_gad_fourth_bucket = 0;

$count_prea_phq_first_bucket = 0;
$count_prea_phq_second_bucket = 0;
$count_prea_phq_third_bucket = 0;
$count_prea_phq_fourth_bucket = 0;
$count_prea_phq_fifth_bucket = 0;

$count_prea_gad_first_bucket_thrp = 0;
$count_prea_gad_second_bucket_thrp = 0;
$count_prea_gad_third_bucket_thrp = 0;
$count_prea_gad_fourth_bucket_thrp = 0;

$count_prea_phq_first_bucket_thrp = 0;
$count_prea_phq_second_bucket_thrp = 0;
$count_prea_phq_third_bucket_thrp = 0;
$count_prea_phq_fourth_bucket_thrp = 0;
$count_prea_phq_fifth_bucket_thrp = 0;

# Start the database 
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#query to fetch all the details from thrp_scheduled_calls grouped by scheduled_date and ordered by datetime
$ssn = ['SSN1', 'SSN2', 'SSN3', 'SSN4', 'SSN5', 'SSN6', 'SSN7', 'SSN8'];
for ($i = 0; $i < count($ssn); $i++) {
    $stmt1 = $dbh->prepare("select sum(total) as sum_total,count(total) AS total from user_mood_survey,asmt_score WHERE user_mood_survey.uid = asmt_score.uid and asmt_score.type='POST' and user_mood_survey.ssn=?");
    $stmt1->execute(array($ssn[$i]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-1000].");
    if ($stmt1->rowCount() != 0) {
        $row1 = $stmt1->fetch();
        $total = $row1['total'];
        $avg = intval($row1['sum_total'] / $row1['total']); // avg in scale of 4-40
        $avg_ssn = round(($avg - 4) * 2.78);
        $avg_scores[$i] = $avg_ssn;
    }

    $stmt2 = $dbh->prepare("select sum(total) as sum_total,count(total) AS total from user_mood_survey,asmt_score,user_therapist WHERE user_mood_survey.uid = asmt_score.uid AND user_mood_survey.uid = user_therapist.uid and asmt_score.type='POST' and user_mood_survey.ssn=? and user_therapist.tid = ?");
    $stmt2->execute(array($ssn[$i], $tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-1001].");
    if ($stmt2->rowCount() != 0) {
        $row2 = $stmt2->fetch();
        $thrp_total = $row2['total'];
		if( $thrp_total != 0){
			$avg = intval($row2['sum_total'] / $row2['total']); // avg in scale of 4-40
			$avg_ssn = round(($avg - 4) * 2.78);
			$avg_scores_thrp[$i] = $avg_ssn;
		}else{
			$avg_scores_thrp[$i] = 0;
		}
    }else{
		$avg_scores_thrp[$i] = 0;
		
	}
}
#---------------------------------------- Before Stress Control online ----------------------------------------#
$stmt13 = $dbh->prepare("SELECT uid FROM asmt_score WHERE type='POST'");
$stmt13->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-101].");
if ($stmt13->rowCount() != 0) {
    while ($row13 = $stmt13->fetch(PDO::FETCH_ASSOC)) {
        $uid = $row13['uid'];
        $stmt11 = $dbh->prepare("SELECT score_gad, score_phq FROM asmt_score WHERE uid=? AND type= ?");
        $stmt11->execute(array($uid, 'PREA'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-101].");
        if ($stmt11->rowCount() != 0) {
            $row11 = $stmt11->fetch();
            $score_gad = $row11['score_gad'];
            $score_phq = $row11['score_phq'];

            if ($score_gad >= $gad_first_limit[0] and $score_gad <= $gad_last_limit[0]) {
                $count_prea_gad_first_bucket = $count_prea_gad_first_bucket + 1;
            } else if ($score_gad >= $gad_first_limit[1] and $score_gad <= $gad_last_limit[1]) {
                $count_prea_gad_second_bucket = $count_prea_gad_second_bucket + 1;
            } else if ($score_gad >= $gad_first_limit[2] and $score_gad <= $gad_last_limit[2]) {
                $count_prea_gad_third_bucket = $count_prea_gad_third_bucket + 1;
            } else if ($score_gad >= $gad_first_limit[3] and $score_gad <= $gad_last_limit[3]) {
                $count_prea_gad_fourth_bucket = $count_prea_gad_fourth_bucket + 1;
            }

            if ($score_phq >= $phq_first_limit[0] and $score_phq <= $phq_last_limit[0]) {
                $count_prea_phq_first_bucket = $count_prea_phq_first_bucket + 1;
            } else if ($score_phq >= $phq_first_limit[1] and $score_phq <= $phq_last_limit[1]) {
                $count_prea_phq_second_bucket = $count_prea_phq_second_bucket + 1;
            } else if ($score_phq >= $phq_first_limit[2] and $score_phq <= $phq_last_limit[2]) {
                $count_prea_phq_third_bucket = $count_prea_phq_third_bucket + 1;
            } else if ($score_phq >= $phq_first_limit[3] and $score_phq <= $phq_last_limit[3]) {
                $count_prea_phq_fourth_bucket = $count_prea_phq_fourth_bucket + 1;
            } else if ($score_phq >= $phq_first_limit[4] and $score_phq <= $phq_last_limit[4]) {
                $count_prea_phq_fifth_bucket = $count_prea_phq_fifth_bucket + 1;
            }
        }
         $perc_pre_gad_first_bucket = ($count_prea_gad_first_bucket * 100) / $total;
        $perc_pre_gad_second_bucket = ($count_prea_gad_second_bucket * 100) / $total;
        $perc_pre_gad_third_bucket = ($count_prea_gad_third_bucket * 100) / $total;
        $perc_pre_gad_fourth_bucket = ($count_prea_gad_fourth_bucket * 100) / $total;

        $perc_pre_phq_first_bucket = ($count_prea_phq_first_bucket * 100) / $total;
        $perc_pre_phq_second_bucket = ($count_prea_phq_second_bucket * 100) / $total;
        $perc_pre_phq_third_bucket = ($count_prea_phq_third_bucket * 100) / $total;
        $perc_pre_phq_fourth_bucket = ($count_prea_phq_fourth_bucket * 100) / $total;
        $perc_pre_phq_fifth_bucket = ($count_prea_phq_fifth_bucket * 100) / $total;



        $stmt12 = $dbh->prepare("SELECT asmt_score.score_gad, asmt_score.score_phq FROM asmt_score,user_therapist WHERE user_therapist.uid=asmt_score.uid AND asmt_score.uid= ? AND asmt_score.type= ? AND user_therapist.tid = ?");
        $stmt12->execute(array($uid, 'PREA', $tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-101].");
        if ($stmt12->rowCount() != 0) {
            $row12 = $stmt12->fetch();
            $score_gad = $row12['score_gad'];
            $score_phq = $row12['score_phq'];

            if ($score_gad >= $gad_first_limit[0] and $score_gad <= $gad_last_limit[0]) {
                $count_prea_gad_first_bucket_thrp = $count_prea_gad_first_bucket_thrp + 1;
            } else if ($score_gad >= $gad_first_limit[1] and $score_gad <= $gad_last_limit[1]) {
                $count_prea_gad_second_bucket_thrp = $count_prea_gad_second_bucket_thrp + 1;
            } else if ($score_gad >= $gad_first_limit[2] and $score_gad <= $gad_last_limit[2]) {
                $count_prea_gad_third_bucket_thrp = $count_prea_gad_third_bucket_thrp + 1;
            } else if ($score_gad >= $gad_first_limit[3] and $score_gad <= $gad_last_limit[3]) {
                $count_prea_gad_fourth_bucket_thrp = $count_prea_gad_fourth_bucket_thrp + 1;
            }

            if ($score_phq >= $phq_first_limit[0] and $score_phq <= $phq_last_limit[0]) {
                $count_prea_phq_first_bucket_thrp = $count_prea_phq_first_bucket_thrp + 1;
            } else if ($score_phq >= $phq_first_limit[1] and $score_phq <= $phq_last_limit[1]) {
                $count_prea_phq_second_bucket_thrp = $count_prea_phq_second_bucket_thrp + 1;
            } else if ($score_phq >= $phq_first_limit[2] and $score_phq <= $phq_last_limit[2]) {
                $count_prea_phq_third_bucket_thrp = $count_prea_phq_third_bucket_thrp + 1;
            } else if ($score_phq >= $phq_first_limit[3] and $score_phq <= $phq_last_limit[3]) {
                $count_prea_phq_fourth_bucket_thrp = $count_prea_phq_fourth_bucket_thrp + 1;
            } else if ($score_phq >= $phq_first_limit[4] and $score_phq <= $phq_last_limit[4]) {
                $count_prea_phq_fifth_bucket_thrp = $count_prea_phq_fifth_bucket_thrp + 1;
            }
        }else{
			$count_prea_gad_first_bucket_thrp = 0;
			$count_prea_gad_second_bucket_thrp = 0;
			$count_prea_gad_third_bucket_thrp = 0;
			$count_prea_gad_fourth_bucket_thrp = 0;

			$count_prea_phq_first_bucket_thrp = 0;
			$count_prea_phq_second_bucket_thrp = 0;
			$count_prea_phq_third_bucket_thrp = 0;
			$count_prea_phq_fourth_bucket_thrp = 0;
			$count_prea_phq_fifth_bucket_thrp = 0;
		}


        $perc_pre_gad_first_bucket_thrp = ($count_prea_gad_first_bucket_thrp * 100) / $total;
        $perc_pre_gad_second_bucket_thrp = ($count_prea_gad_second_bucket_thrp * 100) / $total;
        $perc_pre_gad_third_bucket_thrp = ($count_prea_gad_third_bucket_thrp * 100) / $total;
        $perc_pre_gad_fourth_bucket_thrp = ($count_prea_gad_fourth_bucket_thrp * 100) / $total;

        $perc_pre_phq_first_bucket_thrp = ($count_prea_phq_first_bucket_thrp * 100) / $total;
        $perc_pre_phq_second_bucket_thrp = ($count_prea_phq_second_bucket_thrp * 100) / $total;
        $perc_pre_phq_third_bucket_thrp = ($count_prea_phq_third_bucket_thrp * 100) / $total;
        $perc_pre_phq_fourth_bucket_thrp = ($count_prea_phq_fourth_bucket_thrp * 100) / $total;
        $perc_pre_phq_fifth_bucket_thrp = ($count_prea_phq_fifth_bucket_thrp * 100) / $total;
    }
}

#---------------------------------------- After Stress Control online -----------------------------------------#
for ($i = 0; $i < count($gad_first_limit); $i++) {
    $stmt3 = $dbh->prepare("SELECT count(score_gad) AS count_user from asmt_score Where type = 'POST' and score_gad >= ? and score_gad <= ? ");
    $stmt3->execute(array($gad_first_limit[$i], $gad_last_limit[$i]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-101].");
    if ($stmt3->rowCount() != 0) {
        $row3 = $stmt3->fetch();
        $count_user = $row3['count_user'];
        $after_sco_gad_perc[$i] = intval(($count_user * 100) / $total);
    }
    $stmt6 = $dbh->prepare("SELECT count(asmt_score.uid) AS count_user from asmt_score,user_therapist Where asmt_score.type = 'POST' and asmt_score.score_gad >= ? and asmt_score.score_gad <= ?  AND user_therapist.uid = asmt_score.uid and user_therapist.tid=?");
    $stmt6->execute(array($gad_first_limit[$i], $gad_last_limit[$i], $tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-102].");
    if ($stmt6->rowCount() != 0) {
        $row6 = $stmt6->fetch();
		if ($row6['count_user'] !=0){
			$count_user = $row6['count_user'];
			$after_sco_gad_perc_thrp[$i] = intval(($count_user * 100) / $thrp_total);
		}else{
			$after_sco_gad_perc_thrp[$i] = 0;
		}
    }else{
		$after_sco_gad_perc_thrp[$i] = 0;
	}
}
for ($i = 0; $i < count($phq_first_limit); $i++) {
    $stmt4 = $dbh->prepare("SELECT count(score_phq) AS count_user from asmt_score Where type = 'POST' and score_phq >= ? and score_phq <= ? ");
    $stmt4->execute(array($phq_first_limit[$i], $phq_last_limit[$i]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-103].");
    if ($stmt4->rowCount() != 0) {
        $row4 = $stmt4->fetch();
        $count_user = $row4['count_user'];
        $after_sco_phq_perc[$i] = intval(($count_user * 100) / $total);
    }
    $stmt8 = $dbh->prepare("SELECT count(asmt_score.uid) AS count_user from asmt_score,user_therapist Where asmt_score.type = 'POST' and asmt_score.score_phq >= ? and asmt_score.score_phq <= ?  AND user_therapist.uid = asmt_score.uid and user_therapist.tid=? ");
    $stmt8->execute(array($phq_first_limit[$i], $phq_last_limit[$i], $tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-USER-ASMT-104].");
    if ($stmt8->rowCount() != 0) {
        $row8 = $stmt8->fetch();
		if ($row8['count_user'] !=0){
			$count_user = $row8['count_user'];
			$after_sco_phq_perc_thrp[$i] = intval(($count_user * 100) / $thrp_total);
		}else{
			$after_sco_phq_perc_thrp[$i] = 0;
		}
    }else{
		$after_sco_phq_perc_thrp[$i] = 0;
	}
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>StressControlOnline Assessment Report</title>

        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <link href="../assets/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/users_feedback.css"  rel="stylesheet">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">	
        <style>
            td{
                text-align:center;
            }
            th{
                text-align:center;
            }
        </style>
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view tde page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{% static 'js/modernizr.min.js' %}"></script>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper" >

            <!-- Top Bar Start -->
            <div class="topbar"  style="border-top:0;background-color:#dee1e3; border-bottom:0px solid #223c80;" >
                <div class="row">
                    <div class = "col-lg-1"></div>
                    <div class = "col-lg-10">
                        <div class="row">
                            <div class = "col-lg-2 col-xs-4">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" height="60" widtd="150" style="margin-top:10px;margin-left:10px;margin-bottom:10px;"> 
                            </div>
                            <div class = "col-lg-8 col-xs-8">
                                <div class="title" style="border:0px solid blue;padding:5px 0px 10px 0px;color:#615d5d;">
                                    <center><b>StressControlOnline Assessment Report</b></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Top Bar End -->

            <div class="content-page" style="margin-left:0px;">
                <div class="content">
                    <div class="container">
                        <div class = "row">
                            <div class = "col-lg-1"></div>
                            <div class = "col-lg-10">
                                <div class="row" style="margin-top:30px; ">
                                    <div class="col-md-12">
                                        <div id="chart_div"></div>
                                    </div>									
                                </div>
                                <hr class="style18"></hr>
                                <div class="row">
                                    <div style="margin-bottom:10px;margin-left:8px; font-size:18px; color:#223c80;">
                                        Total Users before Stress Control Online Program
                                    </div>
                                    <div class="col-md-6">
                                        <table width="100%"  border="1">
                                            <tr>
                                                <th colspan="4" align="center" style="color:#1C5D99"> GAD</th>
                                            </tr>
                                            <tr>
                                                <td>Minimum Anxiety</td>
                                                <td>Mild Anxiety</td>
                                                <td>Moderate Anxiety</td>
                                                <td>Severe Anxiety</td>
                                            </tr>
                                            <tr>
                                                <td id='pre_gad1'><?php echo intval($perc_pre_gad_first_bucket); ?>%</td>
                                                <td id='pre_gad2'><?php echo intval($perc_pre_gad_second_bucket); ?>%</td>
                                                <td id='pre_gad3'><?php echo intval($perc_pre_gad_third_bucket); ?>%</td>
                                                <td id='pre_gad4'><?php echo intval($perc_pre_gad_fourth_bucket); ?>%</td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-md-6 verticalLine">
                                        <table width="100%" border="1">
                                            <tr>
                                                <th colspan="5" align="center" style="color:#1C5D99">PHQ</th>
                                            </tr>
                                            <tr>
                                                <td>None / Minimal</td>
                                                <td>Mild</td>
                                                <td>Moderate</td>
                                                <td>Moderately Severe</td>
                                                <td>Severe</td>
                                            </tr>
                                            <tr>
                                                <td id='pre_phq1'><?php echo intval($perc_pre_phq_first_bucket) ?>%</td>
                                                <td id='pre_phq2'><?php echo intval($perc_pre_phq_second_bucket); ?>%</td>
                                                <td id='pre_phq3'><?php echo intval($perc_pre_phq_third_bucket); ?>%</td>
                                                <td id='pre_phq4'><?php echo intval($perc_pre_phq_fourth_bucket); ?>%</td>
                                                <td id='pre_phq5'><?php echo intval($perc_pre_phq_fifth_bucket); ?>%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br/>

                                <div class="row">
                                    <div style="margin-bottom:10px;margin-left:8px; font-size:18px; color:#223c80;">
                                        Total Users after Stress Control Online Program
                                    </div>
                                    <div class="col-md-6">
                                        <table width="100%"  border="1">
                                            <tr>
                                                <th colspan="4" align="center" style="color:#1C5D99"> GAD</th>
                                            </tr>
                                            <tr>
                                                <td>Minimum Anxiety</td>
                                                <td>Mild Anxiety</td>
                                                <td>Moderate Anxiety</td>
                                                <td>Severe Anxiety</td>
                                            </tr>
                                            <tr>
                                                <td id='post_gad1'><?php echo $after_sco_gad_perc[0]; ?>%</td>
                                                <td id='post_gad2'><?php echo $after_sco_gad_perc[1]; ?>%</td>
                                                <td id='post_gad3'><?php echo $after_sco_gad_perc[2]; ?>%</td>
                                                <td id='post_gad4'><?php echo $after_sco_gad_perc[3]; ?>%</td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-md-6 verticalLine">
                                        <table width="100%" border="1">
                                            <tr>
                                                <th colspan="5" align="center" style="color:#1C5D99">PHQ</th>
                                            </tr>
                                            <tr>
                                                <td>None / Minimal</td>
                                                <td>Mild</td>
                                                <td>Moderate</td>
                                                <td>Moderately Severe</td>
                                                <td>Severe</td>
                                            </tr>
                                            <tr>
                                                <td id='post_phq1'><?php echo $after_sco_phq_perc[0]; ?>%</td>
                                                <td id='post_phq2'><?php echo $after_sco_phq_perc[1]; ?>%</td>
                                                <td id='post_gad3'><?php echo $after_sco_phq_perc[2]; ?>%</td>
                                                <td id='post_gad4'><?php echo $after_sco_phq_perc[3]; ?>%</td>
                                                <td id='post_phq5'><?php echo $after_sco_phq_perc[4]; ?>%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br/>
                                <br/><br/>
                                <hr class="style18"></hr>
								<div class="row" style="margin-top:30px;">
                                    <div style="margin-bottom:10px;margin-left:8px; font-size:18px; color:#223c80;">
                                        Yours Users before Stress Control Online Program
                                    </div>
                                    <div class="col-md-6">										
                                        <table width="100%" border="1">
                                            <tr>
                                                <th colspan="4" align="center" style="color:#1C5D99;">GAD</th>
                                            </tr>
                                            <tr>
                                                <td>Minimum Anxiety</td>
                                                <td>Mild Anxiety</td>
                                                <td>Moderate Anxiety</td>
                                                <td>Severe Anxiety</td>
                                            </tr>
                                            <tr>

                                                <td id='pre_gad1'><?php echo intval($perc_pre_gad_first_bucket_thrp); ?>%</td>
                                                <td id='pre_gad2'><?php echo intval($perc_pre_gad_second_bucket_thrp); ?>%</td>
                                                <td id='pre_gad3'><?php echo intval($perc_pre_gad_third_bucket_thrp); ?>%</td>
                                                <td id='pre_gad4'><?php echo intval($perc_pre_gad_fourth_bucket_thrp); ?>%</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6 verticalLine">
                                        <table width="100%" border="1">										
                                            <tr>
                                                <th colspan="5" align="center" style="color:#1C5D99;">PHQ</th>
                                            </tr>
                                            <tr>
                                                <td>None / Minimal</td>
                                                <td>Mild</td>
                                                <td>Moderate</td>
                                                <td>Moderately Severe</td>
                                                <td>Severe</td>
                                            </tr>
                                            <tr>
                                                <td id='pre_phq1'><?php echo intval($perc_pre_phq_first_bucket_thrp) ?>%</td>
                                                <td id='pre_phq2'><?php echo intval($perc_pre_phq_second_bucket_thrp); ?>%</td>
                                                <td id='pre_phq3'><?php echo intval($perc_pre_phq_third_bucket_thrp); ?>%</td>
                                                <td id='pre_phq4'><?php echo intval($perc_pre_phq_fourth_bucket_thrp); ?>%</td>
                                                <td id='pre_phq5'><?php echo intval($perc_pre_phq_fifth_bucket_thrp); ?>%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:30px;">
                                    <div style="margin-bottom:10px;margin-left:8px; font-size:18px; color:#223c80;">
                                        Yours Users after Stress Control Online Program
                                    </div>
                                    <div class="col-md-6">										
                                        <table width="100%" border="1">
                                            <tr>
                                                <th colspan="4" align="center" style="color:#1C5D99;">GAD</th>
                                            </tr>
                                            <tr>
                                                <td>Minimum Anxiety</td>
                                                <td>Mild Anxiety</td>
                                                <td>Moderate Anxiety</td>
                                                <td>Severe Anxiety</td>
                                            </tr>
                                            <tr>
                                                <td id='post_gad1'><?php echo $after_sco_gad_perc_thrp[0]; ?>%</td>
                                                <td id='post_gad2'><?php echo $after_sco_gad_perc_thrp[1]; ?>%</td>
                                                <td id='post_gad3'><?php echo $after_sco_gad_perc_thrp[2]; ?>%</td>
                                                <td id='post_gad4'><?php echo $after_sco_gad_perc_thrp[3]; ?>%</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6 verticalLine">
                                        <table width="100%" border="1">										
                                            <tr>
                                                <th colspan="5" align="center" style="color:#1C5D99;">PHQ</th>
                                            </tr>
                                            <tr>
                                                <td>None / Minimal</td>
                                                <td>Mild</td>
                                                <td>Moderate</td>
                                                <td>Moderately Severe</td>
                                                <td>Severe</td>
                                            </tr>
                                            <tr>
                                                <td id='post_phq1'><?php echo $after_sco_phq_perc_thrp[0]; ?>%</td>
                                                <td id='post_phq2'><?php echo $after_sco_phq_perc_thrp[1]; ?>%</td>
                                                <td id='post_gad3'><?php echo $after_sco_phq_perc_thrp[2]; ?>%</td>
                                                <td id='post_gad4'><?php echo $after_sco_phq_perc_thrp[3]; ?>%</td>
                                                <td id='post_phq5'><?php echo $after_sco_phq_perc_thrp[4]; ?>%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br/><br/>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer" style="left:0;background-color:white">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <script type="text/javascript">
                                        var dt = new Date();
                                        document.write(dt.getFullYear());
									</script> © Silver Oak Health.
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <div style="float:right;">Total number of reponses: (Total Users) = <?php echo $total; ?>, 
									 (Yours Users) = <?php echo $thrp_total; ?>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

            <script src="../assets/js/jquery.min.js"></script>
            <script src="../assets/js/bootstrap.min.js"></script>
            <script src="../assets/js/detect.js"></script>
            <script src="../assets/js/jquery.slimscroll.js"></script>
            <script src="../assets/js/waves.js"></script>
            <script src="../assets/js/jquery.nicescroll.js"></script>
            <script src="../assets/js/jquery.app.js"></script>
            <script src="../assets/js/jquery.core.js"></script>

            <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
            <script>
                                        var avg_scores = <?php echo json_encode($avg_scores); ?>;
                                        var avg_scores_thrp = <?php echo json_encode($avg_scores_thrp); ?>;
            </script>
            <script>
                google.load('visualization', '1', {packages: ['corechart', 'line']});
                google.setOnLoadCallback(drawChart1);
                function drawChart1() {
                    var data = google.visualization.arrayToDataTable([
                        ['x', '$score', 'Thrp User $score'],
                        [1, avg_scores[0], avg_scores_thrp[0]],
                        [2, avg_scores[1], avg_scores_thrp[1]],
                        [3, avg_scores[2], avg_scores_thrp[2]],
                        [4, avg_scores[3], avg_scores_thrp[3]],
                        [5, avg_scores[4], avg_scores_thrp[4]],
                        [6, avg_scores[5], avg_scores_thrp[5]],
                        [7, avg_scores[6], avg_scores_thrp[6]],
                        [8, avg_scores[7], avg_scores_thrp[7]],
                    ]);
                    var options = {
                        title: 'Mood Survey Report',
                        fontName: 'Open Sans',
                        titlePosition: 'right',
                        legend: 'none',
                        // curveType: 'function',
                        lineWidtd: 2,
                        height: 430,
                        series: {
                            colors: ['#1C5D99', '#FFC332', '#E06666', '#FFC332', '#6AA84F', '#3BB9B9', '#83221e', '#674ea7'],
                        },
                        hAxis: {
                            title: 'Session',
                            ticks: [1, 2, 3, 4, 5, 6, 7, 8],
                            gridlines: {color: 'transparent'},
                            viewWindow: {
                                max: 8,
                                min: 0
                            }
                        },
                        vAxis: {
                            title: 'Mood Scale',
                            ticks: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                            gridlines: {color: 'transparent'},
                            viewWindow: {
                                max: 103,
                                min: 0
                            }
                        }
                    };
                    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }

                $(window).resize(function() {
                    drawChart1();
                });
            </script>
    </body>
</html>