<?php
//including files and functions

ini_set('max_execution_time', 300);
include "soh-config.php";
require '../if_loggedin.php';
include 'host.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/get_user_name.php';
include 'coach_function/get_user_count.php';
include 'functions/coach/get_user_status_page.php';
require_once 'coach_function/is_active_count.php';

//to fetch tid from url
//if tid isset then fetch tid else set as blank
if (isset($_REQUEST['tid'])) {
    $tid = $_REQUEST['tid'];
} else {
    $tid = "";
}
//to fetch type from url
//if type is set then fetch type else set as type
if (isset($_REQUEST['type'])) {
    $type = $_REQUEST['type'];
} else {
    $type = "";
}

//declaring variables for as foloowing
$k = 0; //cbt not yet started
$l = 0; //cbt in progress
$m = 0; //cbt finished
$n = 0; //cbt end date reached
$j = 0; //counter for all uids
$o = 0; //counter for inactive clients
# Start the database 
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//to fetch all uids assigned to therpist
$stmt100 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=? ORDER BY uid;");
$stmt100->execute(array($tid));
if ($stmt100->rowCount() != 0) {
    while ($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)) {
        $uid[$j] = $row100['uid'];

        //function to get status of the clients assigned
        $return_value = get_user_count($uid[$j]);

        if ($return_value == 1) {//not yet started cbt
            $uid_not_yet_started[$k] = $uid[$j];
            $k++;
        } else if ($return_value == 2) {//cbt progress//active
            $uid_active[$l] = $uid[$j];
            $l++;
        } else if ($return_value == 3) {//cbt finished
            $uid_cbt_finished[$m] = $uid[$j];
            $m++;
        } else if ($return_value == 4) {//cbt end date reached
            $uid_cbt_end_date_reached[$n] = $uid[$j];
            $n++;
        } else if ($return_value == 5) {//inactive
            $uid_in_active[$o] = $uid[$j];
            $o++;
        }

        $j++;
    }
}
//if uid is set then proceed,else do nothing
if (isset($uid)) {
//to fetch details of users who have completed
    $l = 0;
    for ($k = 0; $k < count($uid); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $uid_ssn_total[$k]) = get_user_status($uid[$k]);
        //to fetch user creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE  user_schedule.uid=user_time_creation.uid  AND user_time_creation.uid =? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation datetime for all uids
                $due_datetime[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime for all uids
                //to check if the user has Done prea and post assessment or not
                $stmt300 = $dbh->prepare("SELECT type FROM asmt_score WHERE uid=? LIMIT 2");
                $stmt300->execute(array($uid[$k]));
                $row_count = $stmt300->rowCount(); //to fetch rowcount
                //if rowcount is 0, then user has not started cbt
                if ($row_count == 0) {
                    $status_total[$l] = 1;
                } else if ($row_count == 1) {//if rowcount is 1,then user has Done pre-assessemnt , hence user is in progress,then proceed
                    $active = is_active_count($uid[$k]); //to check if user is active or not
                    if ($active == 1) {//user is active
                        $status_total[$l] = 2;
                    } else {
                        $status_total[$l] = 3; //cbt end date reacched
                    }
                } else if ($row_count == 2) {//if rowcount is 2, then both pre and post assessement has been Done, hence user has finished cbt
                    $status_total[$l] = 4;
                }

                $l++;
            }
        }
    }
} else {
    //do nothing, since uid is not set
}

//if uid_not_yet_started is set then proceed,else do nothing
if (isset($uid_not_yet_started)) {
//to fetch details of users who have not yet started
    $l = 0;
    for ($k = 0; $k < count($uid_not_yet_started); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_not_yet_started[$k]) = get_user_status($uid_not_yet_started[$k]);
        //to fetch user creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE  user_schedule.uid=user_time_creation.uid  AND user_time_creation.uid =? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_not_yet_started[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_not_yet_started[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation time for uid not yet started cbt
                $due_datetime_not_yet_started[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch end date for uid not yet started cbt
                $l++;
            }
        }
    }
} else {
    //do nothing,since uid_not_yet_started is not set
}

//if uid_cbt_progress is set then proceed,else do nothing
if (isset($uid_active)) {
//to fetch details of users who are in progress
    $l = 0;
    for ($k = 0; $k < count($uid_active); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_active[$k]) = get_user_status($uid_active[$k]);
        //to fetch user creation datetime, due datetime and asmt_score(to check if pre assessment has been done or not)
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time, asmt_score.type AS type ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, asmt_score , user_schedule WHERE asmt_score.uid = user_schedule.uid AND user_time_creation.uid = asmt_score.uid AND asmt_score.uid=? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_active[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_active[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation time
                $asmt_score_active[$l] = $row200['type']; //to fetch pre assesmnet,if value exsists pre assessmnet has been done
                $due_datetime_active[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothinmg, since uid_cbt_progress is not set
}

//if uid_cbt_finished is set then proceed,else do nothing
if (isset($uid_cbt_finished)) {
//to fetch details of users who have completed
    $l = 0;
    for ($k = 0; $k < count($uid_cbt_finished); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_finished[$k]) = get_user_status($uid_cbt_finished[$k]);
        //to fetch creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE  user_schedule.uid=user_time_creation.uid  AND user_time_creation.uid =? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_cbt_finished[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_finished[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation time
                $due_datetime_finished[$l] = date('d-M-Y h:i A', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothing,since uid_cbt_finished is not set
}

//if uid_cbt_end_date_reached is set then proceed,else do nothing
if (isset($uid_cbt_end_date_reached)) {
//to fetch details of users whose end date has been reached
    $l = 0;
    for ($k = 0; $k < count($uid_cbt_end_date_reached); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_reached[$k]) = get_user_status($uid_cbt_end_date_reached[$k]);
        //to fetch creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time, user_schedule.due_datetime AS due_datetime FROM user_time_creation, asmt_score , user_schedule WHERE user_time_creation.uid = user_schedule.uid AND user_time_creation.uid = ? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_cbt_end_date_reached[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_end_date_reached[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation datetime
                $due_datetime_end_date_reached[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothin, since uid_cbt_end_date_Reached
}

//if uid_in_active is set then proceed,else do nothing
if (isset($uid_in_active)) {
//to fetch details of users whose end date has been reached
    $l = 0;
    for ($k = 0; $k < count($uid_in_active); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_in_active[$k]) = get_user_status($uid_in_active[$k]);
        //to fetch creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time, asmt_score.type AS type ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, asmt_score , user_schedule WHERE asmt_score.uid = user_schedule.uid AND user_time_creation.uid = asmt_score.uid AND asmt_score.uid=? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_in_active[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_in_active[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation datetime
                $asmt_score_in_active[$l] = $row200['type']; //to fetch pre assesmnet,if value exsists pre assessmnet has been Done
                $due_datetime_in_active[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothin, since uid_in_Active is not set
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Report</title>
        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <style>
            .dataTables_length{
                display: none;
            }
            .dataTables_filter{
                display: none;
            }
            .dataTables_info{
                color:#C0C0C0;
            }
            .title_color{
                color:#223C80;
                font-size: 35px;
                margin-bottom: 20px;
                padding-top:15px;
            }
            .table-striped > tbody > tr:nth-of-type(odd), .table-hover > tbody > tr:hover, .table > thead > tr > td.active, .table > tbody > tr > td.active, .table > tfoot > tr > td.active, .table > thead > tr > th.active, .table > tbody > tr > th.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > tbody > tr.active > td, .table > tfoot > tr.active > td, .table > thead > tr.active > th, .table > tbody > tr.active > th, .table > tfoot > tr.active > th {
                background-color: #fff !important;
            }
            .dataTables_info{
                display: none;
            }
        </style>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
    </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="card-box">
                            <article style="padding-left: 10px;padding-right: 10px;">
                                <h5 class="title">
                                    Reports
                                </h5>

                                <div class="panel-body">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th># <br/> &nbsp;</th>
                                                <th>UID <br/> &nbsp;</th>
                                                <th>Client Name<br/> &nbsp; </th>
                                                <th>Creation time<br/> &nbsp; </th>
                                                <th>Status<br/> &nbsp; </th>
                                                <th>End Date <br/> &nbsp; </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($uid) || isset($uid_not_yet_started) || isset($uid_cbt_progress) || isset($uid_cbt_finished) || isset($uid_cbt_end_date_reached)) {
                                                switch ($type) {
                                                    case "total"://to display table for all clients
                                                        if (isset($uid) && $uid != null) {
                                                            for ($j = 0; $j < count($uid); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_ssn_total[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime[$j]; ?>                                                       
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "not_yet_started"://to display table for clients not yet started cbt
                                                        if (isset($uid_not_yet_started) && $uid_not_yet_started != null) {
                                                            for ($j = 0; $j < count($uid_not_yet_started); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_not_yet_started[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_not_yet_started[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_not_yet_started[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_not_yet_started[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_not_yet_started[$j]; ?>                                                       
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "active"://to display table for clients who are active
                                                        if (isset($uid_active) && $uid_active != null) {
                                                            for ($j = 0; $j < count($uid_active); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_active[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_active[$j]; ?>                                                       
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "in_active"://to display table for clients who are active
                                                        if (isset($uid_in_active) && $uid_in_active != null) {
                                                            for ($j = 0; $j < count($uid_in_active); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_in_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_in_active[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_in_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_in_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_in_active[$j]; ?>                                                       
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;

                                                    case "end_date_reached"://to display table for clients whose end date is reached
                                                        if (isset($uid_cbt_end_date_reached) && $uid_cbt_end_date_reached != null) {
                                                            for ($j = 0; $j < count($uid_cbt_end_date_reached); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_cbt_end_date_reached[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_cbt_end_date_reached[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_end_date_reached[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_reached[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_end_date_reached[$j]; ?>                                                       
                                                                    </td>
                                                                    <td><!-- Button to extend CBT date--->
                                                                        <button class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal"  data-target="#con-close-modal_<?php echo $j; ?>"><span>Extend CBT</span></button>    
                                                                    </td>
                                                                </tr>
                                                                <!-- Modal to extend cbt end date-->
                                                            <div id="con-close-modal_<?php echo $j; ?>" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content" style="height:25%;">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload()">x</button>
                                                                            <h4 class="modal-title">Extend CBT</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div id="extend_msg" class="msg_disp"></div>
                                                                            <div class="row">
                                                                                <div class="form-group">
                                                                                    <div class="col-md-12" >
                                                                                        <form action="javascript:void(0);" id="extend_cbt_<?php echo $j; ?>" method="POST" >
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" id="datepicker_<?php echo $j; ?>" value="<?php echo date('d-m-Y', strtotime($due_datetime_end_date_reached[$j])); ?>" name="expiry_date">
                                                                                                <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                                                                            </div><!-- input-group -->
                                                                                            <div align="center" style="margin-top:30px;">
                                                                                                <input type="hidden" name="uid" value="<?php echo $uid_cbt_end_date_reached[$j]; ?>"/> 
                                                                                                <input type="hidden" name="page_id" value="view_report"/> 
                                                                                                <input type="hidden" name="tid" value="<?php echo $tid; ?>"/> 
                                                                                                <input type="submit" name="btn-submit-cbt" class="btn btn-success" id="btn-submit-cbt_<?php echo $j; ?>"  value="Update" >
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    break;
                                                case "finished"://to diaplsy table for clients who have finished cbt
                                                    if (isset($uid_cbt_finished) && $uid_cbt_finished != null) {
                                                        for ($j = 0; $j < count($uid_cbt_finished); $j++) {//counter to count all tids
                                                            $sno = $j + 1; //to calculateserial number
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $sno; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $uid_cbt_finished[$j]; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo get_user_name($uid_cbt_finished[$j]); ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $user_creation_datetime_finished[$j]; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $user_ssn_status_finished[$j]; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $due_datetime_finished[$j]; ?>                                                       
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    break;
                                                default:
                                                    ?>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                    break;
                                            }
                                        } else {
                                            //do nothing
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </article>
                        </div><!-- end col -->
                    </div>
                </div> 
            </div> 
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                    var dt = new Date();
                    document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <!-- Datatables-->
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.print.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>
        <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

            $(".dataTables_info").replaceWith("<h2>New heading</h2>");
        </script>
        <script>
            var date = new Date();
            date.setDate(date.getDate());
<?php
if (isset($uid_cbt_end_date_reached) && $uid_cbt_end_date_reached != null) {
    for ($j = 0; $j < count($uid_cbt_end_date_reached); $j++) {
        ?>//counter to count all tids
                    $('#datepicker_<?php echo $j; ?>').datepicker({
                        startDate: date
                    });
        <?php
    }
}
?>
        </script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/clinicians/dashboard/view_client_report.php?tid=<?php echo $tid;?>&type=<?php echo $type;?>";
                    }
        </script>
        <script>
<?php
if (isset($uid_cbt_end_date_reached) && $uid_cbt_end_date_reached != null) {
    for ($j = 0; $j < count($uid_cbt_end_date_reached); $j++) {
        ?>//counter to count all tids
                    $("#extend_cbt_<?php echo $j; ?>").submit(function (event) {

                        //disable the default form submission
                        event.preventDefault();

                        //  grab all form data  
                        var formData = new FormData($(this)[0]);

                        $.ajax({
                            url: "update_expiry_date.php",
                            type: "post",
                            data: formData,
                            async: true,
                            processData: false,
                            cache: false,
                            contentType: false,
                            success: function (response) {
                                if (response == 1) {
                                    $("#extend_msg").addClass("alert");
                                    $("#extend_msg").addClass("alert-success");
                                    $("#extend_msg").html("CBT has been extended.");
                                } else {
                                    $("#extend_msg").addClass("alert");
                                    $("#extend_msg").addClass("alert-danger");
                                    $("#extend_msg").html("Some error occured.");
                                }
                            },
                        });
                    });
        <?php
    }
}
?>
        </script>
    </body>
</html>