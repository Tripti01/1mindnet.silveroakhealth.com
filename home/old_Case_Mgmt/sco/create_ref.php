<?php
#this page is used to send referral links to clients by the clinincians
#the discount for the referral link to be sent is fetched from the thrp_license table
#the amount can be fetched from get_product_amount function
#each link sent is unique with unique code wchich also saved in the user_reference table.
#including functions and other files
require '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/get_product_amount.php';

#to fecth tid from session
$tid = $_SESSION['tid'];

#Default status for sending referral link is o
$status_msg = 0;

$dbh = new PDO($dsn_sco, $login_user, $login_pass);
$dbh->query("use sohdbl");

#to fetch the dscount code to be included in referral email
$stmt01 = $dbh->prepare("SELECT discount_code FROM thrp_license WHERE tid=? LIMIT 1");
$stmt01->execute(array($tid));
if ($stmt01->rowcount() != 0) {
    $row01 = $stmt01->fetch();
    $discount_code = $row01['discount_code'];
} else {
    die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-REFERRAL-1100].");
}

# Calculate discounted amount
$amount = get_product_amount($discount_code);

#on click of link submit btn
if (isset($_REQUEST['link-submit-btn'])) {

    #to check if all fields are set or not
    if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['mobile']) && isset($_POST['message'])) {

        #including requied functions    
        require 'mail/clnc_discount_code.php';
        require 'mail/clnc_discount_email.php';

        #retriving all details from form
        $name = $_POST['name'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $message = $_REQUEST['message'];
        $discount_code = $_REQUEST['discount_code'];
        $thrp_name = $_POST['thrp_name'];

        #to generate a unique discount code to send in referral link
        $unique_code = generate_clnc_discount_code();

        # Make an entry into database, user_reference table
        date_default_timezone_set("Asia/Kolkata");
        $link_sent_datetime = date("Y-m-d H:i:s");

        #Saving in database thus unique code.it is stored in user_reference
        $stmt20 = $dbh->prepare("INSERT INTO user_reference VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt20->execute(array($unique_code, 'CLNC', $tid, $name, $email, $mobile, '0', $link_sent_datetime, '', $discount_code, '0', '0', '0'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-REFERRAL-1101].");

        #Sending email to the user with unqiue email discount link
        send_clnc_discount_mail($email, $name, $message, $thrp_name, $unique_code);

        #if link is sent successfully,set status
        $status_msg = 2;
    } else {
        #if link is not sent,set status
        $status_msg = 1;
    }
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Create Referrals</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" style="height:75%;margin-top:3%;width:55%;">
                     <!--a href="index.php" class="logo" ><p style="margin-top:2%;font-size: 15px;"><span>CLINICIAN'S <span> PRO+</span></span></p></a-->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Create Referral</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li>  
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <?php
                //based on the thrp_type, sidebar changes according to privilages.
                if ($_SESSION['type'] == "CLNC") {
                    include 'sidebar_clnc.php';
                } else if ($_SESSION['type'] == "THRP") {
                    include 'sidebar_coach.php';
                }
                ?>
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <article>
                                        <h5 class="title">
                                            Please provide the following details
                                        </h5>
                                        <div class="panel-body">
                                            <div class="row">
                                                <?php
                                                #to display all the status messages
                                                if ($status_msg == 0) {//help text
                                                    echo '<div style="color:#C0C0C0;margin-left:-6%;">';
                                                    echo 'After successfully filling the details, click "Send Referral Link" button and the client will receive a link to start the sessions.';
                                                    echo '</div>';
                                                } else if ($status_msg == 1) {//if link is not sent 
                                                    echo '<div class="alert alert-danger">Link Not Sent!</div>';
                                                } else if ($status_msg == 2) {//Link Sent Successfully
                                                    echo '<div class="alert alert-success">Link Sent Successfully!</div>';
                                                } else {//if status_msg is nether 1 nor 0
                                                    echo '<div class="alert alert-danger">Error Occured!!</div>';
                                                }
                                                ?>
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" style="margin-top: 15px;">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="userName">Client name <span style="color:red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <input type="text" required="" placeholder="Enter full name of the client" class="form-control" id="name" name="name">
                                                            <div id="err_name" class="error"></div>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="email">Email address <span style="color:red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <input type="email" name="email"  required="" placeholder="Enter email address" class="form-control" id="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="c_email">Confirm email address <span style="color:red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <input type="email" required="" placeholder="Confirm email address" class="form-control" id="c_email" onpaste="return false;" name="c_email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="mobile">Mobile / Phone number <span style="color:red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <input type="text" required="" placeholder="Mobile / Phone number" class="form-control" id="mobile" name="mobile" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" >Message</label>
                                                        <div class="col-md-7">
                                                            <span class="help-block"><small>Sample message is provided below. You can customise this if you want.</small></span>                                                     
                                                            <textarea class="form-control" name="message" rows="15" >
As per our last discussion, below in the email is the link to register for Stress Control Online. This program will really help you and I highly recommend it.

Stress Control Online is an 8-week online program based on Cognitive Behavioural Therapy (CBT). The program is based on the work of Dr. Jim White, whose CBT program has helped tens of thousands of people in the National Health Service, UK and around the world. It is now available with content and context specific to Indian users.

As users undergo the program at the privacy of their home, they will also be assisted by a dedicated CBT trained therapist from Stress Control Online who will give them, one-on-one guidance over the phone every week.

If you have any other questions you can reach out to a Stress Control Online- Psychological Advisor at +91-80-6547 2100 or hello@stresscontrolonline.com

Click below link to create your Stress Control Online account.
                                                            </textarea>
                                                            <span class="help-block"><small>PLEASE NOTE : Unique Discount link will be added automatically at the end .</small></span>  
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="amount">Amount</label>
                                                        <div class="col-md-7">
                                                            <input type="text" required="" placeholder="<?php echo $amount; ?>" class="form-control" id="amount" name="amount" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="form-group text-right m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                                        <input type="hidden" id="discount_code" name="discount_code" value="<?php echo $discount_code; ?>"/>
                                                        <input type="hidden" id="thrp_name" name="thrp_name" value="<?php echo $thrp_name; ?>"/>
                                                        <input type="submit" id="link-submit-btn" name="link-submit-btn" value="Send Referral Link" class="btn btn-success"/>
                                                        <button type="reset" class="btn btn-danger" style="margin-right:10px;">Clear Form</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </article>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                        var dt = new Date();
                        document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>

        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script>
            //Script to prevent copy paste of email and confirm email       
            $(document).ready(function () {
                $('#email').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
                $('#c_email').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });

            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    c_email: {
                        required: true,
                        equalTo: "#email"
                    },
                    mobile: {
                        required: true,
                        phoneUS: true
                    },
                    message: {
                        required: true,
                    }
                },
                messages: {
                    name: {
                        required: "Please enter your full name"
                    },
                    email: {
                        required: "Please enter your email",
                        email: "Invalid Email ID"
                    },
                    c_email: {
                        required: "Please enter same email address",
                        email: "Invalid Email ID"
                    },
                    mobile: {
                        required: "Please enter your mobile number",
                        maxlength: "Please enter a valid mobile number"
                    },
                    message: {
                        required: "Please enter message to be sent along with link"
                    }
                }

            });
        </script>
        <script type="text/javascript">
            //script to enter only alphabets in text box
            $("#name").focusout(function () {
                var name_text = document.getElementById("name").value;

                if ((name_text.match(/^[a-zA-Z\s]+$/) == null) && (name_text != ""))
                {
                    $("#err_name").html("Invalid name");

                }
            });
        </script>
    </body>
</html>


