<?php
#including functions
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
?>
<!DOCTYPE html>
<html>
    <!-- Mirrored from coderthemes.com/adminto_1.3/light/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Jul 2016 08:36:07 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <!-- App title -->
        <title>Help</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- Plugins css-->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .label_css {
                font-family: 'Calibri';
                font-size: 16px;
            }

        </style>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" style="height:75%;margin-top:3%;width:55%;">
                    <!--a href="index.php" class="logo" ><p style="margin-top:2%;font-size: 15px;"><span>CLINICIAN'S <span> PRO+</span></span></p></a--->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Help</h4>
                            </li>
                        </ul>


                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li>  
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <?php
                //Based on the thrp_type,sidebar changes accornding to their privilages
                if ($_SESSION['type'] == "CLNC") {
                    include 'sidebar_clnc.php';
                } else {
                    include 'sidebar_coach.php';
                }
                ?>
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="card-box">
                                <article>
                                    <h4 class="title"><b> Contact Us </b></h4>
                                    <div style="margin-top:20px; margin-bottom:20px"></div>
                                    <label class="label_css"><B> Product Related Support </B></label> <BR/>
                                    For product queries, you can always contact us directly at 
                                    <span style="color:#349Bff;">  help@stresscontrolonline.com </span>
                                    <BR/><BR/>
                                    <label class="label_css"><B>Technical Issues </B></label> <BR/>
                                    If you are facing some technical issues. Do write to us at 
                                    <span style="color:#349BFF;"> help@stresscontrolonline.com  </span>
                                    <BR/><BR/>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                        var dt = new Date();
                        document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
    </body>
</html>