<?php
#including functions and files
require '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';

#getting tid from session
$tid = $_SESSION['tid'];

$dbh = new PDO($dsn_sco, $login_user, $login_pass);
$dbh->query("use sohdbl");

//change_password status
$password_status = 0;
#change info status
$info_status = 0;

# Fetch user Email from user_login table. 
$stmt01 = $dbh->prepare("SELECT email FROM thrp_login WHERE tid=? LIMIT 1");
$stmt01->execute(array($tid));
//if email is present fetch email,else display eror message
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    $email = $row01['email']; //to fetch email
} else {
    //since emial is not set,diplay error message
    die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1101].");
}

##to change password
#on click of change password btn
if (isset($_POST['ChangePwd'])) {
    //Fetch password from database.
    $stmt9 = $dbh->prepare("SELECT password FROM thrp_login WHERE tid=? LIMIT 1");
    $stmt9->execute(array($tid)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1103].");
    $user_password = $stmt9->fetch();
    $db_password = $user_password['password']; //to fetch password from database
    #to get password details
    $cur_password = $_POST['cur_password'];
    $confirm_password = $_POST['password_again'];
    $newpassword = $_POST['password'];

    // password verification
    if (password_verify($cur_password, $db_password)) {
        if ($cur_password != $newpassword) {

            $hash_options = array('cost' => 11); //creating salt to be added to password
            $hashed_password = password_hash($newpassword, PASSWORD_DEFAULT, $hash_options); //hashing password
            //hashed password is updated to the database. 
            $stmt10 = $dbh->prepare("UPDATE thrp_login SET password=? WHERE tid=?");
            $stmt10->execute(array($hashed_password, $tid)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1104].");
            $password_status = 1; //password updated successfully
        } else {
            $password_status = 2; //new and old password are same
        }
    } else {
        $password_status = 3; //current password are incorrect
    }
}

#to fetch the contact number and address of the clinician
$stmt02 = $dbh->prepare("SELECT thrp_contact, thrp_full_addr FROM  thrp_comm_pref WHERE tid=? LIMIT 1");
$stmt02->execute(array($tid));
#if details are present, fetch details.ELsr display error messages
if ($stmt02->rowCount() != 0) {
    $row02 = $stmt02->fetch();
    $mobile = $row02['thrp_contact']; //to fetch contact
    $full_addr = $row02['thrp_full_addr']; //to fetch address
} else {
    //since contact details are not present, display error message
    die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1102].");
}


//Update the current address of the clinician
if (isset($_POST['contact_info_edit'])) {

    #to get the details
    $contact = $_POST['mobile'];
    $full_addr = $_POST['full_addr'];

    #updating the contact and adress of the therapist
    $stmt11 = $dbh->prepare("UPDATE thrp_comm_pref SET thrp_contact=? , thrp_full_addr=? WHERE tid=?");
    $stmt11->execute(array($contact, $full_addr, $tid)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1104].");
    $info_status = 1; //successfully updated
}

#based on type, fetch license details only if its clinicians
if ($_SESSION['type'] == "CLNC") {
# Query database and fetch all the license details of this therapist
    $stmt11 = $dbh->prepare("SELECT * FROM thrp_license WHERE tid=? LIMIT 1;");
    $stmt11->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1105].");
    if ($stmt11->rowCount() != 0) {
        $row11 = $stmt11->fetch();
        $start_date = date('d-m-Y', strtotime($row11['start_date'])); //to fecth license start date
        $end_date = date('d-m-Y', strtotime($row11['end_date'])); //to fetch license end date
        $users = $row11['users']; //to fecth number of users
        $product = $row11['product']; //to fetch product details
    }
# Query to fetch the user count of this therapist
    $stmt21 = $dbh->prepare("SELECT user_count FROM thrp_users_count WHERE tid=? LIMIT 1");
    $stmt21->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ACC1106].");
    if ($stmt21->rowCount() != 0) {
        $row21 = $stmt21->fetch();
        $user_count = $row21['user_count']; //to fecth user count of therapist
    }
    $rem_user_count = $users - $user_count; //remaining user licenses
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <!-- App title -->
        <title>Account</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .label_td{
                width:25%;
            }
            .input_td{
                width:100%;
            }
            .note-danger{
                color:red;
            }
        </style>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" style="height:75%;margin-top:3%;width:55%;">
                    <!--a href="index.php" class="logo" ><p style="margin-top:2%;font-size: 15px;"><span>CLINICIAN'S <span> PRO+</span></span></p></a-->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Account</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li>  
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <?php
                //based on the thrp_type, sidebar changes according to privilages.
                if ($_SESSION['type'] == "CLNC") {
                    include 'sidebar_clnc.php';
                } else if ($_SESSION['type'] == "THRP") {
                    include 'sidebar_coach.php';
                }
                ?>
            </div>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30" style="color:#35b8e0;font-size:18px"></h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="portlet-title tabbable-line">

                                                <ul class="nav nav-tabs">
                                                    <li class="active" id="PI">
                                                        <a href="#personal_info" id="PI" data-toggle="tab">Personal Info</a>
                                                    </li>
                                                    <li id="CP">
                                                        <a href="#change_password" id="CP" data-toggle="tab">Change Password</a>
                                                    </li>
                                                    <li id="CI">
                                                        <a href="#contact_detail" id="CI" data-toggle="tab">Contact Info</a>
                                                    </li> 
                                                    <?php
#based on type, display license details only if its clinicians
                                                    if ($_SESSION['type'] == "CLNC") {
                                                        ?>
                                                        <li id="tab4">
                                                            <a href="#license_info" data-toggle="tab">License Info</a>
                                                        </li> <?php } ?>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">

                                                    <div class="tab-pane active" id="personal_info">
                                                        <!-- Display user information-->  
                                                        <div class="portlet-body">
                                                            <div style="color:#2ABB2A;font-weight:bolder">
                                                                <!--Display information when updation is complete-->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="row" >
                                                                            <div class="col-md-6"  >
                                                                                <div class="form-group">
                                                                                    <div style="text-align:left"><label class="caption-subject caption-subject-color" style="color:grey"> Name </label></div>
                                                                                    <span class="form-control" disabled> <?php echo $thrp_name; ?> </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6" >
                                                                                <div class="form-group">
                                                                                    <div style="text-align:left"><label class="caption-subject caption-subject-color" style="color:grey"> Email </label></div>
                                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $email; ?> </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="change_password">                                                           
                                                        <div class="portlet-body">
                                                            <div style="text-align:center; color:#2ABB2A;font-weight:bolder">
                                                                <!--Display message when password is changed.-->
                                                                <?php if ($password_status == 1) {//Password changed successfully   ?>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="note note-success note-bordered">
                                                                                <p>
                                                                                    <?php echo "Password changed successfully"; ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } else if ($password_status == 2) {//New password and current password are same   ?>                                                
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="note note-danger note-bordered">
                                                                                <p>
                                                                                    <?php echo "New password and current password are same."; ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>																
                                                                <?php } else if ($password_status == 3) {//Current Password is Incorrect
                                                                    ?>                                                
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="note note-danger note-bordered">
                                                                                <p>
                                                                                    <?php echo "Current Password is Incorrect.Please try again"; ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>																
                                                                    <?php
                                                                } else {
                                                                    
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <form id="change_pass_form" action="my_account.php" method="POST">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="caption-subject caption-subject-color">Current Password</label><font color="RED">*</font>
                                                                        <input class="form-control placeholder-no-fix" id="cur_password" type="password" placeholder="Current Password" name="cur_password"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="caption-subject caption-subject-color">New Password</label><font color="RED">*</font>
                                                                        <input type="password" name="password" id="password" class="form-control" placeholder="New Passowrd">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="caption-subject caption-subject-color">Re-type New Password</label><font color="RED">*</font>
                                                                        <input type="password" name="password_again" id="password_again" class="form-control" placeholder="Re-type New Password">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="margin-top-10">
                                                                    <input type="submit" class="btn btn-success" style="background-color:#188ae2;  color:white" name="ChangePwd" value="Change Password" >
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div class="tab-pane" id="contact_detail">           
                                                        <div style="text-align:center; color:#2ABB2A;font-weight:bolder">
                                                            <!--Display message when info is changed.-->
                                                            <?php if ($info_status == 0) { ?>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="note note-success note-bordered">
                                                                            <p>
                                                                                <!-- nothing to display -->
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } else if ($info_status == 1) { // has been changed        ?>                                         
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="note note-success note-bordered">
                                                                            <p>
                                                                                <?php echo "Your details has been changed"; ?>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>	
                                                            <?php } ?>
                                                        </div> 
                                                        <div class="row" >
                                                            <div class="col-md-6" >
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color"> Email </label>
                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $email; ?> </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" >
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color"> Mobile </label>
                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $mobile; ?> </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color"> Full Address </label>
                                                                    <label class="form-control placeholder-no-fix" style="height:50%;" disabled> <?php echo $full_addr; ?> </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <table><!-- Btn edit contact info -->
                                                            <tr><td><a class="btn btn-success" data-toggle="modal" href="#static" style="margin-top:20px">Edit </a></td></tr>
                                                        </table>
                                                    </div>
                                                    <div class="tab-pane" id="license_info">           
                                                        <div class="row" >
                                                            <div class="col-md-6" >
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color"> Total Licenses </label>
                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $users; ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" >
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color"> Used Licenses </label>
                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $user_count; ?> </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color">Remaining Licenses </label>
                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $rem_user_count; ?> </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="caption-subject caption-subject-color">Expiry Date </label>
                                                                    <label class="form-control placeholder-no-fix" disabled> <?php echo $end_date; ?> </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12" style="text-align:center;margin-top: 15px;" >
                                                                To buy more licenses, please send an email to <label>help@stresscontrolonline.com</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- Modal to change contct details-->
        <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edit Information</h4>
                    </div>
                    <form id="contact_form" role="form" action="my_account.php" method="POST">
                        <div class="modal-body">
                            <table class="center">
                                <tr>
                                    <td class="label_td"><label class="control-label col-md-12">Email</label></td>
                                    <td class="input_td"><input type="text" style="width:100%;margin-bottom:4px;" value="<?php echo $email; ?>" class="form-control"  name="email" disabled/></td>
                                </tr>
                                <tr>
                                    <td class="label_td"><label class="control-label col-md-12">Mobile</label></td>
                                    <td class="input_td"><input type="text" style="width:100%;margin-bottom:4px;" value="<?php echo $mobile; ?>" class="form-control" id="mobile" name="mobile" ></td>
                                </tr>
                                <tr>
                                    <td class="label_td"><label class="control-label col-md-12">Full Address</label></td>
                                    <td class="input_td"><textarea type="text" style="width:102%" class="form-control"  name="full_addr"><?php echo $full_addr; ?></textarea></td>
                                </tr>
                            </table>
                            <div class="modal-header" style="margin-bottom:10px"></div>
                            <input class="btn btn-submit btn-success" name="contact_info_edit" type="submit" style="float: left;margin-left:65%" value="Update">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="float: left;margin-left:3%">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="footer">
            <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                        var dt = new Date();
                        document.write(dt.getFullYear());
                </script>, SilverOakHealth. All Rights Reserved.
            </p>
        </footer>
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->  
        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>

        <script>
<?php if (($password_status == 1) || ($password_status == 2) || ($password_status == 3)) { ?>
                $(document).ready(function () {
                    $("#PI").removeClass("active");
                    $("#personal_info").removeClass("active");
                    $("#CP").addClass("active");
                    $("#change_password").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if ($info_status == 1) { ?>
                $(document).ready(function () {
                    $("#PI").removeClass("active");
                    $("#personal_info").removeClass("active");
                    $("#CP").removeClass("active");
                    $("#change_password").removeClass("active");
                    $("#CI").addClass("active");
                    $("#contact_detail").addClass("active");
                });
<?php } ?>
        </script>
        <script>
            jQuery(document).ready(function () {
                $("#change_pass_form").validate({
                    rules: {
                        cur_password: {
                            required: true,
                        },
                        password: {
                            required: true,
                            minlength: 7
                        },
                        password_again: {
                            required: true,
                            equalTo: "#password"
                        }
                    },
                    messages: {
                        cur_password: {
                            required: "Please enter the current password"
                        },
                        password: {
                            required: "Please enter the password"
                        },
                        password_again: {
                            required: "Please enter the password"
                        }
                    }

                });
            });
        </script>
        <script>
            jQuery(document).ready(function () {
                $("#contact_form").validate({
                    rules: {
                        email: {
                            required: true,
                        },
                        mobile: {
                            required: true,
                            phoneUS: true
                        },
                        full_addr: {
                            required: true,
                        }
                    },
                    messages: {
                        email: {
                            required: "Please enter your email"
                        },
                        mobile: {
                            required: "Please enter your mobile number",
                        },
                        full_addr: {
                            required: "Please enter the address"
                        }
                    }

                });
            });
        </script>
    </body>
</html>