<!--this side_bar_clnc is the side bar for therapist.
the side bar varies according to the thrp_type saved in the session
Style of side_bar-->

<style>.side_icon{color: #223C80;}
    .side_text{font-size: 14px;color: #223C80;font-weight: normal;font-family:"Open Sans", sans-serif;}
    .ol,ul{padding-left:0px;}'
    .red-tooltip + .tooltip > .tooltip-inner {
    background-color: black;
    color:white;
    padding:5px 10px 5px 5px;;
    left: 27px;
    }
    .tooltip.bottom .tooltip-arrow{
    border-bottom-color:black;
    }
</style>
<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu" style="margin-top:-7%;">
        <hr/>
        <ul>

            <!--dashboard-->
            <li>
                <a href="index.php" class="waves-effect"><i class="fa fa-home side_icon" ></i> <span class="side_text" >SCO Dashboard </span> </a>
            </li>

            <!--My clients-->
            <li>
                <a href="my_clients.php" class="waves-effect"><i class="fa fa-users side_icon"></i> <span class="side_text" >SCO Clients</span></a>
            </li>

            <!--my schedule-->
            <li>
                <a href="my_schedule.php" class="waves-effect"><i class="fa fa-calendar-o side_icon"></i> <span class=" side_text">SCO Schedules </span> </a>
            </li>
            <!-- Reports---->
            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>SCO Reports</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="users_feedback.php" target="_BLANK">User Satisfaction Report</a></li>
                    <li><a href="asmt.php" target="_BLANK">User Assessment Report</a></li>

                </ul>
            </li>
            <!--Phone Therapy-->
            <!--li>
                <a href="call_mgmt/index.php" class="waves-effect"><i class="fa fa-phone side_icon"></i> <span class=" side_text">Call Management</span> </a>
            </li-->
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div></div>
    <div style="text-align:center;padding-bottom:0%;margin-top:-20%;">
    <div class="row">
        <a href="call_mgmt/my_calls.php" class="waves-effect"><i class="fa fa-phone side_icon" ></i> <span class="side_text" >Call Management</span> </a>
    </div>
</div>
