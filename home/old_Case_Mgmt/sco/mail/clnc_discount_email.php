<?php
#this function is used to send discount referrl links .
#these emails are sent by the clincians to their clients whom they wish to refer.
#this email includes an uniquely generated link attached.
#on click of the link leads to register/reference/activate.php
function send_clnc_discount_mail($to, $to_name, $message, $from_name, $unique_code) {
require 'aws_sdk/aws-autoloader.php';
require 'ses_plugin/autoloader.php';
require 'ses_plugin/mail_credentials.php';
include 'soh-config.php';

$m = new SimpleEmailServiceMessage();

#Creating Unique Link
$unique_link=$host . '/register/reference/activate.php?link=' . $unique_code;

# Addig Unique Link to the message, Separated by new line
$message=$message.$unique_link;

## ------------------------------- EMAIL PARAMETERS ---------------------------##
$to = $to;
$from = ''.$from_name.' <no-reply@stresscontrolonline.com>';
$subject = 'Hi '.$to_name;
$html = '';
//this $addressing to used to attach addressing in the email template
$addressing='Dear '.$to_name.' , 
        ';
$text = $addressing.$message; // plain text version [optional]
## ----------------------------------------------------------------------------##

$m->addTo($to);
$m->setFrom($from);
$m->setSubject($subject);
$m->setMessageFromString($text,'');
$m->setSubjectCharset('ISO-8859-1');

try {
    $ses = new SimpleEmailService($key, $secret); // Sending the message
	$ses->sendEmail($m);
} catch (Exception $ex) {
    die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@stresscontrolonline.com");
}
}