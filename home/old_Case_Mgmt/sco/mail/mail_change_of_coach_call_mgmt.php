<?php

//this function is used to send email to the caller and also all the previously assgned coaches during re assugnung coaches
//the function recives caller_cid, caller_email, caller_name, new_coach name and also previously assigned coach

function mail_change_of_coach_call_mgmt($caller_cid, $caller_email, $caller_name, $new_coach_name, $old_coach_name) {

    //include file and folders
    include 'ewap-config.php';
    include 'soh-config.php';

# Start the database realated stuff
    $dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
    $dbh->query("use scodd");

    //database connection to sco
    $dbh_sco = new PDO($dsn_sco, $login_user, $login_pass);
    $dbh_sco->query("use sohdbl");


#to fetch all email ids of coaches previously assigned to the caller
    $i = 0; //counter to fecth all tids previously assigned
    $stmt00 = $dbh->prepare("SELECT DISTINCT tid FROM call_cid_tid_list WHERE cid = ?");
    $stmt00->execute(array($caller_cid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1000].");
    if ($stmt00->rowCount() != 0) {
        while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {

            $tid_list[$i] = $row00['tid']; //to fetch all tids previously assigned coaches
            //to fetch the list of emails of 
            $stmt10 = $dbh_sco->prepare("SELECT email FROM thrp_login WHERE tid = ?");
            $stmt10->execute(array($tid_list[$i]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1003].");
            if ($stmt10->rowCount() != 0) {
                $row10 = $stmt10->fetch();
                $tid_email_list[$i] = $row10['email']; //to fetch email
            }
            $i++;
        }
    }

    require_once 'aws_sdk/aws-autoloader.php';
    require_once 'ses_plugin/autoloader.php';
    require 'ses_plugin/mail_credentials.php';
    include 'soh-config.php';

    $m = new SimpleEmailServiceMessage();

    ## ------------------------------- EMAIL PARAMETERS ---------------------------##

    $to = $caller_email;
    for ($i = 0; $i < count($tid_list); $i++) {
        $cc_list[$i] = $tid_email_list[$i];
    }
    $from = 'Stress Control Online <no-reply@silveroakhealth.com>';
    $subject = 'Consent to release information';
    $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <title></title>
        <style type="text/css">
            html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
            @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
                *[class="table_width_100"] {
                    width: 96% !important;
                }
                *[class="border-right_mob"] {
                    border-right: 1px solid #dddddd;
                }
                *[class="mob_100"] {
                    width: 100% !important;
                }
                *[class="mob_center"] {
                    text-align: center !important;
                }
                *[class="mob_center_bl"] {
                    float: none !important;
                    display: block !important;
                    margin: 0px auto;
                }	
                .iage_footer a {
                    text-decoration: none;
                    color: #929ca8;
                }
                img.mob_display_none {
                    width: 0px !important;
                    height: 0px !important;
                    display: none !important;
                }
                img.mob_width_50 {
                    width: 40% !important;
                    height: auto !important;
                }
            }
            .table_width_100 {
                width: 680px;
            }
            
        </style>
    </head>
    <body style="padding: 0px; margin: 0px;">
        <div id="mailsub" class="notification" align="center">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" >
                        <!--[if gte mso 10]>
                        <table width="680" border="0" cellspacing="0" cellpadding="0">
                        <tr><td>
                        <![endif]-->
                        <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                             <!--content 1 -->
                            <tr><td align="center" bgcolor="#ffffff">
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                                <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td align="left">
                                                            <div style="line-height: 22px;">
                                                                <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 13px;">
                                                                 Dear ' . $caller_name . ', <BR/>
                                                                        
                                                                 As per your discussion with <b>' . $new_coach_name . '</b>, this is to confirm your consent to release your counseling sessions case notes to <b>' . $new_coach_name . '</b>. This will help <b>' . $new_coach_name . '</b> understand your case history better and help you with the ongoing counseling sessions.
                                                                     <BR/>
                                                                 All information obtained by us will remain confidential. Please refer to our <a href="https://ewap.silveroakhealth.com/html/privacy_policy.html">Privacy Policy</a> and <a href="https://ewap.silveroakhealth.com/html/terms_of_use.html">Terms of Use</a> for further details.                                                                    
                                                                 <div style="margin-top:2%;">Regards,<BR/>
                                                                    Silver Oak Health
                                                                 </div>
                                                                </font>
                                                            </div>
                                                    </td></tr>
                                                </table>
                                                <!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                    </table>		
                                </td></tr>
                            <!--content 1 END-->
                            
                        </table>
                        <!--[if gte mso 10]>
                        </td></tr>
                        </table>
                        <![endif]-->			 
                    </td></tr>
            </table>						
        </div> 
    </body>
</html>';
    $text = ''; // plain text version [optional]
    ## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    for ($i = 0; $i < count($tid_list); $i++) {
        $m->addCC($cc_list[$i]);
    }
    $m->setSubject($subject);
    $m->setMessageFromString($text, $html);
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@stresscontrolonline.com");
    }
}

?>