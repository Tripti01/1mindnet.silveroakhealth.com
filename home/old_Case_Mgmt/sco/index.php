<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'coach_function/get_user_corporate_name.php';

#getting tid from session
$tid = $_SESSION['tid'];

$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

$i = 0; //counter for users assigned

$stmt1 = $dbh->prepare("SELECT user_therapist.uid,user_login.name, user_login.email FROM user_therapist,user_login WHERE user_therapist.uid=user_login.uid AND tid=? ORDER BY uid DESC");
$stmt1->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY_CLIENT-1000].");
if ($stmt1->rowCount() != 0) {
    while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {

        $uid[$i] = $row1['uid'];
        $user_name[$i] = $row1['name'];
        $user_email[$i] = $row1['email'];


        ## Find out the registeration date
        $stmt12 = $dbh->prepare("SELECT creation_time FROM user_time_creation WHERE uid=?");
        $stmt12->execute(array($uid[$i]));
        if ($stmt12->rowCount() != 0) {
            $row12 = $stmt12->fetch();
            $reg_date[$i] = date("d-M-Y", strtotime($row12['creation_time']));
        } else {
            $reg_date[$i] = "";
        }

        $company_name[$i] = get_corporate_name_of_user($uid[$i]);

        $i++;
    }
} else {
    echo "You dont have any SCO users assigned yet to you";
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>My SCO Clients</title>
        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <style>
            .side_text span{
                margin: -1px 1px 1px -1px;
                line-height: 5%;
                border-left: 2px solid blue;
                padding-left: 10px;
            }
            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #ebeff2;
            }
            tbody {
                color: #797979;
            }
            th {
                color: #666666;
                font-weight: 600;
            }
            .title_color{
                color:#223C80;
                font-size: 35px;
                margin-bottom: 20px;
                padding-top:15px;
            }
            .dataTables_info{
                display: none;
            }
            .dataTables_length{
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .title_color{
                color:#223C80;
                font-size: 35px;
                margin-bottom: 20px;
                padding-top:15px;
            }
            .table-striped > tbody > tr:nth-of-type(odd), .table-hover > tbody > tr:hover, .table > thead > tr > td.active, .table > tbody > tr > td.active, .table > tfoot > tr > td.active, .table > thead > tr > th.active, .table > tbody > tr > th.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > tbody > tr.active > td, .table > tfoot > tr.active > td, .table > thead > tr.active > th, .table > tbody > tr.active > th, .table > tfoot > tr.active > th {
                background-color: #fff !important;
            }
            .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
                background-color: #3498db;
                border-color:  #3498db;
            }
        </style>

    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include '../top_navbar.php'; ?>

            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card-box">
                                    <div class="table-responsive">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <table  id='datatable' class='table table-striped table-bordered'>
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th >Client Name</th>
                                                        <th>Email</th>
                                                        <th>Company Name</th>
                                                        <th>Registration Date</th>

                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    for ($k = 0; $k < count($uid); $k++) {

                                                        $sr_no = $k + 1;

                                                        echo '<tr>';
                                                        echo '<td>' . $sr_no . '</td>';
                                                        echo '<td>' . $user_name[$k] . '</a></td>';
                                                        echo '<td>' . $user_email[$k] . '</td>';
                                                        echo '<td>' . $company_name[$k] . '</td>';
                                                        echo '<td>' . $reg_date[$k] . '</td>';
                                                        echo '<td><a href="view_client.php?uid=' . $uid[$k] . '"><button type="button" class="btn btn-primary btn-bordred waves-effect btn-xs waves-light">View</button></a></td>';
                                                        echo '</tr>';
                                                    }
                                                    ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> 
                </div> 
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>

        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- Form wizard -->
        <script src="../assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>

        <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.min.js"></script>
        <!-- Datatables-->
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.print.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

            $(".dataTables_info").replaceWith("<h2>New heading</h2>");
        </script>
    </body>
</html>