<?php
#including functions and 
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/coach/get_session_name.php';

#to fetch current datetime
date_default_timezone_set("Asia/Kolkata");
$cur_date = date('d-M-Y');
$cur_datetime = strtotime(date('Y-m-d H:i:s'));

# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Query database and fetch all the useers who have to be reminded
$i = 0; //counter for thrp_remind
$stmt10 = $dbh->prepare("SELECT * FROM thrp_remind WHERE tid=? AND display=?");
$stmt10->execute(array($tid, '1'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-REMIND-1000].");
if ($stmt10->rowCount() != 0) {
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        # Get User Id from SQL results 
        $uid_list = $row10['uid']; //acts as a flag ,if isset then dsplay uids else doisplay nothing
        $uid[$i] = $row10['uid']; //to fetch uids to array
        $name[$i] = decrypt($row10['name'], $encryption_key); //to feth name
        $ssn[$i] = $row10['ssn'];

        #if session is CBTSTART, display as CBT.else display session full name.
        if ($ssn[$i] == "CBTSTART") {
            $ssn_fullname[$i] = "CBT";
        } else {
            $ssn_fullname[$i] = get_ssn_fullname($ssn[$i]); //to get session full name
        }
        $text[$i] = $row10['text']; //to fecth text
        $date_db[$i] = strtotime(date('Y-m-d H:i:s', strtotime($row10['date']))); //to diaplay datetime of date when reminder has to be sent
        $days[$i] = intval((abs($cur_datetime - $date_db[$i])) / 86400); //to calculate number of days
        $i++;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Reminder To Send</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .heading{
                color:black;
            }
            .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
                background-color: #9c9c9b;
                border-color: #9c9c9b;
            }
            .btn-primary[disabled]:hover, .btn-primary.disabled:focus, .btn-primary.disabled:active, .btn-primary.active, .btn-primary.focus, .btn-primary.disabled:active, .btn-primary.disabled:focus, .btn-primary.disabled:hover, .open > .dropdown-toggle.btn-primary {
                background-color: #9c9c9b;
            }
            th{
                color:black;
            }
            .dataTables_info{
                color:#C0C0C0;
            }
            td{
                vertical-align: middle;
            }
            res_td{
                width:70%;
            }
            /*res_td1{
                width:20%;
            }*/
        </style>
    </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="inbox-widget nicescroll" style="height: 600px;overflow-y:auto;">
                                        <table class="table m-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th class="res_td">Client Name</th>
                                                    <th class="res_td1">Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo '<script src="../assets/js/jquery.min.js"></script>';
                                                if (isset($uid_list)) {//only of uid_list is set proceed
                                                    for ($i = 0; $i < count($uid); $i++) {//counter for all uids
                                                        $sr_no = ($i + 1); //to determine serial number
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $sr_no; ?></td>
                                                            <td class="res_td"><a href="view_client.php?uid=<?php echo $uid[$i]; ?>"><?php echo $name[$i]; ?></a></td>
                                                            <td class="res_td1"><?php echo $ssn_fullname[$i] . ' ' . $text[$i] . ' ' . $days[$i] . ' days.'; ?></td>
                                                            <td>
                                                                <!--- to display dismissal btn--->
                                                                <?php
                                                                echo '<a><button id="btn_' . $i . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#display_client_' . $i . '" ></i></button></a>';
                                                                ?>
                                                                <!--MODAL to dismiss remind client -->
                                                                <div id='display_client_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog modal-sm">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                                                <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                            </div>
                                                                            <div class="modal-body" style="margin-top:5%;">
                                                                                <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                    Are you sure you want to dismiss this ? 
                                                                                    <div style="text-align:center;margin-top:10%;">
                                                                                        <input type="submit"  name="confirm-call-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                        <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                    </div>
                                                                                </form>
                                                                                <?php
                                                                                #on click of 'yes' btn(confirm-call-btn)
                                                                                if (isset($_REQUEST['confirm-call-btn_' . $i . ''])) {
                                                                                    //update display as 0 to dismiss reminder
                                                                                    $stmt400 = $dbh->prepare("UPDATE thrp_remind SET display=0 WHERE tid=? AND uid=? AND ssn=? LIMIT 1");
                                                                                    $stmt400->execute(array($tid, $uid[$i], $ssn[$i]));

                                                                                    echo '<script>window.location.href="remind_client.php";</script>'; //reload page
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div><!-- /.modal-content -->
                                                                    </div><!-- /.modal-dialog -->
                                                                </div><!-- /.modal -->
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    //if no clients are completed ,then display message as no clients to schedule.
                                                    echo "No Clients To Remind.";
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Status as on <?php echo $cur_date; ?>.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
    </body>
</html>



