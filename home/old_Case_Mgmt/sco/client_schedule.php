<?php
#including functions and other files
include 'if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/get_user_status.php';
include 'functions/coach/get_user_status_coach.php';
include 'functions/coach/get_session_name.php';
include 'functions/coach/get_user_name.php';

#to set the timezone
date_default_timezone_set("Asia/Kolkata");

# Start the database
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#to fetch all the details who have completed session 
#on passing the tid and scheduled_status we get the list of uids who have completed sessions and calls have to be scheduled
$j = 0; //counter for completed sessions
$stmt200 = $dbh->prepare("SELECT * FROM thrp_user_ssn_completed WHERE tid=? AND scheduled_status=? ORDER BY completed_on ASC");
$stmt200->execute(array($tid, '0'))or die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code :THRP-SCHEDULE-1000");
if ($stmt200->rowCount() != 0) {
    while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
        # Get User Id from SQL results 
        $uid_completed_list = $row200['uid']; //acts as flag to check if any uids are set or not for the above condition
        $uid_completed[$j] = $row200['uid']; //to fecth uid into array
        $ssn_completed[$j] = $row200['ssn']; //to fecth the session which will be used for further calculations
        $ssn_fullname_completed[$j] = get_ssn_fullname($row200['ssn']); //to get seeion full name
        $ssn_endtime_completed[$j] = date('d-M-Y h:i A', strtotime($row200['completed_on'])); //get completed_on in (25-Nov-2016 12:00 PM)format 
        $completed_on[$j] = $row200['completed_on']; //unformatted completed_on to pass for further calculations
        $name_completed[$j] = get_user_name($uid_completed[$j]); //to get the user name
        $j++;
    }
} else {
    //users assigned to the therapist have not completed any sessions,hence display nothing.
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Calls To Schedule</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .res_td{
                width:25%;
            }
            .res_td1{
                width:45%;
            }
            .heading{
                color:black;
            }
            th{
                color:black;
            }
            td{
                vertical-align: middle;
            }
        </style>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" style="height:75%;margin-top:3%;width:55%;">
                      <!---a href="index.php" class="logo" ><p style="margin-top:2%;font-size: 15px;"><span>CLINICIAN'S <span> PRO+</span></span></p></a--->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Calls To Schedule</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li>  
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <?php
                //based on the thrp_type, sidebar changes according to privilages.
                if ($_SESSION['type'] == "CLNC") {
                    include 'sidebar_clnc.php';
                } else if ($_SESSION['type'] == "THRP") {
                    include 'sidebar_coach.php';
                }
                ?>
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="inbox-widget nicescroll" style="height: 600px;overflow-y:auto;">
                                        <table class="table m-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th class="res_td">Client Name</th>
                                                    <th class="res_td1">Status</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                echo '<script src="../assets/js/jquery.min.js"></script>';
                                                #only if the falg is set, i.e there are clients who have completed session, then proceed further
                                                if (isset($uid_completed_list)) {
                                                    #j acts as a counter for all the uids who have completed session
                                                    for ($j = 0; $j < count($uid_completed); $j++) {
                                                        $sr_no = ($j + 1); //to print the serial number
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $sr_no; ?></td>
                                                            <td class="res_td"><a href="view_client.php?uid=<?php echo $uid_completed[$j]; ?>"><?php echo $name_completed[$j]; ?></a></td>
                                                            <?php
                                                            echo '<td class="res_td1">';
                                                            #only if the session completed is CBTSTART, then display as Started CBT on '' else display as Completed Session_name on ''
                                                            if ($ssn_completed[$j] == "CBTSTART") {
                                                                echo 'Started ' . $ssn_fullname_completed[$j] . ' on<span style="font-size:12px;"> ' . $ssn_endtime_completed[$j] . '</span>';
                                                            } else {
                                                                echo 'Completed ' . $ssn_fullname_completed[$j] . ' on<span style="font-size:12px;"> ' . $ssn_endtime_completed[$j] . '</span>';
                                                            }
                                                            echo '</td>';
                                                            echo '<td>';
                                                            #Button 'Schedule'. on click of this button opens a modal through wchich schedule of call can be done.
                                                            echo'<a><button id="btn_schedule_' . $j . '" class="btn btn-primary waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#schedule_call_' . $j . '" >Schedule</button></a>';
                                                            echo '</td>';
                                                            #Button 'x'. on click of this button opens a modal through wchich dissmissal can be done.
                                                            echo '<td class="td_dismiss"><a><button id="btn_dis_schedule_' . $j . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dismiss" ><i class="fa fa-times" data-toggle="modal" data-target="#display_schedule_' . $j . '" ></i></button></a>';
                                                            echo '</td>';
                                                            ?>

                                                            <!-- Script to call unique frames with id for scheduling call on click on schedule btn--->        
                                                    <script>
                                                        $("#btn_schedule_<?php echo $j; ?>").click(function () {
                                                            var iframe = $("#schedule_call_frame_<?php echo $j; ?>");//defining the id of iframe
                                                            //defining source to the iframe
                                                            iframe.attr("src", "schedule_date.php?uid=<?php echo $uid_completed[$j]; ?>&ssn=<?php echo $ssn_completed[$j]; ?>&page_id=client");
                                                        });
                                                    </script>

                                                    <!--- This unique id of <div> hekps to open unique modal on click of each button--->
                                                    <!-- This modal is used to open schedule call-->
                                                    <div id="schedule_call_<?php echo $j; ?>" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="window.location.reload()">x</button>
                                                                    <h4 class="modal-title">Schedule Call</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <!-- through the id,defined earlier in the script, we define the source for the iframe-->
                                                                    <iframe id="schedule_call_frame_<?php echo $j; ?>" frameborder="0" scrolling="no"  height="430px" width="100%"></iframe> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--- This unique id of <div> hekps to open unique modal on click of each button--->
                                                    <!-- This modal is used to open confirmation modal for dismissial-->
                                                    <div id='display_schedule_<?php echo $j; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="window.location.reload()">x</button>
                                                                    <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                </div>
                                                                <div>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                        Are you sure you want to dismiss this ?
                                                                        <div style="text-align:center;">
                                                                            <input type="submit"  name="confirm-call-btn_<?php echo $j; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                            <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                        </div>
                                                                    </form>
                                                                    <?php
                                                                    #on click of 'yes'(dismisal),proceed the following
                                                                    if (isset($_REQUEST["confirm-call-btn_$j"])) {
                                                                        #insert into thrp_scheduled_calls table but make diaplay as 0 and call_status as 0.
                                                                        $stmt700 = $dbh->prepare("INSERT into thrp_scheduled_calls VALUES ('','$tid','$uid_completed[$j]','$ssn_completed[$j]','','','0','0','') ");
                                                                        $stmt700->execute(array()) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-1101].");

                                                                        #also update thrp_scheduled_calls .set scheduled_satus as 0.
                                                                        $stmt70 = $dbh->prepare("UPDATE thrp_user_ssn_completed SET scheduled_status='1' WHERE tid=? AND uid=? AND completed_on=? LIMIT 1");
                                                                        $stmt70->execute(array($tid, $uid_completed[$j], $completed_on[$j])) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCHEDULE-1102].");
                                                                        echo '<script>window.location.href="client_schedule.php";</script>'; //reload the page
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                    </tr><?php
                                                }
                                            } else {
                                                //if no clients are completed ,then display message as no clients to schedule.
                                                echo "No Clients To Schedule.";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                        var dt = new Date();
                        document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
    </body>
</html>



