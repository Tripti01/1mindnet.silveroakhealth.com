<?php
#including functions 
include '../if_loggedin.php';
include 'host.php';
include 'soh-config.php';
include 'functions/verify_token.php';

#to fetch tid from session
$tid = $_SESSION['tid'];

## Gettig UTM codes  and writing into the session ##
#UTM codes are written to keep track to the page from wchich the registartion process occurrs
if (isset($_REQUEST['utm_source'])) {
    $_SESSION['utm_src'] = $utm_src;
} else {
    $utm_src = '';
}
if (isset($_REQUEST['utm_medium'])) {
    $utm_med = $_REQUEST['utm_medium'];
} else {
    $utm_med = '';
}
if (isset($_REQUEST['utm_campaign'])) {
    $utm_cmp = $_REQUEST['utm_campaign'];
} else {
    $utm_cmp = '';
}
$utm_url = urlencode($_SERVER['REQUEST_URI']);
$_SESSION['utm_url'] = $utm_url;

# By default signup_status is zero.
$signup_status = 0;

#CBT start and end date calculation to display in form
date_default_timezone_set("Asia/Kolkata");
$start_date = date('d-m-Y');
$end_date = date('d-m-Y', strtotime("+10 week"));
#to fetch current datetime
$date = date('Y-m-d h:i:s');

#on click of start-submut-button
if (isset($_REQUEST['start-submit-btn'])) {
    #includng functions and other files
    require_once 'functions/crypto_funtions.php';
    include 'coach_function/success_clnc_func.php';
    include 'functions/token.php';

    #to verify token
    #fetch token from form as well as from the session
    $token_session = $_SESSION['register-form-rbth_token'];
    $token_form = $_REQUEST['form_token'];

    #if both token are same, proceed further, else display message
    if ($token_session == $token_form) {

        # Getting User details
        $user_name = $_REQUEST['name'];
        $user_email = $_REQUEST['email'];
        $user_c_email = $_REQUEST['c_email'];
        $user_mobile = $_REQUEST['mobile'];

        #encrypting user name
        $user_encrypted_name = encrypt($user_name, $encryption_key);

        # Writing user details into the session
        $_SESSION['user_name'] = $user_name;
        $_SESSION['user_email'] = $user_email;
        $_SESSION['user_mobile'] = $user_mobile;

        # Getting utm details
        $utm_src_req = $_REQUEST['utm_src'];
        $utm_med_req = $_REQUEST['utm_med'];
        $utm_cmp_req = $_REQUEST['utm_cmp'];
        $utm_url_req = $_REQUEST['utm_url'];

        # Writing details into the session
        $_SESSION['product'] = 'SC';
        $_SESSION['ref_type'] = "INTH";
        $_SESSION['token_session'] = $token_session;

        #santizing emails
        $user_email = filter_var($user_email, FILTER_SANITIZE_EMAIL);
        $user_c_email = filter_var($user_email, FILTER_SANITIZE_EMAIL);

        // Make Sure we dont have any blank or empty field
        if (isset($user_name) && !empty($user_name) && $user_name !== '' && isset($user_email) && !empty($user_email) && $user_email !== '' && isset($user_mobile) && !empty($user_mobile) && $user_mobile !== '') {

            //comparing btoh email and confirm email,if same then proceed further
            if ((strcmp($user_email, $user_c_email) == 0)) {
                $dbh = new PDO($dsn_sco, $login_user, $login_pass);
                $dbh->query("use sohdbl");

                // Check if an already exits with this email address. If an entry in table +user_temp+ then an account exists with this email addres, may or may not be activated account
                $stmt01 = $dbh->prepare("SELECT * FROM user_temp WHERE email=? LIMIT 1");
                $stmt01->execute(array($user_email));
                if ($stmt01->rowCount() != 0) {
                    # One row exits in database that means some account already exists. Now check if the account is activated or not.
                    # If account is activated, Display your account already exits, Please login
                    # IF account not activated redirect
                    $result = $stmt01->fetch();

                    if ($result['activated'] == 0) {
                        # Account is not activated, display message, to activate your mail
                        $signup_status = 7;
                    } else {
                        # Account is activated, Will suggest to login or recover password
                        $signup_status = 2;
                    }
                } else {
                    # No row exits in table with this email, hence an account does not exits.
                    # Before creating a new account,we will have to check for the license details of the clincians
                    #to fetch the number of users for whom licenses have been purchased
                    $stmt20 = $dbh->prepare("SELECT users FROM thrp_license WHERE tid=? LIMIT 1");
                    $stmt20->execute(array($tid));
                    if ($stmt20->rowCount() != 0) {
                        $result20 = $stmt20->fetch();
                        $users = $result20['users'];

                        #to fetch the user count of the therapist assigned
                        $stmt30 = $dbh->prepare("SELECT user_count FROM thrp_users_count WHERE tid=? LIMIT 1");
                        $stmt30->execute(array($tid));
                        if ($stmt30->rowCount() != 0) {
                            $result30 = $stmt30->fetch();
                            $user_count = $result30['user_count'];

                            #to check the users count, if the number of users license was purchased is greater than user count then proceed further.
                            #else display message as number of users exceeded
                            if ($users > $user_count) {

                                #to fetch the license end date of therapist
                                $stmt40 = $dbh->prepare("SELECT end_date FROM thrp_license WHERE tid=? LIMIT 1");
                                $stmt40->execute(array($tid));

                                if ($stmt40->rowCount() != 0) {
                                    $result40 = $stmt40->fetch();
                                    $end_date = $result40['end_date'];
                                }

                                #to check the license expiry date
                                #if the current datetime is less than the end date of license then proced further.else dispaly message as license expired.
                                if ($date < $end_date) {
                                    # So we will create a new account here
                                    # Generating utm code
                                    $utm_code = create_unique_id();

                                    # Generating ref token
                                    $ref_type = "INTH";
                                    $ref_token = $tid;

                                    # Generating additional_info code
                                    $add_info_code = create_unique_id();

                                    # Encrypt Name
                                    $encrypted_name = encrypt($user_name, $encryption_key);

                                    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                    $dbh->beginTransaction();
                                    try {
                                        # Inserting new row in the user temp table
                                        $stmt03 = $dbh->prepare("INSERT INTO user_temp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
                                        $stmt03->execute(array($user_email, $user_encrypted_name, '', $user_mobile, $ref_type, $ref_token, $utm_code, $add_info_code, '', '', $date, '0', '', ''));

                                        # Inserting new row in the user utm tracking table
                                        $stmt04 = $dbh->prepare("INSERT INTO user_utm_tracking VALUES(?,?,?,?,?,?,?);");
                                        $stmt04->execute(array($utm_code, $utm_src_req, $utm_med_req, $utm_cmp_req, $utm_url_req, '', ''));

                                        # Inserting a new row in the user additional info table
                                        $stmt05 = $dbh->prepare("INSERT INTO user_add_info VALUES(?,?,?,?,?,?,?,?);");
                                        $stmt05->execute(array($add_info_code, '', '', '', '', '', '', ''));

                                        $dbh->commit();
                                        # to make entries in all database tables
                                        $signup_status = success_clnc($user_email);
                                    } catch (PDOException $e) {
                                        $dbh->rollBack();
                                        $signup_status = 8;
                                    }
                                } else {
                                    //license expired
                                    $signup_status = 3;
                                }
                            } else {
                                //number of users exceed
                                $signup_status = 4;
                            }
                        }
                    }
                }
            } else {
                //email is not verified
                $signup_status = 1;
            }
        } else {
            //  something is wrong the client side validation 
            //  either the client has disabled the javascript stop the execution of the script
            die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_CREATE_CLIENT_JS_MSNG");
        }
    } else {
        #token not verified
        die("Some error occured. Please try again.. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_CREATE_CLIENT-1000");
    }
}
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Create Client</title>
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../assets/plugins/morris/morris.css">
        <!-- App css -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <!--script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                                (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '../../../www.google-analytics.com/analytics.js', 'ga');
                ga('create', 'UA-74137680-1', 'auto');
                ga('send', 'pageview');
            </script-->
        <style>

            .hr_css {
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(0, 0, 0, 0.95), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
                margin-top: -10px;
                margin-bottom: 20px;
            }
            .label_css{
                text-align: left;
                margin-bottom: 0px;
                color:black;
                margin-left: 10px;
                margin-top: 2px;
                font-family: 'Calibri';
                font-size: 16px;
            }
        </style>
    </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="y-overflow:auto;">
                                <div class="card-box">
                                    <article>
                                        <h5 class="title">
                                            Please provide the following details
                                        </h5>
                                        <section>
                                            <div class="row">
                                                <!--To display error messgaes --->
                                                <div class="email-confirm-msg" style="margin-top:20px;">
                                                    <?php
                                                    if ($signup_status == 0) {//help text
                                                        echo' <div style="color:#C0C0C0;margin-left:-3%;">
                                                <!-- //help text-->
                                                 After successfully filling the details, click "Start CBT" button and the client will receive a link to start the sessions.
                                              </div>';
                                                    } else if ($signup_status == 1) {//Email is not verified
                                                        echo '<div class="alert alert-danger">';
                                                        echo 'Email is not verified';
                                                        echo '</div>';
                                                    } else if ($signup_status == 2) {//acount already exists
                                                        echo '<div class="alert alert-danger">';
                                                        echo 'Account already exists.';
                                                        echo '</div>';
                                                    } else if ($signup_status == 3) {//License is expired
                                                        echo '<div class="alert alert-danger">';
                                                        echo 'License is expired';
                                                        echo '</div>';
                                                    } else if ($signup_status == 4) {//Number of users exceeded
                                                        echo '<div class="alert alert-danger">';
                                                        echo 'Number of users exceeded';
                                                        echo '</div>';
                                                    } else if ($signup_status == 5) {//Email sent
                                                        echo '<div class="alert alert-success">Email sent to <b>' . $user_email . '.New client created !</b></div>';
                                                    } else if ($signup_status == 6) {//Client not created 
                                                        echo '<div class="alert alert-danger">Client not created ! Please Try again !</div>';
                                                    } else if ($signup_status == 7) {//acount already exists,but activate
                                                        echo '<div class="alert alert-danger">Account already exists.</div>';
                                                    } else if ($signup_status == 8) {//Transaction failed
                                                        echo '<div class="alert alert-danger">Transaction failed in between.</div>';
                                                    } else {//general error message
                                                        echo '<div class="alert alert-danger">';
                                                        echo 'Error Occurred!!!';
                                                        echo '</div>';
                                                    }
                                                    ?>
                                                </div>
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" name="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-md-12 label_css" style="margin-top: 18px">Name</label>
                                                    </div>
                                                    <hr class="hr_css"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="userName"  style="font-weight:normal;text-align:left;">Client name <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Enter full name of client" class="form-control" id="name" name="name"/>
                                                            <div id="err_name" class="error"></div>
                                                        </div> 
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-12 label_css">Contact Details</label>
                                                    </div>
                                                    <hr class="hr_css"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="email"  style="font-weight:normal;text-align:left;">Email address <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="email" name="email" placeholder="Enter email address" class="form-control" id="email" onpaste="return false;" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="c_email"  style="font-weight:normal;text-align:left;">Confirm email address <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="email" placeholder="Confirm email address" class="form-control" id="c_email" name="c_email" onpaste="return false;" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="mobile"  style="font-weight:normal;text-align:left;">Mobile / Phone number <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Mobile / Phone number" class="form-control" id="mobile" name="mobile" >
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-12 label_css">Stress Control Online- CBT Schedule Dates</label>
                                                    </div>
                                                    <hr class="hr_css"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="start_date" style="font-weight:normal;text-align:left;">CBT Start Date <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="<?php echo $start_date; ?>" class="form-control" id="start_date" name="start_date" disabled/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="end_date"  style="font-weight:normal;text-align:left;">CBT End Date <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="<?php echo $end_date; ?>" class="form-control" id="end_date" name="end_date" disabled />
                                                        </div>
                                                    </div> 
                                                    <div class="form-group text-right m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                                        <input type="hidden" name="form_token" value="<?php echo form_verify("register-form-rbth"); ?>">
                                                        <input type="hidden" name="utm_src" value="<?php echo $utm_src ?>">
                                                        <input type="hidden" name="utm_med" value="<?php echo $utm_med ?>">
                                                        <input type="hidden" name="utm_cmp" value="<?php echo $utm_cmp; ?>">
                                                        <input type="hidden" name="utm_url" value="<?php echo $utm_url; ?>">
                                                        <input type="submit" id="start-submit-btn" name="start-submit-btn" value="Start CBT" class="btn btn-success"/>
                                                        <button type="reset" class="btn btn-danger" style="margin-right:10px;">Clear Form</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </section>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type='text/javascript'>
            if (window.self !== window.top) {
                window.top.location.href = window.location.href;
            }
        </script>
        <!---Form validation---->
        <script>
            $(".form-horizontal").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    c_email: {
                        required: true,
                        equalTo: "#email"
                    },
                    mobile: {
                        required: true,
                        phoneUS: true
                    }

                },
                messages: {
                    name: {
                        required: "Please enter your full name"
                    },
                    email: {
                        required: "Please enter your email",
                        email: "Invalid Email ID"
                    },
                    c_email: {
                        required: "Please enter same email address",
                        email: "Invalid Email ID"
                    },
                    mobile: {
                        required: "Please enter your mobile number",
                        maxlength: "Please enter a valid mobile number"
                    }
                }

            });
        </script>
        <script type="text/javascript">
            //script to enter only alphabets in text box
            $("#name").focusout(function () {
                var name_text = document.getElementById("name").value;

                if ((name_text.match(/^[a-zA-Z\s]+$/) == null) && (name_text != ""))
                {
                    $("#err_name").html("Invalid name");

                }
            });
        </script>
    </body>
</html>