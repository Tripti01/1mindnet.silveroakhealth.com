<?php
#including functions
require '../if_loggedin.php';
include 'soh-config.php';
include 'host.php';

#to get tid from session
$tid = $_SESSION['tid'];

#declaring variables
$total_referals = 0; //to calculate total referrals sent
$total_activated_ref = 0; //to calculate activated referrals
$total_notyet_signed = 0; //to calculate users who are not yet signed
# Start the database 
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Query database and fetch all the users referred by this therapist
$i = 0; //counter for referrals
$j = 0; //counter for activated referrals
$stmt1 = $dbh->prepare("SELECT * FROM user_reference WHERE ref_value=? ORDER BY sent_datetime ;");
$stmt1->execute(array($tid))or die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. Error Code : VIEW-REF-1000.");
if ($stmt1->rowCount() != 0) {
    while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
        $name[$i] = $row1['to_name']; //to fecth name
        $ref_on[$i] = date('d-m-Y', strtotime($row1['sent_datetime'])); //to display date on whch link was sent
        #if the user is an activated user display datetime of activation ,else display nothing.
        if (($row1['activated_datetime']) == '0000-00-00 00:00:00') {
            //not activated, hence display nothing
            $activated_datetime[$i] = "";
        } else {
            $activated_datetime[$i] = date('d-m-Y', strtotime($row1['activated_datetime'])); //to display actuvated datetime
            $j++; //count for activated users
        }
        $activated[$i] = $row1['activated']; //to fetch activated status
        $mobile[$i] = $row1['to_mobile']; //to fetch modile deatil
        $email[$i] = $row1['to_email']; //to fetch email to which referral was sent
        $i++;
    }
}
$total_referals = $i; //to calculate total referrals sent
$total_activated_ref = $j; //to calculate activated referrals
$total_notyet_signed = $i - $j; //to calculate users who are not yet signed
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Referrals</title>
        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <style>
            .dataTables_length{
                display: none;
            }
            .dataTables_filter{
                display: none;
            }
            .dataTables_info{
                color:#C0C0C0;
            }
            .title_color{
                color:#223C80;
                font-size: 35px;
                margin-bottom: 20px;
                padding-top:15px;
            }
            .table-striped > tbody > tr:nth-of-type(odd), .table-hover > tbody > tr:hover, .table > thead > tr > td.active, .table > tbody > tr > td.active, .table > tfoot > tr > td.active, .table > thead > tr > th.active, .table > tbody > tr > th.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > tbody > tr.active > td, .table > tfoot > tr.active > td, .table > thead > tr.active > th, .table > tbody > tr.active > th, .table > tfoot > tr.active > th {
                background-color: #fff !important;
            }
            .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
                background-color: #3498db;
                border-color:  #3498db;
            }
            .tooltip fade bottom in{
                left:0px;
            }
        </style>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" style="height:75%;margin-top:3%;width:55%;">
                    <!--a href="index.php" class="logo" ><p style="margin-top:2%;font-size: 15px;"><span>CLINICIAN'S <span> PRO+</span></span></p></a-->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">View Referrals</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li>  
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <?php
                //based on the thrp_type, sidebar changes according to privilages.
                if ($_SESSION['type'] == "CLNC") {
                    include 'sidebar_clnc.php';
                } else if ($_SESSION['type'] == "THRP") {
                    include 'sidebar_coach.php';
                }
                ?>
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-4 col-md-4" style="padding-left: 0px;">
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $total_referals; ?></h1>
                                        <h4>Referred</h4>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4" >
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $total_activated_ref; ?></h1>
                                        <h4>Activated</h4>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4" style="padding-right: 0px;">
                                    <div class="card-box" style="text-align:center;">
                                        <h1 class="header-title m-t-0 title_color"><?php echo $total_notyet_signed; ?></h1>
                                        <h4>Not Yet Signed</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <article style="padding-left: 10px;padding-right: 10px;">
                                <h5 class="title">
                                    Client Referrals And Status
                                </h5>

                                <div class="panel-body">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead style="background-color:#3498db;">
                                            <tr>
                                                <th>#</th>
                                                <th>Client Name &nbsp;
                                                </th>
                                                <th>Referred On</th>
                                                <th>Signed On</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($k = 0; $k < $total_referals; $k++) {
                                                $sr_no = ($k + 1); //to fecth serial number
                                                ?>
                                                <tr>
                                                    <td><?php echo $sr_no; ?></td>
                                                    <td><div data-toggle="tooltip" data-placement="bottom" class="red-tooltip" title="" data-html="true"
                                                             data-original-title="<?php echo $name[$k]; ?><br><?php echo $email[$k]; ?><br><?php echo $mobile[$k]; ?>">
                                                                 <?php echo $name[$k]; ?>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $ref_on[$k]; ?></td>
                                                    <td>
                                                        <span style="display:none"> <?php echo $activated_datetime[$k]; ?></span>
                                                        <!-- data table library uses Y-m-d format for sorting but for readibility purpose we hide this format and added d-m-Y format -->																 
                                                        <span>  <?php echo $activated_datetime[$k]; ?> </span>
                                                    </td>
                                                    <td><?php
                                                                 #if user is activated display as signed else display as not yet signed
                                                                 if ($activated[$k] == 0) {
                                                                     //not activated
                                                                     echo ' <span class="label label-danger"> Not Yet Signed</span>';
                                                                 } else if ($activated[$k] == 1) {
                                                                     //activated
                                                                     echo ' <span class="label label-success"> Signed </span>';
                                                                 }
                                                                 ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </article>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->
                </div> 
            </div> 
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                        var dt = new Date();
                        document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <!-- Datatables-->
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.print.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

            $(".dataTables_info").replaceWith("<h2>New heading</h2>");
        </script>
    </body>
</html>