<?php

//this function is used to get the cid(caller id) for a given employee id and corp id
//First, emp_id and corp_id is check in call_callers_profile if its present or not
//If emp_id and corp_id is found then the cid is fetched and returned
//If not a new cid is created and returned

function get_cid($emp_id, $corp_id, $tid) {

    //include files
    include 'ewap-config.php';

# Start the database realated stuff
    $dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
    $dbh->query("use scodd");

#to check if email exsists or not
    $stmt00 = $dbh->prepare("SELECT cid FROM call_callers_profile WHERE emp_id=? AND corp_id=? LIMIT 1");
    $stmt00->execute(array($emp_id, $corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CID-1000].");
    if ($stmt00->rowCount() != 0) {//if exsists fetch cid
        $row00 = $stmt00->fetch();
        $cid = $row00['cid']; //to fetch cid
    } else {//if email doesnt exsists, create a cid and store it in call_callers_profile
        $cid_first_half = "EWAP"; //intialize first_half
//to fetch cvalue
        $stmt10 = $dbh->prepare("SELECT value FROM cvalue WHERE param=? LIMIT 1");
        $stmt10->execute(array('cid'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CID-1001].");
        if ($stmt10->rowCount() != 0) {//if exsists fetch cid
            $row10 = $stmt10->fetch();
            $cid_second_half = $row10['value'];
        }

        # Increment the running sequnce of cid saved in the database
        $stmt20 = $dbh->prepare("UPDATE cvalue SET value=value+1 WHERE param='cid' LIMIT 1");
        $stmt20->execute(array());

        //create new cid
        $cid = $cid_first_half . $cid_second_half;
        #current curret time
        date_default_timezone_set("Asia/Kolkata");
        $timestamp = date('Y-m-d h:i:s');
        $comment = "New Caller";

        //to fetch name, mobile, gender, yob, location, corp_id, emp_id
        $stmt30 = $dbh->prepare("SELECT name, email, mobile, gender, yob, location FROM call_schedule_queue WHERE emp_id=? AND corp_id=? LIMIT 1");
        $stmt30->execute(array($emp_id, $corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CID-1002].");
        if ($stmt30->rowCount() != 0) {//if exsists fetch cid
            $row30 = $stmt30->fetch();
            $name = $row30['name']; //to fetch encrypted name
            $mobile = $row30['mobile']; //to fetch mobile
            $gender = $row30['gender']; //to fetch gender
            $yob = $row30['yob']; //to fetch yob
            $location = $row30['location']; //to fetch location
            $email = $row30['email']; //to fetch emp_id
        }

        //insert into call_callers_profile
        $stmt40 = $dbh->prepare("INSERT INTO call_callers_profile VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt40->execute(array($cid, $name, $email, $mobile, $gender, $yob, $location, $corp_id, $emp_id, '1', '', '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CID-1003].");

        //insert into call_cid_tid_list
        $stmt50 = $dbh->prepare("INSERT INTO call_cid_tid_list VALUES (?,?,?,?,?,?,?,?)");
        $stmt50->execute(array($cid, $tid, $timestamp, $comment, '1', '', '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CID-1004].");
    }

    //return cid
    return $cid;
}

?>