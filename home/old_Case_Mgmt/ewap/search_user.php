<?php
	include '../if_loggedin.php';
	include 'ewap-config.php';
	include 'host.php';
	include 'get_name.php';
	
	# Start the database realated stuff
	$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
	$dbh->query("use scodd");
		
	$i = 0; //counter for total number of callers 
	$stmt10 = $dbh->prepare(" SELECT  p.name, p.email, p.mobile FROM call_callers_profile p WHERE 1");
	$stmt10->execute()or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-1001].");
	if ($stmt10->rowCount() != 0) {
		while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
			$prev_name[$i] = $row10['name']." (Mobile: ".$row10['mobile'].")"; //to fetch name list
			$prev_email[$i] = $row10['email']." (Mobile: ".$row10['mobile'].", Name: ".$row10['name'].")"; //to fetch email list
			$prev_mobile[$i] = $row10['mobile']. " (Name: ".$row10['name'].")"; //to fetch mobile list
			$i++;
		}
	}
	
	if(isset($_REQUEST['search'])){
		if(isset($_REQUEST['s_type'])){
			if($_REQUEST['s_type'] == 'name'){
				$type = $_REQUEST['s_type'];
				
				$arr = explode(" (", $_REQUEST['name'], 2);
				$name = $arr[0];
				
				$value = '%'.$name.'%';
			}elseif($_REQUEST['s_type'] == 'email'){
				$type = $_REQUEST['s_type'];
				
				$arr = explode(" (", $_REQUEST['email'], 2);
				$email = $arr[0];
				
				$value = '%'.$email.'%';
			}elseif($_REQUEST['s_type'] == 'mobile'){
				$type = $_REQUEST['s_type'];
				
				$arr = explode(" (", $_REQUEST['mobile'], 2);
				$mobile = $arr[0];
				
				$value = '%'.$mobile.'%';
			}
		}
		
		$stmt00 = $dbh->prepare(" SELECT  p.cid, p.name, p.email, p.mobile, p.corp_id, p.emp_id FROM call_callers_profile p  WHERE  p.".$type." LIKE ? ");
		$stmt00->execute(array($value))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [SEARCH-1002].");
	
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Search EWAP Clients</title>
       
        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        
        <style>
			.checkbox input[type=checkbox] {
				cursor: pointer;
				opacity: 0;
				z-index: 1;
				outline: 0!important;
				width: 200px;
				
			}
            .tooltip fade bottom in{
                left:0px;
            }
            .dataTables_length{
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .dataTables_info{
                display: none;
            }
            td{
                padding-left: 0px;
                padding-right: 0px;
            }
        </style>

    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
					<a class="btn btn-primary" style="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin:10px;" 
								href="new_client.php" target="_blank">Create New Client</a>
                    
					<div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
									<form action="search_user.php" method="POST" class="form-horizontal" id="form-horizontal" novalidate="novalidate">
                                        <div id="err_name" class="msg_disp"></div>
                                         <div class="form-group" style="margin-bottom:3%;" >
													<div class="row">
														<div class="col-md-2"></div>
														<div class="col-md-3" style="margin-top:1%;" ><h4><b>Search Client By</b></h4></div></br>
													</div>
													<div class="row">
														<div class="col-md-2"></div>
														<div class="col-md-2 checkbox checkbox-inline checkbox-circle checkbox-purple" style="margin-left:20px">
															<input type="checkbox"  name="s_type" id="s_type1" value="name"  onClick="EnableTextbox('NAME')" >
															<label for="s_type"> Name </label>
														</div>

														<div class="col-md-2 checkbox checkbox-inline checkbox-circle checkbox-purple">
															<input type="checkbox" name="s_type" id="s_type2" value="mobile"   onClick="EnableTextbox('MOBILE')">
															<label for="s_type">Mobile </label>
														</div>
														<div class="col-md-2 checkbox checkbox-inline checkbox-circle checkbox-purple">
															<input type="checkbox" name="s_type" id="s_type3" value="email"   onClick="EnableTextbox('EMAIL')">
															<label for="s_type">Email </label>
														</div>
													</div>
                                          </div>
										  <div class="row">
												<div class="col-sm-2"></div>
												<div class="col-sm-6" id="add_input"></div>
                                                <div class="col-sm-2">
                                                    <input type="submit" value="Search" name="search" class="btn btn-primary" id="search" style="display:none;" />
                                                </div>
                                          </div>
										  
									</form>
								<?php	if(isset($_REQUEST['search'])){ ?>
									<div class="row" style="margin-bottom:1.5%;">
										 <div class="col-md-12">
											<div class='table-scrollable table-scrollable-borderless'>
											   <table id='datatable' class='table table-striped table-bordered'>
												   <thead>
													   <tr> 
														   <th> CallerID </th>
														   <th> Company Name </th>
														   <th> Employee ID </th>
														   <th> Name </th>
														   <th> Email </th>
														   <th> Mobile </th>
														   <th id="da"> Date </th>
														   <th></th>
													   </tr>
													</thead>
													<tbody>
														<?php												
															 if ($stmt00->rowCount() != 0) {
																	$rows_010 = $stmt00->fetchAll();																				
																	foreach ($rows_010 as $row00){
																		echo '<tr>';
																		echo '<td>' . $row00['cid'] . '</td>';
																		echo '<td>' . get_corp_name($row00['corp_id']) . '</td>';
																		echo '<td>' . $row00['emp_id'] . '</td>';
																		echo '<td>' . $row00['name']. '</td>';
																		echo '<td>' . $row00['email'] . '</td>';
																		echo '<td>' . $row00['mobile'] . '</td>';
																		echo '<td style="text-align:center;"><a href="caller_details.php?cid=' . $row00['cid'] . '"><button type="button" class="btn btn-primary btn-bordred waves-effect btn-sm waves-light">View</button></a></td>';
																		echo '</tr>';
																	}
															 }
														  ?>   
													</tbody>
												</table>
											</div>
										 </div>
									</div>
								<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/detect.js"></script>
		<script src="../assets/js/fastclick.js"></script>
		<script src="../assets/js/jquery.slimscroll.js"></script>
		<script src="../assets/js/jquery.blockUI.js"></script>
		<script src="../assets/js/waves.js"></script>
		<script src="../assets/js/jquery.nicescroll.js"></script>
		<script src="../assets/js/jquery.scrollTo.min.js"></script>

		<!-- Form wizard -->
		<script src="../assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
		<!-- App js -->
		<script src="../assets/js/jquery.core.js"></script>
		<script src="../assets/js/jquery.app.js"></script>
		<script src="../assets/js/app.min.js"></script>
				
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

       <script>

                function EnableTextbox(type)
                {

                    if (type == 'NAME') {
						$('#d_email').remove();
						$('#d_mobile').remove();
						$('#search').show();
                        $("#add_input").html('<div class="form-group" id="d_name"> \
                                              <div class="col-md-12" id="name"> \
                                                  <input class="form-control" type="text" name="name" placeholder="Name" style="width:100%;" id="u_name" >\
                                              </div>\
                                          </div>');
						
						
						var availableName = <?php echo json_encode($prev_name); ?>;
						$( "#u_name" ).autocomplete({
						  source: availableName
						});
						
                    }

                    if (type == 'EMAIL') {
						$('#d_name').remove();
						$('#d_mobile').remove();
						$('#search').show();
                        $("#add_input").html('<div class="form-group" id="d_email"> \
                                              <div class="col-md-12" id="email"> \
                                                  <input class="form-control" type="text" name="email" placeholder="Email" style="width:100%;" id="u_email" >\
                                              </div>\
                                          </div>');
										  						
						var availableEmail = <?php echo json_encode($prev_email); ?>;										  
						$( "#u_email" ).autocomplete({
						  source: availableEmail
						});
                    }
					if (type == 'MOBILE') {
						$('#d_name').remove();
						$('#d_email').remove();
						$('#search').show();
                        $("#add_input").html('<div class="form-group" id="d_mobile"> \
                                              <div class="col-md-12" id="mobile"> \
                                                  <input class="form-control" type="text" name="mobile" placeholder="Mobile" style="width:100%;" id="u_mobile" >\
                                              </div>\
                                          </div>');
										  
						
						var availableMobile = <?php echo json_encode($prev_mobile); ?>;
						
						$( "#u_mobile" ).autocomplete({
						  source: availableMobile
						});
                    } 
                }

        </script> 
		<script>
            // the selector will match all input controls of type :checkbox
            // and attach a click event handler
            $("input:checkbox").on('click', function () {
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
        </script>
    </body>
</html>