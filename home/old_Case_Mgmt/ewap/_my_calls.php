<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'ewap-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_name.php';
#getting tid from session
$tid = $_SESSION['tid'];

//to fetch current year
$current_year = date("Y");

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");

#to fetch all the calls and caller details from call_schedule_call and caller_profile by ascending order of date 
$i = 0; //counter for total number of callers scheduled
$stmt00 = $dbh->prepare("SELECT call_schedule_call.sr_no AS sr_no, name, yob, gender, email, mobile, prefered_date, prefered_time, call_schedule_call.cid AS cid, location, corp_id, emp_id  FROM call_schedule_call,call_callers_profile WHERE tid=? AND call_done=? AND call_schedule_call.cid = call_callers_profile.cid ORDER BY prefered_date ASC ");
$stmt00->execute(array($tid, '0'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY-CALL-1000].");
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $sr_no[$i] = $row00['sr_no']; //to fetch serial number
        $encrypt_name[$i] = $row00['name']; //to fetch encrypted name
        $name[$i] = decrypt($encrypt_name[$i], $encryption_key); //to fetch decrypted name 
        $yob[$i] = $row00['yob']; //to fetch yob
        $age[$i] = $current_year - $yob[$i]; //to fecth age
        if ($row00['gender'] == "M") {//if gender is M, then proceed
            $gender[$i] = "Male"; //set as masle
        } else if ($row00['gender'] == "F") {//if gender is F, then proceed
            $gender[$i] = "Female"; //set as female
        } else if ($row00['gender'] == "O") {//if gender is O, then proceed
            $gender[$i] = "Other"; //set as other
        } else if ($row00['gender'] == "W") {//if gender is W, then proceed
            $gender[$i] = "Would rather not say"; //set as would rather not say
        }
        $email[$i] = $row00['email']; //to fetch email
        $mobile[$i] = $row00['mobile']; //to fetch mobile
        $cid[$i] = $row00['cid']; //to fetch cid
        $prefered_date[$i] = $row00['prefered_date']; //to fetch prefered date of call 
        $prefered_time[$i] = $row00['prefered_time']; //to fetch prefered time(text)
        $location[$i] = $row00['location']; //to fetch location
        $corp_id[$i] = $row00['corp_id']; //to fetch corp_id
        $emp_id[$i] = $row00['emp_id']; //to fetch emp_id
        $i++;
    }
}

//to fetch list of all coaches from sco
$dbh_sco = new PDO($dsn_sco, $login_user, $login_pass);
$dbh_sco->query("use sohdbl");

$j = 0; //counter for all coaches
$stmt10 = $dbh_sco->prepare("SELECT name AS thrp_name, thrp_login.tid FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = ?");
$stmt10->execute(array("THRP"))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY-CALL-1001].");
if ($stmt10->rowCount() != 0) {
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $thrp_tid_list[$j] = $row10['tid']; //to fetch tid list
        $thrp_name_list[$j] = decrypt($row10['thrp_name'], $encryption_key); //to fetch thrp_name_list
        $j++;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>My Upcoming Calls</title>
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>

        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

        <style>
            .tooltip fade bottom in{
                left:0px;
            }
            .dataTables_length{
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .dataTables_info{
                display: none;
            }
            td{
                padding-left: 0px;
                padding-right: 0px;
            }
        </style>

    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <div class='table-scrollable table-scrollable-borderless'>
                                        <table id='datatable' class='table table-striped table-bordered'>
                                            <thead>
                                                <tr>
                                                    <th width="3%" style="margin-left:2px;padding-right: 0px;"></th>
                                                    <th width="16%" style="margin-left:2px;padding-right: 0px;">Name</th>
                                                    <th width="11%" style="margin-left:2px;padding-right: 0px;" >Company</th>
                                                    <th width="9%" style="margin-left:2px;padding-right: 0px;">Mobile</th>
                                                    <th width="12%" style="margin-left:2px;padding-right: 0px;">Age/Gender </th>
                                                    <th width="13%" style="margin-left:2px;padding-right: 0px;">Preferred Date</th>
                                                    <th width="13%" style="margin-left:2px;padding-right: 0px;">Preferred Time</th>
                                                    <th width="18%" style="text-align:center;padding-left: 0px;padding-right: 0px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (isset($cid)) {
                                                    for ($i = 0; $i < count($cid); $i++) {

                                                        $seq_num = $i + 1;

                                                        echo '<tr>';
                                                        echo '<td style="padding-right: 0px;">' . $seq_num . '</td>';
                                                        echo '<td style="padding-right: 0px;"><a href=""><div data-toggle="tooltip" data-placement="bottom" data-html="true" class="red-tooltip" title="" 
                                                             data-original-title="Employee Id- ' . $emp_id[$i] . '<br>' . $email[$i] . '<br>' . $location[$i] . '<br>">' . $name[$i] . '</div></a></td>';
                                                        echo '<td style="padding-right: 0px;">' . get_corp_name($corp_id[$i]) . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $mobile[$i] . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $age[$i] . ' yrs / ' . $gender[$i] . ' </td>';
                                                        echo '<td style="padding-right: 0px;">' . date('d-M-Y', strtotime($prefered_date[$i])) . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $prefered_time[$i] . '</td>';
                                                        echo '<td style="text-align:center;padding-right: 0px;"><a href="caller_details.php?cid=' . $cid[$i] . '"><button type="button" class="btn btn-primary btn-sm waves-effect waves-light" style="margin-right:1%;">View</button></a>';
                                                        echo '<a><button type="button" class="btn btn-primary btn-sm waves-effect waves-light" style="margin-right:1%;" data-toggle="modal" data-target="#reschedule_calls_' . $i . '">Re-Schedule</button></a>';
                                                        ?>
                                                        <!-- Modal to reschedule call--->
                                                    <div id='reschedule_calls_<?php echo $i; ?>' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">

                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                                                    <h4 class="modal-title">Re-Schedule Call for <?php echo $name[$i]; ?></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="form-horizontal_<?php echo $i; ?>">
                                                                        <div id="reschedule_msg" class="msg_disp"></div>
                                                                        <div class="row">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Name</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" value="<?php echo $name[$i]; ?>" name="reschedule_name" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 2%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Email</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" value="<?php echo $email[$i]; ?>" name="reschedule_email" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 2%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Mobile</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" value="<?php echo $mobile[$i]; ?>" name="reschedule_mobile" disabled/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 2%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Coach</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <select class="form-control" name="tid_schedule">
                                                                                            <?php for ($j = 0; $j < count($thrp_tid_list); $j++) { ?>
                                                                                                <option value=<?php
                                                                                                echo $thrp_tid_list[$j];
                                                                                                if ($thrp_tid_list[$j] == $tid) {
                                                                                                    echo ' selected=""';
                                                                                                } else {
                                                                                                    
                                                                                                }
                                                                                                ?>> <?php echo $thrp_name_list[$j]; ?></option>
                                                                                                    <?php }
                                                                                                    ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 2%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Date</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <div class="input-group">
                                                                                            <input type="text" class="form-control" id="datepicker_<?php echo $i; ?>" placeholder="Select date here " name="reschedule_date">
                                                                                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 2%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Time</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <select class="form-control" name="reschedule_time">
                                                                                            <option value="7 AM - 7:30 AM">7 AM - 7:30 AM</option>
																							<option value="7:30 AM - 8 AM">7:30 AM - 8 AM</option>
                                                                                            <option value="8 AM - 8:30 AM">8 AM - 8:30 AM</option>
																							<option value="8:30 AM - 9 AM">8:30 AM - 9 AM</option>
                                                                                            <option value="9 AM - 9:30 AM">9 AM - 9:30 AM</option>
																							<option value="9:30 AM - 10 AM">9:30 AM - 10 AM</option>
                                                                                            <option value="10 AM - 10:30 AM">10 AM - 10:30 AM</option>
																							<option value="10:30 AM - 11 AM">10:30 AM - 11 AM</option>
                                                                                            <option value="11 AM - 11:30 PM">11 AM - 11:30 AM</option>
                                                                                            <option value="11:30 AM - 12 PM">11:30 AM - 12 PM</option>
                                                                                            <option value="12 PM - 12:30 PM">12 PM - 12:30 PM</option>
																							<option value="12:30 PM - 1 PM">12:30 PM - 1 PM</option>
                                                                                            <option value="1 PM - 1:30 PM">1 PM - 1:30 PM</option>
																							<option value="1:30 PM - 2 PM">1:30 PM - 2 PM</option>
                                                                                            <option value="2 PM - 2:30 PM">2 PM - 2:30 PM</option>
                                                                                            <option value="2:30 PM - 3 PM">2:30 PM - 3 PM</option>
                                                                                            <option value="3 PM - 3:30 PM">3 PM - 3:30 PM</option>
                                                                                            <option value="3:30 PM - 4 PM">3:30 PM - 4 PM</option>
                                                                                            <option value="4 PM - 4:30 PM">4 PM - 4:30 PM</option>
                                                                                            <option value="4:30 PM - 5 PM">4:30 PM - 5 PM</option>
                                                                                            <option value="5 PM - 5:30 PM">5 PM - 5:30 PM</option>
                                                                                            <option value="5:30 PM - 6 PM">5:30 PM - 6 PM</option>
                                                                                            <option value="6 PM - 6:30 PM">6 PM - 6:30 PM</option>
                                                                                            <option value="6:30 PM - 7 PM">6:30 PM - 7 PM</option>
                                                                                            <option value="7 PM - 7:30 PM">7 PM - 7:30 PM</option>
                                                                                            <option value="7:30 PM - 8 PM">7:30 PM - 8 PM</option>
                                                                                            <option value="8 PM - 8:30 PM">8 PM - 8:30 PM</option>
                                                                                            <option value="8:30 PM - 9 PM">8:30 PM - 9 PM</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 2%;">
                                                                            <div class="col-sm-12" style="text-align: center;margin-top:5%;">
                                                                                <input type="hidden" name="emp_id" value="<?php echo $emp_id[$i] ?>">
                                                                                <input type="hidden" name="modal_id" value="reschedule_calls_<?php echo $i; ?>">
                                                                                <input type="hidden" name="sr_no" value="<?php echo $sr_no[$i] ?>">
                                                                                <input type="hidden" name="prefered_date" value="<?php echo $prefered_date[$i] ?>">
                                                                                <input type="hidden" name="prefered_time" value="<?php echo $prefered_time[$i] ?>">
                                                                                <input type="hidden" name="reschedule_email" value="<?php echo $email[$i] ?>">
                                                                                <input type="hidden" name="reschedule_mobile" value="<?php echo $mobile[$i] ?>">
                                                                                <input type="hidden" name="encrypt_name" value="<?php echo $encrypt_name[$i] ?>">
                                                                                <input type="hidden" name="cid" value="<?php echo $cid[$i] ?>">
                                                                                <input type="hidden" name="corp_id" value="<?php echo $corp_id[$i] ?>">
                                                                                <input type="hidden" name="location" value="<?php echo $location[$i] ?>">
                                                                                <input type="hidden" name="gender" value="<?php echo $gender[$i] ?>">
                                                                                <input type="hidden" name="yob" value="<?php echo $yob[$i] ?>">
                                                                                <input type="submit" value="Re-Schedule Call" name="re_schedule_btn" class="btn btn-primary"/>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <?php
                                                                    #on click of reschedule button
                                                                    if (isset($_REQUEST['re_schedule_btn'])) {
                                                                        if (isset($_REQUEST['tid_schedule']) && !empty($_REQUEST['tid_schedule']) && $_REQUEST['tid_schedule'] !== '' && isset($_REQUEST['reschedule_date']) && !empty($_REQUEST['reschedule_date']) && $_REQUEST['reschedule_date'] !== '' && isset($_REQUEST['reschedule_time']) && !empty($_REQUEST['reschedule_time']) && $_REQUEST['reschedule_time'] !== '') {

                                                                            $sr_no = $_REQUEST['sr_no']; //to get serial number
                                                                            $modal_id = $_REQUEST['modal_id']; //to get modal id
                                                                            $reschedule_name = $_REQUEST['encrypt_name']; //to get name
                                                                            $reschedule_email = $_REQUEST['reschedule_email']; //to get email
                                                                            $reschedule_mobile = $_REQUEST['reschedule_mobile']; //to get mobile
                                                                            $date = str_replace('/', '-', $_REQUEST['reschedule_date']); //to get date
                                                                            $reschedule_date = date('Y-m-d', strtotime($date)); //to get date in format
                                                                            $reschedule_time = $_REQUEST['reschedule_time']; //to get time
                                                                            $tid_schedule = $_REQUEST['tid_schedule']; //to get tid 
                                                                            $cid = $_REQUEST['cid']; //to get cid
                                                                            $corp_id = $_REQUEST['corp_id']; //to get corp_id
                                                                            $location = $_REQUEST['location']; //to get location
                                                                            $gender = $_REQUEST['gender']; //to get gender
                                                                            $yob = $_REQUEST['yob']; //to get yob
                                                                            $emp_id = $_REQUEST['emp_id']; //to get emp_id
                                                                            $prefered_date = $_REQUEST['prefered_date']; //to get previous prefered date
                                                                            $prefered_time = $_REQUEST['prefered_time']; //to get previous prefered time
                                                                            //to update previous value into queue 
                                                                            $stmt10 = $dbh->prepare("UPDATE call_schedule_queue SET assigned = ? WHERE cid=? AND prefered_date=? AND prefered_time=? LIMIT 1");
                                                                            $stmt10->execute(array('2', $cid, $prefered_date, $prefered_time))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY-CALL-1002].");

                                                                            //to insert into queue 
                                                                            $stmt20 = $dbh->prepare("INSERT INTO call_schedule_queue VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                                                            $stmt20->execute(array('', $reschedule_name, $reschedule_email, $reschedule_mobile, $gender, $yob, $location, $reschedule_date, $reschedule_time, $corp_id, $emp_id, $cid, $tid_schedule, 0, 0, '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY-CALL-1003].");

                                                                            //delete from call_schedule_call
                                                                            $stmt30 = $dbh->prepare("DELETE FROM call_schedule_call WHERE sr_no = ? LIMIT 1");
                                                                            $stmt30->execute(array($sr_no))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-MY-CALL-1004].");

                                                                            # redirecting to the caller_Details
                                                                            echo '<script src="../assets/js/jquery.min.js"></script>
                                                                                        <script type="text/javascript"> 
                                                                                           $(document).ready(function(){
                                                                                              $("#reschedule_msg").addClass("alert");
                                                                                              $("#reschedule_msg").addClass("alert-success");
                                                                                              $("#reschedule_msg").html("Call has been re-scheduled.");
                                                                                              $("#reschedule_calls_' . $i . '").modal({"backdrop": "static"});
                                                                                          });
                                                                                       </script>';
                                                                        } else {
                                                                            //  something is wrong the client side validation 
                                                                            //  either the client has disabled the javascript stop the execution of the script
                                                                            die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_MY_CLIENT_JS_MSNG");
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                echo ""; //dont do anything
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                    var dt = new Date();
                    document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/fastclick.js"></script>

        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.print.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.scroller.min.js"></script>
        <script src="../assets/pages/datatables.init.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

            $(".dataTables_info").replaceWith("<h2>New heading</h2>");
        </script>
        <script>
            var date = new Date();
            date.setDate(date.getDate());

<?php for ($i = 0; $i < count($cid); $i++) { ?>
                $('#datepicker_<?php echo $i; ?>').datepicker({
                    startDate: date
                });
<?php } ?>

        </script>
        <script>
<?php for ($i = 0; $i < count($cid); $i++) { ?>
                $('#form-horizontal_<?php echo $i; ?>').validate({
                    rules: {
                        tid_schedule: {
                            required: true,
                        },
                        reschedule_date: {
                            required: true,
                        },
                        reschedule_time: {
                            required: true,
                        }
                    },
                    messages: {
                        tid_schedule: {
                            required: "Please Select Coach",
                        },
                        reschedule_date: {
                            required: "Please Select Re-Schedule Date",
                        },
                        reschedule_time: {
                            required: "Please Select Re-Schedule Time",
                        }
                    }
                });
<?php } ?>
        </script>
        <!-- function to reload page -->
        <script>
            function reload() {
                window.location.href = "<?php echo $host ?>/ewap/my_calls.php";
            }
        </script>
    </body>
</html>