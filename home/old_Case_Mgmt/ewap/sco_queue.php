<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'ewap-config.php';
include 'functions/crypto_funtions.php';
include 'get_name.php';

#getting tid from session
$tid = $_SESSION['tid'];

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");

#to fetch all the details from sco_registration_queue 
$i = 0; //counter for total number of callers in queue
$stmt00 = $dbh->prepare("SELECT * FROM sco_registration_queue WHERE 1 ORDER BY status DESC ,timestamp ASC ");
$stmt00->execute()or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode:[THRP-SCO-QUEUE-1000].");
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $sr_no[$i] = $row00['sr_no']; //to fetch serial number
        $caller_name[$i] = decrypt($row00['name'], $encryption_key); //to fetch decrypted name 
        $caller_email[$i] = $row00['email']; //to fecth email
        $caller_mobile[$i] = $row00['mobile']; //to fetch mobile
        $caller_corp_id[$i] = $row00['corp_id']; //to fetch corp_id
        $caller_emp_id[$i] = $row00['emp_id']; //to fetch emp_id
        $caller_status[$i] = $row00['status']; //to fetch status
        $caller_timestamp[$i] = $row00['timestamp']; //to fetch the timestamp of registration
        $caller_location[$i] = $row00['location'];//to fetch location
        $i++;
    }
}
$total_callers = $i; //total number of callers
//to get the status of sco request
function get_status($status) {
    if ($status == "P") {//if status is 'A', set status name as "approved"
        $status_name = "Approved";
    } else if ($status == "J") {//if status is 'R', set status name as "rejected"
        $status_name = "Rejected";
    } else {//else set as blank
        $status_name = "";
    }
    return $status_name;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>SCO Queue</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .dataTables_info{
                display: none;
            }
            .dataTables_length{
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
        </style>
    </head>
  <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card-box">
                                    <div class="table-responsive ">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <table id='datatable' class='table table-striped table-bordered'>
                                                <thead>
                                                    <tr style="text-align:center;">
                                                        <th>#</th>
                                                        <th style="color:#666666;">Name  </th>
                                                        <th>Location</th>
                                                        <th>Mobile</th>
                                                        <th>Company Name</th>
                                                        <th>Time Of Registration</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (isset($sr_no)) {
                                                        for ($k = 0; $k < $total_callers; $k++) {
                                                            $seq_no = $k + 1;

                                                            echo '<tr>';
                                                            echo '<td>' . $seq_no . '</td>';
                                                            echo '<td style="width:17%;"><span style="font-weight:600;">' . $caller_name[$k] . '</span><br/><span style="font-size:10px;">'.$caller_email[$k].'</span></a></td>';
                                                            echo '<td width="17%">' . $caller_location[$k] . '</td>';
                                                            echo '<td>' . $caller_mobile[$k] . '</td>';
                                                            echo '<td>' . get_corp_name($caller_corp_id[$k]) . '</td>';
                                                            echo '<td>' . date('d-M-Y h:i A', strtotime($caller_timestamp[$k])) . '</td>';
                                                            echo '<td style="width:13%;">';
                                                            //only if status is 'w', i.e if status is waiting, then only display action buttons apporve and reject
                                                            if ($caller_status[$k] == "W") {
                                                                echo '<button id="btn_' . $k . '" class="btn btn-primary btn-xs waves-effect waves-light" data-toggle="modal" data-target="#approve_' . $k . '">Approve</button>';
                                                                ?>
                                                                <!--- This modal open uniquely to spprove--->
                                                            <div id='approve_<?php echo $k; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload();">x</button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                        </div>
                                                                        <div>

                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div id="err_approve_<?php echo $k; ?>"></div>
                                                                            <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' id="form_approve_<?php echo $k; ?>" method='POST'>
                                                                                Are you sure you want to Approve?
                                                                                <div style="text-align:center;margin-top:10%;">
                                                                                    <input type="submit"  name="confirm-approve-btn_<?php echo $k; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                    <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                </div>
                                                                            </form>
                                                                            <?php
                                                                            #on click of 'yes' confirm-approve-btn  
                                                                            if (isset($_REQUEST["confirm-approve-btn_$k"])) {
                                                                                //update as "aprroved"
                                                                                $stmt10 = $dbh->prepare("UPDATE sco_registration_queue SET status = ? WHERE sr_no = ? LIMIT 1");
                                                                                $stmt10->execute(array('P', $sr_no[$k]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCO-QUEUE-1001].");

                                                                                # to display sucess meaagage in the modal
                                                                                echo '<script src="../assets/js/jquery.min.js"></script>
                                                                                        <script type="text/javascript"> 
                                                                                           $(document).ready(function(){
                                                                                              $("#err_approve_' . $k . '").addClass("alert");
                                                                                              $("#err_approve_' . $k . '").addClass("alert-success");
                                                                                              $("#err_approve_' . $k . '").html("Caller has been approved.");
                                                                                              $("#form_approve_' . $k . '").hide();
                                                                                              $("#approve_' . $k . '").modal({"backdrop": "static"});
                                                                                          });
                                                                                       </script>';
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            echo '<button id="btn_' . $k . '" class="btn btn-primary btn-xs waves-effect waves-light" data-toggle="modal" data-target="#reject_' . $k . '">Reject</button>';
                                                            ?>
                                                            <!--- This modal open uniquely to reject--->
                                                            <div id='reject_<?php echo $k; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload();">x</button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                        </div>
                                                                        <div>

                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div id="err_reject_<?php echo $k; ?>"></div>
                                                                            <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' id="form_reject_<?php echo $k; ?>" method='POST'>
                                                                                Are you sure you want to Reject?
                                                                                <div style="text-align:center;margin-top:10%;">
                                                                                    <input type="submit"  name="confirm-reject-btn_<?php echo $k; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                    <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No"/>
                                                                                </div>
                                                                            </form>
                                                                            <?php
                                                                            #on click of 'yes' confirm-reject-btn  
                                                                            if (isset($_REQUEST["confirm-reject-btn_$k"])) {
                                                                                //update as "rejected"
                                                                                $stmt20 = $dbh->prepare("UPDATE sco_registration_queue SET status = ? WHERE sr_no = ? LIMIT 1");
                                                                                $stmt20->execute(array('J', $sr_no[$k]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-SCO-QUEUE-1001].");

                                                                                # to display sucess meaagage in the modal
                                                                                echo '<script src="../assets/js/jquery.min.js"></script>
                                                                                        <script type="text/javascript"> 
                                                                                           $(document).ready(function(){
                                                                                              $("#err_reject_' . $k . '").addClass("alert");
                                                                                              $("#err_reject_' . $k . '").addClass("alert-success");
                                                                                              $("#err_reject_' . $k . '").html("Caller has been rejected.");
                                                                                               $("#form_reject_' . $k . '").hide();
                                                                                              $("#reject_' . $k . '").modal({"backdrop": "static"});
                                                                                          });
                                                                                       </script>';
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            //else if status is approved or rejected, display meassage accordingly
                                                            echo get_status($caller_status[$k]);
                                                        }
                                                        echo '</td>';
                                                        echo '</tr>';
                                                    } //ending for loop
                                                } else {
                                                    echo "No users in registration queue.";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/fastclick.js"></script>

        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
        </script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/ewap/sco_queue.php";
            }
        </script>
    </body>
</html>



