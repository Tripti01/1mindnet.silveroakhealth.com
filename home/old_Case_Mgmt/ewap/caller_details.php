<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'ewap-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_name.php';

//to check if cid are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['cid'])) {
    $cid = $_REQUEST['cid'];
} else {
    die("Unable to fetch user details. Error Code : CIDNIU.");
}
//to check if status are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = "";
}

#getting tid from session
$tid = $_SESSION['tid'];

//to fetch current year
$current_year = date("Y");


# Start the database realated stuff
$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use sohdbl");


$stmt10 = $dbh->prepare("SELECT corp_id, name FROM corp_login WHERE 1 ORDER BY name");
$stmt10->execute(array());
if ($stmt10->rowCount() != 0) {
    $k = 0;
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $corp_ids[$k] = $row10['corp_id'];
        $corp_names[$k] = $row10['name'];
        $k++;
    }
}

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");


$w = 0; //counter to fetch all notes
$stmt20 = $dbh->prepare("SELECT * FROM list_reason WHERE 1");
$stmt20->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1002].");
if ($stmt20->rowCount() != 0) {
    while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC)) {
        $reason_code_list[$w] = $row20['reason_code']; //to fetch reason
        $reason_text_list[$w] = $row20['reason_text'];
        $w++;
    }
}

$w = 0; //counter to fetch all notes
$stmt20 = $dbh->prepare("SELECT * FROM list_calltype WHERE 1");
$stmt20->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1002].");
if ($stmt20->rowCount() != 0) {
    while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC)) {
        $calltype_code_list[$w] = $row20['calltype_code']; //to fetch reason
        $calltype_text_list[$w] = $row20['calltype_text'];
        $w++;
    }
}

#to fetch all the details for the cid from call_callers_profile
$stmt00 = $dbh->prepare("SELECT * FROM call_callers_profile WHERE cid=? LIMIT 1 ");
$stmt00->execute(array($cid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1000].");
if ($stmt00->rowCount() != 0) {
    $row00 = $stmt00->fetch();
    $encrypt_name = $row00['name']; //to fetch encrypted name
    $name = decrypt($encrypt_name, $encryption_key); //to fetch name
    $email_id = $row00['email']; //to fetch email
    $corp_id = $row00['corp_id']; //to fetch corp_id
    $mobile = $row00['mobile']; //to fetch mobile number
    $yob = $row00['yob']; //to fetch yob
    $age = $current_year - $yob; //to calculate age
    $gender_letter = $row00['gender']; //to fetch the gender value 
    if ($row00['gender'] == "M") {//if gender is M, then proceed
        $gender = "Male"; //set as masle
    } else if ($row00['gender'] == "F") {//if gender is F, then proceed
        $gender = "Female"; //set as female
    } else if ($row00['gender'] == "O") {//if gender is O, then proceed
        $gender = "Other"; //set as other
    } else if ($row00['gender'] == "W") {//if gender is W, then proceed
        $gender = "Would rather not say"; //set as would rather not say
    }
    $location = $row00['location']; //to fetch location
    $client_emp_id = $row00['emp_id']; //to fetch emp_id
    $active = $row00['active']; //to fetch active status
} else {
#Show an error
    die("Some Error Occured. Please try again. If the issue persists. Send us an email at help@stresscontrolonline.com. ErrorCode : [CALLER DETAILS-1001]");
}

//to fetch all the notes written for the cid according to taken_at datetime
$k = 0; //counter to fetch all notes
$stmt20 = $dbh->prepare("SELECT * FROM call_callers_notes WHERE cid=? ORDER BY taken_at ASC");
$stmt20->execute(array($cid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1002].");
if ($stmt20->rowCount() != 0) {
    while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC)) {
        $sr_notes[$k] = $row20['sr_no']; //to fetch all serial nummbers of the notes
        $notes[$k] = decrypt($row20['notes_text'], $encryption_key); //to fetch all notes
        $taken_by[$k] = get_coach_name($row20['taken_by']); //to get the coach name
        $taken_at[$k] = $row20['taken_at']; //to fetch taken_at datetime
        $duration[$k] = $row20['duration']; //to fetch duration of call
        $subreason_code[$k] = $row20['reason']; //to fetch reason

        $stmt10 = $dbh->prepare("SELECT s.subreason_text, r.reason_code, r.reason_text FROM list_subreason s, list_reason r WHERE s.reason_code = r.reason_code and s.subreason_code=?");
        $stmt10->execute(array($subreason_code[$k]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1002].");
        if ($stmt10->rowCount() != 0) {
            $row10 = $stmt10->fetch();
            $reason_code[$k] = $row10['reason_code'];
            $subreason_text[$k] = $row10['subreason_text'];
            $reason_text[$k] = $row10['reason_text'];
        } else {
            $reason_code[$k] = "";
            $subreason_text[$k] = "";
            $reason_text[$k] = "";
        }


        $emp_type[$k] = $row20['emp_type'];
        $source[$k] = $row20['source']; //to fetch source of call
        if ($source[$k] == "sc") {
            $call_no[$k] = $row20['call_no']; //to fetch call number
        }
        $k++;
    }
}

$total_notes = $k; //to get total number of notes
//
//
//to fetch tid assigned and check if the therapist(tid) has permission to view notes or not
$stmt30 = $dbh->prepare("SELECT * FROM call_cid_tid_list WHERE cid = ? AND active = ? LIMIT 1");
$stmt30->execute(array($cid, '1'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1009].");
if ($stmt30->rowCount() != 0) {//if tid is present, then proceed
    $row30 = $stmt30->fetch();
    $tid_assigned = $row30['tid']; //to fetch tid
    if ($tid == $tid_assigned) {
        $allow_view_notes = 1; //set allow_view_notes as 1
    } else {
        $allow_view_notes = 0; //else set allow_view_notes as 0
    }
} else {
    $allow_view_notes = 0; //else set allow_view_notes as 0
}




//to fetch list of all coaches from sco
//database connection to sco
$dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh_sco->query("use sohdbl");

$j = 0; //counter for all coaches
$stmt10 = $dbh_sco->prepare("SELECT name AS thrp_name, thrp_login.tid FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = ?");
$stmt10->execute(array("THRP"))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1003].");
if ($stmt10->rowCount() != 0) {
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $thrp_tid_list[$j] = $row10['tid']; //to fetch tids
        $thrp_name_list[$j] = $row10['thrp_name']; //to fetch thrp_name_list
        $j++;
    }
}


#on click edit profile
if (isset($_REQUEST['edit-profile-btn'])) {
    if (isset($_REQUEST['client_name']) && !empty($_REQUEST['client_name']) && $_REQUEST['client_name'] !== '' && isset($_REQUEST['client_email']) && !empty($_REQUEST['client_email']) && $_REQUEST['client_email'] !== '' && isset($_REQUEST['client_mobile']) && !empty($_REQUEST['client_mobile']) && $_REQUEST['client_mobile'] !== '' && isset($_REQUEST['client_age']) && !empty($_REQUEST['client_age']) && $_REQUEST['client_age'] !== '' && isset($_REQUEST['client_gender']) && !empty($_REQUEST['client_gender']) && $_REQUEST['client_gender'] !== '' && isset($_REQUEST['client_location']) && !empty($_REQUEST['client_location']) && $_REQUEST['client_location'] !== '') {

        $cid = $_REQUEST['cid']; //to get cid
        $new_name = encrypt($_REQUEST['client_name'], $encryption_key); //to get and encrypt name
        $new_email = $_REQUEST['client_email']; //to get email
        $new_mobile = $_REQUEST['client_mobile']; //to get mobile
        $new_gender = $_REQUEST['client_gender']; //to get gender
        $new_yob = $_REQUEST['client_age']; //to get yob
        $new_location = $_REQUEST['client_location']; //to get location
        $new_active = $_REQUEST['client_active']; //to get status
        $corp_id = $_REQUEST['corp_id'];
        $client_emp_id = $_REQUEST['client_emp_id'];
        //to update into call_callers_profile
        $stmt30 = $dbh->prepare("UPDATE call_callers_profile SET name = ?, email = ?, mobile = ?, gender = ?, yob = ?, location = ?, corp_id = ?, emp_id = ?, active = ? WHERE cid= ? LIMIT 1");
        $stmt30->execute(array($new_name, $new_email, $new_mobile, $new_gender, $new_yob, $new_location, $corp_id, $client_emp_id, $new_active, $cid)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1005].");

        //reload page
        header('Location:' . $host . '/ewap/caller_details.php?cid=' . $cid . '&status=4');
    } else {
        //  something is wrong the client side validation
        //  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_CALLER_DETAILS_EDIT_PROFILE_JS_MSNG");
    }
}
#on click of 'yes' confirm-call-btn
if (isset($_REQUEST["confirm-assign-btn"])) {

    //include mailer function
    //include '../mail/mail_change_of_coach_call_mgmt.php';

    $cid = $_REQUEST['cid']; //to get cid
    $tid_assigned_new = $_REQUEST['tid_assigned_new']; //to get new assigned tid
    $name_tid_assigned_new = get_coach_name($tid_assigned_new); //to get new tid assigned name
    $name_tid_assigned_old = get_coach_name($_REQUEST['tid_assigned_old']); //to get old tid assigned name
    #current curret time
    date_default_timezone_set("Asia/Kolkata");
    $timestamp = date('Y-m-d h:i:s');
    $comment = "Re-Assigned by " . $tid; //set comments as reassigned by the therapist
    //for all the previous clients assigned to this particular cid, set active as 0
    $stmt10 = $dbh->prepare("UPDATE call_cid_tid_list SET active = ? WHERE cid = ?");
    $stmt10->execute(array('0', $cid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1011].");

    //insert a new value for the cid and tid and set active as 1
    $stmt20 = $dbh->prepare("INSERT INTO call_cid_tid_list VALUES (?,?,?,?,?,?,?,?)");
    $stmt20->execute(array($cid, $tid_assigned_new, $timestamp, $comment, '1', '', '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1012].");

    //mailer function to send all mails to previous coaches
    // Disable as per Support ticket 184 and email discussions. 
    //  mail_change_of_coach_call_mgmt($cid, $email, $name, $name_tid_assigned_new, $name_tid_assigned_old);

    echo '<script>window.location.href="caller_details.php?cid=' . $cid . '&status=3";</script>'; //reload page
}
#on click of notes-submit-button
if (isset($_REQUEST['notes-submit-btn'])) {
    if (isset($_REQUEST['notes']) && !empty($_REQUEST['notes']) && $_REQUEST['notes'] !== '' && isset($_REQUEST['date']) && !empty($_REQUEST['date']) && $_REQUEST['date'] !== '' && isset($_REQUEST['time']) && !empty($_REQUEST['time']) && $_REQUEST['time'] !== '' && isset($_REQUEST['duration']) && !empty($_REQUEST['duration']) && $_REQUEST['duration'] !== '' && isset($_REQUEST['source']) && !empty($_REQUEST['source']) && $_REQUEST['source'] !== '' && isset($_REQUEST['emp_type'])) {

        $cid = $_REQUEST['cid']; //to get cid
        $notes = encrypt($_REQUEST['notes'], $encryption_key); //to get notes
        $date = str_replace('/', '-', $_REQUEST['date']);
        $date = date("Y-m-d ", strtotime($date)); //date from date picker
        $time = date("H:i:s ", strtotime($_REQUEST['time'])); //time from time picker
        $notes_timestamp = $date . $time; //combining both date and time
        $duration = $_REQUEST['duration']; //to get duration
        if (isset($_REQUEST['reason']) && $_REQUEST['reason'] != '') {
            $reason = $_REQUEST['reason'];
            $subreason = $_REQUEST['subreason']; //to get reason
        } else {
            $reason = "";
            $subreason = "";
        }
        $source = $_REQUEST['source']; //to get source
        $tid = $_REQUEST['tid']; //to get tid
        $emp_type = $_REQUEST['emp_type'];

        if ($source == 'sc') {
            $sr_no_call_select = $_REQUEST['call_number']; //to get the call number
            //to update call_Schedule_Call, mark the call as done
            $stmt10 = $dbh->prepare("UPDATE call_schedule_call SET call_done = ? WHERE sr_no = ? LIMIT 1");
            $stmt10->execute(array('1', $sr_no_call_select)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1006].");
            //to insert into call_callers_notes
            $stmt20 = $dbh->prepare("INSERT INTO call_callers_notes VALUES(?,?,?,?,?,?,?,?,?,?)");
            $stmt20->execute(array('', $cid, $notes, $tid, $notes_timestamp, $duration, $subreason, $source, $sr_no_call_select, '')) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1007].");
        } else {
            //to insert into call_callers_notes
            $stmt10 = $dbh->prepare("INSERT INTO call_callers_notes VALUES(?,?,?,?,?,?,?,?,?,?)");
            $stmt10->execute(array('', $cid, $notes, $tid, $notes_timestamp, $duration, $subreason, $source, '', $emp)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1008].");
        }

        //reload page
        header('Location:caller_details.php?cid=' . $cid . '&status=2');
    } else {
        //  something is wrong the client side validation
        //  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_CALLER_DETAILS_NOTES_JS_MSNG");
    }
}

//type of call function
function type_of_source($type) {
    include 'ewap-config.php';

    $dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
    $dbh->query("use scodd");

    $stmt20 = $dbh->prepare("SELECT calltype_text FROM list_calltype WHERE calltype_code=?");
    $stmt20->execute(array($type))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1002].");
    if ($stmt20->rowCount() != 0) {
        $row20 = $stmt20->fetch();
        $type_name = $row20['calltype_text'];
    } else {
        $type_name = "";
    }

    return $type_name;
}

//type of active status function
function type_of_active($active) {
    if ($active == '1') {//if active == 1, then set type_name =active session
        $type_name = "Active Session";
    } else if ($active == '0') {//if active == 0, then set type_name =closed session
        $type_name = "Closed Session";
    } else {//if active == f2f-onsite, then set type_name as blank
        $type_name = " ";
    }
    return $type_name;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>View Caller</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="../../assets/lib/summernote/summernote.css"/>

        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <style>

            th {
                color: #666666;
                font-weight: 600;
            }
            .pac-container { z-index: 100000 !important; }
        </style>
    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include '../top_navbar.php'; ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="card-box">
                                <div class="row">
                                    <?php if (isset($cid)) { ?>
                                        <div class="col-md-12 col-sm-12 col-lg-12" style="margin-top:0%;">
                                            <div class="row" style="border-bottom: 1px solid #e5e5e5;margin-bottom: 2%;">
                                                <div class="col-sm-5">
                                                    <h4>About Caller</h4>
                                                </div>
                                                <div class="col-sm-7" style="align:right;">
                                                    <?php if ($allow_view_notes == 1) { ?><!--if the coach is allwed to view notes, enable add notes btn -->
                                                        <a><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#add_call" style="margin-left:27.5%;" onClick = notes() > Add Notes</button></a>
                                                    <?php } else { ?><!--else diable add notes btn -->
                                                        <a><button class="btn" data-toggle="modal" data-target="#add_call" style="margin-left:32.5%; background-color: #98a6ad;color: white;" disabled> Add Notes</button></a>
                                                    <?php } ?>
                                                        <a href="https://silveroakhealth.as.me/schedule.php" target="_blank"><button type="button" class="btn btn-primary btn-bordred waves-effect w-xs waves-light">Schedule New Call</button></a>
                                                    <a><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#assign_client"  >Re-Assign</button></a>
                                                    <?php if ($allow_view_notes == 1) { ?><!--if the coach is allwed to view notes, enable edit profile btn -->
                                                        <a><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#edit_profile" > Edit Profile</button></a>
                                                    <?php } else { ?><!--else diable edit profile btn -->
                                                        <a><button class="btn" data-toggle="modal" data-target="#edit_profile" style="background-color: #98a6ad;color: white;" disabled> Edit Profile</button></a>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <!-- add call modal, to add notes and other details regarding call-->
                                            <div id='add_call' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg" style="width: 1140px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                                            <h4 class="modal-title">Add Notes for <?php echo $name; ?> <BR/> 
                                                                <span style="font-size:15px;">(Fill under each heading, do not erase the heading)</span>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id="err_name" class="msg_disp"></div>
                                                            <form class="notes-form" id="notes-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" >
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-6">
                                                                                <textarea class="form-control" id="editor1" rows="1" id="notes" name="notes" wrap="virtual" style="min-height:6px;height:530px;">
<b>General Info/notes</b><br/>
    Context setting<br/><br/>

<b>Target of the session</b><br/>
    Goal of session <br/><br/>

<b>Subjective</b><br/>
     Presenting Complaints and other information shared by the client<br/><br/>

<b>Objective</b><br/>
      Factual information and other observations noted by the therapist<br/><br/>

<b>Assessment</b><br/>
      Analysis of Subjective & Objective, MSE, impression/ session description/Counsellor notes<br/><br/>

<b>Plan</b><br/>
     Current therapeutic plan, overall plan and expected outcome<br/><br/>

                                                                                </textarea>
                                                                            </div>
                                                                            <div class="col-sm-6" style="padding-left: 5px;padding-right: 0px;">
                                                                                <div class="row" style="margin-top:0%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> Date  </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <div class="input-group">
                                                                                                    <input type="text" class="form-control" id="datepicker1" placeholder="Select date here" name="date" autocomplete="off">
                                                                                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="margin-top:1.2%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> Time  </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <div class="input-group">
                                                                                                    <div class="bootstrap-timepicker">
                                                                                                        <input id="timepicker2" type="text" class="form-control" name="time" />
                                                                                                    </div>
                                                                                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="margin-top:1.2%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> Duration </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="form-control" name="duration">																								
                                                                                                    <option value="5">5 minutes</option>																									
                                                                                                    <option value="10">10 minutes</option>
                                                                                                    <option value="20">20 minutes</option>
                                                                                                    <option value="30">30 minutes</option>
                                                                                                    <option value="40">40 minutes</option>
                                                                                                    <option value="50">50 minutes</option>
                                                                                                    <option value="60">60 minutes</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="margin-top:1.2%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> Reason  </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="form-control reason-list" name="reason">
                                                                                                    <option value="0" selected disabled>Select Reason</option> 
                                                                                                    <?php
                                                                                                    for ($w = 0; $w < count($reason_code_list); $w++) {
                                                                                                        echo '<option value="' . $reason_code_list[$w] . '">' . $reason_text_list[$w] . '</option>';
                                                                                                    }
                                                                                                    ?>                                                                                                    
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="margin-top:1.2%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> SubReason  </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="form-control subreason-list" name="subreason" disabled>
                                                                                                    <option value="0" selected disabled>Select Reason</option> 
                                                                                                    <?php
                                                                                                    for ($w = 0; $w < count($reason_code_list); $w++) {
                                                                                                        echo '<option value="' . $reason_code_list[$w] . '">' . $reason_text_list[$w] . '</option>';
                                                                                                    }
                                                                                                    ?> 
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="margin-top:1.2%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> Type </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="form-control" name="source" onchange="call_number_func(this);">
                                                                                                    <?php
                                                                                                    for ($w = 0; $w < count($calltype_code_list); $w++) {
                                                                                                        echo '<option value="' . $calltype_code_list[$w] . '">' . $calltype_text_list[$w] . '</option>';
                                                                                                    }
                                                                                                    ?> 
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                              
                                                                                <div class="row" style="margin-top:1.2%;">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label">Employee Type </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="form-control" name="emp_type">
                                                                                                    <option value="E">Employee</option>
                                                                                                    <option value="D">Dependent</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="margin-top:1.2%;display:none;" id="call_number">
                                                                                    <div class="form-group">
                                                                                        <div class=" col-sm-12">
                                                                                            <div class=" col-sm-1"></div>
                                                                                            <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                <label class="control-label"> Scheduled Date Time </label>
                                                                                            </div>
                                                                                            <div class="col-sm-8">
                                                                                                <select class="form-control" name="call_number" id="call_num">
                                                                                                    <?php
                                                                                                    if (isset($sr_no_call)) {//if sr_no_call is set, i.e if there are scheduled calls for the cid, then proceed
                                                                                                        for ($l = 0; $l < count($sr_no_call); $l++) {

                                                                                                            echo '<option value="' . $sr_no_call[$l] . '">' . date('d-M-Y', strtotime($prefered_date_call[$l])) . ' ' . $prefered_time_call[$l] . '</option>';
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer" style="margin-top:2%;">
                                                                    <input type="hidden" name="tid" value="<?php echo $tid; ?>">
                                                                    <input type="hidden" name="cid" value="<?php echo $cid; ?>">
                                                                    <button type="button" class="btn btn-danger waves-effect" style="margin-right:2%;"  id="notes-no-btn" name="notes-no-btn"  >Discard & Close</button>
                                                                    <input type="submit" id="notes-submit-btn" name="notes-submit-btn" value="Save" class="btn btn-primary" style="margin-right:1.7%;"/>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- edit profile -->
                                            <div id='edit_profile' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                                            <h4 class="modal-title"> Edit Client Profile</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id="err_profile" class="msg_disp"></div>
                                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="form-horizontal-profile">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="row">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Client Name</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" value="<?php echo $name; ?>" name="client_name"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Status</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <select id="client_active" name="client_active" title="" class="form-control">
                                                                                            <option value="1" <?php
                                                                                            if ($active == "1") {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?> > Active Session </option>
                                                                                            <option value="0" <?php
                                                                                            if ($active == "0") {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>> Close Session </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Employee ID</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" placeholder="Caller Employee ID" value="<?php echo $client_emp_id; ?>" name="client_emp_id"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Company</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <select id="caller_company" name="corp_id" title="" class="form-control">
                                                                                            <?php for ($i = 0; $i < count($corp_ids); $i++) { ?>
                                                                                                <option value="<?php echo $corp_ids[$i]; ?>" <?php
                                                                                                if ($corp_id == $corp_ids[$i]) {
                                                                                                    echo "selected";
                                                                                                } else {
                                                                                                    echo "";
                                                                                                }
                                                                                                ?>><?php echo $corp_names[$i]; ?></option>
                                                                                                    <?php } ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Email</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" value="<?php echo $email_id; ?>" name="client_email"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Mobile</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <input type="text" class="form-control" value="<?php echo $mobile; ?>" name="client_mobile"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Gender</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <select id="gender" name="client_gender" title="" class="form-control">
                                                                                            <option value="" disabled=""></option>
                                                                                            <option value="M" <?php
                                                                                            if ($gender_letter == "M") {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?> > Male </option>
                                                                                            <option value="F" <?php
                                                                                            if ($gender_letter == "F") {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>> Female </option>
                                                                                            <option value="O" <?php
                                                                                            if ($gender_letter == "O") {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>>Other</option>
                                                                                            <option value="W" <?php
                                                                                            if ($gender_letter == "W") {
                                                                                                echo "selected";
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>>Would rather not say</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Year Of Birth</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <select id="yob" name="client_age" title="" class="form-control">
                                                                                            <option value="" selected="">  -- SELECT -- </option> 
                                                                                            <option value="2010" <?php echo ($yob == 2010) ? "selected" : "" ?>>2010</option>
                                                                                            <option value="2009"<?php echo ($yob == 2009) ? "selected" : "" ?>>2009</option>
                                                                                            <option value="2008"<?php echo ($yob == 2008) ? "selected" : "" ?>>2008</option>
                                                                                            <option value="2007" <?php echo ($yob == 2007) ? "selected" : "" ?>>2007</option>
                                                                                            <option value="2006" <?php echo ($yob == 2006) ? "selected" : "" ?>>2006</option>
                                                                                            <option value="2005" <?php echo ($yob == 2005) ? "selected" : "" ?>>2005</option>
                                                                                            <option value="2004"<?php echo ($yob == 2004) ? "selected" : "" ?>>2004</option>
                                                                                            <option value="2003"<?php echo ($yob == 2003) ? "selected" : "" ?>>2003</option>
                                                                                            <option value="2002"<?php echo ($yob == 2002) ? "selected" : "" ?>>2002</option>
                                                                                            <option value="2001"<?php echo ($yob == 2001) ? "selected" : "" ?>>2001</option>
                                                                                            <option value="2000"<?php echo ($yob == 2000) ? "selected" : "" ?>>2000</option>
                                                                                            <option value="1999"<?php echo ($yob == 1999) ? "selected" : "" ?>>1999</option>
                                                                                            <option value="1998" <?php echo ($yob == 1998) ? "selected" : "" ?>>1998</option>
                                                                                            <option value="1997"<?php echo ($yob == 1997) ? "selected" : "" ?>>1997</option>
                                                                                            <option value="1996" <?php echo ($yob == 1996) ? "selected" : "" ?>>1996</option>
                                                                                            <option value="1995" <?php echo ($yob == 1995) ? "selected" : "" ?>>1995</option>
                                                                                            <option value="1994" <?php echo ($yob == 1994) ? "selected" : "" ?>>1994</option>
                                                                                            <option value="1993" <?php echo ($yob == 1993) ? "selected" : "" ?>>1993</option>
                                                                                            <option value="1992" <?php echo ($yob == 1992) ? "selected" : "" ?>>1992</option>
                                                                                            <option value="1991" <?php echo ($yob == 1991) ? "selected" : "" ?>>1991</option>
                                                                                            <option value="1990" <?php echo ($yob == 1990) ? "selected" : "" ?>>1990</option>
                                                                                            <option value="1989" <?php echo ($yob == 1989) ? "selected" : "" ?>>1989</option>
                                                                                            <option value="1988" <?php echo ($yob == 1988) ? "selected" : "" ?>>1988</option>
                                                                                            <option value="1987" <?php echo ($yob == 1987) ? "selected" : "" ?>>1987</option>
                                                                                            <option value="1986" <?php echo ($yob == 1986) ? "selected" : "" ?>>1986</option>
                                                                                            <option value="1985" <?php echo ($yob == 1985) ? "selected" : "" ?>>1985</option>
                                                                                            <option value="1984" <?php echo ($yob == 1984) ? "selected" : "" ?>>1984</option>
                                                                                            <option value="1983" <?php echo ($yob == 1983) ? "selected" : "" ?>>1983</option>
                                                                                            <option value="1982" <?php echo ($yob == 1982) ? "selected" : "" ?>>1982</option>
                                                                                            <option value="1981" <?php echo ($yob == 1981) ? "selected" : "" ?>>1981</option>
                                                                                            <option value="1980" <?php echo ($yob == 1980) ? "selected" : "" ?>>1980</option>
                                                                                            <option value="1979" <?php echo ($yob == 1979) ? "selected" : "" ?>>1979</option>
                                                                                            <option value="1978" <?php echo ($yob == 1978) ? "selected" : "" ?>>1978</option>
                                                                                            <option value="1977" <?php echo ($yob == 1977) ? "selected" : "" ?>>1977</option>
                                                                                            <option value="1976" <?php echo ($yob == 1976) ? "selected" : "" ?>>1976</option>
                                                                                            <option value="1975" <?php echo ($yob == 1975) ? "selected" : "" ?>>1975</option>                                       
                                                                                            <option value="1974" <?php echo ($yob == 1974) ? "selected" : "" ?>>1974</option>
                                                                                            <option value="1973" <?php echo ($yob == 1973) ? "selected" : "" ?>>1973</option>
                                                                                            <option value="1972" <?php echo ($yob == 1972) ? "selected" : "" ?>>1972</option>
                                                                                            <option value="1971" <?php echo ($yob == 1971) ? "selected" : "" ?>>1971</option>
                                                                                            <option value="1970" <?php echo ($yob == 1970) ? "selected" : "" ?>>1970</option>
                                                                                            <option value="1969" <?php echo ($yob == 1969) ? "selected" : "" ?>>1969</option>
                                                                                            <option value="1968" <?php echo ($yob == 1968) ? "selected" : "" ?>>1968</option>
                                                                                            <option value="1967" <?php echo ($yob == 1967) ? "selected" : "" ?>>1967</option>
                                                                                            <option value="1966" <?php echo ($yob == 1966) ? "selected" : "" ?>>1966</option>
                                                                                            <option value="1965" <?php echo ($yob == 1965) ? "selected" : "" ?>>1965</option>
                                                                                            <option value="1964" <?php echo ($yob == 1964) ? "selected" : "" ?>>1964</option>
                                                                                            <option value="1963" <?php echo ($yob == 1963) ? "selected" : "" ?>>1963</option>
                                                                                            <option value="1962" <?php echo ($yob == 1962) ? "selected" : "" ?>>1962</option>
                                                                                            <option value="1961" <?php echo ($yob == 1961) ? "selected" : "" ?>>1961</option>
                                                                                            <option value="1960" <?php echo ($yob == 1960) ? "selected" : "" ?>>1960</option>
                                                                                            <option value="1959" <?php echo ($yob == 1959) ? "selected" : "" ?>>1959</option>
                                                                                            <option value="1958" <?php echo ($yob == 1958) ? "selected" : "" ?>>1958</option>
                                                                                            <option value="1957" <?php echo ($yob == 1957) ? "selected" : "" ?>>1957</option>
                                                                                            <option value="1956" <?php echo ($yob == 1956) ? "selected" : "" ?>>1956</option>
                                                                                            <option value="1955" <?php echo ($yob == 1955) ? "selected" : "" ?>>1955</option>
                                                                                            <option value="1954" <?php echo ($yob == 1954) ? "selected" : "" ?>>1954</option>
                                                                                            <option value="1953" <?php echo ($yob == 1953) ? "selected" : "" ?>>1953</option>
                                                                                            <option value="1952" <?php echo ($yob == 1952) ? "selected" : "" ?>>1952</option>
                                                                                            <option value="1951" <?php echo ($yob == 1951) ? "selected" : "" ?>>1951</option>
                                                                                            <option value="1950" <?php echo ($yob == 1950) ? "selected" : "" ?>>1950</option>
                                                                                            <option value="1949" <?php echo ($yob == 1949) ? "selected" : "" ?>>1949</option>
                                                                                            <option value="1948" <?php echo ($yob == 1948) ? "selected" : "" ?>>1948</option>
                                                                                            <option value="1947" <?php echo ($yob == 1947) ? "selected" : "" ?>>1947</option>
                                                                                            <option value="1946" <?php echo ($yob == 1946) ? "selected" : "" ?>>1946</option>
                                                                                            <option value="1945" <?php echo ($yob == 1945) ? "selected" : "" ?>>1945</option>
                                                                                            <option value="1944" <?php echo ($yob == 1944) ? "selected" : "" ?>>1944</option>
                                                                                            <option value="1943" <?php echo ($yob == 1943) ? "selected" : "" ?>>1943</option>
                                                                                            <option value="1942" <?php echo ($yob == 1942) ? "selected" : "" ?>>1942</option>
                                                                                            <option value="1941" <?php echo ($yob == 1941) ? "selected" : "" ?>>1941</option>
                                                                                            <option value="1940" <?php echo ($yob == 1940) ? "selected" : "" ?>>1940</option>
                                                                                            <option value="1939" <?php echo ($yob == 1939) ? "selected" : "" ?>>1939</option>
                                                                                            <option value="1938" <?php echo ($yob == 1938) ? "selected" : "" ?>>1938</option>
                                                                                            <option value="1937" <?php echo ($yob == 1937) ? "selected" : "" ?>>1937</option>
                                                                                            <option value="1936" <?php echo ($yob == 1936) ? "selected" : "" ?>>1936</option>
                                                                                            <option value="1935" <?php echo ($yob == 1935) ? "selected" : "" ?>>1935</option>
                                                                                            <option value="1934" <?php echo ($yob == 1934) ? "selected" : "" ?>>1934</option>
                                                                                            <option value="1933" <?php echo ($yob == 1933) ? "selected" : "" ?>>1933</option>
                                                                                            <option value="1932" <?php echo ($yob == 1932) ? "selected" : "" ?>>1932</option>
                                                                                            <option value="1931" <?php echo ($yob == 1931) ? "selected" : "" ?>>1931</option>
                                                                                            <option value="1930" <?php echo ($yob == 1930) ? "selected" : "" ?>>1930</option>
                                                                                            <option value="1929" <?php echo ($yob == 1929) ? "selected" : "" ?>>1929</option>
                                                                                            <option value="1928" <?php echo ($yob == 1928) ? "selected" : "" ?>>1928</option>
                                                                                            <option value="1927" <?php echo ($yob == 1927) ? "selected" : "" ?>>1927</option>
                                                                                            <option value="1926" <?php echo ($yob == 1926) ? "selected" : "" ?>>1926</option>
                                                                                            <option value="1925" <?php echo ($yob == 1925) ? "selected" : "" ?>>1925</option>
                                                                                            <option value="1924" <?php echo ($yob == 1924) ? "selected" : "" ?>>1924</option>
                                                                                            <option value="1923" <?php echo ($yob == 1923) ? "selected" : "" ?>>1923</option>
                                                                                            <option value="1922" <?php echo ($yob == 1922) ? "selected" : "" ?>>1922</option>
                                                                                            <option value="1921" <?php echo ($yob == 1921) ? "selected" : "" ?>>1921</option>
                                                                                            <option value="1920" <?php echo ($yob == 1920) ? "selected" : "" ?>>1920</option>
                                                                                            <option value="1919" <?php echo ($yob == 1919) ? "selected" : "" ?>>1919</option>
                                                                                            <option value="1918" <?php echo ($yob == 1918) ? "selected" : "" ?>>1918</option>
                                                                                            <option value="1917" <?php echo ($yob == 1917) ? "selected" : "" ?>>1917</option>
                                                                                            <option value="1916" <?php echo ($yob == 1916) ? "selected" : "" ?>>1916</option>
                                                                                            <option value="1915" <?php echo ($yob == 1915) ? "selected" : "" ?>>1915</option>
                                                                                            <option value="1914" <?php echo ($yob == 1914) ? "selected" : "" ?>>1914</option>
                                                                                            <option value="1913" <?php echo ($yob == 1913) ? "selected" : "" ?>>1913</option>
                                                                                            <option value="1912" <?php echo ($yob == 1912) ? "selected" : "" ?>>1912</option>
                                                                                            <option value="1911" <?php echo ($yob == 1911) ? "selected" : "" ?>>1911</option>
                                                                                            <option value="1910" <?php echo ($yob == 1910) ? "selected" : "" ?>>1910</option>
                                                                                            <option value="1909" <?php echo ($yob == 1909) ? "selected" : "" ?>>1909</option>
                                                                                            <option value="1908" <?php echo ($yob == 1908) ? "selected" : "" ?>>1908</option>
                                                                                            <option value="1907" <?php echo ($yob == 1907) ? "selected" : "" ?>>1907</option>
                                                                                            <option value="1906" <?php echo ($yob == 1906) ? "selected" : "" ?>>1906</option>
                                                                                            <option value="1905" <?php echo ($yob == 1905) ? "selected" : "" ?>>1905</option>
                                                                                            <option value="1904" <?php echo ($yob == 1904) ? "selected" : "" ?>>1904</option>
                                                                                            <option value="1903" <?php echo ($yob == 1903) ? "selected" : "" ?>>1903</option>
                                                                                            <option value="1902" <?php echo ($yob == 1902) ? "selected" : "" ?>>1902</option>
                                                                                            <option value="1901" <?php echo ($yob == 1901) ? "selected" : "" ?>>1901</option>
                                                                                            <option value="1900" <?php echo ($yob == 1900) ? "selected" : "" ?>>1900</option>
                                                                                        </select>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" style="margin-top:0.5%;">
                                                                            <div class=" col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class=" col-sm-1"></div>
                                                                                    <div class=" col-sm-3">
                                                                                        <label class="control-label">Location</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <div class="geo-details">
                                                                                            <input type="text" placeholder="Location" name="client_location" value="<?php echo $location; ?>" class="form-control" id="location" autocomplete="off">
                                                                                            <input class="form-control placeholder-no-fix" data-geo="country" value="" id="country" name="country" type="hidden">
                                                                                            <input class="form-control placeholder-no-fix" data-geo="administrative_area_level_1" value="" id="state" name="state" type="hidden">
                                                                                            <input class="form-control placeholder-no-fix" data-geo="administrative_area_level_2" value="" id="city" name="city" type="hidden">
                                                                                            <input class="form-control placeholder-no-fix" data-geo="lat" value="" id="latitude" name="latitude" type="hidden">
                                                                                            <input class="form-control placeholder-no-fix" data-geo="lng" value="" id="longitude" name="longitude" type="hidden">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12" style="text-align: center;margin-top:4%;">
                                                                                <input type="hidden" name="cid" value="<?php echo $cid ?>">
                                                                                <input type="submit" value="Save" name="edit-profile-btn" class="btn btn-primary"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                         

                                            <!-- Modal to assign client to you -->
                                            <div id='assign_client' class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                                            <h4 class="modal-title" id="ModalLabel">Re-Assign Caller</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id="err_assign" class="msg_disp"></div>
                                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="assign_client_form" method="POST">
                                                                <div class="row" style="margin-top:-3%;">
                                                                    <div class=" col-sm-12" style="text-align: center;">
                                                                        <div class=" col-sm-12" style="text-align: center;">
                                                                            <label class="control-label"> Re-assign the caller to</label>
                                                                        </div>
                                                                        <div class="col-sm-12" style="margin-top:4%;">
                                                                            <select class="form-control" name="tid_assigned_new">
                                                                                <?php for ($j = 0; $j < count($thrp_tid_list); $j++) { ?>
                                                                                    <option value=<?php
                                                                                    echo $thrp_tid_list[$j];
                                                                                    if ($thrp_tid_list[$j] == $tid) {
                                                                                        echo ' selected=""';
                                                                                    } else {
                                                                                        
                                                                                    }
                                                                                    ?>> <?php echo $thrp_name_list[$j]; ?></option>
                                                                                        <?php }
                                                                                        ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="text-align:center;margin-top:9%;">
                                                                    <input type="hidden" name="cid" value="<?php echo $cid; ?>">
                                                                    <input type="hidden" name="tid_assigned_old" value="<?php echo $tid_assigned; ?>">
                                                                    <input type="submit"  name="confirm-assign-btn" id="confirm-assign-btn" class="btn-success btn-sm m-b-5" value="Assign"  >
                                                                    <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="Discard"/>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table style="width:100%;">
                                                        <thead>
                                                        <th style="width:10%;text-align: center;"></th>
                                                        <th style="width:19%;text-align: center;"></th>
                                                        <th style="width:10%;text-align:left;"></th>
                                                        <th style="text-align: center;"></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Client Name</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $name; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Employee ID</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $client_emp_id; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Age </label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $age . ' years '; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Mobile</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $mobile; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Status</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo type_of_active($active); ?></label></td>
                                                            </tr>        

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-5" style="margin-left:8%;">
                                                    <table style="width:100%;">
                                                        <thead>
                                                        <th style="width:1%;text-align: center;"></th>
                                                        <th style="width:30%;text-align: center;"></th>
                                                        <th style="width:15%;text-align:left;"></th>
                                                        <th style="text-align: center;"></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Email</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $email_id; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Company</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo get_corp_name($corp_id); ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Gender</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $gender; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Location</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $location; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label" >Coach Assigned</label></td>
                                                                <td>:</td>
                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo get_coach_name($tid_assigned); ?></label></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <!-- to print notes -->
                                            <?php
                                            if (isset($total_notes)) {
                                                for ($k = 0; $k < $total_notes; $k++) {
                                                    $serial_num = $k + 1;
                                                    ?>
                                                    <div class="row">
                                                        <div class="row" style="border-bottom: 1px solid #e5e5e5;margin-bottom: 3%;margin-top: 3%;">
                                                            <div class="col-md-8" style="margin-left:0.8%;">
                                                                <h4>Call Number <?php echo $serial_num; ?> - <?php echo $taken_by[$k]; ?>
                                                                </h4> 
                                                            </div>
                                                            <div class="col-md-3" style="margin-top:1%;margin-left:6.2%;padding-left:9.1%;">
                                                                <a style="cursor: pointer;text-decoration: underline;" data-toggle="modal" data-target="#view_notes<?php echo $k; ?>" onClick="notes()"> View Notes</a>
                                                                <a style="cursor: pointer;text-decoration: underline;margin-left:4%;" data-toggle="modal" data-target="#edit_notes<?php echo $k; ?>" onClick="notes()"> Edit Notes</a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">

                                                                    <table style="width:95%;margin-left:40px;">
                                                                        <thead>
                                                                        <th style="width:15%;text-align: center;"></th>
                                                                        <th style="width:35%;text-align: left;"></th>
                                                                        <th style="width:15%;text-align: center;"></th>
                                                                        <th style="width:35%;text-align: left;"></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td><label class="control-label" >Call Type</label></td>
                                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo type_of_source($source[$k]); ?></label></td>
                                                                                <td><label class="control-label" >Time</label></td>
                                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo date('d-m-Y', strtotime($taken_at[$k])); ?> <?php echo date('h:i A', strtotime($taken_at[$k])); ?></label></td>
                                                                            </tr>
                                                                            <tr>

                                                                                <td><label class="control-label" >Reason</label></td>
                                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $reason_text[$k]; ?> (<?php echo $subreason_text[$k]; ?>)</label></td>
                                                                                <td><label class="control-label" >Duration</label></td>
                                                                                <td><label class="control-label" style="font-weight: 500;"><?php echo $duration[$k]; ?> minutes</label></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal to view notes -->
                                                    <div id='view_notes<?php echo $k; ?>' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog" >
                                                            <div class="modal-content" style="width:600px;height:600px;">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload()">x</button>
                                                                    <h4 class="modal-title">Notes for Call Number <?php echo $serial_num; ?></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php if ($allow_view_notes == 1) { ?>
                                                                        <div class="col-sm-12">
                                                                            <textarea class="form-control" rows="1" id="view-notes<?php echo $k; ?>" name="q1" wrap="virtual" style="min-height:6px;height:190px;" disabled><?php echo $notes[$k]; ?></textarea>
                                                                        </div>
                                                                        <?php
                                                                    } else {
                                                                        echo' Access is not granted to view notes for this user. Please contact Admin.';
                                                                    }
                                                                    ?>
                                                                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- edit notes modal, to edit notes and other details regarding call-->
                                                    <div id='edit_notes<?php echo $k; ?>' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog modal-lg" style="width:1150px;">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                                                    <h4 class="modal-title">Edit Notes for <?php echo $name; ?><BR/> 
                                                                <span style="font-size:15px;">(Fill under each heading, do not erase the heading)</span>
                                                                    </h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php if ($allow_view_notes == 1) { ?>

                                                                        <div id="err_edit_notes_<?php echo $k; ?>" class="msg_disp"></div>
                                                                        <form class="notes-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="form_edit_notes_<?php echo $k; ?>" method="POST" >
                                                                            <div class="row">
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="col-sm-6">
                                                                                            <textarea class="form-control" rows="1" id="edit-notes<?php echo $k; ?>" name=" edit_notes" wrap="virtual" style="min-height:6px;height:235px;"><?php echo $notes[$k]; ?></textarea>
                                                                                        </div>
                                                                                        <div class="col-sm-6" style="padding-left: 5px;padding-right: 0px;">
                                                                                            <div class="row" style="margin-top:0%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label"> Date  </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <div class="input-group">
                                                                                                                <input type="text" class="form-control" id="datepicker2_<?php echo $k; ?>" placeholder="Select date here" name="edit_date" value="<?php echo date('d-m-Y', strtotime($taken_at[$k])); ?>">
                                                                                                                <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="margin-top:1.2%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label"> Time  </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <div class="input-group">
                                                                                                                <div class="bootstrap-timepicker">
                                                                                                                    <input id="timepicker3_<?php echo $k; ?>" type="text" class="form-control" name="edit_time" value="<?php echo date('h:i A', strtotime($taken_at[$k])); ?>">
                                                                                                                </div>
                                                                                                                <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="margin-top:1.2%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label"> Duration </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <select class="form-control" name="edit_duration"><?php echo $duration[$k]; ?>
                                                                                                                <option value="20">20 minutes</option>
                                                                                                                <option value="30">30 minutes</option>
                                                                                                                <option value="40">40 minutes</option>
                                                                                                                <option value="50">50 minutes</option>
                                                                                                                <option value="60">60 minutes</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="margin-top:1.2%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label"> Reason  </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <select class="form-control reason-list" name="edit_reason">
                                                                                                                <option value="0" selected disabled>Select Reason</option>
                                                                                                                <?php for ($w = 0; $w < count($reason_code_list); $w++) { ?>
                                                                                                                    <option value="<?php echo $reason_code_list[$w]; ?>" <?php echo $reason_code[$k] == $reason_code_list[$w] ? "selected" : ""; ?>><?php echo $reason_text_list[$w]; ?></option>
                                                                                                                <?php } ?>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="margin-top:1.2%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label"> SubReason  </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <select class="form-control subreason-list" name="edit_subreason">
                                                                                                                <?php
                                                                                                                $x = 0;
                                                                                                                $stmt10 = $dbh->prepare("SELECT * FROM list_subreason WHERE reason_code=?");
                                                                                                                $stmt10->execute(array($reason_code[$k]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1002].");
                                                                                                                if ($stmt10->rowCount() != 0) {
                                                                                                                    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                        $subreasonCode_list[$x] = $row10['subreason_code'];
                                                                                                                        $subreasonText_list[$x] = $row10['subreason_text'];
                                                                                                                        ?>
                                                                                                                        <option value="<?php echo $subreasonCode_list[$x]; ?>" <?php echo $subreason_code[$k] == $subreasonCode_list[$x] ? "selected" : ""; ?>><?php echo $subreasonText_list[$x]; ?></option>
                        <?php $x++;
                    }
                } ?>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" style="margin-top:1.2%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label"> Type </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <select class="form-control" name="edit_type">
                <?php for ($w = 0; $w < count($calltype_code_list); $w++) { ?>
                                                                                                                    <option value="<?php echo $calltype_code_list[$w]; ?>" <?php echo ($source[$k] == $calltype_code_list[$w]) ? 'selected' : ""; ?>><?php echo $calltype_text_list[$w]; ?></option>';
                <?php } ?> 
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>                                                                                                                                                                      
                                                                                            <div class="row" style="margin-top:1.2%;">
                                                                                                <div class="form-group">
                                                                                                    <div class=" col-sm-12">
                                                                                                        <div class=" col-sm-1"></div>
                                                                                                        <div class=" col-sm-3" style="padding-left: 7px;">
                                                                                                            <label class="control-label">Employee Type </label>
                                                                                                        </div>
                                                                                                        <div class="col-sm-8">
                                                                                                            <select class="form-control" name="emp_type">
                                                                                                                <option <?php echo ($emp_type[$k] == 'E') ? "selected" : ""; ?> value="E">Employee</option>
                                                                                                                <option <?php echo ($emp_type[$k] == 'D') ? "selected" : ""; ?> value="D">Dependent</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer" style="margin-top:2%;">
                                                                                <input type="hidden" name="sr_no_notes" value="<?php echo $sr_notes[$k]; ?>">
                                                                                <input type="hidden" name="cid" value="<?php echo $cid; ?>">
                                                                                <input type="hidden" name="tid" value="<?php echo $tid; ?>">
                                                                                <button type="button" class="btn btn-danger waves-effect" style="margin-right:2%;"  id="edit-notes-no-btn" name="edit-notes-no-btn"  >Discard & Close</button>
                                                                                <input type="submit" id="edit-notes-submit-btn" name="edit-notes-submit-btn" value="Save" class="btn btn-primary" style="margin-right:1.7%;"/>
                                                                            </div>
                                                                        </form><?php
                                                                        #on click of edit-notes-submit-button
                                                                        if (isset($_REQUEST['edit-notes-submit-btn'])) {
                                                                            if (isset($_REQUEST['edit_notes']) && !empty($_REQUEST['edit_notes']) && $_REQUEST['edit_notes'] !== '' && isset($_REQUEST['edit_date']) && !empty($_REQUEST['edit_date']) && $_REQUEST['edit_date'] !== '' && isset($_REQUEST['edit_time']) && !empty($_REQUEST['edit_time']) && $_REQUEST['edit_time'] !== '' && isset($_REQUEST['edit_duration']) && !empty($_REQUEST['edit_duration']) && $_REQUEST['edit_duration'] !== '' && isset($_REQUEST['emp_type'])) {

                                                                                $sr_no_notes = $_REQUEST['sr_no_notes']; //to get cid
                                                                                $new_notes = encrypt($_REQUEST['edit_notes'], $encryption_key); //to get notes
                                                                                $new_date = str_replace('/', '-', $_REQUEST['edit_date']);
                                                                                $new_date = date("Y-m-d ", strtotime($new_date)); //date from date picker
                                                                                $new_time = date("H:i:s ", strtotime($_REQUEST['edit_time'])); //time from time picker
                                                                                $new_notes_timestamp = $new_date . $new_time; //combining both date and time
                                                                                $new_duration = $_REQUEST['edit_duration']; //to get duration
                                                                                if (isset($_REQUEST['edit_subreason']) && $_REQUEST['edit_subreason'] != '') {
                                                                                    $new_reason = $_REQUEST['edit_subreason']; //to get reason
                                                                                } else {
                                                                                    $new_reason = "";
                                                                                }
                                                                                $tid = $_REQUEST['tid']; //to get tid
                                                                                $emp_type = $_REQUEST['emp_type'];
                                                                                $source_type = $_REQUEST['edit_type'];
                                                                                //to update into call_callers_notes
                                                                                $stmt30 = $dbh->prepare("UPDATE call_callers_notes SET  notes_text = ?, taken_by = ?, taken_at = ?, duration = ?, reason = ?, source=?, emp_type = ? WHERE sr_no = ?");
                                                                                $stmt30->execute(array($new_notes, $tid, $new_notes_timestamp, $new_duration, $new_reason, $source_type, $emp_type, $sr_no_notes)) or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALLER DETAILS-1007].");

                                                                                echo '<script src="../assets/js/jquery.min.js"></script>
                                                                                <script> $(document).ready(function () {
                                                                                	$("#err_edit_notes_' . $k . '").addClass("alert");
                                                                                	$("#err_edit_notes_' . $k . '").addClass("alert-success");
                                                                                	$("#err_edit_notes_' . $k . '").html("Notes has been updated.");
                                                                                	$("#form_edit_notes_' . $k . '").hide();
                                                                                	$("#edit_notes' . $k . '").modal("show");
                                                                                	});
                                                                                	</script>';
                                                                            } else {
                                                                                //  something is wrong the client side validation
                                                                                //  either the client has disabled the javascript stop the execution of the script
                                                                                die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_CALLER_DETAILS_NOTES_JS_MSNG");
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                    } else {
                                                                        echo' Access is not granted to edit notes for this user. Please contact Admin.';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            } else {
                                                //do nothing
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    } else {
                                        echo "Unable to fetch user details. Error Code : CIDNIU";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <!-- Discard changes confirmation modal -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                        <!-- dialog body for discard changes -->
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            Are you sure you want to discard changes?
                        </div>
                        <!-- dialog buttons -->
                        <button type="button" class="btn btn-danger" onclick="reload();" >Yes </button>
                        <input type="submit" value="No" id="no-btn" name="no-btn" class="btn btn-success" data-dismiss="modal"/>
                    </form>
                </div>
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>

        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/plugins/moment/moment.js"></script>
        <script src="../assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiQbVEMemZgEE5gdyI50TxyIUK0Ba9PBI&libraries=places&autocomplete=true" type="text/javascript" ></script>
        <script src="../assets2/global/scripts/jquery.geocomplete.min.js"></script>

        <script src="../../assets/lib/summernote/summernote.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
        <script type="text/javascript">
    $(document).ready(function () {
        $('#editor1').summernote({
          height: 300
        });
        
     <?php   if (isset($total_notes)) {
        for ($k = 0; $k < $total_notes; $k++) { ?>
        $("#edit-notes<?php echo $k; ?>").summernote({
            height: 300
        });
        $("#view-notes<?php echo $k; ?>").summernote({
            height: 300
        });
    <?php }} ?>
    });
</script>
        <script>
            function notes(){
                $('.note-toolbar').hide();
                $('.note-editable').css('height', '400px');
            }

            $(function () {
                $("#location").geocomplete({
                    details: ".geo-details",
                    detailsAttribute: "data-geo"
                });
            });
        </script>
        <script>
            //script to able to set time from 8 am to 8 pm
            //if time is below or exceeds, set back to 8 am
            // Time Picker
            jQuery('#timepicker2').timepicker({
                showMeridian: true,
                minuteStep: 1,
                showInputs: true,
            });

        </script>
        <script>
            var date = new Date();
            date.setDate(date.getDate());
            $('#datepicker').datepicker({
                startDate: date,
            });
            $('#datepicker1').datepicker({
                autoclose: true,
                endDate: "today",
                maxDate: date
            });

        </script>
        <script>
            $('.notes-form').validate({
                rules: {
                    notes: {
                        required: true,
                    },
                    date: {
                        required: true,
                    },
                    time: {
                        required: true,
                    },
                    duration: {
                        required: true,
                    },
                    source: {
                        required: true,
                    },
                    edit_reason: {
                        required: true,
                    },
                    edit_subreason: {
                        required: true,
                    }
                },
                messages: {
                    notes: {
                        required: "Please Enter Notes",
                    },
                    date: {
                        required: "Please Select a Date",
                    },
                    time: {
                        required: "Please Select a Time",
                    },
                    duration: {
                        required: "Please Select Duration of Call",
                    },
                    source: {
                        required: "Please Select Type",
                    },
                    edit_reason: {
                        required: "Please Select Reason",
                    },
                    edit_subreason: {
                        required: "Please Select SubReason",
                    }
                }
            });
        </script>
        <script>
            $('#form-horizontal').validate({
                rules: {
                    tid_schedule: {
                        required: true,
                    },
                    reschedule_date: {
                        required: true,
                    },
                    reschedule_time: {
                        required: true,
                    }
                },
                messages: {
                    tid_schedule: {
                        required: "Please Select Coach",
                    },
                    reschedule_date: {
                        required: "Please Select Schedule Date",
                    },
                    reschedule_time: {
                        required: "Please Select Re-Schedule Time",
                    }
                }
            });
            $("#form-horizontal-profile").validate({
                rules: {
                    client_name: {
                        required: true,
                    },
                    client_email: {
                        required: true,
                        email: true
                    },
                    client_mobile: {
                        required: true,
                        phoneUS: true
                    },
                    client_age: {
                        required: true,
                    },
                    client_gender: {
                        required: true,
                    },
                    client_active: {
                        required: true,
                    },
                    client_location: {
                        required: true,
                    },
                },
                messages: {
                    client_name: {
                        required: "Please Enter Client Name",
                    },
                    client_email: {
                        required: "Please Enter Client Email",
                    },
                    client_mobile: {
                        required: "Please Enter Client Mobile",
                    },
                    client_age: {
                        required: "Please Select Client Year of Birth",
                    },
                    client_gender: {
                        required: "Please Select Client Gender",
                    },
                    client_active: {
                        required: "Please Select Client Status",
                    },
                    client_location: {
                        required: "Please Enter Client Location",
                    },
                }
            });
        </script>
        <?php
        if (isset($total_notes)) {
            for ($k = 0; $k < $total_notes; $k++) {
                ?>
                <script>
                    $('#form_edit_notes_<?php echo $k; ?>').validate({
                        rules: {
                            edit_notes: {
                                required: true,
                            },
                            edit_date: {
                                required: true,
                            },
                            edit_time: {
                                required: true,
                            },
                            edit_duration: {
                                required: true,
                            },
                            edit_source: {
                                required: true,
                            }
                        },
                        messages: {
                            edit_notes: {
                                required: "Please Enter Notes",
                            },
                            edit_date: {
                                required: "Please Select a Date",
                            },
                            edit_time: {
                                required: "Please Select a Time",
                            },
                            edit_duration: {
                                required: "Please Select Duration of Call",
                            },
                            edit_source: {
                                required: true,
                            }
                        }
                    });

                    //script to able to set time from 8 am to 8 pm
                    //if time is below or exceeds, set back to 8 am
                    // Time Picker
                    jQuery('#timepicker3_<?php echo $k; ?>').timepicker({
                        showMeridian: true,
                        minuteStep: 1,
                        showInputs: true,
                    });
                    $('#datepicker2_<?php echo $k; ?>').datepicker({
                    });
                </script>

                <?php
            }
        }
        ?>


        <script>
            function reload() {
                window.location.href = "caller_details.php?cid=<?php echo $cid; ?>";
                    }
        </script>
        <script>
            $("#notes-no-btn").click(function () {

                var new_text_notes = $("#notes").val();
                var new_text_notes_length = new_text_notes.length;

                if (new_text_notes_length != " ") {

                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        // "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                } else {

                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                }
            });
            $("#edit-notes-no-btn").click(function () {

                var new_text_notes = $("#edit-notes").val();
                var new_text_notes_length = new_text_notes.length;

                if (new_text_notes_length != " ") {

                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        // "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                } else {

                    $("#myModal").on("show", function () {
                        $("#myModal a.btn").on("click", function (e) {
                            $("#myModal").modal('hide');
                        });
                    });
                    $("#myModal").on("hide", function () {
                        $("#myModal a.btn").off("click");
                    });

                    $("#myModal").off("hidden", function () {
                        $("#myModal").remove();
                    });

                    $("#myModal").modal({
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                }
            });
        </script>
        <script type="text/javascript">
<?php if ($status == 1) { ?>
                $(document).ready(function () {
                    $("#reschedule_msg").addClass("alert");
                    $("#reschedule_msg").addClass("alert-success");
                    $("#reschedule_msg").html("Call has been Scheduled.");
                    $("#reschedule_calls").modal("show");
                });
<?php } else if ($status == 2) { ?>
                $(document).ready(function () {
                    $('.note-toolbar').hide();
                    $("#err_name").addClass("alert");
                    $("#err_name").addClass("alert-success");
                    $("#err_name").html("Notes is added.");
                    $("#add_call").modal("show");
                });
<?php } else if ($status == 3) { ?>
                $(document).ready(function () {
                    $("#err_assign").addClass("alert");
                    $("#err_assign").addClass("alert-success");
                    $("#err_assign").html("Coach is changed.");
                    $("#assign_client_form ").hide();
                    $("#assign_client").modal("show");
                });
<?php } else if ($status == 4) { ?>
                $(document).ready(function () {
                    $("#err_profile").addClass("alert");
                    $("#err_profile").addClass("alert-success");
                    $("#err_profile").html("Profile has been update.");
                    $("#edit_profile").modal("show");
                });
<?php } ?>
        </script>
        <script>
            function call_number_func(nameSelect)
            {
                if (nameSelect) {
                    if ("sc" == nameSelect.value) {
                        document.getElementById("call_number").style.display = "block";
                    } else {
                        document.getElementById("call_number").style.display = "none";
                    }
                } else {
                    document.getElementById("call_number").style.display = "none";
                }
            }
        </script>
        <script>
            $("select.reason-list").change(function () {
                var selectedReason = $(this).val();
                $(".subreason-list").prop('disabled', false);
                $.ajax({
                    type: "POST",
                    url: "get_subreason.php",
                    data: {reason: selectedReason}
                }).done(function (data) {
                    $(".subreason-list").html(data);
                });
            });
        </script>
    </body>
</html>