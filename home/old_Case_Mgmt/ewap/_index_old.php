<?php
# Including functions and other files
include '../if_loggedin.php';
include 'ewap-config.php';
include 'soh-config.php';

#getting tid from session
$tid = $_SESSION['tid'];

//intialize the arrays
$reason_array = array("Work Stress", "Relationship Stress", "Parenting", "Personal stress", "Addiction","Mindfulness");
$type_array = array("f2f-onsite", "f2f-soh", "f2f-affiliate", "skype", "dc", "sc");
$type_name_array = array("F2F - Onsite", "F2F - SOH", "F2F - Affiliate", "Skype", "Direct Call", "Scheduled Call");
$reason_color_array = array('#E06666', '#3BB9B9', '#83221e', '#6AA84F', '#FFC332',"#f4f8fb");
$type_color_array = array('#3BB9B9', '#1C5D99', '#83221e', '#FFC332', '#E06666', '#674ea7', '#6AA84F');

//intialize all counters to 0
$count_7to10 = 0;
$count_10to1 = 0;
$count_1to5 = 0;
$count_5to9 = 0;
$count_7to10_7days = 0;
$count_10to1_7days = 0;
$count_1to5_7days = 0;
$count_5to9_7days = 0;
$count_7to10_30days = 0;
$count_10to1_30days = 0;
$count_1to5_30days = 0;
$count_5to9_30days = 0;

#default timezone
date_default_timezone_set("Asia/Kolkata");
$date_current = date('Y-m-d');//to get current date
$date_7days_ago = date('Y-m-d ', strtotime('-7 days'));//to get last 7th day date
$date_30days_ago = date('Y-m-d ', strtotime('-30 days'));//to get last 30th day date

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");

#to fetch overall call timings from call_callers_notes
$i = 0; //counter for total number of call timings
$stmt00 = $dbh->prepare("SELECT HOUR(taken_at) AS hour FROM call_callers_notes WHERE 1");
$stmt00->execute()or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1000].");
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $hour = $row00['hour'];
        if ($hour >= 7 && $hour < 10) {//if hour is between 7AM to 10AM, increment counter,else proceed
            $count_7to10++;
        } else if ($hour >= 10 && $hour < 13) {//if hour is between 10AM to 1PM, increment counter,else proceed
            $count_10to1++;
        } else if ($hour >= 13 && $hour < 17) {//if hour is between 1PM to 5PM, increment counter,else proceed
            $count_1to5++;
        } else if ($hour >= 17 && $hour < 21) {//if hour is between 5PM to 9PM, increment counter,else proceed
            $count_5to9++;
        }
        $i++;
    }
}

#to fetch all the call timings from call_callers_notes for last 7 days
$j = 0; //counter for total number of call timings
$stmt10 = $dbh->prepare("SELECT HOUR(taken_at) AS hour FROM call_callers_notes WHERE DATE(taken_at) <= ? AND DATE(taken_at) >= ?");
$stmt10->execute(array($date_current, $date_7days_ago))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1001].");
if ($stmt10->rowCount() != 0) {
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $hour = $row10['hour'];
        if ($hour >= 7 && $hour < 10) {//if hour is between 7AM to 10AM, increment counter,else proceed
            $count_7to10_7days++;
        } else if ($hour >= 10 && $hour < 13) {//if hour is between 10AM to 1PM, increment counter,else proceed
            $count_10to1_7days++;
        } else if ($hour >= 13 && $hour < 17) {//if hour is between 1PM to 5PM, increment counter,else proceed
            $count_1to5_7days++;
        } else if ($hour >= 17 && $hour < 21) {//if hour is between 5PM to 9PM, increment counter,else proceed
            $count_5to9_7days++;
        }
        $j++;
    }
}

#to fetch all the call timings from call_callers_notes for last 30 days
$k = 0; //counter for total number of call timings
$stmt20 = $dbh->prepare("SELECT HOUR(taken_at) AS hour FROM call_callers_notes WHERE DATE(taken_at) <= ? AND DATE(taken_at) >= ?");
$stmt20->execute(array($date_current, $date_30days_ago))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1002].");
if ($stmt20->rowCount() != 0) {
    while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC)) {
        $hour = $row20['hour'];
        if ($hour >= 7 && $hour < 10) {//if hour is between 7AM to 10AM, increment counter,else proceed
            $count_7to10_30days++;
        } else if ($hour >= 10 && $hour < 13) {//if hour is between 10AM to 1PM, increment counter,else proceed
            $count_10to1_30days++;
        } else if ($hour >= 13 && $hour < 17) {//if hour is between 1PM to 5PM, increment counter,else proceed
            $count_1to5_30days++;
        } else if ($hour >= 17 && $hour < 21) {//if hour is between 5PM to 9PM, increment counter,else proceed
            $count_5to9_30days++;
        }
        $k++;
    }
}

#to fetch overall count of callers for each type of reason 
$l = 0; //counter for total number of call 
for ($m = 0; $m < count($reason_array); $m++) {
    $stmt30 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_callers_notes WHERE reason = ?");
    $stmt30->execute(array($reason_array[$m]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1003].");
    if ($stmt30->rowCount() != 0) {//if count != 0,then proceed
        $row30 = $stmt30->fetch();
        $count_reason[$l] = $row30['count']; //fetch the count and store it an array
    } else {//set value as 0
        $count_reason[$l] = 0;
    }
    $l++; //increment counter
}

#to fetch count of callers for each type of reason for last 7 days
$n = 0; //counter for total number of call 
for ($p = 0; $p < count($reason_array); $p++) {
    $stmt40 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_callers_notes WHERE reason = ? AND DATE(taken_at) <= ? AND DATE(taken_at) >= ?");
    $stmt40->execute(array($reason_array[$p], $date_current, $date_7days_ago))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1004].");
    if ($stmt40->rowCount() != 0) {//if count != 0,then proceed
        $row40 = $stmt40->fetch();
        $count_reason_7days[$n] = $row40['count']; //fetch the count and store it an array
    } else {//set value as 0
        $count_reason_7days[$n] = 0;
    }
    $n++; //increment counter
}

#to fetch count of callers for each type of reason for last 30 dyas
$q = 0; //counter for total number of call 
for ($r = 0; $r < count($reason_array); $r++) {
    $stmt50 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_callers_notes WHERE reason = ? AND DATE(taken_at) <= ? AND DATE(taken_at) >= ?");
    $stmt50->execute(array($reason_array[$r], $date_current, $date_30days_ago))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1005].");
    if ($stmt50->rowCount() != 0) {//if count != 0,then proceed
        $row50 = $stmt50->fetch();
        $count_reason_30days[$q] = $row50['count']; //fetch the count and store it an array
    } else {//set value as 0
        $count_reason_30days[$q] = 0;
    }
    $q++; //increment counter
}

//to fetch overall count of callers for each type of source
$s = 0; //counter for total number of call 
for ($t = 0; $t < count($type_array); $t++) {
    $stmt60 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_callers_notes WHERE source = ?");
    $stmt60->execute(array($type_array[$t]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1006].");
    if ($stmt60->rowCount() != 0) {//if count != 0,then proceed
        $row60 = $stmt60->fetch();
        $count_type[$s] = $row60['count']; //fetch the count and store it an array
    } else {//set value as 0
        $count_type[$s] = 0;
    }
    $s++; //increment counter
}

//to fetch the count of callers for each type of source for last 7 days
$u = 0; //counter for total number of call 
for ($v = 0; $v < count($type_array); $v++) {
    $stmt70 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_callers_notes WHERE source = ? AND DATE(taken_at) <= ? AND DATE(taken_at) >= ?");
    $stmt70->execute(array($type_array[$v], $date_current, $date_7days_ago))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1007].");
    if ($stmt70->rowCount() != 0) {//if count != 0,then proceed
        $row70 = $stmt70->fetch();
        $count_type_7days[$u] = $row70['count']; //fetch the count and store it an array
    } else {//set value as 0
        $count_type_7days[$u] = 0;
    }
    $u++; //increment counter
}

//to fetch the count of callers for each type of source for last 7 days
$w = 0; //counter for total number of call 
for ($x = 0; $x < count($type_array); $x++) {
    $stmt80 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_callers_notes WHERE source = ? AND DATE(taken_at) <= ? AND DATE(taken_at) >= ?");
    $stmt80->execute(array($type_array[$x], $date_current, $date_30days_ago))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1008].");
    if ($stmt80->rowCount() != 0) {//if count != 0,then proceed
        $row80 = $stmt80->fetch();
        $count_type_30days[$w] = $row80['count']; //fetch the count and store it an array
    } else {//set value as 0
        $count_type_30days[$w] = 0;
    }
    $w++; //increment counter
}

//to fetch the count of callers not yet assigned
$stmt30 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_schedule_queue WHERE assigned = ?");
$stmt30->execute(array(0))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1009].");
if ($stmt30->rowCount() != 0) {
    $row30 = $stmt30->fetch();
    $count_unassigned = $row30['count']; //to fetch the count of unassigned calls
}

//to fetch the count of calls pending
$stmt40 = $dbh->prepare("SELECT COUNT(sr_no) AS count FROM call_schedule_call WHERE call_done = ?");
$stmt40->execute(array('0'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CALL-DASHBOARD-1010].");
if ($stmt40->rowCount() != 0) {
    $row40 = $stmt40->fetch();
    $count_total = $row40['count']; //to fetch the count of pending calls
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Dashboard</title>
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/default.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../../assets/js/modernizr.min.js"></script>
        <style>
            .dataTables_info{
                display: none;
            }
            .dataTables_length{
                margin-top: 1.5%;
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-top: 1.5%;
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .tooltip fade bottom in{
                left:0px;
            }

            .title_color{
                color:#223C80;
                font-size: 55px;
                margin-bottom: 30px;
                padding-top:17px;
            }
            
        </style>

    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:75%;margin-top:3%;width:35%;margin-left:-5%;">
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Call Management - Dashboard</h4>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="../my_account.php">
                                            <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                    
                                    <li>
                                        <a href="../help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                    </li> 
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <a href="../logout.php">
                                            <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="left side-menu">
                <?php
// the sidebar varies
                include 'sidebar_call_mgmt.php';
                ?>
            </div>

            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="card-box">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h4 class="header-title m-t-0 m-b-30"> Call Timings</h4>
                                                                <div class="card-tabs" style="margin-top: -52px;"> 
                                                                    <ul class="nav nav-pills pull-right" style="margin-top: -15.5%;"> 
                                                                        <li class=""> 
                                                                            <a href="#cardpills-1" data-toggle="tab" aria-expanded="true">Last 7 days</a> 
                                                                        </li> 
                                                                        <li class=""> 
                                                                            <a href="#cardpills-2" data-toggle="tab" aria-expanded="false">Last 30 days</a> 
                                                                        </li> 
                                                                        <li class="active"> 
                                                                            <a href="#cardpills-3" data-toggle="tab" aria-expanded="false">Total</a> 
                                                                        </li> 
                                                                    </ul> 
                                                                    <div class="tab-content"> 
                                                                        <!-- Last 7 days--->
                                                                        <div id="cardpills-1" class="tab-pane fade"> 
                                                                            <div id="donutchart1" style="margin-top: 100px;"></div>
                                                                        </div> 
                                                                        <!-- Last 30 days--->
                                                                        <div id="cardpills-2" class="tab-pane fade"> 
                                                                            <div id="donutchart2" style="margin-top: 100px;"></div>
                                                                        </div> 
                                                                        <!-- Total--->
                                                                        <div id="cardpills-3" class="tab-pane fade in active"> 
                                                                            <div id="donutchart3" style="margin-top: 100px;"></div>
                                                                        </div>
                                                                    </div> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card-box">
                                                        <div class="row">
                                                            <h4 class="header-title m-t-0 m-b-30">Number of Calls</h4>
                                                            <div class="widget-box-2" style="margin-top:-15px;">
                                                                <div style="text-align:center;">
                                                                    <h1 class="header-title m-t-0 title_color"><?php echo $count_unassigned; ?></h1>
                                                                    <h4>Not yet assigned</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:15px;padding-bottom:10px;">
                                                            <div class="widget-box-2">
                                                                <div style="text-align:center;">
                                                                    <h1 class="header-title m-t-0 title_color"><?php echo $count_total; ?></h1>
                                                                    <h4>Pending</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="card-box">
                                                        <h4 class="header-title m-t-0 m-b-30"> Categories of Call</h4>
                                                        <div class="card-tabs" style="margin-top: -52px;"> 
                                                            <ul class="nav nav-pills pull-right" style="margin-top: -15.5%;"> 
                                                                <li class=""> 
                                                                    <a href="#cardpills-4" data-toggle="tab" aria-expanded="true">Last 7 days</a> 
                                                                </li> 
                                                                <li class=""> 
                                                                    <a href="#cardpills-5" data-toggle="tab" aria-expanded="false">Last 30 days</a> 
                                                                </li> 
                                                                <li class="active"> 
                                                                    <a href="#cardpills-6" data-toggle="tab" aria-expanded="false">Total</a> 
                                                                </li> 
                                                            </ul> 
                                                            <div class="tab-content"> 
                                                                <!-- Last 7 days--->
                                                                <div id="cardpills-4" class="tab-pane fade">
                                                                    <div id="pie1" style="margin-top: 100px;"></div>
                                                                </div> 
                                                                <!-- Last 30 days--->
                                                                <div id="cardpills-5" class="tab-pane fade"> 
                                                                    <div id="pie2" style="margin-top: 100px;"></div>
                                                                </div> 
                                                                <!-- Total--->
                                                                <div id="cardpills-6" class="tab-pane fade in active"> 
                                                                    <div id="pie3" style="margin-top: 100px;"></div>
                                                                </div>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card-box">
                                                        <h4 class="header-title m-t-0 m-b-30"> Reasons for Call</h4>
                                                        <div class="card-tabs" style="margin-top: -52px;"> 
                                                            <ul class="nav nav-pills pull-right" style="margin-top: -15.5%;"> 
                                                                <li class=""> 
                                                                    <a href="#cardpills-7" data-toggle="tab" aria-expanded="true">Last 7 days</a> 
                                                                </li> 
                                                                <li class=""> 
                                                                    <a href="#cardpills-8" data-toggle="tab" aria-expanded="false">Last 30 days</a> 
                                                                </li> 
                                                                <li class="active"> 
                                                                    <a href="#cardpills-9" data-toggle="tab" aria-expanded="false">Total</a> 
                                                                </li> 
                                                            </ul> 
                                                            <div class="tab-content"> 
                                                                <!-- Last 7 days--->
                                                                <div id="cardpills-7" class="tab-pane fade">
                                                                    <div id="reason1" style="margin-top: 100px;"></div>
                                                                </div> 
                                                                <!-- Last 30 days--->
                                                                <div id="cardpills-8" class="tab-pane fade"> 
                                                                    <div id="reason2" style="margin-top: 100px;"></div>
                                                                </div> 
                                                                <!-- Total--->
                                                                <div id="cardpills-9" class="tab-pane fade in active"> 
                                                                    <div id="reason3" style="margin-top: 100px;"></div>
                                                                </div>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                    var dt = new Date();
                    document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>
        <script>
            var resizefunc = [];</script>
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.app.js"></script>
        <script src="../../assets/js/jquery.core.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/graphs.js"></script>
        <script>
            google.charts.setOnLoadCallback(init);
            google.charts.load('current', {'packages': ['corechart', 'table']});
            //Function to draw piechart for department statistics
            //Function to draw chart for male and female statistics
            function drawChart_call_timings1() {
            var data = google.visualization.arrayToDataTable([
            ['Call Timings', 'Number of Calls'],
            ['7 AM to 10 AM',<?php echo $count_7to10 ?>],
            ['10 AM to 1 PM', <?php echo $count_10to1 ?>],
            ['1 PM to 5 PM', <?php echo $count_1to5 ?>],
            ['5 PM to 9 PM', <?php echo $count_5to9 ?>],
            ]);
            var options = {
            title: 'Call Timings',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
                    0: {color: '#1C5D99'},
                            1: {color: '#E06666'},
                            2: {color: '#3BB9B9'},
                            3: {color: '#FFC332'},
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('donutchart3'));
            chart.draw(data, options);
            }
            function drawChart_call_timings2() {
            var data = google.visualization.arrayToDataTable([
            ['Call Timings', 'Number of Calls'],
            ['7 AM to 10 AM',<?php echo $count_7to10_7days ?>],
            ['10 AM to 1 PM', <?php echo $count_10to1_7days ?>],
            ['1 PM to 5 PM', <?php echo $count_1to5_7days ?>],
            ['5 PM to 9 PM', <?php echo $count_5to9_7days ?>],
            ]);
            var options = {
            title: 'Call Timings',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
                    0: {color: '#1C5D99'},
                            1: {color: '#E06666'},
                            2: {color: '#3BB9B9'},
                            3: {color: '#FFC332'},
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('donutchart1'));
            chart.draw(data, options);
            }
            function drawChart_call_timings3() {
            var data = google.visualization.arrayToDataTable([
            ['Call Timings', 'Number of Calls'],
            ['7 AM to 10 AM',<?php echo $count_7to10_30days ?>],
            ['10 AM to 1 PM', <?php echo $count_10to1_30days ?>],
            ['1 PM to 5 PM', <?php echo $count_1to5_30days ?>],
            ['5 PM to 9 PM', <?php echo $count_5to9_30days ?>],
            ]);
            var options = {
            title: 'Call Timings',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
                    0: {color: '#1C5D99'},
                            1: {color: '#E06666'},
                            2: {color: '#3BB9B9'},
                            3: {color: '#FFC332'},
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('donutchart2'));
            chart.draw(data, options);
            }
            function drawChart_categories1() {
            var data = google.visualization.arrayToDataTable([
            ['Categories', 'Number of Calls'],
<?php for ($p = 0; $p < count($type_array); $p++) { ?>
                ['<?php echo $type_name_array[$p]; ?>',<?php echo $count_type_7days[$p]; ?>],
<?php } ?>
            ]);
            var options = {
            title: 'Categories of Call',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
<?php for ($p = 0; $p < count($type_array); $p++) { ?>
    <?php echo $p; ?>: {color: '<?php echo $type_color_array[$p] ?>'},
<?php } ?>
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie1'));
            chart.draw(data, options);
            }
            function drawChart_categories2() {
            var data = google.visualization.arrayToDataTable([
            ['Categories', 'Number of Calls'],
<?php for ($p = 0; $p < count($type_array); $p++) { ?>
                ['<?php echo $type_name_array[$p]; ?>',<?php echo $count_type_30days[$p]; ?>],
<?php } ?>
            ]);
            var options = {
            title: 'Categories of Call',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
<?php for ($p = 0; $p < count($type_array); $p++) { ?>
    <?php echo $p; ?>: {color: '<?php echo $type_color_array[$p] ?>'},
<?php } ?>
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie2'));
            chart.draw(data, options);
            }
            function drawChart_categories3() {
            var data = google.visualization.arrayToDataTable([
            ['Categories', 'Number of Calls'],
<?php for ($p = 0; $p < count($type_array); $p++) { ?>
                ['<?php echo $type_name_array[$p]; ?>',<?php echo $count_type[$p]; ?>],
<?php } ?>
            ]);
            var options = {
            title: 'Categories of Call',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
<?php for ($p = 0; $p < count($type_array); $p++) { ?>
    <?php echo $p; ?>: {color: '<?php echo $type_color_array[$p] ?>'},
<?php } ?>
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('pie3'));
            chart.draw(data, options);
            }
            function drawChart_reasons1() {
            var data = google.visualization.arrayToDataTable([
            ['Reasons', 'Number of Calls'],
<?php for ($p = 0; $p < count($reason_array); $p++) { ?>
                ['<?php echo $reason_array[$p]; ?>',<?php echo $count_reason_7days[$p]; ?>],
<?php } ?>
            ]);
            var options = {
            title: 'Reasons for Call',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
<?php for ($p = 0; $p < count($reason_array); $p++) { ?>
    <?php echo $p; ?>: {color: '<?php echo $reason_color_array[$p] ?>'},
<?php } ?>
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('reason1'));
            chart.draw(data, options);
            }
            function drawChart_reasons2() {
            var data = google.visualization.arrayToDataTable([
            ['Reasons', 'Number of Calls'],
<?php for ($p = 0; $p < count($reason_array); $p++) { ?>
                ['<?php echo $reason_array[$p]; ?>',<?php echo $count_reason_30days[$p]; ?>],
<?php } ?>
            ]);
            var options = {
            title: 'Reasons for Call',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
<?php for ($p = 0; $p < count($reason_array); $p++) { ?>
    <?php echo $p; ?>: {color: '<?php echo $reason_color_array[$p] ?>'},
<?php } ?>
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('reason2'));
            chart.draw(data, options);
            }
            function drawChart_reasons3() {
            var data = google.visualization.arrayToDataTable([
            ['Reasons', 'Number of Calls'],
<?php for ($p = 0; $p < count($reason_array); $p++) { ?>
                ['<?php echo $reason_array[$p]; ?>',<?php echo $count_reason[$p]; ?>],
<?php } ?>
            ]);
            var options = {
            title: 'Reasons for Call',
                    pieSliceText: 'label',
                    chartArea: {width: '80%', height: '60%'},
                    slices: {
<?php for ($p = 0; $p < count($reason_array); $p++) { ?>
    <?php echo $p; ?>: {color: '<?php echo $reason_color_array[$p] ?>'},
<?php } ?>
                    },
                    titlePosition: 'none',
                    pieSliceText: 'none',
            };
            var chart = new google.visualization.PieChart(document.getElementById('reason3'));
            chart.draw(data, options);
            }
            //Function to initialize all charts 
            function init() {
            drawChart_call_timings1();
            drawChart_call_timings2();
            drawChart_call_timings3();
            drawChart_categories1();
            drawChart_categories2();
            drawChart_categories3();
            drawChart_reasons1();
            drawChart_reasons2();
            drawChart_reasons3();
            }
        </script>
    </body>
</html>