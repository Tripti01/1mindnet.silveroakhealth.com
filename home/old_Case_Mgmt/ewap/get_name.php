<?php

//this function is used to get the coach name from the tid
function get_coach_name($tid) {

    //include files and function
    include 'soh-config.php';
    require_once 'functions/crypto_funtions.php';

//databade connection
    $dbh = new PDO($dsn_sco, $sco_user, $sco_pass );
    $dbh->query("use sohdbl");

    //to fetch name from thrp_login
    $stmt10 = $dbh->prepare("SELECT name FROM thrp_login WHERE tid=? LIMIT 1");
    $stmt10->execute(array($tid));
    if ($stmt10->rowCount() != 0) {
        $row10 = $stmt10->fetch();
        $name = decrypt($row10['name'], $encryption_key); // to fetch name
    } else {
        $name = " "; //else set as blank
    }

    return $name;
}

//this function is used to get the corporate name from the corp_id
function get_corp_name($corp_id) {

    //include files and function
    include 'soh-config.php';
	
	$dbh = new PDO($dsn_sco, $sco_user, $sco_pass );
    $dbh->query("use sohdbl");


	$stmt02 = $dbh->prepare("SELECT name FROM corp_login WHERE corp_id=? LIMIT 1");
    $stmt02->execute(array($corp_id));
    if ($stmt02->rowCount() != 0) {
        $row02 = $stmt02->fetch();
        $corp_name = $row02['name'];
    }

    return $corp_name;
}

?>