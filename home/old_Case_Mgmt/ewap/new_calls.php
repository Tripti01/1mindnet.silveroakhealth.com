<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'ewap-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_name.php';

//to check if status are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = "";
}

#getting tid from session
$tid = $_SESSION['tid'];

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");

//to fetch list of all previous callers ordered by cid from call_callers_profile
$i = 0; //counter for total number of callers 
$stmt00 = $dbh->prepare(" SELECT  p.cid, p.name, p.email, p.mobile, p.corp_id, p.emp_id, l.assigned_timestamp FROM call_callers_profile p, call_cid_tid_list l WHERE p.cid = l.cid ORDER BY l.assigned_timestamp DESC LIMIT 500");
$stmt00->execute()or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [NEW CALL-1001].");
/*if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $prev_cid[$i] = $row00['cid']; //to fetch cid list
        $prev_name[$i] = decrypt($row00['name'], $encryption_key); //to fetch name list
        $prev_email[$i] = $row00['email']; //to fetch email list
        $prev_mobile[$i] = $row00['mobile']; //to fetch mobile list
        $prev_corp_id[$i] = $row00['corp_id']; //to fetch corp id list
        $prev_emp_id[$i] = $row00['emp_id']; //to fetch emp id list
		$prev_date[$i] = date("d-m-Y",strtotime($row00['assigned_timestamp'])); //to fetch emp id list
        $i++;
    }
}*/

#on click of new caller button
if (isset($_REQUEST['create_new_btn'])) {
    if (isset($_REQUEST['caller_name']) && !empty($_REQUEST['caller_name']) && $_REQUEST['caller_name'] !== ''  && isset($_REQUEST['caller_company']) && !empty($_REQUEST['caller_company']) && $_REQUEST['caller_company'] !== '' && isset($_REQUEST['caller_mobile']) && !empty($_REQUEST['caller_mobile']) && $_REQUEST['caller_mobile'] !== '' && isset($_REQUEST['caller_yob']) && !empty($_REQUEST['caller_yob']) && $_REQUEST['caller_yob'] !== '' && isset($_REQUEST['caller_gender']) && !empty($_REQUEST['caller_gender']) && $_REQUEST['caller_gender'] !== '' && isset($_REQUEST['location']) && !empty($_REQUEST['location']) && $_REQUEST['location'] !== '' && isset($_REQUEST['caller_emp_id']) && !empty($_REQUEST['caller_emp_id']) && $_REQUEST['caller_emp_id'] !== '') {

        //include files
        include 'ewap-config.php';

        $name = encrypt($_REQUEST['caller_name'], $encryption_key); //to get encrypted name
        $mobile = $_REQUEST['caller_mobile']; //to get mobile
        $corp_id = $_REQUEST['caller_company']; //to get yob
        $gender = $_REQUEST['caller_gender']; //to get gender
        $yob = $_REQUEST['caller_yob']; //to get yob
        $location = $_REQUEST['location']; //to get location
         //to get email
        $emp_id = $_REQUEST['caller_emp_id']; //to get emp_id
		
		if(isset($_REQUEST['caller_email'])){
			$email = $_REQUEST['caller_email'];
		}else{
			$email= '';
		}

        $cid_first_half = "EWAP"; //intialize first_half
        # Start the database realated stuff
        $dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
        $dbh->query("use scodd");

        //to check if cid exsists or not for the given email
        $stmt10 = $dbh->prepare("SELECT cid FROM call_callers_profile WHERE emp_id=? AND corp_id=? LIMIT 1");
        $stmt10->execute(array($emp_id, $corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[NEW CALL-1002].");
        if ($stmt10->rowCount() == 0) {//if doesnt exsists , then proceed.
            //to fetch cvalue
            $stmt20 = $dbh->prepare("SELECT value FROM cvalue WHERE param=? LIMIT 1");
            $stmt20->execute(array('cid'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[NEW CALL-1003].");
            if ($stmt20->rowCount() != 0) {//if exsists fetch cvalue
                $row20 = $stmt20->fetch();
                $cid_second_half = $row20['value']; //to fetch cvalue
            }

            # Increment the running sequnce of cid saved in the database
            $stmt30 = $dbh->prepare("UPDATE cvalue SET value=value+1 WHERE param='cid' LIMIT 1");
            $stmt30->execute(array());

            //create new cid
            $cid = $cid_first_half . $cid_second_half;
            #current curret time
            date_default_timezone_set("Asia/Kolkata");
            $timestamp = date('Y-m-d h:i:s');
            $comment = "New Caller";

            //insert into call_callers_profile
            $stmt40 = $dbh->prepare("INSERT INTO call_callers_profile VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt40->execute(array($cid, $name, $email, $mobile, $gender, $yob, $location, $corp_id, $emp_id, '1', '', '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[NEW CALL-1004].");

            //insert into call_cid_tid_list
            $stmt50 = $dbh->prepare("INSERT INTO call_cid_tid_list VALUES (?,?,?,?,?,?,?,?)");
            $stmt50->execute(array($cid, $tid, $timestamp, $comment, '1', '', '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [CID-1004].");


            # redirecting to the caller_Details
            header('Location:' . $host . '/clinicians/dashboard/call_mgmt/caller_details.php?cid=' . $cid);
        } else {//display error message
            header('Location:' . $host . '/clinicians/dashboard/call_mgmt/new_calls.php?status=1');
        }
    } else {
        //  something is wrong the client side validation 
        //  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:THRP_NEW_CALLS_JS_MSNG");
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
    <title>All Calls</title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

    <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="../assets/js/modernizr.min.js"></script>
    <script src="../assets/js/jquery.min.js"></script>
    <style>
    .pac-container { z-index: 100000 !important; }

    .dataTables_length{
        margin-bottom: 2.5%;
    }
    .dataTables_filter{
        margin-left: 52%;
        margin-bottom: 2.5%;
    }
    .dataTables_info{
        display: none;
    }
    .title_color{
        color:#223C80;
        font-size: 35px;
        margin-bottom: 20px;
        padding-top:15px;
    }
    .table-striped > tbody > tr:nth-of-type(odd), .table-hover > tbody > tr:hover, .table > thead > tr > td.active, .table > tbody > tr > td.active, .table > tfoot > tr > td.active, .table > thead > tr > th.active, .table > tbody > tr > th.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > tbody > tr.active > td, .table > tfoot > tr.active > td, .table > thead > tr.active > th, .table > tbody > tr.active > th, .table > tfoot > tr.active > th {
        background-color: #fff !important;
    }
    .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #3498db;
        border-color:  #3498db;
    }
    .tooltip fade bottom in{
        left:0px;
    }
</style>
</head>
<body class="fixed-left">
    <div id="wrapper">
        <div class="topbar">
            <div class="topbar-left">
                <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:75%;margin-top:3%;width:35%;margin-left:-5%;">
            </div>
            <div class="navbar navbar-default" role="navigation">
                <div class="container">

                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <button class="button-menu-mobile open-left">
                                <i class="zmdi zmdi-menu"></i>
                            </button>
                        </li>
                        <li>
                            <h4 class="page-title">All Calls</h4>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user dropdown-dark">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-user top_right_icon"></i><span class="username username-hide-mobile"><?php echo $thrp_name; ?></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="../my_account.php">
                                        <i class="icon-user top_right_icon"></i> My Account </a>
                                    </li>                                     
                                    <li>
                                        <a href="../help.php">
                                            <i class="icon-question top_right_icon"></i> Help </a>
                                        </li>  
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="../logout.php">
                                                <i class="fa fa-power-off top_right_icon"></i> Log Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="left side-menu">
                        <?php
// the sidebar varies
                        include 'sidebar_call_mgmt.php';
                        ?>
                    </div>
                    <div class="content-page">
                        <!-- Start content -->
                        <div class="content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <div class="row" style="margin-bottom:1.5%;">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-3" style="padding-left:0px;">
                                                        <h4>Search from previous calls</h4>
                                                    </div>
                                                    <div class='col-sm-7'>
                                                        &nbsp;
                                                    </div>
                                                    <div class='col-sm-2' style="margin-top:1%;padding-left: 70px;">
                                                        <a><button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new_call" >New Caller</button></a>
                                                    </div>    
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:1.5%;">
                                                <div class="col-md-12">
                                                    <div class='table-scrollable table-scrollable-borderless'>
                                                        <table id='datatable' class='table table-striped table-bordered'>
                                                            <thead>
                                                                <tr> 
                                                                    <th> CallerID </th>
                                                                    <th> Company Name </th>
                                                                    <th> Employee ID </th>
                                                                    <th> Name </th>
                                                                    <th> Email </th>
                                                                    <th> Mobile </th>
																	<th id="da"> Date </th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if ($stmt00->rowCount() != 0) {
																	 $rows_010 = $stmt00->fetchAll();																				
																	 foreach ($rows_010 as $row00){
																		$prev_date = date("d-m-Y",strtotime($row00['assigned_timestamp'])); 
																		
                                                                        echo '<tr>';
																		//echo '<td style="display:none"></td>';
                                                                        echo '<td>' . $row00['cid'] . '</td>';
                                                                        echo '<td>' . get_corp_name($row00['corp_id']) . '</td>';
                                                                        echo '<td>' . $row00['emp_id'] . '</td>';
                                                                        echo '<td>' . decrypt($row00['name'], $encryption_key) . '</td>';
                                                                        echo '<td>' . $row00['email'] . '</td>';
                                                                        echo '<td>' . $row00['mobile'] . '</td>';
																		echo '<td>' . "<div style=display:none>".date("Y-m-d",strtotime($prev_date))."</div>". $prev_date . '</td>';
                                                                        echo '<td style="text-align:center;"><a href="caller_details.php?cid=' . $row00['cid'] . '"><button type="button" class="btn btn-primary btn-bordred waves-effect btn-sm waves-light">View</button></a></td>';
                                                                        echo '</tr>';
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- New Caller Modal -->
                    <div id='new_call' class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="reload();">x</button>
                                    <h4 class="modal-title"> Create New Caller</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="form-horizontal">
                                        <div id="err_name" class="msg_disp"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class=" col-sm-12">
                                                        <div class="form-group">
                                                            <div class=" col-sm-1"></div>
                                                            <div class=" col-sm-3">
                                                                <label class="control-label">Name</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" placeholder="Caller Name" name="caller_name"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:0.5%;">
                                                    <div class=" col-sm-12">
                                                        <div class="form-group">
                                                            <div class=" col-sm-1"></div>
                                                            <div class=" col-sm-3">
                                                                <label class="control-label">Employee ID</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control" placeholder="Caller Employee ID" name="caller_emp_id"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:0.5%;">
                                                    <div class=" col-sm-12">
                                                        <div class="form-group">
                                                            <div class=" col-sm-1"></div>
                                                            <div class=" col-sm-3">
                                                                <label class="control-label">Company</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <select id="caller_company" name="caller_company" title="" class="form-control">
																  <?php 
																		# Start the database realated stuff
																		$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
																		$dbh->query("use sohdbl");

																		$w = 0; //counter to fetch all notes
																		$stmt20 = $dbh->prepare("SELECT corp_id, corp_fullname FROM corp_profile WHERE 1 ORDER BY corp_fullname ASC");
																		$stmt20->execute(array())or die("Some error occured. Please try again. ErrorCode: [CORP DETAILS-1001].");
																		if ($stmt20->rowCount() != 0) {
																			while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC)) {
																				$corp_id_list[$w] = $row20['corp_id']; //to fetch reason
																				$corp_name_list[$w] = $row20['corp_fullname'];																				
																				echo '<option value="'.$corp_id_list[$w].'">'.$corp_name_list[$w].'</option>';
																				$w++;
																			}
																		}
																	?> 
																</select>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="row" style="margin-top:0.5%;">
                                                <div class=" col-sm-12">
                                                    <div class="form-group">
                                                        <div class=" col-sm-1"></div>
                                                        <div class=" col-sm-3">
                                                            <label class="control-label">Email</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" placeholder="Caller Email" name="caller_email"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:0.5%;">
                                                <div class=" col-sm-12">
                                                    <div class="form-group">
                                                        <div class=" col-sm-1"></div>
                                                        <div class=" col-sm-3">
                                                            <label class="control-label">Mobile</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" placeholder="Caller Mobile" name="caller_mobile"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:0.5%;">
                                                <div class=" col-sm-12">
                                                    <div class="form-group">
                                                        <div class=" col-sm-1"></div>
                                                        <div class=" col-sm-3">
                                                            <label class="control-label">Gender</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <select id="gender" name="caller_gender" title="" class="form-control">
                                                                <option value="" disabled="" selected="">Select</option>
                                                                <option value="M"> Male </option>
                                                                <option value="F"> Female </option>
                                                                <option value="O">Other</option>
                                                                <option value="W">Would rather not say</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top:0.5%;">
                                                <div class=" col-sm-12">
                                                    <div class="form-group">
                                                        <div class=" col-sm-1"></div>
                                                        <div class=" col-sm-3">
                                                            <label class="control-label">Year Of Birth</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <select id="yob" name="caller_yob" title="" class="form-control">
                                                                <option value="2010">2010</option>
                                                                <option value="2009">2009</option>
                                                                <option value="2008">2008</option>
                                                                <option value="2007">2007</option>
                                                                <option value="2006">2006</option>
                                                                <option value="2005">2005</option>
                                                                <option value="2004">2004</option>
                                                                <option value="2003">2003</option>
                                                                <option value="2002">2002</option>
                                                                <option value="2001">2001</option>
                                                                <option value="2000">2000</option>
                                                                <option value="1999">1999</option>
                                                                <option value="1998">1998</option>
                                                                <option value="1997">1997</option>
                                                                <option value="1996">1996</option>
                                                                <option value="1995">1995</option>
                                                                <option value="1994">1994</option>
                                                                <option value="1993">1993</option>
                                                                <option value="1992">1992</option>
                                                                <option value="1991">1991</option>
                                                                <option value="1990">1990</option>
                                                                <option value="1989">1989</option>
                                                                <option value="1988">1988</option>
                                                                <option value="1987">1987</option>
                                                                <option value="1986">1986</option>
                                                                <option value="1985">1985</option>
                                                                <option value="1984">1984</option>
                                                                <option value="1983">1983</option>
                                                                <option value="1982">1982</option>
                                                                <option value="1981">1981</option>
                                                                <option value="1980" selected="">1980</option>
                                                                <option value="1979">1979</option>
                                                                <option value="1978">1978</option>
                                                                <option value="1977">1977</option>
                                                                <option value="1976">1976</option>
                                                                <option value="1975">1975</option>
                                                                <option value="1974">1974</option>
                                                                <option value="1973">1973</option>
                                                                <option value="1972">1972</option>
                                                                <option value="1971">1971</option>
                                                                <option value="1970">1970</option>
                                                                <option value="1969">1969</option>
                                                                <option value="1968">1968</option>
                                                                <option value="1967">1967</option>
                                                                <option value="1966">1966</option>
                                                                <option value="1965">1965</option>
                                                                <option value="1964">1964</option>
                                                                <option value="1963">1963</option>
                                                                <option value="1962">1962</option>
                                                                <option value="1961">1961</option>
                                                                <option value="1960">1960</option>
                                                                <option value="1959">1959</option>
                                                                <option value="1958">1958</option>
                                                                <option value="1957">1957</option>
                                                                <option value="1956">1956</option>
                                                                <option value="1955">1955</option>
                                                                <option value="1954">1954</option>
                                                                <option value="1953">1953</option>
                                                                <option value="1952">1952</option>
                                                                <option value="1951">1951</option>
                                                                <option value="1950">1950</option>
                                                                <option value="1949">1949</option>
                                                                <option value="1948">1948</option>
                                                                <option value="1947">1947</option>
                                                                <option value="1946">1946</option>
                                                                <option value="1945">1945</option>
                                                                <option value="1944">1944</option>
                                                                <option value="1943">1943</option>
                                                                <option value="1942">1942</option>
                                                                <option value="1941">1941</option>
                                                                <option value="1940">1940</option>
                                                                <option value="1939">1939</option>
                                                                <option value="1938">1938</option>
                                                                <option value="1937">1937</option>
                                                                <option value="1936">1936</option>
                                                                <option value="1935">1935</option>
                                                                <option value="1934">1934</option>
                                                                <option value="1933">1933</option>
                                                                <option value="1932">1932</option>
                                                                <option value="1931">1931</option>
                                                                <option value="1930">1930</option>
                                                                <option value="1929">1929</option>
                                                                <option value="1928">1928</option>
                                                                <option value="1927">1927</option>
                                                                <option value="1926">1926</option>
                                                                <option value="1925">1925</option>
                                                                <option value="1924">1924</option>
                                                                <option value="1923">1923</option>
                                                                <option value="1922">1922</option>
                                                                <option value="1921">1921</option>
                                                                <option value="1920">1920</option>
                                                                <option value="1919">1919</option>
                                                                <option value="1918">1918</option>
                                                                <option value="1917">1917</option>
                                                                <option value="1916">1916</option>
                                                                <option value="1915">1915</option>
                                                                <option value="1914">1914</option>
                                                                <option value="1913">1913</option>
                                                                <option value="1912">1912</option>
                                                                <option value="1911">1911</option>
                                                                <option value="1910">1910</option>
                                                                <option value="1909">1909</option>
                                                                <option value="1908">1908</option>
                                                                <option value="1907">1907</option>
                                                                <option value="1906">1906</option>
                                                                <option value="1905">1905</option>
                                                                <option value="1904">1904</option>
                                                                <option value="1903">1903</option>
                                                                <option value="1902">1902</option>
                                                                <option value="1901">1901</option>
                                                                <option value="1900">1900</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top:0.5%;">
                                                <div class=" col-sm-12">
                                                    <div class="form-group">
                                                        <div class=" col-sm-1"></div>
                                                        <div class=" col-sm-3">
                                                            <label class="control-label">Location</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="geo-details">
                                                                <input type="text" placeholder="Location" name="location" class="form-control" id="location" autocomplete="off">
                                                                <input class="form-control placeholder-no-fix" data-geo="country" value="" id="country" name="country" type="hidden">
                                                                <input class="form-control placeholder-no-fix" data-geo="administrative_area_level_1" value="" id="state" name="state" type="hidden">
                                                                <input class="form-control placeholder-no-fix" data-geo="administrative_area_level_2" value="" id="city" name="city" type="hidden">
                                                                <input class="form-control placeholder-no-fix" data-geo="lat" value="" id="latitude" name="latitude" type="hidden">
                                                                <input class="form-control placeholder-no-fix" data-geo="lng" value="" id="longitude" name="longitude" type="hidden">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12" style="text-align: center;margin-top:4%;">
                                                    <input type="submit" value="Create New Caller" name="create_new_btn" class="btn btn-primary"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <p style="text-align:right;">Copyright &copy; <script type="text/javascript">
                        var dt = new Date();
                        document.write(dt.getFullYear());
                    </script>, SilverOakHealth. All Rights Reserved.
                </p>
            </footer>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/fastclick.js"></script>

        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>

        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>

        <script src="../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiQbVEMemZgEE5gdyI50TxyIUK0Ba9PBI&libraries=places&autocomplete=true"></script>
        <script src="../assets2/global/scripts/jquery.geocomplete.min.js"></script>

        <script>
            $(function () {
                $("#location").geocomplete({
                    details: ".geo-details",
                    detailsAttribute: "data-geo"
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable(
				{
				 fixedHeader: true
				});
            });
			$("document").ready(function() {
				setTimeout(function() {
					$("#da").trigger('click');
				},10);
				
				setTimeout(function() {
					$("#da").trigger('click');
				},10);
			});
        </script>
        <!---Form validation---->
        <script>
            $("#form-horizontal_1").validate({
                rules: {
                    req: {
                        required: true,
                    },
                },
                messages: {
                    req: {
                        required: "Please Enter Email or Mobile Number",
                    },
                }

            });
            $("#form-horizontal").validate({
                rules: {
                    caller_name: {
                        required: true,
                    },
                    caller_mobile: {
                        required: true,
                        phoneUS: true
                    },
                    caller_company: {
                        required: true,
                    },
                    caller_yob: {
                        required: true,
                    },
                    caller_gender: {
                        required: true,
                    },
                    location: {
                        required: true,
                    },
                    caller_emp_id: {
                        required: true,
                    },
                },
                messages: {
                    caller_name: {
                        required: "Please Enter Caller Name",
                    },
                    caller_mobile: {
                        required: "Please Enter Caller Mobile",
                    },
                    caller_company: {
                        required: "Please Select Caller Company",
                    },
                    caller_yob: {
                        required: "Please Select Caller Year of Birth",
                    },
                    caller_gender: {
                        required: "Please Select Caller Gender",
                    },
                    location: {
                        required: "Please Enter Caller Location",
                    },
                    caller_emp_id: {
                        required: "Please Enter Caller Employee ID",
                    },
                }
            });
        </script>
        <script>
            function reload() {
                window.location.href = "<?php echo $host; ?>/clinicians/dashboard/call_mgmt/new_calls.php";
            }
        </script>
        <script type="text/javascript">
            <?php if ($status == 1) { ?>
                $(document).ready(function () {
                    $("#err_name").addClass("alert");
                    $("#err_name").addClass("alert-danger");
                    $("#err_name").html("Employee id already exsists.");
                    $("#new_call").modal("show");
                });
                <?php } ?>
            </script>
        </body>
        </html>