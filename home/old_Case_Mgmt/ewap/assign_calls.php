<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'ewap-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_name.php';

#getting tid from session
$tid = $_SESSION['tid'];

//to fetch current year
$current_year = date("Y");

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");

#to fetch all the calls from call_scheduled_queue by ascending order of date
$i = 0; //counter for total number of callers in queue
$stmt00 = $dbh->prepare("SELECT * FROM call_schedule_queue WHERE assigned = ? AND dismiss=? ORDER BY prefered_date ASC");
$stmt00->execute(array('0', '0'))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode:[THRP-ASSIGN-CALL-1000].");
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $sr_no[$i] = $row00['sr_no']; //to fetch serial number for the call
        $caller_name[$i] = decrypt($row00['name'], $encryption_key); //to fetch decrypted name
        $caller_email[$i] = $row00['email']; //to fecth yob
        $caller_mobile[$i] = $row00['mobile']; //to fetch mobile
        $caller_location[$i] = $row00['location']; //to fetch location
        $caller_age = $current_year - $row00['yob']; //to fecth age
        if ($row00['gender'] == "M") {//if gender is M, then proceed
            $caller_gender = "Male"; //set as masle
        } else if ($row00['gender'] == "F") {//if gender is F, then proceed
            $caller_gender = "Female"; //set as female
        } else if ($row00['gender'] == "O") {//if gender is O, then proceed
            $caller_gender = "Other"; //set as other
        } else if ($row00['gender'] == "W") {//if gender is W, then proceed
            $caller_gender = "Would rather not say"; //set as would rather not say
        }else{
			$caller_gender = '';
		}

		if($row00['type'] == '0'){
			$caller_type[$i] = 'Phone';
		}else{
			$caller_type[$i] = $row00['type'];
		}
		
		
		if($caller_age != ''){
			$caller_age_gen[$i] = $caller_age . ' yrs / ' . $caller_gender ;
		}else{
			$caller_age_gen[$i] = '';
		}		
		
		
        $caller_prefered_date[$i] = $row00['prefered_date']; //to fetch prefered date of call
        $caller_prefered_time[$i] = $row00['prefered_time']; //to fetch prefered time(text)
        $caller_corp_id[$i] = $row00['corp_id']; //to fetch corp_id
        $caller_suggested[$i] = get_coach_name($row00['suggested']); //to fetch any suggested coaches(tid)
        $caller_emp_id[$i] = $row00['emp_id']; //to fetch emp_id
        $caller_assigned[$i] = $row00['assigned']; //to fetch assigned status
        $caller_cid[$i] = $row00['cid']; //to fetch cid
        $i++;
    }
}
$total_callers = $i; //total number of callers
//to fetch list of all coaches from sco
//database connection to sco
$dbh_sco = new PDO($dsn_sco, $login_user, $login_pass);
$dbh_sco->query("use sohdbl");

$j = 0; //counter for all coaches
$stmt10 = $dbh_sco->prepare("SELECT name AS thrp_name, thrp_login.tid FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = ?");
$stmt10->execute(array("THRP"))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ASSIGN-CALL-1004].");
if ($stmt10->rowCount() != 0) {
    while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
        $thrp_tid_list[$j] = $row10['tid']; //to fetch tids
        $thrp_name_list[$j] = decrypt($row10['thrp_name'], $encryption_key); //to fetch thrp_name_list
        $j++;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Unassigned Call Request</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />

        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../assets/js/modernizr.min.js"></script>
        <style>
            .dataTables_info{
                display: none;
            }
            .dataTables_length{
                margin-top: 1.5%;
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-top: 1.5%;
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .tooltip fade bottom in{
                left:0px;
            }
        </style>

    </head>
	<body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card-box">
                                    <div class="row" style="margin-bottom:1.5%;">
                                        <div class="col-md-12">
                                            <div class='table-scrollable table-scrollable-borderless'>
                                                <table id='datatable' class='table table-striped table-bordered'>
                                                    <thead>
                                                        <tr style="text-align:center;">
                                                            <th>#</th>
                                                            <th style="color:#666666;">Name  </th>
                                                            <th>Company Name</th>
                                                            <th>Age / Gender</th>
                                                            <th>Preferred Date </th>
                                                            <th>Preferred Time</th>
                                                            <th>Suggestions</th>
															<th>Type</th>
                                                            <th>Assigned To</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if (isset($sr_no)) {
                                                            $seq_no = 0;
                                                            for ($k = 0; $k < $total_callers; $k++) {

                                                                if ($caller_assigned[$k] == '0') {//if the caller has not been assigned, then proceed
                                                                    $seq_no = $seq_no + 1;
                                                                    echo '<tr>';
                                                                    echo '<td>' . $seq_no . '</td>';
                                                                    echo '<td><a href=""><div data-toggle="tooltip" data-placement="bottom" data-html="true" class="red-tooltip" title=""
                                                             data-original-title="Employee Id- ' . $caller_emp_id[$k] . '<br>
                                                             ' . $caller_email[$k] . '<br>
                                                             ' . $caller_mobile[$k] . '<br>
                                                             ' . $caller_location[$k] . '
                                                             ">' . $caller_name[$k] . '</div></a></td>';
                                                                    echo '<td>' . get_corp_name($caller_corp_id[$k]) . '</td>';
                                                                    echo '<td>' . $caller_age_gen[$k]. '</td>';
                                                                    echo '<td>' . date('d-M-Y', strtotime($caller_prefered_date[$k])) . '</td>';
                                                                    echo '<td>' . $caller_prefered_time[$k] . '</td>';
                                                                    echo '<td>' . $caller_suggested[$k] . '</td>';
																	echo '<td>' . $caller_type[$k] . '</td>';
                                                                    echo '<td>';
                                                                    echo '<button id="btn_' . $k . '" class="btn btn-primary btn-sm waves-effect waves-light" data-toggle="modal" data-target="#assign_to_me_' . $k . '">Assign</button>';
                                                                    ?>
                                                                    <!--- This modal open uniquely to assign caller--->
                                                                <div id='assign_to_me_<?php echo $k; ?>' class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog modal-sm">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reload();"c>x</button>
                                                                                <h4 class="modal-title" id="ModalLabel">Assign Caller</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div id="err_assign_msg_<?php echo $k; ?>" class="msg_disp"></div>
                                                                                <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' id="form_assign_<?php echo $k; ?>" method='POST'>
                                                                                    <div class="row" style="margin-top:-3%;" >
                                                                                        <div class=" col-sm-12" style="text-align: center;">
                                                                                            <div class=" col-sm-12" style="text-align: center;">
                                                                                                <label class="control-label"> Assign the caller to</label>
                                                                                            </div>
                                                                                            <div class="col-sm-12" style="margin-top:4%;">
                                                                                                <select class="form-control" name="tid_schedule">
                                                                                                    <?php for ($j = 0; $j < count($thrp_tid_list); $j++) { ?>
                                                                                                        <option value=<?php
                                                                                                        echo $thrp_tid_list[$j];
                                                                                                        if ($thrp_tid_list[$j] == $tid) {
                                                                                                            echo ' selected=""';
                                                                                                        } else {
                                                                                                            
                                                                                                        }
                                                                                                        ?>> <?php echo $thrp_name_list[$j]; ?></option>
                                                                                                            <?php }
                                                                                                            ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="text-align:center;margin-top:9%;">
                                                                                        <input type="submit"  name="confirm-assign-btn_<?php echo $k; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="Assign"  >
                                                                                        <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="Discard"/>
                                                                                    </div>
                                                                                </form>
                                                                                <?php
                                                                                #on click of 'yes' confirm-call-btn 
                                                                                if (isset($_REQUEST["confirm-assign-btn_$k"])) {

                                                                                    $tid_scheduled = $_REQUEST['tid_schedule'];
                                                                                    //include files
                                                                                    include 'get_cid.php';

                                                                                    $cid = get_cid($caller_emp_id[$k], $caller_corp_id[$k], $tid); //to fetch the cid
                                                                                    //insert into call_schedule
                                                                                    $stmt10 = $dbh->prepare("INSERT INTO call_schedule_call VALUES (?,?,?,?,?,?,?,?,?)");
                                                                                    $stmt10->execute(array('', $cid, $tid_scheduled, $caller_prefered_date[$k], $caller_prefered_time[$k], '0', '0', '', ''))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ASSIGN-CALL-1001].");

                                                                                    //update the assigned value as 1 and cid value
                                                                                    $stmt20 = $dbh->prepare("UPDATE call_schedule_queue SET assigned= ?, cid= ? WHERE sr_no = ? LIMIT 1");
                                                                                    $stmt20->execute(array('1', $cid, $sr_no[$k]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ASSIGN-CALL-1002].");

                                                                                    # to display sucess meaagage in the modal
                                                                                    echo '<script src="../assets/js/jquery.min.js"></script>
                                                                                        <script type="text/javascript">
                                                                                           $(document).ready(function(){
                                                                                              $("#err_assign_msg_' . $k . '").addClass("alert");
                                                                                              $("#err_assign_msg_' . $k . '").addClass("alert-success");
                                                                                              $("#err_assign_msg_' . $k . '").html("Caller has been assigned.");
                                                                                              $("#form_assign_' . $k . '").hide();
                                                                                              $("#assign_to_me_' . $k . '").modal({"backdrop": "static"});
                                                                                          });
                                                                                       </script>';
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                echo '</td>';
                                                                echo '</tr>';
                                                            } else {
                                                                //fetch the therapist name assigned
                                                                $stmt30 = $dbh->prepare("SELECT tid FROM call_schedule_call WHERE call_done = ? AND cid = ? AND prefered_date = ? AND prefered_time = ? LIMIT 1");
                                                                $stmt30->execute(array('0', $caller_cid[$k], $caller_prefered_date[$k], $caller_prefered_time[$k]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [THRP-ASSIGN-CALL-1003].");
                                                                if ($stmt30->rowcount() != 0) {
                                                                    $row30 = $stmt30->fetch();
                                                                    $assigned_tid[$k] = $row30['tid'];
                                                                    $seq_no = $seq_no + 1;
                                                                    echo '<tr>';
                                                                    echo '<td>' . $seq_no . '</td>';
                                                                    echo '<td><a href=""><div data-toggle="tooltip" data-placement="bottom" data-html="true" class="red-tooltip" title=""
                                                                                data-original-title="Employee Id- ' . $caller_emp_id[$k] . '<br>
                                                                                ' . $caller_email[$k] . '<br>
                                                                                ' . $caller_mobile[$k] . '<br>
                                                                                ' . $caller_location[$k] . '
                                                                                ">' . $caller_name[$k] . '</div></a></td>';
                                                                    echo '<td>' . get_corp_name($caller_corp_id[$k]) . '</td>';
                                                                    echo '<td>' . $caller_age[$k] . ' yrs / ' . $caller_gender[$k] . '</td>';
                                                                    echo '<td>' . date('d-M-Y', strtotime($caller_prefered_date[$k])) . '</td>';
                                                                    echo '<td>' . $caller_prefered_time[$k] . '</td>';
                                                                    echo '<td>' . $caller_suggested[$k] . '</td>';
                                                                    echo '<td>' . get_coach_name($assigned_tid[$k]) . '</td>';
                                                                    echo '</tr>';
                                                                }
                                                            }
                                                        } //ending for loop
                                                    } else {
                                                        echo "No Calls to Assign.";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <br/><br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                var resizefunc = [];
            </script>
            <script src="../assets/js/jquery.min.js"></script>
            <script src="../assets/js/bootstrap.min.js"></script>
            <script src="../assets/js/detect.js"></script>
            <script src="../assets/js/jquery.slimscroll.js"></script>
            <script src="../assets/js/waves.js"></script>
            <script src="../assets/js/jquery.nicescroll.js"></script>
            <script src="../assets/js/jquery.app.js"></script>
            <script src="../assets/js/jquery.core.js"></script>
            <script src="../assets/js/fastclick.js"></script>


            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
            <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatable').dataTable();
                    var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                });
            </script>
            <script>
                function reload() {
                    window.location.href = "<?php echo $host; ?>/ewap/assign_calls.php";
                }
            </script>
    </body>
</html>