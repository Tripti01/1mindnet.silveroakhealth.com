<?php
# Including functions and other files
include '../if_loggedin.php';
include 'host.php';
include 'ewap-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_name.php';
#getting tid from session
$tid = $_SESSION['tid'];

//to fetch current year
$current_year = date("Y");

# Start the database realated stuff
$dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
$dbh->query("use scodd");

#to fetch all the calls and caller details from call_schedule_call and call_callers_profile assigned to tid orderd by active(status)
$i = 0; //counter for total number of callers scheduled
$stmt00 = $dbh->prepare("SELECT name, yob, gender, email, mobile, call_callers_profile.cid AS cid, location, corp_id, emp_id, call_callers_profile.active AS active  FROM call_cid_tid_list, call_callers_profile WHERE tid=? AND call_cid_tid_list.cid = call_callers_profile.cid AND call_cid_tid_list.active = '1'");
$stmt00->execute(array($tid))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [MY-CALLER-1000].");
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $encrypt_name[$i] = $row00['name']; //to fetch encrypted name
        $name[$i] = decrypt($encrypt_name[$i], $encryption_key); //to fetch decrypted name 
        $yob[$i] = $row00['yob']; //to fetch yob
        $age[$i] = $current_year - $yob[$i]; //to fecth age
        if ($row00['gender'] == "M") {//if gender is M, then proceed
            $gender[$i] = "Male"; //set as masle
        } else if ($row00['gender'] == "F") {//if gender is F, then proceed
            $gender[$i] = "Female"; //set as female
        } else if ($row00['gender'] == "O") {//if gender is O, then proceed
            $gender[$i] = "Other"; //set as other
        } else if ($row00['gender'] == "W") {//if gender is W, then proceed
            $gender[$i] = "Would rather not say"; //set as would rather not say
        }
        $email_list[$i] = $row00['email']; //to fetch email
        $mobile[$i] = $row00['mobile']; //to fetch mobile
        $cid[$i] = $row00['cid']; //to fetch cid
        $location[$i] = $row00['location']; //to fetch location
        $corp_id[$i] = $row00['corp_id']; //to fetch corp_id
        $emp_ids[$i] = $row00['emp_id']; //to fetch emp_id
        $active[$i] = $row00['active']; //to fetch active or closed details
        //to fetch the number of calls taken place for the caller by the coach
        $stmt10 = $dbh->prepare("SELECT  taken_at FROM call_callers_notes WHERE cid = ? ORDER BY taken_at DESC");
        $stmt10->execute(array($cid[$i]))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [MY-CALLER-1001].");
        if ($stmt10->rowCount() != 0) {//if prev calls have taken place,proceed
            $row10 = $stmt10->fetch();
            $call_datetime[$i] = date('d-M-Y h:i A', strtotime($row10['taken_at'])); //to fetch last sesson date time
        } else {//else set as below
            $call_num[$i] = 0; //set as 0
            $call_datetime[$i] = " "; //set as blank
        }
        $i++;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>My EWAP Clients</title>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/default.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <style>
            .tooltip fade bottom in{
                left:0px;
            }
            .dataTables_length{
                margin-bottom: 2.5%;
            }
            .dataTables_filter{
                margin-left: 52%;
                margin-bottom: 2.5%;
            }
            .dataTables_info{
                display: none;
            }
            td{
                padding-left: 0px;
                padding-right: 0px;
            }
        </style>

    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include '../top_navbar.php'; ?>

            <div class="content-page">
                <!-- Start content -->
                <div class="content">					
					<a class="btn btn-primary" style="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin:10px;" 
								href="new_client.php" target="_blank">Create New Client</a>
								
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="card-box">
                                    <div class='table-scrollable table-scrollable-borderless'>
                                        <table id='datatable' class='table table-striped table-bordered'>
                                            <thead>
                                                <tr>
                                                    <th style="margin-left:2px;padding-right: 0px;"></th>
                                                    <th style="margin-left:2px;padding-right: 0px;">Name</th>
                                                    <th style="margin-left:2px;padding-right: 0px;">Company</th>
                                                    <th style="margin-left:2px;padding-right: 0px;">Mobile</th>
                                                    <th style="margin-left:2px;padding-right: 0px;">Email</th>
                                                    <th style="margin-left:2px;padding-right: 0px;">Age/Gender </th>
                                                    <th style="margin-left:2px;padding-right: 0px;">Last Call Time</th>
                                                    <th style="text-align:center;padding-left: 0px;padding-right: 0px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (isset($cid)) {
                                                    for ($i = 0; $i < count($cid); $i++) {

                                                        $seq_num = $i + 1;

                                                        echo '<tr>';
                                                        echo '<td style="padding-right: 0px;">' . $seq_num . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $name[$i] . ' </td>';
                                                        echo '<td style="padding-right: 0px;">' . get_corp_name($corp_id[$i]) . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $mobile[$i] . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $email_list[$i] . '</td>';
                                                        echo '<td style="padding-right: 0px;">' . $age[$i] . ' yrs / ' . $gender[$i] . ' </td>';
                                                        echo '<td style="padding-right: 0px;">' . $call_datetime[$i] . '</td>';
                                                        echo '<td style="text-align:center;padding-right: 0px;"><a href="caller_details.php?cid=' . $cid[$i] . '"><button type="button" class="btn btn-primary btn-sm waves-effect waves-light" style="margin-right:1%;">View</button></a>';
                                                        ?>

                                                        <?php
                                                    }
                                                } else {
                                                    echo ""; //dont do anything
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
        </div>



        <script>
            var resizefunc = [];
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
        <script src="../assets/js/jquery.core.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiQbVEMemZgEE5gdyI50TxyIUK0Ba9PBI&libraries=places&autocomplete=true"></script>
        <script src="../assets2/global/scripts/jquery.geocomplete.min.js"></script>
        <script>
            $(function () {
                $("#location").geocomplete({
                    details: ".geo-details",
                    detailsAttribute: "data-geo"
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable(
                        {
                            fixedHeader: true
                        });
            });
            $("document").ready(function () {
                setTimeout(function () {
                    $("#da").trigger('click');
                }, 10);

                setTimeout(function () {
                    $("#da").trigger('click');
                }, 10);
            });
        </script>



    </body>
</html>