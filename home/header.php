<?php
//include "../if_loggedin.php";
?>

<header class="navbar navbar-header navbar-header-fixed">
    <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
    <div class="navbar-brand">
        <a href="index.php" class="df-logo"><img src="https://cdn.silveroakhealth.com/images/SOH_logo.png" height="40px" width="50px"></a>
    </div><!-- navbar-brand -->
    <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
            <a href="index.php" class="df-logo"><img src="https://cdn.silveroakhealth.com/images/SOH_logo.png" height="40px" width="50px"> </a>
            <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu">
            <li class="nav-label pd-l-20 pd-lg-l-25 d-lg-none">Main Navigation</li>
            <li class="nav-item" id="hd1">
                <a href="index.php" class="nav-link"><i data-feather="pie-chart"></i>CORPORATE INFORMATION SYSTEM</a>
            </li>



        </ul>
    </div><!-- navbar-menu-wrapper -->
    <div class="navbar-right">



        <div class="dropdown dropdown-profile">
            <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
                <div class=""><?php echo "Abhijit" ?></div>
            </a><!-- dropdown-link -->
            <div class="dropdown-menu dropdown-menu-right tx-13">

                 <div class="dropdown-divider"></div>

                <a href="../logout" class="dropdown-item"><i data-feather="log-out"></i>Sign Out</a>
            </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
    </div><!-- navbar-right -->

</header><!-- navbar -->
