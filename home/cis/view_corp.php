<?php
include 'soh-config.php';

# Check if there is response in the URL

if (isset($_REQUEST['corp_id']) && !empty($_REQUEST['corp_id'])) {
    $corp_id = $_REQUEST['corp_id'];

    ##################### FETCH CORP NAME, ALT NAMES, ACTIVE ####################
    $stmt100 = $dbh->prepare("SELECT * FROM corp_master where corp_id=? LIMIT 1");
    $stmt100->execute(array($corp_id));
    if ($stmt100->rowCount() != 0) {
        $row100 = $stmt100->fetch();
        $corp_name = $row100['corp_name'];
        $alt_names = $row100['alt_names'];
        $active = $row100['active'];
    } else {
        die("Some error occured. Error Code : FETCH_CORP_MASTER");
    }
    
    
	##################### FETCH Registered Office Address, About, Industry type ####################
    $stmt101 = $dbh->prepare("SELECT * FROM corp_profile where corp_id=? LIMIT 1");
    $stmt101->execute(array($corp_id));
    if ($stmt101->rowCount() != 0) {
        $row101 = $stmt101->fetch();
        $about = $row101['about'];
        $regd_office = $row101['regd_office'];
        $industry_type = $row101['industry_type'];
    } else {
        die("Some error occured. Error Code : FETCH_CORP_PROFILE");
    }
	
    ##################### FETCH Account Manager ####################
    $stmt200 = $dbh->prepare("SELECT * FROM corp_contract where corp_id=? LIMIT 1");
    $stmt200->execute(array($corp_id));
    if ($stmt200->rowCount() != 0) {
        $row200 = $stmt200->fetch();
		$payment_type = $row200['payment_type'];
		$no_of_emp = $row200['no_of_emp'];
        $account_manager = $row200['account_manager'];

        if( $row200['contract_start_date']<1){
            $contract_start_date = '-';
        }else{
            $contract_start_date = $row200['contract_start_date'];
        }

        if( $row200['contract_end_date']<1){
            $contract_end_date = '-';
        }else{
            $contract_end_date = $row200['contract_end_date'];
        }

        if( $row200['launch_date']<1){
            $launch_date = '-';
        }else{
            $launch_date = $row200['launch_date'];
        }

        if( $row200['contract_duration']<1){
            $contract_duration = '-';
        }else{
            $contract_duration = $row200['contract_duration'];
        }

        if( $row200['notice_period']<1){
            $notice_period = '-';
        }else{
            $notice_period = $row200['notice_period'];
        }
		
		

    } else {
        die("Some error occured. Error Code : FETCH_CORP_CONTRACT");
    }

    
    ##################### FETCH SERVICE PLAN ####################
    $stmt300 = $dbh->prepare("SELECT * FROM corp_service_plan where corp_id=? LIMIT 1");
    $stmt300->execute(array($corp_id));
    if ($stmt300->rowCount() != 0) {
        $row300 = $stmt300->fetch();
        $service_plan = $row300['service_plan'];
    } else {
        die("Some error occured. Error Code : FETCH_CORP_CONTRACT");
    }

    ##################### FETCH ALL SERVICE PLANS ####################
    $stmt301 = $dbh->prepare("SELECT * FROM service_plan_offerings");
    $stmt301->execute(array());
    $sp=0;
    if ($stmt301->rowCount() != 0) {
        while ($row301 = $stmt301->fetch(PDO::FETCH_ASSOC)) {
        
            $service_plans[$sp] = $row301['service_plan'];
            $sp++;
        }
    } else {
        die("Some error occured. Error Code : FETCH_CORP_CONTRACT");
    }

    ##################### FETCH CONTACT DETAILS ####################
	$existing_contacts = 0;
    $m = 0;
    $stmt400 = $dbh->prepare("SELECT * FROM corp_contact where corp_id=?");
    $stmt400->execute(array($corp_id));
    if ($stmt400->rowCount() != 0) {
		while ($row400 = $stmt400->fetch(PDO::FETCH_ASSOC)) {
			
			$contact_name[$m] = $row400['name'];
			$contact_type[$m] = $row400['contact_type'];
			$contact_phone[$m] = $row400['phone'];
			$contact_email[$m] = $row400['email'];
			$contact_sr_no[$m] = $row400['sr_no'];
			$m++;
			}
		$existing_contacts = 1;
    }
    
	
    ##################### FETCH DOMAIN ####################
	$existing_domains = 0;
    $n = 0;
    $stmt500 = $dbh->prepare("SELECT * FROM corp_domain where corp_id=?");
    $stmt500->execute(array($corp_id));
    if ($stmt500->rowCount() != 0) {
        while ($row500 = $stmt500->fetch(PDO::FETCH_ASSOC)) {
            $domain_list[$n] = $row500['domain']; //to fetch domain name.
            $domain_sr_no[$n] = $row500['sr_no'];
            // echo $domain_list[$n];
            $n++;
        }
		$existing_domains = 1;
    }

    $industry_type_name = '';
    ##################### FETCH ALL INDUSTRY TYPE ####################
    $stmt600 = $dbh->prepare("SELECT * FROM corp_industry_type");
    $stmt600->execute(array());
    $cit=0;
    if($stmt600->rowCount()!=0){
        while ($row600 = $stmt600->fetch(PDO::FETCH_ASSOC)) {
            
            $corp_industry_id[$cit] = $row600['industry_type__id'];
            $corp_industry_name[$cit] = $row600['industry_type_name'];
            
            if($industry_type == $corp_industry_id[$cit]){
                $industry_type_name = $corp_industry_name[$cit];
            }
            
            $cit++;
        }
    }


	
} 

else {
    ## This else if for if the corp id exists in the URL
    header('Location:index.php');
    exit();
}




?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

        <title>Corporate Information System</title>

        <!-- vendor css -->
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
    </head>
    <body>

        <?php include '../header.php'; ?>

        <div class="content pd-0 mg-t-50">
            <div class="content-body">
                <div class="container pd-x-0">
                    
                    <?php
                    if (isset($_REQUEST['result']) && !empty($_REQUEST['result'])) {
                        $result = $_REQUEST['result'];

                        if ($result == "SUCCESS") {
                            echo '<span class="badge badge-success">Changes Updated </span>';
                        } else if ($result == "FAILURE") {
                            echo '<span class="badge badge-danger"> Some error occured.  </span>';
                        }
                    }
                        ?>
                    
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                          
                    
                        <div>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-style1 mg-b-10">

                                    <li class="breadcrumb-item "><a href="index.php"><i data-feather="arrow-left" width="10%"></i> All Corporates</a></li>

                                </ol>
                            </nav>
                            <h4 class="mg-b-0 tx-spacing--1"> <?php echo $corp_name; ?> </h4>
                        </div>
                        <div class="d-none d-md-block">
                            <div  align="right">
                                <button href="#modal101" data-toggle="modal" class="btn btn-sm btn-outline-primary" id="customSwitch1"> Activate</button>
                                
                            </div>     
                        </div>
                    </div>

                    <div data-label="" class="df-example">
                        <ul class="nav nav-tabs nav-justified" id="myTab3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="profile-tab3" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="domain-tab3" data-toggle="tab" href="#domain2" role="tab" aria-controls="domain" aria-selected="false">Domain</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="service-tab3" data-toggle="tab" href="#service3" role="tab" aria-controls="service" aria-selected="false">Services and Contract</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab3" data-toggle="tab" href="#contact5" role="tab" aria-controls="contact" aria-selected="false">Contact Details</a>
                            </li>
                        </ul>
                        <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent3">
                            <div class="tab-pane fade show active" id="profile1" role="tabpanel" aria-labelledby="profilee-tab3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" width="30%;">Corporate Id</th>
                                                            <td><?php echo $corp_id; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mg-t-20">
                                        <div class="card">
                                            <div class="card-header d-sm-flex align-items-start justify-content-between">
                                                <h6>Corporate Name</h6>
                                                <a href="#modal1" class="button tx-12" data-toggle="modal"><i class='fas fa-edit'></i></a>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" width="30%;">Corporate Name</th>
                                                            <td><?php echo $corp_name; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Brand/Alternate Names</th>
                                                            <td><?php echo $alt_names; ?></td>
                                                        </tr>
                                                        <tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mg-t-20">
                                        <div class="card">
                                            <div class="card-header d-sm-flex align-items-start justify-content-between">
                                                <h6>Details</h6>
                                                <a href="#modal2" class="button tx-12" data-toggle="modal"><i class='fas fa-edit'></i></a>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                    <th scope="row" width="30%;">Registered Office Address</th>
                                                    <td><?php echo $regd_office; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">About</th>
                                                        <td><?php echo $about; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Industry type</th>
                                                        <td><?php echo $industry_type_name; ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="domain2" role="tabpanel" aria-labelledby="domain-tab3">
                                <div class="row">
                                    <div class="col-md-12 mg-t-20">
                                        <div class="card">
                                            <div class="card-header d-sm-flex align-items-start justify-content-between">
                                                <h6>Domains List</h6>
                                                <a href="#modal03" class="button tx-12" data-toggle="modal"><i class='fas fa-plus'></i></a>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>


                                                    <tr>
                                                        <th scope="row" width="30%;">Domain Name(s)</th>    
                                                            <?php
                                                                for ($g = 0; $g < $stmt500->rowCount(); $g++) {
                                                                    if (isset($domain_list[$g]) && $domain_list[$g] !== '') {
                                                            ?>
                                                                        <td><?php echo $domain_list[$g]; ?><a href="" class="button tx-12" data-toggle="modal" data-target="#editdomain_<?php echo $g; ?>">
                                                                        
                                                                        <i class='fas fa-edit'></i></a></td>
                                                                        
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                        
                                                     </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="service3" role="tabpanel" aria-labelledby="service-tab3">
                            <div class="row">
                                    <div class="col-md-12 mg-t-20">
                                        <div class="card">
                                            <div class="card-header d-sm-flex align-items-start justify-content-between">
                                                <h6>Service Details</h6>
                                                <a href="#modal5" class="button tx-12" data-toggle="modal"><i class='fas fa-edit'></i></a>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" width="30%;">Service Plan</th>
                                                            <td><?php echo $service_plan; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">SOH Account Manager</th>
                                                            <td><?php echo $account_manager; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Payment type</th>
                                                            <td><?php echo $payment_type; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">No. of Employees</th>
                                                            <td><?php echo $no_of_emp; ?></td>
                                                        </tr>
                                                        <!--<tr>
                                                        <th scope="row" width="30%;">No. of Family Members Covered</th>
                                                        <td></td>
                                                        </tr>
                                                        <tr>
                                                        <th scope="row" width="30%;">Location(s)</th>
                                                        <td></td>
                                                        </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mg-t-20">
                                        <div class="card">
                                            <div class="card-header d-sm-flex align-items-start justify-content-between">
                                                <h6>Contract Details</h6>
                                                <a href="#modal4" class="button tx-12" data-toggle="modal"><i class='fas fa-edit'></i></a>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" width="30%;">Contract Start Date</th>
                                                            <td><?php echo $contract_start_date; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Contract Duration</th>
                                                            <td><?php echo $contract_duration; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Contract End Date</th>
                                                            <td><?php echo $contract_end_date; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Launch Date</th>
                                                            <td><?php echo $launch_date; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Notice Period</th>
                                                            <td><?php echo $notice_period; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="tab-pane fade" id="contact5" role="tabpanel" aria-labelledby="cont-tab3">
                                <div class="row">
                                    <div class="col-md-12 mg-t-20">
                                        <div class="card">
                                            <div class="card-header d-sm-flex align-items-start justify-content-between">
                                                <h6>Contact Details</h6>
                                                <a href="#modal7" class="button tx-12" data-toggle="modal"><i class='fas fa-plus'></i></a>
                                            </div>
											<?php
                                                                for ($g = 0; $g < $stmt400->rowCount(); $g++) {
                                                                    if (isset($contact_name[$g]) && $contact_name[$g] !== '') {
                                            ?>
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tbody>
														<tr>
                                                            <th scope="row" width="30%;">Contact Type</th>
                                                            <td><?php echo $contact_type[$g]; ?>
																<a href="" class="button tx-12" data-toggle="modal" data-target="#editcontact_<?php echo $g; ?>">     
                                                                <i class='fas fa-edit'	></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Contact Name</th>
                                                            <td><?php echo $contact_name[$g]; ?>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Contact Phone</th>
                                                            <td><?php echo $contact_phone[$g]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" width="30%;">Contact Email</th>
                                                            <td><?php echo $contact_email[$g]; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
											<?php
                                                                    }
                                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- df-example -->

            </div><!-- container -->
        </div><!-- content -->
        <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="update_corp.php" method="GET" id="form_domain">
                    <div class="modal-content tx-14">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Corporate Name</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label">Corporate Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="new_corp_name" value="<?php echo $corp_name; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label">Brand/Alternate Names<br><span style="font-size: 12px;">(Please enter all alternate/brand names separated by comma. eg.: SOH, Resilience)</span></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="alt_name" value="<?php echo $alt_names; ?>">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                               <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                               <button type="submit" name="btn_edit_corp_name" value="1" class="btn btn-primary tx-13">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
        for ($l = 0; $l < $stmt500->rowCount(); $l++) {
            if (isset($domain_list[$l]) && $domain_list[$l] !== '') {
                ?>                                                       
        <div class="modal fade" id="editdomain_<?php echo $l; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="domain">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Domain</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Domain Name(s)</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Domain Name(s)" name="new_domain" value="<?php echo $domain_list[$l]; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-between">
                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                            <input type="hidden" name="sr_no" value="<?php echo $domain_sr_no[$l]; ?>"/>
                            <button type="submit" class="btn btn-danger tx-13" value="submit" name="btn_delete_domain">Delete</button>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_edit_domain">Modify</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
            }
        }
        ?>
		
		<?php
        for ($l = 0; $l < $stmt400->rowCount(); $l++) {
            if (isset($contact_name[$l]) && $contact_name[$l] !== '') {
                ?>                                                       
        <div class="modal fade" id="editcontact_<?php echo $l; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="contacts">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Contact</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
							<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="new_contact_type" placeholder="" name="new_contact_type" value="<?php echo $contact_type[$l]; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="new_contact" placeholder="" name="new_contact" value="<?php echo $contact_name[$l]; ?>">
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Phone</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="new_contact_phone" placeholder="" name="new_contact_phone" value="<?php echo $contact_phone[$l]; ?>">
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Contact Email</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="new_contact_email" placeholder="" name="new_contact_email" value="<?php echo $contact_email[$l]; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-between">
                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                            <input type="hidden" name="sr_no" value="<?php echo $contact_sr_no[$l]; ?>"/>
                            <button type="submit" class="btn btn-danger tx-13" value="submit" name="btn_delete_contact">Delete</button>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_edit_contact">Modify</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
            }
        }
        ?>

        <div class="modal fade" id="modal03" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="domain">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Add Domain</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Domain Name(s)</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="domain" placeholder="Domain Name(s)" name="add_domain" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
							<input type="hidden" name="sr_no" value="<?php echo $domain_sr_no; ?>"/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_add_domain">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="modal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="contacts">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Add Contact</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
							<div class="form-group row">
                                <label for="inputEmail4" class="col-sm-5 col-form-label">Contact Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact_type" placeholder="Contact Type" name="add_contact_type" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail4" class="col-sm-5 col-form-label">Contact Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact_name" placeholder="Contact Name" name="add_contact_name" value="">
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="inputEmail4" class="col-sm-5 col-form-label">Contact Phone</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact_phone" placeholder="Contact Phone" name="add_contact_phone" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail4" class="col-sm-5 col-form-label">Contact Email</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="contact_email" placeholder="Contact Email" name="add_contact_email" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
							<input type="hidden" name="sr_no" value="<?php echo $contact_sr_no; ?>"/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_add_contact_name">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
				<form action="update_corp.php" method="GET" id="form_domain">
					<div class="modal-content tx-14">
						<div class="modal-header">
							<h6 class="modal-title" id="exampleModalLabel">Edit Details</h6>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-5 col-form-label">Registered Office Address</label>
									<div class="col-sm-7">
                                    <textarea class="form-control" rows="3" style="resize: none;"  name="new_corp_address" ><?php echo $regd_office; ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-5 col-form-label">About</label>
									<div class="col-sm-7">
                                    <textarea class="form-control" rows="3" style="resize: none;" name="new_about" ><?php echo $about; ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-5 col-form-label">Industry type</label>
									<div class="col-sm-7">
                                    <select name="new_industry_type" class="custom-select">
                                        <option value="-1" disabled="">Select Industry Type </option>
                                        
                                        <?php 
                                            for($cit=0;$cit<count($corp_industry_id);$cit++)
                                            {
                                                if($corp_industry_id[$cit] == $industry_type){?>
                                                    <option selected value="<?php echo $corp_industry_id[$cit]; ?>" ><?php echo $corp_industry_name[$cit]; ?></option>
                                                <?php 
                                                }else{
                                                ?>

                                                    <option value="<?php echo $corp_industry_id[$cit]; ?>"><?php echo $corp_industry_name[$cit]; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </select>

										
									</div>
								</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                            <button type="submit" name="btn_edit_corp_details" value="1" class="btn btn-primary tx-13">Save</button>
						</div>
					</div>
				</form>
            </div>
        </div>

        <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
				<form action="update_corp.php" method="GET" id="form_domain">
					<div class="modal-content tx-14">
						<div class="modal-header">
							<h6 class="modal-title" id="exampleModalLabel">Edit Contract Details</h6>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-5 col-form-label">Contract Start Date</label>
									<div class="col-sm-7">
										<input type="date" class="form-control" name="new_contract_start_date" value="<?php echo $contract_start_date; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-5 col-form-label">Contract Duration</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="new_contract_duration" value="<?php echo $contract_duration; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-5 col-form-label">Contract End Date</label>
									<div class="col-sm-7">
										<input type="date" class="form-control" name="new_contract_end_date" value="<?php echo $contract_end_date; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-5 col-form-label">Launch Date</label>
									<div class="col-sm-7">
										<input type="date" class="form-control" name="new_contract_launch_date" value="<?php echo $launch_date; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-5 col-form-label">Notice Period</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="new_contract_notice_period" value="<?php echo $notice_period; ?>">
									</div>
								</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                            <button type="submit" name="btn_edit_contract01" value="1" class="btn btn-primary tx-13">Save</button>
						</div>
					</div>
				</form>
            </div>
        </div>

        <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="post" id="service">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Service Details</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Service Plan</label>
                                <div class="col-sm-7">
                                    <select name="new_service_plan" id="service" type="text" class="form-control" list="service-plan" >
                                        <option value="-1" disabled="">Select Plan</option>
                                        
                                        <?php
                                            for($sp=0;$sp<count($service_plans);$sp++){
                                                if($service_plan == $service_plans[$sp]){?>
                                                    <option selected value="<?php echo $service_plans[$sp]; ?>"><?php echo $service_plans[$sp]; ?></option>
                                            <?php    }else{       
                                        ?>
                                            <option value="<?php echo $service_plans[$sp]; ?>"><?php echo $service_plans[$sp]; ?></option>

                                        <?php  
                                           }
                                         } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label">SOH Account Manager</label>
                                <div class="col-sm-7">
                                    <select type="text" name="new_account_manager" class="form-control" placeholder="SOH Account Manager">
                                        <option value="-1" disabled="">Select Account Manager</option>
                                        <option value="Pallavi" <?php if ($account_manager=="Pallavi") echo "Selected"; ?>>Pallavi </option>
                                        <option value="Gayatri" <?php if ($account_manager=="Gayatri") echo "Selected"; ?>>Gayatri </option>
                                        <option value="Nalini" <?php if ($account_manager=="Nalini") echo "Selected"; ?>>Nalini </option>
                                        <option value="Divya Abraham" <?php if ($account_manager=="Divya Abraham") echo "Selected"; ?>>Divya Abraham </option>
                                        <option value="Saravanan" <?php if ($account_manager=="Saravanan") echo "Selected"; ?>>Saravanan </option>
                                        <option value="Sushil" <?php if ($account_manager=="Sushil") echo "Selected"; ?>>Sushil </option>
                                    </select>
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Payment Type</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="new_payment_type" value="<?php echo $payment_type; ?>">
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">No. of Employees</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="new_no_of_emp" value="<?php echo $no_of_emp; ?>">
                                </div>
                            </div>
							<!--<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">No. of family members covered</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="no_of_family_members" placeholder="No. of family members covered" name="no_of_family_members" value="">
                                </div>
                            </div>
							<div class="form-group row">
                                <label for="inputEmail3" class="col-sm-5 col-form-label">Location(s)<br><span style="font-size: 12px;">(Please enter multiple locations separated by comma.)</span></label>
                                <div class="col-sm-7">
                                <textarea class="form-control" rows="3" style="resize: none;" id="locations" placeholder="Location(s)" name="locations"></textarea>
                                </div>
                            </div>-->
                        </div>
                        <div class="modal-footer">
                        
                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                            <button type="submit" class="btn btn-primary tx-13" value="submit" name="btn_edit_contract"> Modify </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="modal fade" id="modal101" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14">
                    <form action="update_corp.php" method="POST" id="activation">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Details</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-5 col-form-label">Corporate Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="inputEmail3" placeholder="Corporate Name" name="corp_name" value="<?php echo $corp_name; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-5 col-form-label">Service Plan</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="inputEmail3" placeholder="Service Plan" name="service_plan" value="<?php echo $service_plan;?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-5 col-form-label">Domain</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="inputPassword3" placeholder="Domain" name="domain" value="<?php echo $domain_list[0]; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-5 col-form-label">SOH Account Manager</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="inputPassword3" placeholder="SOH Account Manager" name="account_manager" value="" required>
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
							<input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                            <button type="submit" name="activate_btn" value="1" class="btn btn-primary tx-13">Activate Account</button>
                        </div>
                    </form>
                </div>
            </div><!-- container -->
        </div>

    </div>

    <script type="text/javascript">
        function formValidate()
        {
            var val = document.frmDomin;
            if (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value) || /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value)) {
            } else
            {
                $("#err_name").html("Invalid Domain");
                val.domain.focus();
                return false;
            }
        }
    </script>

    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
    <script src="../../assets/js/dashforge.js"></script>
    <script src="../../assets/js/dashforge.aside.js"></script>
</body>
</html>
