<?php
include 'soh-config.php';
$i = 0;
# Retrieving corporate details
$stmt01 = $dbh->prepare("SELECT * FROM corp_master WHERE 1");
$stmt01->execute(array());
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row01['corp_id'];
        $corp_name[$i] = $row01['corp_name'];
        $corp_active_status[$i] = $row01['active'];
        
        if($corp_active_status[$i]==1)
        { $corp_active_status_ui_label[$i] = '<span class="badge badge-success">Active</span>'; }
       else if($corp_active_status[$i]==0)
             { $corp_active_status_ui_label[$i] = '<span class="badge badge-danger">Non Active </span>'; }
        $i++;
    }
} else {
    
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">
        <title>Corporate Information System</title>
        <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/dashforge.css">
        <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
        <style>
            .daterangepicker .ranges li.active {
                background-color: #fff; 
                color: #000; 
            }
        </style>
        <style>
            table.dataTable thead .sorting::after {
                display:none;
            }
            table.dataTable thead .sorting::before {
                display:none;
            }
            table.dataTable thead .sorting_asc::after {
                display:none;
            }
            table.dataTable thead .sorting_desc::after {
                display:none;
            }
        </style>
    </head>
    <body>

<?php include '../header.php'; ?>
        <div class="content pd-0">
            <div class="content-body">
                <div class="container pd-x-0">
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                        <div>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                                    <li class="breadcrumb-item active" style="font-size:15px;" aria-current="page">Corporate Information System</li>
                                </ol>
                            </nav>
                           
                        </div>
                      
                    </div>
                    
                    <div class="row row-xs mg-t-10">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class=" card-body">
                                <table id="tb1" class="table table-striped mg-b-0">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Corp Id</th>
                                            <th>Name</th> 
                                            <th>Active Status</th>                                       
                                            <th>View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (count($corp_id) >= 1) {
                                            for ($j = 0; $j < count($corp_id); $j++) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $corp_id[$j]; ?></td>
                                                    <td><?php echo $corp_name[$j]; ?></td>
                                                    <td><?php echo $corp_active_status_ui_label[$j]; ?></td>                                              
                                                    <td>
                                                        <a class="btn btn-xs btn-secondary"  href="view_corp.php?corp_id=<?php echo $corp_id[$j]; ?>">View & Edit</a>
                                                    </td>
                                                </tr>   
                                                <?php
                                            }
                                        }
                                        ?>								
                                    </tbody>   
                                </table>

                            </div>
                        </div>
                    </div>
                    </div>
   
                </div><!-- container -->
            </div><!-- content -->
        </div>
        <script src="../../assets/lib/jquery/jquery.min.js"></script>
        <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/lib/feather-icons/feather.min.js"></script>
        <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
        <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
        <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
        <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
        <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
        <script src="../../assets/js/dashforge.js"></script>
        <script src="../../assets/js/dashforge.aside.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#tb1').DataTable({
                    dom: 'Bfrtip',
                    "bSort": false,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'Data export'
                        }
                    ]
                });
            });
        </script>   
    </body>
</html>
