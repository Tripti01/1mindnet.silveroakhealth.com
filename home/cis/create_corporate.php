<?php

include 'soh-config.php';

$corp_id = "CORP3";
$corp_name = "Mercedes";

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh->beginTransaction();
try {

    $stmt01 = $dbh->prepare("INSERT INTO corp_master VALUES(?,?,?,?,?,?,?,?,?);");
    $stmt01->execute(array($corp_id, $corp_name, '',0,'','','','',''));

    $stmt02 = $dbh->prepare("INSERT INTO corp_details VALUES (?,?,?,?,?,?)");
    $stmt02->execute(array($corp_id, '', '', '', '',''));

    $stmt03 = $dbh->prepare("INSERT INTO corp_info VALUES (?,?,?,?,?,?)");
    $stmt03->execute(array($corp_id, '', '', '', '',''));

    $stmt04 = $dbh->prepare("INSERT INTO corp_notes VALUES (?,?,?,?,?,?,?,?)");
    $stmt04->execute(array($corp_id,'', '', '','','', '', ''));

    $stmt08 = $dbh->prepare("INSERT INTO corp_service_plan VALUES (?,?,?,?,?,?,?,?)");
    $stmt08->execute(array($corp_id,'','','','','','',''));

    $stmt08 = $dbh->prepare("INSERT INTO corp_contract VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $stmt08->execute(array($corp_id,'','','','','','','','','','','','','','',''));

    $stmt09 = $dbh->prepare("INSERT INTO corp_type VALUES (?,?,?,?,?,?,?,?)");
    $stmt09->execute(array($corp_id ,'','','' ,'','' ,'',''));

    $stmt11 = $dbh->prepare("INSERT INTO corp_profile VALUES (?,?,?,?,?,?,?,?,?,?)");
    $stmt11->execute(array($corp_id ,'','','','','','','','',''));

    $status_code = 2; //all done successfully
    $dbh->commit();
    
} catch (PDOException $e) {
    $dbh->rollBack();
    var_dump($dbh->errorInfo());
    die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code :CRET_CORP_2");
}
?>
