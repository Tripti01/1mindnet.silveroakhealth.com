<?php
include 'soh-config.php';

$corp_id = $_REQUEST['corp_id'];
$sr_no = $_REQUEST['sr_no'];

$success_redirect_link = "view_corp.php?corp_id=" . $corp_id . "&result=SUCCESS";
$failed_redirect_link = "view_corp.php?corp_id=" . $corp_id . "&result=FAILURE";

##################### For updating corp name and alt names ####################
if (isset($_REQUEST['btn_edit_corp_name']) && !empty($_REQUEST['btn_edit_corp_name'])) {
   $new_corp_name= $_REQUEST['new_corp_name'];
   $alt_name= $_REQUEST['alt_name'];
   
  $stmt100 = $dbh->prepare("UPDATE corp_master SET corp_name=?, alt_names=? WHERE corp_id=? LIMIT 1");
    $stmt100->execute(array($new_corp_name, $alt_name, $corp_id));
    if ($stmt100->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}

##################### For updating corp address and about ####################
if (isset($_REQUEST['btn_edit_corp_details']) && !empty($_REQUEST['btn_edit_corp_details'])) {
   $about= $_REQUEST['new_about'];
   $regd_office= $_REQUEST['new_corp_address'];
   $industry_type= $_REQUEST['new_industry_type'];
   
  $stmt101 = $dbh->prepare("UPDATE corp_profile SET about=?, regd_office=?, industry_type=? WHERE corp_id=? LIMIT 1");
    $stmt101->execute(array($about, $regd_office, $industry_type, $corp_id));
    if ($stmt101->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}

##################### For updating contract details ##########################
if (isset($_REQUEST['btn_edit_contract']) && !empty($_REQUEST['btn_edit_contract'])) {
   $new_service_plan= $_REQUEST['new_service_plan'];
   $new_payment_type= $_REQUEST['new_payment_type'];
   $new_no_of_emp= $_REQUEST['new_no_of_emp'];
   $new_account_manager= $_REQUEST['new_account_manager'];
    
  $stmt200 = $dbh->prepare("UPDATE corp_contract SET account_manager=?, payment_type=?, no_of_emp=? WHERE corp_id=? LIMIT 1");
  $stmt200->execute(array($new_account_manager, $new_payment_type, $new_no_of_emp, $corp_id));
  
  $stmt220 = $dbh->prepare("UPDATE corp_service_plan SET service_plan=? WHERE corp_id=? LIMIT 1");
  $stmt220->execute(array($new_service_plan, $corp_id));
   
 
  if ($stmt200->rowCount() or $stmt220->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}


if (isset($_REQUEST['btn_edit_contract01']) && !empty($_REQUEST['btn_edit_contract01'])) {
   $contract_start_date= $_REQUEST['new_contract_start_date'];
   $contract_end_date= $_REQUEST['new_contract_end_date'];
   $contract_duration= $_REQUEST['new_contract_duration'];
   $launch_date= $_REQUEST['new_contract_launch_date'];
   $notice_period= $_REQUEST['new_contract_notice_period'];
   
  $stmt230 = $dbh->prepare("UPDATE corp_contract SET contract_start_date=?, contract_end_date=?, contract_duration=?, launch_date=?, notice_period=? WHERE corp_id=? LIMIT 1");
    $stmt230->execute(array($contract_start_date, $contract_end_date, $contract_duration, $launch_date, $notice_period, $corp_id));
    if ($stmt230->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}

##################### For updating domains ####################
if (isset($_REQUEST['btn_edit_domain']) && !empty($_REQUEST['btn_edit_domain'])) {
   $new_domain = $_REQUEST['new_domain'];
   
    $stmt300 = $dbh->prepare("UPDATE corp_domain SET domain=? WHERE sr_no=? LIMIT 1");
    $stmt300->execute(array($new_domain, $sr_no));
    if ($stmt300->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}
##################### For deleting domains ####################
if (isset($_REQUEST['btn_delete_domain']) && !empty($_REQUEST['btn_delete_domain'])) {
   
    $stmt301 = $dbh->prepare("DELETE from corp_domain WHERE sr_no=? LIMIT 1");
    if ($stmt301->execute(array($sr_no))) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}

################### for adding domains ####################
if (isset($_REQUEST['btn_add_domain']) && !empty($_REQUEST['btn_add_domain'])) {
   $add_domain= $_REQUEST['add_domain'];
   
    $stmt400 = $dbh->prepare("INSERT into corp_domain(corp_id, domain ) values(?,?)");
    $stmt400->execute(array($corp_id, $add_domain));
    if ($stmt400->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}


################### for adding contacts ####################
if (isset($_REQUEST['btn_add_contact_name']) && !empty($_REQUEST['btn_add_contact_name'])) {
   $add_contact_name= $_REQUEST['add_contact_name'];
   $add_contact_type= $_REQUEST['add_contact_type'];
   $add_contact_phone= $_REQUEST['add_contact_phone'];
   $add_contact_email= $_REQUEST['add_contact_email'];
   
    $stmt500 = $dbh->prepare("INSERT into corp_contact(corp_id, contact_type, name, phone, email ) values(?,?,?,?,?)");
    $stmt500->execute(array($corp_id, $add_contact_type, $add_contact_name, $add_contact_phone, $add_contact_email));
    if ($stmt500->rowCount()) {
        header('Location:' . $success_redirect_link);
    } else {
        header('Location:' . $failed_redirect_link);
    }
}


##################### For updating contacts ####################
if (isset($_REQUEST['btn_edit_contact']) && !empty($_REQUEST['btn_edit_contact'])) {
    $contact_name = $_REQUEST['new_contact'];
    $contact_type = $_REQUEST['new_contact_type'];
    $contact_phone = $_REQUEST['new_contact_phone'];
    $contact_email = $_REQUEST['new_contact_email'];
	
     $stmt501 = $dbh->prepare("UPDATE corp_contact SET contact_type=?, name=?, phone=?, email=? WHERE sr_no=? LIMIT 1");
     $stmt501->execute(array($contact_type, $contact_name, $contact_phone, $contact_email, $sr_no));
     if ($stmt501->rowCount()) {
         header('Location:' . $success_redirect_link);
     } else {
         header('Location:' . $failed_redirect_link);
     }
 }
 ##################### For deleting contacts ####################
if (isset($_REQUEST['btn_delete_contact']) && !empty($_REQUEST['btn_delete_contact'])) {
     $stmt502 = $dbh->prepare("DELETE from corp_contact WHERE sr_no=? LIMIT 1");
     if ($stmt502->execute(array($sr_no))) {
         header('Location:' . $success_redirect_link);
     } else {
         header('Location:' . $failed_redirect_link);
     }
 }

##################### For ACCOUNT ACTIVATION ####################
if (isset($_REQUEST['activate_btn']) && !empty($_REQUEST['activate_btn'])) {
    
        $corp_id = $_REQUEST['corp_id'];
        $corp_name = $_REQUEST['corp_name'];
        $service_plan = $_REQUEST['service_plan'];
        $domain = $_REQUEST['domain'];
        $account_manager = $_REQUEST['account_manager'];
        $active = $_REQUEST['active'];
		
		if(isset($_REQUEST['corp_id']) && !empty($_REQUEST['corp_id'])){
        $stmt700 = $dbh->prepare("SELECT * from corp_master where corp_id=? LIMIT 1");
        $stmt700->execute(array($corp_id));
		
		$stmt701 = $dbh->prepare("SELECT * from corp_master where corp_name=? LIMIT 1");
        $stmt701->execute(array($corp_name));
		
		$stmt702 = $dbh->prepare("SELECT * from service_plan_offerings where service_plan=? LIMIT 1");
        $stmt702->execute(array($service_plan));
		
		
		
        if ($stmt700->rowCount() && $stmt701->rowCount() && $stmt702->rowCount()) {
             $stmt710 = $dbh->prepare("UPDATE corp_master SET active=1 WHERE corp_id=? LIMIT 1");
			 $stmt710->execute(array($corp_id));
			 if ($stmt710->rowCount()) {
				 header('Location:' . $success_redirect_link);
			 } else {
				 header('Location:' . $failed_redirect_link);
			 }
	 
        } else {
            header('Location:' . $failed_redirect_link);
        }
    }
}


?>

