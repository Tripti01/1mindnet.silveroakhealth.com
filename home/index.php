<?php
include 'if_loggedin.php';
$name = $_SESSION['name'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">
        <title>mindNET Home</title>
        <link href="../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/dashforge.css">
        <link rel="stylesheet" href="../assets/css/dashforge.dashboard.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    </head>
    <body>
         <?php include 'header.php'; ?>
        <div class="content pd-0">
            <div class="content-body">
                <div class="container pd-x-0 ">
                    <BR/>  <BR/>  <BR/> <BR/>
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                        <!-- <h1>Hello, <span style="color: #8EBE3F;"><?php echo $name; ?></span> </h1> !-->
                    </div>
                    <div class="row row-xs mg-t-10 p-5">
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="CASE_MGMT/">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-copy fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="CASE_MGMT/">Case Management</a></h5>
                                        <p style="text-align: center;">Comprehensive & Secure therapy notes management platform.</p>
                                    </div>
                                </div>
                            </div><!-- card -->
                        </div> <!-- column -->
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                 <i class="fas fa-dice-one fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">CIS (Lite)</a></h5>
                                       <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>
                            </div><!-- card -->
                        </div> <!-- column -->
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-dice-six fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">CIS (Plus)</a></h5>
                                        <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>
                            </div><!-- card -->
                        </div> <!-- column -->
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-chart-area fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">Crystal Clear</a></h5>
                                       <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>
                            </div><!-- card -->
                        </div> <!-- column -->
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-calendar-check fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">Events</a></h5>
                                      <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>
                            </div><!-- card -->
                        </div> <!-- column -->
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-comments fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">Marketing Activities</a></h5>
                                         <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>  
                            </div><!-- card -->
                        </div> <!-- column -->
                        <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-gem fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">Tranquil Premium</a></h5>
                                      <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>
                            </div> <!-- card -->
                        </div> <!-- column -->
                         <div class="col-sm-4 col-md-3 col-lg-4 col-xl-3 mg-b-50">
                            <div class="card card-profile ">
                                <div class="card-body tx-13">
                                    <div>
                                        <a href="#">
                                            <div class="avatar d-inline">
                                                <i class="fas fa-users fa-4x" style="color: #8EBE3F;"></i>
                                            </div>
                                        </a>
                                        <h5><a href="#">User CRM</a></h5>
                                      <p> <BR/>Coming Soon<BR/><BR/></p>
                                    </div>
                                </div>
                            </div> <!-- card -->
                        </div> <!-- column -->

                    </div> <!-- row -->
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?>
        <script src="../assets/lib/jquery/jquery.min.js"></script>
        <script src="../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../assets/lib/feather-icons/feather.min.js"></script>
        <script src="../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="../assets/lib/jquery.flot/jquery.flot.js"></script>
        <script src="../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
        <script src="../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
        <script src="../assets/lib/chart.js/Chart.bundle.min.js"></script>
        <script src="../assets/lib/jqvmap/jquery.vmap.min.js"></script>
        <script src="../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
        <script src="../assets/js/dashforge.js"></script>
        <script src="../assets/js/dashforge.aside.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    </body>
</html>
