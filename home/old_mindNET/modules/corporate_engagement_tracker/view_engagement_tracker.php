<?php
require '../../if_loggedin.php';
include 'mindnet-host.php';
require_once('ewap-config.php');
require_once('soh-config.php');
require_once('tranquil-config.php');

if (isset($_REQUEST['from_date']) && isset($_REQUEST['to_date'])) {
    $from_date = $_REQUEST['from_date'];
    $to_date = $_REQUEST['to_date'];

    # Date to display
    $first_date = date('d-M-y', strtotime($from_date));
    $last_date = date('d-M-y', strtotime($to_date));

    # Date to calculate
    $ewap_first_monthdate = date('Y-m-d', strtotime($from_date));
    $ewap_last_monthdate = date('Y-m-d', strtotime($to_date));
}

$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use sohdbl");

$dbh_tq = new PDO($dsn_tranq, $tranq_user, $tranq_pass);
$dbh_tq->query("use mndfapp");

$i = 0;
$corp_eng_tracker = array();

ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
ini_set('max_execution_time', '0'); // for infinite time of execution 

$stmt010 = $dbh->prepare("SELECT * FROM corp_login WHERE 1 ORDER by name");
$stmt010->execute(array());
if ($stmt010->rowCount() != 0) {
    $rows_010 = $stmt010->fetchAll();

    foreach ($rows_010 as $row010) {
        $corp_id = $row010['corp_id'];
        $corp_name = $row010['name'];


        $stmt011 = $dbh_ewap->prepare("SELECT count(emp_id) as employees FROM emp_db WHERE corp_id=?");
        $stmt011->execute(array($corp_id));
        if ($stmt011->rowCount() != 0) {
            $row011 = $stmt011->fetch();
            $total_employees = $row011['employees'];
        }

        # Database Connection
        $count_tc = 0;
        $count_f2f = 0;
        $count_skype = 0;
        $count_chat = 0;
        $count_email = 0;
        $total_calls = 0;
        $telephonic = '';
        $face2face = '';
        $skype = '';
        $chats = '';
        $email = '';

        $stmt01 = $dbh_ewap->prepare("SELECT call_callers_profile.cid, call_callers_notes.source FROM scodd.call_callers_profile,call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_profile.corp_id=? AND DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=? ORDER BY taken_at ASC");
        $stmt01->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
        if ($stmt01->rowCount() != 0) {

            $rows_01 = $stmt01->fetchAll();

            foreach ($rows_01 as $row01) {
                $cid[$i] = $row01['cid'];
                $type[$i] = $row01['source'];



                if ($type[$i] == '110' || $type[$i] == '111' || $type[$i] == '112' || $type[$i] == '113' || $type[$i] == '114' || $type[$i] == '115' || $type[$i] == '116' || $type[$i] == '117') {
                    $count_tc++;
                } else if ($type[$i] == '103' || $type[$i] == '104' || $type[$i] == '105') {
                    $count_f2f++;
                } else if ($type[$i] == '106') {
                    $count_skype++;
                } else if ($type[$i] == '101' || $type[$i] == '102' || $type[$i] == '118' || $type[$i] == '119') {
                    $count_chat++;
                } else if ($type[$i] == '107' || $type[$i] == '108' || $type[$i] == '109') {
                    $count_email++;
                } else {
                    #Do nothing
                }

                $total_calls = ($count_f2f + $count_skype + $count_tc + $count_chat + $count_email);
            }
        }


        # Calculate total count
        $stmt02 = $dbh->prepare("SELECT count(user_time_creation.uid) as sco_users FROM user_time_creation,corp_users_list WHERE user_time_creation.uid= corp_users_list.uid AND DATE(user_time_creation.creation_time)>= ? AND DATE(user_time_creation.creation_time)<= ? AND corp_users_list.corp_id = ? ORDER BY DATE(user_time_creation.creation_time) ASC");
        $stmt02->execute(array($ewap_first_monthdate, $ewap_last_monthdate, $corp_id));
        if ($stmt02->rowCount() != 0) {
            $row02 = $stmt02->fetch();
            $sco_users = $row02['sco_users'];
        } else {
            $sco_users = 0;
        }

        $stmt12 = $dbh->prepare("SELECT start_date, end_date FROM corp_license WHERE corp_id = ? ");
        $stmt12->execute(array($corp_id));
        if ($stmt12->rowCount() != 0) {
            $row12 = $stmt12->fetch();
            $launch_date = date("d-m-Y", strtotime($row12['start_date']));
            $contract_end_date = date("d-m-Y", strtotime($row12['end_date']));
        } else {
            $launch_date = "";
            $contract_end_date = "";
        }

        $stmt03 = $dbh_tq->prepare("SELECT count(uid) AS tq_users from user_premium WHERE corp_id = ? AND start_date >= ? AND start_date <= ?;");
        $stmt03->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
        if ($stmt03->rowCount() != 0) {
            $row03 = $stmt03->fetch();
            $tranq_users = $row03['tq_users'];
        } else {
            $tranq_users = 0;
        }

        $stmt04 = $dbh->prepare("SELECT count(activity_id) as total_activity FROM corp_activity_track1 WHERE corp_activity_track1.corp_id=? AND DATE(held_on) >= ? AND DATE(held_on)<=? ORDER BY corp_activity_track1.held_on");
        $stmt04->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
        if ($stmt04->rowCount() != 0) {
            $row04 = $stmt04->fetch();
            $total_activity = $row04['total_activity'];
        } else {
            $total_activity = 0;
        }

        $stmt05 = $dbh->prepare("SELECT count(activity_id) as total_marketing_activity FROM marketing_activity WHERE marketing_activity.corp_id=? AND DATE(date) >= ? AND DATE(date)<=? ORDER BY marketing_activity.date");
        $stmt05->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
        if ($stmt05->rowCount() != 0) {
            $row05 = $stmt05->fetch();
            $total_marketing_activity = $row05['total_marketing_activity'];
        } else {
            $total_marketing_activity = 0;
        }

        $corp_counts = array("corp_id" => $corp_id, "corp_name" => $corp_name, "corp_headcount" => $total_employees, "phone" => $count_tc, "email" => $count_email, "video" => $count_skype,
            "chat" => $count_chat, "f2f" => $count_f2f, 'tranquil' => $tranq_users, 'sco' => $sco_users, 'events' => $total_activity,
            "marketing" => $total_marketing_activity, "launch_date" => $launch_date, "end_date" => $contract_end_date);

        $corp_eng_tracker[$i] = $corp_counts;

        $i++;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
<?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary panel-table">
                                <div class="panel-heading panel-heading-divider" style="padding-bottom: 15px;"><b>Corporate Engagement Tracker (<?php echo $first_date; ?> - <?php echo $last_date; ?> )</b>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover" style="width:100%;" >
                                            <thead >
                                                <tr>
                                                    <th>Client Name</th>
                                                    <th>Launch Date</th>
                                                    <th>Contract End Date</th>
                                                    <th>Headcount</th>
                                                    <th>Phone Counselling</th>
                                                    <th>Face to Face</th>
                                                    <th>Video</th>
                                                    <th>Chat</th>
                                                    <th>Email</th>
                                                    <th>Tranquil</th>
                                                    <th>SCO</th>
                                                    <th>Webinars/ Events </th>
                                                    <th>Marketing Collateral </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- to print all employee details in table-->
<?php foreach ($corp_eng_tracker as $corp_tracker) { ?>
                                                    <tr>
                                                        <td><?php echo substr($corp_tracker['corp_name'], 0, 30); ?></td>
                                                        <td><center><?php echo $corp_tracker['launch_date']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['end_date']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['corp_headcount']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['phone']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['f2f']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['video']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['chat']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['email']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['tranquil']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['sco']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['events']; ?></center></td>
                                                <td><center><?php echo $corp_tracker['marketing']; ?></center></td>
                                                </tr>
<?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                //Runs prettify
                prettyPrint();
            });
        </script>
    </body>
</html>
