<?php
//include files
require '../../if_loggedin.php';
include 'mindnet-config.php';
include 'mindnet-host.php';


if (isset($_REQUEST['btn-create-user'])) {

    echo "<script src='https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.min.js' type='text/javascript'>
          </script>";
          
    if (isset($_REQUEST['name']) && isset($_REQUEST['email']) && isset($_REQUEST['mobile']) && isset($_REQUEST['partner_id'])) {

        $username = $_REQUEST['name'];
        $email = $_REQUEST['email'];
        $mobile= $_REQUEST['mobile'];
        $partner_id = $_REQUEST['partner_id'];

            echo " <script>
              $.post( 'success.php', { email: '$email', name :'$username', mobile:'$mobile', partner_id: '$partner_id' })
              .done(function( status) {
                            if(status=='done'){
              $('.confirm-msg' ).replaceWith(
              '<div class=confirm-msg style=color:#3AB149;><b>Account Successfully Created.  <b></div>' );
              }else{
              $('.confirm-msg' ).replaceWith(
              '<div class=confirm-msg style=color:red;> Some Error Occured. Account Not Created. </div>' );
              }
              });
         </script>";
}
else {
    echo "Required Parameters Missing";
}
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Create Partnet User - Snapdeal </div>
                                    <div class="panel-body">
                                        <div Style="text-align: center;font-size: 15px;font-weight:bold;">
                                            <div class="confirm-msg">                                               
                                            </div>
                                        </div>
                                        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Name </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control"  name="name" placeholder="User Name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Email </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control"  name="email" placeholder="User Email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label">Mobile </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" id="mobile" class="form-control"  name="mobile" placeholder="User Mobile Number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label">Partner </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control" name="" value="Snapdeal - PTNR10004043" disabled >
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="hidden" class="form-control" name="partner_id" placeholder="Snapdeal (PTNR10004043)" value="PTNR10004043" >
                                                </div>
                                            </div>
                                            </div>
                                            <br/><br/>
                                            <div class="form-group m-b-0" style="text-align: center;">
                                                <input type="submit" class="btn btn-info waves-effect waves-light" value="Create User" name="btn-create-user"/> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
            });
        </script>
        <script>
            $(".close").click(function () {
                window.top.location.reload();
            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    mobile: {
                        required: true,
                        minlength: 7
                    }
                },
                messages: {
                    name: {
                        required: "Please enter the full name"
                    },
                    email: {
                        required: "Please enter the email",
                        email: "Invalid Email ID"
                    },
                    mobile: {
                        required: "Please enter the mobile number"
                    }
                }
            });
        </script>     
    </body>
</html>