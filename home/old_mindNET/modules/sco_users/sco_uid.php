<?php

function Luhn($number)
{
    $stack = 0;
    $number = str_split(strrev($number));
    foreach ($number as $key => $value)
    {
        if ($key % 2 == 0)
        {
            $value = array_sum(str_split($value * 2));
        } $stack += $value;
    } $stack %= 10;
    if ($stack != 0)
    {
        $stack -= 10;
        $stack = abs($stack);
    } $number = implode('', array_reverse($number));
    $number = strval($stack);
    return $number;
}

function get_char_code($char)
{
    switch ($char)
    {
        case "A" : return "11";
            break;
        case "B" : return "13";
            break;
        case "C" : return "17";
            break;
        case "D" : return "19";
            break;
        case "E" : return "21";
            break;
        case "F" : return "23";
            break;
        case "G" : return "27";
            break;
        case "H" : return "29";
            break;
        case "I" : return "31";
            break;
        case "J" : return "33";
            break;
        case "K" : return "37";
            break;
        case "L" : return "39";
            break;
        case "M" : return "41";
            break;
        case "N" : return "43";
            break;
        case "O" : return "47";
            break;
        case "P" : return "49";
            break;
        case "Q" : return "51";
            break;
        case "R" : return "53";
            break;
        case "S" : return "57";
            break;
        case "T" : return "59";
            break;
        case "U" : return "61";
            break;
        case "V" : return "63";
            break;
        case "X" : return "67";
            break;
        case "Y" : return "69";
            break;
        case "Z" : return "71";
            break;
        default: return "Z";
    }
}


function cal_checksum($uid)
{
    $a = get_char_code($uid[0]);
    $b = get_char_code($uid[1]);
    $c = get_char_code($uid[2]);
    $d = get_char_code($uid[3]);
    $t = $a * $b * $c * $d;

    $e = substr($uid, 4, 7);

    $num = $t * $e;
    $check_digit = Luhn($num);

    return $check_digit;
}

function create_new_uid($user_type)
{

	/*   UID generation methlodology *
	
	UID is of the form : AAAA-NNNN-NNNC
	
	AAAA has following values
	
	INID : For Individual Users
	INCR : For Corporate Users
	INTH : For Therapist Users
	
	CORP: For Corporate (HR)
	THRP : For Therapist Practionar

	NNNN - First four charcters of running sequence
	NNN	- Rest three characters of running sequence
	C - Checksum, for both number as well as intial characters
	
	*/

    include 'soh-config.php';

	$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
	$dbh->query("use sohdbl");
	
	#Forming the first part of UID
	switch($user_type)
	{
		case "INID": $uid_first_half="INID"; break;
		case "INCR": $uid_first_half="INCR"; break;
		case "INTH": $uid_first_half="INTH"; break;
		case "CORP": $uid_first_half="CORP"; break;
		case "THRP": $uid_first_half="THRP"; break;
		case "DEMO": $uid_first_half="DEMO"; break;
        case "PTNR": $uid_first_half="INPT"; break;
		default : $uid_first_half="INID"; //Default Initialises all users to INID
	}
	
    # Now working on second numeric part
	# Get the current value of running sequnce
	
    $stmt00 = $dbh->prepare("SELECT value FROM cvalue WHERE param='uid' LIMIT 1");
    $stmt00->execute(array());
    $row00 = $stmt00->fetch();	
    $uid_second_half = $row00['value'];
	
    # Increment the running sequnce of uid saved in the database
    $stmt01 = $dbh->prepare("UPDATE cvalue SET value=value+1 WHERE param='uid' LIMIT 1");
    $stmt01->execute(array());

    # Combine both first and second part to create the semi-full UID
    $uid = $uid_first_half . $uid_second_half;

    # Now calculating checksum for the semi-UID formed
    $checksm = cal_checksum($uid);

    # Add checksum to get the full UID
    $uid = $uid . $checksm;

    return $uid;
}

?>