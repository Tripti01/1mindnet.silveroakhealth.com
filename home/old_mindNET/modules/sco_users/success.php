<?php

if (isset($_REQUEST['name']) && isset($_REQUEST['email']) && isset($_REQUEST['mobile']) && isset($_REQUEST['partner_id'])) {

    include 'sco_uid.php';
    include 'token.php';
    include '../../../mail/send_scouser_created_email.php';

    $partner_id = $_REQUEST['partner_id'];
    $user_email = $_REQUEST['email'];
    $user_name = $_REQUEST['name'];
    $user_mobile = $_REQUEST['mobile'];

    #---------------Database Connection---------------------#
    $dbhostSCO = "scodbprod.cgbct3oeqwvc.ap-south-1.rds.amazonaws.com";
    $dbportSCO = 3306;
    $dbnameSCO = "sohdbl";
    $dsn1 = "mysql:host={$dbhostSCO};port={$dbportSCO};dbname={$dbnameSCO}";
    $login_user1 = "login_user";
    $login_pass1 = "Bangalore@1";

    #---------------Database Connection---------------------#
    $dbh = new PDO($dsn1, $login_user1, $login_pass1);
    $dbh->query("use sohdbl");

    #--------------User details-----------------------------#
    include 'mindnet/functions/crypto_funtions.php';
    $encryption_key = 'ab78e7997b6d5fcd55f4b5c32611b87cd923e88837b63bfdeccef819dc8ca282';

    $encrypted_name = encrypt($user_name, $encryption_key);
    # Take a has of the password

    $ref_type="PTNR";
    $ref_value=$partner_id;

    $token=create_email_token();

    $uid = create_new_uid("PTNR");

    date_default_timezone_set("Asia/Kolkata");

            #creating email token to send verification email
            $token = create_email_token();

            $order_done_datetime = date('Y-m-d H:i:s');
            $user_creation_time = date('Y-m-d H:i:s');

            #Calucalting the schudle for the user future sessions, We taking time as end of the day so that the comparing date and times become reliable
            $due_datetime_SSN0 = date('Y-m-d 23:59:59'); // Session 0 is due the same day.
            $due_datetime_SSN1 = date('Y-m-d 23:59:59', strtotime("+1 week"));
            $due_datetime_SSN2 = date('Y-m-d 23:59:59', strtotime("+2 week"));
            $due_datetime_SSN3 = date('Y-m-d 23:59:59', strtotime("+3 week"));
            $due_datetime_SSN4 = date('Y-m-d 23:59:59', strtotime("+4 week"));
            $due_datetime_SSN5 = date('Y-m-d 23:59:59', strtotime("+5 week"));
            $due_datetime_SSN6 = date('Y-m-d 23:59:59', strtotime("+6 week"));
            $due_datetime_SSN7 = date('Y-m-d 23:59:59', strtotime("+7 week"));
            $due_datetime_SSN8 = date('Y-m-d 23:59:59', strtotime("+8 week"));
            $due_datetime_CBTEND = date('Y-m-d 23:59:59', strtotime("+10 week"));

            $encrypted_user_name = encrypt($user_name, $encryption_key);
            
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh->beginTransaction();
            try {

                # Updating user temp table

                $stmt10 = $dbh->prepare("INSERT INTO user_temp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
                $stmt10->execute(array($user_email, $encrypted_name, '', $user_mobile, $ref_type, $ref_value, '', '', '', $uid,  $user_creation_time, '0', $token, ''));

                # Inserting new rows for the user
                $stmt50 = $dbh->prepare("INSERT INTO user_login VALUES (?,?,?,?)");
                $stmt50->execute(array($user_email, $uid, $encrypted_user_name, ''));

                $stmt60 = $dbh->prepare("INSERT INTO user_profile VALUES (?,?,?,?,?,?,?)");
                $stmt60->execute(array($uid, '', '', '', '', '', ''));

                $stmt70 = $dbh->prepare("INSERT INTO user_location VALUES (?,?,?,?,?,?,?,?)");
                $stmt70->execute(array($uid, '', '', '', '', '', '', ''));

                $stmt80 = $dbh->prepare("INSERT INTO user_time_creation VALUES (?,?,?)");
                $stmt80->execute(array($uid, $user_creation_time, ""));

                $stmt90 = $dbh->prepare("INSERT INTO user_info VALUES (?,?,?,?,?)");
                $stmt90->execute(array($uid, '0', '', '', ''));

                $stmt95 = $dbh->prepare("INSERT INTO user_comm_pref VALUES (?,?,?,?,?,?)");
                $stmt95->execute(array($uid, $user_email, '', '', '', ''));

                $stmt100 = $dbh->prepare("INSERT INTO user_progress VALUES "
                        . "('$uid','1','SSN0','0','0'),"
                        . "('$uid','2','SSN1','0','0'),"
                        . "('$uid','3','SSN2','0','0'),"
                        . "('$uid','4','SSN3','0','0'),"
                        . "('$uid','5','SSN4','0','0'),"
                        . "('$uid','6','SSN5','0','0'),"
                        . "('$uid','7','SSN6','0','0'),"
                        . "('$uid','8','SSN7','0','0'),"
                        . "('$uid','9','SSN8','0','0');");
                $stmt100->execute(array());

                $stmt105 = $dbh->prepare("INSERT INTO user_time_ssn VALUES "
                        . "('$uid','SSN0','',''),"
                        . "('$uid','SSN1','',''),"
                        . "('$uid','SSN2','',''),"
                        . "('$uid','SSN3','',''),"
                        . "('$uid','SSN4','',''),"
                        . "('$uid','SSN5','',''),"
                        . "('$uid','SSN6','',''),"
                        . "('$uid','SSN7','',''),"
                        . "('$uid','SSN8','','');");
                $stmt105->execute(array());

                $stmt110 = $dbh->prepare("INSERT INTO user_schedule VALUES "
                        . "('$uid','SSN0','$due_datetime_SSN0'),"
                        . "('$uid','SSN1','$due_datetime_SSN1'),"
                        . "('$uid','SSN2','$due_datetime_SSN2'),"
                        . "('$uid','SSN3','$due_datetime_SSN3'),"
                        . "('$uid','SSN4','$due_datetime_SSN4'),"
                        . "('$uid','SSN5','$due_datetime_SSN5'),"
                        . "('$uid','SSN6','$due_datetime_SSN6'),"
                        . "('$uid','SSN7','$due_datetime_SSN7'),"
                        . "('$uid','SSN8','$due_datetime_SSN8'),"
                        . "('$uid','CBTEND','$due_datetime_CBTEND');");
                $stmt110->execute(array());

                # Inserting into the user type table
                $stmt170 = $dbh->prepare("INSERT INTO user_type VALUES (?,?,?,?,?,?)");
                $stmt170->execute(array($uid, 'PTNR', $partner_id, 'SC', '', ''));

                $stmt180 = $dbh->prepare("INSERT INTO user_therapist VALUES (?,?)");
                $stmt180->execute(array($uid,''));

                $stmt180 = $dbh->prepare("INSERT INTO ptnr_users_list VALUES (?,?,?,?)");
                $stmt180->execute(array($uid,$partner_id,'',''));

            send_scouser_created_email($user_name, $token);
       

        $dbh->commit();
        echo 'done';

    } catch (PDOException $e) {
        $dbh->rollBack();
        echo $e->getMessage();
        echo 'not done';
    }
} else {
    echo 'not done';
}
?>