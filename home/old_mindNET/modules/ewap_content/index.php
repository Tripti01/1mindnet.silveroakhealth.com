<?php

//include file
require '../../if_loggedin.php';
include 'newap-config.php';

$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use newapdb");


if(isset($_REQUEST['type'])){
$type = $_REQUEST['type'];
}else{
    $type = " ";
}

$i = 0;
$status_code = 0;
$stmt1 = $dbh->prepare("SELECT * FROM eap_assets_all WHERE type=?");
$stmt1->execute(array($type))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [EWAP - CONTENT -1000].");
if ($stmt1->rowCount() != 0) {
    while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $id[$i] = $row1['asset_id'];
        $heading[$i] = $row1['heading'];
        $i++;
    }
}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="background-image: url('<?php echo $img_loc; ?>'); background-size:cover;background-repeat: no-repeat;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>
            <?php include 'sidebar.php'; ?>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default panel-table">
                                <div class="tab-content" >
                                      <table class="table table-striped table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Content Id</th>
                                                    <th>Heading</th>
                                                    <th>Edit</th>
                                                    <th>Preview</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if(isset($id)){
                                                for($j=0;$j< count($id);$j++){?>
                                                <tr>
                                                    <td><?php echo $j + 1; ?></td>
                                                    <td><?php echo $id[$j]; ?></td>
                                                    <td><?php echo $heading[$j]; ?></td>
                                                    <td>
                                                        <a href="update_content.php?type=<?php echo $type; ?>&asset_id=<?php echo $id[$j]; ?>">Edit</a></td>
                                                    <td><a href="">Preview</a></td>
                                                 </tr>
                                                <?php }} ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-flot/plugins/curvedLines.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/countup/countUp.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jqvmap/jquery.vmap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-dashboard.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.dashboard();

            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('.sub-menu a').filter(function () {
                    return this.href == location.href
                }).parent().addClass('active').siblings().removeClass('active')
                $('.sub-menu a').click(function () {
                    $(this).parent().addClass('active').siblings().removeClass('active')
                })
            })
        </script>
    </body>
</html>