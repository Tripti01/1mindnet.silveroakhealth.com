<?php
//include file
require '../../if_loggedin.php';
include 'ewap-config.php';
//include 'newap-config.php';

if(isset($_REQUEST['status_code'])){
    $status_code = $_REQUEST['status_code'];
}else{
    $status_code = 0;
}


# Select categories from database and display in the form
$stmt00 = $dbh_ewap->prepare("SELECT category_id,category_name FROM article_category WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $category_id[$i] = $row00['category_id'];
        $category_name[$i] = $row00['category_name'];
        $i++;
    }
}	
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>Article List</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/summernote/summernote.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <style>
            .error{
                color:red;
            }
			.alignnone size-full wp-image-1371{
				height:80% !important;
				width: 70% !important;
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
<?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
							<p><a class="btn btn-primary" style ="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin-top:-1%;" href="add_article.php">Back</a></p>
                               <div class="panel panel-default panel-border-color panel-border-color-primary" style="margin-top:2.5%;">
								  <div class="panel-heading panel-heading-divider" style="margin-bottom:0%;"><b>Articles List <span style="font-size:12px;">(Click the article to update)</span</b></div>
                                    
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
												<div class="tab-content" >
												<?php for ($i = 0; $i < $stmt00->rowCount(); $i++) { ?>
												<table class="table table-striped table-borderless">
                                            <thead>
                                                <tr>
                                                    <th><?php echo $category_name[$i]; ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                   <td><?php
                                        # Select the article names of each category and list it in the accordeons
                                        $stmt01 = $dbh_ewap->prepare("SELECT article_id,heading FROM article_content WHERE category_id = ?");
                                        $stmt01->execute(array($category_id[$i]));
                                        if ($stmt01->rowCount() != 0) {
                                            while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
                                                $article_id = $row01['article_id'];
                                                $heading = $row01['heading'];
                                                ?>
                                                <a href="update_article.php?article_id=<?php echo $article_id; ?>&article=<?php echo $heading; ?>"><p class="blog-list"style="text-transform: uppercase"><i style="padding-right:1%;" class="fa fa-dot-circle-o" aria-hidden="true"></i><?php echo $heading; ?></p></a>
                                                        <?php
                                                    }
                                                }
                                                ?></td>
												</tr>
                                            </tbody>
                                        </table>
								<?php }?>
                                    </div>
                                </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/js/main.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="../../../assets/lib/markdown-js/markdown.js" type="text/javascript"></script>
<script src="../../../assets/js/app-form-wysiwyg.js" type="text/javascript"></script>
<script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
        App.textEditors();
    });
</script>

<script>
    $(".form-horizontal").validate({
        rules: {
            asmt_heading: {
                required: true
            },
            asmt_image: {
                required: true
            },
            asmt_body: {
                required: true
            },
            author_id: {
                required: true
            },
            category_id: {
                required: true
            }
        },
        messages: {
            asmt_heading: {
                required: "Please enter the heading"
            },
            asmt_image: {
                required: "Please enter the image url."
            },
            asmt_body: {
                required: "Please enter the body"
            },
            author_id: {
                required: "Please select one option."
            },
            category_id: {
                required: "Please select one option."
            }
        }
    });
</script>
<script>
function validate(form) {
    //The key here is that you get all the "options[]" elements in an array
    var options = document.getElementsByName("options[]");
    
    if(options[0].checked==false && options[1].checked==false && options[2].checked==false) {
        document.getElementById("check_err").innerHTML = "Please check atleast one of the above option";
        return false;
    }
    return true;
}
</script>
<script>
 jQuery(function(){
$("#button").click(function(){
    var isChecked = jQuery("input[name=type]:checked").val();
     if(!isChecked){
         $("#err_type").html("Please enter type");
     }else{
         $("#err_type").html("");
     }
});
});
    </script>
	</body>
</html>