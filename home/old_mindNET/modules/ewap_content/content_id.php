<?php

# This functions returns an content ID. This function does not recieve anything 

function create_content_id() {
    include 'newap-config.php';

    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use newapdb");
    
    $stmt00 = $dbh->prepare("SELECT value FROM eap_cvalue WHERE param='asset_id' LIMIT 1");
    $stmt00->execute(array());
    $row00 = $stmt00->fetch();	
    $id = $row00['value'];
	
    # Increment the running sequnce of id saved in the database
    $stmt01 = $dbh->prepare("UPDATE eap_cvalue SET value=value+1 WHERE param='asset_id' LIMIT 1");
    $stmt01->execute(array());

    # Combine both first and second part to create the semi-full UID
    $asset_id = $id;

    
    return $asset_id;
}
?>