<?php
//include file
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'newap-config.php';

$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use newapdb");

if(isset($_REQUEST['status_code'])){
    $status_code = $_REQUEST['status_code'];
}else{
    $status_code = 0;
}


$j = 0;
$stmt11 = $dbh->prepare("SELECT * FROM eap_blog_authors WHERE 1");
$stmt11->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [ADD-NEW-CONT-1000].");
if ($stmt11->rowCount() != 0) {
    while ($row11 = $stmt11->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $author_id[$j] = $row11['author_id'];
        $author_name[$j] = $row11['author_name'];
        $j++;
    }
}

$i = 0;
$stmt11 = $dbh->prepare("SELECT * FROM eap_content_trck WHERE 1");
$stmt11->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [ADD-NEW-CONT-1001].");
if ($stmt11->rowCount() != 0) {
    while ($row11 = $stmt11->fetch(PDO::FETCH_ASSOC)) {
# Get track Id from SQL results 
        $track_id[$i] = $row11['track_id'];
        $i++;
    }
}

# On form submission check if all the form fields are submitted
if (isset($_REQUEST['submit'])) {
    // Check if all the fields are set
    if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && $_REQUEST['type'] !== '' && isset($_REQUEST['asmt_heading']) && !empty($_REQUEST['asmt_heading']) && $_REQUEST['asmt_heading'] !== '' && isset($_REQUEST['asmt_desc']) && !empty($_REQUEST['asmt_desc']) && $_REQUEST['asmt_desc'] !== '' && isset($_REQUEST['asmt_body']) && !empty($_REQUEST['asmt_body']) && $_REQUEST['asmt_body'] !== '' && isset($_REQUEST['options']) && !empty($_REQUEST['options']) && $_REQUEST['options'] !== '') {

        include 'content_id.php';

        # Radio button
        $type = $_REQUEST['type'];

        #heading
        $heading = $_REQUEST['asmt_heading'];

        $asset_id = create_content_id();

        #body
        $body = $_REQUEST['asmt_body'];

        #description
        $description = $_REQUEST['asmt_desc'];
        
        #priority
        $check1 = $_REQUEST['options'];
        
        $high = 0;
        $medium = 0;
        $low = 0;
        
        #checkbox colloborate
        for ($i = 0; $i < count($check1); $i++) {
            $colloborate = $check1[$i];
            if ($colloborate == "HIG") {
                $high = 1;
            }
            if ($colloborate == "LOW") {
                $low = 1;
            }
            if ($colloborate == "MED") {
                $medium = 1;
            }
        }

        if (isset($_REQUEST['author_id'])) {
            $author_id = $_REQUEST['author_id'];
        } else {
            $author_id = "";
        }
        if (isset($_REQUEST['track_type'])) {
            $track_type = $_REQUEST['track_type'];
        } else {
            $track_type = "";
        }
        
        if(isset($_REQUEST['duration_m'])){
            $minute = $_REQUEST['duration_m'];
        }else{
            $minute = "";
        }
        
        if(isset($_REQUEST['duration_s'])){
            $second = $_REQUEST['duration_s'];
        }else{
            $second= "";
        }
        
        $arr_duration = array ($minute, $second);
        $duration = implode(":",$arr_duration);

        if (isset($_REQUEST['track_id'])) {
            $track_id = $_REQUEST['track_id'];
        } else {
            $track_id = "";
        }

        if (isset($_REQUEST['asmt_url'])) {
            $asmt_url = $_REQUEST['asmt_url'];
        } else {
            $asmt_url = "";
        }

        if (isset($_REQUEST['video_url'])) {
            $video_url = $_REQUEST['video_url'];
        } else {
            $video_url = "";
        }

        if (isset($_REQUEST['journal_url'])) {
            $journal_url = $_REQUEST['journal_url'];
        } else {
            $journal_url = "";
        }

        if (isset($_REQUEST['pstr_url'])) {
            $poster_url = $_REQUEST['pstr_url'];
        } else {
            $pstr_url = "";
        }
        
        
        //upload image
        include('image_check.php'); // getExtension Method
        $msg = '';
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $name = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $tmp = $_FILES['file']['tmp_name'];
            $ext = getExtension($name);

            if (strlen($name) > 0) {
                // File format validation
                if (in_array($ext, $valid_formats)) {
                // File size validation
                    if ($size < (1024 * 1024)) {
                        include('s3_config.php');
                //Rename image name. 
                        $actual_image_name = time() . "." . $ext;

                        if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                            $msg = "S3 Upload Successful.";
                            $s3file = 'https://s3.ap-south-1.amazonaws.com/sohtest/'.$actual_image_name;
                            echo "<img src='$s3file'/>";
                            echo 'S3 File URL:' . $s3file;
                        } else
                            $msg = "S3 Upload Fail.";
                    } else
                        $msg = "Image size Max 1 MB";
                } else
                    $msg = "Invalid file, please upload image file.";
            } else
                $msg = "Please select image file.";
        }

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->beginTransaction();


        #inserting blog data
        if ($type == 'BLOG') {

            $stmt10 = $dbh->prepare("INSERT INTO eap_content_blog VALUES (?,?,?,?,?)");
            $stmt10->execute(array($asset_id, $body, $author_id, "", ""));

            #inserting assessment data
        } else if ($type == 'ASMT') {

            $stmt10 = $dbh->prepare("INSERT INTO eap_content_asmt VALUES (?, ?, ?, ?)");
            $stmt10->execute(array($asset_id, $asmt_url, "", ""));
        } else if ($type == 'PSTR') {

            $stmt10 = $dbh->prepare("INSERT INTO eap_content_pstr VALUES (?,?,?,?,?,?)");
            $stmt10->execute(array($asset_id, $body, " ", $poster_url, "", ""));
        } else if ($type == 'TRCK') {

            $stmt10 = $dbh->prepare("INSERT INTO eap_content_trck VALUES (?,?,?,?,?,?,?)");
            $stmt10->execute(array($asset_id, $track_id, $body, $track_type, $duration, "", ""));
        } else if ($type == 'JRNL') {

            $stmt10 = $dbh->prepare("INSERT INTO eap_content_jrnl VALUES (?,?,?,?,?,?)");
            $stmt10->execute(array($asset_id, $body, "", $journal_url, "", ""));
        } else if ($type == 'VDEO') {

            $stmt10 = $dbh->prepare("INSERT INTO eap_content_vdeo VALUES (?, ?, ?, ?, ?)");
            $stmt10->execute(array($asset_id, $body, $video_url, "", ""));
        }
        
        
        $stmt20 = $dbh->prepare("INSERT INTO eap_assets_all VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt20->execute(array($asset_id, $heading, $description, "",$type, $low, $medium, $high, "", "", "", ""));
        
        $dbh->commit();
        $status_code = 1;
    } else {
       
        $status_code = 2;
    }
    
        $location = 'Location: ' . $host . '/modules/ewap_content/add_new_content.php?status_code=' . $status_code;
        header($location);
        exit();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/summernote/summernote.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <style>
            .error{
                color:red;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
<?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider"><b>Add New Asset</b></div>
                                    <?php
									#displaying success or failure messages
                                    if ($status_code != 0) {
                                        switch ($status_code) {

                                            case 1:
                                                echo '<div class="alert alert-success" style="text-align:center;">';
                                                echo "<strong>Asset Added successfully.</strong>";
                                                echo '</div>';
                                                break;
                                            case 2:
                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                echo "<strong>Some of the fields are empty.</strong>";
                                                echo '</div>';
                                                break;

                                            default: break;
                                        }
                                    }
                                    ?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <form class="form-horizontal" role="form" onsubmit="return validate(this);" method="POST">
                                                    <div class="form-group">
                                                        <div class ="row">
                                                            <div class="col-md-1">
                                                                &nbsp;
                                                            </div>
                                                            <label class="col-md-1" style="margin-left: 2%;margin-top:1.2%;">Type <span style="color:red;">*</span></label>

                                                            <div class="form-group col-md-1" style="text-align:right;">
                                                                <input id="H0" name="type" type="radio" value="VDEO">
                                                                <label for="H0" style="font-weight:400;">
                                                                    Video
                                                                </label>
                                                            </div>

                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H1" name="type" type="radio" value="BLOG">
                                                                <label for="H1" style="font-weight:400;">
                                                                    Blog
                                                                </label>
                                                            </div>

                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H2" name="type" type="radio" value="PSTR">
                                                                <label for="H2" style="font-weight:400;">
                                                                    Poster
                                                                </label>
                                                            </div>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H3" name="type" type="radio" value="JRNL">
                                                                <label for="H3" style="font-weight:400;">
                                                                    Journals
                                                                </label>
                                                            </div>

                                                            <div class="form-group col-md-2" style="text-align:center;margin-left:1%;">
                                                                <input id="H4" name="type" type="radio" value="ASMT">
                                                                <label for="H4" style="font-weight:400;">
                                                                    Assessment
                                                                </label>
                                                            </div>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:2%;">
                                                                <input id="H5" name="type" type="radio" value="TRCK">
                                                                <label for="H5" style="font-weight:400;">
                                                                    Track
                                                                </label>
                                                            </div>
                                                            <div class="form-group col-md-2" style="text-align:center;margin-left:2%;">
                                                                <input id="H6" name="type" type="radio" value="MISC">
                                                                <label for="H6" style="font-weight:400;">
                                                                    Miscellaneous
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2" style="text-align:center;margin-left:15.7%;margin-top:-2%;">
                                                            <div id="err_type" style="color:red;font-size:13px;"></div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-1%;">
                                                        <label class="col-md-2 control-label" style="margin-top:-1.2%;">Priority <span style="color:red;">*</span>
                                                        </label>

                                                        <div class="col-md-10">
                                                            
                                                            <div class="col-md-2"style="margin-left:-1%;">
                                                                <input id="priority1" name="options[]" type="checkbox" value="LOW">
                                                                <label for="priority1">
                                                                    Low
                                                                </label>
                                                            </div>
                                                            
                                                            <div class="col-md-2" style="margin-left:-7%;">
                                                                <input id="priority2" name="options[]" type="checkbox" value="MED">
                                                                <label for="priority2">
                                                                    Medium
                                                                </label>
                                                            </div>
                                                                
                                                            <div class="col-md-2"style="margin-left:-5%;">
                                                                <input id="priority3" name="options[]" type="checkbox" value="HIG">
                                                                <label for="priority3">
                                                                    High
                                                                </label>
                                                            </div>     
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                &nbsp;
                                                            </div>
                                                            <div id="check_err" class="error col-md-9" style="font-size:13px;margin-left:1%;">
                                                                    <!--message is displayed for validation-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-1.5%;">
                                                        <label class="col-md-2 control-label">Heading <span style="color:red;">*</span>
                                                        </label>

                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control input-sm" name="asmt_heading" placeholder="Heading">
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="margin-top:-1%;">
                                                        <label class="col-md-2 control-label">Description <span style="color:red;">*</span>
                                                        </label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control input-sm" name="asmt_desc" placeholder="Description" value="">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-1.5%;">
                                                        <label class="col-md-2 control-label">Body <span style="color:red;">*</span>
                                                        </label>
                                                        <div class="panel-body col-md-10" >
                                                            <textarea id="editor1" type="text" name="asmt_body" style="border:none; background: transparent; outline: 0;"/></textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-2.5%;">
                                                        <label class="col-md-2 control-label">Upload File<span style="color:red;">*</span>
                                                        </label>
                                                        <div class="panel-body col-md-10" >
                                                            <input type="file" id="file" name="asmt_upload"/>
                                                        </div>
                                                    </div>
                                                    
                                                    <div id="H1_div" style='display:none'>
                                                        <div class="form-group" style="">
                                                            <label class="col-md-2 control-label" for="pass1" style="margin-bottom:1%;">Author <span style="color:red;">*</span></label>

                                                            <div class="col-md-4" style="">
                                                                <select id="author_id" name="author_id" title="" class="form-control  input-sm">
                                                                    <option value="" disabled="" selected="">  -- SELECT -- </option> 
                                                                        <?php for ($j = 0; $j < count($author_id); $j++) { ?>
                                                                                <option value="<?php echo $author_id[$j]; ?>"><?php echo $author_name[$j]; ?></option>
                                                                        <?php } ?>
                                                                </select>
                                                                <div id="err_yce" style="color:red"></div>
                                                            </div>
                                                        </div>	
                                                    </div>

                                                    <div id="H3_div" style='display:none'>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Download Url <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input type="text" class="form-control  input-sm" type="text" placeholder="URL" name="journal_url"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="H0_div" style='display:none'>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Download Url <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" placeholder="URL" name="video_url"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="H4_div" style='display:none'>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Assessment Link <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" placeholder="URL" name="asmt_url"/></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="H2_div" style='display:none'>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Download Link <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" placeholder="URL" name="pstr_url"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="H5_div" style='display:none'>
                                                        <div class="form-group" style="margin-top:-2.5%">
                                                            <label class="col-md-2 control-label">Track ID<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <div>
                                                                    <input type="text" class="form-control input-sm" name="track_id" placeholder="Track ID" style="width:109%;">
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">Track Type<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <select name="track_type" title="" class="form-control input-sm" style="">
                                                                    <option value="" disabled="" selected="">  -- SELECT -- </option> 

                                                                    <option value="GD">Guided Tracks</option>
                                                                    <option value="UN">Unguided Tracks</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-top:-0.7%">
                                                            <label class="col-md-2 control-label">Duration<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-md-2">
                                                                <select name="duration_m" title="" class="form-control  input-sm">
                                                                    <option value="" disabled="" selected="">  -- Minutes -- </option> 
                                                                    <?php for($m=1; $m < 60; $m++){?>
                                                                    <option value="<?php echo $m; ?>"><?php echo $m;?></option>
                                                                    <?php }?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <select name="duration_s" title="" class="form-control input-sm">
                                                                    <option value="" disabled="" selected="">  -- Seconds -- </option> 
                                                                    <?php for($s=1; $s<=5; $s++){?>
                                                                       <option value="<?php echo $s*10; ?>"><?php echo $s*10;?></option> 
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-6 col-sm-9">
                                                <button type="submit" id="button" name="submit" class="btn btn-info waves-effect waves-light" name="btn-send-discount-link" style="margin-left:-4%;">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/js/main.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="../../../assets/lib/markdown-js/markdown.js" type="text/javascript"></script>
<script src="../../../assets/js/app-form-wysiwyg.js" type="text/javascript"></script>
<script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
        App.textEditors();
    });
</script>
<script>
    $('input[type="radio"]').click(function () {
        //alert($(this).attr('id'));
        if ($(this).attr('id') == 'H0') {
            $('#H0_div').show();
        } else {
            $('#H0_div').hide();
        }
        if ($(this).attr('id') == 'H1') {
            $('#H1_div').show();
        } else {
            $('#H1_div').hide();
        }
        if ($(this).attr('id') == 'H2') {
            $('#H2_div').show();
        } else {
            $('#H2_div').hide();
        }
        if ($(this).attr('id') == 'H3') {
            $('#H3_div').show();
        } else {
            $('#H3_div').hide();
        }
        if ($(this).attr('id') == 'H4') {
            $('#H4_div').show();
        } else {
            $('#H4_div').hide();
        }
        if ($(this).attr('id') == 'H5') {
            $('#H5_div').show();
        } else {
            $('#H5_div').hide();
        }
         if ($(this).attr('id') == 'H6') {
            $('#H6_div').show();
        } else {
            $('#H6_div').hide();
        }

    });
</script>
<script>
    $(".form-horizontal").validate({
        rules: {
            asmt_heading: {
                required: true
            },
            asmt_desc: {
                required: true
            },
            asmt_body: {
                required: true
            },
            journal_url: {
                required: true
            },
            video_url: {
                required: true
            },
            author_id: {
                required: true
            },
            track_id: {
                required: true
            },
            track_type: {
                required: true
            },
            asmt_url: {
                required: true
            },
            pstr_url: {
                required: true
            }
        },
        messages: {
            asmt_heading: {
                required: "Please enter the heading"
            },
            asmt_desc: {
                required: "Please enter the description"
            },
            asmt_body: {
                required: "Please enter the body"
            },
            journal_url: {
                required: "Please write the url here."
            },
            video_url: {
                required: "Please write the url here."
            },
            author_id: {
                required: "Please select one."
            },
            track_id: {
                required: "Please enter the track ID"
            },
            track_type: {
                required: "Please select one option."
            },
            asmt_url: {
                required: "Please select one option."
            },
            pstr_url: {
                required: "Please select one option."
            }
        }
    });
</script>
<script>
function validate(form) {
    //The key here is that you get all the "options[]" elements in an array
    var options = document.getElementsByName("options[]");
    
    if(options[0].checked==false && options[1].checked==false && options[2].checked==false) {
        document.getElementById("check_err").innerHTML = "Please check atleast one of the above option";
        return false;
    }
    return true;
}
</script>
<script>
 jQuery(function(){
$("#button").click(function(){
    var isChecked = jQuery("input[name=type]:checked").val();
     if(!isChecked){
         $("#err_type").html("Please enter type");
     }else{
         $("#err_type").html("");
     }
});
});
    </script>
</body>
</html>