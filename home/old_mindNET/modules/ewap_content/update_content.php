<?php
//include file
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'newap-config.php';

$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use newapdb");


if(isset($_REQUEST['status_code'])){
    $status_code = $_REQUEST['status_code'];
}else{
    $status_code = 0;
}

if (isset($_REQUEST['type'])) {
    $type = $_REQUEST['type'];
} else {
    $type = "";
}


if(isset($_REQUEST['asset_id'])){
    $asset_id = $_REQUEST['asset_id'];
}else{
    $asset_id = "";
}

$j = 0;
$stmt11 = $dbh->prepare("SELECT * FROM eap_blog_authors WHERE 1");
$stmt11->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [ADD-NEW-CONT-1000].");
if ($stmt11->rowCount() != 0) {
    while ($row11 = $stmt11->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $author_id[$j] = $row11['author_id'];
        $author_name[$j] = $row11['author_name'];
        $j++;
    }
}



#all the values are fetched for particular id to display it on this page.
    $stmt01 = $dbh->prepare("SELECT * FROM eap_assets_all WHERE type = ? and asset_id = ? LIMIT 1");
    $stmt01->execute(array($type, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC1");
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        if(isset($row01['heading'])){
            $heading = $row01['heading'];
        }else{
            $heading = "";
        }
        
        if(isset($row01['descn'])){
            $description = $row01['descn'];
        }else{
            $description ="";
        }
        
        if(isset($row01['preview_img'])){
            $image = $row01['preview_img'];
        }else{
            $image = "";
        }
        
        if(isset($row01['low'])){
            $low = $row01['low'];
        }else{
            $low = "";
        }
        if(isset($row01['medium'])){
            $medium = $row01['medium'];
        }else{
            $medium = "";
        }
        
        if(isset($row01['high'])){
            $high = $row01['high'];
        }else{
            $high = "";
        }
        
        if ($type != $row01['type']) {
            $type = $_REQUEST['type'];
        }
    }
    
    //tracks
    $stmt02 = $dbh->prepare("SELECT * FROM eap_content_trck WHERE asset_id = ? LIMIT 1");
    $stmt02->execute(array($asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC2");
    if ($stmt02->rowCount() != 0) {
        $row02 = $stmt02->fetch();
        if(isset($row02['track_id'])){
            $track_id = $row02['track_id'];
        }else{
            $track_id = "";
        }
        
        if(isset($row02['type'])){
            $track_type = $row02['type'];
            } else{
            $track_type = "";
        }
        
        if(isset($row02['body'])){
            $body_fetched = $row02['body'];
            } else{
            $body_fetched = "";
        }
        if(isset($row02['duration'])){
            $dur_arr = $row02['duration'];
            $duration = explode(':' , $dur_arr);
            $dur_minute = $duration[0];
            $dur_seconds = $duration[1];
        }else{
            $duration_arr = "";
        }
        
    }
    
    //poster
    $stmt03 = $dbh->prepare("SELECT * FROM eap_content_pstr WHERE asset_id = ? LIMIT 1");
    $stmt03->execute(array($asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC3");
    if ($stmt03->rowCount() != 0) {
        $row03 = $stmt03->fetch();
        if(isset($row03['download_path'])){
            $pstr_url = $row03['download_path'];
        }else{
            $pstr_url = "";
        }
        
        if(isset($row03['body'])){
            $body_fetched = $row03['body'];
            } else{
            $body_fetched = "";
        }
    }
    
    //video
    $stmt04 = $dbh->prepare("SELECT * FROM eap_content_vdeo WHERE asset_id = ? LIMIT 1");
    $stmt04->execute(array($asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC4");
    if ($stmt04->rowCount() != 0) {
        $row04 = $stmt04->fetch();
        
        if(isset($row04['video_url'])){
            $video_url = $row04['video_url'];
        }else{
            $video_url = "";
        }
        
         if (isset($row04['body'])) {
            $body_fetched = $row04['body'];
        } else {
            $body_fetched = "";
        }
    }
    
    //blogs
    $stmt05 = $dbh->prepare("SELECT * FROM eap_content_blog WHERE asset_id = ? LIMIT 1");
    $stmt05->execute(array($asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC5");
    if ($stmt05->rowCount() != 0) {
        $row05 = $stmt05->fetch();
        if(isset($row05['author_id'])){
            $author_id_fetched = $row05['author_id'];
             $stmt00 = $dbh->prepare("SELECT * FROM eap_blog_authors WHERE author_id = ? LIMIT 1");
             $stmt00->execute(array($author_id_fetched)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC5");
             if ($stmt00->rowCount() != 0) {
                $row00 = $stmt00->fetch();
                if(isset($row00['author_name'])){
                    $author_name_fetched = $row00['author_name'];
                }else{
                    $author_name_fetched = "";  
                }
             }
        }else{
            $author_id_fetched = "";
        }
        
        if (isset($row05['body'])) {
            $body_fetched = $row05['body'];
        } else {
            $body_fetched = "";
        }
    }
    
    //asmt
    $stmt06 = $dbh->prepare("SELECT * FROM eap_content_asmt WHERE asset_id = ? LIMIT 1");
    $stmt06->execute(array($asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC6");
    if ($stmt06->rowCount() != 0) {
        $row06 = $stmt06->fetch();
        if(isset($row06['path'])){
            $asmt_url= $row06['path'];
            }else{
            $asmt_url = "";
        }
        if (isset($row06['body'])) {
            $body_fetched = $row06['body'];
        } else {
            $body_fetched = "";
        }
    }
    
    //jrnl
    $stmt07 = $dbh->prepare("SELECT * FROM eap_content_jrnl WHERE asset_id = ? LIMIT 1");
    $stmt07->execute(array($asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC7");
    if ($stmt07->rowCount() != 0) {
        $row07 = $stmt07->fetch();
        if(isset($row07['download_path'])){
            $jrnl_url= $row07['download_path'];
            }else{
            $jrnl_url = "";
        }
        if (isset($row07['body'])) {
            $body_fetched = $row07['body'];
        } else {
            $body_fetched = "";
        }
    }
    
    


# On form submission check if all the form fields are submitted
if (isset($_REQUEST['submit'])) {
    // Check if all the fields are set
    if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && $_REQUEST['type'] !== '' && isset($_REQUEST['asmt_heading']) && !empty($_REQUEST['asmt_heading']) && $_REQUEST['asmt_heading'] !== '' && isset($_REQUEST['asmt_desc']) && !empty($_REQUEST['asmt_desc']) && $_REQUEST['asmt_desc'] !== '' && isset($_REQUEST['asmt_body']) && !empty($_REQUEST['asmt_body']) && $_REQUEST['asmt_body'] !== '' && isset($_REQUEST['options']) && !empty($_REQUEST['options']) && $_REQUEST['options'] !== '') {

        include 'content_id.php';

        # Radio button
        $type = $_REQUEST['type'];

        #heading
        $heading = $_REQUEST['asmt_heading'];

        $asset_id = $_REQUEST['asset_id'];

        #body
        $body = $_REQUEST['asmt_body'];

        #description
        $description = $_REQUEST['asmt_desc'];
        
        #priority
        $check1 = $_REQUEST['options'];
        
        $high = 0;
        $medium = 0;
        $low = 0;
        
        #checkbox colloborate
        for ($i = 0; $i < count($check1); $i++) {
            $colloborate = $check1[$i];
            if ($colloborate == "HIG") {
                $high = 1;
            }
            if ($colloborate == "LOW") {
                $low = 1;
            }
            if ($colloborate == "MED") {
                $medium = 1;
            }
        }

        if (isset($_REQUEST['author_id'])) {
            $author_id = $_REQUEST['author_id'];
        } else {
            $author_id = "";
        }
        if (isset($_REQUEST['track_type'])) {
            $track_type = $_REQUEST['track_type'];
        } else {
            $track_type = "";
        }
        
        if(isset($_REQUEST['duration_m'])){
            $minute = $_REQUEST['duration_m'];
        }else{
            $minute = "";
        }
        
        if(isset($_REQUEST['duration_s'])){
            $second = $_REQUEST['duration_s'];
        }else{
            $second= "";
        }
        
        $arr_duration = array ($minute, $second);
        $duration = implode(":",$arr_duration);

        if (isset($_REQUEST['track_id'])) {
            $track_id = $_REQUEST['track_id'];
        } else {
            $track_id = "";
        }

        if (isset($_REQUEST['asmt_url'])) {
            $asmt_url = $_REQUEST['asmt_url'];
        } else {
            $asmt_url = "";
        }

        if (isset($_REQUEST['video_url'])) {
            $video_url = $_REQUEST['video_url'];
        } else {
            $video_url = "";
        }

        if (isset($_REQUEST['journal_url'])) {
            $journal_url = $_REQUEST['journal_url'];
        } else {
            $journal_url = "";
        }

        if (isset($_REQUEST['pstr_url'])) {
            $poster_url = $_REQUEST['pstr_url'];
        } else {
            $pstr_url = "";
        }


        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->beginTransaction();

        try {
            if($type == 'VDEO'){
                
            $stmt10 = $dbh->prepare("UPDATE  eap_content_vdeo SET body=?, video_url=? WHERE asset_id = ?");
            $stmt10->execute(array($body, $video_url, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_10");
            
            }else if($type == 'BLOG'){
                
            $stmt10 = $dbh->prepare("UPDATE  eap_content_blog SET author_id =?, body=? WHERE asset_id=?");
            $stmt10->execute(array($author_id, $body, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_11");
            
            }else if($type == 'JRNL'){
                
            $stmt10 = $dbh->prepare("UPDATE  eap_content_jrnl SET body=?, img=?, download_path=? WHERE asset_id = ?");
            $stmt10->execute(array($body,"", $journal_url, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_12");
            
            }else if($type == 'TRCK'){
                
            $stmt10 = $dbh->prepare("UPDATE  eap_content_trck SET track_id=?, body=?, type=?, duration=? WHERE asset_id = ?");
            $stmt10->execute(array($track_id, $body, $track_type, $duration, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_10");
            
            }else if($type == 'PSTR'){
                
            $stmt10 = $dbh->prepare("UPDATE  eap_content_pstr SET body=?, img=?, download_path=? WHERE asset_id = ?");
            $stmt10->execute(array($body, "", $pstr_url, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_10");
            
            }else if($type == 'ASMT'){
                
            $stmt10 = $dbh->prepare("UPDATE  eap_content_asmt SET  body=?, path=? WHERE asset_id = ?");
            $stmt10->execute(array($asset_id, $author_id, $body, " ", " ")) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_10");
            }
            
            $stmt11 = $dbh->prepare("UPDATE  eap_assets_all SET heading =?, descn=?, preview_img=?, type=?, low=?, medium=?, high=? WHERE asset_id=?");
            $stmt11->execute(array($heading, $description, "", $type, $low, $medium, $high, $asset_id)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : UC_11");
            
            $dbh->commit();
            $status_code = 1;
        } catch (PDOException $e) {
            $dbh->rollBack();
            echo $e->getMessage();
            die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code : UC_12");
        }
    }else{
         $status_code = 2; 
        }
        $location = 'Location: ' . $host . '/modules/ewap_content/update_content.php?type=' . $type . '&asset_id=' . $asset_id . '&status_code=' . $status_code;
        header($location);
        exit();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/summernote/summernote.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <style>
            .error{
                color:red;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
            <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider"><b>Update Asset</b></div>
                                    <?php
									#displaying success or failure messages
                                    if ($status_code != 0) {
                                        switch ($status_code) {

                                            case 1:
                                                echo '<div class="alert alert-success" style="text-align:center;">';
                                                echo "<strong>Asset updated successfully.</strong>";
                                                echo '</div>';
                                                break;
                                            case 2:
                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                echo "<strong>Some of the fields are empty.</strong>";
                                                echo '</div>';
                                                break;

                                            default: break;
                                        }
                                    }
                                    ?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <form class="form-horizontal" role="form" onsubmit="return validate(this);" method="POST">
                                                    <div class="form-group">
                                                        <div class ="row">
                                                            <div class="col-md-1">
                                                                &nbsp;
                                                            </div>
                                                            <label class="col-md-1" style="margin-left: 2%;margin-top:1.2%;">Type <span style="color:red;">*</span></label>
                                                            
                                                            <?php if($type == 'VDEO'){ ?>
                                                            <div class="form-group col-md-1" style="text-align:right;">
                                                                <input id="H1" name="type" type="radio" value="VDEO"<?php echo ($type == 'VDEO') ? "checked" : "" ?>>
                                                                <label for="H0" style="font-weight:400;">
                                                                    Video
                                                                </label>
                                                            </div><?php }else{?> 
                                                                <div class="form-group col-md-1" style="text-align:right;">
                                                                <input id="H1" name="type" type="radio" value="VDEO" disabled>
                                                                <label for="H0" style="font-weight:400;">
                                                                    Video
                                                                </label>
                                                            </div>
                                                            <?php }?>
                                                                
                                                            <?php if($type == 'BLOG'){ ?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H1" name="type" type="radio" value="BLOG"<?php echo ($type == 'BLOG') ? "checked" : "" ?>>
                                                                <label for="H1" style="font-weight:400;">
                                                                    Blog
                                                                </label>
                                                            </div>
                                                            <?php }else{?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H1" name="type" type="radio" value="BLOG" disabled>
                                                                <label for="H1" style="font-weight:400;">
                                                                    Blog
                                                                </label>
                                                            </div>
                                                            <?php }?>
                                                            
                                                            <?php if($type == 'PSTR'){ ?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H2" name="type" type="radio" value="PSTR"<?php echo ($type == 'PSTR') ? "checked" : "" ?>>
                                                                <label for="H2" style="font-weight:400;">
                                                                    Poster
                                                                </label>
                                                            </div>
                                                            <?php }else{ ?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H2" name="type" type="radio" value="PSTR" disabled>
                                                                <label for="H2" style="font-weight:400;">
                                                                    Poster
                                                                </label>
                                                            </div>
                                                            <?php } ?>
                                                            
                                                            <?php if($type == 'JRNL'){ ?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H3" name="type" type="radio" value="JRNL"<?php echo ($type == 'JRNL') ? "checked" : "" ?>>
                                                                <label for="H3" style="font-weight:400;">
                                                                    Journals
                                                                </label>
                                                            </div>
                                                            <?php }else{?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:3%;">
                                                                <input id="H3" name="type" type="radio" value="JRNL" disabled>
                                                                <label for="H3" style="font-weight:400;">
                                                                    Journals
                                                                </label>
                                                            </div>
                                                            <?php } ?>
                                                            
                                                            <?php if($type == 'ASMT'){ ?>
                                                            <div class="form-group col-md-2" style="text-align:center;margin-left:3%;">
                                                                <input id="H4" name="type" type="radio" value="ASMT"<?php echo ($type == 'ASMT') ? "checked" : "" ?>>
                                                                <label for="H4" style="font-weight:400;">
                                                                    Assessment
                                                                </label>
                                                            </div>
                                                            <?php }else{ ?>
                                                            <div class="form-group col-md-2" style="text-align:center;margin-left:3%;">
                                                                <input id="H4" name="type" type="radio" value="ASMT" disabled>
                                                                <label for="H4" style="font-weight:400;">
                                                                    Assessment
                                                                </label>
                                                            </div>
                                                            <?php } ?>
                                                            
                                                            <?php if($type == 'TRCK'){ ?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:2%;">
                                                                <input id="H5" name="type" type="radio" value="TRCK"<?php echo ($type == 'TRCK') ? "checked" : "" ?>>
                                                                <label for="H5" style="font-weight:400;">
                                                                    Track
                                                                </label>
                                                            </div>
                                                            <?php } else { ?>
                                                            <div class="form-group col-md-1" style="text-align:center;margin-left:2%;">
                                                                <input id="H5" name="type" type="radio" value="TRCK" disabled>
                                                                <label for="H5" style="font-weight:400;">
                                                                    Track
                                                                </label>
                                                            </div>
                                                            <?php }?>
                                                            
                                                            <?php if($type == 'MISC'){ ?>
                                                            <div class="form-group col-md-2" style="text-align:center;margin-left:2%;">
                                                                <input id="H6" name="type" type="radio" value="MISC"<?php echo ($type == 'MISC') ? "checked" : "" ?>>
                                                                <label for="H6" style="font-weight:400;">
                                                                    Miscellaneous
                                                                </label>
                                                            </div>
                                                            <?php } else { ?>
                                                            <div class="form-group col-md-2" style="text-align:center;margin-left:2%;">
                                                                <input id="H6" name="type" type="radio" value="MISC" disabled>
                                                                <label for="H6" style="font-weight:400;">
                                                                    Miscellaneous
                                                                </label>
                                                            </div>
                                                            <?php }?>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-1%;">
                                                        <label class="col-md-2 control-label" style="margin-top:-1.2%;">Priority <span style="color:red;">*</span>
                                                        </label>

                                                        <div class="col-md-10">
                                                            
                                                            <div class="col-md-2" style="margin-left:-1%;">
                                                                <input id="priority1" name="options[]" type="checkbox" value="LOW"<?php echo ($low == '1') ? "checked" : "" ?>>
                                                                <label for="priority1">
                                                                    Low
                                                                </label>
                                                            </div>
                                                            
                                                            <div class="col-md-2" style="margin-left:-7%;">
                                                                <input id="priority2" name="options[]" type="checkbox" value="MED"<?php echo ($medium == '1') ? "checked" : "" ?>>
                                                                <label for="priority2">
                                                                    Medium
                                                                </label>
                                                            </div>
                                                                
                                                            <div class="col-md-2"style="margin-left:-5%;">
                                                                <input id="priority3" name="options[]" type="checkbox" value="HIG"<?php echo ($high == '1') ? "checked" : "" ?>>
                                                                <label for="priority3">
                                                                    High
                                                                </label>
                                                            </div>     
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                &nbsp;
                                                            </div>
                                                            <div id="check_err" class="error col-md-9" style="font-size:13px;margin-left:1%;">
                                                                    <!--message is displayed for validation-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-1.5%;">
                                                        <label class="col-md-2 control-label">Heading <span style="color:red;">*</span>
                                                        </label>

                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control input-sm" name="asmt_heading" placeholder="Heading" value="<?php echo $heading; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="margin-top:-1%;">
                                                        <label class="col-md-2 control-label">Description <span style="color:red;">*</span>
                                                        </label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control input-sm" name="asmt_desc" placeholder="Description" value="<?php echo $description; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style="margin-top:-1.5%;">
                                                        <label class="col-md-2 control-label">Body <span style="color:red;">*</span>
                                                        </label>
                                                        <div class="panel-body col-md-10" >
                                                            <textarea id="editor1" type="text" name="asmt_body" value="" style="border:none; background: transparent; outline: 0;"/><?php if($type == 'MISC'){echo "";}else{echo $body_fetched;} ?></textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group" style="margin-top:-2.5%;">
                                                        <label class="col-md-2 control-label">Upload Pic<span style="color:red;">*</span>
                                                        </label>
                                                        <div class="panel-body col-md-10" >
                                                            <input type="file" id="file" name="asmt_upload"/>
                                                        </div>
                                                    </div>
                                                   
                                                   <?php if($type == "BLOG"){?> 
                                                   <div>
                                                        <div class="form-group" style="margin-top:-2.5%;">
                                                            <label class="col-md-2 control-label" for="pass1" style="margin-bottom:1%;">Author <span style="color:red;">*</span></label>

                                                            <div class="col-md-4" style="">
                                                                <select id="author_id" name="author_id" title="" class="form-control input-sm">
                                                                    <?php for ($j = 0; $j < count($author_id); $j++) { ?>
                                                                                <option value="<?php echo $author_id[$j]; ?>" <?php echo ($author_id_fetched== $author_id[$j])? "Selected": "" ?> ><?php echo $author_name[$j]; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <div id="err_yce" style="color:red"></div>
                                                            </div>
                                                        </div>	
                                                    </div>
                                                   <?php }else{?>
                                                    <!-- do nothing -->
                                                   <?php }?>
                                                    
                                                    <?php if($type=="JRNL"){?>
                                                    <div>
                                                        <div class="form-group" style="margin-top:-2.5%;">
                                                            <label class="col-md-2 control-label">Download Url <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" name="journal_url" value="<?php echo $jrnl_url; ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }else{?>
                                                    <!-- do nothing -->
                                                    <?php }?>
                                                    
                                                    <?php if($type=="VDEO"){ ?>
                                                    <div>
                                                        <div class="form-group" style="margin-top:-2.5%;">
                                                            <label class="col-md-2 control-label">Download Url <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" name="video_url" value="<?php echo $video_url; ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }else{?>
                                                    <!-- do nothing -->
                                                    <?php }?>

                                                    <?php if($type =="ASMT"){?>
                                                    <div>
                                                        <div class="form-group" style="margin-top:-2.5%;">
                                                            <label class="col-md-2 control-label">Assessment Link <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" name="asmt_url" value="<?php echo $asmt_url; ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }else{?>
                                                    <!-- do nothing -->
                                                    <?php }?>
                                                    
                                                    <?php if($type == "PSTR"){?>
                                                    <div>
                                                        <div class="form-group" style="margin-top:-2.5%;">
                                                            <label class="col-md-2 control-label">Download Link <span style="color:red;">*</span>
                                                            </label>

                                                            <div class="col-md-10">
                                                                <div>
                                                                    <input class="form-control input-sm" type="text" name="pstr_url" value="<?php echo $pstr_url; ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }else{?>
                                                    <!--do nothing -->
                                                    <?php }?>
                                                    
                                                    
                                                    <?php if($type == 'TRCK'){?>
                                                    <div>
                                                        <div class="form-group" style="margin-top:-2.5%;">
                                                            <label class="col-md-2 control-label">Track ID<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <div>
                                                                    <input type="text" class="form-control input-sm" name="track_id" placeholder="Track ID" value="<?php echo $track_id; ?>" style="width:109%;">
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">Track Type<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <select name="track_type" title="" class="form-control input-sm" >
                                                                    <option  value="GD" <?php echo ($track_type == 'GD') ? "selected" : "" ?>>Guided Tracks</option>
                                                                    <option  value="UN" <?php echo ($track_type == 'UN') ? "selected" : "" ?>>Unguided Tracks</option>
                                                                 </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-top:-0.7%;">
                                                            <label class="col-md-2 control-label">Duration<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-md-2">
                                                                <select name="duration_m" title="" class="form-control input-sm">
                                                                    <?php for($m=1; $m < 60; $m++){?>
                                                                    <option value="<?php echo $m; ?>" <?php echo ($dur_minute == $m)? "selected": "" ?> ><?php echo $m;?></option>
                                                                    <?php }?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <select name="duration_s" title="" class="form-control input-sm">
                                                                    <?php for($s=1; $s<=5; $s++){?>
                                                                       <option value="<?php echo $s*10; ?>" <?php echo ($dur_seconds == $s*10)? "selected": "" ?>><?php echo $s*10;?></option> 
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <?php }else{?>
                                                    <!-- do nothing -->
                                                    <?php }?>
                                            </div>
                                        </div>

                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-6 col-sm-9">
                                                <input type="hidden" name="asset_id" value="<?php echo $asset_id; ?>">
                                                <button type="submit" name="submit" class="btn btn-info waves-effect waves-light" name="btn-send-discount-link" style="margin-left:-4%;">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/js/main.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="../../../assets/lib/markdown-js/markdown.js" type="text/javascript"></script>
<script src="../../../assets/js/app-form-wysiwyg.js" type="text/javascript"></script>
<script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
        App.textEditors();
    });
</script>
<script>
    $('input[type="radio"]').click(function () {
        //alert($(this).attr('id'));
        if ($(this).attr('id') == 'VDEO') {
            $('#H0_div').show();
        } else {
            $('#H0_div').hide();
        }
        if ($(this).attr('id') == 'H1') {
            $('#H1_div').show();
        } else {
            $('#H1_div').hide();
        }
        if ($(this).attr('id') == 'H2') {
            $('#H2_div').show();
        } else {
            $('#H2_div').hide();
        }
        if ($(this).attr('id') == 'H3') {
            $('#H3_div').show();
        } else {
            $('#H3_div').hide();
        }
        if ($(this).attr('id') == 'H4') {
            $('#H4_div').show();
        } else {
            $('#H4_div').hide();
        }
        if ($(this).attr('id') == 'H5') {
            $('#H5_div').show();
        } else {
            $('#H5_div').hide();
        }

    });
</script>
<script>
    $(".form-horizontal").validate({
        rules: {
            asmt_type: {
                required: true
            },
            asmt_heading: {
                required: true
            },
            asmt_desc: {
                required: true
            },
            asmt_body: {
                required: true
            },
            journal_url: {
                required: true
            },
            video_url: {
                required: true
            },
            author_id: {
                required: true
            },
            track_id: {
                required: true
            },
            track_type: {
                required: true
            },
            asmt_url: {
                required: true
            },
            pstr_url: {
                required: true
            }
        },
        messages: {
            asmt_type: {
                required: "Please enter the type"
            },
            asmt_heading: {
                required: "Please enter the heading"
            },
            asmt_desc: {
                required: "Please enter the description"
            },
            asmt_body: {
                required: "Please enter the body"
            },
            journal_url: {
                required: "Please write the url here."
            },
            video_url: {
                required: "Please write the url here."
            },
            author_id: {
                required: "Please select one."
            },
            track_id: {
                required: "Please enter the track ID"
            },
            track_type: {
                required: "Please select one option."
            },
            asmt_url: {
                required: "Please select one option."
            },
            pstr_url: {
                required: "Please select one option."
            }
        }
    });
</script>
<script>
function validate(form) {
    //The key here is that you get all the "options[]" elements in an array
    var options = document.getElementsByName("options[]");
    
    if(options[0].checked==false && options[1].checked==false && options[2].checked==false) {
        document.getElementById("check_err").innerHTML = "Please check atleast one of the above option";
        return false;
    }
    return true;
}
</script>
</body>
</html>