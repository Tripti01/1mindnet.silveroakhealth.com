<?php
//include file
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'ewap-config.php';
//include 'newap-config.php';

if(isset($_REQUEST['status_code'])){
    $status_code = $_REQUEST['status_code'];
}else{
    $status_code = 0;
}


# Check the article_id that was passed onto this page from article_list and check if it is set if not redirect back to article_list page
if (isset($_REQUEST['article_id']) && !empty($_REQUEST['article_id'])) {
    $article_id = $_REQUEST['article_id'];
    
	$j = 0;
	$stmt11 = $dbh_ewap->prepare("SELECT * FROM author WHERE 1");
	$stmt11->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [ADD-NEW-CONT-1000].");
	if ($stmt11->rowCount() != 0) {
		while ($row11 = $stmt11->fetch(PDO::FETCH_ASSOC)) {
	# Get User Id from SQL results 
			$author_id[$j] = $row11['author_id'];
			$author_name[$j] = $row11['author_name'];
			$j++;
		}
	}
}

$n = 0;
$stmt12 = $dbh_ewap->prepare("SELECT * FROM article_category WHERE 1");
$stmt12->execute(array())or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode: [ADD-NEW-ARTC-1001].");
if ($stmt12->rowCount() != 0) {
    while ($row12 = $stmt12->fetch(PDO::FETCH_ASSOC)) {
# Get User Id from SQL results 
        $category_id[$n] = $row12['category_id'];
        $category_name[$n] = $row12['category_name'];
        $n++;
    }
}

    # Select the heading body and images from database for the appropriate article if it doesnt exist redirect to self_help
    $stmt01 = $dbh_ewap->prepare("SELECT article_content.heading,article_content.body,article_content.article_image,article_content.author_id,author.author_name, article_category.category_id, article_category.category_name FROM article_content,author,article_category WHERE article_id = ? and article_content.author_id = author.author_id and article_category.category_id = article_content.category_id LIMIT 1");
    $stmt01->execute(array($article_id));
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        $heading = $row01['heading'];
        $body = $row01['body'];
		
        $image_src = $row01['article_image'];
		#$image_path = new DOMXPath(@DOMDocument::loadHTML($image_src));
		#$image_header= $image_path->evaluate("string(//img/@src)");
		
		$author_id_fetched = $row01['author_id'];
		$category_id_fetched = $row01['category_id'];
	} 




# On form submission check if all the form fields are submitted
if (isset($_REQUEST['submit'])) {
    // Check if all the fields are set
     if (isset($_REQUEST['asmt_heading']) && !empty($_REQUEST['asmt_heading']) && $_REQUEST['asmt_heading'] !== '' && isset($_REQUEST['asmt_image']) && !empty($_REQUEST['asmt_image']) && $_REQUEST['asmt_image'] !== '' && isset($_REQUEST['asmt_body_1']) && !empty($_REQUEST['asmt_body_1']) && $_REQUEST['asmt_body_1'] !== '' && isset($_REQUEST['author_id']) && !empty($_REQUEST['author_id']) && $_REQUEST['author_id'] !== '' && isset($_REQUEST['category_id']) && !empty($_REQUEST['category_id']) && $_REQUEST['category_id'] !== '') {

        #heading
        $heading = $_REQUEST['asmt_heading'];
		
        #image
        $image_url = $_REQUEST['asmt_image'];
		
		$blog_image = '<img class="alignnone size-large wp-image-907" width="70%" height="80%" src="'.$image_url.'"/>';
		
		$article_id = $_REQUEST['article_id'];
		#body
        $body = $_REQUEST['asmt_body_1'];
		
		$xpath = new DOMXPath(@DOMDocument::loadHTML($body));
		$src = $xpath->evaluate("string(//img/@src)");
		
		if(isset($src) && $src != ''){
			$img = "<center><img src='$src' class='alignnone size-large wp-image-907' height='80%' width='70%' /></center>";
			preg_match_all('/<img[^>]+>/i',$body, $result); 
			$image_src_content = $result[0][0];
			$body = str_replace($image_src_content, $img, $body);

		}
		
        #category_id
        $category_id = $_REQUEST['category_id'];
        
        #author_id
        $author_id = $_REQUEST['author_id'];
        


        #inserting the data into blog content
        $stmt20 = $dbh_ewap->prepare("UPDATE article_content SET  category_id=?, heading=?, body = ?, article_image = ?, author_id = ? WHERE article_id = ?");
        $stmt20->execute(array($category_id, $heading, $body, $image_url, $author_id, $article_id));
        
        $status_code = 1;
    } else {
        $status_code = 2;
    }
    
        $location = 'Location: ' . $host .'/modules/ewap_content/update_article.php?status_code=' . $status_code.'&article_id='.$article_id;
        header($location);
        exit();
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>Update Article</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/summernote/summernote.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <style>
            .error{
                color:red;
            }
			.alignnone size-full wp-image-1371{
				height:80% !important;
				width: 70% !important;
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
<?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
							<p><a class="btn btn-primary" style ="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin-top:-1%;" href="article_list.php">Back</a></p>
                               <div class="panel panel-default panel-border-color panel-border-color-primary" style="margin-top:2.5%;">
								  <div class="panel-heading panel-heading-divider"><b>Update Article</b></div>
                                    <?php
									#displaying success or failure messages
                                    if ($status_code != 0) {
                                        switch ($status_code) {

                                            case 1:
                                                echo '<div class="alert alert-success" style="text-align:center;">';
                                                echo "<strong>Article Updated successfully.</strong>";
                                                echo '</div>';
                                                break;
                                            case 2:
                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                echo "<strong>Some of the fields are empty.</strong>";
                                                echo '</div>';
                                                break;

                                            default: break;
                                        }
                                    }
                                    ?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <form class="form-horizontal" role="form" onsubmit="return validate(this);" method="POST">
                                                    <div class="form-group">
                                                        <div class ="row">
                                                                                                                
                                                    <div class="form-group" style="margin-top:-1.5%;">
                                                        <label class="col-md-2 control-label">Blog Title <span style="color:red;">*</span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control input-sm" name="asmt_heading" value="<?php echo $heading; ?>">
                                                        </div>
                                                    </div>
                                                    
													<div class="form-group" style="margin-top:-1%;">
                                                        <label class="col-md-2 control-label">Blog Header Image <span style="color:red;">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control input-sm" name="asmt_image" value="<?php echo $image_src;?>">
                                                        </div>
                                                    </div>
													
                                                    <div class="form-group" style="margin-top:-1.5%;">
                                                        <label class="col-md-2 control-label">Blog Body <span style="color:red;">*</span>
                                                        </label>
                                                        <div class="panel-body col-md-9" >
                                                            <textarea id="editor1" type="text" name="asmt_body_1" style="border:none; background: transparent; outline: 0;"/><?php echo $body; ?></textarea>
                                                        </div>
                                                    </div>
													
													                                                 
                                                                                                      
                                                        <div class="form-group" style="">
                                                            <label class="col-md-2 control-label" for="pass1">Author <span style="color:red;">*</span></label>

                                                            <div class="col-md-4" style="">
                                                                <select id="author_id" name="author_id" title="" class="form-control  input-sm">
                                                                    <option value="" disabled="" selected="">  -- SELECT -- </option> 
                                                                        <?php for ($j = 0; $j < count($author_id); $j++) { ?>
                                                                                <option value="<?php echo $author_id[$j]; ?>" <?php echo ($author_id_fetched== $author_id[$j])? "Selected": "" ?>><?php echo $author_name[$j]; ?></option>
                                                                        <?php } ?>
                                                                </select>
                                                                <div id="err_yce" style="color:red"></div>
                                                            </div>
                                                        </div>	
                                                    
                                                        <div class="form-group" style="">
                                                            <label class="col-md-2 control-label" for="pass1">Category <span style="color:red;">*</span></label>

                                                            <div class="col-md-4" style="">
                                                                <select id="category_id" name="category_id" title="" class="form-control  input-sm">
                                                                    <option value="" disabled="" selected="">  -- SELECT -- </option> 
                                                                        <?php for ($m = 0; $m < count($category_id); $m++) { ?>
                                                                                <option value="<?php echo $category_id[$m]; ?>" <?php echo ($category_id_fetched == $category_id[$m])? "selected": "" ?>><?php echo $category_name[$m]; ?></option>
                                                                        <?php } ?>
                                                                </select>
                                                                <div id="err_yce" style="color:red"></div>
                                                            </div>
                                                        </div>	
                                                    </div>
                                                </div>

                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-6 col-sm-9">
											<input type="hidden" name="article_id" value="<?php echo $article_id;?>">
                                                <button type="submit" id="button" name="submit" class="btn btn-info waves-effect waves-light" name="btn-send-discount-link" style="margin-left:-4%;">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/js/main.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="../../../assets/lib/markdown-js/markdown.js" type="text/javascript"></script>
<script src="../../../assets/js/app-form-wysiwyg.js" type="text/javascript"></script>
<script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
        App.textEditors();
    });
</script>

<script>
    $(".form-horizontal").validate({
        rules: {
            asmt_heading: {
                required: true
            },
            asmt_image: {
                required: true
            },
            asmt_body: {
                required: true
            },
            author_id: {
                required: true
            },
            category_id: {
                required: true
            }
        },
        messages: {
            asmt_heading: {
                required: "Please enter the heading"
            },
            asmt_image: {
                required: "Please enter the image url."
            },
            asmt_body: {
                required: "Please enter the body"
            },
            author_id: {
                required: "Please select one option."
            },
            category_id: {
                required: "Please select one option."
            }
        }
    });
</script>
<script>
function validate(form) {
    //The key here is that you get all the "options[]" elements in an array
    var options = document.getElementsByName("options[]");
    
    if(options[0].checked==false && options[1].checked==false && options[2].checked==false) {
        document.getElementById("check_err").innerHTML = "Please check atleast one of the above option";
        return false;
    }
    return true;
}
</script>
<script>
 jQuery(function(){
$("#button").click(function(){
    var isChecked = jQuery("input[name=type]:checked").val();
     if(!isChecked){
         $("#err_type").html("Please enter type");
     }else{
         $("#err_type").html("");
     }
});
});
    </script>
	</body>
</html>