<?php
//incluing files
require '../if_loggedin.php';
include 'mindnet-config.php';

//to get current datetime 

$current_datetime = date('Y-m-d h:i:s');

//to check if status are set.if set then fetch details, else set as blank.
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = "";
}

//database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

#to fetch details of all employees who are active
$i = 0; //counter for employees
$stmt00 = $dbh->prepare("SELECT emp_login.emp_id,first_name,last_name,dept_name,meeting_datetime FROM emp_dept,dept_master,emp_login,emp_profile WHERE emp_dept.dept_id = dept_master.dept_id AND emp_login.emp_id=emp_dept.emp_id AND emp_login.emp_id=emp_profile.emp_id AND emp_login.active=? ORDER BY emp_login.emp_id");
$stmt00->execute(array('1'));
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $emp_id_list[$i] = $row00['emp_id']; //to fetch all emp_id
        $emp_name_list[$i] = $row00['first_name'] . ' ' . $row00['last_name']; //to fetch employee name
        $dept_list[$i] = $row00['dept_name']; //to fetch dept_name
        $meeting_list[$i] = $row00['meeting_datetime']; //to fetch the latest meeting date and time
        $i++;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"/>

        <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary panel-table">
                                <div class="panel-heading panel-heading-divider" style="padding-bottom: 15px;"><b>HR - View All Meetings List</b>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover" style="width:100%;" >
                                            <thead >
                                                <tr >
                                                    <th></th>
                                                    <th style="width:20%;">Employee Id</th>
                                                    <th style="width:25%;">Employee Name</th>
                                                    <th>Department Name</th>
                                                    <th>Meeting Date</th>
                                                    <th class="actions"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- to print all employee details in table-->
                                                <?php for ($i = 0; $i < count($emp_id_list); $i++) { ?>
                                                    <tr>
                                                        <td></td>
                                                        <td class="user-avatar"> <?php echo $emp_id_list[$i]; ?></td>
                                                        <td><?php echo $emp_name_list[$i]; ?></td>
                                                        <td><?php echo $dept_list[$i]; ?></td>
                                                        <td><?php
                                                            if (($meeting_list[$i] == '0000-00-00 00:00:00') || ($meeting_list[$i] == NULL)) {
                                                                echo "No meeting fixed.";
                                                            } else {
                                                                echo date('d-M-Y', strtotime($meeting_list[$i]));
                                                            }
                                                            ?>
                                                        </td>
                                                        <td class="actions"><button data-toggle="modal" data-target="#form-bp1_<?php echo $i; ?>" type="button" class="btn btn-space btn-primary">Edit</button></td>
                                                        <td>
                                                            <div id="form-bp1_<?php echo $i; ?>" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                                <div class="modal-dialog custom-width">
                                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="form_<?php echo $i; ?>" class="form-horizontal group-border-dashed basic-form">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                                                                                <h3 class="modal-title">Edit</h3>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 col-xs-3 control-label">Date</label>
                                                                                    <div class="col-md-7 col-xs-7">
                                                                                        <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                                                                                            <input size="16" type="text" value=" <?php
                                                                                            if (($meeting_list[$i] == '0000-00-00 00:00:00') || ($meeting_list[$i] == NULL)) {
                                                                                                echo " ";
                                                                                            } else {
                                                                                                echo date('d-M-Y', strtotime($meeting_list[$i]));
                                                                                            }
                                                                                            ?>" name="meeting_date_<?php echo $i; ?>" id="meeting_date_<?php echo $i; ?>" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                                <input type="submit" name="btn-submit<?php echo $i; ?>" value="Update"  class="btn btn-primary"/>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if (isset($_REQUEST['btn-submit' . $i])) {
                                                                $meeting_date = date("Y-m-d ", strtotime($_REQUEST['meeting_date_' . $i . '']));
                                                                $stmt03 = $dbh->prepare("UPDATE emp_profile set meeting_datetime =?  WHERE emp_id=? ");
                                                                $stmt03->execute(array($meeting_date, $emp_id_list[$i]));
                                                                echo '<script>
                                                          window.location.href = "' . $host . '/modules/hr_meeting.php";
                                                          </script>';
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                App.init();
                App.formElements();
            });
        </script>
        <script>
<?php for ($i = 0; $i < count($emp_id_list); $i++) { ?>
                $('#form_<?php echo $i; ?>').validate({
                    rules: {
                        meeting_date_<?php echo $i; ?>: {
                            required: true,
                        },
                        meeting_time_<?php echo $i; ?>: {
                            required: true,
                        }
                    },
                    messages: {
                        meeting_date_<?php echo $i; ?>: {
                            required: "Please Select a Date",
                        },
                        meeting_time_<?php echo $i; ?>: {
                            required: "Please Select a Time",
                        }
                    }
                });
<?php } ?>
        </script>
    </body>
</html>