<?php

//this function calculates the number of working days between start date to end date
function no_of_days($start, $end) {

    include 'mindnet-config.php';

    // to include end date
    $end->modify('+1 day'); //to get current year
    $cur_year = date("Y");

    $interval = $end->diff($start);

    // total days
    $days = $interval->days;

    // create an iterateable period of date (P1D equates to 1 day)
    $period = new DatePeriod($start, new DateInterval('P1D'), $end);

    #Start database connection
    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use mindnet");

    //to fetch list of holidays dates from database
    $holidays = array(); //intialize array
    $stmt01 = $dbh->prepare("SELECT holiday_date FROM soh_holiday_cal WHERE year=?");
    $stmt01->execute(array($cur_year));
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        if ($stmt01->rowCount() != 0) {
            $holidays[] = $row01['holiday_date']; //to fetch list of holiday dates in array
        } else {
            $holidays[] = '';
        }
    }

    foreach ($period as $dt) {

        $curr = $dt->format('D');

        // substract if Saturday or Sunday
        if ($curr == 'Sat' || $curr == 'Sun') {
            $days--;
        } else if (in_array($dt->format('Y-m-d'), $holidays)) {//else subtract if in list
            $days--;
        }
    }


    return $days; // return num of days
}

?>