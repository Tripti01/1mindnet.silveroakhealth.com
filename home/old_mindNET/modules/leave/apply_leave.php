<?php
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'no_of_days.php';

//counter initialization
$i = 0;
$leaves_taken = 0;
$count_el = 0;
$count_cl = 0;
$count_wp = 0;

#to set default dattime zone 
date_default_timezone_set("Asia/Kolkata");

//to get current year
$cur_year = date("Y");
#getting current month
$curr_month = date("m");
//to get current date
$cur_date = date("d");
$next_seven_date = date('M d, Y', strtotime("+7 day"));
$cur_timestamp = date("Y-m-d h:i:s");
$prev_year = date("Y", strtotime("-1 year"));

#if status is set in url,then assign value else set as 0
if (isset($_REQUEST['type'])) {
    $type_of_leave = $_REQUEST['type'];
} else {
    //redirect
    $location = 'Location: ' . $host . '/modules/leave/index.php';
    header($location);
}


#if status is set in url,then assign value else set as 0
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = 0;
}
#if tab status is set in url,then assign value else set as 0
if (isset($_REQUEST['tab_status'])) {
    $tab_status = $_REQUEST['tab_status'];
} else {
    $tab_status = 0;
}

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

//to fetch the el_constant, cl_constant and cf_constant
$stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant WHERE emp_id=? AND year=? LIMIT 2");
$stmt01->execute(array($emp_id, $cur_year));
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    #earned leaves
    $el_constant = $row01['el_constant'];
    #casual leaves
    $cl_constant = $row01['cl_constant'];
    #carried forward leaves
    $cf = $row01['cf'];
}

#casual leaves whole year
$num_of_cl = $cl_constant * 12;

#checking if curr_month is 1 or not
//if curr_month is 1, then check i date is greater than 15 or not
if ($curr_month == 1) {
    if ($cur_date >= 15) {
        //if date is greater than 15, assign prev_moth as curr_month, else set as 0
        $prev_month = $curr_month;
    } else {
        $prev_month = 0;
    }
} else {
    //if date is greater than 15, assign prev_moth as curr_month, else set prev_month as last month number
    if ($cur_date >= 15) {
        $prev_month = $curr_month;
    } else {
        $prev_month = date("m", strtotime("last month"));
    }
}

#no of total earned leaves
$num_of_el = $el_constant * $prev_month;

//to fetch the holidays taken over the period and store in an array
$stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
$stmt03->execute(array($emp_id, $cur_year, $cur_year));
if ($stmt03->rowCount() != 0) {
    while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
        $sno[$i] = $row03['sno']; //to fetch sno for calculation
        $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
        $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
        $type[$i] = $row03['type']; ////to fetch type of leave

        $status_leave[$i] = $row03['status']; //to fetch the status os leave
        $reason[$i] = $row03['reason']; //to fetch the reason of leave
        $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
        if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
            $leaves_taken = $leaves_taken + $no_of_days[$i];
            if ($type[$i] == "CL") {
                $count_cl = $count_cl + $no_of_days[$i]; //number of casual leaves taken
            } else if ($type[$i] == "EL") {
                $count_el = $count_el + $no_of_days[$i]; //number of earned leaves taken
            } else {
                $count_wp = $count_wp + $no_of_days[$i]; //number of without pay leaves taken
            }
        } else {
            $leaves_taken = $leaves_taken;
        }
        if ($status_leave[$i] == "W") {//only if status is waiting, it can be withdrwn,set enable_status for doing same
            $enable_edit[$i] = 1;
        } else {
            $enable_edit[$i] = 0;
        }
        $i++;
    }
} else {//no leaves taken
}

$total_earned_leaves = $num_of_el + $cf; //cf+el is total earned leaves
//total number of leaves left
$leaves_left = ($num_of_cl + $num_of_el + $cf) - $leaves_taken;
$leaves_left_cl = $num_of_cl - $count_cl; //number of cl left
$leaves_left_el = ($num_of_el + $cf) - $count_el; //number of el left
//on click of submit btn for half-day
if (isset($_REQUEST['submit_btn_half'])) {

    include 'mindnet-config.php';
    include '../../../mail/mail_pending_approval.php';

    //to get all details
    $emp_id = $_REQUEST['emp_id'];
    $el_left = $_REQUEST['el_leaves_left'];
    $cl_left = $_REQUEST['cl_leaves_left'];
    $cur_timestamp = $_REQUEST['cur_timestamp'];
    $to_date = date("Y-m-d", strtotime($_REQUEST['date_half_day']));
    $from_date = $to_date;
    $reason = $_REQUEST['reason_half_day'];
    $type_leave = $_REQUEST['type_of_leave_half_day'];
    $number_of_days = 0.5; //no of days for half day set as 0.5
    #Start database connection
    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use mindnet");


    if ($type_leave == "EL") {

        if ($el_left <= 1) {


            if ($number_of_days <= $el_left) {
                //to insert into database
                $stmt05 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
                $stmt05->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
                //set status to display msg
                $status = 1;
            } else {
                $status = 2; //leaves exceeded.
            }
        } else {

            $max_allowed = ceil($el_left / 2);

            if ($number_of_days <= $max_allowed) {
                //to insert into database
                $stmt05 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
                $stmt05->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
                //set status to display msg
                $status = 1;
            } else {
                $status = 2; //leaves exceeded.
            }
        }
    } else if ($type_leave == "CL") {
        if ($number_of_days <= $cl_left) {
            //to insert into database
            $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt06->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
            //set status to display msg
            $status = 1;
        } else {
            $status = 3; //leaves exceeded.
        }
    } else if ($type_leave == "WP") {
        //to insert into database
        $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
        $stmt06->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
        //set status to display msg
        $status = 1;
    } else {
        $status = 4;
    }
    if ($status == 1) {
        mail_approval($emp_id, $emp_name, date("d-M-Y", strtotime($_REQUEST['date_half_day'])), date("d-M-Y", strtotime($_REQUEST['date_half_day'])), $reason);
    }

    $location = 'Location: ' . $host . '/modules/leave/index.php?status=' . $status;
    header($location);
    exit();
}

//on click of submit btn for one-day
if (isset($_REQUEST['submit_btn_one'])) {

    #include files
    include '../../../mail/mail_pending_approval.php';

    //to get all details
    $emp_id = $_REQUEST['emp_id'];
    $to_date = date("Y-m-d", strtotime($_REQUEST['date_one_day']));
    $from_date = $to_date;
    $reason = $_REQUEST['reason_one_day'];
    $type_leave = $_REQUEST['type_of_leave_one_day'];
    $el_left = $_REQUEST['el_leaves_left'];
    $cl_left = $_REQUEST['cl_leaves_left'];
    $cur_timestamp = $_REQUEST['cur_timestamp'];
    $number_of_days = no_of_days(new DateTime($from_date), new DateTime($to_date)); //to get number of days
    #Start database connection
    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use mindnet");


    if ($type_leave == "EL") {

        if ($el_left <= 1) {

            if ($number_of_days <= $el_left) {
                if ($number_of_days <= $el_left) {
                    //to insert into database
                    $stmt05 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
                    $stmt05->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
                    //set status to display msg
                    $status = 1;
                } else {
                    $status = 2; //leaves exceeded.
                }
            } else {
                $status = 2; //leaves exceeded.
            }
        } else {

            $max_allowed = ceil($el_left / 2);
            if ($number_of_days <= $max_allowed) {
                //to insert into database
                $stmt05 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
                $stmt05->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
                //set status to display msg
                $status = 1;
            } else {
                $status = 2; //leaves exceeded.
            }
        }
    } else if ($type_leave == "CL") {
        if ($number_of_days <= $cl_left) {
            //to insert into database
            $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt06->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
            //set status to display msg
            $status = 1;
        } else {
            $status = 3; //leaves exceeded.
        }
    } else if ($type_leave == "WP") {
        //to insert into database
        $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
        $stmt06->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
        //set status to display msg
        $status = 1;
    } else {
        $status = 4;
    }

    if ($status == 1) {
        mail_approval($emp_id, $emp_name, date("d-M-Y", strtotime($_REQUEST['date_one_day'])), date("d-M-Y", strtotime($_REQUEST['date_one_day'])), $reason);
    }

    $location = 'Location: ' . $host . '/modules/leave/index.php?status=' . $status;
    header($location);
    exit();
}

if (isset($_REQUEST['submit_btn_multiple'])) {

    #include files
    include '../../../mail/mail_pending_approval.php';

    $emp_id = $_REQUEST['emp_id'];
    $to_date = date("Y-m-d", strtotime($_REQUEST['to_date_multiple_days']));
    $from_date = date("Y-m-d", strtotime($_REQUEST['from_date_multiple_days']));
    $reason = $_REQUEST['reason_multiple_days'];
    $type_leave = $_REQUEST['type_of_leave_multiple_days'];
    $el_left = $_REQUEST['el_leaves_left'];
    $cl_left = $_REQUEST['cl_leaves_left'];
    $cur_timestamp = $_REQUEST['cur_timestamp'];
    $number_of_days = no_of_days(new DateTime($from_date), new DateTime($to_date));
    #Start database connection
    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use mindnet");


    if ($type_leave == "EL") {

        $max_allowed = ceil($el_left / 2);
        if ($number_of_days <= $max_allowed) {
            //to insert into database
            $stmt05 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt05->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
            //set status to display msg
            $status = 1;
        } else {
            $status = 2; //leaves exceeded.
        }
    } else if ($type_leave == "CL") {
        if ($number_of_days <= $cl_left) {
            //to insert into database
            $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt06->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
            //set status to display msg
            $status = 1;
        } else {
            $status = 3; //leaves exceeded.
        }
    } else if ($type_leave == "WP") {
        //to insert into database
        $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
        $stmt06->execute(array('', $emp_id, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'W', $cur_timestamp, ''));
        //set status to display msg
        $status = 1;
    } else {
        $status = 4;
    }

    if ($status == 1) {
        mail_approval($emp_id, $emp_name, date("d-M-Y", strtotime($_REQUEST['from_date_multiple_days'])), date("d-M-Y", strtotime($_REQUEST['to_date_multiple_days'])), $reason);
    }
    $location = 'Location: ' . $host . '/modules/leave/index.php?status=' . $status;
    header($location);
}

//function to return name of type of leave from id
function type_name($id) {
    switch ($id) {
        case "EL":
            $name = "Earned Leave";
            break;
        case "CL":
            $name = "Casual Leave";
            break;
        case "WP":
            $name = "Leave Without Pay";
            break;

        default:
            $name = "";
            break;
    }
    return $name;
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/bootstrap-datepicker.min.css"/>
        <style>
            .pull-right {
                float: right!important;
            }
            .tab_custom{
                border: 1px solid transparent;
            }
            .tbl_heading{
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-weight: 700;
                color: #666666;
            }
            .tile{
                padding: 3% 2% 2% 3%;
                -webkit-box-shadow: 0px 0px 5px 5px rgba(159,209,245,1);
                -moz-box-shadow: 0px 0px 5px 5px rgba(159,209,245,1);
                box-shadow: 0px 0px 5px 5px rgba(159,209,245,1);
            }
            .tile_num{
                font-size: 25px;
                line-height: 25px;
            }
            .style-six {
                height: 12px;
                border: 0;
                box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);
            }
            .list_text_height{
                margin-top: 1%;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <!--Default Tabs-->
                        <div class="col-md-10 col-sm-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary">
                                <div class="panel-heading" style="z-index:0;"><b>Apply Leave</b></div>
                                <div class="tab-container" >
                                    <div class="nav tab_custom" style="z-index:1;margin-top: -5%;">

                                    </div>
                                    <div class="tab-content" >

                                        <!- Apply Leave  ->
                                        <div id="apply_lev" class="cont">

                                            <div class="row" style="margin-left:-0.5%;margin-right: 2%;margin-top: 4%;">
                                                <div class="col-md-1">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-10 col-xs-12 control-label" for="type"  style="text-align:left;">
                                                    You are applying for <b><?php echo type_name($type_of_leave); ?></b>.
                                                </label>
                                            </div> 

                                            <?php
                                            if ($type_of_leave == "EL") {
                                                echo '
                                                    <div class="row" style="margin-left:-0.5%;margin-right: 2%;margin-top: 0%;">
                                                    <div class="col-md-1">
                                                    &nbsp;
                                                    </div>
                                                    <div class="col-md-10 col-xs-12">
                                                    <ul>';

                                                if ($leaves_left_el <= 1) {
                                                    echo '
                                                        <li class="list_text_height">Total leaves available with you: <b>' . $leaves_left_el . '</b>. Maximum number of leaves that can be applied is <b>' . $leaves_left_el . ' *</b>.</li>
                                                        ';
                                                } else {
                                                    echo '
                                                        <li class="list_text_height">Total leaves available with you: <b>' . $leaves_left_el . '</b>. Maximum number of leaves that can be applied is <b>' . ceil($leaves_left_el / 2) . ' *</b>.</li>
                                                        ';
                                                }
                                                echo '
                                                    <li class="list_text_height">Earliest you can apply leave for is <b>' . $next_seven_date . '**</b>.</li>
                                                    </ul>
                                                    </div>
                                                    </div>
                                                    <div class="row" style="margin-top:2%;">
                                                <span style="margin-bottom:2%;margin-left:10.5%;">* You can only avail maximum 50% of the Earned Leaves.<span style="margin-left:5%;">** Earned Leaves need minimum 7 days of notice.</span></span>
                                            </div>
                                                    ';
                                            } else {
                                                //for any other type of leave, display nothing
                                            }
                                            ?> 



                                            <div class="row" style="margin-left:-0.5%;margin-right: 2%;margin-top: 3%;">
                                                <div class="col-md-1">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label" for="type"  style="text-align:left;">Apply Leave for </label>
                                                <div class="col-md-3 col-xs-12 radio-btn">
                                                    <input type="radio" name="type" id="half_day" value="half_day">
                                                    <label for="half_day" class="label-btn" style="font-weight:normal;">
                                                        Half Day
                                                    </label></div>
                                                <div class="col-md-3 col-xs-12 radio-btn">
                                                    <input type="radio" name="type" id="one_day" value="one_day" >
                                                    <label for="one_day" class="label-btn"style="font-weight:normal;" >
                                                        One Day
                                                    </label></div>
                                                <div class="col-md-3 col-xs-12 radio-btn">
                                                    <input type="radio" name="type" id="multiple_days" value="multiple_days">
                                                    <label for="multiple_days" class="label-btn" style="font-weight:normal;">
                                                        Multiple Days
                                                    </label></div>
                                            </div> 

                                            <div class='error'></div> 
                                            <hr class="style-six">
                                            <!-- apply half day leave -->
                                            <div id="half_day_div" style='display:none'>
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" name="form-horizontal-half" id="half" onsubmit='return test();'>
                                                    <div class="row" style="margin-top:2%;">
                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group" >
                                                                <div class="col-md-1 col-xs-1">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-md-2">
                                                                    <label class="control-label ">Date</label>
                                                                </div>
                                                                <div class="col-xs-12 col-md-7">
                                                                    <div class="input-group date datetimepicker col-sm-10">
                                                                        <input type="text" id="datepicker_half" data-date-format="dd-mm-yyyy" name="date_half_day" placeholder="Select date here " class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-12">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-md-2 col-xs-12">
                                                                    <label for="q4" class="control-label">Reason</label>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                    <select class="form-control" name="reason_half_day">
                                                                        <option value="Vacation">Vacation</option>
                                                                        <option value="Personal">Personal</option>
                                                                        <option value="Not Well">Not Well</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group text-right m-b-0" style="margin-right: 20px;margin-top:30px;padding-bottom:20px;">
                                                            <input type="hidden" name="type_of_leave_half_day" value="<?php echo $type_of_leave; ?>"/> 
                                                            <input type="hidden" name="cl_leaves_left" value="<?php echo $leaves_left_cl; ?>"/> 
                                                            <input type="hidden" name="el_leaves_left" value="<?php echo $leaves_left_el; ?>"/> 
                                                            <input type="hidden" name="cur_timestamp" value="<?php echo $cur_timestamp; ?>"/> 
                                                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>"/> 
                                                            <input type="submit" class="btn btn-primary" value="Apply" name="submit_btn_half" id="submit_btn">
                                                            <button type="reset" class="btn">Clear</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- Apply one day leave -->
                                            <div id="one_day_div" style='display:none'>
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="one" name="form-horizontal-one" onsubmit='return test();'>
                                                    <div class="row" style="margin-top:2%;">
                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-1 ">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-md-2">
                                                                    <label class="control-label ">Date</label>
                                                                </div>
                                                                <div class="col-xs-12 col-md-7">
                                                                    <div class="input-group date datetimepicker col-sm-10">
                                                                        <input type="text" id="datepicker_one" data-date-format="dd-mm-yyyy" name="date_one_day" placeholder="Select date here " class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-1">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-md-2 col-xs-12">
                                                                    <label for="q4" class="control-label">Reason</label>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                    <select class="form-control" name="reason_one_day">
                                                                        <option value="Vacation">Vacation</option>
                                                                        <option value="Personal">Personal</option>
                                                                        <option value="Not Well">Not Well</option>
                                                                    </select></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group text-right m-b-0" style="margin-right: 20px;margin-top:30px;padding-bottom:20px;">
                                                            <input type="hidden" name="type_of_leave_one_day" value="<?php echo $type_of_leave; ?>"/> 
                                                            <input type="hidden" name="cl_leaves_left" value="<?php echo $leaves_left_cl; ?>"/> 
                                                            <input type="hidden" name="el_leaves_left" value="<?php echo $leaves_left_el; ?>"/> 
                                                            <input type="hidden" name="cur_timestamp" value="<?php echo $cur_timestamp; ?>"/> 
                                                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>"/> 
                                                            <input type="submit" class="btn btn-primary" value="Apply" name="submit_btn_one" id="submit_btn">
                                                            <button type="reset" class="btn">Clear</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- Apply multiple days leave -->
                                            <div id="multiple_days_div" style='display:none'>
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="multiple" name="form-horizontal-multiple" onsubmit='return test();'>
                                                    <div class="row" style="margin-top:2%;">
                                                        <div class="row">
                                                            <div class="row" style="margin-left:3.7%;margin-right: 2%">
                                                                <div class="form-group">
                                                                    <div class="col-md-1 col-xs-1">
                                                                        &nbsp;
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-2">
                                                                        <label class="control-label ">From</label>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-7">
                                                                        <div class="input-group date datetimepicker col-sm-10">
                                                                            <input type="text" id="datepicker_multiple_from" data-date-format="dd-mm-yyyy" name="from_date_multiple_days" placeholder="Select date here " class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="margin-left:3.7%;margin-right: 2%">
                                                                <div class="form-group">
                                                                    <div class="col-md-1 col-xs-1">
                                                                        &nbsp;
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-2">
                                                                        <label class="control-label ">To</label>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-7">
                                                                        <div class="input-group date datetimepicker col-sm-10">
                                                                            <input type="text" id="datepicker_multiple_to" data-date-format="dd-mm-yyyy" name="to_date_multiple_days" placeholder="Select date here " class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="margin-left:3.7%;margin-right: 2%">
                                                                <div class="form-group">
                                                                    <div class="col-md-1 col-xs-1">
                                                                        &nbsp;
                                                                    </div>
                                                                    <div class="col-md-2 col-xs-12">
                                                                        <label for="q4" class="control-label">Reason</label>
                                                                    </div>
                                                                    <div class="col-md-6 col-xs-12">
                                                                        <select class="form-control" name="reason_multiple_days">
                                                                            <option value="Vacation">Vacation</option>
                                                                            <option value="Personal">Personal</option>
                                                                            <option value="Not Well">Not Well</option>
                                                                        </select></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group text-right m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                                                <input type="hidden" name="type_of_leave_multiple_days" value="<?php echo $type_of_leave; ?>"/> 
                                                                <input type="hidden" name="cl_leaves_left" value="<?php echo $leaves_left_cl; ?>"/> 
                                                                <input type="hidden" name="el_leaves_left" value="<?php echo $leaves_left_el; ?>"/> 
                                                                <input type="hidden" name="cur_timestamp" value="<?php echo $cur_timestamp; ?>"/> 
                                                                <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>"/> 
                                                                <input type="submit" class="btn btn-primary" value="Apply" name="submit_btn_multiple" id="submit_btn">
                                                                <button type="reset" class="btn" style="margin-right:7%;">Clear</button>
                                                            </div>
                                                        </div>
                                                </form>  
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../../assets/js/main.js" type="text/javascript"></script>
    <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
    <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
    <script src="../../../assets/lib/bootstrap-datepicker.min.js"></script>
    <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
    <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
    <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
    <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        //initialize the javascript
                                                        App.init();
                                                        App.formElements();
                                                    });

    </script>

    <script>
        var date = new Date();

<?php if ($type_of_leave == "EL") { ?>
            date.setDate(date.getDate() + 7);
            $('#datepicker_half').datepicker({
                startDate: date
            });
            $('#datepicker_one').datepicker({
                startDate: date
            });
            $('#datepicker_multiple_to').datepicker({
                startDate: date
            });
            $('#datepicker_multiple_from').datepicker({
                startDate: date
            });
<?php } else { ?>

            date.setDate(date.getDate());
            $('#datepicker_half').datepicker({
                startDate: '01-01-2017'
            });
            $('#datepicker_one').datepicker({
                startDate: '01-01-2017'
            });
            $('#datepicker_multiple_to').datepicker({
                startDate: '01-01-2017'
            });
            $('#datepicker_multiple_from').datepicker({
                startDate: '01-01-2017'
            });
<?php } ?>
    </script>
    <script>
        $('input[type="radio"]').click(function () {
            //alert($(this).attr('id'));
            if ($(this).attr('id') == 'half_day') {
                $('#half_day_div').show();
            } else {
                $('#half_day_div').hide();
            }
            if ($(this).attr('id') == 'one_day') {
                $('#one_day_div').show();
            } else {
                $('#one_day_div').hide();
            }
            if ($(this).attr('id') == 'multiple_days') {
                $('#multiple_days_div').show();
            } else {
                $('#multiple_days_div').hide();
            }
        });
    </script>
    <script>
        function validateRadio(obj, error_no) {
            var result = 0;
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].checked == true) {
                    result = 1;

                    break;
                }
            }
            return result;
        }

        function test() {
            var err = '';
            var q5 = document.getElementsByName("type");

            if (!validateRadio(q5, 5)) {
                $("#error5").addClass("red-error");
                err += '\n 2';
            }

            if (err.length) {
                $("div.error").html("Please select.");
                return false;
            } else {

                return true;
            }

        }

    </script>
    <script>
        $('#half').validate({
            rules: {
                date_half_day: {
                    required: true,
                },
                reason_half_day: {
                    required: true,
                }
            },
            messages: {
                date_half_day: {
                    required: "Please enter a date",
                },
                reason_half_day: {
                    required: "Please enter a reason",
                }
            }
        });
        $('#one').validate({
            rules: {
                date_one_day: {
                    required: true,
                },
                reason_one_day: {
                    required: true,
                }
            },
            messages: {
                date_one_day: {
                    required: "Please enter a date",
                },
                reason_one_day: {
                    required: "Please enter a reason",
                }
            }
        });
        $('#multiple').validate({
            rules: {
                to_date_multiple_days: {
                    required: true,
                },
                from_date_multiple_days: {
                    required: true,
                },
                reason_multiple_days: {
                    required: true,
                }
            },
            messages: {
                to_date_multiple_days: {
                    required: "Please enter a date",
                },
                from_date_multiple_days: {
                    required: "Please enter a date",
                },
                reason_multiple_days: {
                    required: "Please enter a reason",
                }
            }
        });
    </script>
</body>
</html> 