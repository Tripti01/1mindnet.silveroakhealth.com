<?php
//including in files and folders
require '../../if_loggedin.php';
include 'mindnet-config.php';
include 'mindnet-config.php';
include 'no_of_days.php';

//counter initialization
$i = 0;
#if status is set in url, then assign value, else set as 0
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = 0;
}

//to get current year
$cur_year = date("Y");

//to get current year
$cur_year = date("Y");
#getting current month
$curr_month = date("m");
//to get current date
$cur_date = date("d");
$prev_year = date("Y", strtotime("-1 year"));

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

//to fetch list of all reporting employee details
$j = 0; //counter to fetch all reporting employees
$stmt04 = $dbh->prepare("SELECT emp_login.first_name AS first_name,emp_login.last_name AS last_name, emp_login.emp_id AS emp_id FROM emp_login WHERE active = 1");
$stmt04->execute(array($emp_id));
if ($stmt04->rowCount() != 0) {
    while ($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)) {
        $emp_name_all[$j] = $row04['first_name'] . " " . $row04['last_name']; //to fetch all reporting emplyoees names
        $emp_id_all[$j] = $row04['emp_id']; //to fetch all reporting employee ids
        $j++;
    }
} else {//no employees 
}

//to get message for appropriate status
function leave_status($status_leave) {
    switch ($status_leave) {
        case "A" :
            $msg = "Approved";
            break;
        case "W" :
            $msg = "Waiting";
            break;
        case "R" :
            $msg = "Rejected";
            break;
        case "M" :
            $msg = "Manager Recorded";
            break;
        default:
            $msg = "";
            break;
    }

    return $msg;
}

//function to return name of type of leave from id
function type_name($id) {
    switch ($id) {
        case "EL":
            $name = "Earned Leave";
            break;
        case "CL":
            $name = "Casual Leave";
            break;
        case "WP":
            $name = "Leave Without Pay";
            break;

        default:
            $name = "";
            break;
    }
    return $name;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title> mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>
        <style>
            .pull-right {
                float: right!important;
            }
            .tab_custom{
                border: 1px solid transparent;
            }
            .tbl_heading{
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-weight: 700;
                color: #666666;
            }

        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <!--Default Tabs-->
                        <div class="col-md-10 col-xs-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary" >
                                <div class="panel-heading" style="z-index:0;"><b>HR - View All Employees Leaves</b></div>
                                <div class="tab-container" >
                                    <div class="nav tab_custom" style="z-index:1;margin-top: -1%;">
                                        <ul class="nav nav-tabs pull-right">
                                            <li id="lev_balance_tab" class="active"><a href="#leave_log_2018" data-toggle="tab"><?php echo $cur_year; ?></a></li>
                                            <li id="lev_policy_tab"><a href="#leave_log_2017" data-toggle="tab"><?php echo $prev_year; ?></a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content" >
                                        <!--Leave Log - 2017 -->
                                        <div id="leave_log_2018" class="tab-pane active cont">
                                            <div class="row">



                                                <?php
                                                if (isset($emp_id_all)) {
                                                    //counter to fetch all emp assigned
                                                    for ($l = 0; $l < count($emp_id_all); $l++) {
                                                        $i = 0; //counter for each emp_id
                                                        $leaves_taken = 0; //set as 0 for each emp_id
                                                        //        //counter initialization
                                                        $leaves_taken = 0;
                                                        $count_el = 0;
                                                        $count_cl = 0;
                                                        $count_wp = 0;
                                                        $num_of_el = 0;
                                                        $num_of_cl = 0;
                                                        //to fetch the holidays taken over the period and store in an array for the emp-id
                                                        $stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
                                                        $stmt03->execute(array($emp_id_all[$l], $cur_year, $cur_year));
                                                        if ($stmt03->rowCount() != 0) {
                                                            while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
                                                                $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
                                                                $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
                                                                $type[$i] = $row03['type']; ////to fetch type of leave

                                                                $status_leave[$i] = $row03['status']; //to fetch the status os leave
                                                                $reason[$i] = $row03['reason']; //to fetch the reason of leave
                                                                $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
                                                                if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                    $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                    if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                        $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                        if ($type[$i] == "CL") {
                                                                            $count_cl = $count_cl + $no_of_days[$i]; //number of casual leaves taken
                                                                        } else if ($type[$i] == "EL") {
                                                                            $count_el = $count_el + $no_of_days[$i]; //number of earned leaves taken
                                                                        } else {
                                                                            $count_wp = $count_wp + $no_of_days[$i]; //number of without pay leaves taken
                                                                        }
                                                                    } else {
                                                                        $leaves_taken = $leaves_taken;
                                                                    }
                                                                } else {
                                                                    $leaves_taken = $leaves_taken;
                                                                }
                                                                $i++;
                                                            }
                                                        }
                                                        //to fetch the el_constant, cl_constant and cf_constant for the given emp_id

                                                        $stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant WHERE emp_id=? AND year=? LIMIT 2");
                                                        $stmt01->execute(array($emp_id_all[$l], $cur_year));
                                                        if ($stmt01->rowCount() != 0) {
                                                            $row01 = $stmt01->fetch();
                                                            #earned leaves
                                                            $el_constant = $row01['el_constant'];
                                                            #casual leaves
                                                            $cl_constant = $row01['cl_constant'];
                                                            #carried forward leaves
                                                            $cf = $row01['cf'];
                                                        }

#casual leaves whole year
                                                        $num_of_cl = $cl_constant * 12;

#checking if curr_month is 1 or not
//if curr_month is 1, then check i date is greater than 15 or not
                                                        if ($curr_month == 1) {
                                                            if ($cur_date >= 15) {
                                                                //if date is greater than 15, assign prev_moth as curr_month, else set as 0
                                                                $prev_month = $curr_month;
                                                            } else {
                                                                $prev_month = 0;
                                                            }
                                                        } else {
                                                            //if date is greater than 15, assign prev_moth as curr_month, else set prev_month as last month number
                                                            if ($cur_date >= 15) {
                                                                $prev_month = $curr_month;
                                                            } else {
                                                                $prev_month = date("m", strtotime("last month"));
                                                            }
                                                        }

#no of total earned leaves
                                                        $num_of_el = ($el_constant * $prev_month) + $cf;
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-sm-1"></div>
                                                            <div class="col-sm-10 col-xs-12">
                                                                <div class="panel panel-default panel-table" style="padding-left: 15px;padding-right: 15px;">
                                                                    <div class="panel-body">
                                                                        <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:-1%;margin-top:2%;"><b><?php echo $emp_name_all[$l]; ?></b><span style="font-size:15px;"> ( Earned Leave - <?php echo $count_el . ' / ' . $num_of_el; ?>, Casual Leave - <?php echo $count_cl . ' / ' . $num_of_cl; ?> )</span></h4>
                                                                        <hr class="style-two"/>
                                                                        <div class="table-responsive">
                                                                            <?php
                                                                            if ($stmt03->rowCount() != 0) {
                                                                                ?>
                                                                                <table class="table table-striped table-borderless" style="margin-top:-1%;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th >From</th>
                                                                                            <th >To</th>
                                                                                            <th>Type</th>
                                                                                            <th >Number of days</th>
                                                                                            <th >Reason</th>
                                                                                            <th >Status</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if (isset($leaves_taken_from)) {
                                                                                            for ($m = 0; $m < $stmt03->rowCount(); $m++) {
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><?php echo date('d-M-Y', strtotime($leaves_taken_from[$m])); ?></td>
                                                                                                    <td><?php echo date('d-M-Y', strtotime($leaves_taken_to[$m])); ?></td>
                                                                                                    <td><?php echo type_name($type[$m]); ?></td>
                                                                                                    <td><?php echo $no_of_days[$m]; ?></td>
                                                                                                    <td><?php echo $reason[$m]; ?></td>
                                                                                                    <td><?php echo leave_status($status_leave[$m]); ?></td>
                                                                                                </tr>

                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        echo "No leaves taken.";
                                                                                    }
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                                </table>
                                            </div>
                                        </div>  
                                        <!--Leave Log - 2017 -->
                                        <div id="leave_log_2017" class="tab-pane">
                                            <div class="row">
                                              <?php
                                                if (isset($emp_id_all)) {
                                                    //counter to fetch all emp assigned
                                                    for ($l = 0; $l < count($emp_id_all); $l++) {
                                                        $i = 0; //counter for each emp_id
                                                        $leaves_taken = 0; //set as 0 for each emp_id
                                                        //        //counter initialization
                                                        $leaves_taken = 0;
                                                        $count_el = 0;
                                                        $count_cl = 0;
                                                        $count_wp = 0;
                                                        $num_of_el = 0;
                                                        $num_of_cl = 0;
                                                        //to fetch the holidays taken over the period and store in an array for the emp-id
                                                        $stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
                                                        $stmt03->execute(array($emp_id_all[$l], $prev_year, $prev_year));
                                                        if ($stmt03->rowCount() != 0) {
                                                            while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
                                                                $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
                                                                $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
                                                                $type[$i] = $row03['type']; ////to fetch type of leave

                                                                $status_leave[$i] = $row03['status']; //to fetch the status os leave
                                                                $reason[$i] = $row03['reason']; //to fetch the reason of leave
                                                                $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
                                                                if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                    $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                    if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                        $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                        if ($type[$i] == "CL") {
                                                                            $count_cl = $count_cl + $no_of_days[$i]; //number of casual leaves taken
                                                                        } else if ($type[$i] == "EL") {
                                                                            $count_el = $count_el + $no_of_days[$i]; //number of earned leaves taken
                                                                        } else {
                                                                            $count_wp = $count_wp + $no_of_days[$i]; //number of without pay leaves taken
                                                                        }
                                                                    } else {
                                                                        $leaves_taken = $leaves_taken;
                                                                    }
                                                                } else {
                                                                    $leaves_taken = $leaves_taken;
                                                                }
                                                                $i++;
                                                            }
                                                        }
                                                        //to fetch the el_constant, cl_constant and cf_constant for the given emp_id

                                                        $stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant WHERE emp_id=? AND year=? LIMIT 2");
                                                        $stmt01->execute(array($emp_id_all[$l], $prev_year));
                                                        if ($stmt01->rowCount() != 0) {
                                                            $row01 = $stmt01->fetch();
                                                            #earned leaves
                                                            $el_constant = $row01['el_constant'];
                                                            #casual leaves
                                                            $cl_constant = $row01['cl_constant'];
                                                            #carried forward leaves
                                                            $cf = $row01['cf'];
                                                        }

#casual leaves whole year
                                                        $num_of_cl = $cl_constant * 12;

//since year has been completed , previous month will be "12"
                                                        $prev_month = 12;
#no of total earned leaves
                                                        $num_of_el = ($el_constant * $prev_month) + $cf;
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-sm-1"></div>
                                                            <div class="col-sm-10 col-xs-12">
                                                                <div class="panel panel-default panel-table" style="padding-left: 15px;padding-right: 15px;">
                                                                    <div class="panel-body">
                                                                        <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:-1%;margin-top:2%;"><b><?php echo $emp_name_all[$l]; ?></b><span style="font-size:15px;"> ( Earned Leave - <?php echo $count_el . ' / ' . $num_of_el; ?>, Casual Leave - <?php echo $count_cl . ' / ' . $num_of_cl; ?> )</span></h4>
                                                                        <hr class="style-two"/>
                                                                        <div class="table-responsive">
                                                                            <?php
                                                                            if ($stmt03->rowCount() != 0) {
                                                                                ?>
                                                                                <table class="table table-striped table-borderless" style="margin-top:-1%;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th >From</th>
                                                                                            <th >To</th>
                                                                                            <th>Type</th>
                                                                                            <th >Number of days</th>
                                                                                            <th >Reason</th>
                                                                                            <th >Status</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if (isset($leaves_taken_from)) {
                                                                                            for ($m = 0; $m < $stmt03->rowCount(); $m++) {
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><?php echo date('d-M-Y', strtotime($leaves_taken_from[$m])); ?></td>
                                                                                                    <td><?php echo date('d-M-Y', strtotime($leaves_taken_to[$m])); ?></td>
                                                                                                    <td><?php echo type_name($type[$m]); ?></td>
                                                                                                    <td><?php echo $no_of_days[$m]; ?></td>
                                                                                                    <td><?php echo $reason[$m]; ?></td>
                                                                                                    <td><?php echo leave_status($status_leave[$m]); ?></td>
                                                                                                </tr>

                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        echo "No leaves taken.";
                                                                                    }
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
            });
        </script>
    </body>
</html>