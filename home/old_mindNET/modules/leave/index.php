<?php
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'no_of_days.php';

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

$cur_year = date("Y");
#getting current month
$curr_month = date("m");
//to get current date
$cur_date = date("d");
$cur_timestamp = date("Y-m-d h:i:s");


//to fetch list of holidays
$j = 0; //counter to fetch list of holidays
$stmt04 = $dbh->prepare("SELECT * FROM soh_holiday_cal WHERE year=?");
$stmt04->execute(array($cur_year));
if ($stmt04->rowCount() != 0) {
    while ($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)) {
        $occasion[$j] = $row04['holiday_name']; //to fetch occasion
        $date_holiday[$j] = $row04['holiday_date']; //to fetch date
        $j++;
    }
} else {//no holidays
    //echo "No Holidays.";
}

/*
//counter initialization
$i = 0;
$leaves_taken = 0;
$count_el = 0;
$count_cl = 0;
$count_wp = 0;
$leaves_taken_2017 = 0;
$count_el_2017 = 0;
$count_cl_2017 = 0;
$count_wp_2017 = 0;

#to set default dattime zone
date_default_timezone_set("Asia/Kolkata");

//to get current year
$cur_year = date("Y");
#getting current month
$curr_month = date("m");
//to get current date
$cur_date = date("d");
$cur_timestamp = date("Y-m-d h:i:s");
$prev_year = date("Y", strtotime("-1 year"));


#if status is set in url,then assign value else set as 0
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = 0;
}
#if tab status is set in url,then assign value else set as 0
if (isset($_REQUEST['tab_status'])) {
    $tab_status = $_REQUEST['tab_status'];
} else {
    $tab_status = 0;
}

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

//to fetch the el_constant, cl_constant and cf_constant
$stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant WHERE emp_id=? AND year=? LIMIT 2");
$stmt01->execute(array($emp_id, $cur_year));
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    #earned leaves
    $el_constant = $row01['el_constant'];
    #casual leaves
    $cl_constant = $row01['cl_constant'];
    #carried forward leaves
    $cf = $row01['cf'];
}

#casual leaves whole year
$num_of_cl = $cl_constant * 12;

#checking if curr_month is 1 or not
//if curr_month is 1, then check i date is greater than 15 or not
if ($curr_month == 1) {
    if ($cur_date >= 15) {
        //if date is greater than 15, assign prev_moth as curr_month, else set as 0
        $prev_month = $curr_month;
    } else {
        $prev_month = 0;
    }
} else {
    //if date is greater than 15, assign prev_moth as curr_month, else set prev_month as last month number
    if ($cur_date >= 15) {
        $prev_month = $curr_month;
    } else {
        $prev_month = date("m", strtotime("last month"));
    }
}

#no of total earned leaves
$num_of_el = $el_constant * $prev_month;

//to fetch the holidays taken over the period and store in an array
$stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
$stmt03->execute(array($emp_id, $cur_year, $cur_year));
if ($stmt03->rowCount() != 0) {
    while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
        $sno[$i] = $row03['sno']; //to fetch sno for calculation
        $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
        $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
        $type[$i] = $row03['type']; ////to fetch type of leave

        $status_leave[$i] = $row03['status']; //to fetch the status os leave
        $reason[$i] = $row03['reason']; //to fetch the reason of leave
        $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
        if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
            $leaves_taken = $leaves_taken + $no_of_days[$i];
            if ($type[$i] == "CL") {
                $count_cl = $count_cl + $no_of_days[$i]; //number of casual leaves taken
            } else if ($type[$i] == "EL") {
                $count_el = $count_el + $no_of_days[$i]; //number of earned leaves taken
            } else {
                $count_wp = $count_wp + $no_of_days[$i]; //number of without pay leaves taken
            }
        } else {
            $leaves_taken = $leaves_taken;
        }
        if ($status_leave[$i] == "W") {//only if status is waiting, it can be withdrwn,set enable_status for doing same
            $enable_edit[$i] = 1;
        } else {
            $enable_edit[$i] = 0;
        }
        $i++;
    }
} else {//no leaves taken
}
$total_earned_leaves = $num_of_el + $cf; //cf+el is total earned leaves
//total number of leaves left
$leaves_left = ($num_of_cl + $num_of_el + $cf) - $leaves_taken;
$leaves_left_cl = $num_of_cl - $count_cl; //number of cl left
$leaves_left_el = ($num_of_el + $cf) - $count_el; //number of el left
//to fetch the holidays taken over last year - 2017 and store in an array
$ii = 0;
$stmt13 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
$stmt13->execute(array($emp_id, $prev_year, $prev_year));
if ($stmt13->rowCount() != 0) {
    while ($row13 = $stmt13->fetch(PDO::FETCH_ASSOC)) {
        $sno_2017[$ii] = $row13['sno']; //to fetch sno for calculation
        $leaves_taken_from_2017[$ii] = $row13['from_date']; //to fetch list of dates from date its taken
        $leaves_taken_to_2017[$ii] = $row13['to_date']; //to fetch list of dates to date its taken
        $type_2017[$ii] = $row13['type']; ////to fetch type of leave

        $status_leave_2017[$ii] = $row13['status']; //to fetch the status os leave
        $reason_2017[$ii] = $row13['reason']; //to fetch the reason of leave
        $no_of_days_2017[$ii] = $row13['no_of_days']; //to fetch number of days
        if ($status_leave_2017[$ii] == "A" || $status_leave_2017[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
            $leaves_taken_2017 = $leaves_taken_2017 + $no_of_days_2017[$ii];
            if ($type_2017[$ii] == "CL") {
                $count_cl_2017 = $count_cl_2017 + $no_of_days_2017[$ii]; //number of casual leaves taken
            } else if ($type_2017[$ii] == "EL") {
                $count_el_2017 = $count_el_2017 + $no_of_days_2017[$ii]; //number of earned leaves taken
            } else {
                $count_wp_2017 = $count_wp_2017 + $no_of_days_2017[$ii]; //number of without pay leaves taken
            }
        } else {
            $leaves_taken_2017 = $leaves_taken_2017;
        }
        if ($status_leave_2017[$ii] == "W") {//only if status is waiting, it can be withdrwn,set enable_status for doing same
            $enable_edit_2017[$ii] = 1;
        } else {
            $enable_edit_2017[$ii] = 0;
        }
        $ii++;
    }
} else {//no leaves taken
}

//to fetch list of holidays
$j = 0; //counter to fetch list of holidays
$stmt04 = $dbh->prepare("SELECT * FROM soh_holiday_cal WHERE year=?");
$stmt04->execute(array($cur_year));
if ($stmt04->rowCount() != 0) {
    while ($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)) {
        $occasion[$j] = $row04['holiday_name']; //to fetch occasion
        $date_holiday[$j] = $row04['holiday_date']; //to fetch date
        $j++;
    }
} else {//no holidays
    //echo "No Holidays.";
}
//to fetch list of holidays - previous year
$jj = 0; //counter to fetch list of holidays
$stmt14 = $dbh->prepare("SELECT * FROM soh_holiday_cal WHERE year=?");
$stmt14->execute(array($prev_year));
if ($stmt14->rowCount() != 0) {
    while ($row14 = $stmt14->fetch(PDO::FETCH_ASSOC)) {
        $occasion_prev_year[$jj] = $row14['holiday_name']; //to fetch occasion
        $date_holiday_prev_year[$jj] = $row14['holiday_date']; //to fetch date
        $jj++;
    }
} else {//no holidays
    // echo "No Holidays.";
}

//to get message for appropriate status
function leave_status($status_leave) {
    switch ($status_leave) {
        case "A" :
            $msg = "Approved";
            break;
        case "W" :
            $msg = "Waiting";
            break;
        case "R" :
            $msg = "Rejected";
            break;
        case "M" :
            $msg = "Manager Recorded";
            break;
        default:
            $msg = "";
            break;
    }

    return $msg;
}

//function to return name of type of leave from id
function type_name($id) {
    switch ($id) {
        case "EL":
            $name = "Earned Leave";
            break;
        case "CL":
            $name = "Casual Leave";
            break;
        case "WP":
            $name = "Leave Without Pay";
            break;

        default:
            $name = "";
            break;
    }
    return $name;
}
*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>
        <style>
            .pull-right {
                float: right!important;
            }
            .tab_custom{
                border: 1px solid transparent;
            }
            .tbl_heading{
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-weight: 700;
                color: #666666;
            }
            .tile{
                padding: 3% 2% 2% 3%;
                -webkit-box-shadow: 0px 0px 5px 5px rgba(159,209,245,1);
                -moz-box-shadow: 0px 0px 5px 5px rgba(159,209,245,1);
                box-shadow: 0px 0px 5px 5px rgba(159,209,245,1);
            }
            .tile_num{
                font-size: 25px;
                line-height: 25px;
            }
            .style-six {
                height: 12px;
                border: 0;
                box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);
            }
            hr.style-one {
                border: 0;
                height: 1px;
                background: #333;
                background-image: linear-gradient(to right, #ccc, #333, #ccc);
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <!--Default Tabs-->
                        <div class="col-md-8 col-sm-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary">
                                <div class="panel-heading" style="z-index:0;"><b> Holidays - <?php echo $cur_year; ?></b></div>
                                <div class="tab-container" >
                                    <div class="nav tab_custom" style="z-index:1;margin-top: -5%;">
                                        <ul class="nav nav-tabs pull-right">
                                            <!--  <li id="lev_balance_tab" class="active"><a href="#lev_balance" data-toggle="tab">Leave Balance</a></li> -->
                                            <!--li id="apply_lev_tab"><a href="#apply_lev" data-toggle="tab">Apply Leave</a></li-->
                                            <!-- <li id="lev_log_tab"><a href="#lev_log" data-toggle="tab">Leave Log</a></li> -->
                                       <!--     <li id="holiday_tab"><a href="#holiday" data-toggle="tab">Holiday</a></li>-->
                                            <!--<li id="lev_policy_tab"><a href="#lev_policy" data-toggle="tab">Leave Policy</a></li> -->
                                            <BR/>  <BR/>
                                        </ul>
                                    </div>
                                    <div class="tab-content" >
                                        <!--Leave Balance -->
                                        <!--<div id="lev_balance" class="tab-pane active cont">
                                            <?php
                                            if ($status == 1) {
                                                echo '<div class="alert alert-success">Successfully Applied. </div>';
                                            } else if ($status == 2) {
                                                echo '<div class="alert alert-danger">Leave Application not successful. You dont have these many Earned Leaves.</div>';
                                            } else if ($status == 3) {
                                                echo '<div class="alert alert-danger">Leave Apllication not successful. You dont have these many Casual Leaves. </div>';
                                            } else if ($status == 4) {
                                                echo '<div class="alert alert-danger">Type of leave is not valid</div>';
                                            } else {
                                                
                                            }
                                            ?>
                                            <div class="row">
                                                <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:2%;margin-left:1.5%;">Earned Leave</h4>
                                                <div class="col-xs-12 col-md-1"></div>
                                                <div class="col-xs-12 col-md-3" >
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num" style="font-size:14px;font-weight:bold;"><?php echo $count_el; ?></h1>
                                                        <span style="font-size:12px;">Availed</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3" >
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num" style="font-size:14px;font-weight:bold;"><?php echo $leaves_left_el; ?></h1>
                                                        <span style="font-size:12px;">Balance</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3" >
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num" style="font-size:14px;font-weight:bold;"><?php echo $total_earned_leaves; ?></h1>
                                                        <span style="font-size:12px;">Total</span><br/>
                                                        <span style="margin-bottom:2%;margin-left:1.5%;font-size:10px;">(This year <b><?php echo $num_of_el; ?></b> ; Carried Forward <b><?php echo $cf; ?></b>)</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-2" style="margin-top:1.5%;" >
                                                    <a href="apply_leave.php?type=EL"><input type="submit" class="btn btn-primary" style="padding:1% 8% 1% 8%;" value="Apply"></a>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row" style="margin-top:2%;">
                                                <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:2%;margin-left:1.5%;">Casual Leave</h4>
                                                <div class="col-xs-12 col-md-1"></div>
                                                <div class="col-xs-12 col-md-3" >
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num" style="font-size:14px;font-weight:bold;"><?php echo $count_cl; ?></h1>
                                                        <span style="font-size:12px;">Availed</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3" >
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num" style="font-size:14px;font-weight:bold;"><?php echo $leaves_left_cl; ?></h1>
                                                        <span style="font-size:12px;">Balance</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3">
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num" style="font-size:14px;font-weight:bold;"><?php echo $num_of_cl; ?></h1>
                                                        <span style="font-size:12px;">Total</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-2" style="margin-top:1.5%;">
                                                    <a href="apply_leave.php?type=CL"><input type="submit" class="btn btn-primary" style="padding:1% 8% 1% 8%;" value="Apply"></a>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row" style="margin-top:2%;padding-bottom:5%;">
                                                <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:2%;margin-left:1.5%;">Leave Without Pay</h4>
                                                <div class="col-xs-12 col-md-1"></div>
                                                <div class="col-xs-12 col-md-3" >
                                                    <div class="" style="text-align:center;">
                                                        <h1 class="header-title m-t-0 title_color tile_num"style="font-size:14px;font-weight:bold;"><?php echo $count_wp; ?></h1>
                                                        <span style="font-size:12px;">Availed</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3" >
                                                    &nbsp;
                                                </div>
                                                <div class="col-xs-12 col-md-3" >
                                                    &nbsp;
                                                </div>
                                                <div class="col-xs-12 col-md-2" style="margin-top:1.5%;">
                                                    <a href="apply_leave.php?type=WP"><input type="submit" class="btn btn-primary" style="padding:1% 8% 1% 8%;" value="Apply"></a>
                                                </div>


                                            </div>
                                        </div> -->

                                        <!-- Leave Log -->
                                        <!-- <div id="lev_log" class="tab-pane">
                                            <div class="tab-container" >
                                                <div class="nav tab_custom" style="z-index:1;margin-top: -1%;">
                                                    <ul class="nav nav-tabs pull-right">
                                                        <li id="lev_balance_tab" class="active"><a href="#leave_log_2018" data-toggle="tab"><?php echo $cur_year; ?></a></li>
                                                        <li id="lev_policy_tab"><a href="#leave_log_2017" data-toggle="tab"><?php echo $prev_year; ?></a></li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content" >
                                                    <!--Leave Log - 2017 -->
                                                  <!--  <div id="leave_log_2018" class="tab-pane active cont">
                                                        <div class="row">
                                                            <?php
                                                            if (isset($leaves_taken_from)) {
                                                                
                                                            } else {
                                                                echo "<div style='margin-left:1.5%;'>No Leaves Taken.</div>";
                                                            }
                                                            ?>
                                                            <div class="col-sm-12 col-xs-12">
                                                                <div class="panel panel-default panel-table" style="padding-left: 15px;padding-right: 15px;">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped table-borderless">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th >#</th>
                                                                                        <th >From</th>
                                                                                        <th >To</th>
                                                                                        <th>Type</th>
                                                                                        <th >Number of days</th>
                                                                                        <th >Reason</th>
                                                                                        <th >Status</th>
                                                                                        <th >Actions</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                    if (isset($leaves_taken_from)) {
                                                                                        for ($i = 0; $i < count($leaves_taken_from); $i++) {
                                                                                            $serial_no = $i + 1;
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?php echo $serial_no; ?></td>
                                                                                                <td><?php echo date('d-M-Y', strtotime($leaves_taken_from[$i])); ?></td>
                                                                                                <td><?php echo date('d-M-Y', strtotime($leaves_taken_to[$i])); ?></td>
                                                                                                <td><?php echo type_name($type[$i]); ?></td>
                                                                                                <td><?php echo $no_of_days[$i]; ?></td>
                                                                                                <td><?php echo $reason[$i]; ?></td>
                                                                                                <td><?php echo leave_status($status_leave[$i]); ?></td>

                                                                                                <?php
                                                                                                if ($enable_edit[$i] == 1) {
                                                                                                    echo '<td><a><button id="btn_del' . $i . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#del_leave_' . $i . '"  >Withdraw</button></a></td>';
                                                                                                } else {
                                                                                                    echo '<td></td>';
                                                                                                }
                                                                                                ?> 
                                                                                            </tr>
                                                                                        <div id='del_leave_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                                            <div class="modal-dialog modal-sm">
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="window.location.reload()">x</button>
                                                                                                        <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                    </div>
                                                                                                    <div class="modal-body">
                                                                                                        <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                                            Are you sure you want to Withdraw leave applied ?
                                                                                                            <div style="text-align:center;margin-top:10%;">
                                                                                                                <input type="hidden" name="sno" value="<?php echo $sno[$i]; ?>"/>
                                                                                                                <input type="submit"  name="confirm-del-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  >
                                                                                                                <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No" />
                                                                                                            </div>
                                                                                                        </form>
                                                                                                        <?php
                                                                                                        if (isset($_REQUEST["confirm-del-btn_$i"])) {
                                                                                                            $sno = $_REQUEST['sno'];
                                                                                                            $stmt600 = $dbh->prepare("DELETE FROM emp_leave WHERE sno=? LIMIT 1");
                                                                                                            $stmt600->execute(array($sno));
                                                                                                            $tab_status = 1;
                                                                                                            echo '<script>window.location.href="index.php?tab_status=1";</script>';
                                                                                                        }
                                                                                                        ?>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                 <!--   <div id="leave_log_2017" class="tab-pane">
                                                        <div class="row">
                                                            <?php
                                                            if (isset($leaves_taken_from_2017)) {
                                                                
                                                            } else {
                                                                echo "<div style='margin-left:1.5%;'>No Leaves Taken.</div>";
                                                            }
                                                            ?>
                                                            <div class="col-sm-12 col-xs-12">
                                                                <div class="panel panel-default panel-table" style="padding-left: 15px;padding-right: 15px;">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped table-borderless">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th >#</th>
                                                                                        <th >From</th>
                                                                                        <th >To</th>
                                                                                        <th>Type</th>
                                                                                        <th >Number of days</th>
                                                                                        <th >Reason</th>
                                                                                        <th >Status</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                    if (isset($leaves_taken_from_2017)) {
                                                                                        for ($i = 0; $i < count($leaves_taken_from_2017); $i++) {
                                                                                            $serial_no_2017 = $i + 1;
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?php echo $serial_no_2017; ?></td>
                                                                                                <td><?php echo date('d-M-Y', strtotime($leaves_taken_from_2017[$i])); ?></td>
                                                                                                <td><?php echo date('d-M-Y', strtotime($leaves_taken_to_2017[$i])); ?></td>
                                                                                                <td><?php echo type_name($type_2017[$i]); ?></td>
                                                                                                <td><?php echo $no_of_days_2017[$i]; ?></td>
                                                                                                <td><?php echo $reason_2017[$i]; ?></td>
                                                                                                <td><?php echo leave_status($status_leave_2017[$i]); ?></td>
                                                                                            </tr>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->

                                        <!-- Holiday List -->
                                        <div id="holiday" class="tab-pane active cont">
                                            <div class="tab-container" >
                                                <div class="nav tab_custom" style="z-index:1;margin-top: -1%;">
                                                    <ul class="nav nav-tabs pull-right">
                                                      
                                                    
                                                    </ul>
                                                </div>
                                                <div class="tab-content" >
                                                    <!--Leave Log - 2017 -->
                                                    <div id="holiday_2018" class="tab-pane active cont">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="panel panel-default panel-table">
                                                                    <div class="panel-body">
                                                                        <table class="table table-striped table-borderless">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Occasion</th>
                                                                                    <th>Day</th>
                                                                                    <th>Date</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                if (isset($occasion)) {
                                                                                    for ($i = 0; $i < count($occasion); $i++) {
                                                                                        $sno = $i + 1;
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td><?php echo $sno; ?></td>
                                                                                            <td><?php echo $occasion[$i]; ?></td>
                                                                                            <td><?php echo date("D", strtotime($date_holiday[$i])); ?></td>
                                                                                            <td><?php echo date('d-M-Y', strtotime($date_holiday[$i])); ?></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Leave Policy
                                        <div id="lev_policy" class="tab-pane">
                                            Coming Soon !!!
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
                                                                                                            $(document).ready(function () {
                                                                                                                //initialize the javascript
                                                                                                                App.init();
                                                                                                                App.formElements();
                                                                                                            });
        </script>
        <script>
<?php if ($tab_status == 1) { ?>
                $(document).ready(function () {
                    $("#lev_balance_tab").removeClass("active");
                    $("#lev_balance").removeClass("active");
                    $("#lev_log_tab").addClass("active");
                    $("#lev_log").addClass("active in");
                });
<?php } ?>
        </script>
        <script>
<?php if (($status == 1) || ($status == 2) || ($status == 3) || ($status == 4)) { ?>
                $(document).ready(function () {
                    $("#lev_balance_tab").addClass("active");
                    $("#lev_balance").addClass("active in");
                });
<?php } ?>
        </script>
        <script>
            $('input[type="radio"]').click(function () {
                //alert($(this).attr('id'));
                if ($(this).attr('id') == 'half_day') {
                    $('#half_day_div').show();
                } else {
                    $('#half_day_div').hide();
                }
                if ($(this).attr('id') == 'one_day') {
                    $('#one_day_div').show();
                } else {
                    $('#one_day_div').hide();
                }
                if ($(this).attr('id') == 'multiple_days') {
                    $('#multiple_days_div').show();
                } else {
                    $('#multiple_days_div').hide();
                }
            });
        </script>
        <script>
            function validateRadio(obj, error_no) {
                var result = 0;
                for (var i = 0; i < obj.length; i++) {
                    if (obj[i].checked == true) {
                        result = 1;

                        break;
                    }
                }
                return result;
            }

            function test() {
                var err = '';
                var q5 = document.getElementsByName("type");

                if (!validateRadio(q5, 5)) {
                    $("#error5").addClass("red-error");
                    err += '\n 2';
                }

                if (err.length) {
                    $("div.error").html("Please select.");
                    return false;
                } else {

                    return true;
                }

            }

        </script>
        <script>
            $('#half').validate({
                rules: {
                    type_of_leave_half_day: {
                        required: true,
                    },
                    date_half_day: {
                        required: true,
                    },
                    reason_half_day: {
                        required: true,
                    }
                },
                messages: {
                    type_of_leave_half_day: {
                        required: "Please enter a type",
                    },
                    date_half_day: {
                        required: "Please enter a date",
                    },
                    reason_half_day: {
                        required: "Please enter a reason",
                    }
                }
            });
            $('#one').validate({
                rules: {
                    type_of_leave_one_day: {
                        required: true,
                    },
                    date_one_day: {
                        required: true,
                    },
                    reason_one_day: {
                        required: true,
                    }
                },
                messages: {
                    type_of_leave_one_day: {
                        required: "Please enter a type",
                    },
                    date_one_day: {
                        required: "Please enter a date",
                    },
                    reason_one_day: {
                        required: "Please enter a reason",
                    }
                }
            });
            $('#multiple').validate({
                rules: {
                    type_of_leave_multiple_days: {
                        required: true,
                    },
                    to_date_multiple_days: {
                        required: true,
                    },
                    from_date_multiple_days: {
                        required: true,
                    },
                    reason_multiple_days: {
                        required: true,
                    }
                },
                messages: {
                    type_of_leave_multiple_days: {
                        required: "Please enter a type",
                    },
                    to_date_multiple_days: {
                        required: "Please enter a date",
                    },
                    from_date_multiple_days: {
                        required: "Please enter a date",
                    },
                    reason_multiple_days: {
                        required: "Please enter a reason",
                    }
                }
            });
        </script>
    </body>
</html>
