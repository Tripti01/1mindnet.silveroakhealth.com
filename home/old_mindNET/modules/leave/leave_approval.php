<?php
//including in files and folders
require '../../if_loggedin.php';
include 'mindnet-config.php';
include 'no_of_days.php';

//counter initialization
$i = 0;
#if status is set in url, then assign value, else set as 0
if (isset($_REQUEST['status'])) {
    $status = $_REQUEST['status'];
} else {
    $status = 0;
}

#to set default dattime zone 
date_default_timezone_set("Asia/Kolkata");
//to get current year
$cur_year = date("Y");
#getting current month
$curr_month = date("m");
//to get current date
$cur_date = date("d");
$cur_timestamp = date("Y-m-d h:i:s");
$prev_year = date("Y", strtotime("-1 year"));

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

//to fetch leave approval details according to prvlgs and dr_id
$stmt03 = $dbh->prepare("SELECT * FROM emp_leave,emp_dr,emp_login WHERE emp_login.emp_id = emp_dr.emp_id AND emp_leave.emp_id = emp_dr.emp_id AND emp_leave.status=? AND emp_dr.dr_of_id=?");
$stmt03->execute(array("W", $emp_id));
if ($stmt03->rowCount() != 0) {
    while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
        $sno[$i] = $row03['sno']; //to fetch sno for calculation
        $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
        $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
        $type[$i] = $row03['type']; ////to fetch type of leave
        $status_leave[$i] = $row03['status']; //to fetch the status os leave
        $reason[$i] = $row03['reason']; //to fetch the reason of leave
        $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
        $name[$i] = $row03['first_name'] . " " . $row03['last_name'];
        $email_list[$i] = $row03['email'];
        $i++;
    }
} else {//no leaves to approve
}


//to fetch list of all reporting employee details
$j = 0; //counter to fetch all reporting employees
$stmt04 = $dbh->prepare("SELECT emp_login.first_name AS first_name,emp_login.last_name AS last_name, emp_login.emp_id AS emp_id FROM emp_dr,emp_login WHERE emp_dr.emp_id = emp_login.emp_id AND emp_dr.dr_of_id=? AND emp_login.active=?");
$stmt04->execute(array($emp_id, "1"));
if ($stmt04->rowCount() != 0) {
    while ($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)) {
        $emp_name_all[$j] = $row04['first_name'] . " " . $row04['last_name']; //to fetch all reporting emplyoees names
        $emp_id_all[$j] = $row04['emp_id']; //to fetch all reporting employee ids
        $j++;
    }
} else {//no employees 
}



//on click of submit btn for half-day
if (isset($_REQUEST['submit_btn'])) {
#include files
    include 'mindnet-config.php';
    include '../../../mail/mail_approved_leave.php';


    //to get all details
    $emp_id_all = $_REQUEST['emp_id_lev'];
    $type = $_REQUEST['type'];
    $to_date = date("Y-m-d", strtotime($_REQUEST['to_date']));
    $from_date = date("Y-m-d", strtotime($_REQUEST['from_date']));
    $reason = $_REQUEST['reason'];
    $cur_timestamp = $_REQUEST['cur_timestamp'];
    $type_leave = $_REQUEST['type_of_leave'];
    if ($type == "half_day") {
        $number_of_days = 0.5; //no of days for half day set as 0.5
    } else {
        $number_of_days = no_of_days(new DateTime($from_date), new DateTime($to_date)); //to get number of days
    }

    //counter initialization
    $i = 0;
    $leaves_taken = 0;
    $count_el = 0;
    $count_cl = 0;
    $count_wp = 0;
    //to get current year
    $cur_year = date("Y");
    #getting current month
    $curr_month = date("m");
    //to get current date
    $cur_date = date("d");

    #Start database connection
    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use mindnet");

    //to fetch the el_constant, cl_constant and cf_constant and also name and email
    $stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant,emp_login WHERE emp_login.emp_id=emp_leave_constant.emp_id AND emp_login.emp_id=? AND year=? LIMIT 2");
    $stmt01->execute(array($emp_id_all, $cur_year));
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        #earned leaves
        $el_constant = $row01['el_constant'];
        #casual leaves
        $cl_constant = $row01['cl_constant'];
        #carried forward leaves
        $cf = $row01['cf'];
        $name_mail = $row01['first_name'] . " " . $row01['last_name'];
        $email_mail = $row01['email'];
    }

    #casual leaves whole year
    $num_of_cl = $cl_constant * 12;

    #checking if curr_month is 1 or not
    if ($curr_month == 1) {
        if ($cur_date >= 15) {
            $prev_month = $curr_month;
        } else {
            $prev_month = 0;
        }
    } else {
        if ($cur_date >= 15) {
            $prev_month = $curr_month;
        } else {
            $prev_month = date("m", strtotime("last month"));
        }
    }


    #no of total earned leaves
    $num_of_el = $el_constant * $prev_month;

    //to fetch the holidays taken over the period and store in an array
    $stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=?");
    $stmt03->execute(array($emp_id_all));
    if ($stmt03->rowCount() != 0) {
        while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
            $status_leave[$i] = $row03['status']; //to fetch the status os leave
            $no_of_days_taken[$i] = $row03['no_of_days']; //to fetch number of days
            $type_taken[$i] = $row03['type'];
            if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                $leaves_taken = $leaves_taken + $no_of_days_taken[$i];
                if ($type_taken[$i] == "CL") {
                    $count_cl = $count_cl + $no_of_days_taken[$i]; //number of casual leaves taken
                } else if ($type_taken[$i] == "EL") {
                    $count_el = $count_el + $no_of_days_taken[$i]; //number of earned leaves taken
                } else {
                    $count_wp = $count_wp + $no_of_days_taken[$i]; //number of earned leaves taken
                }
            } else {
                $leaves_taken = $leaves_taken;
            }
            $i++;
        }
    } else {//no leaves taken
    }
    $total_earned_leaves = $num_of_el + $cf; //cf+el is total earned leaves
    //total number of leaves left
    $leaves_left = ($num_of_cl + $num_of_el + $cf) - $leaves_taken;
    $cl_taken = $num_of_cl - $count_cl; //number of cl left
    $el_taken = ($num_of_el + $cf) - $count_el; //number of el left

    if ($type_leave == "EL") {
        if ($number_of_days <= $el_taken) {
            //to insert into database
            $stmt05 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt05->execute(array('', $emp_id_all, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'M', $cur_timestamp, $cur_timestamp));
            //set status to display msg
            $status = 1;
        } else {
            $status = 2; //leaves exceeded.
        }
    } else if ($type_leave == "CL") {
        if ($number_of_days <= $cl_taken) {
            //to insert into database
            $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt06->execute(array('', $emp_id_all, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'M', $cur_timestamp, $cur_timestamp));
            //set status to display msg
            $status = 1;
        } else {
            $status = 3; //leaves exceeded.
        }
    } else if ($type_leave == "WP") {
        //to insert into database
        $stmt06 = $dbh->prepare("INSERT INTO emp_leave VALUES (?,?,?,?,?,?,?,?,?,?)");
        $stmt06->execute(array('', $emp_id_all, $from_date, $to_date, $type_leave, $number_of_days, $reason, 'M', $cur_timestamp, $cur_timestamp));
        //set status to display msg
        $status = 1;
    } else {
        $status = 4;
    }

    if ($status == 1) {
        mail_manager_recorded($name_mail, $email_mail, date("d-M-Y", strtotime($_REQUEST['from_date'])), date("d-M-Y", strtotime($_REQUEST['to_date'])));
    }
    // $location = 'Location: ' . $host . '/modules/leave/leave_approval.php?status=' . $status;
    // header($location);
}

//to get message for appropriate status
function leave_status($status_leave) {
    switch ($status_leave) {
        case "A" :
            $msg = "Approved";
            break;
        case "W" :
            $msg = "Waiting";
            break;
        case "R" :
            $msg = "Rejected";
            break;
        case "M" :
            $msg = "Manager Recorded";
            break;
        default:
            $msg = "";
            break;
    }

    return $msg;
}

//function to return name of type of leave from id
function type_name($id) {
    switch ($id) {
        case "EL":
            $name = "Earned Leave";
            break;
        case "CL":
            $name = "Casual Leave";
            break;
        case "WP":
            $name = "Leave Without Pay";
            break;

        default:
            $name = "";
            break;
    }
    return $name;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title> mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>
        <style>
            .pull-right {
                float: right!important;
            }
            .tab_custom{
                border: 1px solid transparent;
            }
            .tbl_heading{
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-weight: 700;
                color: #666666;
            }

        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <!--Default Tabs-->
                        <div class="col-md-10 col-xs-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary">
                                <div class="panel-heading" style="z-index:0;"><b>Leave Approvals</b></div>
                                <div class="tab-container" >
                                    <div class="nav tab_custom" style="z-index:1;margin-top: -5%;">
                                        <ul class="nav nav-tabs pull-right">
                                            <li id="approval_tab" class="active"><a href="#approval" data-toggle="tab">Leave Approval</a></li>
                                            <li id="record_tab"><a href="#record" data-toggle="tab">Record A Leave</a></li>
                                            <li id="team_tab"><a href="#team" data-toggle="tab">Team Leave Log</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content" >
                                        <div id="approval" class="tab-pane active cont">
                                            <div class="row">
                                                <?php
                                                if (isset($leaves_taken_from)) {
                                                    
                                                } else {
                                                    echo '<div style="margin-left:1.5%;">No Leaves To Approve.</div>';
                                                }
                                                ?>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-11 col-xs-12">
                                                    <div class="panel panel-default panel-table">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-borderless">
                                                                    <thead>
                                                                        <tr>
                                                                            <th rowspan="2">#</th>
                                                                            <th>Name</th>
                                                                            <th rowspan="2">From</th>
                                                                            <th rowspan="2">To</th>
                                                                            <th rowspan="2">Type</th>
                                                                            <th rowspan="2">Number of days</th>
                                                                            <th rowspan="2">Reason</th>
                                                                            <th rowspan="2">Status</th>
                                                                            <th colspan="2" style="text-align:center;">Actions</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        if (isset($leaves_taken_from)) {
                                                                            for ($i = 0; $i < count($leaves_taken_from); $i++) {
                                                                                $serial_no = $i + 1;
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo $serial_no; ?></td>
                                                                                    <td><?php echo $name[$i]; ?></td>
                                                                                    <td><?php echo date('d-M-Y', strtotime($leaves_taken_from[$i])); ?></td>
                                                                                    <td><?php echo date('d-M-Y', strtotime($leaves_taken_to[$i])); ?></td>
                                                                                    <td><?php echo type_name($type[$i]); ?></td>
                                                                                    <td><?php echo $no_of_days[$i]; ?></td>
                                                                                    <td><?php echo $reason[$i]; ?></td>
                                                                                    <td><?php echo leave_status($status_leave[$i]); ?></td>
                                                                                    <?php
                                                                                    echo '<td><a><button id="btn_approve_' . $i . '" class="btn btn-success waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#approve_leave_' . $i . '" >Approve</button></a></td>';
                                                                                    echo '<td><a><button id="btn_reject_' . $i . '" class="btn btn-danger waves-effect waves-light btn-xs m-b-5" data-toggle="modal" data-target="#reject_leave_' . $i . '"  >Reject</button></a></td>';
                                                                                    ?>
                                                                                    </td>     
                                                                                </tr>
                                                                            <div id='reject_leave_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                                <div class="modal-dialog modal-sm">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="window.location.reload()">x</button>
                                                                                            <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                        </div>
                                                                                        <div>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                                Are you sure you want to Reject this leave ?
                                                                                                <div style="text-align:center;margin-top:10%;">
                                                                                                    <input type="hidden" name="sno" value="<?php echo $sno[$i]; ?>"/> 
                                                                                                    <input type="submit"  name="confirm-display-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                                    <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No" />
                                                                                                </div>
                                                                                            </form>
                                                                                            <?php
                                                                                            //to dismiss call just set display as 0
                                                                                            if (isset($_REQUEST["confirm-display-btn_$i"])) {
                                                                                                #include files
                                                                                                include '../../../mail/mail_approved_leave.php';
                                                                                                $sno = $_REQUEST['sno'];
                                                                                                $stmt600 = $dbh->prepare("UPDATE emp_leave SET status=?,approved_on=? WHERE sno=? LIMIT 1");
                                                                                                $stmt600->execute(array('R', $cur_timestamp, $sno));
                                                                                                mail_rejected_leaves($name[$i], $email_list[$i], date('d-M-Y', strtotime($leaves_taken_from[$i])), date('d-M-Y', strtotime($leaves_taken_to[$i])));
                                                                                                echo '<script>window.location.href="leave_approval.php";</script>';
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                    </div><!-- /.modal-content -->
                                                                                </div><!-- /.modal-dialog -->
                                                                            </div><!-- /.modal -->
                                                                            <div id='approve_leave_<?php echo $i; ?>' class="modal fade bs-example-modal-sm" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                                <div class="modal-dialog modal-sm">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"onClick="window.location.reload()">x</button>
                                                                                            <h4 class="modal-title" id="mySmallModalLabel">Confirm</h4>
                                                                                        </div>
                                                                                        <div>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>' method='POST'>
                                                                                                Are you sure you want to Approve this leave ?
                                                                                                <div style="text-align:center;margin-top:10%">
                                                                                                    <input type="hidden" name="sno" value="<?php echo $sno[$i]; ?>"/> 
                                                                                                    <input type="submit"  name="confirm-approve-btn_<?php echo $i; ?>" id="confirm-submit-btn" class="btn-success btn-sm m-b-5" value="YES"  > 
                                                                                                    <input type="button" class="btn-danger btn-sm m-b-5"  data-dismiss="modal" aria-hidden="true" value="No" />
                                                                                                </div>
                                                                                            </form>
                                                                                            <?php
                                                                                            //to dismiss call just set display as 0
                                                                                            if (isset($_REQUEST["confirm-approve-btn_$i"])) {
                                                                                                #include files
                                                                                                include '../../../mail/mail_approved_leave.php';
                                                                                                $sno = $_REQUEST['sno'];
                                                                                                $stmt700 = $dbh->prepare("UPDATE emp_leave SET status=?,approved_on=? WHERE sno=? LIMIT 1");
                                                                                                $stmt700->execute(array('A', $cur_timestamp, $sno));
                                                                                                mail_approved_leaves($name[$i], $email_list[$i], date('d-M-Y', strtotime($leaves_taken_from[$i])), date('d-M-Y', strtotime($leaves_taken_to[$i])));
                                                                                                echo '<script>window.location.href="leave_approval.php"</script>';
                                                                                            }
                                                                                            ?>
                                                                                        </div>
                                                                                    </div><!-- /.modal-content -->
                                                                                </div><!-- /.modal-dialog -->
                                                                            </div><!-- /.modal -->
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="record" class="tab-pane cont">
                                            <div class="row">
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" id="multiple" name="form-horizontal-multiple" onsubmit='return test();'>
                                                    <!--apply leave form--->
                                                    <?php
                                                    if ($status == 1) {
                                                        echo '<div class="alert alert-success">Leave has been successfully recorded.</div>';
                                                    } else if ($status == 2) {
                                                        echo '<div class="alert alert-danger">Earned Leaves Exceeded</div>';
                                                    } else if ($status == 3) {
                                                        echo '<div class="alert alert-danger">Casual Leaves Exceeded</div>';
                                                    } else if ($status == 4) {
                                                        echo '<div class="alert alert-danger">Type of leave is not valid</div>';
                                                    } else {
                                                        
                                                    }
                                                    ?> 
                                                    <div class="row" style="margin-left:0%;margin-right:0%">
                                                        <div class="form-group">
                                                            <div class="col-md-1 col-xs-1">
                                                                &nbsp;
                                                            </div>
                                                            <div class="col-md-3 col-xs-12">
                                                                <label for="q4" class="control-label" style="text-align:left;">Employee Name </label>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12">
                                                                <select class="form-control" name="emp_id_lev">
                                                                    <?php
                                                                    for ($i = 0; $i < count($emp_id_all); $i++) {
                                                                        echo '<option value = "' . $emp_id_all[$i] . '">' . $emp_name_all[$i] . '</option>';
                                                                    }
                                                                    ?>
                                                                </select></div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left:-1%;margin-right: 2%;margin-top: 2%">
                                                        <div class="col-md-1 col-xs-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 col-xs-12 control-label" for="type"  style="text-align:left;">Apply Leave for </label>

                                                        <div class="col-md-2 col-xs-12 radio-btn" style="margin-left:1%;">
                                                            <input type="radio" name="type" id="half_day" value="half_day">
                                                            <label for="half_day" class="label-btn" style="font-weight:normal;">
                                                                Half-A-Day
                                                            </label>
                                                        </div>

                                                        <div class="col-md-2 col-xs-12 radio-btn">
                                                            <input type="radio" name="type" id="one_day" value="one_day" >
                                                            <label for="one_day" class="label-btn"style="font-weight:normal;" >
                                                                One Day
                                                            </label></div>
                                                        <div class="col-md-2 col-xs-12 radio-btn">
                                                            <input type="radio" name="type" id="multiple_days" value="multiple_days">
                                                            <label for="multiple_days" class="label-btn" style="font-weight:normal;">
                                                                Multiple Days
                                                            </label></div>
                                                    </div> 

                                                    <div class='error' style='color:red;margin-left: 35%;'></div> 
                                                    <div class="row" style="margin-top:2%;">
                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-1">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-md-3">
                                                                    <label class="control-label ">From</label>
                                                                </div>
                                                                <div class="col-xs-12 col-md-7">
                                                                    <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker col-sm-10">
                                                                        <input type="text" name="from_date" placeholder="Select date here " class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-1">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-md-3">
                                                                    <label class="control-label ">To</label>
                                                                </div>
                                                                <div class="col-xs-12 col-md-7">
                                                                    <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker col-sm-10">
                                                                        <input type="text" name="to_date" placeholder="Select date here " class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-1">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-md-3">
                                                                    <label class="control-label ">Type</label>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <select class="form-control" name="type_of_leave">
                                                                        <option value="EL">Earned Leave</option>
                                                                        <option value="CL">Casual Leave</option>
                                                                        <option value="WP">Leave Without Pay</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-left:2%;margin-right: 2%">
                                                            <div class="form-group">
                                                                <div class="col-md-1 col-xs-1">
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-md-3 col-xs-12">
                                                                    <label for="q4" class="control-label">Reason </label>
                                                                </div>
                                                                <div class="col-md-6 col-xs-12">
                                                                    <select class="form-control" name="reason">
                                                                        <option value="Vacation">Vacation</option>
                                                                        <option value="Personal">Personal</option>
                                                                        <option value="Not Well">Not Well</option>
                                                                    </select></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group text-right m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                                            <input type="hidden" name="cur_timestamp" value="<?php echo $cur_timestamp; ?>"/> 
                                                            <input type="submit" class="btn btn-primary" value="Apply" name="submit_btn" id="submit_btn">
                                                            <button type="reset" class="btn" style="margin-right:6%;">Clear</button>
                                                        </div>
                                                    </div>

                                                </form> 
                                            </div>
                                        </div>
                                        <!-- to display the leave log for the team--->
                                        <div id="team" class="tab-pane cont">
                                            <div class="tab-container" >
                                                <div class="nav tab_custom" style="z-index:1;margin-top: -1%;">
                                                    <ul class="nav nav-tabs pull-right">
                                                        <li id="lev_balance_tab" class="active"><a href="#holiday_2018" data-toggle="tab"><?php echo $cur_year; ?></a></li>
                                                        <li id="lev_policy_tab"><a href="#holiday_2017" data-toggle="tab"><?php echo $prev_year; ?></a></li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content" >
                                                    <!--Leave Log - 2017 -->
                                                    <div id="holiday_2018" class="tab-pane active cont">
                                                        <?php
                                                        if (isset($emp_id_all)) {
                                                            //counter to fetch all emp assigned
                                                            for ($l = 0; $l < count($emp_id_all); $l++) {
                                                                $i = 0; //counter for each emp_id
                                                                $leaves_taken = 0; //set as 0 for each emp_id
                                                                //        //counter initialization
                                                                $leaves_taken = 0;
                                                                $count_el = 0;
                                                                $count_cl = 0;
                                                                $count_wp = 0;
                                                                $num_of_el = 0;
                                                                $num_of_cl = 0;
                                                                //to fetch the holidays taken over the period and store in an array for the emp-id
                                                                $stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
                                                                $stmt03->execute(array($emp_id_all[$l], $cur_year, $cur_year));
                                                                if ($stmt03->rowCount() != 0) {
                                                                    while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
                                                                        $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
                                                                        $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
                                                                        $type[$i] = $row03['type']; ////to fetch type of leave

                                                                        $status_leave[$i] = $row03['status']; //to fetch the status os leave
                                                                        $reason[$i] = $row03['reason']; //to fetch the reason of leave
                                                                        $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
                                                                        if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                            $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                            if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                                $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                                if ($type[$i] == "CL") {
                                                                                    $count_cl = $count_cl + $no_of_days[$i]; //number of casual leaves taken
                                                                                } else if ($type[$i] == "EL") {
                                                                                    $count_el = $count_el + $no_of_days[$i]; //number of earned leaves taken
                                                                                } else {
                                                                                    $count_wp = $count_wp + $no_of_days[$i]; //number of without pay leaves taken
                                                                                }
                                                                            } else {
                                                                                $leaves_taken = $leaves_taken;
                                                                            }
                                                                        } else {
                                                                            $leaves_taken = $leaves_taken;
                                                                        }
                                                                        $i++;
                                                                    }
                                                                }
                                                                //to fetch the el_constant, cl_constant and cf_constant for the given emp_id

                                                                $stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant WHERE emp_id=? AND year=? LIMIT 2");
                                                                $stmt01->execute(array($emp_id_all[$l], $cur_year));
                                                                if ($stmt01->rowCount() != 0) {
                                                                    $row01 = $stmt01->fetch();
                                                                    #earned leaves
                                                                    $el_constant = $row01['el_constant'];
                                                                    #casual leaves
                                                                    $cl_constant = $row01['cl_constant'];
                                                                    #carried forward leaves
                                                                    $cf = $row01['cf'];
                                                                }

#casual leaves whole year
                                                                $num_of_cl = $cl_constant * 12;

#checking if curr_month is 1 or not
//if curr_month is 1, then check i date is greater than 15 or not
                                                                if ($curr_month == 1) {
                                                                    if ($cur_date >= 15) {
                                                                        //if date is greater than 15, assign prev_moth as curr_month, else set as 0
                                                                        $prev_month = $curr_month;
                                                                    } else {
                                                                        $prev_month = 0;
                                                                    }
                                                                } else {
                                                                    //if date is greater than 15, assign prev_moth as curr_month, else set prev_month as last month number
                                                                    if ($cur_date >= 15) {
                                                                        $prev_month = $curr_month;
                                                                    } else {
                                                                        $prev_month = date("m", strtotime("last month"));
                                                                    }
                                                                }

#no of total earned leaves
                                                                $num_of_el = ($el_constant * $prev_month) + $cf;
                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-sm-1"></div>
                                                                    <div class="col-sm-10 col-xs-12">
                                                                        <div class="panel panel-default panel-table" style="padding-left: 15px;padding-right: 15px;">
                                                                            <div class="panel-body">
                                                                                <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:-1%;margin-top:2%;"><b><?php echo $emp_name_all[$l]; ?></b><span style="font-size:15px;"> ( Earned Leave - <?php echo $count_el . ' / ' . $num_of_el; ?>, Casual Leave - <?php echo $count_cl . ' / ' . $num_of_cl; ?> )</span></h4>
                                                                                <hr class="style-two"/>
                                                                                <div class="table-responsive">
                                                                                    <?php
                                                                                    if ($stmt03->rowCount() != 0) {
                                                                                        ?>
                                                                                        <table class="table table-striped table-borderless" style="margin-top:-1%;">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th >From</th>
                                                                                                    <th >To</th>
                                                                                                    <th>Type</th>
                                                                                                    <th >Number of days</th>
                                                                                                    <th >Reason</th>
                                                                                                    <th >Status</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <?php
                                                                                                if (isset($leaves_taken_from)) {
                                                                                                    for ($m = 0; $m < $stmt03->rowCount(); $m++) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td><?php echo date('d-M-Y', strtotime($leaves_taken_from[$m])); ?></td>
                                                                                                            <td><?php echo date('d-M-Y', strtotime($leaves_taken_to[$m])); ?></td>
                                                                                                            <td><?php echo type_name($type[$m]); ?></td>
                                                                                                            <td><?php echo $no_of_days[$m]; ?></td>
                                                                                                            <td><?php echo $reason[$m]; ?></td>
                                                                                                            <td><?php echo leave_status($status_leave[$m]); ?></td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                echo "No leaves taken.";
                                                                                            }
                                                                                            ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div id="holiday_2017" class="tab-pane">
                                                        <?php
                                                        if (isset($emp_id_all)) {
                                                            //counter to fetch all emp assigned
                                                            for ($l = 0; $l < count($emp_id_all); $l++) {
                                                                $i = 0; //counter for each emp_id
                                                                $leaves_taken = 0; //set as 0 for each emp_id
                                                                //        //counter initialization
                                                                $leaves_taken = 0;
                                                                $count_el = 0;
                                                                $count_cl = 0;
                                                                $count_wp = 0;
                                                                $num_of_el = 0;
                                                                $num_of_cl = 0;
                                                                //to fetch the holidays taken over the period and store in an array for the emp-id
                                                                $stmt03 = $dbh->prepare("SELECT * FROM emp_leave WHERE emp_id=? AND YEAR(from_date)=? AND YEAR(to_date)=? ORDER BY from_date");
                                                                $stmt03->execute(array($emp_id_all[$l], $prev_year, $prev_year));
                                                                if ($stmt03->rowCount() != 0) {
                                                                    while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
                                                                        $leaves_taken_from[$i] = $row03['from_date']; //to fetch list of dates from date its taken
                                                                        $leaves_taken_to[$i] = $row03['to_date']; //to fetch list of dates to date its taken
                                                                        $type[$i] = $row03['type']; ////to fetch type of leave

                                                                        $status_leave[$i] = $row03['status']; //to fetch the status os leave
                                                                        $reason[$i] = $row03['reason']; //to fetch the reason of leave
                                                                        $no_of_days[$i] = $row03['no_of_days']; //to fetch number of days
                                                                        if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                            $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                            if ($status_leave[$i] == "A" || $status_leave[$i] == "M") {//only if status is approved, consider as $total_leaves_taken else set as previous value
                                                                                $leaves_taken = $leaves_taken + $no_of_days[$i];
                                                                                if ($type[$i] == "CL") {
                                                                                    $count_cl = $count_cl + $no_of_days[$i]; //number of casual leaves taken
                                                                                } else if ($type[$i] == "EL") {
                                                                                    $count_el = $count_el + $no_of_days[$i]; //number of earned leaves taken
                                                                                } else {
                                                                                    $count_wp = $count_wp + $no_of_days[$i]; //number of without pay leaves taken
                                                                                }
                                                                            } else {
                                                                                $leaves_taken = $leaves_taken;
                                                                            }
                                                                        } else {
                                                                            $leaves_taken = $leaves_taken;
                                                                        }
                                                                        $i++;
                                                                    }
                                                                }
                                                                //to fetch the el_constant, cl_constant and cf_constant for the given emp_id

                                                                $stmt01 = $dbh->prepare("SELECT * FROM emp_leave_constant WHERE emp_id=? AND year=? LIMIT 2");
                                                                $stmt01->execute(array($emp_id_all[$l], $prev_year));
                                                                if ($stmt01->rowCount() != 0) {
                                                                    $row01 = $stmt01->fetch();
                                                                    #earned leaves
                                                                    $el_constant = $row01['el_constant'];
                                                                    #casual leaves
                                                                    $cl_constant = $row01['cl_constant'];
                                                                    #carried forward leaves
                                                                    $cf = $row01['cf'];
                                                                }

#casual leaves whole year
                                                                $num_of_cl = $cl_constant * 12;

//since year has been completed , previous month will be "12"
                                                                $prev_month = 12;
#no of total earned leaves
                                                                $num_of_el = ($el_constant * $prev_month) + $cf;
                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-sm-1"></div>
                                                                    <div class="col-sm-10 col-xs-12">
                                                                        <div class="panel panel-default panel-table" style="padding-left: 15px;padding-right: 15px;">
                                                                            <div class="panel-body">
                                                                                <h4 class="header-title m-t-0 m-b-30" style="margin-bottom:-1%;margin-top:2%;"><b><?php echo $emp_name_all[$l]; ?></b><span style="font-size:15px;"> ( Earned Leave - <?php echo $count_el . ' / ' . $num_of_el; ?>, Casual Leave - <?php echo $count_cl . ' / ' . $num_of_cl; ?> )</span></h4>
                                                                                <hr class="style-two"/>
                                                                                <div class="table-responsive">
                                                                                    <?php
                                                                                    if ($stmt03->rowCount() != 0) {
                                                                                        ?>
                                                                                        <table class="table table-striped table-borderless" style="margin-top:-1%;">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th >From</th>
                                                                                                    <th >To</th>
                                                                                                    <th>Type</th>
                                                                                                    <th >Number of days</th>
                                                                                                    <th >Reason</th>
                                                                                                    <th >Status</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <?php
                                                                                                if (isset($leaves_taken_from)) {
                                                                                                    for ($m = 0; $m < $stmt03->rowCount(); $m++) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td><?php echo date('d-M-Y', strtotime($leaves_taken_from[$m])); ?></td>
                                                                                                            <td><?php echo date('d-M-Y', strtotime($leaves_taken_to[$m])); ?></td>
                                                                                                            <td><?php echo type_name($type[$m]); ?></td>
                                                                                                            <td><?php echo $no_of_days[$m]; ?></td>
                                                                                                            <td><?php echo $reason[$m]; ?></td>
                                                                                                            <td><?php echo leave_status($status_leave[$m]); ?></td>
                                                                                                        </tr>

                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                echo "No leaves taken.";
                                                                                            }
                                                                                            ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/js/main.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
<script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
<script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
<script type="text/javascript">
                                                    $(document).ready(function () {
                                                        //initialize the javascript
                                                        App.init();
                                                        App.formElements();
                                                    });
</script>
<script>
<?php if (($status == 1) || ($status == 2) || ($status == 3) || ($status == 4)) { ?>
        $(document).ready(function () {
            $("#approval_tab").removeClass("active");
            $("#approval").removeClass("active");
            $("#record_tab").addClass("active");
            $("#record").addClass("active in");
        });
<?php } ?>
</script>
<script>
    function validateRadio(obj, error_no) {
        var result = 0;
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked == true) {
                result = 1;

                break;
            }
        }
        return result;
    }

    function test() {
        var err = '';
        var q5 = document.getElementsByName("type");

        if (!validateRadio(q5, 5)) {
            $("#error5").addClass("red-error");
            err += '\n 2';
        }

        if (err.length) {
            $("div.error").html("Please select");
            return false;
        } else {

            return true;
        }

    }

</script>
<script>
    $('#multiple').validate({
        rules: {
            type_of_leave: {
                required: true,
            },
            to_date: {
                required: true,
            },
            from_date: {
                required: true,
            },
            reason: {
                required: true,
            }
        },
        messages: {
            type_of_leaves: {
                required: "Please enter a type",
            },
            to_date: {
                required: "Please enter a date",
            },
            from_date: {
                required: "Please enter a date",
            },
            reason: {
                required: "Please enter a reason",
            }
        }
    });
</script>
</body>
</html>