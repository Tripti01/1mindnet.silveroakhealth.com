<?php
/*
  1.In the sidebar when view_corporates is clicked we land onto this page.
  2.All the corporates details can be seen here, there id,name and registration type are displayed with a view button in last column
  3.Clicking onto the view button takes you to the individual corporate information
 */

#checking the login details
include '../../if_loggedin.php';


#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';

#REQUEST activity_id
$activity_id = $_REQUEST['activity_id'];

# Start database transactions
$i = 0;
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Select categories from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_login WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row00['corp_id'];
        $corp_name[$i] = $row00['name'];
        $i++;
    }
}

# Select activities from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_activity WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $activity_type[$i] = $row00['activity_type'];
        $activity_name[$i] = $row00['activity_name'];
        $i++;
    }
}

# Select activities from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_activity_track1,corp_activity_track2 WHERE corp_activity_track1.activity_id = corp_activity_track2.activity_id AND corp_activity_track1.activity_id=?");
$stmt00->execute(array($activity_id));
if ($stmt00->rowCount() != 0) {
    $row00 = $stmt00->fetch();
    $ac_corp_id = $row00['corp_id'];
    $held_on = date("d-M-Y", strtotime($row00['held_on']));
    $ac_type = $row00['activity_type'];
    $country = $row00['country'];
    $state = $row00['state'];
    $city = $row00['city'];
    $additional_details = $row00['add_details'];
    $facilitator_name = $row00['facilitator_name'];
    $ac_remarks = $row00['remarks'];
    $completed_status = $row00['status'];
    $location = $city . " " . $state . " " . $country;

    $stmt01 = $dbh->prepare("SELECT activity_name FROM corp_activity WHERE activity_type=?");
    $stmt01->execute(array($ac_type));
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        $ac_name = $row01['activity_name'];
    }

    $stmt02 = $dbh->prepare("SELECT name FROM corp_login WHERE corp_id=?");
    $stmt02->execute(array($ac_corp_id));
    if ($stmt02->rowCount() != 0) {
        $row02 = $stmt02->fetch();
        $ac_corp_name = $row02['name'];
    }
}
if (isset($_REQUEST['corp_id']) && isset($_REQUEST['activity_type']) && isset($_REQUEST['date']) && isset($_REQUEST['country']) && isset($_REQUEST['state']) && isset($_REQUEST['city']) && isset($_REQUEST['fac_name']) && isset($_REQUEST['add_details']) && isset($_REQUEST['remarks'])) {

    $activity_id = $_REQUEST['activity_id'];
    $corp_id = $_REQUEST['corp_id'];
    $activity_type = $_REQUEST['activity_type'];
    $date = str_replace('/', '-', $_REQUEST['date']);
    $held_on_date = date("Y-m-d ", strtotime($_REQUEST['date']));
    $country = $_REQUEST['country'];
    $state = $_REQUEST['state'];
    $city = $_REQUEST['city'];
    $fac_name = $_REQUEST['fac_name'];
    $add_details = $_REQUEST['add_details'];
    $remarks = $_REQUEST['remarks'];
    $completed = $_REQUEST['completed'];

    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use sohdbl");

    $stmt20 = $dbh->prepare("UPDATE corp_activity_track1 SET corp_id=?,activity_type=?,held_on=?,country=?,state=?,city=?,status=? WHERE activity_id=?");
    $stmt20->execute(array($corp_id, $activity_type, $held_on_date, $country, $state, $city, $completed, $activity_id));

    $stmt60 = $dbh->prepare("UPDATE corp_activity_track2 SET facilitator_name=?,add_details=?,remarks=? WHERE activity_id=?");
    $stmt60->execute(array($fac_name, $add_details, $remarks, $activity_id));

    header("Location:" . $host . "/modules/corporate/activity_tracking.php?edit_status_code=2");
}

if (isset($_REQUEST['del_activity'])) {
    if (isset($_REQUEST['activity_id'])) {

        $corp_id = $_REQUEST['activity_id'];

        $dbh = new PDO($dsn, $login_user, $login_pass);
        $dbh->query("use sohdbl");

        $stmt20 = $dbh->prepare("DELETE FROM corp_activity_track1 WHERE activity_id=?");
        $stmt20->execute(array($activity_id));

        $stmt30 = $dbh->prepare("DELETE FROM corp_activity_track2 WHERE activity_id=?");
        $stmt30->execute(array($activity_id));

        header("Location:" . $host . "/modules/corporate/activity_tracking.php?edit_status_code=4");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

        <!-- App title -->
        <title>Edit Activity - Silver Oak Health</title>

        <!-- DataTables -->
        <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

        <script src="assets/js/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiQbVEMemZgEE5gdyI50TxyIUK0Ba9PBI&sensor=false&libraries=places"></script>
        <script src="../../../assets2/global/scripts/jquery.geocomplete.min.js"></script>
        <script src="../../assets/js/modernizr.min.js"></script>
        <script>
            $(function () {
                $("#location").geocomplete({
                    details: ".geo-details",
                    detailsAttribute: "data-geo"
                });

            });
        </script>
        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .row{
                padding : 8px;
            }			
            .modal{
                margin-top:20px;
            }
            .modal-backdrop.in {
                filter: alpha(opacity=50);
                opacity: 0;
            }
        </style>
    </head>
    <body class=" fixed-left" style="background-color:#fff">
        <!-- Modal to add role details -->
        <div class="container">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST"  id="modal_role" target="_top" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="toplocation()">&times;</button>
                    <h4 class="modal-title">Edit Activity</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="col-sm-6">
                                <label  style="font-family:'Open Sans';">Corporate<span style="color:red;">*</span></label>
                                <select id="corp-list-id" class="corp-list form-control"  name="corp_id">
                                    <option value="0" disabled="" selected="">SELECT</option>
                                    <?php for ($i = 0; $i < count($corp_id); $i++) { ?>
                                        <option value="<?php echo $corp_id[$i]; ?>" <?php
                                        if ($corp_id[$i] == $ac_corp_id) {
                                            echo 'selected';
                                        }
                                        ?>><?php echo $corp_name[$i]; ?></option>
<?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label  style="font-family:'Open Sans';">Activity<span style="color:red;">*</span></label>
                                <select id="corp-list-id" class="corp-list form-control"  name="activity_type">
                                    <option value="0" disabled="" selected="">SELECT</option>
                                    <?php for ($i = 0; $i < count($activity_type); $i++) { ?>
                                        <option value="<?php echo $activity_type[$i]; ?>" <?php
                                                if ($activity_type[$i] == $ac_type) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $activity_name[$i]; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="col-sm-4">
                                <label  style="font-family:'Open Sans';">Held On<span style="color:red;">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="datepicker" placeholder="Select date here " name="date" value="<?php echo $held_on; ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label  style="font-family:'Open Sans';">Location<span style="color:red;">*</span></label>
                                <div class="geo-details">
                                    <input type="text" placeholder="Location" name="location" id="location" class="form-control" style="padding-left:12px" autocomplete="off" value="<?php echo $location; ?>">
                                    <input class="form-control placeholder-no-fix" data-geo="country" value="" id="country" name="country" type="hidden">
                                    <input class="form-control placeholder-no-fix" data-geo="administrative_area_level_1" value="" id="state" name="state" type="hidden">
                                    <input class="form-control placeholder-no-fix" data-geo="administrative_area_level_2" value="" id="city" name="city" type="hidden">
                                    <input class="form-control placeholder-no-fix" data-geo="lat" value="" id="latitude" name="latitude" type="hidden">
                                    <input class="form-control placeholder-no-fix" data-geo="lng" value="" id="longitude" name="longitude" type="hidden">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label  style="font-family:'Open Sans';">Status<span style="color:red;">*</span></label><br/>
                                <label style="margin: 10px 5px 0px 0px;float:left;font-weight:400;"><input type="radio"  name="completed" value='C' <?php echo ($completed_status == 'C') ? 'checked' : '' ?>> Completed </label>
                                <label style="margin: 10px 5px 0px 20px;float:left;font-weight:400;"><input type="radio" name="completed" value='S' <?php echo ($completed_status == 'S') ? 'checked' : '' ?>> Scheduled	</label>													
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="col-sm-6">
                                <label  style="font-family:'Open Sans';">Facilitator Name</label>
                                <input type="text" class="form-control"  placeholder="" name="fac_name" value="<?php echo $facilitator_name; ?>">															
                            </div>
                            <div class="col-sm-6">
                                <label  style="font-family:'Open Sans';">Additional Details</label>
                                <input type="text" class="form-control"  placeholder="" name="add_details" value="<?php echo $additional_details; ?>">	
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="col-sm-12">
                                <label  style="font-family:'Open Sans';">Remarks</label>
                                <textarea  class="form-control"  placeholder="" name="remarks"><?php echo $ac_remarks; ?></textarea>															
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="<?php echo $activity_id; ?>" name="activity_id" />
                    <input  type="submit" name="create_activity"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                    <button type="button" class="btn btn-danger" style="margin-left:3%;" onclick="toplocation()">Cancel</button>
                    <button type="button" class="btn btn-info" style="margin-left:3%;float:left;background-color: #223c87 !important; border-color: #223c87 !important;" data-toggle="modal" data-target="#del">Delete</button>
                </div>
            </form>
        </div>
        <div class="container">
            <div class="modal fade" id="del"  role="dialog">
                <div class="modal-dialog" style="width:400px;">
                    <div class="modal-content" style="height:172px;margin-top:100px;background-color:#eaf0f0">
                        <center>Are you sure you want to delete this?
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST"  id="modal_role"  class="form-horizontal">
                                <input type="hidden" value="<?php echo $activity_id; ?>" name="activity_id" />
                                <input  type="submit" name="del_activity"  class="btn btn-info" value="Yes" style="background-color: #223c87 !important; border-color: #223c87 !important;margin-top:20px" >
                                <button type="button" class="btn btn-danger" onclick="framelocation()" style="margin-left:3%;margin-top:20px">No</button>
                            </form></center>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->

        <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>			
        <script src="assets/plugins/moment/moment.js"></script>
        <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script>
                                    var date = new Date();
                                    date.setDate(date.getDate());

                                    $('#datepicker').datepicker({
                                        startDate: '01/01/1990'
                                    });
        </script>
        <!--validation for the form -->
        <script type="text/javascript">

            $(".form-horizontal").validate({
                rules: {
                    corp_id: {
                        required: true
                    },
                    activity_type: {
                        required: true
                    },
                    date: {
                        required: true,
                    },
                    location: {
                        required: true,
                    }

                },
                // Specify validation error messages
                messages: {
                    corp_id: {
                        required: "Please select corporate"
                    },
                    activity_type: {
                        required: "Please select activity"
                    },
                    date: {
                        required: "Please enter a date",
                    },
                    location: {
                        required: "Please enter a location",
                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

        </script>
        <script>
            function toplocation() {
                window.top.location = 'activity_tracking.php';
            }
            function framelocation() {
                window.location.href = 'edit_activity.php?activity_id=<?php echo $activity_id; ?>';
            }
        </script>	
    </body>
</html>