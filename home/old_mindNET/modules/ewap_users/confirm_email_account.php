<?php

function send_activation_mail($to, $to_name, $token) {
require_once 'aws_sdk/aws-autoloader.php';
require_once 'ses_plugin/autoloader.php';
require 'ses_plugin/mail_credentials.php';


$m = new SimpleEmailServiceMessage();

## ------------------------------- EMAIL PARAMETERS ---------------------------##

$link = 'https://stresscontrolonline.com/register/individual/confirm.php?token=' . $token;
$to = $to;
$from = 'Stress Control Online <no-reply@stresscontrolonline.com>';
$subject = 'Please confirm your email address';
$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <title></title>
        <style type="text/css">
            html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
            @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
                *[class="table_width_100"] {
                    width: 96% !important;
                }
                *[class="border-right_mob"] {
                    border-right: 1px solid #dddddd;
                }
                *[class="mob_100"] {
                    width: 100% !important;
                }
                *[class="mob_center"] {
                    text-align: center !important;
                }
                *[class="mob_center_bl"] {
                    float: none !important;
                    display: block !important;
                    margin: 0px auto;
                }	
                .iage_footer a {
                    text-decoration: none;
                    color: #929ca8;
                }
                img.mob_display_none {
                    width: 0px !important;
                    height: 0px !important;
                    display: none !important;
                }
                img.mob_width_50 {
                    width: 40% !important;
                    height: auto !important;
                }
            }
            .table_width_100 {
                width: 680px;
            }
        </style>
    </head>
    <body style="padding: 0px; margin: 0px;">
        <div id="mailsub" class="notification" align="center">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" >
                        <!--[if gte mso 10]>
                        <table width="680" border="0" cellspacing="0" cellpadding="0">
                        <tr><td>
                        <![endif]-->
                        <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                            <!--header -->
                            <tr><td align="center" bgcolor="#eff3f8">
                                    <!-- padding --><div style="height: 0px; line-height: 0px; font-size: 10px;">&nbsp;</div>
                                    <table width="96%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="left"><!-- 

                                                        Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 98%;">
                                                    <table class="mob_center" width="98%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;">
                                                        <tr><td align="left" valign="middle">
                                                                <!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
                                                                <table width="98%" border="0" cellspacing="0" cellpadding="0" >
                                                                    <tr><td align="center" valign="top" class="mob_center">
                                                                            <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                                                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" border="0" style="display: block;" /></font></a>
                                                                        </td></tr>
                                                                </table>						
                                                            </td></tr>
                                                    </table></div><!-- Item END--><!--[if gte mso 10]>
                                                    </td>
                                                    <td align="right">
                                            <![endif]--></td>
                                        </tr>
                                    </table>
                                    <!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
                                </td></tr>
                            <!--header END-->
                            <!--content 1 -->
                            <tr><td align="center" bgcolor="#ffffff">
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                                <!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;">&nbsp;</div>
                                                <!-- padding --><div style="height: 30px; line-height: 10px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                        <tr><td align="center">
                                                <div style="line-height: 30px;">
                                                    <font face="Arial, Helvetica, sans-serif" size="5" color="#4db3a4" style="font-size: 17px;">
                                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #4db3a4;">

                                                        </span></font>
                                                </div>
                                            </td></tr>
                                        <tr><td align="center">
                                                <table width="80%" align="center" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td align="center">
                                                            <div style="line-height: 24px;">
                                                                <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 16px;">
                                                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
                                                                        Hi&nbsp;' . $to_name . ', Thank you for signing up with StressControlOnline.com. <BR/>Click the button below to confirm your email address. 												

                                                                    </span></font>
                                                            </div>
                                                        </td></tr>
                                                </table>
                                                <!-- padding --><div style="height: 45px; line-height: 45px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                        <tr><td align="center">
                                                <div style="line-height:24px;">
                                                    <a href="' . $link . '" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 20px;display: block; padding: 15px; text-decoration: none; width: 280px;color: #FFFFFF; background-color: #3AB149">
                                                        Confirm Email Address
                                                    </a>
                                                </div>
                                                <!-- padding --><div style="height: 45px; line-height: 45px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                    </table>		
                                </td></tr>
                            <!--content 1 END-->
                            <!--footer -->
                            <tr><td class="iage_footer" align="center" bgcolor="#eff3f8">
                                    <!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>	

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                                <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                                        2016 &copy; Silver Oak Health.  
                                                    </span></font>				
                                            </td></tr>			
                                    </table>

                                    <!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>	
                                </td></tr>
                            <!--footer END-->
                        </table>
                        <!--[if gte mso 10]>
                        </td></tr>
                        </table>
                        <![endif]-->			 
                    </td></tr>
            </table>						
        </div> 
    </body>
</html>';
$text = ''; // plain text version [optional]
## ----------------------------------------------------------------------------##

$m->addTo($to);
$m->setFrom($from);
$m->setSubject($subject);
$m->setMessageFromString($text, $html);
$m->setSubjectCharset('ISO-8859-1');

try {
    $ses = new SimpleEmailService($key, $secret); // Sending the message
	$ses->sendEmail($m);
} catch (Exception $ex) {
    die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
}
}

function send_activation_mail_emp($to, $to_name, $token) {
include 'soh-config.php';
require 'aws_sdk/aws-autoloader.php';
require 'ses_plugin/autoloader.php';
require 'ses_plugin/mail_credentials.php';

$m = new SimpleEmailServiceMessage();

## ------------------------------- EMAIL PARAMETERS ---------------------------##

 $link = 'http://www.stresscontrolonline.com/register/corporate/confirm_emp.php?token=' . $token;
$to = $to;
$from = 'Stress Control Online <no-reply@stresscontrolonline.com>';
$subject = 'Please confirm your email address';
$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <title></title>
        <style type="text/css">
            html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
            @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
                *[class="table_width_100"] {
                    width: 96% !important;
                }
                *[class="border-right_mob"] {
                    border-right: 1px solid #dddddd;
                }
                *[class="mob_100"] {
                    width: 100% !important;
                }
                *[class="mob_center"] {
                    text-align: center !important;
                }
                *[class="mob_center_bl"] {
                    float: none !important;
                    display: block !important;
                    margin: 0px auto;
                }	
                .iage_footer a {
                    text-decoration: none;
                    color: #929ca8;
                }
                img.mob_display_none {
                    width: 0px !important;
                    height: 0px !important;
                    display: none !important;
                }
                img.mob_width_50 {
                    width: 40% !important;
                    height: auto !important;
                }
            }
            .table_width_100 {
                width: 680px;
            }
        </style>
    </head>
    <body style="padding: 0px; margin: 0px;">
        <div id="mailsub" class="notification" align="center">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" >
                        <!--[if gte mso 10]>
                        <table width="680" border="0" cellspacing="0" cellpadding="0">
                        <tr><td>
                        <![endif]-->
                        <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                            <!--header -->
                            <tr><td align="center" bgcolor="#eff3f8">
                                    <!-- padding --><div style="height: 0px; line-height: 0px; font-size: 10px;">&nbsp;</div>
                                    <table width="96%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="left"><!-- 

                                                        Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 98%;">
                                                    <table class="mob_center" width="98%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;">
                                                        <tr><td align="left" valign="middle">
                                                                <!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
                                                                <table width="98%" border="0" cellspacing="0" cellpadding="0" >
                                                                    <tr><td align="center" valign="top" class="mob_center">
                                                                            <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                                                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/StressControlLogo.png" border="0" style="display: block;" /></font></a>
                                                                        </td></tr>
                                                                </table>						
                                                            </td></tr>
                                                    </table></div><!-- Item END--><!--[if gte mso 10]>
                                                    </td>
                                                    <td align="right">
                                            <![endif]--></td>
                                        </tr>
                                    </table>
                                    <!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
                                </td></tr>
                            <!--header END-->
                            <!--content 1 -->
                            <tr><td align="center" bgcolor="#ffffff">
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                                <!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;">&nbsp;</div>
                                                <!-- padding --><div style="height: 30px; line-height: 10px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                        <tr><td align="center">
                                                <div style="line-height: 30px;">
                                                    <font face="Arial, Helvetica, sans-serif" size="5" color="#4db3a4" style="font-size: 17px;">
                                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #4db3a4;">

                                                        </span></font>
                                                </div>
                                            </td></tr>
                                        <tr><td align="center">
                                                <table width="80%" align="center" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td align="center">
                                                            <div style="line-height: 24px;">
                                                                <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 16px;">
                                                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
                                                                        Hi&nbsp;' . $to_name . ', Thank you for signing up with StressControlOnline.com. <BR/>Click the button below to confirm your email address. 												

                                                                    </span></font>
                                                            </div>
                                                        </td></tr>
                                                </table>
                                                <!-- padding --><div style="height: 45px; line-height: 45px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                        <tr><td align="center">
                                                <div style="line-height:24px;">
                                                    <a href="' . $link . '" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 20px;display: block; padding: 15px; text-decoration: none; width: 280px;color: #FFFFFF; background-color: #3AB149">
                                                        Confirm Email Address
                                                    </a>
                                                </div>
                                                <!-- padding --><div style="height: 45px; line-height: 45px; font-size: 10px;">&nbsp;</div>
                                            </td></tr>
                                    </table>		
                                </td></tr>
                            <!--content 1 END-->
                            <!--footer -->
                            <tr><td class="iage_footer" align="center" bgcolor="#eff3f8">
                                    <!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>	

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                                <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                                        2016 &copy; Silver Oak Health.  
                                                    </span></font>				
                                            </td></tr>			
                                    </table>

                                    <!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>	
                                </td></tr>
                            <!--footer END-->
                        </table>
                        <!--[if gte mso 10]>
                        </td></tr>
                        </table>
                        <![endif]-->			 
                    </td></tr>
            </table>						
        </div> 
    </body>
</html>';
$text = ''; // plain text version [optional]
## ----------------------------------------------------------------------------##

$m->addTo($to);
$m->setFrom($from);
$m->setSubject($subject);
$m->setMessageFromString($text, $html);
$m->setSubjectCharset('ISO-8859-1');

try {
    $ses = new SimpleEmailService($key, $secret); // Sending the message
	$ses->sendEmail($m);
} catch (Exception $ex) {
    die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
}
}
?>