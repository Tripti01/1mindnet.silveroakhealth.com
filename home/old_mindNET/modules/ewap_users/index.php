<?php
//include files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'sco_config.php';
include 'mindnet-config.php';
require_once 'mindnet/functions/crypto_funtions.php';

$dbh = new PDO($sco_dsn, $sco_login_user, $sco_login_pass);
$dbh->query("use sohdbl");

# Select categories from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_login ORDER BY name");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row00['corp_id'];
        $corp_name[$i] = $row00['name'];
        $i++;
    }
}

$signup_status = 0;

if (isset($_REQUEST['btn-create-user'])) {

    if (isset($_REQUEST['emp_id']) && $_REQUEST['emp_id'] != '' && isset($_REQUEST['corp_id']) && $_REQUEST['corp_id'] != '') {

        $employee_id = $_REQUEST['emp_id'];
        $corp_id = $_REQUEST['corp_id'];


        #---------------Database Connection EWAP---------------------#
        $dbhost = "scodd.cgbct3oeqwvc.ap-south-1.rds.amazonaws.com";
        $dbport = 3306;
        $dbname = "scodd";
        $ewap_dsn = "mysql:host={$dbhost};port={$dbport};dbname={$dbname}";

        $ewap_user = "ewap_user";
        $ewap_pass = "SCO@7764#128";
        #---------------Database Connection EWAP---------------------#
        #connection to database
        $dbh = new PDO($ewap_dsn, $ewap_user, $ewap_pass);
        $dbh->query("use scodd");

        # Get location vertical process of this user
        $stmt = $dbh->prepare("SELECT corp_id,emp_name,location, vertical,process FROM emp_db WHERE emp_id=? AND corp_id=? LIMIT 1");
        $stmt->execute(array($employee_id, $corp_id));
        if ($stmt->rowCount() != 0) {
            $row = $stmt->fetch();
            $corp_id = $row['corp_id'];
            $employee_name = $row['emp_name'];
            $location = $row['location'];
            $vertical = $row['vertical'];
            $process = $row['process'];

            echo '<form method="POST" name="redirect" action="create_user.php">';
            echo '<input type="hidden" name="employee_id" value="' . $employee_id . '">';
            echo '<input type="hidden" name="employee_name" value="' . $employee_name . '">';
            echo '<input type="hidden" name="location" value="' . $location . '">';
            echo '<input type="hidden" name="vertical" value="' . $vertical . '">';
            echo '<input type="hidden" name="process" value="' . $process . '">';
            echo '<input type="hidden" name="corp_id" value="' . $corp_id . '">';
            echo '</form>';
            echo '<script language="javascript">document.redirect.submit();</script>';

            $signup_status = 1;
        } else {
            $signup_status = 2;
        }
    } else {
        $signup_status = 3;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Create User</div>
                                    <div class="panel-body">
                                        <div Style="text-align: center;font-size: 15px;font-weight:bold;">
                                            <?php
                                            if ($signup_status == 2) {
                                                echo "No employee exist with  id :" . "<font color='red'>" . $employee_id . "</font>";
                                            } else if ($signup_status == 3) {
                                                echo "<font color='red'>Please enter the required details</font>";
                                            }
                                            ?>
                                            <div class="confirm-msg">												
                                            </div>
                                        </div>
                                        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Corporate Id </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <select id="corp-list-id" class="form-control"  name="corp_id">
                                                        <option value="0" disabled="" selected="">SELECT</option>
                                                        <?php
                                                        for ($i = 0; $i < count($corp_id); $i++) {
                                                            echo '<option value="' . $corp_id[$i] . '">' . $corp_name[$i] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Employee Id </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control"  name="emp_id" placeholder="Employee Id">
                                                </div>
                                            </div>
                                            <br/><br/>
                                            <div class="form-group m-b-0" style="text-align: center;">
                                                <input type="submit" class="btn btn-info waves-effect waves-light" value="Proceed" name="btn-create-user"/> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>