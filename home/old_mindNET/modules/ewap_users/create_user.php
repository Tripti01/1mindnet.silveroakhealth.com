<?php
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

## Gettig UTM codes ##
if (isset($_SESSION['utm_src'])) {
    $utm_src = $_SESSION['utm_src'];
} else {
    $utm_src = '';
}
if (isset($_SESSION['utm_med'])) {
    $utm_med = $_SESSION['utm_med'];
} else {
    $utm_med = '';
}
if (isset($_SESSION['utm_cmp'])) {
    $utm_cmp = $_SESSION['utm_cmp'];
} else {
    $utm_cmp = '';
}
if (isset($_SESSION['utm_url'])) {
    $utm_url = $_SESSION['utm_url'];
} else {
    $utm_url = '';
}

if(isset($_REQUEST['employee_name']) && isset($_REQUEST['corp_id']) && isset($_REQUEST['employee_id']) && isset($_REQUEST['location']) && isset($_REQUEST['vertical']) && isset($_REQUEST['process'])){
	$corp_id = $_REQUEST['corp_id'];
	$employee_name = $_REQUEST['employee_name'] ;
	$vertical = $_REQUEST['vertical'];
	$process =  $_REQUEST['process'];
	$employee_id = $_REQUEST['employee_id'];
	$location = $_REQUEST['location'];
}else{
	header('Location:' . $host . '/modules/ewap_users/');
}

# By default signup_status is zero.
$signup_status = 0;

## Processing below when form is submitted
if (isset($_REQUEST['btn-create-user'])) {

		require_once 'mindnet/functions/crypto_funtions.php';
        include 'mindnet/functions/token.php';
		include 'sco_config.php';
		
        # Getting user details
        $name = trim($_REQUEST['name']);
        $email = trim($_REQUEST['email']);
		$c_email = $_REQUEST['c_email'];
        $mobile = $_REQUEST['mobile'];
		
		$corp_id = $_REQUEST['corp_id'];
		$employee_name = $_REQUEST['employee_name'] ;
		$location = $_REQUEST['location'];
		$vertical = $_REQUEST['vertical'];
		$process =  $_REQUEST['process'];
		$employee_id = $_REQUEST['employee_id'];

        # Getting utm details
        $utm_src_req = $_REQUEST['utm_src'];
        $utm_med_req = $_REQUEST['utm_med'];
        $utm_cmp_req = $_REQUEST['utm_cmp'];
        $utm_url_req = $_REQUEST['utm_url'];

        $encrypted_name = encrypt($name, $encryption_key);
		
		
		echo "<script src='https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.min.js' type='text/javascript'></script>";

        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $c_email = filter_var($c_email, FILTER_SANITIZE_EMAIL); // Trim is needed before FILTEER so that extra spaces are trimmed first
        // Make Sure we dont have any blank or empty field
        if (isset($name) && !empty($name) && $name !== '' && isset($email) && !empty($email) && $email !== '' && isset($c_email) && !empty($c_email) && $c_email !== '' && isset($mobile) && !empty($mobile) && $mobile !== '') {

            if (strcmp($email, $c_email) == 0) {
                $dbh = new PDO($sco_dsn, $sco_login_user, $sco_login_pass);
                $dbh->query("use sohdbl");
				
			
                // Check if an already exits with this email address. If an entry in table +user_temp+ then an account exists with thi email addres, may or may not be activated account
                $stmt01 = $dbh->prepare("SELECT * FROM user_temp WHERE email=? LIMIT 1");
                $stmt01->execute(array($email));
			
                if ($stmt01->rowCount() != 0) {
                    # One row exits in database that means some account already exists. Now check if the account is activated or not.
                    # If account is activated, Display your account already exits, Please login
                    # IF account not activated redirect to payment gateway
                    $result = $stmt01->fetch();

                    if ($result['activated'] == 0) {
                        # Account is not activated, We will update name and mobile no. and proceed them to payment gateway
                        $token = $result['token'];

                        # Update name, order id and mobile no. into the user temp table 
                        $stmt02 = $dbh->prepare("UPDATE user_temp SET name = ? , mobile = ?, token = ? WHERE email=? LIMIT 1");
                        $stmt02->execute(array($encrypted_name, $mobile, $token, $email));
						
						#include 'confirm_email_account.php';
	
                        # Resend activation email
						#send_activation_mail($email, $name, $token);
						 
						$signup_status = 1;
                    } else {
                        # Account is activated, Will suggest to login or recover password
                        $signup_status = 1;
                    }
                } else {
                    # No row exits in table with this email, hence an account does not exits.
                    # So we will create a new account here
										
                    date_default_timezone_set("Asia/Kolkata");
                    $timestamp = date("Y-m-d H:i:s");

                    # Create a utm code
                    $utm_code = create_unique_id();

                    # Create a add_info_token
                    $add_info_code = create_unique_id();

                    # Create a ref_info_token
                    $ref_type = "CORP";
                    $ref_token = $corp_id;

                    # Encrypt Name
                    $encrypted_name = encrypt($name, $encryption_key);

                    # Inserting new row in the user temp table
                    $stmt03 = $dbh->prepare("INSERT INTO user_temp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
                    $stmt03->execute(array($email, $encrypted_name, '', $mobile, $ref_type, $ref_token, $utm_code, $add_info_code, '', '', $timestamp, '0', '', ''));

                    # Inserting new row in the user utm tracking table
                    $stmt04 = $dbh->prepare("INSERT INTO user_utm_tracking VALUES(?,?,?,?,?,?,?);");
                    $stmt04->execute(array($utm_code, $utm_src_req, $utm_med_req, $utm_cmp_req, $utm_url_req, '', ''));

                    # Inserting a new row in the user additional info table
                    $stmt05 = $dbh->prepare("INSERT INTO user_add_info VALUES(?,?,?,?,?,?,?,?);");
                    $stmt05->execute(array($add_info_code, '', '', '', '', '', '', ''));
					
					echo " <script>
						  $.post( 'success.php', { employee_id:'$employee_id', email: '$email', location :'$location', vertical:'$vertical',process: '$process'})
						  .done(function( status) {
							if(status=='done'){
						  $('.confirm-msg' ).replaceWith(
						  '<div class=confirm-msg style=color:#3AB149;><b>$email <b>is now stresscontrolonline user!</div>' );
						  }else{
						  $('.confirm-msg' ).replaceWith(
						  '<div class=confirm-msg style=color:red;><b>$email <b>is not able to register!<br> Please Try again !</div>' );
						  }
						  });
						  </script>";								
					
                }
            } else {
                $signup_status = 2;
            }
        } else {
            //  something is wrong the client side validation 
            //  either the client has disabled the javascript stop the execution of the script
            die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@silveroakhealth.com");
        }
    
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Create EWAP SCO User</div>
                                    <div class="panel-body">
                                        <div Style="text-align: center;font-size: 15px;font-weight:bold;">
                                           <?php
											if ($signup_status == 2) {
												echo '<div class="error-msg">';
												echo 'Email is not verified';
												echo '</div>';
											} else if ($signup_status == 1) {
												echo '<div class="error-msg">';
												echo 'Acount already exists';
												echo '</div>';
											}
											?>
                                            <div class="confirm-msg">					
                                            </div>
                                        </div>
                                        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Name </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control" value="<?php echo $employee_name; ?>" name="name" placeholder="User name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Email </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control"  name="email" placeholder="User email" id="email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label">Confirm Email</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" id="c_email" class="form-control"  name="c_email" placeholder="Re-type Email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="control-label col-md-2 col-xs-12">Mobile</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control" id="mobile" placeholder="User Mobile" name="mobile">
                                                </div>

                                            </div>
                                            <br/><br/>
                                            <div class="form-group m-b-0" style="text-align: center;">
												<?php
												echo '<input type="hidden" name="employee_id" value="' . $employee_id . '">';
												echo '<input type="hidden" name="employee_name" value="' . $employee_name . '">';
												echo '<input type="hidden" name="location" value="' . $location . '">';
												echo '<input type="hidden" name="vertical" value="' . $vertical . '">';
												echo '<input type="hidden" name="process" value="' . $process . '">';
												echo '<input type="hidden" name="corp_id" value="' . $corp_id . '">'; ?>
												<input type="hidden" name="utm_src" value="<?php echo $utm_src ?>">
												<input type="hidden" name="utm_med" value="<?php echo $utm_med ?>">
												<input type="hidden" name="utm_cmp" value="<?php echo $utm_cmp; ?>">
												<input type="hidden" name="utm_url" value="<?php echo $utm_url; ?>">
                                                <input type="submit" class="btn btn-info waves-effect waves-light" value="Create User" name="btn-create-user"/> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
					 c_email: {
                        required: true,
                        equalTo: "#email"
                    },
                    mobile: {
                        required: true,
                    }
                },
                messages: {
                    name: {
                        required: "Please enter the full name"
                    },
                    email: {
                        required: "Please enter the email",
                        email: "Invalid Email ID"
                    },
					c_email: {
                        required: "Please enter same email address",
                        email: "Invalid Email ID"
                    },
                    mobile: {
                        required: "Please enter the mobile no."
                    }                   
                }
            });
        </script>     
        <script type="text/javascript">
            //script to enter only alphabets in text box
            $("#r_register_password").focusout(function () {
                var pass1 = document.getElementById("register_password").value;
                var pass2 = document.getElementById("r_register_password").value;
                if (pass1 != pass2) {
                    //alert("Passwords Do not match");
                    $("#r_password").html("Please enter same password");

                } else {
                    $("#r_password").html(" ");
                }
            });
        </script>
    </body>
</html>