<?php
session_start();

if(isset($_REQUEST['email']) &&  isset($_REQUEST['employee_id']) && isset($_REQUEST['location']) && isset($_REQUEST['vertical']) && isset($_REQUEST['process'])){

	# Getting email
	$email = $_REQUEST['email'];
	$location = $_REQUEST['location'];
	$vertical = $_REQUEST['vertical'];
	$process =  $_REQUEST['process'];
	$employee_id = $_REQUEST['employee_id'];

	# Lets Create a new user
	include 'sco_config.php';
	include 'mindnet/functions/crypto_funtions.php';
	include 'mindnet/functions/token.php';
	include 'ewap_uid.php';
	include 'confirm_email_account.php';

	$dbh = new PDO($sco_dsn, $sco_login_user, $sco_login_pass);
	$dbh->query("use sohdbl");

	# Fetch this users name and password from user_temp table, This will be inserted into user_login table
	$stmt10 = $dbh->prepare("SELECT * FROM user_temp WHERE email=? LIMIT 1");
	$stmt10->execute(array($email));
	if ($stmt10->rowCount() != 0) {
		$result10 = $stmt10->fetch();
		$encrypted_name = $result10['name'];
		$hashed_password = $result10['password'];
		$ref_type = $result10['ref_type'];
		$corp_id = $result10['ref_value'];
		$name = decrypt($encrypted_name, $encryption_key);
	} else {
		die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code : CORP_SCCS_EC1");
	}
	date_default_timezone_set("Asia/Kolkata");

	# Calculating The parameters
	$uid = create_new_uid("INCR");

	date_default_timezone_set("Asia/Kolkata");
	$order_done_datetime = date('Y-m-d H:i:s');
	$user_creation_time = date('Y-m-d H:i:s');

	#Calucalting the schudle for the user future sessions, We taking time as end of the day so that the comparing date and times become reliable
	$due_datetime_SSN0 = date('Y-m-d 23:59:59'); // Session 0 is due the same day.
	$due_datetime_SSN1 = date('Y-m-d 23:59:59', strtotime("+1 week"));
	$due_datetime_SSN2 = date('Y-m-d 23:59:59', strtotime("+2 week"));
	$due_datetime_SSN3 = date('Y-m-d 23:59:59', strtotime("+3 week"));
	$due_datetime_SSN4 = date('Y-m-d 23:59:59', strtotime("+4 week"));
	$due_datetime_SSN5 = date('Y-m-d 23:59:59', strtotime("+5 week"));
	$due_datetime_SSN6 = date('Y-m-d 23:59:59', strtotime("+6 week"));
	$due_datetime_SSN7 = date('Y-m-d 23:59:59', strtotime("+7 week"));
	$due_datetime_SSN8 = date('Y-m-d 23:59:59', strtotime("+8 week"));
	$due_datetime_CBTEND = date('Y-m-d 23:59:59', strtotime("+10 week"));

	# Get remote ip address
	$ip_add = $_SERVER['REMOTE_ADDR'];

	# Following task will done as part of transation
	# Creating the correspoding user tables

	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbh->beginTransaction();
	try {

		
		# Inserting new rows for the user
		$stmt20 = $dbh->prepare("INSERT INTO user_login VALUES (?,?,?,?)");
		$stmt20->execute(array($email, $uid, $encrypted_name, $hashed_password));

		# Updating user temp table
		$stmt30 = $dbh->prepare("UPDATE user_temp SET uid=? WHERE email=?  LIMIT 1");
		$stmt30->execute(array($uid, $email));
		# Inserting user id to all the tables
		$stmt40 = $dbh->prepare("INSERT INTO user_profile VALUES (?,?,?,?,?,?,?)");
		$stmt40->execute(array($uid, '', '', '', '', '', ''));

		$stmt50 = $dbh->prepare("INSERT INTO user_location VALUES (?,?,?,?,?,?,?,?)");
		$stmt50->execute(array($uid, '', '', '', '', '', '', ''));

		$stmt60 = $dbh->prepare("INSERT INTO user_time_creation VALUES (?,?,?)");
		$stmt60->execute(array($uid, $user_creation_time, $ip_add));

		$stmt70 = $dbh->prepare("INSERT INTO user_info VALUES (?,?,?,?,?)");
		$stmt70->execute(array($uid, '', '', '', ''));

		$stmt80 = $dbh->prepare("INSERT INTO user_comm_pref VALUES (?,?,?,?,?,?)");
		$stmt80->execute(array($uid, '', '', '', '', ''));

		$stmt90 = $dbh->prepare("INSERT INTO user_progress VALUES "
				. "('$uid','1','SSN0','0','0'),"
				. "('$uid','2','SSN1','0','0'),"
				. "('$uid','3','SSN2','0','0'),"
				. "('$uid','4','SSN3','0','0'),"
				. "('$uid','5','SSN4','0','0'),"
				. "('$uid','6','SSN5','0','0'),"
				. "('$uid','7','SSN6','0','0'),"
				. "('$uid','8','SSN7','0','0'),"
				. "('$uid','9','SSN8','0','0');");
		$stmt90->execute(array());

		$stmt100 = $dbh->prepare("INSERT INTO user_time_ssn VALUES "
				. "('$uid','SSN0','',''),"
				. "('$uid','SSN1','',''),"
				. "('$uid','SSN2','',''),"
				. "('$uid','SSN3','',''),"
				. "('$uid','SSN4','',''),"
				. "('$uid','SSN5','',''),"
				. "('$uid','SSN6','',''),"
				. "('$uid','SSN7','',''),"
				. "('$uid','SSN8','','');");
		$stmt100->execute(array());

		$stmt110 = $dbh->prepare("INSERT INTO user_schedule VALUES "
				. "('$uid','SSN0','$due_datetime_SSN0'),"
				. "('$uid','SSN1','$due_datetime_SSN1'),"
				. "('$uid','SSN2','$due_datetime_SSN2'),"
				. "('$uid','SSN3','$due_datetime_SSN3'),"
				. "('$uid','SSN4','$due_datetime_SSN4'),"
				. "('$uid','SSN5','$due_datetime_SSN5'),"
				. "('$uid','SSN6','$due_datetime_SSN6'),"
				. "('$uid','SSN7','$due_datetime_SSN7'),"
				. "('$uid','SSN8','$due_datetime_SSN8'),"
				. "('$uid','CBTEND','$due_datetime_CBTEND');");
		$stmt110->execute(array());

		$stmt120 = $dbh->prepare("INSERT INTO user_time_all VALUES (?,?,?)");
		$stmt120->execute(array($uid, $user_creation_time, ''));

		$stmt150 = $dbh->prepare("UPDATE corp_users_count SET user_count=user_count+1 WHERE corp_id=?");
		$stmt150->execute(array($corp_id));

		$stmt160 = $dbh->prepare("SELECT product FROM corp_license WHERE corp_id=? LIMIT 1");
		$stmt160->execute(array($corp_id));
		if ($stmt160->rowCount() != 0) {
			$result160 = $stmt160->fetch();
			$product = $result160['product'];
		} else {
			die("OOPS !!! Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code : PRODUCT_NF");
		}
		
		
		# create a email token
		$token = create_email_token();

		#update the user_temp to add the email token into the table
		$stmt150 = $dbh->prepare("UPDATE user_temp SET token=? WHERE email=?");
		$stmt150->execute(array($token, $email));

		$stmt170 = $dbh->prepare("INSERT INTO user_type VALUES (?,?,?,?,?,?)");
		$stmt170->execute(array($uid, 'CR', $corp_id, $product, '', ''));

		$stmt180 = $dbh->prepare("INSERT INTO corp_users_list VALUES (?,?)");
		$stmt180->execute(array($corp_id, $uid));

		$stmt190 = $dbh->prepare("INSERT INTO user_profile2 VALUES (?,?,?,?,?,?,?,?)");
		$stmt190->execute(array($uid, '', '', $employee_id, $location, $vertical, $process, ''));
		
		## Asssigns a therapist to the this new user
		## In future Only assign depending upon the prduct choosen
		$tid = "";
		$stmt180 = $dbh->prepare("INSERT INTO user_therapist VALUES (?,?)");
		$stmt180->execute(array($uid, $tid));

		$dbh->commit();
		
		# sending activation mail to user
		send_activation_mail($email, $name, $token);

		echo 'done';		
		
	} catch (PDOException $e) {
		$dbh->rollBack();
	   // echo $e->getMessage();
		die("Some Error Occured. Please try again. If the issue still persists. Send us an email at info@silveroakhealth.com. Error Code : CORP_SCCS_ECR2");
	}
}else{
	die("Some Error Occured. Please try again. If the issue still persists. Send us an email at info@silveroakhealth.com. Error Code : EML_NF");
}
?>