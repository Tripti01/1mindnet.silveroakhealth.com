<?php
#include files
require '../../if_loggedin.php';
include 'mindnet-config.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link href="https://s3-ap-southeast-1.amazonaws.com/scoreg/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav><div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider"><b>Downloadable Forms</b></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="Expense_reimbursement_Form.xls" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> Reimbursement Claim </label></div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="Confidentiality Agreement.docx" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> Confidentiality Agreement</label></div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="FBP Bracket_2017-18 .xlsx" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> FBP (2017-2018) </label></div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="FBP_reimbursement_Form.xls" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> FBP Reimbursement </label></div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 1.5%;">
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="Income-Tax-Calculator-FY-2017-18.xls" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> Income Tax Calculator FY (2017-18) </label></div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="IT Declaration form_2017-18.xls" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> IT Declaration form (2017-18) </label></div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="New Hire Guideline_2017.pdf" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> New Hire Guideline 2017 </label></div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="INCOME TAX READY RECKONER F.Y. 2017-18.pdf" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> Income Tax Ready Reckoner FY (2017-18) </label></div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 1.5%;">
                                            <div class="col-md-3 col-sm-6 col-xs-6" style="text-align: center;">
                                                <a href="FBP Structure guidelines.docx" download ><i class="fa fa-3x fa-file-pdf-o"></i></a>
                                                <div style="margin-top:3%;"><label> FBP Structure Guidelines </label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script src="../../../../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();

                //Runs prettify
                prettyPrint();
            });
        </script>
    </body>
</html>