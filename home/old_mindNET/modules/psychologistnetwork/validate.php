<?php
/*
  1.This page is for validating and manipulating data. Values are fetched from the psy_network to display it on this page.
  2.We can update the data which is previously filled by applying psychologists.
  3. Two additional fields are added which are REMARKS and Profile selected(with yyes or no option)
 */
include 'psy/psy_nw_config.php';
include 'mindnet-host.php';

#if pid is set in url
if (isset($_REQUEST['pid'])) {
    $pid = $_REQUEST['pid'];

    #to set the timezone
    date_default_timezone_set("Asia/Kolkata");

    $currentDateTime = date('Y-m-d H:i:s');
	
	
    #all the values are fetched for particular id to display it on this page.
    $stmt01 = $db->prepare("SELECT * FROM psy_info WHERE pid = ? LIMIT 1");
    $stmt01->execute(array($pid)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : DETNF");
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        $fname = $row01['first_name'];
        $lname = $row01['last_name'];
        $email = $row01['email'];
        $mobile = $row01['mobile'];
        $landline = $row01['landline'];
        $user_yob = $row01['yob'];
        $qual = $row01['qualification'];
        if ($qual != $row01['qualification']) {
            $qual = $_REQUEST['qual_others_text'];
        }
        $position = $row01['position'];
        $yce = $row01['counseling_experience'];
        $state = $row01['state'];
        $city = $row01['city'];
        $timestamp = $row01['timestamp'];
		$verify = $row01['profile_selected'];
		$remarks= $row01['remarks'];
        $PC = $row01['aoi_PC'];
        $OC = $row01['aoi_OC'];
        $WS = $row01['aoi_WS'];
        $WA = $row01['aoi_WA'];
        $EAP = $row01['discipline_eap'];
        $PT = $row01['discipline_psychotherapy'];
        $WSE = $row01['discipline_workshops'];
        $PM = $row01['discipline_pyschometry'];
		if ($row01['discipline_other'] != '0') {
        $dis_others = $row01['discipline_other'];
			} else {
					$dis_others = "";
			}
        
		#initialization for language array
        $l = 0;
        $stmt04 = $db->prepare("SELECT language FROM psy_info WHERE pid = ? LIMIT 1");
        $stmt04->execute(array($pid)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : DETNF");
        if ($stmt04->rowCount() != 0) {
            $row04 = $stmt04->fetch();
            $language = $row04['language'];
            $language = explode(",", $language);
        } else {
            die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : VALPN");
        }
    }
}


# Initialize the status_code code to 0
$status_code = 0;

# On form submission check if all the form fields are submitted
if (isset($_REQUEST['submit_btn'])) {

    //include file
    include 'psy/psy_nw_config.php';

    // Check if all the fields are set
    if (isset($_REQUEST['fname']) && !empty($_REQUEST['fname']) && $_REQUEST['fname'] !== '' && isset($_REQUEST['lname']) && !empty($_REQUEST['lname']) && $_REQUEST['lname'] !== '' && isset($_REQUEST['email']) && !empty($_REQUEST['email']) && $_REQUEST['email'] !== '' && isset($_REQUEST['yob']) && !empty($_REQUEST['yob']) && $_REQUEST['yob'] !== '' && isset($_REQUEST['mobile']) && !empty($_REQUEST['mobile']) && $_REQUEST['mobile'] !== '' && isset($_REQUEST['state']) && !empty($_REQUEST['state']) && $_REQUEST['state'] !== '' && isset($_REQUEST['city']) && !empty($_REQUEST['city']) && $_REQUEST['city'] !== '' && isset($_REQUEST['position']) && !empty($_REQUEST['position']) && $_REQUEST['position'] !== '' && isset($_REQUEST['yce']) && !empty($_REQUEST['yce']) && $_REQUEST['yce'] !== '' && (isset($_REQUEST ['qual']) && $_REQUEST ['qual'] != "") && (isset($_REQUEST ['profile_selected']) && $_REQUEST ['profile_selected'] != "")) {

        $pid = $_REQUEST['pid'];
        # Recieve values from form
        $fname = $_REQUEST['fname'];
        $lname = $_REQUEST['lname'];
        $email = $_REQUEST['email'];
        $mobile = $_REQUEST['mobile'];
        $yob = $_REQUEST['yob'];
        $qual = $_REQUEST['qual'];
        if ($qual == "others") {
            if (isset($_REQUEST['qual_others_text'])) {
                $qual = $_REQUEST['qual_others_text'];
            }
        }
        $position = $_REQUEST['position'];
        $yce = $_REQUEST['yce'];
        $state = $_REQUEST['state'];
        $city = $_REQUEST['city'];
        $check1 = $_REQUEST['check1'];
        $check2 = $_REQUEST['check2'];
        $languages = '';
        $check3 = $_REQUEST['check3'];

        $remarks = $_REQUEST['remarks'];
        $verify = $_REQUEST['profile_selected'];

        #declaration for area of expertise
        $EAP = 0;
        $PT = 0;
        $WSE = 0;
        $PM = 0;
        $dis_others = 0;

        #declaration for area of colloboration
        $PC = 0;
        $OC = 0;
        $WS = 0;
        $WA = 0;

        if (isset($_REQUEST['landline'])) {
            $landline = $_REQUEST['landline'];
        } else {
            $landline = "";
        }
        #checkbox language
        for ($i = 0; $i < count($check2); $i++) {
            $languages = $languages . $check2[$i] . ",";
        }
        #checkbox colloborate
        for ($i = 0; $i < count($check1); $i++) {
            $colloborate = $check1[$i];
            if ($colloborate == "PC") {
                $PC = 1;
            }
            if ($colloborate == "OC") {
                $OC = 1;
            }
            if ($colloborate == "WS") {
                $WS = 1;
            }
            if ($colloborate == "WA") {
                $WA = 1;
            }
        }

        #checkbox Area of expertise
        for ($i = 0; $i < count($check3); $i++) {
            $expertise = $check3[$i];
            if ($expertise == "EAP") {
                $EAP = 1;
            }
            if ($expertise == "PT") {
                $PT = 1;
            }
            if ($expertise == "WS") {
                $WSE = 1;
            }
            if ($expertise == "PM") {
                $PM = 1;
            }
            if ($expertise == "others") {
                if (isset($_REQUEST['disc_others_text'])) {
                    $dis_others = $_REQUEST['disc_others_text'];
                }
            }
        }


        $db = new PDO($dsn, $psy_network_user, $psy_network_pass);
        $db->query("use psy_network");

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        # Begin creating tables
        $db->beginTransaction();

        try {

            $stmt05 = $db->prepare("UPDATE  psy_info SET first_name=?, last_name=?,email=?, yob=?, landline=?, mobile=?, city=?, state=?, aoi_PC=?, aoi_OC=?, aoi_WS=?, aoi_WA=?, t1=?, t2=?, language=?, qualification = ?, position = ?, counseling_experience=?, discipline_eap=?, discipline_psychotherapy=?, discipline_workshops=?, discipline_pyschometry=?, discipline_other=?, remarks=?, profile_selected=?, t5=?, timestamp=? WHERE pid=?");
            $stmt05->execute(array($fname, $lname, $email, $yob, $landline, $mobile, $city, $state, $PC, $OC, $WS, $WA, '', '', $languages, $qual, $position, $yce, $EAP, $PT, $WSE, $PM, $dis_others, $remarks, $verify, '', $currentDateTime, $pid)) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : SOHPN_098");
            $db->commit();
            $status_code = 1;
        } catch (PDOException $e) {
            $db->rollBack();
            echo $e->getMessage();
            die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code : SOHPN_03");
        }
    } else {
        $status_code = 2; //Some fields are empty
    }
    echo '<script>window.top.location.href="validate.php?pid=' . $pid . '&status_code=' . $status_code . '</script>';
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <title> Silver Oak Health | Psychologist Network </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <link href="https://s3-ap-southeast-1.amazonaws.com/cronus1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://s3-ap-southeast-1.amazonaws.com/cronus1/css/core.css" rel="stylesheet" type="text/css" />
        <link href="https://s3-ap-southeast-1.amazonaws.com/cronus1/css/components.css" rel="stylesheet" type="text/css" />
        <link href="https://s3-ap-southeast-1.amazonaws.com/cronus1/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="https://s3-ap-southeast-1.amazonaws.com/cronus1/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="https://s3-ap-southeast-1.amazonaws.com/scoreg/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://s3-ap-southeast-1.amazonaws.com/cronus1/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="https://s3-ap-south-1.amazonaws.com/sohcdn2/css/components-rounded.css"></script>
    <style>
        html,body{
            width:100%;
            overflow-x:hidden;
        }
    </style>
</head>
<body style="background: #eff3f8">
    <div class='text-center' style="width:100%;background-color:white;">
        <div class='row'>
            <div class='col-md-12'>
                <div style="text-align: center; margin: 15px 0 10px 0;"> <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" alt="SilverOakHealth" height="70" width="90"> </div>
                <p style="text-align:center;font-size:24px;">  SilverOakHealth's Psychologist Network </p>
            </div>
        </div>
    </div>

    <div class="page-content" >
        <div class="row">
            <div class="col-md-2">
                &nbsp;
            </div>
            <div class="col-md-8">
                <p><a class="btn btn-primary" style ="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin-top:2%;" href="index.php">Back</a></p>
            </div>
            <div class="col-md-2">
                &nbsp;
            </div>
        </div>
        <div class="row" style="margin-top:2%;">
            <div class="col-md-2">
                &nbsp;
            </div>
            <div class="col-md-8" style="y-overflow:auto;">
                <div class="card-box" style="border:2px solid #D3D3D3;">
                    <article>
                        <section>
                            <div class="row" style='margin-top:3%;'> 
                                <div class="email-confirm-msg" style="margin-top:2px;">
                                    <?php
									#displaying success or failure messages
                                    if ($status_code != 0) {
                                        switch ($status_code) {

                                            case 1:
                                                echo '<div class="alert alert-success" style="text-align:center;">';
                                                echo "<strong>Profile updated successfully.<br/>Thank You</strong>";
                                                echo '</div>';
                                                break;
                                            case 2:
                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                echo "<strong>Some of the fields are empty.</strong>";
                                                echo '</div>';
                                                break;

                                            default: break;
                                        }
                                    }
                                    ?>
                                </div>
                                <form action="validate.php" method="POST"   class="form-horizontal" id="form" name="form-horizontal" onClick="validateChecks(this);">
                                    <!-- Name -->  
                                    <div class="form-group">
                                        <label class="col-md-12 label_css" style="margin-left:2%;margin-bottom:1px;color:green;font-size:20px;">Basic Information</label>
                                    </div>
                                    <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:3%;margin-top:-1%;"/>

                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-2 control-label" for="userName"  style="font-weight:normal;text-align:left;">First Name <span style="color:red;">*</span></label>
                                        <div class="col-md-3">
                                            <input type="text" placeholder="Enter First Name" class="form-control" id="fname" name="fname" value="<?php echo $fname; ?>" />
                                            <div id="err_fname" style="color:red"></div>
                                        </div>
                                        <label class="col-md-2 control-label"  style="font-weight:normal;text-align:left;">Last Name <span style="color:red;">*</span></label>
                                        <div class="col-md-3">
                                            <input type="text" placeholder="Enter Last Name" class="form-control" id="lname" value="<?php echo $lname; ?>" name="lname" />
                                            <div id="err_lname" style="color:red"></div>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-2 control-label" for="email"  style="font-weight:normal;text-align:left;">Email Address <span style="color:red;">*</span></label>
                                        <div class="col-md-3">
                                            <input type="text" name="email" placeholder="Enter Email Address" class="form-control" id="email" value="<?php echo $email; ?>">
                                            <div id="err_email" style="color:red"></div>
                                        </div>
                                        <label class="col-md-2 control-label" style="font-weight:normal;text-align:left;"> Year Of Birth <font color="RED"> * </font> </label>
                                        <div class="col-md-3" style="text-align:left;">
                                            <select id="yob" name="yob" title="" class="form-control">
                                                <option value="" disabled="" selected="">  -- SELECT -- </option> 
                                                <option value="2010" <?php echo ($user_yob == 2010) ? "selected" : "" ?>>2010</option>
                                                <option value="2009"<?php echo ($user_yob == 2009) ? "selected" : "" ?>>2009</option>
                                                <option value="2008"<?php echo ($user_yob == 2008) ? "selected" : "" ?>>2008</option>
                                                <option value="2007" <?php echo ($user_yob == 2007) ? "selected" : "" ?>>2007</option>
                                                <option value="2006" <?php echo ($user_yob == 2006) ? "selected" : "" ?>>2006</option>
                                                <option value="2005" <?php echo ($user_yob == 2005) ? "selected" : "" ?>>2005</option>
                                                <option value="2004"<?php echo ($user_yob == 2004) ? "selected" : "" ?>>2004</option>
                                                <option value="2003"<?php echo ($user_yob == 2003) ? "selected" : "" ?>>2003</option>
                                                <option value="2002"<?php echo ($user_yob == 2002) ? "selected" : "" ?>>2002</option>
                                                <option value="2001"<?php echo ($user_yob == 2001) ? "selected" : "" ?>>2001</option>
                                                <option value="2000"<?php echo ($user_yob == 2000) ? "selected" : "" ?>>2000</option>
                                                <option value="1999"<?php echo ($user_yob == 1999) ? "selected" : "" ?>>1999</option>
                                                <option value="1998" <?php echo ($user_yob == 1998) ? "selected" : "" ?>>1998</option>
                                                <option value="1997"<?php echo ($user_yob == 1997) ? "selected" : "" ?>>1997</option>
                                                <option value="1996" <?php echo ($user_yob == 1996) ? "selected" : "" ?>>1996</option>
                                                <option value="1995" <?php echo ($user_yob == 1995) ? "selected" : "" ?>>1995</option>
                                                <option value="1994" <?php echo ($user_yob == 1994) ? "selected" : "" ?>>1994</option>
                                                <option value="1993" <?php echo ($user_yob == 1993) ? "selected" : "" ?>>1993</option>
                                                <option value="1992" <?php echo ($user_yob == 1992) ? "selected" : "" ?>>1992</option>
                                                <option value="1991" <?php echo ($user_yob == 1991) ? "selected" : "" ?>>1991</option>
                                                <option value="1990" <?php echo ($user_yob == 1990) ? "selected" : "" ?>>1990</option>
                                                <option value="1989" <?php echo ($user_yob == 1989) ? "selected" : "" ?>>1989</option>
                                                <option value="1988" <?php echo ($user_yob == 1988) ? "selected" : "" ?>>1988</option>
                                                <option value="1987" <?php echo ($user_yob == 1987) ? "selected" : "" ?>>1987</option>
                                                <option value="1986" <?php echo ($user_yob == 1986) ? "selected" : "" ?>>1986</option>
                                                <option value="1985" <?php echo ($user_yob == 1985) ? "selected" : "" ?>>1985</option>
                                                <option value="1984" <?php echo ($user_yob == 1984) ? "selected" : "" ?>>1984</option>
                                                <option value="1983" <?php echo ($user_yob == 1983) ? "selected" : "" ?>>1983</option>
                                                <option value="1982" <?php echo ($user_yob == 1982) ? "selected" : "" ?>>1982</option>
                                                <option value="1981" <?php echo ($user_yob == 1981) ? "selected" : "" ?>>1981</option>
                                                <option value="1980" <?php echo ($user_yob == 1980) ? "selected" : "" ?>>1980</option>
                                                <option value="1979" <?php echo ($user_yob == 1979) ? "selected" : "" ?>>1979</option>
                                                <option value="1978" <?php echo ($user_yob == 1978) ? "selected" : "" ?>>1978</option>
                                                <option value="1977" <?php echo ($user_yob == 1977) ? "selected" : "" ?>>1977</option>
                                                <option value="1976" <?php echo ($user_yob == 1976) ? "selected" : "" ?>>1976</option>
                                                <option value="1975" <?php echo ($user_yob == 1975) ? "selected" : "" ?>>1975</option>                                       
                                                <option value="1974" <?php echo ($user_yob == 1974) ? "selected" : "" ?>>1974</option>
                                                <option value="1973" <?php echo ($user_yob == 1973) ? "selected" : "" ?>>1973</option>
                                                <option value="1972" <?php echo ($user_yob == 1972) ? "selected" : "" ?>>1972</option>
                                                <option value="1971" <?php echo ($user_yob == 1971) ? "selected" : "" ?>>1971</option>
                                                <option value="1970" <?php echo ($user_yob == 1970) ? "selected" : "" ?>>1970</option>
                                                <option value="1969" <?php echo ($user_yob == 1969) ? "selected" : "" ?>>1969</option>
                                                <option value="1968" <?php echo ($user_yob == 1968) ? "selected" : "" ?>>1968</option>
                                                <option value="1967" <?php echo ($user_yob == 1967) ? "selected" : "" ?>>1967</option>
                                                <option value="1966" <?php echo ($user_yob == 1966) ? "selected" : "" ?>>1966</option>
                                                <option value="1965" <?php echo ($user_yob == 1965) ? "selected" : "" ?>>1965</option>
                                                <option value="1964" <?php echo ($user_yob == 1964) ? "selected" : "" ?>>1964</option>
                                                <option value="1963" <?php echo ($user_yob == 1963) ? "selected" : "" ?>>1963</option>
                                                <option value="1962" <?php echo ($user_yob == 1962) ? "selected" : "" ?>>1962</option>
                                                <option value="1961" <?php echo ($user_yob == 1961) ? "selected" : "" ?>>1961</option>
                                                <option value="1960" <?php echo ($user_yob == 1960) ? "selected" : "" ?>>1960</option>
                                                <option value="1959" <?php echo ($user_yob == 1959) ? "selected" : "" ?>>1959</option>
                                                <option value="1958" <?php echo ($user_yob == 1958) ? "selected" : "" ?>>1958</option>
                                                <option value="1957" <?php echo ($user_yob == 1957) ? "selected" : "" ?>>1957</option>
                                                <option value="1956" <?php echo ($user_yob == 1956) ? "selected" : "" ?>>1956</option>
                                                <option value="1955" <?php echo ($user_yob == 1955) ? "selected" : "" ?>>1955</option>
                                                <option value="1954" <?php echo ($user_yob == 1954) ? "selected" : "" ?>>1954</option>
                                                <option value="1953" <?php echo ($user_yob == 1953) ? "selected" : "" ?>>1953</option>
                                                <option value="1952" <?php echo ($user_yob == 1952) ? "selected" : "" ?>>1952</option>
                                                <option value="1951" <?php echo ($user_yob == 1951) ? "selected" : "" ?>>1951</option>
                                                <option value="1950" <?php echo ($user_yob == 1950) ? "selected" : "" ?>>1950</option>
                                                <option value="1949" <?php echo ($user_yob == 1949) ? "selected" : "" ?>>1949</option>
                                                <option value="1948" <?php echo ($user_yob == 1948) ? "selected" : "" ?>>1948</option>
                                                <option value="1947" <?php echo ($user_yob == 1947) ? "selected" : "" ?>>1947</option>
                                                <option value="1946" <?php echo ($user_yob == 1946) ? "selected" : "" ?>>1946</option>
                                                <option value="1945" <?php echo ($user_yob == 1945) ? "selected" : "" ?>>1945</option>
                                                <option value="1944" <?php echo ($user_yob == 1944) ? "selected" : "" ?>>1944</option>
                                                <option value="1943" <?php echo ($user_yob == 1943) ? "selected" : "" ?>>1943</option>
                                                <option value="1942" <?php echo ($user_yob == 1942) ? "selected" : "" ?>>1942</option>
                                                <option value="1941" <?php echo ($user_yob == 1941) ? "selected" : "" ?>>1941</option>
                                                <option value="1940" <?php echo ($user_yob == 1940) ? "selected" : "" ?>>1940</option>
                                                <option value="1939" <?php echo ($user_yob == 1939) ? "selected" : "" ?>>1939</option>
                                                <option value="1938" <?php echo ($user_yob == 1938) ? "selected" : "" ?>>1938</option>
                                                <option value="1937" <?php echo ($user_yob == 1937) ? "selected" : "" ?>>1937</option>
                                                <option value="1936" <?php echo ($user_yob == 1936) ? "selected" : "" ?>>1936</option>
                                                <option value="1935" <?php echo ($user_yob == 1935) ? "selected" : "" ?>>1935</option>
                                                <option value="1934" <?php echo ($user_yob == 1934) ? "selected" : "" ?>>1934</option>
                                                <option value="1933" <?php echo ($user_yob == 1933) ? "selected" : "" ?>>1933</option>
                                                <option value="1932" <?php echo ($user_yob == 1932) ? "selected" : "" ?>>1932</option>
                                                <option value="1931" <?php echo ($user_yob == 1931) ? "selected" : "" ?>>1931</option>
                                                <option value="1930" <?php echo ($user_yob == 1930) ? "selected" : "" ?>>1930</option>
                                                <option value="1929" <?php echo ($user_yob == 1929) ? "selected" : "" ?>>1929</option>
                                                <option value="1928" <?php echo ($user_yob == 1928) ? "selected" : "" ?>>1928</option>
                                                <option value="1927" <?php echo ($user_yob == 1927) ? "selected" : "" ?>>1927</option>
                                                <option value="1926" <?php echo ($user_yob == 1926) ? "selected" : "" ?>>1926</option>
                                                <option value="1925" <?php echo ($user_yob == 1925) ? "selected" : "" ?>>1925</option>
                                                <option value="1924" <?php echo ($user_yob == 1924) ? "selected" : "" ?>>1924</option>
                                                <option value="1923" <?php echo ($user_yob == 1923) ? "selected" : "" ?>>1923</option>
                                                <option value="1922" <?php echo ($user_yob == 1922) ? "selected" : "" ?>>1922</option>
                                                <option value="1921" <?php echo ($user_yob == 1921) ? "selected" : "" ?>>1921</option>
                                                <option value="1920" <?php echo ($user_yob == 1920) ? "selected" : "" ?>>1920</option>
                                                <option value="1919" <?php echo ($user_yob == 1919) ? "selected" : "" ?>>1919</option>
                                                <option value="1918" <?php echo ($user_yob == 1918) ? "selected" : "" ?>>1918</option>
                                                <option value="1917" <?php echo ($user_yob == 1917) ? "selected" : "" ?>>1917</option>
                                                <option value="1916" <?php echo ($user_yob == 1916) ? "selected" : "" ?>>1916</option>
                                                <option value="1915" <?php echo ($user_yob == 1915) ? "selected" : "" ?>>1915</option>
                                                <option value="1914" <?php echo ($user_yob == 1914) ? "selected" : "" ?>>1914</option>
                                                <option value="1913" <?php echo ($user_yob == 1913) ? "selected" : "" ?>>1913</option>
                                                <option value="1912" <?php echo ($user_yob == 1912) ? "selected" : "" ?>>1912</option>
                                                <option value="1911" <?php echo ($user_yob == 1911) ? "selected" : "" ?>>1911</option>
                                                <option value="1910" <?php echo ($user_yob == 1910) ? "selected" : "" ?>>1910</option>
                                                <option value="1909" <?php echo ($user_yob == 1909) ? "selected" : "" ?>>1909</option>
                                                <option value="1908" <?php echo ($user_yob == 1908) ? "selected" : "" ?>>1908</option>
                                                <option value="1907" <?php echo ($user_yob == 1907) ? "selected" : "" ?>>1907</option>
                                                <option value="1906" <?php echo ($user_yob == 1906) ? "selected" : "" ?>>1906</option>
                                                <option value="1905" <?php echo ($user_yob == 1905) ? "selected" : "" ?>>1905</option>
                                                <option value="1904" <?php echo ($user_yob == 1904) ? "selected" : "" ?>>1904</option>
                                                <option value="1903" <?php echo ($user_yob == 1903) ? "selected" : "" ?>>1903</option>
                                                <option value="1902" <?php echo ($user_yob == 1902) ? "selected" : "" ?>>1902</option>
                                                <option value="1901" <?php echo ($user_yob == 1901) ? "selected" : "" ?>>1901</option>
                                                <option value="1900" <?php echo ($user_yob == 1900) ? "selected" : "" ?>>1900</option>
                                            </select>
                                            <div id="err_yob" style="color:red"></div>
                                        </div>

                                    </div>
                                    <!-- Phone-->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-2 control-label" for="phone"  style="font-weight:normal;text-align:left;">Mobile<span style="color:red;"> *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></label>
                                        <div class="col-md-3">
                                            <input type="mobile" placeholder="Mobile" class="form-control" value="<?php echo $mobile; ?>" id="mobile" name="mobile" />
                                            <div id="err_mobile" style="color:red"></div>

                                        </div>
                                        <label class="col-md-2 control-label" for="mobile" style="font-weight:normal;text-align:left;">Landline(Optional)</label>
                                        <div class="col-md-3">
                                            <input type="telephone" placeholder="Telephone" name="landline" id="landline" value="<?php echo $landline; ?>" class="form-control" />
                                            <div id="err_landline" style="color:red"></div>

                                        </div>
                                    </div>
                                    <!-- Current City-->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-2 control-label" for="state" style="font-weight:normal;text-align:left;">State <span style="color:red;">*</span></label>
                                        <div class="col-md-3" style="">
                                            <select name="state"  id="state" class="form-control state" style="color:#999999">
                                                <option value="" disabled="" selected="">--SELECT STATE--</option>
                                                <?php
                                                $stmt = $db->prepare("SELECT DISTINCT(state) FROM statelist  ORDER BY state ASC ");
                                                $stmt->execute();
                                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $row['state']; ?>" <?php echo ($state == $row['state']) ? "selected" : "" ?> > <?php echo ($state == $row['state']) ? $state : $row['state']; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <div id="err_state" style="color:red"></div>
                                        </div>
                                        <label class="col-md-2 control-label" for="city"  style="font-weight:normal;text-align:left;">City<span style="color:red;"> *</span></label>
                                        <div class="col-md-3">
                                            <select name="city" id="city" class="form-control city " style="color:#999999" >
                                                <option value="" disabled="" selected=""></option>
                                            </select>
                                            <div id="err_city" style="color:red"></div>
                                        </div>
                                    </div>

                                    <!-- area of interest -->
                                    <div class="form-group">
                                        <label class="col-md-12 label_css" style="margin-top: 8px;margin-left:2%;margin-bottom:1px;color:green;font-size:20px;">Areas Of Interest</label>
                                    </div>
                                    <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:3%;margin-top:-1%;"/>

                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-11 label_css" style="margin-bottom:-5px;">Areas you would like to collaborate with SilverOakHealth<span style="color:red;"> *</span></label>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="A1" name="check1[]" type="checkbox" value="PC" <?php echo ($PC == '1') ? "checked" : "" ?>>
                                            <label for="A1">
                                                Phone Counselling
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="A2" name="check1[]" type="checkbox" value="OC" <?php echo ($OC == '1') ? "checked" : "" ?>>
                                            <label for="A2">
                                                Onsite Counselling
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="A3" name="check1[]" type="checkbox" value="WS" <?php echo ($WS == '1') ? "checked" : "" ?>>
                                            <label for="A3">
                                                Workshops
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="A4" name="check1[]" type="checkbox" value="WA" <?php echo ($PC == '1') ? "checked" : "" ?>>
                                            <label for="A4">
                                                Writing Articles
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div id="err_checkbox1" class="error col-md-3" style="font-size:13px;">
                                            <!--message is displayed for validation-->
                                        </div>
                                    </div>

                                    <!-- Profienct Language-->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-11 control-label" for="pass1" style="font-weight:bold;text-align:left;margin-top:2%;">Please list all languages in which you have Speaking proficiency <span style="color:red;">*</span></label>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B1" name="check2[]" type="checkbox" value="English" <?php
                                            if (in_array("English", $language)) {
                                                echo "checked";
                                            }
                                            ?>/>
                                            <label for="B1">
                                                English
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B3" name="check2[]" type="checkbox" value="Hindi" <?php
                                            if (in_array("Hindi", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B3">
                                                Hindi
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B4" name="check2[]" type="checkbox" value="Bengali" <?php
                                            if (in_array("Bengali", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B4">
                                                Bengali
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B5" name="check2[]" type="checkbox" value="Telugu" <?php
                                            if (in_array("Telugu", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B5">
                                                Telugu
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B6" name="check2[]" type="checkbox" value="Marathi" <?php
                                            if (in_array("Marathi", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B6">
                                                Marathi
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B7" name="check2[]" type="checkbox" value="Tamil" <?php
                                            if (in_array("Tamil", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B7">
                                                Tamil
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B8" name="check2[]" type="checkbox" value="Urdu" <?php
                                            if (in_array("Urdu", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B8">
                                                Urdu
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B9" name="check2[]" type="checkbox" value="Gujarati" <?php
                                            if (in_array("Gujarati", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B9">
                                                Gujarati
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B10" name="check2[]" type="checkbox" value="Kannada" <?php
                                            if (in_array("Kannada", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B10">
                                                Kannada
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B11" name="check2[]" type="checkbox" value="Malyalam" <?php
                                            if (in_array("Malyalam", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B11">
                                                Malyalam
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>

                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B12" name="check2[]" type="checkbox" value="Odia" <?php
                                            if (in_array("Odia", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B12">
                                                Odia
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B13" name="check2[]" type="checkbox" value="Punjabi" <?php
                                            if (in_array("Punjabi", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B13">
                                                Punjabi
                                            </label>
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input id="B2" name="check2[]" type="checkbox" value="Assamese" <?php
                                            if (in_array("Assamese", $language)) {
                                                echo "checked";
                                            }
                                            ?>>
                                            <label for="B2">
                                                Assamese
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div id="err_checkbox2" class="error col-md-3" style="font-size:13px;">
                                            <!--message is displayed for validation-->
                                        </div>
                                    </div>

                                    <!--Highest Qualification -->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-11 label_css" style="margin-bottom:-5px;margin-top:2%;">Highest Qualification <span style="color:red;">*</span></label>
                                    </div>
                                    <div class="row" style="margin-left:0.1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input id="H1" name="qual" type="radio" value="PHD" <?php echo ($qual == 'PHD') ? "checked" : "" ?>>
                                            <label for="H1" style="font-weight:400;">
                                                PHD
                                            </label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input id="H2" name="qual" type="radio" value="MPhil" <?php echo ($qual == 'MPhil') ? "checked" : "" ?>>
                                            <label for="H2" style="font-weight:400;">
                                                MPhil
                                            </label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input id="H3" name="qual" type="radio" value="MSc" <?php echo ($qual == 'MSc') ? "checked" : "" ?>>
                                            <label for="H3" style="font-weight:400;">
                                                MSc
                                            </label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input id="H4" name="qual" type="radio" value="MA" <?php echo ($qual == 'MA') ? "checked" : "" ?>>
                                            <label for="H4" style="font-weight:400;">
                                                MA
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom:3%;margin-left:0.1%;" >
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input id="H5" name="qual" type="radio" value="others" <?php echo ($qual != 'PHD' && $qual != 'MPhil' && $qual != 'MSc' && $qual != 'MA') ? "checked" : "" ?> onClick="EnableTextbox('qual_others_text')" >
                                            <label for="reg_type" style="font-weight:400;"> Others: </label>

                                        </div>
                                        <div class="col-md-8" style="margin-left:-5%;">
                                            <input class="form-control" type="text" name="qual_others_text"  style="width:63%;" disabled  id="qual_others_text" value="<?php if($qual != 'PHD' && $qual != 'MPhil' && $qual != 'MSc' && $qual != 'MA'){echo $qual;} else { echo " ";} ?>" >

                                        </div>
                                    </div>

                                    <div class="row" style="margin-top:-3%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div id="err_qual" style="font-size:13px;"></div>
                                        <!--message is displayed for validation-->
                                    </div>


                                    <!-- current organisation -->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-11 control-label" for="curpos" style="font-weight:bold;text-align:left;margin-bottom:1%;margin-top:2%">Present Position/Current Organization<span style="color:red;"> *</span></label>
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" placeholder="" class="form-control" id="position" value="<?php echo $position; ?>" name="position"/>
                                            <div id="err_position" style="color:red;"></div>
                                        </div>
                                    </div>

                                    <!-- Years of Counseling Experience -->
                                    <div class="form-group" style="margin-top:4%;">
                                        <div class="row">
                                            <div class="col-md-1">
                                                &nbsp;
                                            </div>
                                            <label class="col-md-4 control-label" for="pass1" style="font-weight:bold;text-align:left;margin-bottom:1%;">Years of Counseling Experience <span style="color:red;">*</span></label>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2" style="margin-left:0.6%;">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-4" style="">
                                                <select class="form-control" style="color:#999999" name="yce" id="yce">
                                                    <option  value="" disabled="" selected="">--SELECT--</option>
                                                    <option  value="0-5" <?php echo ($yce == '0-5') ? "selected" : "" ?>>0 to 5 years</option>
                                                    <option  value="6-10" <?php echo ($yce == '6-10') ? "selected" : "" ?>>6 to 10 years</option>
                                                    <option  value="11-15" <?php echo ($yce == '11-15') ? "selected" : "" ?>>11 to 15 years</option>
                                                    <option  value=">15" <?php echo ($yce == '>15') ? "selected" : "" ?>>More than 15 years</option>
                                                </select>
                                                <div id="err_yce" style="color:red"></div>
                                            </div>
                                        </div>	
                                    </div>

                                    <!--Areas of interest -->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-11 label_css" style="margin-bottom:-5px;margin-top:1%;">Areas Of Expertise <span style="color:red;">*</span></label>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="D1" name="check3[]" type="checkbox" value="EAP" <?php echo ($EAP == '1') ? "checked" : "" ?>>
                                            <label for="D1">
                                                EAP
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="D2" name="check3[]" type="checkbox" value="PT" <?php echo ($PT == '1') ? "checked" : "" ?>>
                                            <label for="D2">
                                                Psychotherapy
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="D3" name="check3[]" type="checkbox" value="WS" <?php echo ($WSE == '1') ? "checked" : "" ?>>
                                            <label for="D3">
                                                Workshops
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left:1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-3 checkbox checkbox-primary">
                                            <input id="D4" name="check3[]" type="checkbox" value="PM" <?php echo ($PM == '1') ? "checked" : "" ?>>
                                            <label for="D4">
                                                Psychometry
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom:3%;margin-left:1%;" >
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="form-group col-md-2 checkbox checkbox-primary">
                                            <input type="checkbox"  name="check3[]" id="disc_others" value="others"  <?php echo ($row01['discipline_other'] != '0') ? "checked" : "" ?>  onClick="EnableTextbox('disc_others_text')" >
                                            <label for="disc_others"> Others: </label>

                                        </div>
                                        <div class="col-md-8" style="margin-left:-5%;">
                                            <input class="form-control" type="text" name="disc_others_text"  style="width:63%;" disabled  id="disc_others_text"  value="<?php
                                            if ($row01['discipline_other'] != '0') {
                                                echo $row01['discipline_other'];
                                            } else {
                                               echo ""; 
                                            }
                                            ?>" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div id="err_checkbox4" class="error col-md-3" style="margin-top:-3%;font-size:13px;">
                                        </div>
                                    </div>

                                    <!-- Remarks -->
                                    <div class="form-group">
                                        <div class="row"style="margin-top:-3%;">
                                            <div class="col-md-1">
                                                &nbsp;
                                            </div>
                                            <label class="col-md-10 control-label" for="remarks" style="font-weight:bold;text-align:left;margin-bottom:1%;">Remarks</label>
                                            <div class="col-md-2">
                                                &nbsp;
                                            </div>
                                            <div class="col-md-8">
                                                <textarea type="text" placeholder="" class="form-control" id="remarks" name="remarks" value=""/><?php echo $remarks; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Verification -->
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            &nbsp;
                                        </div>
                                        <label class="col-md-11 label_css" style="margin-bottom:-5px;margin-top:2%;">Profile Selected <span style="color:red;">*</span></label>
                                    </div>
                                    <div class="row" style="margin-left:0.1%;">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input id="v1" name="profile_selected" type="radio" value="1" <?php echo ($verify == '1') ? "checked" : "" ?>>
                                            <label for="v1" style="font-weight:400;">
                                                Yes
                                            </label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input id="v2" name="profile_selected" type="radio" value="0" <?php echo ($verify == '0') ? "checked" : "" ?>>
                                            <label for="v2" style="font-weight:400;">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div id="err_verify" class="error col-md-3" style="margin-top:-1%;font-size:13px;">
                                        </div>
                                    </div>

                                    <div class="form-group m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                        <div class="col-md-8">
                                            &nbsp;
                                        </div>
                                        <input type="hidden" name="pid" value="<?php echo $pid; ?>">
                                        <input type="submit" class="btn btn-primary" style="background:#223c87 !important; border-color: #223c87 !important;font-family:open sans, sans-serif;" value=" Submit"  name = "submit_btn" id="submit_btn">                                          
                                        <button type="reset" class="btn btn-custom" style="background:#b1b2b8 !important; border-color: #b1b2b8 !important;font-family:open sans, sans-serif;">
                                            Clear
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </article>
                </div>
            </div>
            <div class="col-md-2">
                &nbsp;
            </div>
        </div>
    </div>
</div>
<!--[if lt IE 9]>
<script src="../../assets2/global/plugins/respond.min.js"></script>
<script src="../../assets2/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../../../assets2/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets2/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="../../../assets2/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets2/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
<?php if (isset($state)) { ?>
        var id = "<?php echo $state; ?>";
        var city = "<?php echo $city; ?>";
        $.ajax({
				type: "POST",
				url: "get_city.php",
				data: {
					'id': id,
					'city': city,
					},
				cache: false,
				success: function (html)
					{
					$("#loding2").hide();
					$(".city").html(html);
					}
               });

<?php } ?>
        $(document).ready(function ()
            {
				$(".state").change(function ()
				{
				var id = $(this).val();
				var city = ''
				$.ajax({
						type: "POST",
						url: "get_city.php",
						data: {
					   'id': id,
					   'city': city,
						},
						cache: false,
						success: function (html)
						{
						$("#loding2").hide();
						$(".city").html(html);
						}
					});
				});
            });

</script>
<script>
    function EnableTextbox(type)
    {
        if (type == 'disc_others_text') {
            document.getElementById('disc_others_text').disabled = false;
        } else {
            document.getElementById('qual_others_text').disabled = false;
        }
    }
</script>
<script>
    function validate_fields() {
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var mobile = $("#mobile").val();
        var landline = $("#landline").val();
        var email = $("#email").val();
        var yob = $("#yob").val();
        var yce = $("#yce").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var position = $("#position").val();
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (fname != '' && lname != '' && mobile != '' && position != '' && email != '' && email.match(emailReg) != null && yob != null && yce != null) {
            return true;
        } else {
            if (fname == '') {
                $("#err_fname").html("Please enter your name");
            }
            if (yob == null) {
                $("#err_yob").html("Please enter your year of birth");
            }
            if (yce == null) {
                $("#err_yce").html("Please enter your year of experience");
            }
            if (state == null) {
                $("#err_state").html("Please enter your state");
            }
            if (city == null) {
                $("#err_city").html("Please enter your city");
            }
            if (position == '') {
                $("#err_position").html("Please enter your current position");
            }
            if (lname == '') {
                $("#err_lname").html("Please enter your last name");
            }
            if (mobile == '') {
                $("#err_mobile").html("Please enter your mobile");
            } else if (mobile.match(phoneno) == null) {
                $("#err_mobile").html("Invalid mobile number");
            }
            if (email == '') {
                $("#err_email").html("Please enter your email");
            } else if (email.match(emailReg) == null) {
                $("#err_email").html("Invalid email");
            }
            return false;
        }
    }
    $("#mobile").focusout(function () {
        var mobile = $("#mobile").val();
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

        if (mobile.length > 0 && mobile.match(phoneno) != null) {
            $("#err_mobile").html("");
        } else if (mobile.match(phoneno) == null) {
            $("#err_mobile").html("Invalid mobile number");

        }
    });
    $("#fname").focusout(function () {
        var fname = $("#fname").val();
        if (fname.length > 0) {
            $("#err_fname").html("");
        }
    });
    $("#lname").focusout(function () {
        var lname = $("#lname").val();
        if (lname.length > 0) {
            $("#err_lname").html("");
        }
    });
    $("#position").focusout(function () {
        var pos = $("#position").val();
        if (pos.length > 0) {
            $("#err_position").html("");
        }
    });
    $("#email").focusout(function () {
        var email = $("#email").val();
        var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (email.length > 0 && email.match(emailReg) != null) {
            $("#err_email").html("");
        }
    });
    $("#yob").focusout(function () {
        var yob = $("#yob").val();
        if (yob != null) {
            $("#err_yob").html("");
        }
    });
    $("#yce").focusout(function () {
        var yce = $("#yce").val();
        if (yce != null) {
            $("#err_yce").html("");
        }
    });
    $("#state").focusout(function () {
        var s = $("#state").val();
        if (s != null) {
            $("#err_state").html("");
        }
    });
    $("#city").focusout(function () {
        var c = $("#city").val();
        if (c != null) {
            $("#err_city").html("");
        }
    });
</script>
<script type="text/javascript" language="JavaScript">
    function validateChecks(funn) {
        var chks = document.getElementsByName(funn);

        var checkCount = 0;
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) {
                checkCount++;
            }
        }
        if (checkCount < 1) {
            return false;
        }
        return true;
    }
    function validate() {
        err1 = 0;
        err2 = 0;
        err3 = 0;

        if (validateChecks('check1[]') == false) {
            $("#err_checkbox1").html("Mark atleast one option");
            err1 = 0;
        } else
        {
            $("#err_checkbox1").html("");
            err1 = 1;
        }
        if (validateChecks('check2[]') == false) {
            $("#err_checkbox2").html("Mark atleast one option");
            err2 = 0;
        } else
        {
            $("#err_checkbox2").html("");
            err2 = 1;
        }
        if (validateChecks('check3[]') == false) {
            $("#err_checkbox4").html("Mark atleast one option");
            err3 = 0;
        } else
        {
            $("#err_checkbox4").html("");
            err3 = 1;
        }
        if (err1 == 1 && err2 == 1 && err3 == 1) {
            return true;
        } else {
            return false;
        }
    }
    function validateRadio(obj, error_no) {
        var result = 0;
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked == true) {
                result = 1;
                break;
            }
        }
		
		return result;
    }
</script>
<script>
    
	$(document).ready(function () {
        $("form").submit(function () {
        var qual = document.getElementsByName("qual");
        var profile_selected = document.getElementsByName("profile_selected");
		
		if (!validateRadio(qual, 1)) {
                $("#err_qual").html("Please enter your qualification");
               
            } else {

                $("#err_qual").html("");
            }
		
		if (!validateRadio(profile_selected, 1)) {
			   $("#err_verify").html("Please enter one of the options");
               } else {
               $("#err_qual").html("");
               
            }

            var validate_checkbox = validate();
            var fields = validate_fields();
            if (validate_checkbox == true && fields == true) {
                return true;
            } else {
                return false;
            }
        });
    });
	
</script>
</body>
</html>