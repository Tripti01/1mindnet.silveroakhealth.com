<?php
#file inclusion for various function happening in the ui
include 'newap-config.php';
include 'soh-config.php';
include '../../if_loggedin.php';
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Add New Psychologist - Silver Oak Health</title>
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <!-- Plugins css-->
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <script src="assets/js/modernizr.min.js"></script>
        <link href="https://s3-ap-southeast-1.amazonaws.com/scoreg/css/font-awesome.min.css" rel="stylesheet">

        <style>
            .url_div{
                width:80%;
                border:solid .5px #afabab;
                float:left;
                padding:11px 11px;
                border-radius:3px;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="background-image: url('<?php echo $img_loc; ?>'); background-size:cover;background-repeat: no-repeat;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>

            <div class="container">
                <p><a class="btn btn-primary" style ="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin-top:-4%;" href="index.php">Back</a></p>
                 <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-default panel-border-color panel-border-color-primary" style="padding-bottom:2%;">
                            <p style="font-size:15px;font-weight: normal;color:black;margin-left:4%;padding-top:4%;">Copy and send the link below via email to psychologist.<br/>

                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-10">

                                    <br/>
                                    <div id="copyTarget" class="url_div" style="font-family:sans-serif;margin-left:6%;">
                                        http://www.stresscontrolonline.com/psy
                                    </div> <button class="btn btn-success" onclick="showDiv()" style="margin: 0 -10px;background:#223C80 !important;border-color: #223C80 !important;" id="copyButton">Copy</button><br><br>

                                    <div id="successDiv" style="float:left;display:none;color:#0b992a">Copied to clipboard</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery  -->
        <script>
            document.getElementById("copyButton").addEventListener("click", function () {
                copyToClipboard(document.getElementById("copyTarget"));
            });

            function copyToClipboard(elem) {
                // create hidden text element, if it doesn't already exist
                var targetId = "_hiddenCopyText_";
                var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
                var origSelectionStart, origSelectionEnd;
                if (isInput) {
                    // can just use the original source element for the selection and copy
                    target = elem;
                    origSelectionStart = elem.selectionStart;
                    origSelectionEnd = elem.selectionEnd;
                } else {
                    // must use a temporary form element for the selection and copy
                    target = document.getElementById(targetId);
                    if (!target) {
                        var target = document.createElement("textarea");
                        target.style.position = "absolute";
                        target.style.left = "-9999px";
                        target.style.top = "0";
                        target.id = targetId;
                        document.body.appendChild(target);
                    }
                    target.textContent = elem.textContent;
                }
                // select the content
                var currentFocus = document.activeElement;
                target.focus();
                target.setSelectionRange(0, target.value.length);

                // copy the selection
                var succeed;
                try {
                    succeed = document.execCommand("copy");
                } catch (e) {
                    succeed = false;
                }
                // restore original focus
                if (currentFocus && typeof currentFocus.focus === "function") {
                    currentFocus.focus();
                }

                if (isInput) {
                    // restore prior selection
                    elem.setSelectionRange(origSelectionStart, origSelectionEnd);
                } else {
                    // clear temporary content
                    target.textContent = "";
                }
                return succeed;
            }
            function showDiv() {
                document.getElementById('successDiv').style.display = "block";
                setInterval(function () {
                    document.getElementById('successDiv').style.display = "none";
                }, 1300);
            }
        </script>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- Form wizard -->
        <script src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    </body>
</html>