<?php
ini_set('max_execution_time', 300);
/*
  1.In the sidebar when view_psychologist is clicked we land onto this page.
  2.All the applied psychologists can be seen here, there name, email, area of expertise, area of colloboration and qualification are displayed.
 */

include '../../if_loggedin.php';
include 'mindnet-host.php';


$i = 0;
include 'psy/psy_nw_config.php';

#fetching all the values necessary in the table to display
$stmt01 = $db->prepare("SELECT DISTINCT * FROM psy_info");
$stmt01->execute(array()) or die("OOPs Error Occured! Please try again. If the issue persists send us a mail at help@stresscontrol.com.Error Code : DETNF");
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $pid[$i] = $row01['pid'];
        $fname[$i] = $row01['first_name'];
        $lname[$i] = $row01['last_name'];
		$emails[$i] = $row01['email'];
        $mobile[$i] = $row01['mobile'];
        $landline[$i] = $row01['landline'];
        $user_yob[$i] = $row01['yob'];
        $qual[$i] = $row01['qualification'];
        $position[$i] = $row01['position'];
        $language[$i] = $row01['language'];
        $yce[$i] = $row01['counseling_experience'];
        $state[$i] = $row01['state'];
        $city[$i] = $row01['city'];
        $PC[$i] = $row01['aoi_PC'];
        $OC[$i] = $row01['aoi_OC'];
        $WS[$i] = $row01['aoi_WS'];
        $WA[$i] = $row01['aoi_WA'];
        $EAP[$i] = $row01['discipline_eap'];
        $PT[$i] = $row01['discipline_psychotherapy'];
        $WSE[$i] = $row01['discipline_workshops'];
        $PM[$i] = $row01['discipline_pyschometry'];
        $remarks[$i] = $row01['remarks'];
        $profile_selected[$i] = $row01['profile_selected'];

        //checking the value stored in the database and changing them corresponding data. as IF PM=1 it means psychometry.
        if ($row01['discipline_other'] != '0') {
            $dis_others[$i] = $row01['discipline_other'];
        } else {
            $dis_others[$i] = "";
        }
        if ($PM[$i] == '1') {
            $PM[$i] = "Psychometry, ";
        } else {
            $PM[$i] = "";
        }
        if ($WSE[$i] == '1') {
            $WSE[$i] = "Workshops, ";
        } else {
            $WSE[$i] = "";
        }
        if ($PT[$i] == 1) {
            $PT[$i] = "Psychotherapy, ";
        } else {
            $PT[$i] = "";
        }
        if ($EAP[$i] == 1) {
            $EAP[$i] = "EAP, ";
        } else {
            $EAP[$i] = "";
        }
        $i++;
    }
}
?>
<!DOCTYPE html>
<html>
   
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <!-- App title -->
        <title>View Psychologists | Silver Oak Health</title>
         <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
                                                                                                                                         
        <!-- DataTables -->
        <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
       <script src="assets/js/modernizr.min.js"></script>
        <link href="https://s3-ap-southeast-1.amazonaws.com/scoreg/css/font-awesome.min.css" rel="stylesheet">


        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            input{
                margin-bottom:2%;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .hr_class_sidebar{
                margin: 20px 0;
                border: 0;
                border-top: 1px solid #eee;
                border-bottom: 0;
            }
            .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
                background-color: #2590e3;
                border-color: #2590e3;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="padding-top: 20px;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>
            <div class="container-fluid in">
            <a class="btn btn-primary" style ="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin-bottom:10px;" href="create_new_psychologist.php">Add New</a></p>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-color panel-info" style="margin-bottom: 5px;">
                        <div class="panel-body" style="background-color:#FCFCFB;">
                            <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th style="color:#188ae2;text-align:center;width:3%;"><br/>&nbsp;</th>

                                        <th style="color:#188ae2;text-align:center;width:3%;"><br/>&nbsp;</th>

                                        <th style="color:#188ae2;text-align:center;width:12%;font-weight:normal;">Full Name<br/>&nbsp;</th>

                                        <th style="color:#188ae2;text-align:center;width:3%;font-weight:normal;"><div data-toggle="tooltip" data-placement="bottom" class="red-tooltip" title="" data-html="true"  data-original-title="Phone Counselling">
                                                PC<br/>&nbsp;</div></th>

                                        <th style="color:#188ae2;text-align:center;width:3%;font-weight:normal;"><div data-toggle="tooltip" data-placement="bottom" class="red-tooltip" title="" data-html="true"  data-original-title="Onsite Counselling">
                                                OC<br/>&nbsp;</div></th>

                                        <th style="color:#188ae2;text-align:center;width:3%;font-weight:normal;"><div data-toggle="tooltip" data-placement="bottom" class="red-tooltip" title="" data-html="true"  data-original-title="Workshops">
                                                WS<br/>&nbsp;</div></th>

                                        <th style="color:#188ae2;text-align:center;width:3%;font-weight:normal;"><div data-toggle="tooltip" data-placement="bottom" class="red-tooltip" title="" data-html="true"  data-original-title="Writing Articles">
                                                WA<br/>&nbsp;</div></th>

                                        

                                        <th style="color:#188ae2;text-align:center;width:8%;font-weight:normal;">City<br/>&nbsp;</th>
                                        <th style="color:#188ae2;text-align:center;width:8%;font-weight:normal;">Contact<br/>&nbsp;</th>

                                        <th style="color:#188ae2;text-align:center;width:15%;font-weight:normal;">Languages<br/>&nbsp;</th>

                                      

                                        <th style="color:#188ae2;text-align:center;width:20%;font-weight:normal;"><div data-toggle="tooltip" data-placement="bottom" class="red-tooltip" title="" data-html="true"  data-original-title="Areas Of Expertise">
                                                AOE<br/>&nbsp;</div></span></th>

                                        <th style="color:#188ae2;text-align:center;width:2%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody style="font-family:'Open Sans';font-size:10px;">
                                    <?php
                                    for ($j = 0; $j < count($pid); $j++) {
                                        ?>
                                        <tr>
                                            <td align="center">
                                                <?php
                                                if (strlen($remarks[$j]) > 2) {
                                                    echo '<i class="fa fa-pencil" aria-hidden="true"></i>';
                                                } else {
                                                    echo '';
                                                }
                                                ?>
                                            </td>
                                            <td align="center">
                                                <?php
                                                if ($profile_selected[$j] == 1) {
                                                    echo '<i class="fa fa-check" aria-hidden="true"></i>';
                                                } else {
                                                    echo '';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $fname[$j]; ?>&nbsp;<?php echo $lname[$j]; ?>
                                            </td>
                                            <td align="center">
                                                <?php if ($PC[$j] == '1') { ?>
                                                    Y
                                                <?php } ?>
                                            </td>
                                            <td align="center">
                                                <?php if ($OC[$j] == '1') { ?>
                                                    Y
                                                <?php } ?>
                                            </td>
                                            <td align="center">
                                                <?php if ($WS[$j] == '1') { ?>
                                                    Y
                                                <?php } ?>
                                            </td>
                                            <td align="center">
                                                <?php if ($WA[$j] == '1') { ?>
                                                    Y
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php echo $city[$j]; ?>
                                            </td>
                                            <td align="left">
                                                <?php echo $mobile[$j]; ?>
                                                <BR/>
                                                <?php echo $emails[$j]; ?>
                                            </td>
											<td>
                                                <?php echo $language[$j]; ?>
                                            </td>
                                            <td>
                                                <?php echo $EAP[$j] . $PT[$j] . $WSE[$j] . $PM[$j] . $dis_others[$j]; ?>
                                            </td>
                                            <td align="center" style="vertical-align:middle;">
                                                <a style ="color: #223c87 !important; border-color: #223c87 !important;" href="validate.php?pid=<?php echo $pid[$j]; ?>" src="validate.php?pid=<?php echo $pid[$j]; ?>">View</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
<!-- END wrapper -->
<script>
    var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<!-- Datatables-->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<!-- Datatable init js -->
<script src="assets/pages/datatables.init.js"></script>
<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({ajax: "../../assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollX: 120, scrollCollapse: true, scroller: true});
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
    });
    TableManageButtons.init();

</script>
</body>
</html>