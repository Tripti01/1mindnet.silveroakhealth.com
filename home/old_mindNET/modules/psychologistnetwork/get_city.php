<?php
include('psy/psy_nw_config.php');
if(isset($_POST['id']) && isset($_POST['city']))
{
	$id=$_POST['id'];
	$city = $_POST['city'];
	
	$stmt = $db->prepare("SELECT DISTINCT(city_name) FROM statelist WHERE state=:id");
	$stmt->execute(array(':id' => $id));
	
	?><option selected="selected">--SELECT CITY-- :</option>
	<?php while($row=$stmt->fetch(PDO::FETCH_ASSOC))
	{
		?>
		<option value="<?php echo $row['city_name']; ?>" <?php echo ($city == $row['city_name']) ? "selected" : "" ?>><?php echo $row['city_name']; ?></option>
		<?php
	}
}
?>
<option value="other">Other</option>