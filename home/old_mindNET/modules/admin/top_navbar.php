<?php include 'mindnet/functions/get_profile_pic.php'; ?>
<header id="topnav">
			<div class="active">
                    <div class="container-fluid in">
                        <div id="navigation" class="active">
                            <!-- Navigation Menu-->
                            <ul class="navigation-menu in">

                                <li class="has-submenu">
                                    <a href="<?php echo $host;?>/" class="navbar-brand"></a>
                                </li>
								
								<li class="has-submenu">
                                    <a href="<?php echo $host;?>/"><b style="color:#5a5a5a;">Home</b></a>
                                </li>
								
                            </ul>
                            <!-- End navigation menu -->

							<div class="be-right-navbar">
								<ul class="nav navbar-nav navbar-right be-user-nav">
									<li class="dropdown"><a href="<?php echo $host;?>/index.php" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
											<img src="<?php echo $host;?>/../assets/img/profile_pic/<?php get_emp_profile_pic($emp_id); ?>" style="border-radius: 50%;" height="30" width="30" alt="Avatar"></a>
										<ul role="menu" class="dropdown-menu">
											<li>
												<div class="user-info">
													<div class="user-name"><?php echo $emp_name;?></div>
												</div>
											</li>
											<li><a href="<?php echo $host;?>/my_profile.php"><span class="icon mdi mdi-face"></span> Profile </a></li>
											<li><a href="<?php echo $host;?>/logout.php"><span class="icon mdi mdi-power"></span> Logout</a></li>
										</ul>
									</li>
								</ul>
							</div>
                        </div>
                        <!-- end #navigation -->
                    </div>
                    <!-- end container -->
                </div>
                <!-- end navbar-custom -->

            </header>