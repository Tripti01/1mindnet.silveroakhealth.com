<?php
#this is cron job to add reminders for the clients of a given therapist who are in-progress, completed or not yet started cbt.
#the details of clients are then inserted into thrp_remin table
#including files and functions
include 'soh-config.php';
include 'mindnet-host.php';
include 'fun_rem.php';

$counter=0;

# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//to fecth all the tids from therapist_login
$j = 0; //counter to fetch all the therapist
$stmt00 = $dbh->prepare("SELECT tid FROM thrp_login;");
$stmt00->execute(array());
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {

        $tid = $row00['tid']; //to fetch therpaist
        
        #to fecth all the uids of client assigned to therapist
        $i = 0; //counter to fetch all uids assigned 
        $stmt10 = $dbh->prepare("SELECT user_therapist.uid,user_login.name FROM user_therapist,user_login WHERE tid=? AND user_therapist.uid=user_login.uid;");
        $stmt10->execute(array($tid));
        if ($stmt10->rowCount() != 0) {
            while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC)) {
                # Get User Id from SQL results 
                $uid[$i] = $row10['uid'];
                $name[$i] = $row10['name'];

                //to pass uids into function to check weather they have to remined or not
                //based on the return values text messages are added
                list($status[$i], $date[$i], $ssn[$i]) = rem_status($uid[$i]);

                //to set $text based on the retirn status
                if ($status[$i] == 0) {//do nothing,as user has time to complete
                   //do nothing
                } else {
                    $counter++;
                }
                
                $i++;
            }
            
                echo $counter;
        }$j++;
    }
}
?>