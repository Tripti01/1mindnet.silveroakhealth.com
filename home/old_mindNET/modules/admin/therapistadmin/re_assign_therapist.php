<?php
   /*
     1.we can re-assign the therapist for the client if we need to, in this file
     2.Here the details can be viewed such as client's name loaction age
     3.on click of re-assign button the modal opens where we can assign the new therapist
    */
   include '../../../if_loggedin.php';
   include '../check_prvg.php';
   ## Check if user has access to view this page ##
   $if_allowed_to_view_this_page = user_has_prvg("THAD");
   if (!$if_allowed_to_view_this_page) {
       exit();
   }
   
   
   #file inclusion for various function happening in the ui
   include 'mindnet-host.php';
   include 'soh-config.php';
   include 'functions/crypto_funtions.php';
   include 'functions/get_type_full_name.php';
   

   # initialize status code for error msgs
   $status_code = 0;
   
   $cur_year = date('Y');
   
   # Start database transaction
   $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
   $dbh->query("use sohdbl");
   
   	$status = 0;
   	$email = "";
   	if(isset($_REQUEST['email']) && $_REQUEST['email'] != ''){
   	 
   		 $email = $_REQUEST['email'];
   	 
   		$stmt02 = $dbh->prepare("SELECT activated,uid,name FROM user_temp WHERE email = ?");
   		$stmt02->execute(array($email));
   		if ($stmt02->rowCount() != 0) {
   			
   			$row02 = $stmt02->fetch();
			$uid = $row02['uid'];
   			$name = $row02['name'];
   				
   			$stmt03 = $dbh->prepare("SELECT user_therapist.tid, thrp_login.name FROM user_therapist, thrp_login WHERE thrp_login.tid = user_therapist.tid AND user_therapist.uid = ?");
   			$stmt03->execute(array($row02['uid']));
   			if ($stmt03->rowCount() != 0) {
   					$row03 = $stmt03->fetch();
   					$curp_thrp_id = $row03['tid'];
   					$cur_thrp_name = $row03['name'];
   			}
   			$status = 2;
   			
   		}else{			
   			$status = 3;			
   		}
   	}
   	
   
   $i = 0; //counter to fetch all name,email,tid
   	
   $stmt06 = $dbh->prepare("SELECT thrp_type.tid,thrp_login.name,thrp_login.email FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = 'THRP' ORDER BY email ASC");
   $stmt06->execute(array());
   if ($stmt06->rowCount() != 0) {
       while ($row06 = $stmt06->fetch(PDO::FETCH_ASSOC)) {
           $thrp_name[$i] = $row06['name']; //to fetch name
           $thrp_email[$i] = $row06['email']; //to fetch email
           $thrp_id[$i] = $row06['tid']; //to fetch tid
           $decrypted_thrp_name[$i] = $thrp_name[$i]; //to fetch name,and decrypt it
           $i++; //incrementing tid
       }
   } else {
       $status_code = 1; // No rows collected
   }
   
   	
   	
   //on-click of assign-submit
   if (isset($_REQUEST['assign_submit'])) {
	   
	   include '../admin_mail/send_user_assigned_mail.php';
   
       # check if the radio button submitted has a value or not
       if (isset($_REQUEST['radio'])) {
   
           # request uid from hidden value that was passed on submission
           $uid = $_REQUEST['uid'];
           $tid = $_REQUEST['radio']; //to fetch tid from radio button
   
           if (isset($uid) && isset($tid)) {
   
               $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
               $dbh->query("use sohdbl");
   
               //to fetch the therapist details
               $stmt00 = $dbh->prepare("SELECT name,email FROM thrp_login WHERE tid = ? LIMIT 1");
               $stmt00->execute(array($tid));
               if ($stmt00->rowCount() != 0) {
                   $result00 = $stmt00->fetch();
                   $thrp_email = $result00['email'];
                   $thrp_name_dec = decrypt($result00['name'], $encryption_key);
               }
               //to fetch user details
               $stmt01 = $dbh->prepare("SELECT name from user_temp Where uid = ? LIMIT 1");
               $stmt01->execute(array($uid));
               if ($stmt01->rowCount() != 0) {
                   $row01 = $stmt01->fetch();
                   $user_name = decrypt($row01['name'], $encryption_key);
               }
   
               //to update user_therapist
               $stmt02 = $dbh->prepare("UPDATE user_therapist set tid=? WHERE uid=?");
               $stmt02->execute(array($tid, $uid));
   
               //to send user_Assigned _email
               send_user_assigned_email($thrp_email, $thrp_name_dec, $user_name);
   
			   $status_code = 2; //All done successfully
			  
           } else {
               $status_code = 3; // Some error occured please refresh and try again
           }
       } else {
           $status_code = 4; // select your therapist
       }
   }
   ?>
<!DOCTYPE html>
<html>
   <head>
      <!-- App Favicon -->
      <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
      <!-- App title -->
      <title>Reassign Therapist | Silver Oak Health</title>
      <!-- App CSS -->
     	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
         
      <style>
         .hr_class{
         border: 0;
         height: 1px;
         background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
         }
         .hr_class_sidebar{
         margin: 20px 0;
         border: 0;
         border-top: 1px solid #eee;
         border-bottom: 0;
         }
         .radio{
         margin-top: 0px;
         margin-bottom :0px;
         }
      </style>
   </head>
     <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
					 <a class="btn btn-primary" style="background-color: #223c87 !important; border-color: #223c87 !important;float:right;" 
								href="assign_therapist.php">Assign Therapist</a>
								<BR/><BR/>
                  <div class="row">
                     <div class="col-sm-12">
					
                        <div class="card-box">
                           <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                              <div class="form-group">
                                 <div class="col-md-1 col-xs-12">
                                    &nbsp;
                                 </div>
                                 <label class="col-md-2 col-xs-12 control-label"> Enter User Email </label>
                                 <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control input-sm"  name="email" placeholder="User Email">
                                 </div>
                                 <div class="col-md-2 col-xs-12">
                                    <input type="submit" class="btn btn-primary" value="Submit" name="btn-create-user"/>
                                 </div>
                              </div>
                              <div class="form-group m-b-0" style="text-align: center;">
                              </div>
                           </form>
                           <div class="row">
                              <div class="col-sm-12">
                                 <div Style="text-align: center;font-size: 15px;">
                                    <div class="confirm-msg">
									 <?php  if ($status_code != 0 && $status_code != 1) {
                                                      switch ($status_code) {
                                                      	case 2: echo '<div class="alert alert-success" style="text-align:center;">';
                                                      		echo "<strong>Therapist has been re-assigned to ".$thrp_name_dec."</strong>";
                                                      		echo '</div>';
                                                      		break;
                                                      	case 3: echo '<div class="alert alert-danger" style="text-align:center;">';
                                                      		echo "<strong>OOPS some error occure please refresh and try again</strong>";
                                                      		echo '</div>';
                                                      		break;
                                                      	default: break;
                                                      }
                                                      } ?>
                                       <?php if($status == 3){
                                          echo '<div class="alert alert-danger" style="text-align:center;">'.$email.' is not a Stress Control Online user</div>';
                                          }elseif($status == 4){
										   echo '<div class="alert alert-danger" style="text-align:center;">'.$email.' is not an active Stress Control Online user</div>';
										  }else if($status == 2){ ?>
                                       <form  action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" id="assign_therapist">
                                          <div class="col-sm-12">
                                             <?php for ($j = 0; $j < count($thrp_id); $j++) { ?>
                                             <div id="msg<?php echo $j; ?>" style="font-weight: bold;font-family: 'Open Sans';text-align: center;"></div>
                                             <?php } ?>
                                             <div style="text-align: center; font-family: 'Open Sans'; color:#9C9E97; font-size: 11px; font-weight: bold;">
                                                <p style = "font-size: 14px; font-weight: bold;color:#33b519;"><strong style="color:#30302E;">
												<?php
												 if (isset($name) && $name != "") {
                                                   	echo $name;
                                                   } else
                                                   	echo "No therapist"
                                                   	?></strong> is currently assigned to <strong style="color:#30302E;"><?php
                                                   if (isset($cur_thrp_name) && $cur_thrp_name != "") {
                                                   	echo $cur_thrp_name;
                                                   } else {
                                                   	echo"Client";
                                                   }
                                                   ?></strong></p>
                                                <p>(Please select one of the therapist to assign <strong style="color:#30302E;"><?php
                                                   if (isset($name) && $name != "") {
                                                   	echo $name;
                                                   } else
                                                   	echo ""
												 	?></strong> and click assign button below)</p>
                                             </div>
                                             <div>
                                                <hr style=' border: 0;height: 1px;margin-top:1%;margin-bottom:1%;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));'>
                                             </div>
                                             <br/>
                                             <table id="datatable-buttons" class="table table-striped table-bordered">
                                                <thead>
                                                   <tr style="font-family: 'Open Sans';">
                                                      <th>            </th>
                                                      <th style="color:#188ae2;">Therapist's Name</th>
                                                      <th style="color:#188ae2;"> Therapist's Email</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      if ($status_code == '1') {
                                                      	echo "No therapists available";
                                                      } else {
                                                      	for ($i = 0; $i < count($thrp_id); $i++) {
                                                      		?>
                                                   <tr style="font-family: 'Open Sans';">
                                                      <td>
                                                         <div class="radio radio-primary">
                                                            <input type="radio" name="radio" id="radio<?php echo $i; ?>" value="<?php echo $thrp_id[$i]; ?>"  >
                                                            <label></label>
                                                         </div>
                                                         <input type="hidden" name="thrp_name" id="thrp_name<?php echo $i; ?>" value="<?php echo $decrypted_thrp_name[$i]; ?>"  >
                                                      </td>
                                                      <td style="text-align:left"><?php echo $decrypted_thrp_name[$i]; ?></td>
                                                      <td style="text-align:left"><?php echo $thrp_email[$i]; ?></td>
                                                   </tr>
                                                   <?php
                                                      }
                                                      }
                                                      
                                                    ?>
                                                </tbody>
                                             </table>
                                             <br/>
                                             <div style="text-align: right;">
                                                <input type="hidden" name="uid" id="uid" value ="<?php echo $uid; ?>">
                                               <!-- <input type="hidden" name="tid" id="id" value ="<?php echo $tid; ?>">-->
                                                <input type="submit" class="btn btn-success" style="font-family: 'Open Sans';background:#223C80 !important; border-color: #223C80 !important;" value="Assign" id="assign_submit" name="assign_submit">
                                                <input type="reset" class="btn btn-custom" style="font-family: 'Open Sans';background:#b1b2b8 !important; border-color: #b1b2b8 !important;"  value="Clear">
                                             </div>
                                          </div>
                                          <!-- end col -->
                                       </form>
                                       <?php }?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- end row -->
                        </div>
                     </div>
                     <!-- end col -->
                  </div>
                  <!-- end row -->						
               </div>
               <!-- container -->
            </div>
         </div>
      </div>
      <!-- END wrapper -->
      <script src="../assets/js/jquery.min.js"></script>
      <script src="../assets/js/bootstrap.min.js"></script>
      <script src="../assets/js/detect.js"></script>
      <script src="../assets/js/jquery.slimscroll.js"></script>
      <script src="../assets/js/waves.js"></script>
      <script src="../assets/js/jquery.nicescroll.js"></script>
      <script src="../assets/js/jquery.app.js"></script>
      <script src="../assets/js/jquery.core.js"></script>
      <script src="../assets/js/fastclick.js"></script>
	  <script src="../assets/js/app.min.js"></script>
   </body>
</html>