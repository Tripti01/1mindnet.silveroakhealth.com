<?php
include '../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}

ini_set('max_execution_time', 300);
//including files and functions
include 'mindnet-host.php';
include "soh-config.php";
include 'functions/crypto_funtions.php';
include 'functions/coach/get_user_name.php';
include 'get_user_count.php';
include 'functions/get_user_status.php';

//to fetch tid from url
//if tid isset then fetch tid else set as blank
if (isset($_REQUEST['tid'])) {
    $tid = $_REQUEST['tid'];
} else {
    $tid = "";
}
//to fetch type from url
//if type is set then fetch type else set as type
if (isset($_REQUEST['type'])) {
    $type = $_REQUEST['type'];
} else {
    $type = "";
}
//Take the name from the session
$admin_name = $_SESSION['name'];

//declaring variables for as foloowing
$k = 0; //cbt not yet started
$l = 0; //cbt in progress
$m = 0; //cbt finished
$n = 0; //cbt end date reached
$j = 0; //counter for all uids
$o = 0; //counter for inactive clients
# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//to fetch all uids assigned to therpist
$stmt100 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=? ORDER BY uid;");
$stmt100->execute(array($tid));
if ($stmt100->rowCount() != 0) {
    while ($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)) {
        $uid[$j] = $row100['uid'];

        //function to get status of the clients assigned 
        $return_value = get_user_count($uid[$j]);

        if ($return_value == 1) {//not yet started cbt
            $uid_not_yet_started[$k] = $uid[$j];
            $k++;
        } else if ($return_value == 2) {//cbt progress//active
            $uid_active[$l] = $uid[$j];
            $l++;
        } else if ($return_value == 3) {//cbt finished
            $uid_cbt_finished[$m] = $uid[$j];
            $m++;
        } else if ($return_value == 4) {//cbt end date reached
            $uid_cbt_end_date_reached[$n] = $uid[$j];
            $n++;
        } else if ($return_value == 5) {//inactive
            $uid_in_active[$o] = $uid[$j];
            $o++;
        }

        $j++;
    }
}
//if uid is set then proceed,else do nothing
if (isset($uid)) {
//to fetch details of users who have completed 
    $l = 0;
    for ($k = 0; $k < count($uid); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $uid_ssn_total[$k]) = get_user_status($uid[$k]);
        //to fetch user creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE  user_schedule.uid=user_time_creation.uid  AND user_time_creation.uid =? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation datetime for all uids 
                $due_datetime[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime for all uids
                //to check if the user has Done prea and post assessment or not
                $stmt300 = $dbh->prepare("SELECT type FROM asmt_score WHERE uid=? LIMIT 2");
                $stmt300->execute(array($uid[$k]));
                $row_count = $stmt300->rowCount(); //to fetch rowcount
                //if rowcount is 0, then user has not started cbt
                if ($row_count == 0) {
                    $status_total[$l] = 1;
                } else if ($row_count == 1) {//if rowcount is 1,then user has Done pre-assessemnt , hence user is in progress,then proceed
                    $active = is_active($uid[$k]); //to check if user is active or not
                    if ($active == 1) {//user is active
                        $status_total[$l] = 2;
                    } else {
                        $status_total[$l] = 3; //cbt end date reacched
                    }
                } else if ($row_count == 2) {//if rowcount is 2, then both pre and post assessement has been Done, hence user has finished cbt
                    $status_total[$l] = 4;
                }

                $l++;
            }
        }
    }
} else {
    //do nothing, since uid is not set
}

//if uid_not_yet_started is set then proceed,else do nothing
if (isset($uid_not_yet_started)) {
//to fetch details of users who have not yet started
    $l = 0;
    for ($k = 0; $k < count($uid_not_yet_started); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_not_yet_started[$k]) = get_user_status($uid_not_yet_started[$k]);
        //to fetch user creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE  user_schedule.uid=user_time_creation.uid  AND user_time_creation.uid =? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_not_yet_started[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_not_yet_started[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation time for uid not yet started cbt
                $due_datetime_not_yet_started[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch end date for uid not yet started cbt
                $l++;
            }
        }
    }
} else {
    //do nothing,since uid_not_yet_started is not set
}

//if uid_cbt_progress is set then proceed,else do nothing
if (isset($uid_active)) {
//to fetch details of users who are in progress
    $l = 0;
    for ($k = 0; $k < count($uid_active); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_active[$k]) = get_user_status($uid_active[$k]);
        //to fetch user creation datetime, due datetime and asmt_score(to check if pre assessment has been done or not)
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time, asmt_score.type AS type ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, asmt_score , user_schedule WHERE asmt_score.uid = user_schedule.uid AND user_time_creation.uid = asmt_score.uid AND asmt_score.uid=? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_active[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_active[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation time 
                $asmt_score_active[$l] = $row200['type']; //to fetch pre assesmnet,if value exsists pre assessmnet has been done
                $due_datetime_active[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothinmg, since uid_cbt_progress is not set
}

//if uid_cbt_finished is set then proceed,else do nothing
if (isset($uid_cbt_finished)) {
//to fetch details of users who have completed 
    $l = 0;
    for ($k = 0; $k < count($uid_cbt_finished); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_finished[$k]) = get_user_status($uid_cbt_finished[$k]);
        //to fetch creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE  user_schedule.uid=user_time_creation.uid  AND user_time_creation.uid =? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_cbt_finished[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_finished[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation time
                $due_datetime_finished[$l] = date('d-M-Y h:i A', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothing,since uid_cbt_finished is not set
}

//if uid_cbt_end_date_reached is set then proceed,else do nothing
if (isset($uid_cbt_end_date_reached)) {
//to fetch details of users whose end date has been reached
    $l = 0;
    for ($k = 0; $k < count($uid_cbt_end_date_reached); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_reached[$k]) = get_user_status($uid_cbt_end_date_reached[$k]);
        //to fetch creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time,user_schedule.due_datetime AS due_datetime FROM user_time_creation, user_schedule WHERE user_time_creation.uid = user_schedule.uid AND user_time_creation.uid = ? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_cbt_end_date_reached[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_end_date_reached[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation datetime
                $due_datetime_end_date_reached[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothin, since uid_cbt_end_date_Reached
}

//if uid_in_active is set then proceed,else do nothing
if (isset($uid_in_active)) {
//to fetch details of users whose end date has been reached
    $l = 0;
    for ($k = 0; $k < count($uid_in_active); $k++) {
        //to fetch the status
        list($user_ssn_seqn[$k], $user_ssn_status_in_active[$k]) = get_user_status($uid_in_active[$k]);
        //to fetch creation datetime and due datetime
        $stmt200 = $dbh->prepare("SELECT user_time_creation.creation_time AS creation_time, asmt_score.type AS type ,user_schedule.due_datetime AS due_datetime FROM user_time_creation, asmt_score , user_schedule WHERE asmt_score.uid = user_schedule.uid AND user_time_creation.uid = asmt_score.uid AND asmt_score.uid=? AND user_schedule.ssn='CBTEND' LIMIT 1");
        $stmt200->execute(array($uid_in_active[$k]));
        if ($stmt200->rowCount() != 0) {
            while ($row200 = $stmt200->fetch(PDO::FETCH_ASSOC)) {
                $user_creation_datetime_in_active[$l] = date('d-M-Y h:i A', strtotime($row200['creation_time'])); //to fetch creation datetime
                $asmt_score_in_active[$l] = $row200['type']; //to fetch pre assesmnet,if value exsists pre assessmnet has been Done
                $due_datetime_in_active[$l] = date('d-M-Y', strtotime($row200['due_datetime'])); //to fetch due datetime
                $l++;
            }
        }
    }
} else {
    //do nothin, since uid_in_Active is not set
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Report</title>

        <!-- DataTables -->
        <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/jquery.dataTable.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="../../assets/js/modernizr.min.js"></script>
        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .hr_class_sidebar{
                margin: 20px 0;
                border: 0;
                border-top: 1px solid #eee;
                border-bottom: 0;
            }
            .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
                background-color: #2590e3;
                border-color: #2590e3;
            }
        </style>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../../assets/js/modernizr.min.js"></script>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:65%;margin-top:1%;">
                    <p style="margin-top:1%;font-size: 16px;font-weight:bold"><span>Admin Console</span></p>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <span class="page-title" style="font-family: Roboto;color:#223C80;">Client Reports -</span><span style="font-size:16px;font-family: Roboto;color:#223C80;font-weight:bold;"> <?php
                                    switch ($type) {
                                        case "total"://to display  all clients
                                            echo "All Clients";
                                            break;
                                        case "not_yet_started"://to display  not yet started cbt
                                            echo "Not Yet Started CBT";
                                            break;
                                        case "active"://to display  active
                                            echo "Active";
                                            break;
                                        case "in_active"://to display  active
                                            echo "In-active";
                                            break;
                                        case "end_date_reached"://to display end date is reached
                                            echo "End Date Reached";
                                            break;
                                        case "finished"://to diaplsy  finished cbt
                                            echo "CBT Finished";
                                            break;
                                        default:
                                            break;
                                    }
                                    ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <HR class="hr_class_sidebar" style="margin-bottom:20px;margin-top:-8%;">
                        <h5>
                            <a href="#"  style="font-family:'Open Sans';font-weight:bolder; font-size:13px; color:#6FA7D7;margin-bottom:20px;"><?php echo $admin_name ?></a>
                        </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="../my_account.php" alt="settings" >
                                    <i class="zmdi zmdi-settings" style="color:#6FA7D7;"></i>
                                </a>
                            </li>

                            <li>
                                <a href="../logout.php" class="text-custom">
                                    <i class="zmdi zmdi-power" style="color:#6FA7D7;"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="sidebar-menu" style="margin-top:-10%;">
                        <ul>
                            <?php include '../sidebar.php'; ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page" style="margin-top:0.5%;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <div class="card-box">
                            <article style="padding-left: 10px;padding-right: 10px;">
                                <h5 class="title">
                                    Reports
                                </h5>

                                <div class="panel-body">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th># <br/> &nbsp;</th>
                                                <th>UID <br/> &nbsp;</th>
                                                <th>Client Name<br/> &nbsp; </th>
                                                <th>Creation time<br/> &nbsp; </th>
                                                <th>Status<br/> &nbsp; </th>
                                                <th>End Date <br/> &nbsp; </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($uid) || isset($uid_not_yet_started) || isset($uid_cbt_progress) || isset($uid_cbt_finished) || isset($uid_cbt_end_date_reached)) {
                                                switch ($type) {
                                                    case "total"://to display table for all clients
                                                        if (isset($uid) && $uid!= null) {
                                                            for ($j = 0; $j < count($uid); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_ssn_total[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime[$j]; ?>                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "not_yet_started"://to display table for clients not yet started cbt
                                                        if (isset($uid_not_yet_started) && $uid_not_yet_started != null) {
                                                            for ($j = 0; $j < count($uid_not_yet_started); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_not_yet_started[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_not_yet_started[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_not_yet_started[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_not_yet_started[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_not_yet_started[$j]; ?>                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "active"://to display table for clients who are active
                                                        if (isset($uid_active) && $uid_active != null) {
                                                            for ($j = 0; $j < count($uid_active); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_active[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_active[$j]; ?>                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "in_active"://to display table for clients who are active
                                                        if (isset($uid_in_active) && $uid_in_active != null) {
                                                            for ($j = 0; $j < count($uid_in_active); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_in_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_in_active[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_in_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_in_active[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_in_active[$j]; ?>                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;

                                                    case "end_date_reached"://to display table for clients whose end date is reached
                                                        if (isset($uid_cbt_end_date_reached) && $uid_cbt_end_date_reached != null) {
                                                            for ($j = 0; $j < count($uid_cbt_end_date_reached); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_cbt_end_date_reached[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_cbt_end_date_reached[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_end_date_reached[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_reached[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_end_date_reached[$j]; ?>                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    case "finished"://to diaplsy table for clients who have finished cbt
                                                        if (isset($uid_cbt_finished) && $uid_cbt_finished != null) {
                                                            for ($j = 0; $j < count($uid_cbt_finished); $j++) {//counter to count all tids
                                                                $sno = $j + 1; //to calculateserial number
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo $sno; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $uid_cbt_finished[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo get_user_name($uid_cbt_finished[$j]); ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_creation_datetime_finished[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $user_ssn_status_finished[$j]; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $due_datetime_finished[$j]; ?>                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        break;
                                                    default:
                                                        ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <?php
                                                        break;
                                                }
                                            } else {
                                                //do nothing
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </article>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->
                </div> 
            </div> 
            <footer class="footer">
                <p style="text-align:left;"><script type="text/javascript">var year = new Date();document.write(year.getFullYear());</script> &copy; SilverOakHealth.
                </p>
            </footer>
        </div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <!-- Datatables-->
        <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../../assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="../../assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../../assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../../assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="../../assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="../../assets/plugins/datatables/buttons.print.min.js"></script>
        <!-- Datatable init js -->
        <script src="../../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../../assets/js/jquery.core.js"></script>
        <script src="../../assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

        </script>
    </body>
</html>