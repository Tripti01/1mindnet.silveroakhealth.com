<?php
ini_set('max_execution_time', 300);
//including files and folders
include 'mindnet-host.php';
include "soh-config.php";
include 'functions/crypto_funtions.php';
include 'get_user_count.php';

//intialisation of arrays
$not_yet_started = array();
$cbt_progress = array();
$cbt_finished = array();
$cbt_end_date_reached = array();
$total_users = array();
$i = 0; //counter to fetch all the therapist
# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//qury to fecth all tids
$stmt00 = $dbh->prepare("SELECT tid,name FROM thrp_login ORDER BY tid;");
$stmt00->execute(array());
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {

        $tid[$i] = $row00['tid']; //to fetch therpaist
        $tid_name[$i] = decrypt($row00['name'], $encryption_key); //to fetch therapist name
        //intializ counter to 0, for every tid
        $count_not_yet_started = 0;
        $count_cbt_progress = 0;
        $count_cbt_finished = 0;
        $count_cbt_end_date_reached = 0;
        $j = 0; //counter for all uids
        //to fetch all uids assigned to therpist
        $stmt100 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=? ORDER BY uid;");
        $stmt100->execute(array($tid[$i]));
        if ($stmt100->rowCount() != 0) {
            while ($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)) {
                $uid[$j] = $row100['uid'];
                //function to get status of the clients assigned 
                $return_value = get_user_count($uid[$j]);

                if ($return_value == 1) {//user has not yet started cbt
                    $count_not_yet_started++;
                } else if ($return_value == 2) {//user cbt is in progress
                    $count_cbt_progress++;
                } else if ($return_value == 3) {//user cbt is finished
                    $count_cbt_finished++;
                } else if ($return_value == 4) {//user cbt end date reached
                    $count_cbt_end_date_reached++;
                }

                $j++;
            }
        }
        //fetch all the uids approriately into arrays
        $total_users[$i] = $j; //to fecth all uids into total_users array
        $not_yet_started[$i] = $count_not_yet_started; //to fetch uids nt yet started cbt into not_yet_started array
        $cbt_progress[$i] = $count_cbt_progress; //to fetch uids nt yet started cbt into cbt_progress array
        $cbt_finished[$i] = $count_cbt_finished; //to fetch uids finished cbt into cbt_finished array
        $cbt_end_date_reached [$i] = $count_cbt_end_date_reached; //to fetch uids whose end date reached into cbt_end_Date_reached array
        $i++; //incrementing tid
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <title>Report</title>
        <!-- DataTables -->
        <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/core_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/components_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/menu_clnc.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/default.css" rel="stylesheet" type="text/css" />
        <style>
            .dataTables_length{
                display: none;
            }
            .dataTables_filter{
                display: none;
            }
            .dataTables_info{
                display: none;
            }
            .dataTables_info{
                color:#C0C0C0;
            }
            .table-striped > tbody > tr:nth-of-type(odd), .table-hover > tbody > tr:hover, .table > thead > tr > td.active, .table > tbody > tr > td.active, .table > tfoot > tr > td.active, .table > thead > tr > th.active, .table > tbody > tr > th.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > tbody > tr.active > td, .table > tfoot > tr.active > td, .table > thead > tr.active > th, .table > tbody > tr.active > th, .table > tfoot > tr.active > th {
                background-color: #fff !important;
            }
            .tooltip fade bottom in{
                left:0px;
            }
            th,td{
                text-align: center;
            }
        </style>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../../assets/js/modernizr.min.js"></script>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:65%;margin-top:1%;">
                    <p style="margin-top:1%;font-size: 16px;font-weight:bold"><span>Admin Console</span></p>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title" style="font-family:'Open Sans';color:#223C80;">Client Report</h4>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <HR class="hr_class_sidebar" style="margin-bottom:20px;margin-top:-8%;">
                        <h5>
                            <a href="#"  style="font-family:'Open Sans';font-weight:bolder; font-size:13px; color:#6FA7D7;margin-bottom:20px;"><?php echo $admin_name ?></a>
                        </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="../my_account.php" alt="settings" >
                                    <i class="zmdi zmdi-settings" style="color:#6FA7D7;"></i>
                                </a>
                            </li>

                            <li>
                                <a href="../logout.php" class="text-custom">
                                    <i class="zmdi zmdi-power" style="color:#6FA7D7;"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="sidebar-menu" style="margin-top:-10%;">
                        <ul>
                            <?php include '../sidebar.php'; ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <div class="card-box">
                            <article style="padding-left: 10px;padding-right: 10px;">
                                <h5 class="title">
                                    Reports
                                </h5>

                                <div class="panel-body">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">#<br/>&nbsp;</th>
                                                <th rowspan="2">Coach / Clinician Name<br/>&nbsp;</th>
                                                <th rowspan="2">Total<br/>&nbsp;</th>
                                                <th rowspan="2">Not Yet Started<br/>&nbsp;</th>
                                                <th colspan="3">Progress</th>
                                                <th rowspan="2">Completed<br/>&nbsp;</th>
                                            </tr>
                                            <tr>
                                                <th>Active</th>
                                                <th>In-Active</th>
                                                <th>Date Reached</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($j = 0; $j < count($tid); $j++) {//counter to count all tids
                                                $sno = $j + 1; //to calculateserial number
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $sno; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $tid_name[$j]; ?>
                                                    </td>
                                                    <td>
                                                        <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=total"><?php echo $total_users[$j]; ?></a>
                                                    </td>
                                                    <td>
                                                        <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=not_yet_started"><?php echo $not_yet_started[$j]; ?></a>
                                                    </td>
                                                    <td>
                                                        <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=active"><?php echo $cbt_progress[$j]; ?></a>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=end_date_reached"><?php echo $cbt_end_date_reached[$j]; ?></a>
                                                    </td>
                                                    <td>
                                                        <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=finished"><?php echo $cbt_finished[$j]; ?></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </article>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->
                </div> 
            </div> 
            <footer class="footer">
                <p style="text-align:left;">2017 &copy; SilverOakHealth.
                </p>
            </footer>
        </div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>
        <!-- Datatables-->
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/jquery.dataTables.min_clnc.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.bootstrap.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.buttons.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.html5.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/buttons.print.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.fixedHeader.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.keyTable.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.responsive.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/responsive.bootstrap.min.js"></script>
        <script src="https://s3.ap-south-1.amazonaws.com/clinician/js/dataTables.scroller.min.js"></script>
        <!-- Datatable init js -->
        <script src="../../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../../assets/js/jquery.core.js"></script>
        <script src="../../assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();
        </script>
    </body>
</html>