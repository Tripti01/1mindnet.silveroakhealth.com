<?php
/*
  1.THIS IS A MODAL OPENS AFTER THE ASSIGN BUTTON IS CLICKED
  2.it displays all the therapist which we can assign for particular client or user
  3.therapist can click on the radio button and assign the therapist to client
 */

include '../../../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}

#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_user_count.php';
include '../admin_mail/send_user_assigned_mail.php';


#if set then fetch the status code else set to 0
if (isset($_REQUEST['status_code'])) {
    $status_code = $_REQUEST['status_code'];
} else {
    $status_code = 0;
}0;

//intialisation of arrays
$not_yet_started = array();
$cbt_progress = array();
$cbt_finished = array();
$cbt_end_date_reached = array();
$count = array();
$in_active = array();

//counter initialization
$i = 0;

//start database connection
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Start databse transactions to retrieve data for listing all therapists to be assigned
# Retrieving therapist details
$i = 0;

$stmt06 = $dbh->prepare("SELECT thrp_type.tid,thrp_login.name,thrp_login.email FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = 'THRP' ORDER BY email ASC");
$stmt06->execute(array());
if ($stmt06->rowCount() != 0) {
    while ($row06 = $stmt06->fetch(PDO::FETCH_ASSOC)) {
        $thrp_name[$i] = $row06['name'];
        $thrp_email[$i] = $row06['email'];
        $thrp_id[$i] = $row06['tid'];
        $decrypted_thrp_name[$i] = decrypt($thrp_name[$i], $encryption_key);

        $count_not_yet_started = 0;
        $count_cbt_progress = 0;
        $count_cbt_finished = 0;
        $count_cbt_end_date_reached = 0;
        $count_inactive = 0;
        $j = 0;

        $stmt16 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=? ORDER BY uid;");
        $stmt16->execute(array($thrp_id[$i]));
        if ($stmt16->rowCount() != 0) {
            while ($row16 = $stmt16->fetch(PDO::FETCH_ASSOC)) {
                $uid_all[$j] = $row16['uid'];
                //function to get status of the clients assigned 
                $return_value = 1;//get_user_count($uid_all[$j]);

                if ($return_value == 1) {//user has not yet started cbt
                    $count_not_yet_started++;
                } else if ($return_value == 2) {//user cbt is in progress
                    $count_cbt_progress++;
                } else if ($return_value == 3) {//user cbt is finished
                    $count_cbt_finished++;
                } else if ($return_value == 4) {//user cbt end date reached
                    $count_cbt_end_date_reached++;
                } else if ($return_value == 5) {
                    $count_inactive++;
                }
                $j++;
            }
        }
        //fetch all the uids approriately into arrays
        $cbt_finished[$i] = $count_cbt_finished; //to fetch uids finished cbt into cbt_finished array
        $count[$i] = $j - $cbt_finished[$i]; //to fecth all uids into total_users array
        $i++; //incrementing tid
    }
} else {
    $status_code = 1; // No rows collected
}
$uid = $_REQUEST['uid'];
$stmt08 = $dbh->prepare("SELECT name from user_temp Where uid = ?");
$stmt08->execute(array($uid));
if ($stmt08->rowCount() != 0) {
    $row08 = $stmt08->fetch();
    $enc_name = $row08['name'];
    $name = decrypt($enc_name, $encryption_key);
}
# On submit request the form values
if (isset($_REQUEST['assign_submit'])) {
#CHECK if the radio button submitted has a value assigned or not
    if (isset($_REQUEST['radio'])) {

        if (isset($uid)) {
            $tid = $_REQUEST['radio'];

            $uid = $_REQUEST['uid'];

            $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
            $dbh->query("use sohdbl");

            $stmt00 = $dbh->prepare("SELECT name,email FROM thrp_login WHERE tid = ? LIMIT 1");
            $stmt00->execute(array($tid));
            if ($stmt00->rowCount() != 0) {
                $result00 = $stmt00->fetch();
                $thrp_email = $result00['email'];
                $thrp_name_dec = decrypt($result00['name'], $encryption_key);
            }

            $stmt01 = $dbh->prepare("SELECT name from user_temp Where uid = ?");
            $stmt01->execute(array($uid));
            if ($stmt01->rowCount() != 0) {
                $row01 = $stmt01->fetch();
                $enc_user_name = $row01['name'];
                $user_name = decrypt($enc_user_name, $encryption_key);
            }

            # start database transaction
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh->beginTransaction();
            try {
                #updation of user_therapist according to the tid operation performed up
                $stmt02 = $dbh->prepare("UPDATE user_therapist set tid=? WHERE uid=?");
                $stmt02->execute(array($tid, $uid));

                $stmt03 = $dbh->prepare("INSERT INTO thrp_user_ssn_completed VALUES('','$tid', '$uid', 'CBTSTART', '', '0','','')");
                $stmt03->execute(array());


                $dbh->commit();

                send_user_assigned_email($thrp_email, $thrp_name_dec, $user_name);

                $status_code = 2; //All done successfully
                //to reload the page
                echo '<script>window.location.href="do_assign_therapist.php?uid=' . $uid . '&tid=' . $tid . '&status_code=2"</script>';
            } catch (PDOException $e) {
                $dbh->rollBack();
                die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code : ADMN_DOAS_01");
            }
        } else {
            $status_code = 3; // Some error occured please refresh and try again
        }
    } else {
        $status_code = 4; // select your therapist
    }
}
?>
<!DOCTYPE html>
<html>

    <!-- Mirrored from coderthemes.com/adminto_1.3/light/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Jul 2016 08:36:20 GMT -->
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="../assets/images/favicon.ico">
        <!-- App title -->
        <title>Adminto - Responsive Admin Dashboard Template</title>
        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="../assets/js/modernizr.min.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-74137680-1', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body class="fixed-left" style="background: white;">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Start content -->
            <div class="row">
                <form  action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" id="assign_therapist">
                    <div class="col-sm-12">
                        <?php for ($j = 0; $j < count($thrp_id); $j++) { ?>
                            <div id="msg<?php echo $j; ?>" style="font-weight: bold;font-family: 'Open Sans';text-align: center;"></div>
                        <?php } ?>
                        <div style="text-align: center;font-family: 'Open Sans';color:#9C9E97; font-size: 11px; font-weight: bold;">
                            <p>(Please select one of the therapist to assign to <strong style="color:#30302E;"><?php echo $name; ?></strong> and click assign button below)</p>
                        </div>

                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr style="font-family: 'Open Sans';">
                                    <th>            </th>
                                    <th style="font-weight: bold;color:#188ae2;">Therapist's Name</th>
                                    <th style="font-weight: bold;color:#188ae2;"> Therapist's Email</th>
                                    <th style="font-weight: bold;color:#188ae2;">No. of Clients assigned</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($status_code == '1') {
                                    echo "No therapists available";
                                } else {
                                    for ($i = 0; $i < count($thrp_id); $i++) {
                                        ?>
                                        <tr style="font-family: 'Open Sans';">
                                            <td><div class="radio radio-primary">
                                                    <input type="radio" name="radio" id="radio<?php echo $i; ?>" value="<?php echo $thrp_id[$i]; ?>"  >
                                                    <label></label>
                                                </div>
                                            </td>
                                            <td style=""><?php echo $decrypted_thrp_name[$i]; ?></td>
                                            <td style=""><?php echo $thrp_email[$i]; ?></td>
                                            <td style=""><?php echo $count[$i]; ?></td>                                                                        
                                        </tr>
                                        <?php
                                    }
                                }
                                if ($status_code != 0 && $status_code != 1) {
                                    switch ($status_code) {
                                        case 2: echo '<div class="alert alert-success" style="text-align:center;">';
                                            echo "<strong>Therapist has been assigned. Click the close button to finish.</strong>";
                                            echo '</div>';
                                            break;
                                        case 3: echo '<div class="alert alert-danger" style="text-align:center;">';
                                            echo "<strong>OOPS some error occure please refresh and try again</strong>";
                                            echo '</div>';
                                            break;
                                        case 4: echo '<div class="alert alert-danger" style="text-align:center;">';
                                            echo "<strong>Please choose one of the therapist from the list and then press assign button</strong>";
                                            echo '</div>';
                                            break;
                                        default: break;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <br/>
                        <div style="text-align: right;">
                            <input type="hidden" name="uid" id="uid" value ="<?php echo $uid; ?>">
                            <input type="submit" class="btn btn-success" style="font-family: 'Open Sans';background:#223C80 !important; border-color: #223C80 !important;" value="Assign" id="assign_submit" name="assign_submit">
                            <input type="reset" class="btn btn-custom" style="font-family: 'Open Sans';background:#b1b2b8 !important; border-color: #b1b2b8 !important;"  value="Clear">
                        </div>
                    </div><!-- end col -->
                </form>
            </div> <!-- end row -->
        </div><!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
    </body>
</html>