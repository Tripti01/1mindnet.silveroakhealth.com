<?php
//The algorithm is as follows:
#1. Get the details for the given uid 
#2. if the user has not yet started cbt, fetch the creation time, add the additional days and compare. if the number of days is exceeded return 3,else return 0
#3. if the user is in progress and not yet completed that session,fetch the session start time, add the additional days and compare. if the number of days is exceeded return 1,else return 0
#4. if the user has completed the session and not yet started the next session, fecth the session end time and due_datetime from user_schedule and compare with current datetme.if the number of days is exceeded return 2,else return 0
// This function recieves the UID and returns the status as 
#1. 0- if user still has time, do nothing
#2. 1-if the user has not yet completed the session(in progress) within a number of days(10) from the start of session
#3. 2-if the user has completed the session, but not yet started next sssion within a number of days(10) from the start of session provided the session is not session 8
#4. 3-if the user has not yet started cbt within a number of days(10) from creation 
#$add_days are the number of days extra given to user,further wchich the clinet as to be reminded
#return_date returns the date when the session was started 

function rem_status($uid) {
    global $dbh;
	include 'mindnet-host.php';
    include 'soh-config.php';

    //the number of days to complete the session 
    $add_days = 10;

    # Initial return of status, do nothing
    $return_status = 0;
    #Intial number of days since client status is same
    $return_date = 0;
    #intial return of session s 0
    $return_ssn = 0;

    #to fetch current datetime
    $cur_datetime = strtotime(date('Y-m-d H:i:s'));

    # Start the database realated stuff
    $dbh = new PDO($dsn, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");

    #to check if user has done pre-assessment or not
    $stmt01 = $dbh->prepare("SELECT * FROM asmt_score WHERE uid=? and type=? LIMIT 1");
    $stmt01->execute(array($uid, 'PREA')) or die(print_r($stmt01->errorInfo(), true));
    //if pre-assessment is done, then proceed
    if ($stmt01->rowCount() != 0) {

        //now check the user progress from user_progress
        $stmt = $dbh->prepare("SELECT * FROM user_progress WHERE uid=? ORDER BY seqn DESC LIMIT 10");
        $stmt->execute(array($uid)) or die(print_r($stmt->errorInfo(), true));
        # When ordered by descending order The last ones will be 0 till we encounter 1 or 9
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

            #Do nothing if status 0 is just skip and keep on finding next 1 or 9
            if ($row['status'] == '0') {
                $return_status = 0;
                $return_date = 0;
                $return_ssn = 0;
            } else {

                # We found 9, That means this is in Progres
                if ($row['status'] == '9') {
                    
                    $ssn = $row['ssn']; // Get the session 
                    #to fecth the start date of session
                    $stmt11 = $dbh->prepare("SELECT ssn_starttime FROM user_time_ssn WHERE uid=? AND ssn=? LIMIT 1");
                    $stmt11->execute(array($uid, $ssn)) or die(print_r($stmt11->errorInfo(), true));
                    if ($stmt11->rowCount() != 0) {
                        $row11 = $stmt11->fetch();
                        //we fetch the session starttime and add $add_days to get the reminder check datetime
                        $rem_datetime = $cur_datetime - (24 * 3600 * $add_days);
                    }

                    # Now we have to find if reminder check datetime is less than the current datetime or not
                    //if reminder check datetime is more than the current datetime,then proceed
                    if ($rem_datetime < $cur_datetime) {//if reminder check datetime is more than the current datetime,set as below
                        $return_status = 1; //status for in-progress in a session 
                        $return_date = $row11['ssn_starttime']; //date from wchich in-progress in a session 
                        $return_ssn = $ssn; //session for in-progress 
                    } else {//reminder check datetime is less than the current datetime,set all as 0
                        $return_status = 0;
                        $return_date = 0;
                        $return_ssn = 0;
                    }
                    break;
                } else if ($row['status'] == '1') {//We found 1, That means this is Completed, 
                    $ssn = $row['ssn']; // Get the session 
                    #if session 8 is completed do notihng
                    if ($ssn == "SSN8") {
                        //set all as 0
                        $return_status = 0;
                        $return_date = 0;
                        $return_ssn = 0;
                    } else {//any other session other than session 8
                        #to fecth the end date of session
                        $stmt11 = $dbh->prepare("SELECT ssn_endtime FROM user_time_ssn WHERE uid=? AND ssn=? LIMIT 1");
                        $stmt11->execute(array($uid, $ssn)) or die(print_r($stmt11->errorInfo(), true));
                        if ($stmt11->rowCount() != 0) {
                            $row11 = $stmt11->fetch();
                            //we fetch the session end datetime and add $add_days to get the reminder check datetime
                            $rem_datetime = strtotime(date('Y-m-d H:i:s', strtotime($row11['ssn_endtime']) + (24 * 3600 * $add_days)));
                        }

                        #to get next sequence 
                        $ssn_num = substr($ssn, -1);
                        $ssn_next = 'SSN' . ($ssn_num + 1);

                        #to chech if next session has been started or not
                        $stmt21 = $dbh->prepare("SELECT ssn_starttime FROM user_time_ssn WHERE uid=? AND ssn=? LIMIT 1");
                        $stmt21->execute(array($uid, $ssn_next)) or die(print_r($stmt21->errorInfo(), true));
                        if ($stmt21->rowCount() != 0) {
                            $row21 = $stmt21->fetch();
                            $strt_datetime = $row21['ssn_starttime'];
                        }

                        #proceed only if ssn_starttime is null
                        if ($strt_datetime == '0000-00-00 00:00:00') {

                            # Now we have to find if reminder check datetime is less than the current datetime or not
                            //if reminder check datetime is more than the current datetime,then proceed
                            if ($rem_datetime < $cur_datetime) {
                                $return_status = 2; //status for wchich completed session but not yet started the next session
                                $return_date = $row11['ssn_endtime']; //date from wchich the completed session has not been proceeded
                               $return_ssn = $ssn; //session for wchich completed session
                               
                            } else {//reminder check datetime is less than the current datetime,set all as 0
                                $return_status = 0;
                                $return_date = 0;
                                $return_ssn = 0;
                            }
                        } else {//next session is started hence set all as 0
                            $return_status = 0;
                            $return_date = 0;
                            $return_ssn = 0;
                        }
                        break;
                    }
                }
            }
        }
    } else {
        //if the person has not yet started cbt,we find the details of when the user is registered from user_time_creation
        $stmt121 = $dbh->prepare("SELECT creation_time FROM user_time_creation WHERE uid=? LIMIT 1");
        $stmt121->execute(array($uid)) or die(print_r($stmt121->errorInfo(), true));
        if ($stmt121->rowCount() != 0) {
            $row121 = $stmt121->fetch();
            //we fetch the creation datetime and add $add_days to get the reminder check datetime
            $creation_datetime = strtotime(date('Y-m-d H:i:s', strtotime($row121['creation_time']) + (24 * 3600 * $add_days)));
        }

        //if reminder check datetime is more than the current datetime,then proceed
        if ($creation_datetime < $cur_datetime) {
            $return_status = 3; //status for not yet started cbt
            $return_date = $row121['creation_time']; //creation datetime for cbt start
            $return_ssn = "CBTSTART"; //return session as cbt start
        } else {//reminder check datetime is less than the current datetime,set all as 0
            $return_status = 0;
            $return_date = 0;
            $return_ssn = 0;
        }
    }
    //echo $return_ssn;
    # Reurning values 
    return array($return_status, $return_date, $return_ssn);
}
?>