
<?php
/*
  1.If the privilege is thrp admin login page lands to the therapist dashboard and we can select the rest functionalities.
  2.Admin is allowed to insert the data into the given form and the credentials of ots therapist creation form.
  3.Mail could be sent if needed with particular credentials.
 */
include '../../../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}


#file inclusion for various function happening in the ui
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/uid.php';
include '../admin_mail/send_user_created_email.php';

#Take the name and priviledge list from the session
$admin_email = $_SESSION['email'];

# Status codes for error and success messages
$status_code = 0;
# Current time stamp for therapist creation time
$timestamp = date("Y-m-d H:i:s");


// On form submission check if all the form fields are submitted
if (isset($_REQUEST['submit_btn'])) {

    //to get details from the form
    $name = $_REQUEST['name'];
    $email = $_REQUEST['email'];
    $password = $_REQUEST['pass1'];
    $c_password = $_REQUEST['pass2'];
    $gender = $_REQUEST['gender'];

    //if isset the values go ahead
    if (isset($name) && !empty($name) && $name !== '' && isset($email) && !empty($email) && $email !== '' && isset($password) && !empty($password) && $c_password !== '' && isset($c_password) && !empty($c_password) && $c_password !== '') {

        //type is set to therapist
        $thrp_type = 'THRP';

        // check if the password and confirm password are same
        if (strcmp($password, $c_password) == 0) {
            // if the all the conditions are checked then start database transaction
            $encrypted_name = encrypt($name, $encryption_key); //name is encrypted
            $hash_options = array('cost' => 11); //hash password
            $hashed_password = password_hash($password, PASSWORD_DEFAULT, $hash_options);
            $tid = create_new_uid("THRP");

            //database connection
            $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
            $dbh->query("use sohdbl");

            #Checking email already exist or not, If not will verify the password
            $stmt01 = $dbh->prepare("SELECT name FROM thrp_login WHERE email=? LIMIT 1");
            $stmt01->execute(array($email));
            if ($stmt01->rowCount() != 0) {
                $status_code = 1; // The email id is already registerd
            } else {
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $dbh->beginTransaction();
                try {
                    $stmt02 = $dbh->prepare("INSERT INTO thrp_login VALUES(?,?,?,?);");
                    $stmt02->execute(array($email, $tid, $encrypted_name, $hashed_password));

                    $stmt03 = $dbh->prepare("INSERT INTO thrp_profile VALUES (?,?,?,?,?)");
                    $stmt03->execute(array($tid, $gender, '', '', ''));

                    $stmt04 = $dbh->prepare("INSERT INTO thrp_time_creation VALUES (?,?)");
                    $stmt04->execute(array($tid, $timestamp));

                    $stmt05 = $dbh->prepare("INSERT INTO thrp_info VALUES (?,?,?,?,?)");
                    $stmt05->execute(array($tid, '', '', '', ''));

                    $stmt06 = $dbh->prepare("INSERT INTO thrp_location VALUES (?,?,?,?,?,?,?,?)");
                    $stmt06->execute(array($tid, '', '', '', '', '', '', ''));

                    $stmt07 = $dbh->prepare("INSERT INTO thrp_profile2 VALUES (?,?,?,?,?,?)");
                    $stmt07->execute(array($tid, '', '', '', '', ''));

                    $stmt08 = $dbh->prepare("INSERT INTO thrp_type VALUES (?,?,?,?,?,?)");
                    $stmt08->execute(array($tid, $thrp_type, '', '', '', ''));

                    $stmt09 = $dbh->prepare("INSERT INTO thrp_comm_pref VALUES (?,?,?,?,?,?,?)");
                    $stmt09->execute(array($tid, $email, '', '', '', '', ''));

                    # Send therapist created mail to the admin with the login details
                    send_thrp_created_email($admin_email, $name, $email, $password);

                    $status_code = 2; //all done successfully
                    $dbh->commit();
                } catch (PDOException $e) {
                    $dbh->rollBack();
                    echo $e->getMessage();
                    die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code : THRP_CRE_01");
                }
            }
        } else {
            $status_code = 3; //passwords dont match
        }
    } else {
        $status_code = 4; //some empty fields were submitted
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App title -->
        <title>Create Therapist | Silver Oak Health</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
       	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
         
        <style>
			.error{
				color:red;
				font-weight:400;
			}
            ::-webkit-input-placeholder {
                font-family:'Open Sans';
                color: #B3B3AC;
                font-size: 11px;
                font-weight: bold;
            }

            :-moz-placeholder { /* Firefox 18- */
                font-family:'Open Sans';
                color: #B3B3AC;  
                font-size: 11px;
                font-weight: bold;
            }

            ::-moz-placeholder {  /* Firefox 19+ */
                font-family:'Open Sans';
                color: #B3B3AC; 
                font-size: 11px;
                font-weight: bold;
            }

            :-ms-input-placeholder {  
                font-family:'Open Sans';
                color: #B3B3AC; 
                font-size: 11px;
                font-weight: bold;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .hr_class_sidebar{
                margin: 20px 0;
                border: 0;
                border-top: 1px solid #eee;
                border-bottom: 0;
            }
            article {
                position: relative;
                padding: 0 5em;
                border: 1px solid #223C87;
                margin-bottom: 3em;
                border-radius: 5px;
            }

            article:hover {
                border: 1px solid #3AB149;
            }

            article:first-child {
                margin-top: 2em;
            }

            article .title {
                position: absolute;
                left: 30px;
                top: -25px;
                padding-left: 5px; padding-right: 5px;
                padding-bottom: 5px;
                font-family: 'Calibri';
                background-color: #fff;
                font-size:20px;
            }

            @media (max-width: 960px) {

                article {
                    padding: 0 2em;
                }
            }
            .article{
                line-height:10px;
            }
            .hr_css{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(0, 0, 0, 0.95), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
                margin-top: -15px;
                margin-bottom: 20px;
            }


        </style>
    </head>
      <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="y-overflow:auto;">
                                <div class="card-box">
                                    <article>
                                        <h5 class="title">
                                            Please provide the following details
                                        </h5>
                                        <div style="text-align: center; font-family:'Open Sans'; color:#9C9E97; font-size: 11px; font-weight: bold;margin-top:2%;"><p>(Please enter the therapist details. After successful creation of the therapist an email will be sent to <strong style="color:#30302E;"><?php echo $admin_email; ?></strong> with the login details)</p></div>
                                        <section>
                                            <div class="row">  
                                                <!--To display error messgaes --->
                                                <div class="email-confirm-msg" style="margin-top:2px;">
                                                    <?php
                                                    if ($status_code != 0) {
                                                        switch ($status_code) {
                                                            case 1: echo '<div class="alert alert-warning" style="text-align:center;">';
                                                                echo "<strong> This email is already registered</strong>";
                                                                echo '</div>';
                                                                break;
                                                            case 2:
                                                                echo '<div class="alert alert-success" style="text-align:center;">';
                                                                echo "<strong>Therapist has been created successfully.</strong>";
                                                                echo '</div>';
                                                                break;
                                                            case 3:
                                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                                echo "<strong>The password's dont match</strong>";
                                                                echo '</div>';
                                                                break;
                                                            case 4:
                                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                                echo "<strong>Some empty fields were submitted, Please try again</strong>";
                                                                echo '</div>';
                                                                break;
                                                            default: break;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" class="form-horizontal" name="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-md-12 label_css" style="margin-top: 8px;margin-bottom:1px;">Name</label>
                                                    </div>
                                                    <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="userName"  style="font-weight:normal;text-align:left;">Therapist name <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Enter Therapist Name" class="form-control" id="name" name="name"/>
                                                            <!--div id="err_name" class="error"></div-->
                                                        </div> 
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-12 label_css" style="margin-bottom:1px;">Login Details</label>
                                                    </div>
                                                    <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="email"  style="font-weight:normal;text-align:left;">Email address <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="email" name="email" placeholder="Enter email address" class="form-control" id="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="pass1" style="font-weight:normal;text-align:left;">Password <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="password" placeholder="Enter Password" class="form-control" id="pass1" name="pass1"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="pass2"  style="font-weight:normal;text-align:left;">Confirm Password <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="password" placeholder="Confirm Password" class="form-control" id="pass2" name="pass2" />
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-12 label_css" style="margin-bottom:1px;">Profile Details</label>
                                                    </div>
                                                    <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="mobile"  style="font-weight:normal;text-align:left;">Gender <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <select id="gender" name="gender" title="" required class="form-control" style="margin-top:0px;color: #808080;font-size: 11px;font-weight: bold;">
                                                                <option value="-1" disabled="" selected="" style="font-weight:bold;color:#808080;">  -- SELECT --  </option> 
                                                                <option value="M" style="font-weight:bold;color:#808080;font-size:14px;"> Male </option>
                                                                <option value="F" style="font-weight:bold;color:#808080;font-size:14px;"> Female </option>
                                                            </select>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-12 label_css" style="margin-bottom:1px;">Contact Details</label>
                                                    </div>
                                                    <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                    <div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="Location"  style="font-weight:normal;text-align:left;">Location <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="location"  class="form-control" id="location" value='Silver Oak Health, Bangalore, India' disabled="true" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group text-right m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                                        <input type="submit" class="btn btn-primary" style="background:#223c87 !important; border-color: #223c87 !important;font-family:'Open Sans';" value=" Create Therapist"  name = "submit_btn" id="submit_btn">                                          
                                                        <button type="reset" class="btn btn-custom" style="background:#b1b2b8 !important; border-color: #b1b2b8 !important;font-family:'Open Sans';">
                                                            Clear
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </section>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <p style="text-align:left;"><script type="text/javascript">var year = new Date();document.write(year.getFullYear());</script> &copy; SilverOakHealth.
                    </p>
                </footer>
            </div>
            <script src="../assets/js/jquery.min.js"></script>
            <script src="../assets/js/bootstrap.min.js"></script>
            <script src="../assets/js/fastclick.js"></script>
            <script src="../assets/js/jquery.blockUI.js"></script>
            <script src="../assets/js/waves.js"></script>
            <script src="../assets/js/jquery.scrollTo.min.js"></script>
            <script src="../assets/js/jquery.core.js"></script>
            <script src="../assets/js/jquery.app.js"></script>
			<script src="../assets/js/app.min.js"></script>
			<script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
            <!---Form validation---->
            <script>
                        $(".form-horizontal").validate({
                            rules: {
                                name: {
                                    required: true,
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                pass1: {
                                    required: true,
                                },
                                pass2: {
                                    required: true,
                                    equalTo: "#pass1"
                                }

                            },
                            messages: {
                                name: {
                                    required: "Please enter your full name"
                                },
                                email: {
                                    required: "Please enter your email",
                                    email: "Invalid Email ID"
                                },
                                pass1: {
                                    required: "Please enter password",
                                    pass1: "Invalid Password"
                                },
                                pass2: {
                                    required: "Please enter same password",
                                    pass1: "Invalid password"
                                }
                            }

                        });
            </script>
            <script type="text/javascript">
                //script to enter only alphabets in text box
                $("#name").focusout(function () {
                    var name_text = document.getElementById("name").value;

                    if ((name_text.match(/^[a-zA-Z]+$/) == null) && (name_text != ""))
                    {
                        $("#err_name").html("Invalid name");

                    }
                });
            </script>
    </body>
</html>