<?php
/*
  1.we can re-assign the therapist for the client if we need to, in this file
  2.Here the details can be viewed such as client's name loaction age
  3.on click of re-assign button the modal opens where we can assign the new therapist
 */
include '../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}


#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/get_type_full_name.php';

# Take the name from the session
$admin_name = $_SESSION['name'];

# initialize status code for error msgs
$status_code = 0;

$cur_year = date('Y');

# Start database transaction
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Retrieve all the users for whom therapist is not assigned
$i = 0; //counter to fecth all tid and uids
// 
# Database transactions for retrieveing list of users with unassigned therapists
$stmt01 = $dbh->prepare("SELECT user_therapist.uid,user_therapist.tid,user_time_creation.uid,user_time_creation.creation_time FROM user_therapist,user_time_creation,user_schedule WHERE user_therapist.uid=user_time_creation.uid AND user_schedule.uid=user_time_creation.uid  AND user_therapist.tid != '' AND user_schedule.ssn = ? AND user_schedule.due_datetime > CURDATE() ORDER BY user_time_creation.creation_time DESC");
$stmt01->execute(array("CBTEND"));
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $uid[$i] = $row01['uid'];
        $tid[$i] = $row01['tid'];
        $creation_time[$i] = date('d M Y', strtotime($row01['creation_time']));

# retrieve name, email and user type location of user, gender and age for users
        $stmt02 = $dbh->prepare("SELECT user_temp.name,user_temp.email,user_temp.ref_type,user_location.country,user_location.state,user_location.city,user_profile.gender,user_profile.yob FROM user_temp,user_location,user_profile WHERE user_temp.uid=user_profile.uid AND user_location.uid=user_profile.uid AND user_temp.uid = ? LIMIT 1");
        $stmt02->execute(array($uid[$i]));
        if ($stmt02->rowCount() != 0) {
            $row02 = $stmt02->fetch();
            $encrypted_name[$i] = $row02['name'];
            $name[$i] = decrypt($encrypted_name[$i], $encryption_key);
            $ref_type[$i] = $row02['ref_type'];
//if location is set then retrieve location,else set as balnk
            $user_country[$i] = $row02['country'];
            $user_state[$i] = $row02['state'];
            $user_city[$i] = $row02['city'];
            $gender[$i] = $row02['gender'];
            $yob[$i] = $row02['yob'];
            $age[$i] = date('Y') - $yob[$i];
        } else {
            $user_country[$i] = "";
            $user_state[$i] = "";
            $user_city[$i] = "";
            $age[$i] = "";
        }
        $location[$i] = $user_city[$i] . " " . $user_state[$i] . " " . $user_country[$i] . ""; //combing all to get location
//fetching the name form thrp_login
        $stmt05 = $dbh->prepare("SELECT name FROM thrp_login WHERE tid=? LIMIT 1");
        $stmt05->execute(array($tid[$i]));
        if ($stmt05->rowCount() != 0) {
            $result05 = $stmt05->fetch();
            $current_thrp_name[$i] = decrypt($result05['name'], $encryption_key);
        }

        $i++;
    }
} else {
    $status_code = 1; // No rows collected
}
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App title -->
        <title>Reassign Therapist | Silver Oak Health</title>
        <!-- Custom box css -->
        <link href="../../assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <!-- App CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="../../assets/js/modernizr.min.js"></script>
        <style>
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .hr_class_sidebar{
                margin: 20px 0;
                border: 0;
                border-top: 1px solid #eee;
                border-bottom: 0;
            }
        </style>
    </head>
    <body class="fixed-left"><!-- Begin page -->
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:65%;margin-top:1%;">
                    <p style="margin-top:1%;font-size: 16px;font-weight:bold"><span>Admin Console</span></p>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title" style="font-family:'Open Sans';color:#223C80;">Re-assign Therapist</h4>
                            </li>
                        </ul>
                    </div>
                </div>          
            </div>
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-8%;"/><h5>
                            <a href="#"  style="font-family:'Open Sans';font-weight:bolder; font-size:13px; color:#6FA7D7;margin-bottom:20px;"><?php echo $admin_name ?></a> 
                        </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="../my_account.php" alt="settings" >
                                    <i class="zmdi zmdi-settings" style="color:#6FA7D7;"></i>
                                </a>
                            </li>

                            <li>
                                <a href="../logout.php" class="text-custom">
                                    <i class="zmdi zmdi-power" style="color:#6FA7D7;"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="sidebar-menu" style="margin-top:-10%;">
                        <ul>
                            <?php include '../sidebar.php'; ?>			
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="content-page" style="margin-top: 0.5%;"><!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                                    <div class="panel-body" style="background-color:#FCFCFB;">
                                        <table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                                <tr style="font-family:'Open Sans';font-weight: bold;color:#188ae2;">
                                                    <th style="width:25%;">Client's Name<br/>&nbsp;</th>
                                                    <th style="width:5%;">Age/<br/>Gender</th>
                                                    <th style="width:25%;">Location<br/>&nbsp;</th>
                                                    <th style="width:10%;">Registration<br/>Date</th>
                                                    <th style="width:25%;">Therapist Assigned<br/>&nbsp;</th>
                                                    <th style="width:5%;">Client<br/> Type</th>
                                                    <th style="width:5%;">            </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
//if status code is 1 therapist is not available else get the name
                                                if ($status_code == '1') {
                                                    echo "No therapists available";
                                                } else {
                                                    for ($i = 0; $i < $stmt01->rowCount(); $i++) {
                                                        ?>
                                                        <tr style="font-family:'Open Sans';">
                                                            <td><?php
                                                                if (isset($name[$i])) {
                                                                    echo $name[$i];
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?></td>
                                                            <td>
                                                                <?php
                                                                if (isset($age[$i]) && isset($gender[$i]) && $age[$i] != null && $gender[$i] != null && $age[$i] != $cur_year) {
                                                                    echo $age[$i];
                                                                    echo "/";
                                                                    echo $gender[$i];
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?></td>
                                                            <td><?php
                                                                if (isset($location[$i])) {
                                                                    echo $location[$i];
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>
                                                            </td>
                                                            <td><?php
                                                                if (isset($creation_time[$i])) {
                                                                    echo $creation_time[$i];
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?></td>
                                                            <td><?php
                                                                if (isset($current_thrp_name[$i])) {
                                                                    echo $current_thrp_name[$i];
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>
                                                            </td> <td><?php get_user_type_full_name($uid[$i]) ?></td>
                                                            <td><a class="btn btn-success" style="background:#223C80 !important; border-color: #223C80 !important;" data-toggle="modal" data-target="#custom-width-modal<?php echo $i ?>" >Reassign</a></td>
                                                    <div id="custom-width-modal<?php echo $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog" style="width:55%;">
                                                            <div class="modal-content p-0 b-0">
                                                                <div class="panel panel-color panel-primary">
                                                                    <div class="panel-heading">
                                                                        <button type="button" class="close" id='close' data-dismiss="modal" aria-hidden="true" style="color:white;">X</button>
                                                                        <h4 class="modal-title" style="font-weight: bold;color:white;font-family:'Open Sans';">Select Therapist</h4>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <form  method="POST" id="assign_therapist<?php echo $i; ?>">
                                                                            <div class="modal-body" style='padding:0px;'>                        
                                                                                <iframe src="do_re_assign_therapist.php?uid=<?php echo $uid[$i]; ?>&tid=<?php echo $tid[$i]; ?>" style="zoom:0" width="100%" height="400" frameborder="0"></iframe>                                    
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.modal -->
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div> <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->


                <footer class="footer">
                    2017 © Silver Oak Health.
                </footer>
            </div>
        </div><!-- END wrapper -->

        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.app.js"></script>
        <script src="../../assets/js/jquery.core.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script>
            $(".close").click(function () {
                window.top.location.href = "<?php echo $host . '/modules/admin/therapistadmin/re_assign_therapist.php'; ?>";
            });
        </script>
    </body>
</html>