<?php
/*
  1.therapist who are currently assigned can be assigned to particular user
  2.and the modal will be opened to assign the various therapist present
 */

include '../../../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}


#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'functions/get_type_full_name.php';

# initialize status code for error $dsn_sco_sco
$status_code = 0;

$cur_year=date('Y');
# Start database transaction
# Initialize loop index value to 0
$i = 0;
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Retrieve all the users for whom therapit is not assigned
# Database transactions for retrieveing list of users with unassigned therapists
$stmt00 = $dbh->prepare("SELECT user_therapist.uid,user_therapist.tid,user_time_creation.uid,creation_time FROM user_therapist,user_time_creation WHERE user_therapist.uid=user_time_creation.uid && user_therapist.tid = '' ORDER BY creation_time ASC");
$stmt00->execute(array());
$count_of_unassigned_users = $stmt00->rowCount();

$stmt01 = $dbh->prepare("SELECT user_therapist.uid,user_therapist.tid,user_time_creation.uid,creation_time FROM user_therapist,user_time_creation WHERE user_therapist.uid=user_time_creation.uid && user_therapist.tid = '' ORDER BY creation_time ASC LIMIT 40");
$stmt01->execute(array());
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $uid[$i] = $row01['uid'];
# retrieve name email and user type
        $stmt02 = $dbh->prepare("SELECT name,email,ref_type, mobile FROM user_temp WHERE uid = ?");
        $stmt02->execute(array($uid[$i]));
        if ($stmt02->rowCount() != 0) {
            $row02 = $stmt02->fetch();
            $encrypted_name[$i] = $row02['name'];
            $user_email[$i] = $row02['email'];
             $mobile[$i] = $row02['mobile'];
            $name[$i] = decrypt($encrypted_name[$i], $encryption_key); //name is decrypted 
            $ref_type[$i] = $row02['ref_type']; //fetch the ref type if its individual corporate
        }
# retrieve location of user
        $stmt03 = $dbh->prepare("SELECT country, state, city FROM user_location WHERE uid=?");
        $stmt03->execute(array($uid[$i]));
        //if the location is set fetch data else do nothing
        if ($stmt03->rowCount() != 0) {
            $result03 = $stmt03->fetch();
            $user_country[$i] = $result03['country'];
            $user_state[$i] = $result03['state'];
            $user_city[$i] = $result03['city'];
        } else {
            $user_country[$i] = "";
            $user_state[$i] = "";
            $user_city[$i] = "";
        }
//combining all to get the location
        $location[$i] = $user_city[$i] . " " . $user_state[$i] . " " . $user_country[$i] . "";
# select gender and age for these users
        $stmt04 = $dbh->prepare("SELECT gender,yob FROM user_profile WHERE uid=?");
        $stmt04->execute(array($uid[$i]));
        if ($stmt04->rowCount() != 0) {
            $result04 = $stmt04->fetch();
            $gender[$i] = $result04['gender'];
            $yob[$i] = $result04['yob'];
            $age[$i] = date('Y') - $yob[$i]; //get the age by subtracting this year with year of birth
        }
# retrieving creation time 
        $stmt05 = $dbh->prepare("SELECT creation_time FROM user_time_creation WHERE uid = ?");
        $stmt05->execute(array($uid[$i]));
        if ($stmt05->rowCount() != 0) {
            $row05 = $stmt05->fetch();
            $creation_time[$i] = $row05['creation_time'];
            $creation_time[$i] = date('d M Y', strtotime($creation_time[$i]));
        }
        $i++;
    }
}
# Start databse transactions to retrieve data for listing all therapists to be assigned
# Retrieving therapist details
$i = 0;
$stmt06 = $dbh->prepare("SELECT name,email,tid FROM thrp_login ORDER BY email ASC");
$stmt06->execute(array());
if ($stmt06->rowCount() != 0) {
    while ($row06 = $stmt06->fetch(PDO::FETCH_ASSOC)) {
        $thrp_name[$i] = $row06['name'];
        $thrp_email[$i] = $row06['email'];
        $thrp_id[$i] = $row06['tid'];
        $decrypted_thrp_name[$i] = decrypt($thrp_name[$i], $encryption_key);
        $stmt07 = $dbh->prepare("SELECT count(uid) AS count_user FROM user_therapist WHERE tid = ?");
        $stmt07->execute(array($thrp_id[$i]));
        if ($stmt07->rowCount() != 0) {
            $row07 = $stmt07->fetch();
            $count[$i] = $row07['count_user'];
        }
        $i++;
    }
} else {
    $status_code = 1; // No rows collected
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App title -->
        <title>Assign Therapist | Silver Oak Health</title>
        <!-- Custom box css -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <style>
			.hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .hr_class_sidebar{
                margin: 20px 0;
                border: 0;
                border-top: 1px solid #eee;
                border-bottom: 0;
            }
        </style>
    </head>
      <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
								<div class="card-box"><a class="btn btn-primary" style="background-color: #223c87 !important; border-color: #223c87 !important;float:right;" 
								href="re_assign_therapist.php">Re-Assign Therapist</a>
								<BR/><BR/>
                                <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                                    <div class="panel-body" style="background-color:#FCFCFB;">
                                        <table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                                <tr style="font-weight: bold;color:#188ae2;font-family:'Open Sans';">
                                                    <th style="width:25%;">Client's Name</th>
                                                   
                                                    <th style="width:30%;">Contact</th>
                                                    <th style="width:20%;">Registration Date</th>
                                                    <th style="width:20%;">Client Type</th>
                                                    <th style="width:20%;"> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($status_code == '1') {
                                                    echo "No therapists available";
                                                } else {
                                                    for ($i = 0; $i < $stmt01->rowCount(); $i++) {
                                                        ?>
                                                        <tr>
                                                            <td  style="font-weight: bold;"><?php if(isset($name[$i]) && $name[$i]!=""){ echo $name[$i];}else {echo "";} ?></td>
                                                        
                                                            <td  style="font-weight: bold;"><?php echo $user_email[$i]; ?> / <?php echo $mobile[$i]; ?></td>
                                                            <td  style="font-weight: bold;"><?php echo $creation_time[$i]; ?></td>
                                                            <td  style="font-weight: bold;"><?php get_user_type_full_name($uid[$i]) ?></td>
                                                            <td><a class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#custom-width-modal<?php echo $i ?>" >Assign</a></td>
                                                        </tr>

                                                        </div> <!-- container -->
                                                    <div id="custom-width-modal<?php echo $i ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog" style="width:55%;">
                                                            <div class="modal-content p-0 b-0">
                                                                <div class="panel panel-color panel-primary">
                                                                    <div class="panel-heading">
                                                                        <button type="button" class="close" id='close' data-dismiss="modal" aria-hidden="true" style="color:white;">X</button>
                                                                        <h4 class="modal-title" style="font-weight: bold;color:white;font-family:'Open Sans';">Select Therapist</h4>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <form  method="POST" id="assign_therapist">
                                                                            <div class="modal-body" style='padding:0px;'>                        
                                                                                <iframe src="do_assign_therapist.php?uid=<?php echo $uid[$i]; ?>" style="zoom:0" width="100%" height="400" frameborder="0"></iframe>                                    
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.modal -->
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <div style="float:right;"><span style="font-weight:normal;">(The count of users without a therapist assigned is <strong style="color:#9C9E97;"><?php echo $count_of_unassigned_users; ?></strong>)</span></div>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                    </div>          
                </div>
            </div>
            <!-- END wrapper -->
            <script>
                var resizefunc = [];
            </script>
            <!-- jQuery  -->
            <script src="../assets/js/jquery.min.js"></script>
            <script src="../assets/js/bootstrap.min.js"></script>
            <script src="../assets/js/detect.js"></script>
            <script src="../assets/js/fastclick.js"></script>
            <script src="../assets/js/jquery.slimscroll.js"></script>
            <script src="../assets/js/jquery.blockUI.js"></script>
            <script src="../assets/js/waves.js"></script>
            <script src="../assets/js/jquery.nicescroll.js"></script>
            <script src="../assets/js/jquery.scrollTo.min.js"></script>
            <!-- App js -->
            <script src="../assets/js/jquery.core.js"></script>
            <script src="../assets/js/jquery.app.js"></script>

            <!-- Modal-Effect -->
            <script src="../assets/plugins/custombox/dist/custombox.min.js"></script>
            <script src="../assets/plugins/custombox/dist/legacy.min.js"></script>
			<script src="../assets/js/app.min.js"></script>

            <script>
                $(".close").click(function () {
                    window.top.location.href = "<?php echo $host . '/modules/admin/therapistadmin/assign_therapist.php'; ?>";
                });
            </script>
    </body>
</html>