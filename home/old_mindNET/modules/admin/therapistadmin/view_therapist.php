<?php
/*
  1.In the sidebar when view_therapist is clicked we land onto this page.
  2.All the therapist admin can be seen here, there name, email, id and no. of clients are displayed.
 */

include '../../../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}


#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include 'get_user_count.php';

# Status codes for error 
$status_code = 0;

$current_date = date('d/m/Y');


//intialisation of arrays
$not_yet_started = array();
$cbt_progress = array();
$cbt_finished = array();
$cbt_end_date_reached = array();
$total_users = array();
$in_active = array();
$i = 0; //counter to fetch all the therapist
# Start the database realated stuff
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

//qury to fecth all tids
$stmt00 = $dbh->prepare("SELECT thrp_login.tid,thrp_login.name,thrp_type.type FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = 'THRP'");
$stmt00->execute(array());
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {

        $tid[$i] = $row00['tid']; //to fetch therpaist
        $tid_name[$i] = decrypt($row00['name'], $encryption_key); //to fetch therapist name
        //intializ counter to 0, for every tid
        $count_not_yet_started = 0;
        $count_cbt_progress = 0;
        $count_cbt_finished = 0;
        $count_cbt_end_date_reached = 0;
        $count_inactive = 0;
        $j = 0; //counter for all uids
        //to fetch all uids assigned to therpist
        $stmt100 = $dbh->prepare("SELECT uid FROM user_therapist WHERE tid=? ORDER BY uid;");
        $stmt100->execute(array($tid[$i]));
        if ($stmt100->rowCount() != 0) {
            while ($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)) {
                $uid[$j] = $row100['uid'];
                //function to get status of the clients assigned 
                $return_value = get_user_count($uid[$j]);

                if ($return_value == 1) {//user has not yet started cbt
                    $count_not_yet_started++;
                } else if ($return_value == 2) {//user cbt is in progress
                    $count_cbt_progress++;
                } else if ($return_value == 3) {//user cbt is finished
                    $count_cbt_finished++;
                } else if ($return_value == 4) {//user cbt end date reached
                    $count_cbt_end_date_reached++;
                } else if ($return_value == 5) {
                    $count_inactive++;
                }
                $j++;
            }
        }
        //fetch all the uids approriately into arrays
        $cbt_finished[$i] = $count_cbt_finished; //to fetch uids finished cbt into cbt_finished array
        $total_users[$i] = $j - $cbt_finished[$i]; //to fecth all uids into total_users array
        $not_yet_started[$i] = $count_not_yet_started; //to fetch uids nt yet started cbt into not_yet_started array
        $cbt_progress[$i] = $count_cbt_progress; //to fetch uids nt yet started cbt into cbt_progress array
        $cbt_end_date_reached [$i] = $count_cbt_end_date_reached; //to fetch uids whose end date reached into cbt_end_Date_reached array
        $in_active[$i] = $count_inactive;
        $i++; //incrementing tid
    }
}
?>
<!DOCTYPE html>
<html>
  
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App title -->
        <title>View Therapists | Silver Oak Health</title>

        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
       	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
         
        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .hr_class_sidebar{
                margin: 20px 0;
                border: 0;
                border-top: 1px solid #eee;
                border-bottom: 0;
            }
            .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
                background-color: #2590e3;
                border-color: #2590e3;
            }
        </style>
    </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
								<a class="btn btn-primary" style="background-color: #223c87 !important; border-color: #223c87 !important;float:right;margin:10px;" 
								href="create_therapist.php">Create Therapist</a>
								<BR/><BR/>
                                    <div class="panel-body" style="background-color:#FCFCFB;">
                                        <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2" style="color:#188ae2;text-align:center;width:3%;">ID<br/>&nbsp;</th>
                                                    <th rowspan="2" style="color:#188ae2;text-align:center;width:30%;">Therapist Name<br/>&nbsp;</th>
                                                    <th rowspan="2" style="color:#188ae2;text-align:center;width:5%;">Total<br/>&nbsp;</th>
                                                    <th rowspan="2" style="color:#188ae2;text-align:center;width:18%;">Not Yet Started<br/>&nbsp;</th>
                                                    <th colspan="3" style="color:#188ae2;text-align:center;width:35%;">Progress</th>
                                                    <th rowspan="2" style="color:#188ae2;text-align:center;width:10%;">Completed<br/>&nbsp;</th>
                                                </tr>
                                                <tr>
                                                    <th style="color:#188ae2;text-align:center;">Active</th>
                                                    <th style="color:#188ae2;text-align:center">In-Active<span style="color:#223c80"> *</span></th>
                                                    <th style="color:#188ae2;text-align:center">Date Reached<span style="color:#223c80"> #</span></th>
                                                </tr>
                                            </thead>
                                            <tbody style="font-family:'Open Sans';">
                                                <?php
//if the status code is 1 echo following message else print the data into tables
                                                if ($status_code == '1') {
                                                    echo "No therapists available";
                                                } else {
                                                    for ($j = 0; $j < count($tid); $j++) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $tid[$j]; ?></td>
                                                            <td><?php echo $tid_name[$j]; ?></td>
                                                            <td align="center"><a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=total" style="font-weight:bold;color:black;"><?php echo $total_users[$j]; ?></a></td>
                                                            <td align="center">
                                                                <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=not_yet_started" style="font-weight:bold;color:#d66a00;" ><?php echo $not_yet_started[$j]; ?></a>
                                                            </td>
                                                            <td align="center">
                                                                <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=active" style="font-weight:bold;"><?php echo $cbt_progress[$j]; ?></a>
                                                            </td>
                                                            <td align="center">
                                                                <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=in_active" style="font-weight:bold;color:red;"><?php echo $in_active[$j]; ?></a>
                                                            </td>
                                                            <td align="center">
                                                                <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=end_date_reached" style="font-weight:bold;color:red;"><?php echo $cbt_end_date_reached[$j]; ?></a>
                                                            </td>
                                                            <td align="center">
                                                                <a href="report_clients.php?tid=<?php echo $tid[$j]; ?>&type=finished" style="font-weight:bold;color:#00be29;"><span style="align:center"><?php echo $cbt_finished[$j]; ?></span></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        (<span style="color:#223c80">*</span>)&nbsp;<span style="margin-right:2px;">Not logged-in from past 2 weeks.</span>
                                        (<span style="color:#223c80">#</span>)&nbsp;<span>CBT end date is reached.</span>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
                <footer class="footer">
                
                </footer>
            </div>
        </div>
        <!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <!-- Datatables-->
        <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="../assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="../assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="../assets/plugins/datatables/buttons.print.min.js"></script>
        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>
		<script src="../assets/js/app.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable({keys: true});
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable({ajax: "../assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true});
                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            });
            TableManageButtons.init();

        </script>
    </body>
</html>