<?php
/*
 * THIS FILE IS OPEN AS MODAL
  1.Here the list of all the therapist is seen from which admin can assign the value
  2.click on the radio button adn assign the therpist and click on update.
 */

include '../if_loggedin.php';
include '../check_prvg.php';
## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("THAD");
if (!$if_allowed_to_view_this_page) {
    exit();
}

#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';
include '../admin_mail/send_user_assigned_mail.php';


//Take the name from the session
$admin_name = $_SESSION['name'];

# Retrieve the uid and tid value passed from assign therapist
$uid = $_REQUEST['uid'];
$tid = $_REQUEST['tid'];

#if set then fetch the status code else set to 0
if (isset($_REQUEST['status_code'])) {
    $status_code = $_REQUEST['status_code'];
} else {
    $status_code = 0;
}

# Start database transaction
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Start databse transactions to retrieve data for listing all therapists to be assigned
# Retrieving therapist details
$i = 0; //counter to fetch all name,email,tid

$stmt06 = $dbh->prepare("SELECT thrp_type.tid,thrp_login.name,thrp_login.email FROM thrp_login, thrp_type WHERE thrp_login.tid = thrp_type.tid AND thrp_type.type = 'THRP' ORDER BY email ASC");
$stmt06->execute(array());
if ($stmt06->rowCount() != 0) {
    while ($row06 = $stmt06->fetch(PDO::FETCH_ASSOC)) {
        $thrp_name[$i] = $row06['name']; //to fetch name
        $thrp_email[$i] = $row06['email']; //to fetch email
        $thrp_id[$i] = $row06['tid']; //to fetch tid
        $decrypted_thrp_name[$i] = decrypt($thrp_name[$i], $encryption_key); //to fetch name,and decrypt it
        $i++; //incrementing tid
    }
} else {
    $status_code = 1; // No rows collected
}

//to fetch the name of user and therapist to whom user is currently assigned to
$stmt08 = $dbh->prepare("SELECT user_temp.name AS u_name ,thrp_login.name AS t_name from user_temp,thrp_login Where uid = ? AND tid = ?");
$stmt08->execute(array($uid, $tid));
if ($stmt08->rowCount() != 0) {
    $row08 = $stmt08->fetch();
    $name = decrypt($row08['u_name'], $encryption_key); //to fetch the user name
    $cur_thrp_name = decrypt($row08['t_name'], $encryption_key); //to fetch the therapist id
}

//on-click of assign-submit
if (isset($_REQUEST['assign_submit'])) {

    # check if the radio button submitted has a value or not
    if (isset($_REQUEST['radio'])) {

        # request uid from hidden value that was passed on submission
        $uid = $_REQUEST['uid'];
        $tid = $_REQUEST['radio']; //to fetch tid from radio button

        if (isset($uid) && isset($tid)) {

            $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
            $dbh->query("use sohdbl");

            //to fetch the therapist details
            $stmt00 = $dbh->prepare("SELECT name,email FROM thrp_login WHERE tid = ? LIMIT 1");
            $stmt00->execute(array($tid));
            if ($stmt00->rowCount() != 0) {
                $result00 = $stmt00->fetch();
                $thrp_email = $result00['email'];
                $thrp_name_dec = decrypt($result00['name'], $encryption_key);
            }
            //to fetch user details
            $stmt01 = $dbh->prepare("SELECT name from user_temp Where uid = ? LIMIT 1");
            $stmt01->execute(array($uid));
            if ($stmt01->rowCount() != 0) {
                $row01 = $stmt01->fetch();
                $user_name = decrypt($row01['name'], $encryption_key);
            }

            //to update user_therapist
            $stmt02 = $dbh->prepare("UPDATE user_therapist set tid=? WHERE uid=?");
            $stmt02->execute(array($tid, $uid));

            //to send user_Assigned _email
            send_user_assigned_email($thrp_email, $thrp_name_dec, $user_name);

            $status_code = 2; //All done successfully
//reload page
            echo '<script>window.location.href="do_re_assign_therapist.php?uid=' . $uid . '&tid=' . $tid . '&status_code=2"</script>';
        } else {
            $status_code = 3; // Some error occured please refresh and try again
        }
    } else {
        $status_code = 4; // select your therapist
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="../../assets/images/favicon.ico">
        <!-- App title -->
        <title>Silver Oak Health - Admin Dashboard</title
        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="../../assets/js/modernizr.min.js"></script>

    </head>
    <body class="fixed-left" style='background: white;'>
        <!-- Begin page -->
        <div id="wrapper">
            <div class="row">
                <form  action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" id="assign_therapist">
                    <div class="col-sm-12">
                        <?php for ($j = 0; $j < count($thrp_id); $j++) { ?>
                            <div id="msg<?php echo $j; ?>" style="font-weight: bold;font-family: 'Open Sans';text-align: center;"></div>
                        <?php } ?>
                        <div style="text-align: center; font-family: 'Open Sans'; color:#9C9E97; font-size: 11px; font-weight: bold;">
                            <p style = "font-size: 14px; font-weight: bold;color:#33b519;"><strong style="color:#30302E;"><?php
                                    if (isset($name) && $name != "") {
                                        echo $name;
                                    } else
                                        echo "No therapist"
                                        ?></strong> is currently assigned to <strong style="color:#30302E;"><?php
                                    if (isset($cur_thrp_name) && $cur_thrp_name != "") {
                                        echo $cur_thrp_name;
                                    } else {
                                        echo"Client";
                                    }
                                    ?></strong></p>
                            <p>(Please select one of the therapist to assign <strong style="color:#30302E;"><?php
                                    if (isset($name) && $name != "") {
                                        echo $name;
                                    } else
                                        echo ""
                                        ?></strong> and click assign button below)</p>
                        </div>
                        <div><hr style=' border: 0;height: 1px;margin-top:1%;margin-bottom:1%;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));'>
                        </div><br/>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr style="font-family: 'Open Sans';">
                                    <th>            </th>
                                    <th style="color:#188ae2;">Therapist's Name</th>
                                    <th style="color:#188ae2;"> Therapist's Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($status_code == '1') {
                                    echo "No therapists available";
                                } else {
                                    for ($i = 0; $i < count($thrp_id); $i++) {
                                        ?>
                                        <tr style="font-family: 'Open Sans';">
                                            <td><div class="radio radio-primary">
                                                    <input type="radio" name="radio" id="radio<?php echo $i; ?>" value="<?php echo $thrp_id[$i]; ?>"  >
                                                    <label></label>
                                                </div>
                                                <input type="hidden" name="thrp_name" id="thrp_name<?php echo $i; ?>" value="<?php echo $decrypted_thrp_name[$i]; ?>"  >
                                            </td>
                                            <td><?php echo $decrypted_thrp_name[$i]; ?></td>
                                            <td><?php echo $thrp_email[$i]; ?></td>                                                                                                        
                                        </tr>
                                        <?php
                                    }
                                }
                                if ($status_code != 0 && $status_code != 1) {
                                    switch ($status_code) {
                                        case 2: echo '<div class="alert alert-success" style="text-align:center;">';
                                            echo "<strong>Therapist has been re-assigned</strong>";
                                            echo '</div>';
                                            break;
                                        case 3: echo '<div class="alert alert-danger" style="text-align:center;">';
                                            echo "<strong>OOPS some error occure please refresh and try again</strong>";
                                            echo '</div>';
                                            break;
                                        case 4: echo '<div class="alert alert-danger" style="text-align:center;">';
                                            echo "<strong>Please choose one therapist from the list</strong>";
                                            echo '</div>';
                                            break;
                                        default: break;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <br/>
                        <div style="text-align: right;">
                            <input type="hidden" name="uid" id="uid" value ="<?php echo $uid; ?>">
                            <input type="hidden" name="tid" id="id" value ="<?php echo $tid; ?>">
                            <input type="submit" class="btn btn-success" style="font-family: 'Open Sans';background:#223C80 !important; border-color: #223C80 !important;" value="Assign" id="assign_submit" name="assign_submit">
                            <input type="reset" class="btn btn-custom" style="font-family: 'Open Sans';background:#b1b2b8 !important; border-color: #b1b2b8 !important;"  value="Clear">
                        </div>
                    </div><!-- end col -->
                </form>
            </div><!-- end row -->
        </div><!-- END wrapper -->
        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>
        <!-- App js -->
        <script src="../../assets/js/jquery.app.js"></script>
        <script src="../../assets/js/jquery.core.js"></script>
    </body>
</html>