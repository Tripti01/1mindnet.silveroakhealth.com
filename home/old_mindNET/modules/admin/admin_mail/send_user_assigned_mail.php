<?php
# This mail function is used for informing the therapist about new user assignment
function send_user_assigned_email($to,$to_name,$user_name){
	require_once 'aws_sdk/aws-autoloader.php';
	require_once 'ses_plugin/autoloader.php';
	require 'ses_plugin/mail_credentials.php';

	$m = new SimpleEmailServiceMessage();


	## ------------------------------- EMAIL PARAMETERS ---------------------------##
	$to = $to;
	$from = 'System <no-reply@stresscontrolonline.com>';
	$subject = "New user has been assigned";
	$html = '';
	$text = 'Dear '.$to_name.',
		
			 A new user ('.$user_name.') has been assigned to you.
			 
	Thanks & Regards,
	SilverOakHealth
			';
	## ----------------------------------------------------------------------------##

	$m->addTo($to);
	$m->setFrom($from);
	$m->setSubject($subject);
	$m->setMessageFromString($text,'');
	$m->setSubjectCharset('ISO-8859-1');

	try {
		$ses = new SimpleEmailService($key, $secret); // Sending the message
		$ses->sendEmail($m);
	} catch (Exception $ex) {
		die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
	}
}

?>