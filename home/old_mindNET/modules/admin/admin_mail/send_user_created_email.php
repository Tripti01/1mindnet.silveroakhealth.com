<?php

function send_admin_created_email($to, $to_name, $admin_name, $admin_email, $admin_password, $msg) {
    require_once 'aws_sdk/aws-autoloader.php';
    require_once 'ses_plugin/autoloader.php';
    require 'ses_plugin/mail_credentials.php';

    $m = new SimpleEmailServiceMessage();


    ## ------------------------------- EMAIL PARAMETERS ---------------------------##
    $to = $to;
    $from = 'System <no-reply@stresscontrolonline.com>';
    $subject = "New stresscontrol therapist/coach created";
    $html = '';
    $text = 'Dear ' . $to_name . ',
		
			 As requested, ' . $msg . '
			 Here are the details:
			 Admin name: ' . $admin_name . '
			 Email :' . $admin_email . '
			 Password :' . $admin_password . '
			 
	Thanks & Regards,
	SilverOakHealth
			';
    ## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    $m->setSubject($subject);
    $m->setMessageFromString($text, '');
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
    }
}

function send_thrp_created_email($to, $to_name, $thrp_email, $thrp_password) {
    require_once 'aws_sdk/aws-autoloader.php';
    require_once 'ses_plugin/autoloader.php';
    require 'ses_plugin/mail_credentials.php';

    $m = new SimpleEmailServiceMessage();


    ## ------------------------------- EMAIL PARAMETERS ---------------------------##
    $to = $to;
    $from = 'System <no-reply@stresscontrolonline.com>';
    $subject = "New stresscontrol therapist/coach created";
    $html = '';
    $text = 'Dear ' . $to . ',
		
			 As requested, a therapist account has been created.
			 Here are the details:
			 User name :' . $thrp_email . '
			 Password :' . $thrp_password . '
			 
	Thanks & Regards,
	SilverOakHealth
			';
    ## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    $m->setSubject($subject);
    $m->setMessageFromString($text, '');
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
    }
}

function send_clinician_created_email($to, $to_name, $admn_email, $admn_password) {
    require_once 'aws_sdk/aws-autoloader.php';
    require_once 'ses_plugin/autoloader.php';
    require 'ses_plugin/mail_credentials.php';

    $m = new SimpleEmailServiceMessage();


    ## ------------------------------- EMAIL PARAMETERS ---------------------------##
    $to = $to;
    $from = 'System <no-reply@stresscontrolonline.com>';
    $subject = "New stresscontrol clinician created";
    $html = '';
    $text = 'Dear ' . $to_name . ',
		
			 As requested, a clinician account has been created.
			 Here are the details:
			 User name :' . $admn_email . '
			 Password :' . $admn_password . '
			 
	Thanks & Regards,
	SilverOakHealth
			';
    ## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    $m->setSubject($subject);
    $m->setMessageFromString($text, '');
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
    }
}

function send_corporate_created_email($to, $to_name, $admn_email, $admn_password) {
    require_once 'aws_sdk/aws-autoloader.php';
    require_once 'ses_plugin/autoloader.php';
    require 'ses_plugin/mail_credentials.php';

    $m = new SimpleEmailServiceMessage();


    ## ------------------------------- EMAIL PARAMETERS ---------------------------##
    $to = $to;
    $from = 'System <no-reply@stresscontrolonline.com>';
    $subject = "New stresscontrol corporate account created";
    $html = '';
    $text = 'Dear ' . $to_name . ',
		
			 As requested, a corporate account has been created.
			 Here are the details:
			 User name :' . $admn_email . '
			 Password :' . $admn_password . '
			 
	Thanks & Regards,
	SilverOakHealth
			';
    ## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    $m->setSubject($subject);
    $m->setMessageFromString($text, '');
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
    }
}

?>