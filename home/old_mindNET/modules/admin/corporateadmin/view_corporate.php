<?php
/*
  1.When the view button is clicked in the "view_corporates.php" it lands in here.
  2.Check for the corp_id passed there and for particluar corporate the information is displayed
  3.CORPORATE DETAIL-can be changed on click of edit button
  4.CORPORATE UPLOAD LOGO-Here they have to upload the logo.
  4.CORPORATE LICENCE-you can view the selected option which was choosen while creating corporate.
  5.DEPARTMENT DETAILS-can be edited,deleted and an extra domain can also be addded for the same corp_id.
  6.RESET PASSWORD-password can be reset.
 */

#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include '../../../if_loggedin.php';
include '../check_prvg.php';
include 'functions/verify_token.php';
include 'functions/crypto_funtions.php';


#to fetch corp_id from url
$corp_id = $_REQUEST['corp_id'];

## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("ADMIN_CORPORATE");
if (!$if_allowed_to_view_this_page) {
    header("Location: index.php");
    exit();
}


#once the corporate details are edited to make this tab active
if (isset($_REQUEST['info_code'])) {
    $info_code = $_REQUEST['info_code'];
} else {
    $info_code = 0;
}


#once the password is reset to make reset password tab active
if (isset($_REQUEST['password_status'])) {
    $password_status = $_REQUEST['password_status'];
} else {
    $password_status = 0;
}

#once we delete deptartment details to make department details tab active
if (isset($_REQUEST['deptdel_code'])) {
    $deptdel_code = $_REQUEST['deptdel_code'];
} else {
    $deptdel_code = 0;
}


#once we edit deptartment details to make  department details tab active
if (isset($_REQUEST['deptedit_code'])) {
    $deptedit_code = $_REQUEST['deptedit_code'];
} else {
    $deptedit_code = 0;
}

##once we add deptartment details to make department details tab active
if (isset($_REQUEST['deptadd_code'])) {
    $deptadd_code = $_REQUEST['deptadd_code'];
} else {
    $deptadd_code = 0;
}

#to get all department details.
$i = 0;
#start database connection
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

#to fetch the name and email from corp_login
$stmt01 = $dbh->prepare("SELECT name,email,corp_id FROM corp_login WHERE corp_id=? LIMIT 1");
$stmt01->execute(array($corp_id));

if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    $name = $row01['name'];
    $decrypted_corp_name = decrypt($name, $encryption_key); //to fetch name and decrypt
    $email = $row01['email']; //to fetch email
    $corp_id = $row01['corp_id']; //to fetch corp_id
    //
    #to fetch address from corp_profile
    $stmt02 = $dbh->prepare("SELECT address FROM corp_profile WHERE corp_id=?");
    $stmt02->execute(array($corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[VW-CORP-01].");

    $row02 = $stmt02->fetch();
    $address = $row02['address']; //to fetch address
    #to fetch dept. name ,role name and domain name from corp_dept, corp_role and corp_domain respectively.
    $stmt03 = $dbh->prepare("SELECT dept_name FROM corp_dept where corp_id=?");
    $stmt03->execute(array($corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[VW-CORP-02].");

    if ($stmt03->rowCount() != 0) {
        while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
            $dept_list[$i] = $row03['dept_name']; //to fetch dept name.
            $i++;
        }
        $dept_code = 01; //all done sucessfully
    } else {
        $dept_code = 02; //unsucessfull
    }

    function get_corp_profile_pic($corp_id) {
        include 'soh-config.php';

        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        $stmt13 = $dbh->prepare("SELECT corp_logo FROM corp_profile WHERE corp_id=? LIMIT 1");
        $stmt13->execute(array($corp_id));
        if ($stmt13->rowCount() != 0) {
            $result13 = $stmt13->fetch();
            $profile_pic = $result13['corp_logo'];
            if ($profile_pic != '') {
                $profile_pics = $profile_pic;
            } else {
                $profile_pics = 'default_user.png';
            }
        }
        echo $profile_pics;
    }

    #to get all the role details
    $m = 0;

    $stmt04 = $dbh->prepare("SELECT role_name FROM corp_role where corp_id=? ");
    $stmt04->execute(array($corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[VW-CORP-03].");

    if ($stmt04->rowCount() != 0) {
        while ($row04 = $stmt04->fetch(PDO::FETCH_ASSOC)) {
            $role_list[$m] = $row04['role_name']; //to fetch role name.
            $m++;
        }
        $role_code = 01; //all done successfully
    } else {
        $role_code = 02; // unsucessfull
    }

    #to get all the domain details
    $n = 0;

    $stmt05 = $dbh->prepare("SELECT domain FROM corp_domain where corp_id=? ");
    $stmt05->execute(array($corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[VW-CORP-04].");

    if ($stmt05->rowCount() != 0) {
        while ($row05 = $stmt05->fetch(PDO::FETCH_ASSOC)) {
            $domain_list[$n] = $row05['domain']; //to fetch domain name.
            //echo $domain_list[$i];
            $n++;
        }
        $domain_code = 01; //all done succesfull
    } else {
        $domain_code = 02; //unsuccessfull
    }

    //to fetch the registeration type
    $stmt06 = $dbh->prepare("SELECT type FROM corp_type WHERE corp_id=?");
    $stmt06->execute(array($corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[VW-CORP-06].");

    if ($stmt06->rowCount() != 0) {
        $row06 = $stmt06->fetch();
        $reg_type = $row06['type']; //to fetch the registered users
        if ($reg_type == 'CORP') {
            $stmt07 = $dbh->prepare("SELECT users, start_date, end_date FROM corp_license WHERE corp_id=?");
            $stmt07->execute(array($corp_id));
            if ($stmt07->rowCount() != 0) {
                $row07 = $stmt07->fetch();
                $no_of_users = $row07['users'];
				$start_date = date('d-m-Y', strtotime($row07['start_date']));
				$end_date = date('d-m-Y', strtotime($row07['end_date']));
                $amount = 0;
            } else {
                $no_of_users = 0;
				$start_date = '';
				$end_date = '';
                $amount = 0;
            }
        } elseif ($reg_type == 'EMPL') {
            $stmt08 = $dbh->prepare("SELECT amount FROM corp_license2 WHERE corp_id=?");
            $stmt08->execute(array($corp_id));
            if ($stmt08->rowCount() != 0) {
                $row08 = $stmt08->fetch();
                $amount = $row08['amount'];
                $no_of_users = 0;
            } else {
                $no_of_users = 0;
                $amount = 0;
            }
        }
		
		 $stmt08 = $dbh->prepare("SELECT l.vertical_name, l.vertical_id FROM corp_vertical v, corp_vertical_list l WHERE l.vertical_id = v.vertical_id and v.corp_id=?");
         $stmt08->execute(array($corp_id));
         if ($stmt08->rowCount() != 0) {
				$row08 = $stmt08->fetch();
				$vertical_id = $row08['vertical_id'];
				$vertical_name = $row08['vertical_name'];
	     }else{
			 $vertical_name = '';
		 }
    }

    //to get the count of active users
    $stmt09 = $dbh->prepare("SELECT user_count FROM corp_users_count WHERE corp_id=?");
    $stmt09->execute(array($corp_id))or die("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[VW-CORP-06].");

    if ($stmt09->rowCount() != 0) {
        $row09 = $stmt09->fetch();
        $reg_users = $row09['user_count']; //to fetch the registered users.
    } else {
        $reg_users = '0'; //all rows selected
    }
}
#updating the personal info.
if (isset($_REQUEST['save_personal_detail'])) {

    //checking if all the values are set
    if (isset($_REQUEST['name']) && $_REQUEST['name'] !== '' && isset($_REQUEST['email']) && $_REQUEST['email'] !== '' && isset($_REQUEST['address']) && $_REQUEST['address'] !== '') {


        #to get the details
        $corp_id = $_REQUEST['corp_id'];
        $name = $_REQUEST['name'];
        $encrypted_name = encrypt($name, $encryption_key);
        $email = $_REQUEST['email'];
        $address = $_REQUEST['address'];

        #database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //updating name and email for corporate
        $stmt101 = $dbh->prepare("UPDATE corp_login SET name=?, email=? WHERE corp_id=? LIMIT 1");
        $stmt101->execute(array($encrypted_name, $email, $corp_id));

        //updating the address for corporate
        $stmt102 = $dbh->prepare("UPDATE corp_profile SET address=? WHERE corp_id=? LIMIT 1");
        $stmt102->execute(array($address, $corp_id));
        $info_code = 01; //all done succesfully
        //reload the page pass the corp_id and the info_code too.
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&info_code=' . $info_code);
    } else {
        $info_code = 02; //some empty fields were submitted
    }
}


#editing the department details.
if (isset($_REQUEST['edit_dept'])) {

    //checking if the values are set
    if (isset($_REQUEST['dept']) && $_REQUEST['dept'] !== '') {

        //include file
        include 'soh-config.php';

        //to get the details
        $corp_id = $_REQUEST['corp_id'];
        $dept = $_REQUEST['dept'];
        $old = $_REQUEST['dept_list'];
        //echo $old;
        #database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //updating dept. details
        $stmt201 = $dbh->prepare("UPDATE corp_dept SET dept_name=? WHERE corp_id=? AND dept_name= ? LIMIT 1");
        $stmt201->execute(array($dept, $corp_id, $old));
        $deptedit_code = 01; //all done successfully
        //to reload page ans pass corp_id in the Url.
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&deptedit_code=' . $deptedit_code);
    } else {
        $deptedit_code = 02; //submitted empty field
    }
}



#editing role details
if (isset($_REQUEST['edit_role'])) {

//to check if the values are set
    if (isset($_REQUEST['role']) && $_REQUEST['role'] !== '') {

        //to get the details
        $corp_id = $_REQUEST['corp_id'];
        $role = $_REQUEST['role'];
        $old = $_REQUEST['role_list'];

        //database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //updating the role details
        $stmt301 = $dbh->prepare("UPDATE corp_role SET role_name=? WHERE corp_id=? AND role_name=? LIMIT 1");
        $stmt301->execute(array($role, $corp_id, $old));
        $deptedit_code = 01; //all done successfully
        //to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&deptedit_code=' . $deptedit_code);
    } else {
        $deptedit_code = 02; // submitted empty field
    }
}

#editing domain details
if (isset($_REQUEST['edit_dom'])) {

    //to check if the values are set
    if (isset($_REQUEST['domain']) && $_REQUEST['domain'] !== '') {


        //to get details
        $corp_id = $_REQUEST['corp_id'];
        $domain = $_REQUEST['domain'];
        $old = $_REQUEST['domain_list'];

        #database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //updating domain details
        $stmt401 = $dbh->prepare("UPDATE corp_domain SET domain=? WHERE corp_id=? AND domain=? LIMIT 1");
        $stmt401->execute(array($domain, $corp_id, $old));
        $deptedit_code = 01; //all done successfully
        //to reload page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&deptedit_code=' . $deptedit_code);
    } else {
        $deptedit_code = 02; //empty field submitted
    }
}

#to reset password
if (isset($_REQUEST['change_pwd'])) {


    if (isset($_REQUEST['password']) && isset($_REQUEST['password_again'])) {

        //to fetch details from form
        $corp_id = $_REQUEST['corp_id'];
        $newpassword = $_REQUEST['password'];
        $confirm_password = $_REQUEST['password_again'];

        //hash password
        $hash_options = array('cost' => 11);
        $hashed_password = password_hash($newpassword, PASSWORD_DEFAULT, $hash_options);

        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //hashed password is updated to the database.
        $stmt501 = $dbh->prepare("UPDATE corp_login SET password=? WHERE corp_id=? LIMIT 1");
        $stmt501->execute(array($hashed_password, $corp_id));
        $password_status = 1; //all done successfully
        //to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id);
    } else {
        $password_status = 2; //some empty fields were submitted
    }
}

#to update start_date
if (isset($_REQUEST['edit_startdate'])) {


    if (isset($_REQUEST['start_date'])) {

        //to fetch details from form
        $corp_id = $_REQUEST['corp_id'];
        $start_date = date("Y-m-d", strtotime($_REQUEST['start_date']));;
		
        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //hashed password is updated to the database.
        $stmt501 = $dbh->prepare("UPDATE corp_license SET start_date = ? WHERE corp_id=? LIMIT 1");
        $stmt501->execute(array($start_date, $corp_id));
        $start_date_status = 1; //all done successfully
        
		//to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id);
    } else {
        $start_date = 2; //some empty fields were submitted
    }
}

#to update end_date
if (isset($_REQUEST['edit_enddate'])) {


    if (isset($_REQUEST['end_date'])) {

        //to fetch details from form
        $corp_id = $_REQUEST['corp_id'];
        $end_date = date("Y-m-d", strtotime($_REQUEST['end_date']));
		
        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //hashed password is updated to the database.
        $stmt501 = $dbh->prepare("UPDATE corp_license SET end_date =? WHERE corp_id=? LIMIT 1");
        $stmt501->execute(array($end_date, $corp_id));
        $end_date_status = 1; //all done successfully
        
		//to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id);
    } else {
        $end_date_status = 2; //some empty fields were submitted
    }
}

#to update vertical
if (isset($_REQUEST['edit_vertical'])) {
    if (isset($_REQUEST['vertical'])) {

        //to fetch details from form
        $corp_id = $_REQUEST['corp_id'];
        $vertical_id = $_REQUEST['vertical'];
		
        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //hashed password is updated to the database.
        $stmt501 = $dbh->prepare("UPDATE corp_vertical SET vertical_id =? WHERE corp_id=? LIMIT 1");
        $stmt501->execute(array($vertical_id, $corp_id));
        $vertical_id_status = 1; //all done successfully
        
		//to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id);
    } else {
        $vertical_id_status = 2; //some empty fields were submitted
    }
}

#delete the dept. details
if (isset($_REQUEST['del_dep'])) {


    if (isset($_REQUEST['corp_id']) && isset($_REQUEST['corp_id']) !== '') {

        #fetch the values from the form
        $dep_id = $_REQUEST['corp_id'];
        //echo $corp_id;
        $dept = $_REQUEST['old_value'];

        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");


        #delete the values from the database
        $stmt601 = $dbh->prepare("DELETE FROM corp_dept WHERE corp_id = ? AND dept_name=? LIMIT 1");
        $stmt601->execute(array($dep_id, $dept));

        $deptdel_code = 1; //all done successfully
        //to reload the page
        header("Location:view_corporate.php?corp_id=" . $dep_id . '&deptdel_code=' . $deptdel_code);
    } else {

        $deptdel_code = 2; //some empty fields were submitted
    }
}

#delete the role deatails
if (isset($_REQUEST['delete_role'])) {


    if (isset($_REQUEST['corp_id']) && isset($_REQUEST['corp_id']) !== '') {

        #fetch the values from the form
        $role_id = $_REQUEST['corp_id'];
        //echo $corp_id;
        $role = $_REQUEST['old_value'];

        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");


        #delete the values from the database
        $stmt602 = $dbh->prepare("DELETE FROM corp_role WHERE corp_id = ? AND role_name=? LIMIT 1");
        $stmt602->execute(array($role_id, $role));

        $deptdel_code = 1; //all done successfully
        //to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $role_id . '&deptdel_code=' . $deptdel_code);
    } else {
        $deptdel_code = 2; //some empty fields were submitted
    }
}

#delete the domain deatails
if (isset($_REQUEST['delete_domain'])) {

    echo $_REQUEST['old_value'];
    if (isset($_REQUEST['corp_id']) && isset($_REQUEST['corp_id']) !== '') {

        #fetch the values from the form
        $domain_id = $_REQUEST['corp_id'];
        //echo $corp_id;
        $domain = $_REQUEST['old_value'];

        #Start database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");


        #delete the values from the database
        $stmt603 = $dbh->prepare("DELETE FROM corp_domain WHERE corp_id = ? AND domain=? LIMIT 1");
        $stmt603->execute(array($domain_id, $domain));


        $deptdel_code = 1; //all done successfully
        //to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $domain_id . '&deptdel_code=' . $deptdel_code);
    } else {

        $deptdel_code = 2; //some empty fields were submitted
    }
}

#adding the department details.
if (isset($_REQUEST['add_dpt'])) {

    //checking if the values are set
    if (isset($_REQUEST['dept']) && $_REQUEST['dept'] !== '') {


        //to get the details
        $corp_id = $_REQUEST['corp_id'];
        $dept = $_REQUEST['dept'];

        #database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //inserting into dept.
        $stmt202 = $dbh->prepare("INSERT INTO corp_dept VALUES (?,?,?)");
        $stmt202->execute(array('', $corp_id, $dept));
        $deptadd_code = 01; //all done successfully
        //to reload page ans pass corp_id in the Url.
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&deptadd_code=' . $deptadd_code);
    } else {
        $deptadd_code = 02; //submitted empty field
    }
}

#adding role details
if (isset($_REQUEST['add_role'])) {

//to check if the values are set
    if (isset($_REQUEST['role']) && $_REQUEST['role'] !== '') {


        //to get the details
        $corp_id = $_REQUEST['corp_id'];
        $role = $_REQUEST['role'];

        //database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //inserting the role details
        $stmt301 = $dbh->prepare("INSERT INTO corp_role VALUES (?,?,?)");
        $stmt301->execute(array('', $corp_id, $role));
        $deptadd_code = 01; //all done successfully
        //to reload the page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&deptadd_code=' . $deptadd_code);
    } else {
        $deptadd_code = 02; // submitted empty field
    }
}

#adding domain details
if (isset($_REQUEST['add_domain'])) {

    //to check if the values are set
    if (isset($_REQUEST['domain']) && $_REQUEST['domain'] !== '') {


        //to get details
        $corp_id = $_REQUEST['corp_id'];
        $domain = $_REQUEST['domain'];

        #database connection
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        //inserting domain details
        $stmt401 = $dbh->prepare("INSERT INTO corp_domain VALUES(?,?)");
        $stmt401->execute(array($corp_id, $domain));
        $deptadd_code = 01; //all done successfully
        //to reload page
        header("Location:" . $host . "/modules/admin/corporateadmin/view_corporate.php?corp_id=" . $corp_id . '&deptadd_code=' . $deptadd_code);
    } else {
        $deptadd_code = 02; //empty field submitted
    }
}


# Select verticals from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_vertical_list ORDER BY vertical_name");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $vertical_ids[$i] = $row00['vertical_id'];
		$vertical_names[$i] = $row00['vertical_name'];
        $i++;
    }
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="../../../../assets/img/logo-fav.png">
        <!-- App title -->
        <title>View Corporate-Silver Oak Health</title>
        <!--Plugins css-->
        <link href = "../assets/css/components.css" rel = "stylesheet" type = "text/css" />        
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
         
        <style>

            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            body {

                font-weight: normal;
            }

            article {
                position: relative;
                padding: 0 5em;
                border: 1px solid #223C87;
                margin-bottom: 3em;
                border-radius: 5px;
            }

            article:hover {
                border: 1px solid #3AB149;
            }

            article:first-child {
                margin-top: 2em;
            }

            article .title {
                position: absolute;
                left: 30px;
                top: -25px;
                padding-left: 5px; padding-right: 5px;
                padding-bottom: 5px;
                font-family: 'open sans';
                background-color: #fff;
                font-size:15px;
            }

            @media (max-width: 960px) {

                article {
                    padding: 0 2em;
                }
            }
            .article{
                line-height:10px;
            }
        </style>
   </head>
   <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="header-title m-t-0 m-b-30" style="color:#35b8e0;font-size:18px"></h4>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-color panel-tabs panel-success"  style="margin-top: -30px;margin-bottom: 5px;">
                                        <div class="panel-heading" style="background-color:#2590e3;">
                                            <ul class="nav nav-pills pull-right">
                                                <li class="active" id="CPI">
                                                    <a href="#personal_info" id="CPI" data-toggle="tab" aria-expanded="true" style="font-family:'Open Sans';">Detail</a>
                                                </li>
                                                <li class="" id="UPL">
                                                    <a href="#upload_logo" id="UPL" data-toggle="tab" aria-expanded="true" style="font-family:'Open Sans';">Logo</a>
                                                </li>
                                                <li class="" id="CDD">
                                                    <a href="#dept_details" id="CDD" data-toggle="tab" aria-expanded="false" style="font-family:'Open Sans';">Domain Details</a>
                                                </li>
                                            </ul>
                                            <h3 class="panel-title" style="font-family:'Open Sans';padding:1.3%"></h3>
                                        </div>

                                        <!--CORPORATE DETAIL-->
                                        <div class="panel-body" style="background-color:#FCFCFB;">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="personal_info">
                                                    <!-- Display user information--> 
                                                    <form id="personal_info" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                                        <div class="portlet-body">
                                                            <div style="text-align:center; color:#2ABB2A;font-weight:bolder">
                                                                <!--Display message when personal info has changed.-->
                                                                <?php if ($info_code == 1) {// changes successfully              ?>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="note note-success note-bordered">
                                                                                <p>
                                                                                    <?php echo "Changes done successfully"; ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } else if ($info_code == 2) {//No changes                          ?>                                                
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="note note-danger note-bordered">
                                                                                <p>
                                                                                    <?php echo "No changes."; ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>																
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <!--Display information when update is completed-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <article>
                                                                    <h5 class="title" style="margin-top:3%;">
                                                                        Login Details
                                                                    </h5>
                                                                    <section>
                                                                        <div class="row" style="margin-top: 8%;" >
                                                                            <div style="text-align:left;font-family:'Open Sans'; "><label class="caption-subject caption-subject-color col-sm-3" style="margin-left:-10%;">HR Email </label>
                                                                                <span class="col-sm-7" style="; font-size:14px;"><?php echo $email; ?></span>
                                                                                <a href="" class="col-sm-3" data-toggle="modal" data-target="#prsnldet" style="margin-left:0%; font-size:12px;">&nbsp;&nbsp; Edit</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 8%;" >
                                                                            <div style="text-align:left;font-family:'Open Sans';"><label class="caption-subject caption-subject-color col-md-3"style="margin-left:-10%;">Password </label>
                                                                                <span class="col-sm-5" ><?php echo "*********" ?></span>
                                                                                <a href="" class="col-sm-3" data-toggle="modal" data-target="#reset_pswd" style="margin-left:18%;font-size:12px;">Reset</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 8%;" >
                                                                            <div style="text-align:left;font-family:'Open Sans';"><label class="caption-subject caption-subject-color col-md-3" style="margin-left:-10%;">Login URL </label>
                                                                                <a href="<?php echo $host_corp; ?>/corporate/" target="_blank"><?php echo $host_corp; ?>/corporate/</a>
                                                                            </div>
                                                                        </div>

                                                                    </section>
                                                                </article>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <article>
                                                                    <h5 class="title" style="margin-top:3%;">
                                                                        License Details
                                                                    </h5>
                                                                    <section>
                                                                        <div class="row" style="margin-top: 4%;" >
                                                                            <div style="text-align:left;font-family:'Open Sans';"><label class="caption-subject caption-subject-color col-md-6">License Type </label>
                                                                                <!--Display corporate information when update is completed-->
                                                                                <div> <?php
                                                                                    if ($reg_type == 'CORP') {
                                                                                        echo "Corporate Paid";
                                                                                    } else {
                                                                                        echo "Employee Paid";
                                                                                    }
                                                                                    ?>                                                                                               
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 4%;">
                                                                            <div style="text-align:left;font-family:'Open Sans';"><label class="caption-subject caption-subject-color col-md-6">No of Licenses </label>
                                                                                <?php echo $no_of_users; ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 4%;">
                                                                            <div style="text-align:left;font-family:'Open Sans';"><label class="caption-subject caption-subject-color col-md-6">Registered Users </label>
                                                                                <?php echo $reg_users; ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 4%;">
                                                                            <div style="text-align:left;font-family:'Open Sans';"><label class="caption-subject caption-subject-color col-md-6">Amount </label>
                                                                                <?php echo $amount; ?>
                                                                            </div>
                                                                        </div>
																		<div class="row" style="margin-top: 4%;">
                                                                           <div style="text-align:left;font-family:'Open Sans'; "><label class="caption-subject caption-subject-color col-sm-6">Start Date </label>
                                                                                <span class="col-sm-4" style="margin-left:-3%; font-size:14px;"><?php echo $start_date; ?></span>
                                                                                <a href="#" class="col-sm-2" data-toggle="modal" data-target="#start_date" style="margin-left:0%; font-size:12px;">&nbsp;&nbsp; Edit</a>
                                                                            </div>
                                                                        </div>
																		<div class="row" style="margin-top: 4%;">
                                                                           <div style="text-align:left;font-family:'Open Sans'; "><label class="caption-subject caption-subject-color col-sm-6">End Date </label>
                                                                                <span class="col-sm-4" style="margin-left:-3%; font-size:14px;"><?php echo $end_date; ?></span>
                                                                                <a href="#" class="col-sm-2" data-toggle="modal" data-target="#end_date" style="margin-left:0%; font-size:12px;">&nbsp;&nbsp; Edit</a>
                                                                            </div>
                                                                        </div>
																		<div class="row" style="margin-top: 4%;">
																			 <div style="text-align:left;font-family:'Open Sans'; "><label class="caption-subject caption-subject-color col-sm-6">Vertical </label>
                                                                                <span class="col-sm-4" style="margin-left:-3%; font-size:14px;"><?php echo $vertical_name; ?></span>
                                                                                <a href="#" class="col-sm-2" data-toggle="modal" data-target="#vertical" style="margin-left:0%; font-size:12px;">&nbsp;&nbsp; Edit</a>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </article>      
                                                            </div><!-- col-md-6-->
                                                        </div><!-- row-->

                                                        <article>
                                                            <h5 class="title" style="margin-top:1.4%;">
                                                                Domain Details
                                                            </h5>
                                                            <section style="margin-top: 2%;">
                                                                <?php
                                                                for ($g = 0; $g < $stmt05->rowCount(); $g++) {
                                                                    if (isset($domain_list[$g]) && $domain_list[$g] !== '') {
                                                                        ?>

                                                                        <ul>
                                                                            <li><?php echo $domain_list[$g]; ?></li>
                                                                        </ul>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </section>
                                                        </article>

                                                    </form>
                                                </div>

                                                <!--UPLOAD LOGO --> 
                                                <div class="tab-pane" id="upload_logo">
                                                    <form id="upload_logo_form" action="javascript:void(0);" method="POST" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-3" style="margin-top:2%;">Current Logo</label>
                                                                <img src="../assets/corp/profile_pic/<?php get_corp_profile_pic($corp_id); ?>" style="margin-left:1%;height:8%;width:8%;" >
                                                                <div  id="img-upload-msg-div">
                                                                    <p id="img-upload-msg">

                                                                    </p>
                                                                </div>
                                                                <label class="col-sm-3" style="margin-top:2%;">Upload Logo<font color="RED">*<br/>(Logo should be in square shape)</font></label>
                                                                <div class="col-sm-3" style="margin-top:2%;">
                                                                    <input type="file" name="upload" id="upload" value="" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="margin-top-10" style="margin-top:2%;">
                                                                <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>">
                                                                <input type="hidden" name="form_token" id="form_token" value="<?php echo form_verify("upload_logo_form"); ?>">
                                                                <input type="hidden" name="formname" id="formname" value="upload_logo_form">
                                                                <input type="submit" class="btn" id="upload" name="upload_submit" value="Upload" style="background-color:#223c80;  color:white;font-family:'Open Sans';">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="tab-pane" id="dept_details">
                                                    <!--To display domain details and also options to edit delete and add -->
                                                    <!--DOMAIN DETAILS-->
                                                    <div class="row" style="margin-top:2%;margin-top:0%;">
                                                        <div class="form-group">
                                                            <label  style="font-family:'Open Sans';margin-left:8%;">Domain</label>
                                                           
                                                            <div id='TextBoxesGroup' style="">
                                                                <div id="TextBoxDiv1" >
                                                                    <div class="panel-body" style="background-color:#FCFCFB; ">
                                                                        <table id="datatable-buttons" class="" style="width:75%;margin-top:-4%;">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style=""></th>
                                                                                    <th style=""></th>
                                                                                    <th style=""></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>


                                                                                <?php
                                                                                for ($l = 0; $l < $stmt05->rowCount(); $l++) {
                                                                                    if (isset($domain_list[$l]) && $domain_list[$l] !== '') {
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td class="col-md-1">&nbsp;</td>
                                                                                            <td class="col-md-2" style="padding-top:2%;padding-left:1%;"><?php echo $domain_list[$l]; ?></td>
                                                                                            <td class="col-md-1" style="padding-top:2.5%;"><a href="" style="font-size: 12px;margin-left:-1.8%;" data-toggle="modal" data-target="#editdomain_<?php echo $l; ?>"> Edit</a>
                                                                                                <span style="margin-left:2.5%;"><a href="" style="font-size: 12px;" data-toggle="modal" data-target="#deldomain_<?php echo $l; ?>"> Delete</a></span>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <!-- Modal used to delete the domain details -->
                                                                                    <div class="container">
                                                                                        <div class="modal fade" id="deldomain_<?php echo $l; ?>"  role="dialog">
                                                                                            <div class="modal-dialog" >
                                                                                                <div class="modal-content">
                                                                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  >
                                                                                                        <div class="modal-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                            <h4 class="modal-title">Delete Domain</h4>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <p> Are you sure you want to delete ?</p>
                                                                                                        </div>
                                                                                                        <div class="modal-footer">
                                                                                                            <input type="hidden" name="old_value" value="<?php echo $domain_list[$l]; ?>"/>
                                                                                                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                                                                                            <input type="submit" class="btn btn-info" name="delete_domain" value="Yes" style="background-color: #223c87 !important; border-color: #223c87 !important;"/>
                                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">No</button>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--Modal to edit domain details-->
                                                                                    <div class="container">
                                                                                        <!-- Modal to edit domain details -->
                                                                                        <div class="modal fade" id="editdomain_<?php echo $l; ?>"  role="dialog">
                                                                                            <div class="modal-dialog" >
                                                                                                <div class="modal-content">
                                                                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="form_domain">
                                                                                                        <div class="modal-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                            <h4 class="modal-title">Edit</h4>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <div class="row" style="margin-left:1.3%;"  >
                                                                                                                <div class="form-group">
                                                                                                                    <label class="col-sm-4" >Domain</label>
                                                                                                                    <div class="col-sm-8">
                                                                                                                        <input class="form-control placeholder-no-fix" id="domain" type="text" placeholder="Domain"  name="domain" value="<?php echo $domain_list[$l] ?>"  style='width:75%;margin-bottom:10px;'>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                        <div class="modal-footer">
                                                                                                            <input type="hidden" name="domain_list" value="<?php echo $domain_list[$l]; ?>"/>
                                                                                                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                                                                                            <input  type="submit" class="btn btn-info" name="edit_dom" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;"  >
                                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!--form-group-->
                                                    </div><!--row-->
                                                    <div class="row" style="margin-top:-2%;">
                                                        <div class="col-md-3">&nbsp;</div>
                                                        <div class="col-md-7" style="margin-left:-5.3%;"><a href="" data-toggle="modal" data-target="#domdet">Add New Domain</a></div>
                                                        <div class="col-md-2">&nbsp;</div>
                                                    </div>

                                                    <!--DEPARTMENT DETAILS-->
                                                    <div class="row" style="margin-top:2%;">
                                                        <div class="form-group">
                                                            <label  style="font-family:'Open Sans';margin-left:8%">Department</label>
                                                          
                                                            <div class="panel-body" style="background-color:#FCFCFB;">
                                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                                                    <table id="datatable-buttons" class="" style="width:75%;margin-top:-4%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style=""></th>
                                                                                <th  style=""></th>
                                                                                <th  style=""></th>
                                                                            </tr>

                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            for ($j = 0; $j < $stmt03->rowCount(); $j++) {
                                                                                if (isset($dept_list[$j]) && $dept_list[$j] !== '') {
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td class="col-md-1">&nbsp;</td>
                                                                                        <td class="col-md-2" style="padding-top: 2.5%;padding-left:1%;"><?php echo $dept_list[$j]; ?></td>
                                                                                        <td class="col-md-1" style="padding-top: 2.5%;"><a href="" style="font-size:12px;" data-toggle="modal" data-target="#edittdept_<?php echo $j; ?>"> Edit</a>
                                                                                            <span><a href="" style="padding-top: 2.5%;font-size:12px;margin-left:2%;" data-toggle="modal" data-target="#deldet_<?php echo $j; ?>"> Delete</a></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                <div class="container">
                                                                                    <!-- Modal used to delete the department details -->
                                                                                    <div class="modal fade" id="deldet_<?php echo $j; ?>"  role="dialog">
                                                                                        <div class="modal-dialog" >
                                                                                            <div class="modal-content">
                                                                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  >
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                        <h4 class="modal-title" >Delete Department</h4>
                                                                                                    </div>
                                                                                                    <div class="modal-body">
                                                                                                        <p> Are you sure you want to delete ?</p>
                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        <input type="hidden" name="old_value" value="<?php echo $dept_list[$j]; ?>"/>
                                                                                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                                                                                        <input type="submit" class="btn btn-info" name="del_dep" value="Yes" style="background-color: #223c87 !important; border-color: #223c87 !important;"/>
                                                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">No</button>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- MODAl for edit dept-->
                                                                                <!-- Modal to change dept details -->
                                                                                <div class="modal fade" id="edittdept_<?php echo $j; ?>"  role="dialog">
                                                                                    <div class="modal-dialog" >
                                                                                        <div class="modal-content">
                                                                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="form_dept">
                                                                                                <div class="modal-header">
                                                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                    <h4 class="modal-title">Edit Department</h4>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    <div class="row" style="margin-left:1.3%;"  >
                                                                                                        <div class="form-group">
                                                                                                            <label class="col-sm-4" style="font-family:'Open Sans';">Department</label>
                                                                                                            <div class="col-sm-8">
                                                                                                                <input class="form-control placeholder-no-fix" id="dept" type="text"  name="dept" value="<?php echo $dept_list[$j]; ?>" placeholder="Department"  style='width:75%;margin-bottom:10px;'>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="modal-footer">
                                                                                                    <input type="hidden" name="dept_list" value="<?php echo $dept_list[$j]; ?>"/>
                                                                                                    <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                                                                                    <input class="btn btn-info" type="submit" name="edit_dept" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;"  >
                                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top:-2%;">
                                                        <div class="col-md-3">&nbsp;</div>
                                                        <a href="" class="col-md-7" style="margin-left:-5.3%;" data-toggle="modal" data-target="#deptdet">Add New Department</a> 
                                                        <div class="col-md-2">&nbsp;</div>
                                                    </div>

                                                    <!--ROLE DETAILS-->
                                                    <div class="row" style="margin-top:2%;">
                                                        <div class="form-group">
                                                            <label  style="font-family:'Open Sans';margin-left:8%">Role</label>
                                                           
                                                            <div id='TextBoxesGroup' style="">
                                                                <div id="TextBoxDiv1" >
                                                                    <div class="panel-body" style="background-color:#FCFCFB;">
                                                                        <table id="datatable-buttons" class="" style="width:75%;margin-top:3%;margin-top:-4%;">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style=""></th>
                                                                                    <th style=""></th>
                                                                                    <th style=""></th>
                                                                                </tr>

                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                for ($k = 0; $k < $stmt04->rowCount(); $k++) {
                                                                                    if (isset($role_list[$k]) && $role_list[$k] !== '') {
                                                                                        ?>
                                                                                        <tr>
                                                                                            <td class="col-md-1">&nbsp;</td>
                                                                                            <td class="col-md-2" style=" padding-top: 2.5%;padding-left:1%;"><?php echo $role_list[$k]; ?></td>
                                                                                            <td class="col-md-1" style=" padding-top: 2.5%;"><a href="" style="font-size:12px;" data-toggle="modal" data-target="#editrole_<?php echo $k; ?>"> Edit</a>
                                                                                                <span style="padding-top: 2.5%;margin-left:2%;"><a href="" style="font-size:12px;" data-toggle="modal" data-target="#delrole_<?php echo $k; ?>"> Delete</a> 
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <div class="container">
                                                                                        <!-- Modal used to delete the role details -->
                                                                                        <div class="modal fade" id="delrole_<?php echo $k; ?>"  role="dialog">
                                                                                            <div class="modal-dialog" >
                                                                                                <div class="modal-content">
                                                                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  >
                                                                                                        <div class="modal-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                            <h4 class="modal-title">Delete Role</h4>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <p> Are you sure you want to delete ?</p>
                                                                                                        </div>
                                                                                                        <div class="modal-footer">
                                                                                                            <input type="hidden" name="old_value" value="<?php echo $role_list[$k]; ?>"/>
                                                                                                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                                                                                            <input type="submit" class="btn btn-info" name="delete_role" value="Yes" style="background-color: #223c87 !important; border-color: #223c87 !important;"/>
                                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">No</button>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <!--Modal to edit role -->

                                                                                    <div class="container">
                                                                                        <!-- Modal to change role details -->
                                                                                        <div class="modal fade" id="editrole_<?php echo $k; ?>"  role="dialog">
                                                                                            <div class="modal-dialog" >
                                                                                                <div class="modal-content">
                                                                                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="form_role">
                                                                                                        <div class="modal-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                                            <h4 class="modal-title">Edit Role</h4>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <div class="row" style="margin-left:1.3%;"  >
                                                                                                                <div class="form-group">
                                                                                                                    <label class="col-sm-4" style="font-family:'Open Sans';">Role</label>
                                                                                                                    <div class="col-sm-8">
                                                                                                                        <input class="form-control placeholder-no-fix" id="role" type="text" placeholder="Role"  name="role" value="<?php echo $role_list[$k] ?>"  style='width:75%;margin-bottom:10px;'>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="modal-footer">
                                                                                                            <input type="hidden" name="role_list" value="<?php echo $role_list[$k]; ?>"/>
                                                                                                            <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                                                                                            <input  type="submit" class="btn btn-info" name="edit_role"   value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top:-2%;">
                                                        <div class="col-md-3">&nbsp;</div>
                                                        <div class="col-md-7" style="margin-left:-5.3%;"> <a href=""  data-toggle="modal" data-target="#roledet">Add New Role</a></div> 
                                                        <div class="col-md-2"> &nbsp;</div>
                                                    </div>
                                                    <div class="form-actions">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- MODAL -->
                <!-- Modal to Reset Password -->
                <div class="container">
                    <div class="modal fade" id="reset_pswd"  role="dialog">
                        <div class="modal-dialog">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" name="form-control" id="change_pwd"  method="POST">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row" >
                                                    <div class="row" >
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-3" style="margin-top:1%;margin-left:3%;">New Password<font color="RED">*</font></label>
                                                                <div class="col-sm-8">
                                                                    <input type="password" name="password" id="password" class="form-control" placeholder="New Password" style='width:55%;margin-bottom:10px; '>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 " style=" margin-top:1%;margin-left:3%;">Confirm Password<font color="RED">*</font></label>
                                                                <div class="col-sm-8">
                                                                    <input type="password" name="password_again" id="password_again" class="form-control" placeholder="Confirm Password" style='width:55%;margin-bottom:10px;'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input type="submit" name="change_pwd"  value="Save" class="btn btn-info" style="background-color: #223c87 !important; border-color: #223c87 !important;"  >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- MODAL -->
                <div class="container">
                    <!-- Modal to edit corporate_details -->
                    <div class="modal fade" id="prsnldet"  role="dialog">
                        <div class="modal-dialog">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" name="form-control" id="personal_info_edit"  method="POST">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Details</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row" >
                                                    <div class="row" >
                                                        <div class="row" style="margin-left:1.3%;"  >
                                                            <div class="form-group">
                                                                <label class="col-sm-4" >Name<span style="color:red;">*</span></label>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control placeholder-no-fix"  type="text" name="name" value="<?php echo $decrypted_corp_name; ?>"   style='width:75%;margin-bottom:10px;'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-left:1.3%;">
                                                            <div class="form-group">
                                                                <label class="col-sm-4">Email<span style="color:red;">*</span></label>
                                                                <div class="col-sm-8">
                                                                    <input class="form-control placeholder-no-fix"  type="text" name="email" value="<?php echo $email; ?>"   style='width:75%;margin-bottom:10px;'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-left:1.3%;" >
                                                            <div class="form-group">
                                                                <label class="col-sm-4">Address<span style="color:red;">*</span></label>
                                                                <div class="col-sm-8">
                                                                    <textarea class="form-control placeholder-no-fix"  type="text"  name="address"  style='width:75%;margin-bottom:10px;'><?php echo $address; ?></textarea> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input type="submit" name="save_personal_detail"  value="Save" class="btn btn-info" style="background-color: #223c87 !important; border-color: #223c87 !important;"  >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <!-- Modal to change domain details -->
                    <div class="modal fade" id="domdet"  role="dialog">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="modal_domain" name="frmDomin" onsubmit="return frmValidate();">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Domain</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-left:1.3%;"  >
                                            <div class="form-group">
                                                <label class="col-sm-4" style="font-family:'Open Sans';">Domain<span style="color:red;">*</span></label>
                                                <div class="col-sm-8">
                                                    <input class="form-control placeholder-no-fix" id="domain" type="text" placeholder="Domain"  name="domain" value=""  style='width:75%;margin-bottom:10px;'>
                                                    <div id="err_name" class="error"></div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input  type="submit" class="btn btn-info" name="add_domain" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;"  >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal to add department details -->
                <div class="container">
                    <div class="modal fade" id="deptdet"  role="dialog">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="modal_dept">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add New Department</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-left:1.3%;"  >
                                            <div class="form-group">
                                                <label class="col-sm-4" style="font-family:'Open Sans';">Department<span style="color:red;">*</span></label>
                                                <div class="col-sm-8">
                                                    <input class="form-control placeholder-no-fix" id="dept" type="text"  name="dept" value="" placeholder="Department"  style='width:75%;margin-bottom:10px;'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input class="btn btn-info" type="submit" name="add_dpt" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;"  >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal to add role details -->
                <div class="container">
                    <div class="modal fade" id="roledet"  role="dialog">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="modal_role">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add New Role</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-left:1.3%;"  >
                                            <div class="form-group">
                                                <label class="col-sm-4" style="font-family:'Open Sans';">Role<span style="color:red;">*</span></label>
                                                <div class="col-sm-8">
                                                    <input class="form-control placeholder-no-fix" id="role" type="text" placeholder="Role"  name="role" value=""  style='width:75%;margin-bottom:10px;'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input  type="submit" name="add_role"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
				
				 <!-- Modal to edit vertical details -->
                <div class="container">
                    <div class="modal fade" id="vertical"  role="dialog">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="modal_vertical">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Vertical</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-left:1.3%;"  >
												<div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="vertical"  style="font-weight:normal;text-align:left;">Vertical <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <select id="vertical" name="vertical" title="" required class="form-control" style="margin-top:0px;font-size: 12px;">
                                                                <option value="-1" style="color:#808080ad;">   SELECT   </option> 
                                                               <?php
																for ($i = 0; $i < count($vertical_ids); $i++) { echo $vertical_id; ?>
																
																	<option value="<?php echo $vertical_ids[$i];?>" <?php echo $vertical_ids[$i]== $vertical_id ? "selected" : ""; ?>><?php echo $vertical_names[$i] ;?></option>';
																<?php }
																?>
                                                            </select>
                                                        </div>
                                                 </div> 
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input  type="submit" name="edit_vertical"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
				 <!-- Modal to edit date details -->
                <div class="container">
                    <div class="modal fade" id="start_date"  role="dialog">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="modal_startdate">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Start DAte</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-left:1.3%;"  >
											<div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="start_date" style="font-weight:normal;text-align:left;"> Start Date <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <div class="input-group" style="width:100%;">
                                                            <input type="text"  name="start_date" class="form-control" value="<?php echo $start_date;?>" placeholder="dd/mm/yyyy" id="datepicker-autoclose1" autocomplete="off">
                                                            <span class="input-group-addon bg-primary b-0 text-white" ><i class="ti-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input  type="submit" name="edit_startdate"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
				 <!-- Modal to edit end date details -->
                <div class="container">
                    <div class="modal fade" id="end_date"  role="dialog">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"  id="modal_enddate">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit End DAte</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-left:1.3%;"  >
											<div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="start_date" style="font-weight:normal;text-align:left;"> End Date <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <div class="input-group" style="width:100%;">
                                                            <input type="text"  name="end_date" class="form-control" value="<?php echo $end_date;?>" placeholder="dd/mm/yyyy" id="datepicker-autoclose" autocomplete="off">
                                                            <span class="input-group-addon bg-primary b-0 text-white" ><i class="ti-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="corp_id" value="<?php echo $corp_id; ?>"/>
                                        <input  type="submit" name="edit_enddate"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="margin-left:3%;">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </div>
    <!-- END wrapper -->
    <script>
        var resizefunc = [];
    </script>
    <!-- jQuery  -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/detect.js"></script>
    <script src="../assets/js/fastclick.js"></script>
    <script src="../assets/js/jquery.slimscroll.js"></script>
    <script src="../assets/js/jquery.blockUI.js"></script>
    <script src="../assets/js/waves.js"></script>
    <script src="../assets/js/jquery.nicescroll.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <script src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- App js -->
    <script src="../assets/js/jquery.core.js"></script>
    <script src="../assets/js/jquery.app.js"></script>

    <!-- Modal-Effect -->
    <script src="../assets/plugins/custombox/dist/custombox.min.js"></script>
    <script src="../assets/plugins/custombox/dist/legacy.min.js"></script>

    <!--File Input -->
    <script src="../assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css" type="text/css"></script>
    <script src="../assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
	<script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<script src="../assets/js/app.min.js"></script>


    <!--on editing department details to show department details tab active-->
    <script>
<?php if (($deptedit_code == 1) || ($deptedit_code == 2)) { ?>
            $(document).ready(function () {
                $("#CPI").removeClass("active");
                $("#personal_info").removeClass("active");
                $("#CDD").addClass("active")
                $("#dept_details").addClass("active");
            });
<?php } ?>
    </script>


    <!--on deletion of department details to show department details tab active -->
    <script>
<?php if (($deptdel_code == 1) || ($deptdel_code == 2)) { ?>
            $(document).ready(function () {
                $("#CPI").removeClass("active");
                $("#personal_info").removeClass("active");
                $("#CDD").addClass("active")
                $("#dept_details").addClass("active");
            });
<?php } ?>
    </script>
    <!--on deletion of department details to show department details tab active -->
    <script>
<?php if (($deptadd_code == 1) || ($deptadd_code == 2)) { ?>
            $(document).ready(function () {
                $("#CPI").removeClass("active");
                $("#personal_info").removeClass("active");
                $("#CDD").addClass("active")
                $("#dept_details").addClass("active");
            });
<?php } ?>
    </script>
    <script>
<?php
if (isset($_GET['status'])) {
    if (($_GET['status'] == 1)) {
        ?>
                $(document).ready(function () {
                    $("#CPI").removeClass("active");
                    $("#personal_info").removeClass("active");
                    $("#UPL").addClass("active")
                    $("#upload_logo").addClass("active in");
                });

        <?php
    }
}
?>
    </script>
    <script>
        // form submit update-profile pic-form
        $("form#upload_logo_form").submit(function (event) {
            var imgVal = $('#upload').val();
            if (imgVal == '')
            {
                $('#img-upload-msg-div').removeClass('note note-success note-bordered');
                $('#img-upload-msg-div').addClass('note note-danger note-bordered');
                $('#img-upload-msg').html('Please select a picture');

            } else {

                form_dom = $(this);

                //disable the default form submission
                event.preventDefault();
                //  grab all form data
                var formData = new FormData($(this)[0]);

                $("#btn-save", form_dom).val("updating...");
                $("#btn-save", form_dom).attr("disabled", "disabled");


                formData.append('formname', 'upload_logo_form');
                // retyped password matches
                $.ajax({
                    url: "ajax/updatelogo_ajax.php",
                    type: "post",
                    data: formData,
                    async: true,
                    processData: false,
                    cache: false,
                    contentType: false,
                    success: function (response) {
                        if (response == 1) {
                            $('#img-upload-msg-div').removeClass('note note-danger note-bordered');
                            $('#img-upload-msg-div').addClass('note note-success note-bordered');
                            $('#img-upload-msg').html('Logo is changed successfully');
                            window.top.location.href = "<?php echo $host . '/modules/admin/corporateadmin/view_corporate.php?corp_id=' . $corp_id; ?>&status=1";

                        } else {

                            $('#img-upload-msg-div').removeClass('note note-success note-bordered');
                            $('#img-upload-msg-div').addClass('note note-danger note-bordered');
                            if (response == 0) {
                                $('#img-upload-msg').html('File-size limit of 1MB exceeded!');
                            } else if (response == 2) {
                                $('#img-upload-msg').html('Error saving image on the server, try again!');
                            }
                        }
                        $("#btn-save", form_dom).val("Save changes");
                        $("#btn-save", form_dom).removeAttr("disabled");

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#btn-save", form_dom).val("Save changes");
                        $("#btn-save", form_dom).removeAttr("disabled");
                    }
                });
            }
        });
    </script>

    <!--Validation rules and messages for personal-info,department,role and domain  -->
    <script>
        jQuery(document).ready(function () {
            $("#personal_info_edit").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    address: {
                        required: true
                    }
                },
                messages: {
                    name: {
                        required: "Please enter corporate's name"
                    },
                    email: {
                        required: "Please enter corporate's email id",
                        email: "Please enter a valid email id"
                    },
                    address: {
                        required: "Please enter the full address of the corporate"
                    }
                }

            });
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            $("#modal_dept").validate({
                rules: {
                    dept: {
                        required: true,
                    }
                },
                messages: {
                    dept: {
                        required: "Please enter department's name"
                    }
                }

            });
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            $("#modal_role").validate({
                rules: {
                    role: {
                        required: true,
                    }
                },
                messages: {
                    role: {
                        required: "Please enter role name"
                    }
                }

            });
        });
    </script>

    <!--validation to reset password-->
    <script>
        jQuery(document).ready(function () {
            $("#change_pass_form").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 7
                    },
                    password_again: {
                        required: true,
                        equalTo: "#password"
                    }
                },
                messages: {
                    password: {
                        required: "Please enter the password"
                    },
                    password_again: {
                        required: "Please enter the password"
                    }
                }

            });
        });
    </script>
    <script type="text/javascript">
        function frmValidate()
        {
            var val = document.frmDomin;
            if (/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value) || /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}\.[a-zA-Z]{1,}$/.test(val.domain.value) ) {
            } else
            {
                $("#err_name").html("Invalid Domain");
                val.domain.focus();
                return false;
            }
        }
    </script>
	<script>
        // Date Picker
        jQuery('#datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
		jQuery('#datepicker-autoclose1').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('#datepicker-inline').datepicker();
        jQuery('#datepicker-multiple-date').datepicker({
            format: "mm/dd/yyyy",
            clearBtn: true,
            multidate: true,
            multidateSeparator: ","
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });

    </script>
</body>
</html>