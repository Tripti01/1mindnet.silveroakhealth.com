<?php
/*
  1.In the sidebar when view_corporates is clicked we land onto this page.
  2.All the corporates details can be seen here, there id,name and registration type are displayed with a view button in last column
  3.Clicking onto the view button takes you to the individual corporate information
 */

#checking the login details
include '../../../if_loggedin.php';
include '../check_prvg.php';

## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("ADMIN_CORPORATE");
if (!$if_allowed_to_view_this_page) {
    exit();
}

#file inclusion for various function happening in the ui
include 'soh-config.php';
include 'mindnet-host.php';
include 'functions/crypto_funtions.php';

# Now check if from date and to date are set.
if (isset($_REQUEST['daterange'])) {
    $daterange = explode(" - ", $_REQUEST['daterange']);
    $from_date = date("Y-m-d", strtotime($daterange[0]));
    $to_date = date("Y-m-d", strtotime($daterange[1]));
} else {
    #From date and to date are not set. We need to set it to default.
    $to_date = date('Y-m-d H:i:s');
    $from_date = date("Y-m-d H:i:s", strtotime($to_date . "-30 day"));
}


# Date to display
$ui_from_date = date('d-M-y', strtotime($from_date));
$ui_to_date = date('d-M-y', strtotime($to_date));


# Status codes for error and success messages
$create_status_code = 0;
$edit_status_code = 0;

if (isset($_REQUEST['create_status_code'])) {
    $create_status_code = $_REQUEST['create_status_code'];
} else if (isset($_REQUEST['edit_status_code'])) {
    $edit_status_code = $_REQUEST['edit_status_code'];
} else {
    $create_status_code = 0;
    $edit_status_code = 0;
}

# Start database transactions
$i = 0;
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Select categories from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_login WHERE 1 ORDER BY name ASC");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row00['corp_id'];
        $corp_name[$i] = decrypt($row00['name'], $encryption_key);
        $i++;
    }
}

# Select activities from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_activity WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $activity_type[$i] = $row00['activity_type'];
        $activity_name[$i] = $row00['activity_name'];
        $i++;
    }
}

# Select activities from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_activity_track1,corp_activity_track2 WHERE corp_activity_track1.activity_id = corp_activity_track2.activity_id AND DATE(held_on)>=? AND DATE(held_on)<=? ORDER BY corp_activity_track1.held_on DESC ");
$stmt00->execute(array($from_date, $to_date));
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $activity_id[$i] = $row00['activity_id'];
        $ac_corp_id[$i] = $row00['corp_id'];
        $held_on[$i] = date("d-M-Y", strtotime($row00['held_on']));
        $activity_type[$i] = $row00['activity_type'];
        $country[$i] = $row00['country'];
        $state[$i] = $row00['state'];
        $city[$i] = $row00['city'];
        $additional_details[$i] = $row00['add_details'];
        $facilitator_name[$i] = $row00['facilitator_name'];
        $ac_remarks[$i] = $row00['remarks'];
        $location[$i] = $city[$i];

        /* 	if( $row00['status'] == 'C'){
          $completed_status[$i] = 'Completed';
          }else if($row00['status'] == 'S'){
          $completed_status[$i] = 'Scheduled';
          }
         */

        $no_of_attendee[$i] = $row00['no_of_attendee'];

        $stmt01 = $dbh->prepare("SELECT activity_name FROM corp_activity WHERE activity_type=?");
        $stmt01->execute(array($activity_type[$i]));
        if ($stmt01->rowCount() != 0) {
            $row01 = $stmt01->fetch();
            $ac_name[$i] = $row01['activity_name'];
        }

        $stmt02 = $dbh->prepare("SELECT name FROM corp_login WHERE corp_id=?");
        $stmt02->execute(array($ac_corp_id[$i]));
        if ($stmt02->rowCount() != 0) {
            $row02 = $stmt02->fetch();
            $ac_corp_name[$i] = decrypt($row02['name'], $encryption_key);
        }

        $i++;
    }
}

if (isset($_REQUEST['submit'])) {
    if (isset($_REQUEST['corp_id'])) {
        $corp_id = $_REQUEST['corp_id'];

        if (isset($_REQUEST['month']) && $_REQUEST['month'] != "") {
            $date_timestamp = strtotime($_REQUEST['month']);

            $month = date("F", $date_timestamp);
            $year = date("Y", $date_timestamp);
            header("Location:view_activity_report.php?period=" . $month . "&year=" . $year . "&corp_id=" . $corp_id);
        }
        if (isset($_REQUEST['from_date']) && isset($_REQUEST['to_date']) && $_REQUEST['from_date'] != "" && $_REQUEST['to_date'] != "") {
            $from_date = $_REQUEST['from_date'];
            $to_date = $_REQUEST['to_date'];
            header("Location:view_activity_report.php?from_date=" . $from_date . "&to_date=" . $to_date . "&corp_id=" . $corp_id);
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

        <!-- App title -->
        <title>Activity Tracking - Silver Oak Health</title>

        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

        <script src="../assets/js/jquery.min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .row{
                padding : 8px;
            }
            .modal{
                margin-top:20px;
            }

        </style>
    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
            <?php include '../top_navbar.php'; ?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <!-- Hiding this button as its not needed.
                    <button class="btn btn-primary" style="margin-right:10px;float:right;background-color:#223c87" data-toggle="modal" data-target="#view_report" onclick="add()">
                            View Report
                    </button> 
                    !-->
                    <button class="btn btn-primary" style="margin-right:10px;float:right;background-color:#223c87" data-toggle="modal" data-target="#activity_edit" onclick="add()" >
                        Create Activity
                    </button>
                    <BR/><BR/>
                    <div class="container-fluid">						
                        <div class="row">							
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-6">                        
                                        <span class="link-03" style="margin:20px 0px 0px 20px">Showing from</span> <b><?php echo $ui_from_date; ?></b> <span class="link-03">to </span> <b><?php echo $ui_to_date; ?></b>
                                    </div>
                                    <div  class="col-md-6">
                                        <div class="input-group mg-b-10" style="float:right">
                                            <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                                <div class="row ">
                                                    <div class="col-lg-11" style="padding-right:5px">
                                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                            <i class="fa fa-calendar"></i>&nbsp;
                                                            <span id="get_date"></span> <i class="fa fa-caret-down"></i>
                                                        </div>										
                                                    </div>
                                                    <div class="col-lg-1" style="padding-left:0px">
                                                        <input type="hidden" name="daterange" id="daterange" />
                                                        <button class="btn btn-primary" onclick="get_daterange()" style="font-size:10px;background-color:#223c87;line-height:18px !important;"> Show </button>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                                    <div class="panel-body">
                                        <?php
                                        if ($create_status_code == 1) {
                                            echo '<div style="color:#3AB149;text-align:center;">Activity Created Successfully!</div>';
                                        } else if ($edit_status_code == 2) {
                                            echo '<div style="color:#3AB149;text-align:center;">Activity Updated Successfully!</div>';
                                        } else if ($edit_status_code == 4) {
                                            echo '<div style="color:#3AB149;text-align:center;">Activity Deleted Successfully!</div>';
                                        } else if ($create_status_code == 3 || $edit_status_code == 3) {
                                            echo '<div style="color:red;text-align:center;">Some Error Occurred</div>';
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                        <table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="color:#188ae2;">Sno</th>
                                                    <th style="color:#188ae2;">Corporate Name</th>
                                                    <th style="color:#188ae2;">Activity</th>
                                                    <th style="color:#188ae2;">Date</th>
                                                    <th style="color:#188ae2;">Location</th>
                                                    <th style="color:#188ae2;">No.of Attende</th>
                                                    <th style="color:#188ae2;">Event Name</th>
                                                    <th style="color:#188ae2;">Facilitator Name</th>
                                                    <th style="color:#188ae2;">Remarks</th>
                                                    <th style="color:#188ae2;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>												
                                                <?php
                                                if (isset($ac_corp_id)) {
                                                    for ($k = 0; $k < count($ac_corp_id); $k++) {
                                                        ?>
                                                        <!--if status_code==1 displays  corporator id name and register type-->
                                                        <tr>
                                                            <td><?php echo ($k + 1); ?></td>
                                                            <td><?php echo $ac_corp_name[$k]; ?></td>
                                                            <td><?php echo $ac_name[$k]; ?></td>
                                                            <td><?php echo $held_on[$k]; ?></td>
                                                            <td><?php echo $location[$k]; ?></td>
                                                            <td><?php echo $no_of_attendee[$k]; ?></td>
                                                            <td><?php echo $additional_details[$k]; ?> </td>
                                                            <td><?php echo $facilitator_name[$k]; ?> </td>
                                                            <td>
                                                                <?php
                                                                if ($ac_remarks[$k] != '') {
                                                                    echo substr($ac_remarks[$k], 0, 25);
                                                                    echo '...<span style="color:#188ae2;cursor:pointer" data-toggle="modal" data-target="#show_remark_' . $k . '">more</span>';
                                                                }
                                                                ?>
                                                                <div class="container">
                                                                    <div class="modal fade" id="show_remark_<?php echo $k; ?>"  role="dialog">
                                                                        <div class="modal-dialog" style="width:600px;">
                                                                            <div class="modal-content">
                                                                                <button type="button" class="close" data-dismiss="modal" style="background-color:#fff;margin-top:-20px;margin-bottom:10px;">&times;</button>
                                                                                <textarea class="remarks form-control" style="width:100%;height:<?php echo strlen($ac_remarks[$k]) / 2.5; ?>px"> <?php echo $ac_remarks[$k]; ?> </textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                            <td onclick="edit('<?php echo $activity_id[$k]; ?>')" data-toggle="modal" data-target="#activity_edit" title="edit"><i class="fa fa-edit"></i></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>

                    <div class="container">
                        <div class="modal fade" id="activity_edit"  role="dialog">
                            <div class="modal-dialog" style="width:900px;">
                                <div class="modal-content">
                                    <iframe src="" scrolling="no" id="f_edit" width="100%" height="500px" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="modal fade" id="view_report"  role="dialog">
                            <div class="modal-dialog" style="width:500px;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" onclick="toplocation()">&times;</button>
                                        <h4 class="modal-title">View Activity Report</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="activity_tracking.php" method="POST">
                                            <article>
                                                <section>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <table width="100%" align="center">
                                                                <tr>
                                                                    <td width="45%"><div class="ewap_cmp_name"><b>Select Corporate Name</b></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <select id="corp-list-id" class="corp-list form-control"  name="corp_id" style="width:390px;">
                                                                            <option value="0" disabled="" selected="">SELECT</option>
                                                                            <?php
                                                                            for ($i = 0; $i < count($corp_id); $i++) {
                                                                                echo '<option value="' . $corp_id[$i] . '">' . $corp_name[$i] . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%" style="padding-top:20px;"><div class="ewap_cmp_name"><b>Select Date Format</b></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">
                                                                        <div style="margin:10px">
                                                                            <div class="radio radio-info" id="view_m">
                                                                                <input type="radio" id="inlineRadio4" value="ALL" name="radioInline">
                                                                                <label for="inlineRadio4"> Month Wise </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">
                                                                        <div style="margin:10px;">
                                                                            <div class="radio radio-info" id="view_d">
                                                                                <input type="radio" id="inlineRadio5" value="ALL" name="radioInline">
                                                                                <label for="inlineRadio5"> Date Wise </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div> 
                                                    <div class="row" id="view_usage_month" style="margin-top:20px;display:none">

                                                        <div class="col-md-6">
                                                            <table width="100%" border="0">
                                                                <tr>
                                                                    <td width="45%"><div class="ewap_cmp_name"><b>Select Month</b></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <select id="corp-list-id" class="corp-list form-control"  name="month">
                                                                            <option value="" disabled="" selected="">SELECT</option>
                                                                            <option value="MAY-2017">May 2017</option>	
                                                                            <option value="JUN-2017">June 2017</option>
                                                                            <option value="JUL-2017">July 2017</option>
                                                                            <option value="AUG-2017">August 2017</option>
                                                                            <option value="SEP-2017">September 2017</option>
                                                                            <option value="OCT-2017">October 2017</option>
                                                                            <option value="NOV-2017">November 2017</option>
                                                                            <option value="DEC-2017">December 2017</option>
                                                                            <option value="JAN-2018">January 2018</option>
                                                                            <option value="FEB-2018">February 2018</option>
                                                                            <option value="MAR-2018">March 2018</option>
                                                                            <option value="APR-2018">April 2018</option>
                                                                            <option value="MAY-2018">May 2018</option>
                                                                            <option value="JUN-2018">June 2018</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>  
                                                    <div class="row" id="view_usage_date" style="margin-top:20px;display:none">
                                                        <div class="col-md-12">
                                                            <table width="100%" border="0">
                                                                <tr>
                                                                    <td width="50%"><div class="ewap_cmp_name" style="padding-left:10px;"><b>Select Date</b></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%">
                                                                        <div style="padding:10px;width:100%;float:left">
                                                                            Select From Date
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar">
                                                                                    </i>
                                                                                </div>
                                                                                <input class="form-control" id="datepicker1" name="from_date" placeholder="DD/MM/YYYY" type="text"/>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td width="50%">
                                                                        <div style="padding:10px;width:100%;float:left">
                                                                            Select To Date
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar">
                                                                                    </i>
                                                                                </div>
                                                                                <input class="form-control" id="datepicker2" name="to_date" placeholder="DD/MM/YYYY" type="text"/>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div> 
                                                    <div class="row" id="view_usage_btn" style="display:none">
                                                        <div class="col-md-6">
                                                            <table width="100%" border="0">
                                                                <tr>
                                                                    <td width="45%">
                                                                        <div style="padding:10px;">
                                                                            <button class="btn btn-primary " name="submit" type="submit">
                                                                                Submit
                                                                            </button>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div> 
                                                </section>
                                            </article>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- jQuery  -->
            <script src="../assets/js/bootstrap.min.js"></script>
            <script src="../assets/js/detect.js"></script>
            <script src="../assets/js/fastclick.js"></script>
            <script src="../assets/js/jquery.slimscroll.js"></script>
            <script src="../assets/js/jquery.blockUI.js"></script>
            <script src="../assets/js/waves.js"></script>
            <script src="../assets/js/jquery.nicescroll.js"></script>
            <script src="../assets/js/jquery.scrollTo.min.js"></script>
            <!-- Datatables-->
            <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.bootstrap.js"></script>
            <script src="../assets/plugins/datatables/dataTables.buttons.min.js"></script>
            <script src="../assets/plugins/datatables/buttons.bootstrap.min.js"></script>
            <script src="../assets/plugins/datatables/pdfmake.min.js"></script>
            <script src="../assets/plugins/datatables/vfs_fonts.js"></script>
            <script src="../assets/plugins/datatables/buttons.html5.min.js"></script>
            <script src="../assets/plugins/datatables/buttons.print.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.keyTable.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.responsive.min.js"></script>
            <script src="../assets/plugins/datatables/responsive.bootstrap.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.scroller.min.js"></script>
            <!-- Datatable init js -->
            <script src="../assets/pages/datatables.init.js"></script>
            <!-- App js -->	

            <script src="../assets/plugins/moment/moment.js"></script>
            <script src="../assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
            <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>

            <script src="../assets/js/jquery.core.js"></script>
            <script type="text/javascript">
                                            $(document).ready(function () {
                                                $('#datatable').dataTable();
                                                $('#datatable-keytable').DataTable({keys: true});
                                                $('#datatable-responsive').DataTable();
                                                $('#datatable-scroller').DataTable({ajax: "../assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true});
                                                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                                            });
                                            TableManageButtons.init();

            </script>

            <!--validation for the form -->
            <script type="text/javascript">

                $(".form-horizontal").validate({
                    rules: {
                        corp_id: {
                            required: true
                        },
                        activity_type: {
                            required: true
                        },
                        date: {
                            required: true,
                        },
                        location: {
                            required: true,
                        }

                    },
                    // Specify validation error messages
                    messages: {
                        corp_id: {
                            required: "Please select corporate"
                        },
                        activity_type: {
                            required: "Please select activity"
                        },
                        date: {
                            required: "Please enter a date",
                        },
                        location: {
                            required: "Please enter a location",
                        }
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function (form) {
                        form.submit();
                    }
                });

            </script>

            <script>
                $('#datepicker1').datepicker({
                    startDate: '01/01/1990'
                });
                $('#datepicker2').datepicker({
                    startDate: '01/01/1990'
                });
            </script>
            <script>
                function edit(activity_id) {
                    $("#f_edit").attr("src", "edit_activity.php?activity_id=" + activity_id);
                }
                function add() {
                    $("#f_edit").attr("src", "add_activity.php");
                }
            </script>	
            <script>
                $("#view_m").click(function () {

                    $("#view_usage_month").show();
                    $("#view_usage_date").hide();
                    $('#view_usage_btn').show();
                });
                $("#view_d").click(function () {
                    $("#view_usage_date").show();
                    $("#view_usage_month").hide();
                    $('#view_usage_btn').show();
                });
            </script>

            <script type="text/javascript">
                $(function () {

                    var start = moment().subtract(29, 'days');
                    var end = moment();

                    show_date = '<?php echo date("F d, Y", strtotime($from_date)); ?>';
                    end_date = '<?php echo date("F d, Y", strtotime($to_date)); ?>';

                    function cb(start, end) {
                        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }

                    $('#reportrange').daterangepicker({
                        startDate: start,
                        endDate: end,
                        maxDate: end,
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        }
                    }, cb);

                    cb(moment(show_date), moment(end_date));

                });
            </script>

            <script>
                function get_daterange() {
                    var get_date_range = $("#get_date").text();
                    $("#daterange").val(get_date_range);
                }
            </script>
    </body>
</html>