<?php
/*
  1.In the sidebar when view_corporates is clicked we land onto this page.
  2.All the corporates details can be seen here, there id,name and registration type are displayed with a view button in last column
  3.Clicking onto the view button takes you to the individual corporate information
 */

#checking the login details
include '../../../if_loggedin.php';
include '../check_prvg.php';

## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("ADMIN_CORPORATE");
if (!$if_allowed_to_view_this_page) {
    exit();
}

#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';


# Status codes for error and success messages
$status_code = 0;

# Start database transactions
$i = 0;
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Retrieving corporate details
$stmt01 = $dbh->prepare("SELECT corp_login.corp_id,corp_login.name,corp_type.type FROM corp_login, corp_type WHERE corp_login.corp_id = corp_type.corp_id");
$stmt01->execute(array());
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {

        $corp_id[$i] = $row01['corp_id'];
        $decrypted_corp_name[$i] = decrypt($row01['name'], $encryption_key); //to fetch naqme and decrypt it
        $type = $row01['type']; //to fetch the type
        if ($type == 'CORP') {//if the value is corp then corporate paid is displayed otherwise employee paid if value is empl
            $reg_type[$i] = 'Corporate Paid';
            $amount[$i] = '0';
        } else if ($type == 'EMPL') {
            $reg_type[$i] = 'Employee Paid';
            $stmt02 = $dbh->prepare("SELECT amount FROM corp_license2 WHERE corp_id=?");
            $stmt02->execute(array($corp_id[$i]));
            if ($stmt02->rowCount() != 0) {
                $row02 = $stmt02->fetch();
                $amount[$i] = $row02['amount'];
            }
        }
        $stmt03 = $dbh->prepare("SELECT user_count FROM corp_users_count WHERE corp_id=?");
        $stmt03->execute(array($corp_id[$i]));
        if ($stmt03->rowCount() != 0) {
            $row03 = $stmt03->fetch();
            $reg_users[$i] = $row03['user_count']; //to fetch the count of users registerd
        } else {
            $reg_users[$i] = '0'; // No rows collected
        }
        $i++;
    }
} else {
    $status_code = 1; // No rows collected
}
$j = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

        <!-- App title -->
        <title>View Corporates - Silver Oak Health</title>

        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
         

        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }

        </style>
    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
							<div class="card-box"><a class="btn btn-primary" style="background-color: #223c87 !important; border-color: #223c87 !important;float:right;" 
								href="create_corporate.php">Create Corporate</a>
								<BR/><BR/>
								<div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                                    <div class="panel-body">
										<table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="color:#188ae2;">ID</th>
                                                    <th style="color:#188ae2;">Corporate Name</th>
                                                 
                                                    <th>                                          
                                                     </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php for ($k = 0; $k < count($corp_id); $k++) { ?>
                                                    <!--if status_code==1 displays  corporator id name and register type-->
                                                    <tr>
                                                        <td><?php echo $corp_id[$k]; ?></td>
                                                        <td><?php echo $decrypted_corp_name[$k]; ?></td>
                                                        <td><a class="btn btn-primary" style ="background-color: #223c87 !important; border-color: #223c87 !important; " href="view_corporate.php?corp_id=<?php echo $corp_id[$k]; ?>">View & Edit</a></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>

            <!-- jQuery  -->
            <script src="../assets/js/jquery.min.js"></script>
            <script src="../assets/js/bootstrap.min.js"></script>
            <script src="../assets/js/detect.js"></script>
            <script src="../assets/js/fastclick.js"></script>
            <script src="../assets/js/jquery.slimscroll.js"></script>
            <script src="../assets/js/jquery.blockUI.js"></script>
            <script src="../assets/js/waves.js"></script>
            <script src="../assets/js/jquery.nicescroll.js"></script>
            <script src="../assets/js/jquery.scrollTo.min.js"></script>
            <!-- Datatables-->
            <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.bootstrap.js"></script>
            <script src="../assets/plugins/datatables/dataTables.buttons.min.js"></script>
            <script src="../assets/plugins/datatables/buttons.bootstrap.min.js"></script>
            <script src="../assets/plugins/datatables/jszip.min.js"></script>
            <script src="../assets/plugins/datatables/pdfmake.min.js"></script>
            <script src="../assets/plugins/datatables/vfs_fonts.js"></script>
            <script src="../assets/plugins/datatables/buttons.html5.min.js"></script>
            <script src="../assets/plugins/datatables/buttons.print.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.keyTable.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.responsive.min.js"></script>
            <script src="../assets/plugins/datatables/responsive.bootstrap.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.scroller.min.js"></script>
            <!-- Datatable init js -->
            <script src="../assets/pages/datatables.init.js"></script>
            <!-- App js -->
            <script src="../assets/js/jquery.core.js"></script>
            <script src="../assets/js/jquery.app.js"></script>
			<script src="../assets/app.min.js"></script>

            <script type="text/javascript">
                            $(document).ready(function () {
                                $('#datatable').dataTable();
                                $('#datatable-keytable').DataTable({keys: true});
                                $('#datatable-responsive').DataTable();
                                $('#datatable-scroller').DataTable({ajax: "../assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true});
                                var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                            });
                            TableManageButtons.init();

            </script>
    </body>
</html>