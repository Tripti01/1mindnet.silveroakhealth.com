<?php
/*
  1.After the privilege set by superadmin he can choose the corporate on the slidebar then when he clicks create corporate he lands on this page.
  2.Coprporate admin  is allowed to insert the data into the given form and the credentials of new corporate is created.
  3.Mail could be sent with particular credentials.
 */

#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'soh-config.php';
include '../../../if_loggedin.php';
include '../check_prvg.php';
include 'functions/crypto_funtions.php';
include 'functions/uid.php';
include '../admin_mail/send_user_created_email.php';


#default value set for department,domain and role
$value = 'Not Applicable';

$end = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));

## Check if user has access to view this page ##
 $if_allowed_to_view_this_page = user_has_prvg("ADMIN_CORPORATE");
if (!$if_allowed_to_view_this_page) {
    header("Location: index.php");
    exit();
}
 
#start the database transaction
$dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
$dbh->query("use sohdbl");

# Select verticals from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_vertical_list ORDER BY vertical_name");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $vertical_id[$i] = $row00['vertical_id'];
		$vertical_name[$i] = $row00['vertical_name'];
        $i++;
    }
}


# Status codes for error and success messages
$status_code = 0;

if (isset($_REQUEST['submit_btn'])) {

    if (isset($_REQUEST['email']) && $_REQUEST['email'] !== '' && isset($_REQUEST['name']) && $_REQUEST['name'] !== '' && isset($_REQUEST['password']) && $_REQUEST['password'] !== '' && isset($_REQUEST['address']) && $_REQUEST['address'] !== '' && isset($_REQUEST['dept']) && $_REQUEST['dept'] !== '' && isset($_REQUEST['role']) && $_REQUEST['role'] !== '' && isset($_REQUEST['domain']) && $_REQUEST['domain'] !== '' && isset($_REQUEST['reg_type']) && $_REQUEST['reg_type'] !== '') {


        # Current time stamp for corporate creation time
        date_default_timezone_set("Asia/Kolkata");
        $email = $_REQUEST['email']; //fetch the email
        $name = $_REQUEST['name']; //fetch the name
        $password = $_REQUEST['password'];
        $address = $_REQUEST['address']; //fetch the address
        $dept = $_REQUEST['dept'];
        $role = $_REQUEST['role'];
        $domain = $_REQUEST['domain'];
        $reg_type = $_REQUEST['reg_type'];
		$license_start_date = $_REQUEST['start_date'];
		$license_start_date = date('Y-m-d h:i:s A', strtotime($license_start_date));
		$license_end_date = $_REQUEST['end_date'];
		$license_end_date = date('Y-m-d h:i:s A', strtotime($license_end_date));
		$vertical_id = $_REQUEST['vertical'];

        #to check the value for the register type if its CORP then no.of users text field is enabled
        #and if the value is EMPL then amount text field is enabled
        if (strcmp($reg_type, 'CORP') == 0) {
            if (!empty($_REQUEST['no_of_users']) && isset($_REQUEST['no_of_users'])) {
                $no_of_users = $_REQUEST['no_of_users'];
            } else {
                $no_of_users = "";
            }
        } else if (strcmp($reg_type, 'EMPL') == 0) {
            if (!empty($_REQUEST['amount']) && isset($_REQUEST['amount'])) {
                $amount = $_REQUEST['amount'];
            } else {
                $amount = "";
            }
        }


        # if the all the conditions are checked then start database transaction
        $encrypted_name = encrypt($name, $encryption_key);
        $hash_options = array('cost' => 11);
        $hashed_password = password_hash($password, PASSWORD_DEFAULT, $hash_options);

        #create new corporate uid
        $corp_id = create_new_uid($reg_type);

        #start the database transaction
        $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
        $dbh->query("use sohdbl");

        #Checking email already exist or not, If not will verify the password
        $stmt01 = $dbh->prepare("SELECT name FROM corp_login WHERE email=? LIMIT 1");
        $stmt01->execute(array($email));
        if ($stmt01->rowCount() != 0) {
            $status_code = 1; // The email id is already registerd
        } else {
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh->beginTransaction();
            try {

                $stmt02 = $dbh->prepare("INSERT INTO corp_login VALUES(?,?,?,?);");
                $stmt02->execute(array($email, $corp_id, $encrypted_name, $hashed_password));

                $stmt03 = $dbh->prepare("INSERT INTO corp_profile VALUES (?,?,?,?,?,?,?,?)");
                $stmt03->execute(array($corp_id, '', $address, '', $name, '', '', ''));

                $stmt04 = $dbh->prepare("INSERT INTO corp_dept VALUES (?,?,?)");
                $stmt04->execute(array('', $corp_id, $dept));

                $stmt05 = $dbh->prepare("INSERT INTO corp_domain VALUES (?,?)");
                $stmt05->execute(array($corp_id, ''));


                if ($reg_type == 'CORP') {
                    $stmt06 = $dbh->prepare("INSERT INTO corp_license VALUES (?,?,?,?,?,?,?,?,?)");
                    $stmt06->execute(array($corp_id, $license_start_date, $license_end_date, 'SC', $no_of_users, '', '', '', ''));
                } else if ($reg_type == 'EMPL') {
                    $stmt07 = $dbh->prepare("INSERT INTO corp_license2 VALUES (?,?,?,?,?,?)");
                    $stmt07->execute(array($corp_id, $amount, '', '', '', ''));
                }
				
				$stmt08 = $dbh->prepare("INSERT INTO corp_vertical VALUES (?,?)");
                $stmt08->execute(array($corp_id, $vertical_id));


                $stmt08 = $dbh->prepare("INSERT INTO corp_role VALUES (?,?,?)");
                $stmt08->execute(array('', $corp_id, $role));

                $stmt09 = $dbh->prepare("INSERT INTO corp_type VALUES (?,?)");
                $stmt09->execute(array($corp_id, $reg_type));

                $stmt10 = $dbh->prepare("INSERT INTO corp_users_count VALUES (?,?)");
                $stmt10->execute(array($corp_id, '0'));

                # Send corporate created mail to the admin with the login details
                //send_corporate_created_email($admin_email, $admin_name, $email, $password);


                $status_code = 2; //all done successfully
                $dbh->commit();
            } catch (PDOException $e) {
                $dbh->rollBack();
                die("Some Error Occured. Please try again. If the issue still persists. Send us an email at help@stresscontrolonline.com. Error Code :CRET_CORP_2");
            }
        }
    } else {
        $status_code = 3; //some empty fields were submitted
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Create Corporate - Silver Oak Health</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- Plugins css-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="../assets/app.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" /> 
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="../assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
         
		   <style>
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
                margin-bottom:1.6%;
            }
            .input-group {
                width:55%;
            }
            .input-group-btn-vertical {
                margin-left:350px;
            }
            .form-control{
                height: 35px;
            }
            .btn btn-primary bootstrap-touchspin-up{
                padding: 9px 10px;
            }
            .input-group-btn-vertical > .btn {
                padding: 9px 10px;
            }
            .form-group{
                margin-bottom: 20px;
            }
            .ui-datepicker { 
                margin-left: 350px;
                z-index: 1000;
                position: absolute;
            }
            article {
                position: relative;
                padding: 0 5em;
                border: 1px solid #223C87;
                margin-bottom: 3em;
                border-radius: 5px;
            }

            article:hover {
                border: 1px solid #3AB149;
            }

            article:first-child {
                margin-top: 2em;
            }

            article .title {
                position: absolute;
                left: 30px;
                top: -25px;
                padding-left: 5px; padding-right: 5px;
                padding-bottom: 5px;
                font-family: 'open sans';
                background-color: #fff;
                font-size:20px;
            }

            @media (max-width: 960px) {

                article {
                    padding: 0 2em;
                }
            }
            .article{
                line-height:10px;
            }
            .hr_css {
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(0, 0, 0, 0.95), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
                margin-top: -15px;

            }
            .label_css{
                text-align: left;
                margin-bottom: 0px;
                color:black;
                margin-left: 10px;
                margin-top: 2px;

                font-size: 16px;
            }
            .btn-success {
                background-color: #71b6f9 !important;
                border: 1px solid #71b6f9 !important;
            }
            .btn-danger {
                background-color: #71b6f9 !important;
                border: 1px solid #71b6f9 !important;
            }
			.checkbox input[type=checkbox] {
				cursor: pointer;
				opacity: 0;
				z-index: 1;
				outline: 0!important;
				width: 200px;
			}
        </style>
    </head>
    <body data-layout="horizontal" data-topbar="dark">
        <div id="wrapper">
			<?php include '../top_navbar.php';?>
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="y-overflow:auto;">
                                <div class="card-box">
                                    <article>
                                        <h5 class="title">
                                            Please provide the following details
                                        </h5>
                                        <section>
                                            <div class="row">
                                                <!--To display error messages --->
                                                <div class="email-confirm-msg" style="margin-top:12px;">
                                                    <?php
                                                    if ($status_code != 0) {
                                                        switch ($status_code) {
                                                            case 1: echo '<div  style="text-align:center; color:#3AB149;">';
                                                                echo " This email is already registered";
                                                                echo '</div>';
                                                                break;
                                                            case 2:echo '<div  style="text-align:center; color:#3AB149;">';
                                                                echo "Corporate has been created successfully with corp_id " . $corp_id;
                                                                echo '</div>';
                                                                echo '<div  style="text-align:center; color:#3AB149;">';
                                                                echo "To modify Domain,Department,Role details ";
                                                                echo '<a href="view_corporate.php?corp_id=' . $corp_id . '"> Click Here</a>';
                                                                echo '</div>';
                                                                break;
                                                            case 3:
                                                                echo '<div  style="text-align:center; color:green;">';
                                                                echo "Some empty fields were submitted, Please try again";
                                                                echo '</div>';
                                                                break;
                                                            default: break;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <!--NAME -->
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" id="form-horizontal" class="form-horizontal" name="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-12 label_css" style="margin-top: 45px;color:#797979;">Name</label>
                                                </div>
                                                <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="CorporateName"  style="font-weight:normal;text-align:left;">Corporate Name <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="name" placeholder="Enter Corporate Name" class="form-control" id="name" />
                                                    </div>
                                                </div>
                                                <!--LOGIN DETAILS -->
                                                <div class="form-group">
                                                    <label class="col-md-12 label_css" style="color:#797979;">Login Details</label>
                                                </div>
                                                <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="email"  style="font-weight:normal;text-align:left;">HR Email Address <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="email" name="email" placeholder="Enter HR Email Address" class="form-control" id="email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="password"  style="font-weight:normal;text-align:left;">Password <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="password" name="password" placeholder="Enter Password" class="form-control" id="password"  >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="c_password"  style="font-weight:normal;text-align:left;">Confirm Password <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="password" name="c_password" placeholder="Confirm Password" class="form-control" id="c_password" >
                                                    </div>
                                                </div>

                                                <!--CONTACT DETAILS -->
                                                <div class="form-group" >
                                                    <label class="col-md-12 label_css" style="color:#797979;">Contact Details</label>
                                                </div>
                                                <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="address"  style="font-weight:normal;text-align:left;">Corporate Address <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="address" placeholder="Enter Corporate Address" class="form-control" id="address"  >
                                                    </div>
                                                </div>

                                                <!--LICENSE DETAILS -->
                                                <div class="form-group">
                                                    <label class="col-md-12 label_css" style="color:#797979;">License Details</label>
                                                </div>
                                                <HR class="hr_class" style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;margin-top:-1%;"/>
                                                <div class="form-group" style="margin-bottom:3%;" >
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
													<div class="col-md-3" style="margin-top:1%;" >License Type <span style="color:red;">*</span></div>
                                                    <div class="col-md-2 checkbox checkbox-inline checkbox-circle checkbox-purple">
                                                        <input type="checkbox"  name="reg_type" id="reg_type1" value="CORP"  onClick="EnableTextbox('corporate')" >
                                                        <label for="reg_type"> Corporate Paid </label>
                                                    </div>

                                                    <div class="checkbox checkbox-inline checkbox-circle checkbox-purple">
                                                        <input type="checkbox" name="reg_type" id="reg_type2" value="EMPL"   onClick="EnableTextbox('employee')">
                                                        <label for="reg_type"> Employee Paid </label>
                                                        <div id="err_checkbox" class="error" style="margin-left:-165%;">
                                                            <!--message is displayed for validation-->
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="no_of_users"  style="font-weight:normal;text-align:left;">No. of Users <span style="color:red;">*</span></label>
                                                    <div class="col-md-7">
                                                        <input class="form-control" type="text" name="no_of_users" placeholder="No. of Users" style="width:63%;" disabled  id="no_of_users" >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="amount"  style="font-weight:normal;text-align:left;">Amount <span style="color:red;">*</span></label>
                                                    <div class="col-md-7">

                                                        <input class="form-control" type="text" name="amount" placeholder="Amount" style="width:63%;" disabled  id="amount">
                                                    </div>
                                                </div>
												<div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="start_date" style="font-weight:normal;text-align:left;"> Start Date <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <div class="input-group" style="width:100%;">
                                                            <input type="text"  name="start_date" class="form-control" placeholder="dd/mm/yyyy" id="datepicker-autoclose">
                                                            <span class="input-group-addon bg-primary b-0 text-white" ><i class="ti-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
												
												<div class="form-group">
                                                    <div class="col-md-1">
                                                        &nbsp;
                                                    </div>
                                                    <label class="col-md-3 control-label" for="end_date" style="font-weight:normal;text-align:left;"> End Date <span style="color:red;">*</span></label>
                                                    <div class="col-md-6">
                                                        <div class="input-group" style="width:100%;">
                                                            <input type="text"  name="end_date" class="form-control" placeholder="dd/mm/yyyy" id="datepicker-autoclose1">
                                                            <span class="input-group-addon bg-primary b-0 text-white" ><i class="ti-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
												
												<div class="form-group">
                                                        <div class="col-md-1">
                                                            &nbsp;
                                                        </div>
                                                        <label class="col-md-3 control-label" for="vertical"  style="font-weight:normal;text-align:left;">Vertical <span style="color:red;">*</span></label>
                                                        <div class="col-md-6">
                                                            <select id="vertical" name="vertical" title="" required class="form-control" style="margin-top:0px;font-size: 12px;">
                                                                <option value="-1" disabled="" selected="" style="color:#808080ad;">   SELECT   </option> 
                                                               <?php
																for ($i = 0; $i < count($vertical_id); $i++) {
																	echo '<option value="' . $vertical_id[$i] . '">' . $vertical_name[$i] . '</option>';
																}
																?>
                                                            </select>
                                                        </div>
                                                 </div> 
													
                                                <div class="form-group text-right m-b-0" style="margin-top:30px;padding-bottom:20px;">
                                                    <input type="hidden" name="dept" id="dept" value="<?php echo $value; ?>">
                                                    <input type="hidden" name="role" id="role" value="<?php echo $value; ?>">
                                                    <input type="hidden" name="domain" id="domain" value="<?php echo $value; ?>">
                                                    <input type="submit" class="btn btn-primary" style="background:#223c87 !important; border-color: #223c87 !important;" value=" Create Corporate" name="submit_btn" id="submit_btn">
                                                    <button type="reset" class="btn btn-primary" style="background:#b1b2b8  !important; border-color: #b1b2b8  !important;">Clear</button>
                                                </div>
                                            </form>

                                        </section>
                                    </article>
                                </div><!-- card-box -->
                            </div><!-- col-lg-12 -->
                        </div><!-- row -->
                    </div><!--container-->
                </div><!--content-->
            </div><!-- content-page -->
        </div><!-- wrapper-->
        <!-- jQuery  -->
        <!-- jQuery  -->
	 <script>
        var resizefunc = [];
    </script>
     <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/detect.js"></script>
    <script src="../assets/js/fastclick.js"></script>
    <script src="../assets/js/jquery.slimscroll.js"></script>
    <script src="../assets/js/jquery.blockUI.js"></script>
    <script src="../assets/js/waves.js"></script>
    <script src="../assets/js/jquery.nicescroll.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>

    <!-- Form wizard -->
    <script src="../assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
    <script src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <!-- App js -->
    <script src="../assets/js/jquery.core.js"></script>
    <script src="../assets/js/jquery.app.js"></script>

    <script src="../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

		<!-- App js -->
        <script src="../assets/js/app.min.js"></script>

        <!--function to enable the text field based on the radio button checked  -->
		 <script>
			$(function () {
				$("#datepicker").datepicker({dateFormat: 'dd/mm/yyyy'}).val();
			});
		</script>
        <script>

                function EnableTextbox(type)
                {

                    if (type == 'corporate') {
                        document.getElementById('amount').disabled = true;
                        document.getElementById('amount').value = "";
                        $('#reg_type').on('click', function () {
                            $("#amount").reset();
                        });
                        $("#reg_type").html("<input type='hidden' name='amount' value='0'/>");
                    } else {
                        document.getElementById('amount').disabled = false;
                    }

                    if (type == 'employee') {
                        document.getElementById('no_of_users').disabled = true;
                        document.getElementById('no_of_users').value = "";
                        $("#reg_type").html("<input type='hidden' name='no_of_users' value='0'/>");
                    } else {
                        document.getElementById('no_of_users').disabled = false;
                    }
                }

        </script>

        <script>
            // the selector will match all input controls of type :checkbox
            // and attach a click event handler
            $("input:checkbox").on('click', function () {
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
        </script>

        <!--validation for the form -->
        <script type="text/javascript">

            $(".form-horizontal").validate({
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 7
                    },
                    c_password: {
                        required: true,
                        minlength: 7,
                        equalTo: '#password'
                    },
                    address: {
                        required: true
                    },
                    vertical: {
                        required: true
                    },
                    start_date: {
                        required: true
                    },
					end_date: {
                        required: true
                    }

                },
                // Specify validation error messages
                messages: {
                    name: {
                        required: "Please enter corporate's name"
                    },
                    email: {
                        required: "Please enter HR's email address"
                    },
                    password: {
                        required: "Please enter a valid password",
                        minlength: "Make sure the password has a minimum length of 7 "
                    },
                    c_password: {
                        required: "Please enter a valid password",
                        minlength: "Make sure the password has a minimum length of 7 ",
                        equalTo: "Password needs to be same as above"
                    },
                    address: {
                        required: "Please enter the corporate address"

                    },					
                    vertical: {
                        required: "Please enter vertical"
                    },
                    start_date: {
                        required: "Please enter start date"
                    },
					end_date: {
                        required: "Please enter end date"
                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

        </script>

        <script>
            $("#submit_btn").click(function () {
                if ($('input[type=checkbox]:checked').length == 0)
                {
                    $("#err_checkbox").html("Select atleast one type");
                } else {
                    $("#err_checkbox").html("");
                }
            });
        </script>
		<script>
        // Date Picker
        jQuery('#datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
		jQuery('#datepicker-autoclose1').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('#datepicker-inline').datepicker();
        jQuery('#datepicker-multiple-date').datepicker({
            format: "mm/dd/yyyy",
            clearBtn: true,
            multidate: true,
            multidateSeparator: ","
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });

    </script>
    </body>
</html>
