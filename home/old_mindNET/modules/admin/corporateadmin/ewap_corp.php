<?php
/*
  1.After the privilege set by superadmin he can choose the corporate on the slidebar then when he clicks create corporate he lands on this page.
  2.Coprporate admin  is allowed to insert the data into the given form and the credentials of new corporate is created.
  3.Mail could be sent with particular credentials.
 */

#file inclusion for various function happening in the ui
include 'soh-config.php';
include 'mindnet-host.php';
include '../if_loggedin.php';
include '../check_prvg.php';
include 'functions/crypto_funtions.php';
include 'functions/uid.php';
include '../admin_mail/send_user_created_email.php';

#Get the admin_name and admin_email from the session
$admin_name = $_SESSION['name'];
$admin_email = $_SESSION['email'];

#default value set for department,domain and role
$value = 'Not Applicable';

$end = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));

## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("ADMIN_CORPORATE");
if (!$if_allowed_to_view_this_page) {
    header("Location: index.php");
    exit();
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Create Corporate - Silver Oak Health</title>
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- Plugins css-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="../../assets/js/modernizr.min.js"></script>
        <style>
            #field {
                margin-bottom:20px;
            }
            .newdiv{
                margin-left:33.5%;
                width:28%;
                margin-top:5px;
            }
            .form-control{
                float:left;
                margin-left:10%;
            }
            .segmented-button  {
                width: 100px;
                height: 37px;
                margin-left: 10px;
                background: linear-gradient(#59a9f8 , #59a9f8 );
                color: white;
                padding: 15px;
                padding-bottom: 3px;
                font-size: 12px;
                cursor: pointer;
                font-family: 'open sans';
                border-radius: 2px;
                margin-right: -5px;
            }
            button {
                overflow: visible;
                background-color:#59a9f8;
            }
            .btn-success {
                background-color: #71b6f9 !important;
                border: 1px solid #71b6f9 !important;
            }
            article {
                position: relative;
                padding: 0 5em;
                border: 1px solid #223C87;
                margin-bottom: 3em;
                border-radius: 5px;
            }

            article:hover {
                border: 1px solid #3AB149;
            }

            article:first-child {
                margin-top: 2em;
            }

            article .title {
                position: absolute;
                left: 30px;
                top: -25px;
                padding-left: 5px; padding-right: 5px;
                padding-bottom: 5px;
                font-family: 'open sans';
                background-color: #fff;
                font-size:20px;
            }

            @media (max-width: 960px) {

                article {
                    padding: 0 2em;
                }
            }
            .article{
                line-height:10px;
            }
            .hr_css {
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(0, 0, 0, 0.95), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
                margin-top: -10px;
                margin-bottom: 20px;
            }
            .hr_class{
                border: 0;height: 1px;background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));margin-bottom:1.6%;
            }
            .label_css{
                text-align: left;
                margin-bottom: 0px;
                color:black;
                margin-left: 10px;
                margin-top: 2px;

                font-size: 16px;
            }
            .btn-danger {
                background-color: #71b6f9 !important;
                border: 1px solid #71b6f9 !important;
            }
            .form-control{
                float:left;
                margin-left:10%;
            }
            .segmented-button  {
                width: 100px;
                height: 37px;
                margin-left: 10px;
                background: linear-gradient(#59a9f8 , #59a9f8 );
                color: white;
                padding: 15px;
                padding-bottom: 3px;
                font-size: 12px;
                cursor: pointer;
                font-family: 'open sans';
                border-radius: 2px;
                margin-right: -5px;
            }
            button {
                overflow: visible;
                background-color:#59a9f8;
            }
            .btn-success {
                background-color: #59a9f8 !important;
                border: 1px solid #59a9f8 !important;
            }
        </style>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <img src="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/silver_oak_health_logo.png" style="height:55%;">
                    <p style="margin-top:1%;font-size: 16px;font-weight:bold"><span>Admin Console</span></p>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title" style="color:#223c87;">Create Corporate</h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <HR class="hr_class" style="margin-bottom:20px;margin-top:-10%;" >
                        <h5>
                            <a href="#"  style="font-weight:bolder; font-size:14px; color:#6fa7d7; margin-bottom:20px;"><?php echo $admin_name ?></a>
                        </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="../my_account.php" alt="settings" >
                                    <i class="zmdi zmdi-settings" style="color:#6FA7D7;"></i>
                                </a>
                            </li>
                            <li>
                                <a href="../logout.php" class="text-custom">
                                    <i class="zmdi zmdi-power" style="color:#6FA7D7;"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="sidebar-menu" style="margin-top:-11%;">
                        <ul>
                            <?php include '../sidebar.php'; ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12" style="y-overflow:auto;">
                                <div class="card-box">
                                    <article>
                                        <h5 class="title">
                                            Please provide the following details
                                        </h5>
                                        <section>
										<iframe src="http://cronus.silveroakhealth.com/cronus/upload_ewap_db_xl/upload_ewap_db_xl" height="200px" width="200px" frameBorder="0" scrolling="no"></iframe>
                                        </section>
                                    </article>
                                </div><!-- card-box -->
                            </div><!-- col-lg-12 -->
                        </div><!-- row -->
                    </div><!--container-->
                </div><!--content-->
            </div><!-- content-page -->
        </div><!-- wrapper-->

        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>

        <!-- Form wizard -->
        <script src="../../assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="../../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <!-- App js -->
        <script src="../../assets/js/jquery.core.js"></script>
        <script src="../../assets/js/jquery.app.js"></script>

        <script src="../../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

      


    </body>
</html>
