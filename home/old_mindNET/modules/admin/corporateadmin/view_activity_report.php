<?php
include '../../../if_loggedin.php';
include '../check_prvg.php';
include 'mindnet-host.php';
include 'scodd.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';

## Check if user has access to view this page ##
$if_allowed_to_view_this_page = user_has_prvg("ADMIN_CORPORATE");
if (!$if_allowed_to_view_this_page) {
    exit();
}

if (isset($_REQUEST['corp_id'])) {
   
    $corp_id = $_REQUEST['corp_id'];
    
    $dbh = new PDO($dsn_sco, $ssn_user, $ssn_pass);
    $dbh->query("use sohdbl");
    
    $stmt01 = $dbh->prepare("SELECT * FROM corp_login WHERE corp_id=?");
    $stmt01->execute(array($corp_id));
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        $corp_name = decrypt($row01['name'],$encryption_key);
    }else{
        $corp_name = "";
    }
    
    if(isset($_REQUEST['period'])){
	
        $period = $_REQUEST['period'];
        
        # Calculate 1st and last date of month
        $curr_year = $_REQUEST['year'];
		
        # Date to calculate
        $first_monthdate = date('Y-m-01', strtotime($period . " " . $curr_year));
        $last_monthdate = date('Y-m-t', strtotime($period . " " . $curr_year));
    }

    if(isset($_REQUEST['from_date']) &&  isset($_REQUEST['to_date'])){
        $from_date = $_REQUEST['from_date'];
        $to_date = $_REQUEST['to_date'];
        
        
        # Date to calculate
        $first_monthdate = date('Y-m-d',strtotime($from_date));
        $last_monthdate = date('Y-m-d',strtotime($to_date));
    }

} else {
    die("Oops! Something went wrong.");
}

	# Select activities from database and display in the form
	$stmt00 = $dbh->prepare("SELECT * FROM corp_activity_track1,corp_activity_track2 WHERE corp_activity_track1.activity_id = corp_activity_track2.activity_id AND corp_activity_track1.corp_id=? AND DATE(held_on) >= ? AND DATE(held_on)<=? ORDER BY corp_activity_track1.held_on");
	$stmt00->execute(array($corp_id, $first_monthdate, $last_monthdate));
	if ($stmt00->rowCount() != 0) {
		$i = 0;
		while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
			$activity_id[$i] = $row00['activity_id'];
			$ac_corp_id[$i] = $row00['corp_id'];
		 	$held_on[$i] = date("d-M-Y", strtotime($row00['held_on']));
			$activity_type[$i] = $row00['activity_type'];
			$country[$i] = $row00['country']; 
			$state[$i] = $row00['state']; 
			$city[$i] = $row00['city']; 
			$additional_details[$i] = $row00['add_details']; 
			$facilitator_name[$i] = $row00['facilitator_name'];
			$ac_remarks[$i] = $row00['remarks'];
			$location[$i] = $city[$i];
			
			if( $row00['status'] == 'C'){
				$completed_status[$i] = 'Completed';
			}else if($row00['status'] == 'S'){
				$completed_status[$i] = 'Scheduled';
			}else{
				$completed_status[$i] = '';
			}
			
			$stmt01 = $dbh->prepare("SELECT activity_name FROM corp_activity WHERE activity_type=?");
			$stmt01->execute(array($activity_type[$i]));
			if ($stmt01->rowCount() != 0) {
				$row01 = $stmt01->fetch();
				$ac_name[$i] = $row01['activity_name'];
			}
			
			$i++;
		}
	}
?>
<html>
    <HEAD>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">
        <!-- App title -->
        <title>Activity Report | Silver Oak Health</title>

        <!-- DataTables -->
        <link href="../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="style.css" rel="stylesheet">
    </HEAD>
    <body>
			<!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="card-box">
                                <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                                    <div class="panel-body" style="background-color:#FCFCFB;">		
										<div style="padding-bottom:30px;font-size:18px;color:#000;font-weight:bolder;text-align:center"> <?php echo $corp_name; ?> Activity Report</div>
                                        <table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="color:#188ae2;">Sno</th>
                                                    <th style="color:#188ae2;">Corporate Name</th>
                                                    <th style="color:#188ae2;">Activity</th>
                                                    <th style="color:#188ae2;width:9%">Date</th>
                                                    <th style="color:#188ae2;">Location</th>													
                                                    <th style="color:#188ae2;">Status</th>
                                                    <th style="color:#188ae2;">Additional Details</th>
                                                    <th style="color:#188ae2;">Facilitator Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>												
                                                <?php 
													if(isset($ac_corp_id)){
														for ($k = 0; $k < count($ac_corp_id); $k++) { ?>
															<!--if status_code==1 displays  corporator id name and register type-->
															<tr>
																<td><?php echo ($k+1); ?></td>
																<td><?php echo $corp_name; ?></td>
																<td><?php echo $ac_name[$k]; ?></td>
																<td><?php echo $held_on[$k]; ?></td>
																<td><?php echo $location[$k]; ?></td>																
																<td><?php echo $completed_status[$k]; ?></td>
																<td><?php echo $additional_details[$k]; ?> </td>
																<td><?php echo $facilitator_name[$k]; ?> </td>
															</tr>
														<?php } 
													}	?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			<!-- jQuery  -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/bootstrap.min.js"></script>
			<script src="../assets/js/detect.js"></script>
			<script src="../assets/js/fastclick.js"></script>
			<script src="../assets/js/jquery.slimscroll.js"></script>
			<script src="../assets/js/jquery.blockUI.js"></script>
			<script src="../assets/js/waves.js"></script>
			
			<!-- Datatables-->
            <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.bootstrap.js"></script>
            <script src="../assets/plugins/datatables/dataTables.buttons.min.js"></script>
            <script src="../assets/plugins/datatables/buttons.bootstrap.min.js"></script>
            <script src="../assets/plugins/datatables/jszip.min.js"></script>
            <script src="../assets/plugins/datatables/pdfmake.min.js"></script>
            <script src="../assets/plugins/datatables/vfs_fonts.js"></script>
            <script src="../assets/plugins/datatables/buttons.html5.min.js"></script>
            <script src="../assets/plugins/datatables/buttons.print.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.keyTable.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.responsive.min.js"></script>
            <script src="../assets/plugins/datatables/responsive.bootstrap.min.js"></script>
            <script src="../assets/plugins/datatables/dataTables.scroller.min.js"></script>
            <!-- Datatable init js -->
            <script src="../assets/pages/datatables.init.js"></script>
			
			<!-- App js -->
			<script src="../assets/js/jquery.core.js"></script>
			<script src="../assets/js/jquery.app.js"></script>
    </body>
</html>