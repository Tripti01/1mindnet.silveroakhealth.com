<?php
require_once('ewap-config.php');
require_once('soh-config.php');
require_once('tranquil-config.php');
include 'functions/crypto_funtions.php';

function get_ssn_fullname($ssn) {
    switch ($ssn) {
        case "SSN0":
            return "Introduction";
            break;
        case "SSN1":
            return "Session 1";
            break;
        case "SSN2":
            return "Session 2";
            break;
        case "SSN3":
            return "Session 3";
            break;
        case "SSN4":
            return "Session 4";
            break;
        case "SSN5":
            return "Session 5";
            break;
        case "SSN6":
            return "Session 6";
            break;
        case "SSN7":
            return "Session 7";
            break;
        case "SSN8":
            return "Session 8";
            break;
        case "CBTSTART":
            return "CBT Start";
            break;
        case "CBTEND":
            return "CBT End";
            break;
        default;
    }
}

if (isset($_REQUEST['corp_id']))
{

    $corp_id = $_REQUEST['corp_id'];

    $dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
    $dbh->query("use sohdbl");

    $stmt01 = $dbh->prepare("SELECT * FROM corp_profile WHERE corp_id=?");
    $stmt01->execute(array(
        $corp_id
    ));
    if ($stmt01->rowCount() != 0)
    {
        $row01 = $stmt01->fetch();
        $corp_name = $row01['corp_fullname'];
        $address = $row01['address'];
    } else
    {
        $corp_name = "";
        $address = "";
    }
}
if (isset($_REQUEST['from_date']) && isset($_REQUEST['to_date']))
{
    $from_date = $_REQUEST['from_date'];
    $to_date = $_REQUEST['to_date'];

    # Date to display
    $first_date = date('d-M-y', strtotime($from_date));
    $last_date = date('d-M-y', strtotime($to_date));

    # Date to calculate
    $ewap_first_monthdate = date('Y-m-d', strtotime($from_date));
    $ewap_last_monthdate = date('Y-m-d', strtotime($to_date));
}
?>
<!DOCTYPE html>
<html>

    <head>
        <style>
            table, th, td {
                border: 1px solid #DCDCDC;
            }
        </style> 
    </head>
    <body>
        <p><b>EWAP Activity</b></p>
        <table style="cborder: 1px solid black;">
            <tr>
                <th></th>
                <th>Name</th>
                <th>Caller Id</th>
                <th>Employee Id</th>
                <th>YOB</th>
                <th>Mobile</th>
                <th>DateTime</th>
                <th>Gender</th>
                <th>Vertical</th>
                <th>Process</th>
                <th>Location</th>
                <th>Employee Type</th>
                <th>Reason</th>
                <th>Call Type</th>
            </tr>
            <?php
            $i = 0;
# Retrieving callers details
            $stmt01 = $dbh_ewap->prepare("SELECT call_callers_profile.cid, call_callers_profile.name, call_callers_profile.emp_id, call_callers_profile.yob, call_callers_profile.mobile, call_callers_profile.gender, call_callers_profile.location, call_callers_notes.taken_at, call_callers_notes.emp_type, call_callers_notes.reason, call_callers_notes.source
FROM call_callers_notes, call_callers_profile
WHERE call_callers_notes.cid = call_callers_profile.cid AND call_callers_profile.corp_id =? AND DATE(call_callers_notes.taken_at) >=? AND DATE(call_callers_notes.taken_at)<=? ORDER BY call_callers_profile.cid ASC, taken_at ASC");
            $stmt01->execute(array(
                $corp_id,
                $ewap_first_monthdate,
                $ewap_last_monthdate
            ));
            if ($stmt01->rowCount() != 0)
            {
                while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
                    
                    $vertical=$process="";
                    
                    $cid[$i] = $row01['cid'];
                    $encrypted_name[$i] = $row01['name'];

                    $name[$i] = decrypt($encrypted_name[$i], $encryption_key);

                    $emp_id[$i] = $row01['emp_id'];
                    $yob[$i] = $row01['yob'];
                    $mobile[$i] = $row01['mobile'];
                    $gender[$i] = $row01['gender'];
                    $location[$i] = $row01['location'];

                    $date = date_create($row01['taken_at']);
                    $datetime[$i] = date_format($date, "d-M-y H:i");
                    $call_type = $row01['source'];
                    $subreason_code = $row01['reason'];
					
					$stmt14 = $dbh_ewap->prepare("SELECT r.reason_text FROM list_subreason s , list_reason r where s.reason_code= r.reason_code AND s.subreason_code=?");
				    $stmt14->execute(array($subreason_code));
				    if($stmt14->rowCount() != 0) {
						$row14 = $stmt14->fetch();
						$reason[$i]= $row14['reason_text'];
				    }
										
					$stmt15 = $dbh_ewap->prepare("SELECT calltype_text FROM list_calltype where calltype_code=?");
				    $stmt15->execute(array($call_type));
				    if($stmt15->rowCount() != 0) {
					   $row15 = $stmt15->fetch();
					   $type[$i]= $row15['calltype_text'];
				    }
					
                    $emp_type[$i] = $row01['emp_type'];                                      
 

                    if ($emp_type[$i] == "E")
                    {
                        $employee_type[$i] = "Employee";
                    } else if ($emp_type[$i] == "D")
                    {
                        $employee_type[$i] = "Dependent";
                    } else
                    {
                        $employee_type[$i] = "Employee";
                    }

                    
                                       #Get the vertical and process
                    $stmt04 = $dbh_ewap->prepare("SELECT * FROM emp_db WHERE emp_id=? LIMIT 1");
                    $stmt04->execute(array($emp_id[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                    $row04 = $stmt04->fetch();
                    $vertical = $row04['vertical'];
                    $process = $row04['process'];
                    }

                    echo '<tr>';
                    echo '<td>'.($i+1).'</td>';
                    echo '<td>' . $name[$i] . '</td>';
                    echo '<td>' . $cid[$i] . '</td>';
                    echo '<td>' . $emp_id[$i] . '</td>';
                    echo '<td>' . $yob[$i] . '</td>';
                    echo '<td>' . $mobile[$i] . '</td>';
                    echo '<td>' . $datetime[$i] . '</td>';
                    echo '<td>' . $gender[$i] . '</td>';
                    echo '<td>' . $vertical . '</td>';
                    echo '<td>' . $process . '</td>';
                    echo '<td>' . $location[$i] . '</td>';
                    echo '<td>' . $employee_type[$i] . '</td>';
                    echo '<td>' . $reason[$i] . '</td>';
                    echo '<td>' . $type[$i] . '</td>';
                    echo '</tr>';
                    $i++;
                }
            }
            ?>
        </table>
        <p><b>SCO Activity</b></p>
        <table style="cborder: 1px solid black;">
            <tr>
                <th></th>
                <th>Name</th>
                <th>User Id</th>
                <th>Employee Id</th>
                <th>YOB</th>
                <th>Mobile</th>
                <th>DateTime</th>
                <th>Gender</th>
                <th>Vertical</th>
                <th>Process</th>
                <th>Location</th>
                <th>Therapist</th>
                <th>Session</th>

            </tr>
            <?php
            # Connection to sohdbl
            $dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
            $dbh_sco->query("use sohdbl");


            $i = 0;
# Retrieving callers details
            $stmt02 = $dbh_sco->prepare("SELECT user_time_ssn.uid, corp_users_list.corp_id, user_time_ssn.ssn_endtime, user_time_ssn.ssn,view_user_profile.yob, view_user_profile.gender, view_user_profile.emp_id,view_user_profile.location,view_user_profile.vertical,view_user_profile.process 
FROM user_time_ssn,corp_users_list,view_user_profile 
WHERE user_time_ssn.uid= corp_users_list.uid AND view_user_profile.uid = corp_users_list.uid AND DATE(user_time_ssn.ssn_endtime)>= ? AND DATE(user_time_ssn.ssn_endtime)<= ? AND corp_users_list.corp_id = ? AND user_time_ssn.ssn != 'SSN0'ORDER BY DATE(user_time_ssn.ssn_endtime) ASC");
            $stmt02->execute(array($ewap_first_monthdate, $ewap_last_monthdate, $corp_id));
            if ($stmt02->rowCount() != 0)
            {
                while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
                    $uid[$i] = $row02['uid'];

                    $emp_id[$i] = $row02['emp_id'];
                    $yob[$i] = $row02['yob'];
                    $gender[$i] = $row02['gender'];

                    $vertical[$i] = $row02['vertical'];
                    $process[$i] = $row02['process'];
                    $location[$i] = $row02['location'];

                    $date = date_create($row02['ssn_endtime']);
                    $datetime[$i] = date_format($date, "d-M-y H:i");

                    $ssn[$i] = $row02['ssn'];
                    
                    #Get the user name
                    $stmt04 = $dbh_sco->prepare("SELECT name FROM user_login WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $encrypted_name[$i] = $row04['name'];
                        $name[$i] = decrypt($encrypted_name[$i], $encryption_key);
                    }

                    #Get the user therpist
                    $stmt04 = $dbh_sco->prepare("SELECT tid FROM user_therapist WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $tid[$i] = $row04['tid'];
                    }
                    #get the user mobile number
                    #Get the user therpist
                    $stmt04 = $dbh_sco->prepare("SELECT user_contact FROM user_comm_pref WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $mobile[$i] = $row04['user_contact'];
                    }

                    echo '<tr>';
                     echo '<td>' . ($i+1). '</td>';
                    echo '<td>' . $name[$i] . '</td>';
                    echo '<td>' . $uid[$i] . '</td>';
                    echo '<td>' . $emp_id[$i] . '</td>';
                    echo '<td>' . $yob[$i] . '</td>';
                    echo '<td>' . $mobile[$i] . '</td>';
                    echo '<td>' . $datetime[$i] . '</td>';
                    echo '<td>' . $gender[$i] . '</td>';
                    echo '<td>' . $vertical[$i] . '</td>';
                    echo '<td>' . $process[$i] . '</td>';
                    echo '<td>' . $location[$i] . '</td>';
                    echo '<td>' . $tid[$i] . '</td>';
                    echo '<td>' . $ssn[$i] . '</td>';


                    echo '</tr>';


                    $i++;
                }
            }
            ?>
        </table>
		<p><b>SCO Notes Activity</b></p>
        <table style="cborder: 1px solid black;">
            <tr>
                <th></th>
                <th>Name</th>
                <th>User Id</th>
                <th>Employee Id</th>
                <th>YOB</th>
                <th>Mobile</th>
                <th>DateTime</th>
                <th>Gender</th>
                <th>Vertical</th>
                <th>Process</th>
                <th>Location</th>
                <th>Therapist</th>
                <th>Session</th>

            </tr>
            <?php
            # Connection to sohdbl
            $dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
            $dbh_sco->query("use sohdbl");


            $i = 0;
# Retrieving callers details
            $stmt02 = $dbh_sco->prepare("SELECT thrp_notes.uid, corp_users_list.corp_id, thrp_notes.timestamp, thrp_notes.ssn,view_user_profile.yob, view_user_profile.gender, view_user_profile.emp_id,view_user_profile.location,view_user_profile.vertical,view_user_profile.process FROM thrp_notes,corp_users_list,view_user_profile WHERE thrp_notes.uid= corp_users_list.uid AND view_user_profile.uid = corp_users_list.uid AND DATE(thrp_notes.timestamp)>= ? AND DATE(thrp_notes.timestamp)<= ? AND corp_users_list.corp_id = ? AND thrp_notes.ssn != 'SSN0' ORDER BY DATE(thrp_notes.timestamp) ASC");
            $stmt02->execute(array($ewap_first_monthdate, $ewap_last_monthdate, $corp_id));
            if ($stmt02->rowCount() != 0)
            {
                while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
                    $uid[$i] = $row02['uid'];

                    $emp_id[$i] = $row02['emp_id'];
                    $yob[$i] = $row02['yob'];
                    $gender[$i] = $row02['gender'];

                    $vertical[$i] = $row02['vertical'];
                    $process[$i] = $row02['process'];
                    $location[$i] = $row02['location'];

                    $date = date_create($row02['timestamp']);
                    $datetime[$i] = date_format($date, "d-M-y H:i");

                    $ssn[$i] = $row02['ssn'];
                    
                    #Get the user name
                    $stmt04 = $dbh_sco->prepare("SELECT name FROM user_login WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $encrypted_name[$i] = $row04['name'];
                        $name[$i] = decrypt($encrypted_name[$i], $encryption_key);
                    }

                    #Get the user therpist
                    $stmt04 = $dbh_sco->prepare("SELECT tid FROM user_therapist WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $tid[$i] = $row04['tid'];
                    }
                    #get the user mobile number
                    #Get the user therpist
                    $stmt04 = $dbh_sco->prepare("SELECT user_contact FROM user_comm_pref WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $mobile[$i] = $row04['user_contact'];
                    }

                    echo '<tr>';
                     echo '<td>' . ($i+1). '</td>';
                    echo '<td>' . $name[$i] . '</td>';
                    echo '<td>' . $uid[$i] . '</td>';
                    echo '<td>' . $emp_id[$i] . '</td>';
                    echo '<td>' . $yob[$i] . '</td>';
                    echo '<td>' . $mobile[$i] . '</td>';
                    echo '<td>' . $datetime[$i] . '</td>';
                    echo '<td>' . $gender[$i] . '</td>';
                    echo '<td>' . $vertical[$i] . '</td>';
                    echo '<td>' . $process[$i] . '</td>';
                    echo '<td>' . $location[$i] . '</td>';
                    echo '<td>' . $tid[$i] . '</td>';
                    echo '<td>' . $ssn[$i] . '</td>';


                    echo '</tr>';


                    $i++;
                }
            }
            ?>
        </table>
        <p><b>Tranquil Activity</b></p>
        <table style="cborder: 1px solid black;">
            <tr>
                <th></th>
                <th>Name</th>
                <th>Tranquil Id</th>
                <th>Employee Id</th>
                <th>Access code</th>
                <th>Duration</th>
            </tr>
            <?php
            # Connection to sohdbl
           # Tranquil count
    $dbh_tq = new PDO($dsn_tranq, $tranq_user, $tranq_pass);
    $dbh_tq->query("use mndfapp");

            $i = 0;

            $stmt02 = $dbh_tq->prepare("SELECT t.uid, duration, access_code from tracks_activity t, user_premium u WHERE t.uid = u.uid AND u.corp_id =? AND t.time >= ? AND t.time <= ?");
            $stmt02->execute(array($corp_id,$ewap_first_monthdate, $ewap_last_monthdate));
			
            if ($stmt02->rowCount() != 0)
            {
                while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
                    $uid[$i] = $row02['uid'];

                    $duration[$i] = (int)($row02['duration']/60);
                    $access_code[$i] = $row02['access_code'];
                    
                    #Get the user name
                    $stmt04 = $dbh_sco->prepare("SELECT fullname FROM user_login WHERE uid=? LIMIT 1");
                    $stmt04->execute(array($uid[$i]));
                    if ($stmt04->rowCount() != 0)
                    {
                        $row04 = $stmt04->fetch();
                        $encrypted_name[$i] = $row04['fullname'];
                        $name[$i] = decrypt($encrypted_name[$i], $encryption_key);

                    }

                    
                    echo '<tr>';
                    echo '<td>' . ($i+1). '</td>';
                    echo '<td>' . $name[$i] . '</td>';
                    echo '<td>' . $uid[$i] . '</td>';
                    echo '<td></td>';
                    echo '<td>' . $access_code[$i] . '</td>';
                    echo '<td>' . $duration[$i] . ' mins </td>';
                    echo '</tr>';
                    $i++;
                }
				$i++;
            }
            ?>
        </table>

    </body>
</html>