<?php

function welcome_emp($to, $to_name, $emp_id) {
    require_once 'aws_sdk/aws-autoloader.php';
    require_once 'ses_plugin/autoloader.php';
    require 'ses_plugin/mail_credentials.php';

    $m = new SimpleEmailServiceMessage();


    ## ------------------------------- EMAIL PARAMETERS ---------------------------##
    $to = $to;
    $from = 'System <no-reply@silveroakhealth.com>';
    $subject = "Welcome to Silver Oak Health";
    $text = '';
    $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <title></title>
        <style type="text/css">
            html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
            @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
                *[class="table_width_100"] {
                    width: 96% !important;
                }
                *[class="border-right_mob"] {
                    border-right: 1px solid #dddddd;
                }
                *[class="mob_100"] {
                    width: 100% !important;
                }
                *[class="mob_center"] {
                    text-align: center !important;
                }
                *[class="mob_center_bl"] {
                    float: none !important;
                    display: block !important;
                    margin: 0px auto;
                }	
                .iage_footer a {
                    text-decoration: none;
                    color: #929ca8;
                }
                img.mob_display_none {
                    width: 0px !important;
                    height: 0px !important;
                    display: none !important;
                }
                img.mob_width_50 {
                    width: 40% !important;
                    height: auto !important;
                }
            }
            .table_width_100 {
                width: 680px;
            }
        </style>
    </head>
    <body style="padding: 0px; margin: 0px;">
	Dear ' . $to_name. ',
		
<p>Welcome to Silver Oak Health!</p><br/>
             
<p>As part of your on-boarding process you will be provided access to our internal tool called Mindnet.
You can access several internal systems using Mindnet, for running reports or entering data into the system.</p><br/>
			 
<p>Human Resources team or your Manager will orient you through these systems, based on your role. <br/>
Also, please make a note of your employee id <b>'. $emp_id . '</b>.</p><br/>
			 
<p>Please log in to Mindnet by clicking the below link and create the password. 
You will also be asked to create a 4 digit pin, which you will need to enter every time when you log into the system for security purpose. <br/><br/> 

<a href="https://silveroakhealth.com/mindnet">silveroakhealth.com/mindnet</a>
</p><br/>             

<p>If you have any questions or need support please write <a href="mailto:help@silveroakhealth.com">
help@silveroakhealth.com</a>/<a href="mailto:techsupport@silveroakhealth.com">techsupport@silveroakhealth.com</a></p>
<br/>             

Regards,<br/>             
Silver Oak Health HR Team
			</body>
</html>';
    ## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    $m->setSubject($subject);
	$m->setMessageFromString($text, $html);
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
    }
}
?>