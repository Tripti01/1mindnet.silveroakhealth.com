<?php
//include files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

#to check if emp_id from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['view_emp_id'])) {
    $view_emp_id = $_REQUEST['view_emp_id'];
} else {
    echo "Unable to fetch employee details. Error Code : EMPERR";
}
#to check if basic_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['basic_status'])) {
    $basic_status = $_REQUEST['basic_status'];
} else {
    $basic_status = 0;
}
#to check if dept_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['dept_status'])) {
    $dept_status = $_REQUEST['dept_status'];
} else {
    $dept_status = 0;
}
#to check if lev_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['lev_status'])) {
    $lev_status = $_REQUEST['lev_status'];
} else {
    $lev_status = 0;
}
#to check if personal_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['personal_status'])) {
    $personal_status = $_REQUEST['personal_status'];
} else {
    $personal_status = 0;
}
#to check if emergency_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['emergency_status'])) {
    $emergency_status = $_REQUEST['emergency_status'];
} else {
    $emergency_status = 0;
}
#to check if prvg_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['prvg_status'])) {
    $prvg_status = $_REQUEST['prvg_status'];
} else {
    $prvg_status = 0;
}

#database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

#to fetch the dept_name for the given emp_id
$stmt00 = $dbh->prepare("SELECT dept_name FROM dept_master,emp_dept WHERE dept_master.dept_id = emp_dept.dept_id AND emp_dept.emp_id=? LIMIT 1");
$stmt00->execute(array($view_emp_id));
if ($stmt00->rowCount() != 0) {
    $row00 = $stmt00->fetch();
    $dept_name = $row00['dept_name']; //dept_name
}

#to fetch all basic details from emp_login,emp_profile tables
$stmt01 = $dbh->prepare("SELECT first_name,last_name,email,gender,DOB,DOJ,job_title,DOL FROM emp_login,emp_profile WHERE emp_login.emp_id=? AND emp_login.emp_id = emp_profile.emp_id");
$stmt01->execute(array($view_emp_id));
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    $first_name = $row01['first_name']; //to fetch first name
    $last_name = $row01['last_name']; //to fetch last name
    $email = $row01['email']; //to fetch email
    $gender = $row01['gender']; //to fetch gender
    $dob = $row01['DOB']; //to fetch date of birth
    $doj = $row01['DOJ']; //to fetch date of joining
    $dol = $row01['DOL']; //to fetch date of leaving
    $job_title = $row01['job_title']; //to fetch job title
}

#to fetch all the list of all employees
$j = 0; //counter to fetch list of employees
$stmt02 = $dbh->prepare("SELECT first_name,last_name,emp_id FROM emp_login WHERE active=1 ");
$stmt02->execute();
if ($stmt02->rowCount() != 0) {
    while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
        $emp_name_list[$j] = $row02['first_name'] . " " . $row02['last_name']; //to fetch name of employees
        $emp_id_list[$j] = $row02['emp_id']; //to fetch the emp_id
        $j++;
    }
}

#to fetch the dr of the given emp_id 
$stmt03 = $dbh->prepare("SELECT first_name,last_name,emp_dr.dr_of_id FROM emp_dr,emp_login WHERE emp_dr.dr_of_id = emp_login.emp_id AND emp_dr.emp_id=? LIMIT 1 ");
$stmt03->execute(array($view_emp_id));
if ($stmt03->rowCount() != 0) {//if rowcount != 0
    $row03 = $stmt03->fetch();
    $dr_name = $row03['first_name'] . " " . $row03['last_name']; //to fetch the emp dr name
    $dr_id = $row03['dr_of_id']; //to fetch the emp dr id
} else {//if data is not set
    $dr_name = " "; //set date to blank
    $dr_id = " "; //set id to blank
}

#to fetch all leave constants year wise for the emp_id
#for 2017
$stmt04 = $dbh->prepare("SELECT cf,el_constant,cl_constant FROM emp_leave_constant WHERE emp_id=? AND `year`=? LIMIT 1");
$stmt04->execute(array($view_emp_id, '2017'));
if ($stmt04->rowCount() != 0) {
    $row04 = $stmt04->fetch();
    $cf_2017 = $row04['cf']; //carried forward
    $el_constant_2017 = $row04['el_constant']; //el_contant
    $cl_constant_2017 = $row04['cl_constant']; //cl_constant
} else {//set all to blank
    $cf_2017 = 0.00;
    $el_constant_2017 = 0.00;
    $cl_constant_2017 = 0.00;
}
#for 2018
$stmt05 = $dbh->prepare("SELECT cf,el_constant,cl_constant FROM emp_leave_constant WHERE emp_id=? AND `year`=? LIMIT 1");
$stmt05->execute(array($view_emp_id, '2018'));
if ($stmt05->rowCount() != 0) {
    $row05 = $stmt05->fetch();
    $cf_2018 = $row05['cf']; //carried forward
    $el_constant_2018 = $row05['el_constant']; //el_constant
    $cl_constant_2018 = $row05['cl_constant']; //cl_Constant
} else {//set all to blank
    $cf_2018 = 0.00;
    $el_constant_2018 = 0.00;
    $cl_constant_2018 = 0.00;
}
#for 2019
$stmt06 = $dbh->prepare("SELECT cf,el_constant,cl_constant FROM emp_leave_constant WHERE emp_id=? AND `year`=? LIMIT 1");
$stmt06->execute(array($view_emp_id, '2019'));
if ($stmt06->rowCount() != 0) {
    $row06 = $stmt06->fetch();
    $cf_2019 = $row06['cf']; //carried forward
    $el_constant_2019 = $row06['el_constant']; //el_constant
    $cl_constant_2019 = $row06['cl_constant']; //cl_constant
} else {//set all to blank
    $cf_2019 = 0.00;
    $el_constant_2019 = 0.00;
    $cl_constant_2019 = 0.00;
}

#to fetch a list of all prvgs and their names
$l = 0; //counter to fetch privilages
$stmt07 = $dbh->prepare("SELECT prvg_id,prvg_name, prvg_desc FROM prvg_details WHERE 1");
$stmt07->execute();
if ($stmt07->rowCount() != 0) {
    while ($row07 = $stmt07->fetch(PDO::FETCH_ASSOC)) {
        $prvg_id_list[$l] = $row07['prvg_id']; //to fetch all prvg_id
        $prvg_name_list[$l] = $row07['prvg_name']; //to fetch all prvg names
		$prvg_desc_list[$l] = $row07['prvg_desc'];
        $l++;
    }
}

#to fetch all dept names along with their ids
$m = 0; //counter to fetch all dept names
$stmt08 = $dbh->prepare("SELECT dept_id,dept_name FROM dept_master WHERE 1");
$stmt08->execute();
if ($stmt08->rowCount() != 0) {
    while ($row08 = $stmt08->fetch(PDO::FETCH_ASSOC)) {
        $dept_name_list[$m] = $row08['dept_name']; //to fetch all dept_name
        $dept_id_list[$m] = $row08['dept_id']; //to fetch all dept_id
        $m++;
    }
}

#to fetch all personal details for the given emp_id
$stmt09 = $dbh->prepare("SELECT phone,phone_alt,personal_email FROM emp_contact WHERE emp_id=? LIMIT 1");
$stmt09->execute(array($view_emp_id));
if ($stmt09->rowCount() != 0) {
    $row09 = $stmt09->fetch();
    $phone = $row09['phone']; //to fetch phone number
    $phone_alt = $row09['phone_alt']; //to fetch alternate phone number
    $personal_email = $row09['personal_email']; // to fetch a personal email
}

#to fetch address with the latest timestamp for the given emp_id
$stmt10 = $dbh->prepare("SELECT full_addr FROM emp_addr WHERE emp_id=? AND `timestamp`= (SELECT max(`timestamp`) FROM emp_addr WHERE emp_addr.emp_id=? LIMIT 1) LIMIT 1");
$stmt10->execute(array($view_emp_id, $view_emp_id));
if ($stmt10->rowCount() != 0) {
    $row10 = $stmt10->fetch();
    $addr = $row10['full_addr']; //to fetch address
} else {
    $addr = " ";
}

#to fetch emergency contact details of the given emp_id
$stmt11 = $dbh->prepare("SELECT * FROM emp_emergency WHERE emp_id=? LIMIT 1");
$stmt11->execute(array($view_emp_id));
if ($stmt11->rowCount() != 0) {
    $row11 = $stmt11->fetch();
    $emergency_name = $row11['emergency_name']; //emergency contact1 name
    $emergency_relation = $row11['emergency_relation']; //emergency contact1 relation with employee
    $emergency_contact = $row11['emergency_contact']; //emergency contact1 contact number
    $emergency_name2 = $row11['emergency_name2']; //emergency contact2 name
    $emergency_relation2 = $row11['emergency_relation2']; //emergency contact2 relation with employee
    $emergency_contact2 = $row11['emergency_contact2']; //emergency contact2 contact number
}

#to fetch a list of all prvgs assigned to emp_id
$n = 0; //counter to fetch prvgs assigned to employee
$stmt12 = $dbh->prepare("SELECT emp_prvg.prvg_id,prvg_details.prvg_name, prvg_details.prvg_desc FROM emp_prvg,prvg_details WHERE emp_prvg.prvg_id = prvg_details.prvg_id AND emp_prvg.emp_id=?");
$stmt12->execute(array($view_emp_id));
if ($stmt12->rowCount() != 0) {
    while ($row12 = $stmt12->fetch(PDO::FETCH_ASSOC)) {
        $assgnd_prvg_id_list[$n] = $row12['prvg_id']; //to fetch all prvg_id assigned to emp_id
        $assgnd_prvg_name_list[$n] = $row12['prvg_name']; //to fetch all prvg_names assigned to emp_id
		$assgnd_prvg_desc[$n] = $row12['prvg_desc']; //to fetch all prvg_names assigned to emp_id
        $n++;
    }
}


#on click of edit btn- basic tab
if (isset($_REQUEST['basic-btn'])) {
    if (isset($_REQUEST['email']) && !empty($_REQUEST['email']) && isset($_REQUEST['gender']) && !empty($_REQUEST['gender']) && isset($_REQUEST['first_name']) && !empty($_REQUEST['first_name']) && isset($_REQUEST['last_name']) && !empty($_REQUEST['last_name']) && isset($_REQUEST['dob']) && !empty($_REQUEST['dob']) && isset($_REQUEST['doj']) && !empty($_REQUEST['doj'])) {

        $new_first_name = $_REQUEST['first_name'];
        $new_last_name = $_REQUEST['last_name'];
        $new_email = $_REQUEST['email'];
        $new_gender = $_REQUEST['gender'];
        $new_dob = date("Y-m-d h:i:s", strtotime($_REQUEST['dob']));
        $new_doj = date("Y-m-d h:i:s", strtotime($_REQUEST['doj']));
        echo $new_dol = date("Y-m-d h:i:s", strtotime($_REQUEST['dol']));
        $view_emp_id = $_REQUEST['view_emp_id'];

        $dbh->beginTransaction();
        try {
#to update values in emp_profile
            $stmt13 = $dbh->prepare("UPDATE emp_profile SET gender=?,DOJ=?, DOB=?, DOL=? WHERE emp_id=?");
            $stmt13->execute(array($new_gender, $new_doj, $new_dob, $new_dol, $view_emp_id));
            #to update emp_login
            $stmt14 = $dbh->prepare("UPDATE emp_login SET name=?,last_name=?,email=? WHERE emp_id=?");
            $stmt14->execute(array($new_first_name, $new_last_name, $new_email, $view_emp_id));

            $basic_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $basic_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&basic_status=' . $basic_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com.[EDIT1000]");
    }
}

#on click of edit btn- dept tab
if (isset($_REQUEST['dept-btn'])) {
    if (isset($_REQUEST['emp_dept']) && !empty($_REQUEST['emp_dept']) && isset($_REQUEST['job_title']) && !empty($_REQUEST['job_title']) && isset($_REQUEST['dr']) && !empty($_REQUEST['dr'])) {

        $new_emp_dept = $_REQUEST['emp_dept'];
        $new_job_title = $_REQUEST['job_title'];
        $new_dr = $_REQUEST['dr'];
        $view_emp_id = $_REQUEST['view_emp_id'];

        $dbh->beginTransaction();
        try {
#to update values in emp_dr
            $stmt15 = $dbh->prepare("UPDATE emp_dr SET dr_of_id=? WHERE emp_id=?");
            $stmt15->execute(array($new_dr, $view_emp_id));
            #to update values in emp_dept    
            $stmt16 = $dbh->prepare("UPDATE emp_dept SET dept_id=? WHERE emp_id=?");
            $stmt16->execute(array($new_emp_dept, $view_emp_id));
            #to update values in emp_profile
            $stmt17 = $dbh->prepare("UPDATE emp_profile SET job_title=? WHERE emp_id=?");
            $stmt17->execute(array($new_job_title, $view_emp_id));

            $dept_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $dept_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&dept_status=' . $dept_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com.[EDIT1001]");
    }
}

#on click of edit btn- dept tab
if (isset($_REQUEST['dept-btn'])) {
    if (isset($_REQUEST['emp_dept']) && !empty($_REQUEST['emp_dept']) && isset($_REQUEST['job_title']) && !empty($_REQUEST['job_title']) && isset($_REQUEST['dr']) && !empty($_REQUEST['dr'])) {

        $new_emp_dept = $_REQUEST['emp_dept'];
        $new_job_title = $_REQUEST['job_title'];
        $new_dr = $_REQUEST['dr'];
        $view_emp_id = $_REQUEST['view_emp_id'];

        $dbh->beginTransaction();
        try {
#to update values in emp_dr
            $stmt15 = $dbh->prepare("UPDATE emp_dr SET dr_of_id=? WHERE emp_id=?");
            $stmt15->execute(array($new_dr, $view_emp_id));
            #to update values in emp_dept
            $stmt16 = $dbh->prepare("UPDATE emp_dept SET dept_id=? WHERE emp_id=?");
            $stmt16->execute(array($new_emp_dept, $view_emp_id));
            #to update values in emp_profile
            $stmt17 = $dbh->prepare("UPDATE emp_profile SET job_title=? WHERE emp_id=?");
            $stmt17->execute(array($new_job_title, $view_emp_id));

            $dept_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $dept_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&dept_status=' . $dept_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com.[EDIT1002]");
    }
}

#on click of edit btn- personal contact tab
if (isset($_REQUEST['personal-btn'])) {
    if (isset($_REQUEST['phone']) && !empty($_REQUEST['phone']) && isset($_REQUEST['phone_alt']) && !empty($_REQUEST['phone_alt']) && isset($_REQUEST['per_email']) && !empty($_REQUEST['per_email']) && isset($_REQUEST['new_addr']) && !empty($_REQUEST['new_addr'])) {

        $new_phone = $_REQUEST['phone'];
        $new_phone_alt = $_REQUEST['phone_alt'];
        $new_per_email = $_REQUEST['per_email'];
        $new_addr = $_REQUEST['new_addr'];
        $old_addr = $_REQUEST['addr'];
        $view_emp_id = $_REQUEST['view_emp_id'];
        date_default_timezone_set("Asia/Kolkata");
        $timestamp = date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s')));

        $dbh->beginTransaction();
        try {
#to update values in emp_contact
            $stmt18 = $dbh->prepare("UPDATE emp_contact SET phone=?,phone_alt=?,personal_email=? WHERE emp_id=?");
            $stmt18->execute(array($new_phone, $new_phone_alt, $new_per_email, $view_emp_id));

            //check if old address is equal to new addressor not.
            if ($old_addr != $new_addr) {
                //if old address is not equal new address, then insert into emp_addr
                $stmt19 = $dbh->prepare("INSERT INTO emp_addr VALUES (?,?,?,?,?)");
                $stmt19->execute(array('', $view_emp_id, $new_addr, $timestamp, ''));
            }

            $personal_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $personal_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&personal_status=' . $personal_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com.[EDIT1003]");
    }
}

#on click of edit btn- emergency contact tab
if (isset($_REQUEST['emergency-btn'])) {
    if (isset($_REQUEST['emergency_name']) && !empty($_REQUEST['emergency_name']) && isset($_REQUEST['emergency_contact']) && !empty($_REQUEST['emergency_contact']) && isset($_REQUEST['emergency_relation']) && !empty($_REQUEST['emergency_relation']) && isset($_REQUEST['emergency_name2']) && !empty($_REQUEST['emergency_name2']) && isset($_REQUEST['emergency_contact2']) && !empty($_REQUEST['emergency_contact2']) && isset($_REQUEST['emergency_relation2']) && !empty($_REQUEST['emergency_relation2'])) {

        $new_emergency_name = $_REQUEST['emergency_name'];
        $new_emergency_phone = $_REQUEST['emergency_contact'];
        $new_emergency_relation = $_REQUEST['emergency_relation'];
        $new_emergency_name2 = $_REQUEST['emergency_name2'];
        $new_emergency_phone2 = $_REQUEST['emergency_contact2'];
        $new_emergency_relation2 = $_REQUEST['emergency_relation2'];
        $view_emp_id = $_REQUEST['view_emp_id'];

        $dbh->beginTransaction();
        try {
#to update values in emp_emergency
            $stmt20 = $dbh->prepare("UPDATE emp_emergency SET emergency_name=?,emergency_contact=?,emergency_relation=?,emergency_name2=?,emergency_contact2=?,emergency_relation2=? WHERE emp_id=?");
            $stmt20->execute(array($new_emergency_name, $new_emergency_phone, $new_emergency_relation, $new_emergency_name2, $new_emergency_phone2, $new_emergency_relation2, $view_emp_id));

            $emergency_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $emergency_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&emergency_status=' . $emergency_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com.[EDIT1004]");
    }
}

#on click of edit btn- privilages tab
if (isset($_REQUEST['prvg-btn'])) {

    //to fetch all the checked boxes values
    $i = 0; //intialization of counter for check_array
    if (!empty($_REQUEST['check_list'])) {
        foreach ($_REQUEST['check_list'] as $check) {
            $check_array[$i] = $check; //the value set for each checked checkbox are stored in array
            $i++;
        }
    }

    $view_emp_id = $_REQUEST['view_emp_id'];

    //to begin transaction
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

#on update delete all the privilege and set again the checked values
    try {
        #to delete all privilege for that id 
        $stmt21 = $dbh->prepare("DELETE FROM emp_prvg WHERE emp_id = ?  ");
        $stmt21->execute(array($view_emp_id));

        #to insert all privileges marked in the database
        if (isset($check_array)) {
            for ($i = 0; $i < count($check_array); $i++) {
                $stmt22 = $dbh->prepare("INSERT INTO emp_prvg VALUES(?,?)");
                $stmt22->execute(array($view_emp_id, $check_array[$i]));
            }
        }
        $dbh->commit();

        $prvg_status = 1; // successful transaction
    } catch (PDOException $e) {
        $dbh->rollBack();
        $prvg_status = 2; //transaction failed
    }
    echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&prvg_status=' . $prvg_status . '"</script>';
}

#on click of edit btn- leave tab
if (isset($_REQUEST['lev-btn'])) {
    if (isset($_REQUEST['cf']) && !empty($_REQUEST['cf']) && isset($_REQUEST['el']) && !empty($_REQUEST['el']) && isset($_REQUEST['cl']) && !empty($_REQUEST['cl'])) {

        $cf = $_REQUEST['cf'];
        $cl = $_REQUEST['cl'];
        $el = $_REQUEST['el'];
        $year = $_REQUEST['year'];

        //to begin transaction
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->beginTransaction();

        try {
#to update values in emp_leave
            $stmt23 = $dbh->prepare("UPDATE emp_leave_constant SET cf=?,el_constant=?,cl_constant=? WHERE emp_id=? AND year=?");
            $stmt23->execute(array($cf, $el, $cl, $view_emp_id, $year));

            $dbh->commit();

            $lev_status = 1; // successful transaction
        } catch (PDOException $e) {
            $dbh->rollBack();
            $lev_status = 2; //transaction failed
        }
        echo '<script>window.top.location.href="edit_emp.php?view_emp_id=' . $view_emp_id . '&lev_status=' . $lev_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com.[EDIT1005]");
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>
        <style>
            .pull-right {
                float: right!important;
            }
            .tab_custom{
                border: 1px solid transparent;
            }
            .tbl_heading{
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-weight: 700;
                color: #666666;
            }
            .colon{
                text-align: left;
            }
			.prvg_desc{
				color:grey;
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <!--Default Tabs-->
                        <div class="col-md-10 col-sm-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary">
                                <div class="panel-heading" style="z-index:0;"><b>Edit Employee</b></div>
                                <div class="tab-container" >
                                    <div class="nav tab_custom" style="z-index:1;margin-top: -4%;">
                                        <ul class="nav nav-tabs pull-right">
                                            <li id="basic_tab" class="active"><a href="#basic" data-toggle="tab">Basic</a></li>
                                            <li id="dept_tab"><a href="#dept" data-toggle="tab">Department</a></li>
                                         <!--   <li id="leave_tab"><a href="#leave" data-toggle="tab">Leave</a></li> !-->
                                            <li id="contact_tab"><a href="#contact" data-toggle="tab">Contact</a></li>
                                            <li id="prvg_tab"><a href="#prvg" data-toggle="tab">Privileges</a></li>
                                            <!---li><a href="#mindnet" data-toggle="tab">Mindnet</a></li--->
                                        </ul>
                                    </div>
                                    <div class="tab-content" >
                                        <!--- Basic Contact Details tab--->
                                        <div id="basic" class="tab-pane active cont">
                                            <div class="row">
                                                <?php
                                                if ($basic_status == 0) {
                                                    //do nothing
                                                } else if ($basic_status == 1) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                                } else if ($basic_status == 2) {
                                                    //transaction failed
                                                    echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-8 panel">
                                                    <table style="width:100%;margin-top: 2%;">
                                                        <thead>
                                                        <th style="width:1%;"></th>
                                                        <th style="width:15%;"></th>
                                                        <th style="width:15%;"></th>
                                                        <th></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Name</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php echo $first_name . " " . $last_name; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Email</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php echo $email; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Gender</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php
                                                                        if ($gender == "F") {
                                                                            echo "Female";
                                                                        } else if ($gender == "M") {
                                                                            echo "Male";
                                                                        }
                                                                        ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Date of Birth</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php
                                                                        if ($dob == "0000-00-00") {
                                                                            echo " ";
                                                                        } else {
                                                                            echo date("d-m-Y", strtotime($dob));
                                                                        }
                                                                        ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Date of Joining</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php
                                                                        if ($doj == "0000-00-00") {
                                                                            echo " ";
                                                                        } else {
                                                                            echo date("d-m-Y", strtotime($doj));
                                                                        }
                                                                        ?></label></td>
                                                            </tr>
                                                            <tr>

                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Date of Leaving</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php
                                                                        if (isset($dol)) {
                                                                            echo date("d-m-Y", strtotime($dol));
                                                                        } else {
                                                                            echo " ";
                                                                        }
                                                                        ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <a data-toggle="modal" style="cursor: pointer;" data-target="#form-bp1"  type="button" data-backdrop="static" data-keyboard="false">
                                                                        Edit</a>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div >
                                            <!---Moadl - Form to fill Basic Contact Details --->
                                            <div id="form-bp1" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                <div class="modal-dialog custom-width">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="padding-bottom: 20px;padding-top: 20px;">
                                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                            <h3 class="modal-title">Edit Basic Details </h3>
                                                        </div>
                                                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="form-horizontal group-border-dashed basic-form">
                                                            <div class="modal-body" style="padding-bottom:40px;">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="control-label col-sm-1 tbl_heading">Name</label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-1"></div>
                                                                        <div class="col-sm-5">
                                                                            <input type="text" name="first_name" value="<?php echo $first_name; ?>" class="form-control ">
                                                                        </div><div class="col-sm-5">
                                                                            <input type="text" name="last_name" value="<?php echo $last_name; ?>" class="form-control ">
                                                                        </div></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-sm-1"></div>
                                                                        <label class="control-label col-sm-1 tbl_heading">Email</label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-1"></div>
                                                                        <div class="col-sm-10">
                                                                            <input type="email" name="email" value="<?php echo $email; ?>" class="form-control " onpaste="return false;" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                                        </div></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="control-label xs-pt-20 tbl_heading">Gender</label>
                                                                    <div>
                                                                        <div class="col-sm-1"></div>
                                                                        <div class="be-radio-icon inline">
                                                                            <input type="radio" <?php
                                                                            if ($gender == "F") {
                                                                                echo "checked";
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            ?> name="gender" id="rad1" value="F">
                                                                            <label for="rad1"><span class="mdi mdi-female"></span></label>
                                                                        </div>
                                                                        <div class="be-radio-icon inline">
                                                                            <input type="radio" name="gender" <?php
                                                                            if ($gender == "M") {
                                                                                echo "checked";
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            ?> id="rad2" value="M">
                                                                            <label for="rad2"><span class="mdi mdi-male-alt"></span></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class=" control-label tbl_heading"> Date of Birth</label>
                                                                    <div>
                                                                        <div class="col-sm-1"></div>
                                                                        <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker col-sm-10">
                                                                            <input type="text" value="<?php
                                                                            if ($dob == "0000-00-00") {
                                                                                echo " ";
                                                                            } else {
                                                                                echo date("d-m-Y", strtotime($dob));
                                                                            }
                                                                            ?>" name="dob" class="form-control col-sm-6"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="control-label tbl_heading"> Date of Joining </label>
                                                                    <div>
                                                                        <div class="col-sm-1"></div>
                                                                        <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker col-sm-10">
                                                                            <input type="text" name="doj" value="<?php
                                                                            if ($doj == "0000-00-00") {
                                                                                echo " ";
                                                                            } else {
                                                                                echo date("d-m-Y", strtotime($doj));
                                                                            }
                                                                            ?>" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1"></div>
                                                                    <label class="control-label tbl_heading"> Date of Leaving </label>
                                                                    <div>
                                                                        <div class="col-sm-1"></div>
                                                                        <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker col-sm-10">
                                                                            <input type="text" name="dol" value="<?php
                                                                            if (isset($dol)) {
                                                                                echo date("d-m-Y", strtotime($dol));
                                                                            } else {
                                                                                echo " ";
                                                                            }
                                                                            ?>" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <BR/><BR/><BR/><BR/><BR/><BR/>
                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="view_emp_id" value="<?php echo $view_emp_id; ?>"/>
                                                                    <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                    <input type="submit" value="Save" name="basic-btn" class="btn btn-primary"/>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--- Department Details tab--->
                                        <div id="dept" class="tab-pane cont">
                                            <div class="row">
                                                <?php
                                                if ($dept_status == 0) {
                                                    //do nothing
                                                } else if ($dept_status == 1) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                                } else if ($dept_status == 2) {
                                                    //transactiom failed
                                                    echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-8 panel">
                                                    <table style="width:100%;margin-top: 2%;">
                                                        <thead>
                                                        <th style="width:1%;"></th>
                                                        <th style="width:13%;"></th>
                                                        <th style="width:15%;"></th>
                                                        <th></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Department</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php echo $dept_name; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Job Title</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php echo $job_title; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><label class="control-label tbl_heading">Reports to</label></td>
                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                <td><label class="control-label"><?php echo $dr_name; ?></label></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="3"><a data-toggle="modal" style="cursor: pointer;" data-target="#form-bp_dept" type="button" data-backdrop="static" data-keyboard="false">Edit</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>    
                                            <!--- Department Details tab--->
                                            <div id="form-bp_dept" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                <div class="modal-dialog custom-width">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="padding-bottom: 20px;padding-top: 20px;">
                                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                            <h3 class="modal-title">Edit Department Details</h3>
                                                        </div>
                                                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="form-horizontal group-border-dashed dept-form">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label tbl_heading">Department</label>
                                                                    <div class="col-sm-6">
                                                                        <select class="form-control" name="emp_dept">
                                                                            <?php for ($i = 0; $i < count($dept_name_list); $i++) { ?>
                                                                                <option value="<?php echo $dept_id_list[$i]; ?>" <?php
                                                                                if ($dept_name_list[$i] == $dept_name) {
                                                                                    echo 'selected="selected"';
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                ?>><?php echo $dept_name_list[$i]; ?> </option>
                                                                                    <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label tbl_heading">Job Title</label>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" name="job_title" value="<?php echo $job_title; ?>" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label tbl_heading">Reports To</label>
                                                                    <div class="col-sm-6">
                                                                        <select class="form-control" name="dr">
                                                                            <?php for ($i = 0; $i < count($emp_name_list); $i++) { ?>
                                                                                <option value="<?php echo $emp_id_list[$i]; ?>" <?php
                                                                                if ($emp_id_list[$i] == $dr_id) {
                                                                                    echo 'selected="selected"';
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                ?>><?php echo $emp_name_list[$i]; ?> </option>
                                                                                    <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="view_emp_id" value="<?php echo $view_emp_id; ?>"/>
                                                                    <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                    <input type="submit" value="Save" name="dept-btn" class="btn btn-primary"/>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--- Leave Details tab--->
                                        <div id="leave" class="tab-pane">
                                            <?php
                                            if ($lev_status == 0) {
                                                //do nothing
                                            } else if ($lev_status == 1) {
                                                //successfully created employee
                                                echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                            } else if ($lev_status == 2) {
                                                //transactiom failed
                                                echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                            } else {
                                                
                                            }
                                            ?>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th style="text-align:center;">Year</th>
                                                        <th style="text-align:center;">Carried Forward From Last Year</th>
                                                        <th style="text-align:center;">Earned Leave</th>
                                                        <th style="text-align:center;">Casual Leave</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody style="text-align:center;">
                                                    <tr>
                                                        <td></td>
                                                        <td>2017</td>
                                                        <td><?php echo $cf_2017; ?></td>
                                                        <td><?php echo ($el_constant_2017 * 12) . ' (' . $el_constant_2017 . ' per month)' ?></td>
                                                        <td><?php echo ($cl_constant_2017 * 12) . ' (' . $cl_constant_2017 . ' per month)'; ?></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>2018</td>
                                                        <td><?php echo $cf_2018; ?></td>
                                                        <td><?php echo ($el_constant_2018 * 12) . ' (' . $el_constant_2018 . ' per month)' ?></td>
                                                        <td><?php echo ($cl_constant_2018 * 12) . ' (' . $cl_constant_2018 . ' per month)'; ?></td>
                                                        <td><a style="cursor: pointer;" data-toggle="modal" data-target="#form-bp_2018" type="button" data-backdrop="static" data-keyboard="false">Edit</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>2019</td>
                                                        <td><?php echo $cf_2019; ?></td>
                                                        <td><?php echo ($el_constant_2019 * 12) . ' (' . $el_constant_2019 . ' per month)' ?></td>
                                                        <td><?php echo ($cl_constant_2019 * 12) . ' (' . $cl_constant_2019 . ' per month)'; ?></td>
                                                        <td><a style="cursor: pointer;" data-toggle="modal" data-target="#form-bp_2019" type="button" data-backdrop="static" data-keyboard="false">Edit</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>	
                                            <?php
                                            $year_array = array('2018', '2019');
                                            for ($k = 0; $k < count($year_array); $k++) {
                                                ?>
                                                <!---Form to update leave--->
                                                <div id="form-bp_<?php echo $year_array[$k]; ?>" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                    <div class="modal-dialog custom-width">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                                <h3 class="modal-title">Edit Leave For Year - <?php echo $year_array[$k]; ?> </h3>
                                                            </div>
                                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed lev-form">
                                                                <div class="modal-body">
                                                                    <div class="row" style="margin-bottom:0.5%;">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-1"></div>
                                                                            <label class="col-sm-5 tbl_heading">Carried Forward from last year</label>
                                                                            <div class="col-sm-5">
                                                                                <input type="text" name="cf" value="<?php
                                                                                if ($year_array[$k] == '2018') {
                                                                                    echo $cf_2018;
                                                                                } else if ($year_array[$k] == '2019') {
                                                                                    echo $cf_2019;
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                ?>" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="margin-bottom:0.5%;">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-1"></div>
                                                                            <label class="col-sm-5 tbl_heading">Earned leave per month</label>
                                                                            <div class="col-sm-5">
                                                                                <input type="text" name="el" class="form-control" value="<?php
                                                                                if ($year_array[$k] == '2018') {
                                                                                    echo $cl_constant_2018;
                                                                                } else if ($year_array[$k] == '2019') {
                                                                                    echo $cl_constant_2019;
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-1"></div>
                                                                            <label class="col-sm-5 tbl_heading">Casual leave per month</label>
                                                                            <div class="col-sm-5">
                                                                                <input type="text" name="cl" class="form-control" value="<?php
                                                                                if ($year_array[$k] == '2018') {
                                                                                    echo $el_constant_2018;
                                                                                } else if ($year_array[$k] == '2019') {
                                                                                    echo $el_constant_2019;
                                                                                } else {
                                                                                    echo "";
                                                                                }
                                                                                ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="view_emp_id" value="<?php echo $view_emp_id; ?>"/>
                                                                    <input type="hidden" name="year" value="<?php echo $year_array[$k]; ?>"/>
                                                                    <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                    <input type="submit" name="lev-btn" class="btn btn-primary" value="Save">
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>      
                                            <?php } ?>
                                        </div>
                                        <!--- Contact Details tab--->
                                        <div id="contact" class="tab-pane">
                                            <div class="row">
                                                <?php
                                                if (($personal_status == 0) && ($emergency_status == 0)) {
                                                    //do nothing
                                                } else if (($personal_status == 1) || ($emergency_status == 1)) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                                } else if (($personal_status == 2) || ($emergency_status == 2)) {
                                                    //transactiom failed
                                                    echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading panel-heading-divider">Personal Contact Details</div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:1%;"></th>
                                                                        <th style="width:22%;"></th>
                                                                        <th style="width:5%;"></th>
                                                                        <th style="width:38%;"></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Contact</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $phone; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Alternate Contact</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $phone_alt; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Personal Email</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $personal_email; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Address</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $addr; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    <a style="cursor: pointer;" data-toggle="modal" data-target="#form-bp_personal" type="button" data-backdrop="static" data-keyboard="false">Edit</a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--- Personal Contact Details tab--->
                                                    <div id="form-bp_personal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                        <div class="modal-dialog custom-width">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                                    <h3 class="modal-title">Edit Contact</h3>
                                                                </div>
                                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed personal-form">
                                                                    <div class="modal-body" style="margin-left:2%;">
                                                                        <div class="row" style="margin-bottom:0.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Contact</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="phone" value="<?php echo $phone; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Alternate Contact</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="phone_alt" value="<?php echo $phone_alt; ?>"  class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Personal Email</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="per_email" value="<?php echo $personal_email; ?>"  class="form-control" onpaste="return false;" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Address</label>
                                                                                <div class="col-sm-8">
                                                                                    <textarea name="new_addr"  class="form-control"><?php echo $addr; ?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="view_emp_id" value="<?php echo $view_emp_id; ?>"/>
                                                                        <input type="hidden" name="addr" value="<?php echo $addr; ?>"/>
                                                                        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                        <input type="submit" name="personal-btn" class="btn btn-primary" value="Save">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>      
                                                </div>  
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading panel-heading-divider">Emergency Contact Details</div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:5%;"></th>
                                                                        <th style="width:22%;"></th>
                                                                        <th style="width:5%;"></th>
                                                                        <th style="width:38%;"></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td colspan="4"><label class="control-label tbl_heading">Emergency Contact 1</label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Name</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_name; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Relation</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_relation; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Contact Number</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_contact; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4"><label class="control-label tbl_heading">Emergency Contact 2</label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Name</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_name2; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Relation</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_relation2; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Contact Number</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_contact2; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <a data-toggle="modal" style="cursor: pointer;" data-target="#form-bp_emergency" type="button" data-backdrop="static" data-keyboard="false">Edit</a>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal- Form to add emergency contact details --->
                                                    <div id="form-bp_emergency" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                        <div class="modal-dialog custom-width">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                                    <h3 class="modal-title">Edit Contact</h3>
                                                                </div>
                                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed emergency-form">
                                                                    <div class="modal-body" style="margin-left:0.5%;">
                                                                        <div class="row" style="margin-bottom:0.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 tbl_heading">Emergency Contact 1</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Name</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" value="<?php echo $emergency_name; ?>" name="emergency_name" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Relation</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_relation" value="<?php echo $emergency_relation; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Contact Number</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_contact" value="<?php echo $emergency_contact; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 tbl_heading">Emergency Contact 2</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Name</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_name2" value="<?php echo $emergency_name2; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Relation</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_relation2" value="<?php echo $emergency_relation2; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Contact Number</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_contact2" value="<?php echo $emergency_contact2; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="view_emp_id" value="<?php echo $view_emp_id; ?>"/>
                                                                        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                        <input type="submit" name="emergency-btn" class="btn btn-primary" value="Save">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>      
                                                </div>
                                            </div>
                                        </div>
                                        <!--- Privilage Details tab--->
                                        <div id="prvg" class="tab-pane">
                                            <div class="row">
                                                <?php
                                                if (isset($assgnd_prvg_name_list)) {
                                                    
                                                } else {
                                                    echo '<div style="margin-left:18%;">No privilages assigned yet.</div>';
                                                }
                                                if ($prvg_status == 0) {
                                                    //do nothing
                                                } else if ($prvg_status == 1) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                                } else if ($prvg_status == 2) {
                                                    //transactiom failed
                                                    echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>
                                                <div class="col-md-2"></div>
                                                <div class="col-md-8">
                                                    <table style="width:100%;margin-top: 2%;">
                                                        <thead>
                                                        <th style="width:5%;"></th>
                                                        <th style="width:15%;"></th>
                                                        <th style="width:20%;"></th>
                                                        <th style="width:5%;"></th>
                                                        <th style="width:15%;"></th>
                                                        <th style="width:20%;"></th>
                                                        <th style="width:5%;"></th>
                                                        <th style="width:15%;"></th>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (isset($assgnd_prvg_name_list)) {


                                                                for ($l = 0; $l < count($assgnd_prvg_name_list); $l = $l + 3) {
                                                                    ?>
                                                                    <tr style="margin-bottom:5%;">
                                                                        <td><label class="control-label tbl_heading"><div class="icon"><span class="mdi mdi-check-square"></span></div>
                                                                            </label></td>
                                                                        <td><label class="control-label tbl_heading"><?php echo $assgnd_prvg_name_list[$l]; ?></label></td>
                                                                        <td></td> 
                                                                        <?php if (($l + 1) < count($assgnd_prvg_name_list)) { ?>
                                                                            <td><label class="control-label tbl_heading"><div class="icon"><span class="mdi mdi-check-square"></span></div>
                                                                                </label></td>
                                                                            <td><label class="control-label tbl_heading"><?php echo $assgnd_prvg_name_list[$l + 1]; ?></label></td>
                                                                        <?php } ?>
                                                                        <td></td>
                                                                        <?php if (($l + 2) < count($assgnd_prvg_name_list)) { ?>
                                                                            <td><label class="control-label tbl_heading"><div class="icon"><span class="mdi mdi-check-square"></span></div>
                                                                                </label></td>
                                                                            <td><label class="control-label tbl_heading"><?php echo $assgnd_prvg_name_list[$l + 2]; ?></label></td>
                                                                        <?php } ?>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                            ?><tr>
                                                                <td colspan="4">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <a data-toggle="modal" style="cursor: pointer;" data-target="#form-bp_prvg" type="button" data-backdrop="static" data-keyboard="false">Edit</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <!-- Modal- Form to add privilages --->
                                            <div id="form-bp_prvg" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                <div class="modal-dialog custom-width" style="width:900px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="padding-bottom: 20px;padding-top: 20px;">
                                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                            <h3 class="modal-title">Edit Privileges</h3>
                                                        </div>
                                                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed prvg-form" onsubmit='return test();' >
                                                            <div class="modal-body" style="padding-bottom:40px;">
                                                                <table style="width:100%;margin-top: 2%;">
                                                                    <thead>
                                                                    <th style="width:5;"></th>
                                                                    <th style="width:15;"></th>
                                                                    <th style="width:20;"></th>
                                                                    <th style="width:5;"></th>
                                                                    <th style="width:15;"></th>
                                                                    <th style="width:20;"></th>
                                                                    <th style="width:5;"></th>
                                                                    <th style="width:15;"></th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        for ($l = 0; $l < count($prvg_name_list); $l = $l + 2) {
                                                                            ?>
                                                                            <tr style="margin-bottom:5%;">
                                                                                <td>
																					<label class="control-label tbl_heading"><input id="check_<?php echo $l; ?>" value="<?php echo $prvg_id_list[$l]; ?>" type="checkbox" name="check_list[]" class="be-checkbox be-checkbox-color inline" <?php
                                                                                        if (isset($assgnd_prvg_id_list)) {
                                                                                            if (in_array($prvg_id_list[$l], $assgnd_prvg_id_list)) {// if privilege is set to 
                                                                                                echo "checked";
                                                                                            } else {
                                                                                                echo ""; //do nothing
                                                                                            }
                                                                                        }
                                                                                        ?> >
                                                                                    </label>
																				</td>
                                                                                <td style="padding-left:10px">
																					<label class="control-label tbl_heading"><?php echo $prvg_name_list[$l]; ?></label>
																					<br>
																					<div class="prvg_desc">(<?php echo $prvg_desc_list[$l]; ?>)</div>
																				</td>
                                                                                <td></td> 
                                                                                <?php if (($l + 1) < count($prvg_name_list)) { ?>
                                                                                    <td><label class="control-label tbl_heading"><input id="check_<?php echo $l + 1; ?>" value="<?php echo $prvg_id_list[$l + 1]; ?>" type="checkbox" name="check_list[]" class="be-checkbox be-checkbox-color inline" <?php
                                                                                            if (isset($assgnd_prvg_id_list)) {
                                                                                                if (in_array($prvg_id_list[$l + 1], $assgnd_prvg_id_list)) {// if privilege is set to 
                                                                                                    echo "checked";
                                                                                                } else {
                                                                                                    echo ""; //do nothing
                                                                                                }
                                                                                            }
                                                                                            ?>>
                                                                                        </label></td>
                                                                                    <td style="padding-left:10px">
																						<label class="control-label tbl_heading"><?php echo $prvg_name_list[$l + 1]; ?></label>
																						<br>
																						<div class="prvg_desc">(<?php echo $prvg_desc_list[$l + 1]; ?>)</div>
																					</td>
                                                                                <?php } ?>
                                                                                <td></td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                    <div class='error' style='color:red;text-align:center;'></div>  
                                                                </table>
                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="view_emp_id" value="<?php echo $view_emp_id; ?>"/>
                                                                    <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                    <input type="submit" value="Save" name="prvg-btn" id="prvg-btn" class="btn btn-primary"/>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!---div id="mindnet" class="tab-pane">
                                            <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                                        </div---->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script>
                                                            $(document).ready(function () {
                                                                //initialize the javascript
                                                                App.init();
                                                                App.formElements();
                                                            });
        </script>
        <script>
<?php if (($dept_status == 1) || ($dept_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#dept_tab").addClass("active");
                    $("#dept").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if (($lev_status == 1) || ($lev_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#dept_tab").removeClass("active");
                    $("#dept").removeClass("active");
                    $("#leave_tab").addClass("active");
                    $("#leave").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if (($personal_status == 1) || ($personal_status == 2) || ($emergency_status == 1) || ($emergency_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#dept_tab").removeClass("active");
                    $("#dept").removeClass("active");
                    $("#leave_tab").removeClass("active");
                    $("#leave").removeClass("active");
                    $("#contact_tab").addClass("active");
                    $("#contact").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if (($prvg_status == 1) || ($prvg_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#dept_tab").removeClass("active");
                    $("#dept").removeClass("active");
                    $("#leave_tab").removeClass("active");
                    $("#leave").removeClass("active");
                    $("#contact_tab").removeClass("active");
                    $("#contact").removeClass("active");
                    $("#prvg_tab").addClass("active");
                    $("#prvg").addClass("active");
                });
<?php } ?>
        </script>
        <!---Form validation---->
        <script>
            $(".basic-form").validate({
                rules: {
                    dob: {
                        required: true,
                    },
                    doj: {
                        required: true,
                    },
                    first_name: {
                        required: true,
                    },
                    last_name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    gender: {
                        required: true,
                    }
                },
                messages: {
                    dob: {
                        required: "Please Enter Date Of Birth ",
                    },
                    doj: {
                        required: "Please Enter Date Of Joining ",
                    },
                    first_name: {
                        required: "Please Enter First Name",
                    },
                    last_name: {
                        required: "Please Enter Last Name",
                    },
                    email: {
                        required: "Please Enter Email",
                        email: "Invalid Email ID",
                    },
                    gender: {
                        required: "Please Select ",
                    }
                }
            });
        </script>
        <script>
            $(".dept-form").validate({
                rules: {
                    emp_dept: {
                        required: true,
                    },
                    job_title: {
                        required: true,
                    },
                    dr: {
                        required: true,
                    }
                },
                messages: {
                    emp_dept: {
                        required: "Please Select Department ",
                    },
                    job_title: {
                        required: "Please Enter Job Title ",
                    },
                    dr: {
                        required: "Please Select ",
                    }
                }
            });
        </script>
        <script>
            $(".personal-form").validate({
                rules: {
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    phone_alt: {
                        required: true,
                        phoneUS: true
                    },
                    per_email: {
                        required: true,
                        email: true,
                    },
                    new_addr: {
                        required: true,
                    }
                },
                messages: {
                    phone: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    phone_alt: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    per_email: {
                        required: "Please enter email",
                        email: "Invalid Email ID",
                    },
                    new_addr: {
                        required: "Please enter address",
                    }
                }
            });
        </script>
        <script>
            $(".emergency-form").validate({
                rules: {
                    emergency_name: {
                        required: true,
                    },
                    emergency_contact: {
                        required: true,
                        phoneUS: true
                    },
                    emergency_relation: {
                        required: true,
                    },
                    emergency_name2: {
                        required: true,
                    },
                    emergency_contact2: {
                        required: true,
                        phoneUS: true
                    },
                    emergency_relation2: {
                        required: true,
                    }
                },
                messages: {
                    emergency_name: {
                        required: "Please enter name",
                    },
                    emergency_contact: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    emergency_relation: {
                        required: "Please enter relation",
                    },
                    emergency_name2: {
                        required: "Please enter name",
                    },
                    emergency_contact2: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    emergency_relation2: {
                        required: "Please enter relation",
                    },
                }
            });
        </script>
        <script>
            $(".lev-form").validate({
                rules: {
                    cf: {
                        required: true,
                    },
                    cl: {
                        required: true,
                    },
                    el: {
                        required: true,
                    },
                },
                messages: {
                    cf: {
                        required: "Please enter carried lorward",
                    },
                    cl: {
                        required: "Please enter casual leave",
                    },
                    el: {
                        required: "Please enter earned leave",
                    }
                }
            });
        </script>
        <script>
            function validateRadio(obj, error_no) {
                var result = 0;
                for (var i = 0; i < obj.length; i++) {
                    if (obj[i].checked == true) {
                        result = 1;

                        break;
                    }
                }
                return result;
            }

            function test() {
                var err = '';
                var q5 = document.getElementsByName("check_list[]");

                if (!validateRadio(q5, 5)) {
                    $("#error5").addClass("red-error");
                    err += '\n 2';
                }

                if (err.length) {
                    $("div.error").html("Please mark atleast one of options");
                    return false;
                } else {

                    return true;
                }
            }
        </script>          
    </body>
</html>