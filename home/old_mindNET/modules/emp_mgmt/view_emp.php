<?php
//incluing files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

//database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

#to fetch details of all employees who are active
$i = 0; //counter for employees
$stmt00 = $dbh->prepare("SELECT emp_login.emp_id,first_name,last_name,dept_name,email FROM emp_dept,dept_master,emp_login WHERE emp_dept.dept_id = dept_master.dept_id AND emp_login.emp_id=emp_dept.emp_id AND emp_login.active=? ORDER BY emp_login.emp_id");
$stmt00->execute(array('1'));
if ($stmt00->rowCount() != 0) {
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $emp_id_list[$i] = $row00['emp_id']; //to fetch all emp_id
        $emp_name_list[$i] = $row00['first_name'] . ' ' . $row00['last_name']; //to fetch employee name
        $dept_list[$i] = $row00['dept_name']; //to fetch dept_name
        $email_list[$i] = $row00['email']; //to fetch email
        $i++;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
           <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary panel-table">
                                <div class="panel-heading panel-heading-divider" style="padding-bottom: 15px;"><b>HR - View All Employees</b>
                                    <div style="float: right;"> <a href="add_emp.php"><button class="btn btn-space btn-primary"><i class="icon icon-left mdi mdi-account-add"></i>  Add Employee</button></div></a>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover" style="width:100%;" >
                                            <thead >
                                                <tr >
                                                    <th style="width:20%;">Employee Id</th>
                                                    <th style="width:25%;">Employee Name</th>
                                                    <th>Department Name</th>
                                                    <th>Email</th>
                                                    <th class="actions"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- to print all employee details in table-->
                                                <?php for ($i = 0; $i < count($emp_id_list); $i++) { ?>
                                                    <tr>
                                                        <td class="user-avatar"> <?php echo $emp_id_list[$i]; ?></td>
                                                        <td><?php echo $emp_name_list[$i]; ?></td>
                                                        <td><?php echo $dept_list[$i]; ?></td>
                                                        <td><?php echo $email_list[$i]; ?></td>
                                                        <td class="actions"><a style="cursor: pointer;" href="edit_emp.php?view_emp_id=<?php echo $emp_id_list[$i]; ?>">View</a></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script src="../../../../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();

                //Runs prettify
                prettyPrint();
            });
        </script>
    </body>
</html>