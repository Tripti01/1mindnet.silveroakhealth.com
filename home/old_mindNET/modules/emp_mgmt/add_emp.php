<?php
#include files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

//intialization
$return_status = 0;

#database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

#to fetch the the next emp_id in the series from cvalue
$stmt00 = $dbh->prepare("SELECT value FROM cvalue WHERE param=? LIMIT 1 ");
$stmt00->execute(array('emp_id'));
if ($stmt00->rowCount() != 0) {
    $row00 = $stmt00->fetch();
    $last_id = $row00['value']; //fetch cvalue
}
$next_id = $last_id + 1; //next emp_id
#to fetch all dept names along with their ids
$i = 0; //counter to fetch all dept ids and names
$stmt01 = $dbh->prepare("SELECT dept_id,dept_name FROM dept_master WHERE 1");
$stmt01->execute();
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $dept_name[$i] = $row01['dept_name']; //to fetch dept names
        $dept_id[$i] = $row01['dept_id']; //to fetch dept_ids
        $i++;
    }
}

#on click of button
if (isset($_REQUEST['submit-btn'])) {
    if (isset($_REQUEST['emp_first_name']) && !empty($_REQUEST['emp_first_name']) && isset($_REQUEST['emp_last_name']) && !empty($_REQUEST['emp_last_name']) && isset($_REQUEST['emp_email']) && !empty($_REQUEST['emp_email']) && isset($_REQUEST['emp_id']) && !empty($_REQUEST['emp_id'])) {

        include 'mail/welcome_emp.php';
        include 'functions/uid.php';
        include 'soh-config.php';

        $emp_first_name = $_REQUEST['emp_first_name'];
        $emp_last_name = $_REQUEST['emp_last_name'];
        $emp_id = $_REQUEST['emp_id'];
        $emp_email = $_REQUEST['emp_email'];
        $emp_dept = $_REQUEST['emp_dept'];

        #therapist details		
        $thrp_type = 'THRP';
        $tid = create_new_uid("THRP");
        $timestamp = date("Y-m-d H:i:s");
        $emp_fullname = $therapist_name = $emp_first_name . " " . $emp_last_name;


        $dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
        $dbh_sco->query("use mindnet");

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->beginTransaction();
        $dbh_sco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh_sco->beginTransaction();
        try {
            #to insert values in tables
            $stmt02 = $dbh->prepare("INSERT INTO emp_login VALUES (?,?,?,?,?,?,?)");
            $stmt02->execute(array($emp_id, $emp_first_name, $emp_last_name, $emp_email, '', '1', ''));

            $stmt03 = $dbh->prepare("INSERT INTO emp_dept VALUES (?,?)");
            $stmt03->execute(array($emp_id, $emp_dept));

            $stmt05 = $dbh->prepare("INSERT INTO emp_contact VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt05->execute(array($emp_id, '', '', '', '', '', '', '', '', ''));

            $stmt06 = $dbh->prepare("INSERT INTO emp_dr VALUES (?,?)");
            $stmt06->execute(array($emp_id, ' '));

            $stmt07 = $dbh->prepare("INSERT INTO emp_emergency VALUES (?,?,?,?,?,?,?)");
            $stmt07->execute(array($emp_id, '', '', '', '', '', ''));

            #leave constant for year 2018
            $stmt09 = $dbh->prepare("INSERT INTO emp_leave_constant VALUES (?,?,?,?,?,?,?)");
            $stmt09->execute(array($emp_id, '2018', '', '', '', '', ''));

            #leave constant for year 2019
            $stmt10 = $dbh->prepare("INSERT INTO emp_leave_constant VALUES (?,?,?,?,?,?,?)");
            $stmt10->execute(array($emp_id, '2019', '', '', '', '', ''));

            $stmt11 = $dbh->prepare("INSERT INTO emp_profile VALUES (?,?,?,?,?,?,?,?)");
            $stmt11->execute(array($emp_id, '', '', '', '', '', '', ''));

            $stmt12 = $dbh->prepare("INSERT INTO emp_prvg VALUES (?,?)");
            $stmt12->execute(array($emp_id, ''));

            #updating cvalue 
            $stmt13 = $dbh->prepare("UPDATE cvalue SET value=?");
            $stmt13->execute(array($emp_id));



            #INSERT INTO THERAPIST TABLES
            $stmt02 = $dbh_sco->prepare("INSERT INTO thrp_login VALUES(?,?,?,?);");
            $stmt02->execute(array($emp_email, $tid, $therapist_name, ''));

            $stmt03 = $dbh_sco->prepare("INSERT INTO thrp_profile VALUES (?,?,?,?,?)");
            $stmt03->execute(array($tid, '', '', '', ''));

            $stmt04 = $dbh_sco->prepare("INSERT INTO thrp_time_creation VALUES (?,?)");
            $stmt04->execute(array($tid, $timestamp));

            $stmt05 = $dbh_sco->prepare("INSERT INTO thrp_info VALUES (?,?,?,?,?)");
            $stmt05->execute(array($tid, '', '', '', ''));

            $stmt06 = $dbh_sco->prepare("INSERT INTO thrp_location VALUES (?,?,?,?,?,?,?,?)");
            $stmt06->execute(array($tid, '', '', '', '', '', '', ''));

            $stmt07 = $dbh_sco->prepare("INSERT INTO thrp_profile2 VALUES (?,?,?,?,?,?)");
            $stmt07->execute(array($tid, '', '', '', '', ''));

            $stmt08 = $dbh_sco->prepare("INSERT INTO thrp_type VALUES (?,?,?,?,?,?)");
            $stmt08->execute(array($tid, $thrp_type, '', '', '', ''));

            $stmt09 = $dbh_sco->prepare("INSERT INTO thrp_comm_pref VALUES (?,?,?,?,?,?,?)");
            $stmt09->execute(array($tid, $emp_email, '', '', '', '', ''));

            $dbh->commit();
            $dbh_sco->commit();

            $return_status = 1; //successfull entry to database

            welcome_emp($emp_email, $emp_fullname, $emp_id);
        } catch (PDOException $e) {
            $dbh->rollBack();
            echo $e->getMessage();
            $return_status = 2; //error occured while making entry to database
        }
    } else {
        //  something is wrong the client side validation 
        //  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. ADD_EMP_JS_MSNG");
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider"><b>Add An Employee</b></div>
                                    <div class="panel-body">
                                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                            <?php
                                            if ($return_status == 0) {
                                                //do nothing
                                            } else if ($return_status == 1) {
                                                //successfully created employee
                                                echo '<div class="alert alert-success"><b>Employee added successfully.Go to edit employee to provide other details.</b></div>';
                                            } else if ($return_status == 2) {
                                                //eeror occured
                                                echo '<div class="alert alert-danger">Transaction failed in between.Try again.</div>';
                                            } else {
                                                
                                            }
                                            ?>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Employee Id</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="<?php echo $next_id; ?>" name="emp_id">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Name</label>
                                                <div class="col-sm-3">
                                                    <input type="text" placeholder="Enter employee first name" class="form-control" name="emp_first_name">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" placeholder="Enter employee last name" class="form-control" name="emp_last_name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email</label>
                                                <div class="col-sm-6">
                                                    <input type="email" placeholder="Enter employee email" class="form-control" name="emp_email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Department</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control" name="emp_dept">
                                                        <?php
                                                        for ($i = 0; $i < count($dept_name); $i++) {
                                                            echo '<option value="' . $dept_id[$i] . '">' . $dept_name[$i] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row xs-pt-15">
                                                <div class="col-xs-6" style="margin-left:45%;">
                                                    <p class="text-right" >
                                                        <input type="submit" name="submit-btn" class="btn btn-space btn-primary" value="Add emplyoee">
                                                        <button type="reset" class="btn btn-space btn-default">Cancel</button>
                                                    </p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();

                //Runs prettify
                prettyPrint();
            });
        </script>
        <!---Form validation---->
        <script>
            $(".form-horizontal").validate({
                rules: {
                    emp_first_name: {
                        required: true,
                    },
                    emp_last_name: {
                        required: true,
                    },
                    emp_email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    emp_first_name: {
                        required: "Please enter employee first name"
                    },
                    emp_last_name: {
                        required: "Please enter employee last name"
                    },
                    emp_email: {
                        required: "Please enter employee email",
                        email: "Invalid email id"
                    }
                }

            });
        </script>
        <script type="text/javascript">
            //script to enter only alphabets in text box
            $("#name").focusout(function () {
                var name_text = document.getElementById("name").value;

                if ((name_text.match(/^[a-zA-Z\s]+$/) == null) && (name_text != ""))
                {
                    $("#err_name").html("Invalid name");

                }
            });
        </script>
    </body>
</html>