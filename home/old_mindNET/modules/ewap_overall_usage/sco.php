<?php
/*
  1.After the privilege set by superadmin he can choose the corporate on the slidebar then when he clicks create corporate he lands on this page.
  2.Coprporate admin  is allowed to insert the data into the given form and the credentials of new corporate is created.
  3.Mail could be sent with particular credentials.
 */

#file inclusion for various function happening in the ui
include 'ewap-config.php';
include 'soh-config.php';
require '../../if_loggedin.php';
include 'mindnet/functions/crypto_funtions.php';


$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use sohdbl");

# Select categories from database and display in the form
$stmt00 = $dbh->prepare("select ts.uid, ts.creation_time, ud.name, ud.mobile, ud.ref_value from user_time_creation ts, user_temp ud WHERE ts.uid =ud.uid AND ts.uid like 'INCR%';");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {   
		$uid[$i] = $row00['uid'];
		$taken_at[$i] = date('Y-m-d H:i', strtotime($row00['creation_time']));
		$name[$i] = decrypt($row00['name'], $encryption_key);
		$mobile[$i] = $row00['mobile'];
		$corp_id = $row00['ref_value'];		
		
		$stmt01 = $dbh->prepare("SELECT corp_fullname FROM corp_profile WHERE corp_id=?");
		$stmt01->execute(array($corp_id));
		if ($stmt01->rowCount() != 0) {
			$row01 = $stmt01->fetch();
			$corp_name[$i] = $row01['corp_fullname'];			
			if($corp_name[$i] == ''){
				$corp_name[$i] = $corp_id;
			}	
			
		}
        $i++;
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>EWAP Usage Report</title>
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
              <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>
            .ewap_cmp_name{
                padding:10px 25px;
                font-size:16px;
                color:#1078c4;
            }
            .corp-list{
                padding:8px;
                width:350px;
                margin-left:25px;
            }
			.radio{
				margin-left:25px;
			}
			.dates{
				width:45%;
				float:left;
				margin-left:25px;
				
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="background-image: url('<?php echo $img_loc; ?>'); background-size:cover;background-repeat: no-repeat;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-default panel-border-color panel-border-color-primary">
							<table style="width:98%;margin:10px" cellpadding="20" border=1>
								<tr>
									<th>Employee Name</th>
									<th>Mobile No.</th>
									<th>Company</th>
									<th>Creation Date</th>
								</tr>
								<?php for($i=0; $i<count($uid);$i++){ ?>
								<tr>
									<td><?php echo $name[$i]; ?></td>
									<td><?php echo $mobile[$i]; ?>.</td>
									<td><?php echo $corp_name[$i]; ?></td>
									<td><?php echo $taken_at[$i]; ?></td>
								</tr>								
								<?php } ?>
							</table>
						</div>
                    </div>
                </div>
            </div>

                                   
            <!-- jQuery  -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/detect.js"></script>
            <script src="assets/js/fastclick.js"></script>
            <script src="assets/js/jquery.slimscroll.js"></script>
            <script src="assets/js/jquery.blockUI.js"></script>
            <script src="assets/js/waves.js"></script>
            <script src="assets/js/jquery.nicescroll.js"></script>
            <script src="assets/js/jquery.scrollTo.min.js"></script>
            <script src="assets/js/jquery.core.js"></script>
            <script src="assets/js/jquery.app.js"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

            <!-- Include Date Range Picker -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

            <script>
                $(document).ready(function () {
                    var date_input = $('input[name="from_date"]'); //our date input has the name "date"
                    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'dd-mm-yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    });

                    var date_input = $('input[name="to_date"]'); //our date input has the name "date"
                    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'dd-mm-yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    })
                })
            </script>
    </body>
</html>
