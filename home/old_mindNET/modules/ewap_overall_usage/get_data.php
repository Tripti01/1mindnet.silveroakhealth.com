<?php
/*
  1.After the privilege set by superadmin he can choose the corporate on the slidebar then when he clicks create corporate he lands on this page.
  2.Coprporate admin  is allowed to insert the data into the given form and the credentials of new corporate is created.
  3.Mail could be sent with particular credentials.
 */

#file inclusion for various function happening in the ui
include 'ewap-config.php';
include 'soh-config.php';
require '../../if_loggedin.php';
include 'mindnet/functions/crypto_funtions.php';


    if (isset($_REQUEST['corporate_id']) && isset($_REQUEST['type'])) {
        $corporate_id = $_REQUEST['corporate_id'];
		$type = $_REQUEST['type'];

        if (isset($_REQUEST['from_date']) && isset($_REQUEST['to_date']) && $_REQUEST['from_date'] != "" && $_REQUEST['to_date'] != "") {
            $from_date = date('Y-m-d',strtotime($_REQUEST['from_date']));
            $to_date = date('Y-m-d',strtotime($_REQUEST['to_date']));
			
			$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
			$dbh->query("use $dbname");
			
			# Select categories from database and display in the form
			$stmt00 = $dbh->prepare("SELECT * FROM corp_login WHERE corp_id=?");
			$stmt00->execute(array($corporate_id));
			if ($stmt00->rowCount() != 0) {
				$row00 = $stmt00->fetch();
				$corp_name = decrypt($row00['name'], $encryption_key);
			}
			
			if($type=="LOGIN")
			{
				$stmt00 = $dbh_ewap->prepare("SELECT emp_id, id, datetime,type FROM visitor_log WHERE corp_id=? AND (type='LOGIN' || type='LOGIN_APP') AND DATE(datetime)>=? and DATE(datetime)<=?");
			}
		else if($type=="ARTICLE")
		{
			$stmt00 = $dbh_ewap->prepare("SELECT emp_id, id, datetime,type FROM visitor_log WHERE corp_id=? AND type='ARTICLE' AND DATE(datetime)>=? and DATE(datetime)<=?");
		}

			# Select categories from database and display in the form
			$stmt00->execute(array($corporate_id, $from_date, $to_date));
			if ($stmt00->rowCount() != 0) {
				$i = 0;
				while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
					$employee_id[$i] = $row00['emp_id'];
					$emp_datetime[$i] = date('d-M-Y H:i', strtotime($row00['datetime']));
					$type_ui[$i] = $row00['type'];
					
					if($row00['id'] != ''){
						$article_id[$i] = $row00['id'];	
						$stmt01 = $dbh_ewap->prepare("SELECT heading FROM article_content WHERE article_id=?");
						$stmt01->execute(array($article_id[$i]));
						if ($stmt01->rowCount() != 0) {
							$row01 = $stmt01->fetch();
							$article_heading[$i] = $row01['heading'];								
						}
					}
							
					$stmt01 = $dbh_ewap->prepare("SELECT emp_name, location, vertical, process FROM emp_db WHERE emp_id=? and corp_id=?");
					$stmt01->execute(array($employee_id[$i], $corporate_id));
					if ($stmt01->rowCount() != 0) {
						$row01 = $stmt01->fetch();
						$employee_name[$i] = $row01['emp_name'];	
						$emp_location[$i] = $row01['location'];	
						$emp_vertical[$i] = $row01['vertical'];	
						$emp_process[$i] = $row01['process'];								
					}else{
						$employee_name[$i] = '';	
						$emp_location[$i] = '';
						$emp_vertical[$i] = '';
						$emp_process[$i] = '';
					}
					$i++;
				}
			}
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>EWAP Portal Data</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
              <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
       <style>
          
			.dates{
				width:90%;
				float:left;
				
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="padding-top: 41px;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>
            <div style="margin:0px 50px;">
				<?php if(!isset($employee_id)){?>				
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<h3>No Data Exist of <B><?php echo $corp_name; ?></b></h3>
						</div>
					</div>
				<?php } else { ?>
				<div class="row">
						<div class="col-md-12 col-sm-12">
							<h3>Portal Data of <B><?php echo $corp_name; ?></b></h3> (No. of records : <?php echo count($employee_id)?>)
						</div>
				</div>
				 <BR/>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-default panel-border-color panel-border-color-primary">
							<table style="width:98%;margin:10px" cellpadding="20" border=1>
								<tr>
									<th>Employee Id</th>
									<th>Employee Name</th>
									<th>Vertical</th>
									<th>Process</th>
									<th>Location</th>
									<th>Datetime</th>
									<th>Type</th>
									<?php if(isset($article_id)){ ?>
										<th>Article</th>
									<?php } ?>
								</tr>
								<?php for($i=0; $i<count($employee_id);$i++){ ?>
								<tr>
									<td><?php echo $employee_id[$i]; ?></td>
									<td><?php echo $employee_name[$i]; ?></td>
									<td><?php echo $emp_vertical[$i]; ?>.</td>
									<td><?php echo $emp_process[$i]; ?></td>
									<td><?php echo $emp_location[$i]; ?></td>
									<td><?php echo $emp_datetime[$i]; ?></td>
									<td><?php echo $type_ui[$i]; ?></td>
									<?php if(isset($article_id)){ ?>
										<td><?php echo $article_heading[$i]; ?></td>
									<?php } ?>									
								</tr>								
								<?php } ?>
							</table>
						</div>
                    </div>
                </div>
				<?php } ?>
            </div>

                                   
            <!-- jQuery  -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/detect.js"></script>
            <script src="assets/js/fastclick.js"></script>
            <script src="assets/js/jquery.slimscroll.js"></script>
            <script src="assets/js/jquery.blockUI.js"></script>
            <script src="assets/js/waves.js"></script>
            <script src="assets/js/jquery.nicescroll.js"></script>
            <script src="assets/js/jquery.scrollTo.min.js"></script>
            <script src="assets/js/jquery.core.js"></script>
            <script src="assets/js/jquery.app.js"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

            <!-- Include Date Range Picker -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

            <script>
                $(document).ready(function () {
                    var date_input = $('input[name="from_date"]'); //our date input has the name "date"
                    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'dd-mm-yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    });

                    var date_input = $('input[name="to_date"]'); //our date input has the name "date"
                    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'dd-mm-yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    })
                })
            </script>
    </body>
</html>
