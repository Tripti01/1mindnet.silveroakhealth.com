<?php

require_once('ewap-config.php');

  
    if(isset($_REQUEST['from_date']) &&  isset($_REQUEST['to_date'])){
        $from_date = $_REQUEST['from_date'];
        $to_date = $_REQUEST['to_date'];
        
        # Date to display
        $first_date = date('d-M-y',strtotime($from_date));
        $last_date = date('d-M-y',strtotime($to_date));

        # Date to calculate
        $ewap_first_monthdate = date('Y-m-d',strtotime($from_date));
        $ewap_last_monthdate = date('Y-m-d',strtotime($to_date));
    }

    $pdf_name = "ewap_overall_usage.pdf";


    # Database Connection
    $i = 0;
    $count_tc = 0;
    $count_f2f = 0;
    $count_skype = 0;
	$count_chat = 0;
	$count_email = 0;
    $total_calls = 0;
    $overall_usage = '';
	$overall_count = '';
    # Retrieving callers details
    $stmt01 = $dbh_ewap->prepare("SELECT call_callers_notes.taken_at, call_callers_profile.corp_id, call_callers_profile.cid, call_callers_notes.source FROM call_callers_profile, call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_notes.taken_at >= ? AND call_callers_notes.taken_at <=? ORDER BY call_callers_notes.taken_at");
    $stmt01->execute(array($ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt01->rowCount() != 0) {
        while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
            $cid[$i] = $row01['cid'];
			$date = date_create($row01['taken_at']);
            $datetime[$i] = date_format($date, "d-M-y");
			$corp_id = $row01['corp_id'];
            $calltype_code = $row01['source'];
			
			$stmt11 = $dbh_ewap->prepare("SELECT corp_name FROM corp_profile WHERE corp_id=?");
			$stmt11->execute(array($corp_id));
			if ($stmt11->rowCount() != 0) {
				$row11 = $stmt11->fetch();
				$corp_name[$i] = $row11['corp_name'];
			}else{
				$corp_name[$i] = "";
			}
			
			$stmt02 = $dbh_ewap->prepare("SELECT calltype_text FROM list_calltype WHERE calltype_code=?");
			$stmt02->execute(array($calltype_code));
			if ($stmt02->rowCount() != 0) {
				$row02 = $stmt02->fetch();
				$calltype[$i] = $row02['calltype_text'];
			}else{
				$calltype[$i] = "";
			}
            
            $overall_usage.= "<tr><td>$datetime[$i]</td><td>$corp_name[$i]</td><td>$cid[$i]</td><td>$calltype[$i]</td></tr>";

            $i++;
        }
        
    }

	
	# Retrieving callers details
    $stmt01 = $dbh_ewap->prepare("SELECT call_callers_profile.corp_id, call_callers_notes.source, COUNT(call_callers_notes.source) as count_source FROM call_callers_profile, call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_notes.taken_at >= ? AND call_callers_notes.taken_at <=? GROUP BY call_callers_profile.corp_id, call_callers_notes.source");
    $stmt01->execute(array($ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt01->rowCount() != 0) {
        while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
            $count_type[$i] = $row01['count_source'];
			$corp_id = $row01['corp_id'];
            $calltype_code = $row01['source'];
			
			$stmt11 = $dbh_ewap->prepare("SELECT corp_name FROM corp_profile WHERE corp_id=?");
			$stmt11->execute(array($corp_id));
			if ($stmt11->rowCount() != 0) {
				$row11 = $stmt11->fetch();
				$corp_name[$i] = $row11['corp_name'];
			}else{
				$corp_name[$i] = "";
			}
						
			$stmt02 = $dbh_ewap->prepare("SELECT calltype_text FROM list_calltype WHERE calltype_code=?");
			$stmt02->execute(array($calltype_code));
			if ($stmt02->rowCount() != 0) {
				$row02 = $stmt02->fetch();
				$calltype[$i] = $row02['calltype_text'];
			}else{
				$calltype[$i] = "";
			}
			            
            $overall_count.= "<tr><td>$corp_name[$i]</td><td>$calltype[$i]</td><td>$count_type[$i]</td></tr>";

            $i++;
        }
        
    }


//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, '12px', PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'EWAP Usage Report', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 10);

//-------------------------------------------------------------------------------

$tbl3 = <<<EOD
<table cellspacing="0" cellpadding="10" border="0" width="100%">
    <tr>
        <td align="center"><b>EWAP overall usage report</b><br> ($from_date to $to_date)</td>
    </tr>
    <tr>
    	<td>
    		<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="20%"><b>Datetime</b></th>
					    <th width="40%"><b>Company</b></th>						
					    <th width="20%"><b>Caller ID</b></th>
						<th><b>Activity</b></th>
				    </tr>
				    $overall_usage
			    </tbody>
	    	</table>
    	</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tbl3, true, false, false, false, '');

$tbl4 = <<<EOD
<table cellspacing="0" cellpadding="10" border="0" width="100%">
    <tr>
        <td align="center"><b>EWAP Corporate Activity Count</b><br> ($from_date to $to_date)</td>
    </tr>
    <tr>
    	<td>
    		<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="40%"><b>Company</b></th>	
						<th width="40%"><b>Activity</b></th>
					    <th width="20%"><b>Count</b></th>
				    </tr>
				    $overall_count
			    </tbody>
	    	</table>
    	</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tbl4, true, false, false, false, '');

//Close and output PDF document
$pdf->Output($pdf_name, 'I');

//============================================================+
// END OF FILE
//============================================================+
