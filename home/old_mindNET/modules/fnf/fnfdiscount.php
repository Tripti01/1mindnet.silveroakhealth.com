<?php
#include files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

#to check if link_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['link_status'])) {
    $link_status = $_REQUEST['link_status'];
    $fnf_email = $_REQUEST['fnf_email'];
} else {
    $link_status = 0;
}

#on click of button
if (isset($_REQUEST['btn-send-discount-link'])) {

    if (isset($_REQUEST['fnf_name']) && isset($_REQUEST['fnf_email']) && isset($_REQUEST['fnf_message'])) {

        $fnf_name = $_REQUEST['fnf_name'];
        $fnf_email = $_REQUEST['fnf_email'];
        $fnf_message = $_REQUEST['fnf_message'];

        # These detaislw ill be saved in SOHDBL database and not mindnet database hence including tne relavent config file 
        include 'soh-config.php';
        require 'mindnet/functions/fnf_discount_code.php';
        require '../../../mail/fnf_discount_email.php';

        $unique_code = generate_fnf_discount_code();

        # Make an entry into database, y_fnf_ref table
        date_default_timezone_set("Asia/Kolkata");
        $link_sent_datetime = date("Y-m-d H:i:s");

        $dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
        $dbh_sco->query("use sohdbl");

        #Saving in database thus unique code
        $stmt20 = $dbh_sco->prepare("INSERT INTO user_reference VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt20->execute(array($unique_code, "EMP", $emp_id, $fnf_name, $fnf_email, '', '0', $link_sent_datetime, '', "FNF_25", '0', '0', '0'))or die(print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[CID-1001]."));

        #Sending email to the user using unqiue email discount link
       send_fnfdiscount_mail($fnf_email, $fnf_name, $fnf_message, $emp_name, $unique_code);
        $link_status = 1; //set status as 1
    } else {
        # Even after Jacript validation that all values need not be blank, Some blank values were submitted, throw a error
        echo 'Some Error Occured. Errror Code : FNF_FRM_01';
        die();
    }
     echo '<script>window.top.location.href="fnfdiscount.php?link_status=' . $link_status . '&fnf_email='.$fnf_email.'"</script>';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
           <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider"><b>Send Referral</b></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <?php
                                                if ($link_status == 1) {//on link sent successfully
                                                    echo '<div class="alert alert-success">Email sent to <b>' . $fnf_email . '</b></div>';
                                                } else {
                                                    //dont display anything
                                                }
                                                ?>
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label"> Name </label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control" name="fnf_name" placeholder="Your Friend/Family member name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label"> Email </label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control" name="fnf_email" placeholder="His/Her email address">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Your Name</label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control" disabled="" value="<?php echo $emp_name; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label" >Message</label>
                                                        <div class="col-md-10">
                                                            <span class="help-block"><small>Sample message is provided below. You can customise this if you want.</small></span>                                                     
                                                            <textarea class="form-control" name="fnf_message" rows="15" >
Dear Friend,  

Thank you for choosing Stress Control Online. Stress is a leading health challenge affecting millions of people in our country. Our online product is designed to help people cope with stress and improve their health and wellbeing. As users undergo the program at the privacy of their home, they will be assisted by a dedicated CBT trained coach who will give them, one-on-one guidance over the phone. 

Stress Control Online is an 8-week online program based on Cognitive Behavioural Therapy (CBT). Stress Control Online is based on the work of Dr. Jim White, whose CBT program has helped tens of thousands of people in the National Health Service, UK and around the world and now is available for Indian users.

You can click the below link to create your Stress Control Online account. Since I have referred you to this program, you will receive 25% discount.
                            
If you have any questions, you can reach our Psychological Advisor at 080-6547 2100 or coach@stresscontrolonline.com


                                                            </textarea>
                                                            <span class="help-block"><small>PLEASE NOTE : Unique Discount link will be generated and added automatically.</small></span>  
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-b-0">
                                                        <div class="col-sm-offset-6 col-sm-9">
                                                            <button type="submit" class="btn btn-info waves-effect waves-light" name="btn-send-discount-link"> Send Discount Link </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();

                //Runs prettify
                prettyPrint();
            });
        </script>
        <script>
            $(".close").click(function () {
                window.top.location.reload();
            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    fnf_name: {
                        required: true,
                    },
                    fnf_email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    fnf_name: {
                        required: "Please enter the name"
                    },
                    fnf_email: {
                        required: "Please enter the email",
                        email: "Invalid Email ID"
                    }
                }
            });
        </script>
    </body>
</html>