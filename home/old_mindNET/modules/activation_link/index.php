<?php
include 'soh-config.php';
include '../../if_loggedin.php';
include 'mindnet-host.php';

	# Connection to sohdbl
	$dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
	$dbh_sco->query("use sohdbl");
 
	$status = 0;
	$email = "";
	if(isset($_REQUEST['email']) && $_REQUEST['email'] != ''){
	 
		$email = $_REQUEST['email'];
	 
		$stmt02 = $dbh_sco->prepare("SELECT activated, token FROM user_temp WHERE email = ?");
		$stmt02->execute(array($email));
		if ($stmt02->rowCount() != 0) {
			
			$row02 = $stmt02->fetch();
			if($row02['activated'] != 1){
				
				$token = $row02['token'];
				if( $token != ""){
					$status = 1;
				}else{
					$status = 4;
				}
			}else{
				$status = 2;
			}
		}else{			
			$status = 3;			
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Send SCO Activation Link </div>
                                    <div class="panel-body">
                                        <div Style="text-align: center;font-size: 15px;">
                                            <div class="confirm-msg">  												
												<?php if($status == 3){
													echo '<div style="color:red">'.$email.' is not a Stress Control Online user</div>';
												}else if($status == 2){
													echo '<div style="color:green">'.$email.' is already an <b>Active</b> Stress Control Online user</div>';
												}else if($status == 4){
													echo '<div style="color:red">Activation email for this user cannot be generated automatically.<br/> Please create a support ticket to generate email for this user.</div>';
												}?>
                                            </div>
                                        </div>
                                        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Enter User Email </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control input-sm"  name="email" placeholder="User Email">
                                                </div>
												<div class="col-md-2 col-xs-12">
                                                    <input type="submit" class="btn btn-info waves-effect waves-light" value="Submit" name="btn-create-user"/>
                                                </div>
                                            </div>
                                            <div class="form-group m-b-0" style="text-align: center;">
                                                 
                                            </div>
                                        </form>
										<?php if($status == 1){?>
											<div class="row">
												<label class="col-md-3 col-xs-12 control-label" style="text-align:right;"> <b>Activation Email<b> </label>
													<div class="col-md-8 col-xs-12">
														<div style="height:250px;width:700px;padding:20px;border:1px solid black;">
Dear User,<br/><br/>

Please use the below link to activate your account. 
<a href="https://stresscontrolonline.com/register/individual/confirm.php?token=<?php echo $token;?>" target="_BLANK">https://stresscontrolonline.com/register/individual/confirm.php?token=<?php echo $token;?> </a>
<br/><br/>
Once your account is activated. You can login directly by visiting <a href="http://stresscontrolonline.com/login" target="_BLANK">http://stresscontrolonline.com/login</a>
but entering the password chosen by you while creating the account. 
<br/><br/>														 
Regards,<br/>
Silver Oak Health Team
														</div>
													</div>
											</div>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                    </div>
					
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: "Please enter the email",
                        email: "Invalid Email ID"
                    }
                }
            });
        </script>     
    </body>
</html>