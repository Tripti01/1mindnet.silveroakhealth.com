<?php

require_once('ewap-config.php');
require_once('soh-config.php');
require_once('tranquil-config.php');

function get_ssn_fullname($ssn){
	switch ($ssn) {
        case "SSN0":
            return "Introduction";
            break;
        case "SSN1":
            return "Session 1";
            break;
        case "SSN2":
            return "Session 2";
            break;
        case "SSN3":
            return "Session 3";
            break;
        case "SSN4":
            return "Session 4";
            break;
        case "SSN5":
            return "Session 5";
            break;
        case "SSN6":
            return "Session 6";
            break;
        case "SSN7":
            return "Session 7";
            break;
        case "SSN8":
            return "Session 8";
            break;
			case "CBTSTART":
            return "CBT Start";
            break;
			case "CBTEND":
            return "CBT End";
            break;
        default;
    }
}


    $corp_id = 'CORP10007118';
    
    $dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
    $dbh->query("use sohdbl");
    
    $stmt01 = $dbh->prepare("SELECT * FROM corp_profile WHERE corp_id=?");
    $stmt01->execute(array($corp_id));
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        $corp_name = $row01['corp_fullname'];
        $address = $row01['address'];
    }else{
        $corp_name = "";
		$address = "";
    }

    $pdf_name = $corp_name."_ewap_usage_report.pdf";

    if(isset($_REQUEST['from_date']) &&  isset($_REQUEST['to_date'])){
        $from_date = $_REQUEST['from_date'];
        $to_date = $_REQUEST['to_date'];
        
        # Date to display
        $first_date = date('d-M-y',strtotime($from_date));
        $last_date = date('d-M-y',strtotime($to_date));

        # Date to calculate
        $ewap_first_monthdate = date('Y-m-d',strtotime($from_date));
        $ewap_last_monthdate = date('Y-m-d',strtotime($to_date));
    }else{
		die("From and To Date is missing!");
	}



    # Database Connection
    $i = 0;
    $count_tc = 0;
    $count_f2f = 0;
    $count_skype = 0;
	$count_chat = 0;
    $total_calls = 0;
    $telephonic = '';
    $face2face = '';
    $skype = '';
	$chats = '';

    
	$stmt01 = $dbh_ewap->prepare("SELECT count(emp_id) as employees FROM emp_db WHERE corp_id=?");
    $stmt01->execute(array($corp_id));
    if ($stmt01->rowCount() != 0) {
		$row01 = $stmt01->fetch();
		$total_employees = $row01['employees'];
	}
	
    # Retrieving callers details
    $stmt01 = $dbh_ewap->prepare("SELECT call_callers_profile.cid, call_callers_profile.emp_id, call_callers_notes.source, call_callers_notes.taken_at FROM scodd.call_callers_profile,call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_profile.corp_id=? AND DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=? ORDER BY taken_at ASC");
    $stmt01->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt01->rowCount() != 0) {
        while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
            $cid[$i] = $row01['cid'];
            $date = date_create($row01['taken_at']);
            $datetime[$i] = date_format($date, "d-M-y H:i");
            $type[$i] = $row01['source'];
            $emp_id = $row01['emp_id'];
            
            # Find out the empid details. 
            $stmt02 = $dbh_ewap->prepare("SELECT location FROM call_callers_profile WHERE emp_id=? AND corp_id=? LIMIT 1");
            $stmt02->execute(array($emp_id, $corp_id));
            if ($stmt02->rowCount() != 0) {
                $row02 = $stmt02->fetch();

            	$location_calls[$i] = $row02['location'];
            }else{
            	$location_calls[$i] = '';
            }

            $stmt02 = $dbh_ewap->prepare("SELECT vertical, process FROM emp_db  WHERE corp_id=? and emp_id=? LIMIT 1");
            $stmt02->execute(array($corp_id, $emp_id));
            if ($stmt02->rowCount() != 0) {
                $row02 = $stmt02->fetch();

                $vertical[$i] = $row02['vertical'];
                $process[$i] = $row02['process'];
            }else{
                $vertical[$i] = '';
                $process[$i] = '';
            }

            if ($type[$i] == 'sc' || $type[$i] == 'dc') {
                $count_tc++;
                $telephonic  .= "<tr><td>$cid[$i]</td><td>$datetime[$i]</td><td>$location_calls[$i]</td><td>$vertical[$i]</td><td>$process[$i]</td></tr>";

            } else if ($type[$i] == 'f2f-onsite' || $type[$i] == 'f2f-soh' || $type[$i] == 'f2f-affiliate') {
                $count_f2f++;
                $face2face  .= "<tr><td>$cid[$i]</td><td>$datetime[$i]</td><td>$location_calls[$i]</td><td>$vertical[$i]</td><td>$process[$i]</td></tr>";
            } else if ($type[$i] == 'skype') {
                $count_skype++;
                $skype  .= "<tr><td>$cid[$i]</td><td>$datetime[$i]</td><td>$location_calls[$i]</td><td>$vertical[$i]</td><td>$process[$i]</td></tr>";
            } else if ($type[$i] == 'chat') {
                $count_chat++;
                $chats  .= "<tr><td>$cid[$i]</td><td>$datetime[$i]</td><td>$location_calls[$i]</td><td>$vertical[$i]</td><td>$process[$i]</td></tr>";
            } else {
                #Do nothing
            }


            $i++;
        }
        
        
        $total_calls = ($count_f2f + $count_skype + $count_tc + $count_chat);
    }

    # Connection to sohdbl
    $dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
    $dbh_sco->query("use sohdbl");
	 
    $j = 0;
    $total_sco_calls = 0;
    $sco_calls ='';
    # Retrieving ewap sco user details -- SCO calls
    $stmt02 = $dbh_sco->prepare("SELECT thrp_notes.uid,thrp_notes.timestamp,thrp_notes.ssn,user_profile2.location,user_profile2.vertical,user_profile2.process FROM thrp_notes,corp_users_list,user_profile2 WHERE thrp_notes.uid= corp_users_list.uid AND user_profile2.uid = corp_users_list.uid AND DATE(thrp_notes.timestamp)>= ? AND DATE(thrp_notes.timestamp)<= ? AND corp_users_list.corp_id = ? AND thrp_notes.ssn != 'SSN0' ORDER BY DATE(thrp_notes.timestamp) ASC");
    $stmt02->execute(array($ewap_first_monthdate, $ewap_last_monthdate, $corp_id));
    if ($stmt02->rowCount() != 0) {
        while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
            $uid[$j] = $row02['uid'];
            $date = date_create($row02['timestamp']);
            $thrp_calls[$j] = date_format($date, "d-M-y");
            $ssn_sco_calls[$j] = get_ssn_fullname($row02['ssn']);
            $location_sco_calls[$j] = $row02['location'];
            $vertical_sco_calls[$j] = $row02['vertical'];
            $process_sco_calls[$j] = $row02['process'];

            $sco_calls  .= "<tr><td>$uid[$j]</td><td>$thrp_calls[$j]</td><td>$ssn_sco_calls[$j]</td><td>$location_sco_calls[$j]</td><td>$vertical_sco_calls[$j]</td><td>$process_sco_calls[$j]</td></tr>";

            $j++;
        }
        $total_sco_calls = $j;
    }


    # Total Counselling Calls 
    $total_counsellling_calls = $total_calls + $total_sco_calls;


    $j = 0;
    $total_sco = 0;
    $sco_ssn = '';
    # Retrieving ewap sco user details --  SCO Session
    $stmt02 = $dbh_sco->prepare("SELECT user_time_ssn.uid,user_time_ssn.ssn_endtime,user_time_ssn.ssn,user_profile2.location,user_profile2.vertical,user_profile2.process FROM user_time_ssn,corp_users_list,user_profile2 WHERE user_time_ssn.uid= corp_users_list.uid AND user_profile2.uid = corp_users_list.uid AND DATE(user_time_ssn.ssn_endtime)>= ? AND DATE(user_time_ssn.ssn_endtime)<= ? AND corp_users_list.corp_id = ? AND user_time_ssn.ssn != 'SSN0' ORDER BY DATE(user_time_ssn.ssn_endtime) ASC");
    $stmt02->execute(array($ewap_first_monthdate, $ewap_last_monthdate, $corp_id));
    if ($stmt02->rowCount() != 0) {
        while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
            $uid[$j] = $row02['uid'];
            $date = date_create($row02['ssn_endtime']);
            $ssn_endtime[$j] = date_format($date, "d-M-y");
            $ssn[$j] = get_ssn_fullname($row02['ssn']);
            $location_sco[$j] = $row02['location'];
            $vertical_sco[$j] = $row02['vertical'];
            $process_sco[$j] = $row02['process'];

            $sco_ssn  .= "<tr><td>$uid[$j]</td><td>$ssn_endtime[$j]</td><td>$ssn[$j]</td><td>$location_sco[$j]</td><td>$vertical_sco[$j]</td><td>$process_sco[$j]</td></tr>";


            $j++;
        }
        $total_sco = $j;
    }
	
	$j = 0;
	$total_user_signedup = 0;
	$sco_new_reg = '';
    # Retrieving ewap sco user destails -- SCO user Signup
    $stmt02 = $dbh_sco->prepare("SELECT user_time_creation.uid,user_time_creation.creation_time,user_profile2.location,user_profile2.vertical,user_profile2.process FROM user_time_creation,corp_users_list,user_profile2 WHERE user_time_creation.uid= corp_users_list.uid AND user_profile2.uid = corp_users_list.uid AND DATE(user_time_creation.creation_time)>= ? AND DATE(user_time_creation.creation_time)<= ? AND corp_users_list.corp_id = ? ORDER BY DATE(user_time_creation.creation_time) ASC");
    $stmt02->execute(array($ewap_first_monthdate, $ewap_last_monthdate, $corp_id));
    if ($stmt02->rowCount() != 0) {
		$total_user_signedup = $stmt02->rowCount();
        while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
            $new_reg_uid[$j] = $row02['uid'];
            $date = date_create($row02['creation_time']);
            $creation_time[$j] = date_format($date, "d-M-y");
            $new_reg_location[$j] = $row02['location'];
            $new_reg_vertical[$j] = $row02['vertical'];
            $new_reg_process[$j] = $row02['process'];

            $sco_new_reg .= "<tr><td>$new_reg_uid[$j]</td><td>$creation_time[$j]</td><td>$new_reg_location[$j]</td><td>$new_reg_vertical[$j]</td><td>$new_reg_process[$j]</td></tr>";

            $j++;
        }        
    }

    $total_others = 0;
    $others = '';
	# Retriving Actiivity of corporate
    $stmt00 = $dbh_sco->prepare("SELECT * FROM corp_activity_track1,corp_activity_track2 WHERE corp_activity_track1.activity_id = corp_activity_track2.activity_id AND corp_activity_track1.corp_id=? AND DATE(held_on) >= ? AND DATE(held_on)<=? ORDER BY corp_activity_track1.held_on");
    $stmt00->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt00->rowCount() != 0) {
      $i = 0;
      while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
         $activity_id[$i] = $row00['activity_id'];
         $ac_corp_id[$i] = $row00['corp_id'];
         $held_on[$i] = date("d-M-Y", strtotime($row00['held_on']));
         $activity_type[$i] = $row00['activity_type'];
         $country[$i] = $row00['country']; 
         $state[$i] = $row00['state']; 
         $city[$i] = $row00['city']; 
         $additional_details[$i] = $row00['add_details']; 
         $location[$i] = $city[$i];

         $stmt01 = $dbh_sco->prepare("SELECT activity_name FROM corp_activity WHERE activity_type=?");
         $stmt01->execute(array($activity_type[$i]));
         if ($stmt01->rowCount() != 0) {
            $row01 = $stmt01->fetch();
            $ac_name[$i] = $row01['activity_name'];
        }

        $others .= "<tr><td>$held_on[$i]</td><td>$ac_name[$i]</td><td>$location[$i]</td><td>$additional_details[$i]</td></tr>";

        $i++;
    }

    $total_others = count($activity_id);
}
    
    # Annexure Details
    $f_count = 0;
    $m_count = 0;
    $total_annex_calls = 0;
    $stmt11 = $dbh_ewap->prepare("SELECT call_callers_profile.gender, count(call_callers_profile.cid) as g_count FROM scodd.call_callers_profile,call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_profile.corp_id=? AND DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=? GROUP BY gender");
    $stmt11->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt11->rowCount() != 0) {
        while ($row11 = $stmt11->fetch(PDO::FETCH_ASSOC)) {
           if($row11['gender'] == 'F'){
                $f_count = $row11['g_count'];
           }else if( $row11['gender'] == 'M'){
                $m_count = $row11['g_count'];
           }
        }

        $total_annex_calls = $f_count + $m_count;
    }

    $k =0;
    $annex_location ='';
    $stmt12 = $dbh_ewap->prepare("SELECT  call_callers_profile.location, count(call_callers_profile.cid) as l_count FROM scodd.call_callers_profile,call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_profile.corp_id=? AND DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=? GROUP BY location");
    $stmt12->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt12->rowCount() != 0) {
        while ($row12 = $stmt12->fetch(PDO::FETCH_ASSOC)) {
          	$seq = ($k+1);
          	$loc = $row12['location'];
          	$l_count = $row12['l_count'];
            $annex_location .= "<tr><td colspan='2'>$seq. $loc</td><td align=right>$l_count</td></tr>";

           $k++;
        }
    }

    $k =0;
    $annex_reason ='';
    $stmt13 = $dbh_ewap->prepare("SELECT call_callers_notes.reason, count(call_callers_profile.cid) as r_count FROM scodd.call_callers_profile,call_callers_notes WHERE call_callers_profile.cid = call_callers_notes.cid AND call_callers_profile.corp_id=? AND DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=? GROUP BY reason");
    $stmt13->execute(array($corp_id, $ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt13->rowCount() != 0) {
        while ($row13 = $stmt13->fetch(PDO::FETCH_ASSOC)) {
           $seq = ($k+1);
           $reason[$k] = $row13['reason'];
           $r_count[$k] = $row13['r_count'];
           $annex_reason .= "<tr><td>$seq. $reason[$k]</td><td>$r_count[$k]</td></tr>";

           $k++;
        }
    }

    # Annex SCO VISITOR LOG count
    $article_count = 0;
    $login_count = 0;
    $asmt_count = 0;
    $stmt14 = $dbh_ewap->prepare("select type,count(emp_id) as v_count FROM visitor_log WHERE corp_id=? AND DATE(datetime)>=? AND DATE(datetime)<=? GROUP BY type");
    $stmt14->execute(array($corp_id,$ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt14->rowCount() != 0) {
        while ($row14 = $stmt14->fetch(PDO::FETCH_ASSOC)) {
            if($row14['type'] == 'ARTICLE'){
                $article_count = $row14['v_count'];
           }else if($row14['type'] == 'LOGIN'){
                $login_count = $row14['v_count'];
           }else if($row14['type'] == 'ASMT'){
                $asmt_count = $row14['v_count'];
           }
        }
    }
	
	# Tranquil count
	$dbh_tq = new PDO($dsn_tranq, $tranq_user, $tranq_pass);
    $dbh_tq->query("use mndfapp");
	
	$stmt13 = $dbh_tq->prepare("select count(uid) AS users from user_premium WHERE corp_id = ?");
    $stmt13->execute(array($corp_id));
	if ($stmt13->rowCount() != 0) {
		$row13 = $stmt13->fetch();
		$tranq_users =  $row13['users'];
	}else{
		$tranq_users = 0;
	}
	
		
	$stmt13 = $dbh_tq->prepare("SELECT COUNT(t.track_id) AS tracks, SUM(t.duration) AS duration from tracks_activity t, user_premium u WHERE t.uid = u.uid AND u.corp_id =? AND t.time >= ? AND t.time <= ?;");
    $stmt13->execute(array($corp_id,$ewap_first_monthdate, $ewap_last_monthdate));
	if ($stmt13->rowCount() != 0) {
		$row13 = $stmt13->fetch();
		$no_of_track = $row13['tracks'];
		$seconds =  $row13['duration'];
		$hours = floor($seconds / 3600);
		$minutes = floor(($seconds / 60) % 60);
		$seconds = $seconds % 60;
		
		$total_duration = $hours."hrs ".$minutes."mins";
	}else{
		$total_duration = "0hrs 0mins";
		$no_of_track = 0;
	}

	#Annexure 2 Details 
	$annex2_var = '';
	$stmt11 = $dbh_ewap->prepare("SELECT vertical ,count(cid) As vertical_count FROM view_accor WHERE taken_at>=? AND taken_at<=? GROUP BY vertical");
	$stmt11->execute(array($ewap_first_monthdate, $ewap_last_monthdate));
    if ($stmt11->rowCount() != 0) {
		$i = 0;
		
        while ($row11 = $stmt11->fetch(PDO::FETCH_ASSOC)) {
           
            $vertical= $row11['vertical'];	
			$vertical_count = $row11['vertical_count'];
			
			$m = $f = $eq = $ws = $rs = $pt = $ps = $ad = $mf = $cc = $pc = $ac = $cg = $es= $ep = $ef = $others = 0;
			
			$stmt12 = $dbh_ewap->prepare("SELECT gender ,reason FROM view_accor WHERE vertical =? AND taken_at>=? AND taken_at<=?");
			$stmt12->execute(array($vertical, $ewap_first_monthdate, $ewap_last_monthdate));
			if ($stmt12->rowCount() != 0) {
				while ($row12 = $stmt12->fetch(PDO::FETCH_ASSOC)) {
						if($row12['gender'] == 'F'){
							$f++;							
						}else if( $row12['gender'] == 'M'){
							$m++;
						}
						
						if($row12['reason'] == 'Enquiry'){	
							$eq++;								
						}else if($row12['reason'] == 'Work Stress'){
							$ws++;
						}else if($row12['reason'] == 'Relationship Stress'){
							$rs++;
						}else if($row12['reason'] == 'Parenting'){
							$pt++;
						}else if($row12['reason'] == 'Personal stress'){
							$ps++;
						}else if($row12['reason'] == 'Addiction'){
							$ad++;
						}else if($row12['reason'] == 'Mindfulness'){
							$mf++;
						}else if($row12['reason'] == 'Crisis Concerns'){
							$cc++;
						}else if($row12['reason'] == 'Psychological Concerns'){
							$pc++;
						}else if($row12['reason'] == 'Academic Concerns'){
							$ac++;
						}else if($row12['reason'] == 'Career Guidance'){
							$cg++;
						}else if($row12['reason'] == 'Enquiry – Student'){
							$es++;
						}else if($row12['reason'] == 'Enquiry – Parent'){
							$ep++;
						}else if($row12['reason'] == 'Enquiry – Faculty'){
							$ef++;
						}else{
							$others++;
						}
						
						$enq = $es + $ep + $ef + $eq;
								 
				}		
			}
			
			$annex2_var .= '<tr><td><b> '.($i+1).'. '.$vertical.'</b> (Total Calls : <b>'.$vertical_count.'</b>)</td></tr>'.
								'<tr><td width="100%">'.
								'<ol>'.
								'<li>Gender wise call count<BR/>'.
								'<table width="90%" cellpadding="6px"><tr><td>(a) Male  </td><td style="text-align:right;">'.$m.'</td></tr><tr><td>(b) Female </td><td style="text-align:right;">'.$f.'</td></tr></table>'.
								'</li>'.
								'<li>Issue type wise call count<BR/>'.
								'<table width="90%" cellpadding="6px"><tr><td>(a) Work Stress </td><td style="text-align:right;">'.$ws.'</td></tr><tr><td>(b) Relationship Stress </td><td style="text-align:right;">'.$rs.'</td></tr><tr><td>(c) Personal Stress </td><td style="text-align:right;"> '.$ps.'</td></tr><tr><td>(d) Parenting </td><td style="text-align:right;">'.$pt.'</td></tr><tr><td>(e) Addiction </td><td style="text-align:right;">'.$ad.'</td></tr><tr><td>(f) Mindfulness </td><td style="text-align:right;">'.$mf.'</td></tr><tr><td>(g) Crisis Concerns </td><td style="text-align:right;">'.$cc.'</td></tr><tr><td>(h) Psychological Concerns </td><td style="text-align:right;"> '.$pc.'</td></tr><tr><td>(i) Academic Concerns </td><td style="text-align:right;"> '.$ac.'</td></tr><tr><td>(j) Career Guidance </td><td style="text-align:right;"> '.$cg.'</td></tr><tr><td>(k) Enquiry </td><td style="text-align:right;">'.$enq.'</td></tr><tr><td>(l) Others </td><td style="text-align:right;">'.$others.'</td></tr></table>'.
								'</li>'.
								'</ol>'.
								'</td></tr>';
								
			
			$i++;
        
        }  
		
    }
	

//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, '12px', PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'EWAP Usage Report', '', 0, 'C', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 10);




// -----------------------------------------------------------------------------

$tbl = <<<EOD
<table cellspacing="0" cellpadding="10" border="1">
    <tr>
        <td colspan="2" align="center"><b>EWAP Usage Report</b></td>
    </tr>
    <tr>
    	<td><b>Corporate Name</b><br /> $corp_name
    	</td>
    	<td><b>Corporate ID </b> <br/> $corp_id </td>
    </tr>
    <tr>
       <td><b>Address </b> <br/>
		$address
		</td>
		<td>
			<b>Report Period</b><br/>
			$first_date to $last_date
		</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$tbl2 = <<<EOD
<table cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td colspan="2" align="center"><b>Service usage details</b></td>
    </tr>
    <tr>
    	<td colspan="2"><b>1. Counseling</b><br/>
    		<table cellspacing="0" cellpadding="5" border="0" width="100%">
    		<tr>
		    	<td width="85%">(a). Telephonic Calls</td>
		    	<td width="15%" align="center">$count_tc</td>
		    </tr>
		    <tr>
		    	<td>(b). Face to Face</td>
		    	<td align="center">$count_f2f</td>
		    </tr>
		    <tr>
		    	<td>(c). Skype Calls</td>
		    	<td align="center">$count_skype</td>
		    </tr>
		    <tr>
		    	<td>(d). Stress Control Online Calls</td>
		    	<td align="center">$total_sco_calls</td>
		    </tr>
			<tr>
		    	<td>(e). Chat</td>
		    	<td align="center">$count_chat</td>
		    </tr>
		    <tr>
		    	<td align="right">Total</td>
		    	<td align="center"><div style="border-top:2px solid #000; border-bottom:2px solid #000;">$total_counsellling_calls </div></td>
		    </tr>
		</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="2"><b>2. Stress Control Online</b><br/>
    		<table cellspacing="0" cellpadding="5" border="0">
    		<tr>
		    	<td width="85%">(a). New User SignUp</td>
		    	<td width="15%" align="center">$total_user_signedup</td>
		    </tr>
		    <tr>
		    	<td>(b). Sessions</td>
		    	<td align="center">$total_sco</td>
		    </tr>
		</table>
    	</td>
    </tr>
	<tr>
    	<td width="84%"><b>3. Tranquil App Usage</b></td>
    	<td width="16%" align="center">$total_duration</td>
    </tr>
	<tr>
    	<td width="84%"><b>4. Other</b></td>
    	<td width="16%" align="center">$total_others</td>
    </tr>
</table>
<br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/>
<br/><br/><br/>
EOD;


$pdf->writeHTML($tbl2, true, false, false, false, '');


$tbl3 = <<<EOD
<table cellspacing="0" cellpadding="10" border="0" width="100%">
    <tr>
        <td colspan="5" align="center"><b>EWAP itemised usage report</b></td>
    </tr>
    <tr>
    	<td colspan="5"><b>1. Counseling</b></td>
    </tr>
    <tr>
    	<td colspan="5">(a) Telephonic Calls <br/><br/>
    		<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>Caller ID</b></th>
					    <th width="20%"><b>Datetime</b></th>
					    <th width="27%"><b>Emp Location</b></th>
					    <th width="18%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				    $telephonic
			    </tbody>
	    	</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="5">(b) Face to Face<br/><br/>
	    	<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>Caller ID</b></th>
					    <th width="20%"><b>Datetime</b></th>
					    <th width="27%"><b>Emp Location</b></th>
					    <th width="18%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				   $face2face
			    </tbody>
	    	</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="5">(c) Skype Calls<br/><br/>
	    	<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>Caller ID</b></th>
					    <th width="20%"><b>Datetime</b></th>
					    <th width="27%"><b>Emp Location</b></th>
					    <th width="18%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				    $skype
			    </tbody>
	    	</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="5">(d) Stress Control Online Calls<br/><br/>
    		<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>Caller ID</b></th>
					    <th width="16%"><b>Datetime</b></th>
					    <th><b>Sessions</b> </th>
					    <th width="20%"><b>Emp Location</b></th>
					    <th width="16%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				   $sco_calls
			    </tbody>
	    	</table>
    	</td>
    </tr>
	<tr>
    	<td colspan="5">(e) Chat <br/><br/>
	    	<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>Caller ID</b></th>
					    <th width="20%"><b>Datetime</b></th>
					    <th width="27%"><b>Emp Location</b></th>
					    <th width="18%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				    $chats
			    </tbody>
	    	</table>
    	</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tbl3, true, false, false, false, '');


$tbl4 = <<<EOD
<table cellspacing="0" cellpadding="10" border="0" width="100%">
    <tr>
    	<td colspan="5"><b>2. Stress Control Online</b></td>
    </tr>
    <tr>
    	<td colspan="5">(a) New User SignUp<br/><br/>
    		<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>User ID</b></th>
					    <th width="20%"><b>Registration Time</b></th>
					    <th width="27%"><b>Emp Location</b></th>
					    <th width="18%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				     $sco_new_reg
			    </tbody>
	    	</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="5">
    		(b) Sessions<br/><br/>
	    	<table border="1" width="98%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="16%"><b>User ID</b></th>
					    <th width="15%"><b>Date</b></th>
					    <th width="15%"><b>Session</b></th>
					    <th width="22%"><b>Emp Location</b></th>
					    <th width="16%"><b>Vertical</b></th>
					    <th><b>Process</b></th>
				    </tr>
				    $sco_ssn
			    </tbody>
	    	</table>
    	</td>
    </tr>

</table>
EOD;

$pdf->writeHTML($tbl4, true, false, false, false, '');

$tbl5 = <<<EOD
<table cellspacing="0" cellpadding="10" border="0" width="100%">
    <tr>
    	<td colspan="5"><b>3. Tranquil App Usage</b></td>
    </tr>
    <tr>
    	<td colspan="5">
	    	<table border="1" width="100%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="30%"><b>Duration</b></th>
					    <th width="20%"><b>No. of Users</b></th>
					    <th width="20%"><b>No. of tracks listened</b></th>
						<th width="30%"><b>Total duration of tracks listened</b></th>
				    </tr>
				    <tr>
					    <td>$first_date to $last_date</td>
						<td>$tranq_users</td>
						<td>$no_of_track</td>
						<td>$total_duration</td>
				    </tr>
			    </tbody>
	    	</table>
    	</td>
    </tr>    
</table>
<table cellspacing="0" cellpadding="10" border="0" width="100%">
    <tr>
    	<td colspan="5"><b>4. Other</b></td>
    </tr>
    <tr>
    	<td colspan="5">
	    	<table border="1" width="100%" cellspacing="0" cellpadding="2">
			    <tbody>
				    <tr>
					    <th width="20%"><b>Date</b></th>
					    <th width="23%"><b>Activity</b></th>
					    <th width="30%"><b>Location</b></th>
					    <th><b>Additional Details</b></th>
				    </tr>
				    $others
			    </tbody>
	    	</table>
    	</td>
    </tr>    
</table>
EOD;

$pdf->writeHTML($tbl5, true, false, false, false, '');

// -----------------------------------------------------------------------------
$tbl2 = <<<EOD
<table cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td colspan="2" align="center"><b>Annexure</b></td>
    </tr>
    <tr>
    	<td colspan="2"><b>1. Portal Visitors Data</b><br/><br/>
    		<table cellspacing="0" cellpadding="5" border="0" width="100%">
	    		<tr>
			    	<td width="85%">(a). No. of logins</td>
			    	<td width="15%">$login_count</td>
			    </tr>
			    <tr>
			    	<td>(b). No. of articles read</td>
			    	<td>$article_count</td>
			    </tr>
			    <tr>
			    	<td>(c). No. of assessment done</td>
			    	<td>$asmt_count</td>
			    </tr>
			</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="2"><b>2. Counselling calls Data</b><br/><br/>

    	<table cellspacing="0" cellpadding="5" border="0" wiidth="100%">
    		<tr>
		    	<td width="85%">(a). Total Calls</td>
		    	<td width="15%">$total_annex_calls</td>
		    </tr>
		    <tr>
		    	<td colspan="2">(b).  Gender Wise Calls
		    		<br/>
		    		<table cellspacing="0" border="0" width="100%" style="padding:5px 25px;">
		    			<tr>
		    				<td width="82%" >1. Male</td>
		    				<td width="18%">$m_count</td>
		    			</tr>
		    			<tr>
		    				<td>2. Female</td>
		    				<td>$f_count</td>
		    			</tr>
		    		</table>
		    	</td>
		    </tr>
		    <tr>
		    	<td colspan="2">(c).  Location Wise Data
		    		<br/>
		    		<table cellspacing="0" border="0" width="100%" style="padding:5px 25px;">
		    			<tbody>
						    <tr>
							    <th width="82%"></th>
							    <th width="18%"></th>
						    </tr>
						   $annex_location
					    </tbody>
		    			
		    		</table>
		    	</td>
		    </tr>
		    <tr>
		    	<td colspan="2">(d). Issue Type
		    		<br/>
		    		<table cellspacing="0"  border="0" width="100%" style="padding:5px 25px;">
		    			<tbody>
						    <tr>
							    <th width="82%"></th>
							    <th width="18%"></th>
						    </tr>
						   $annex_reason
					    </tbody>
		    		</table>
		    	</td>
		    </tr>
		</table> <BR/><BR/>
        <HR/>
    	</td>
    </tr>
</table> <br/><br/><br/><BR/>
EOD;

$pdf->writeHTML($tbl2, true, false, false, false, '');

$tbl3 = <<<EOD
<table cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td colspan="2" align="center"><b>Annexure 2 <br/><br/></b></td>
    </tr>
	$annex2_var
</table>
EOD;

$pdf->writeHTML($tbl3, true, false, false, false, '');
//Close and output PDF document
$pdf->Output($pdf_name, 'I');

//============================================================+
// END OF FILE
//============================================================+
