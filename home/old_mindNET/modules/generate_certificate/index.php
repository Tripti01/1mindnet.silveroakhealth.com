<?php
include 'soh-config.php';
include 'mindnet-host.php';
include '../../if_loggedin.php';
	# Connection to sohdbl
	$dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
	$dbh_sco->query("use sohdbl");
 
	$status = 0;
	$email = "";
	if(isset($_REQUEST['email']) && $_REQUEST['email'] != ''){
	 
		$email = $_REQUEST['email'];
	 //ssbaa@allstate.com
		$stmt02 = $dbh_sco->prepare("SELECT activated,uid, name FROM user_temp WHERE email = ?");
		$stmt02->execute(array($email));
		echo $stmt02->rowCount();
		if ($stmt02->rowCount() != 0) {			
			$row02 = $stmt02->fetch();
			if($row02['activated'] == 1){
				$user_name = $row02['name'];
				$stmt01 = $dbh_sco->prepare("SELECT * FROM asmt_score WHERE uid=? and type=? LIMIT 1");
                $stmt01->execute(array($row02['uid'], 'POST'));
                if ($stmt01->rowCount() != 0) {
                   header("Location:sco/cert/?name=".$user_name);
                }else{
					$status = 2;	
				}
			}
		}else{			
			$status = 1;			
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Generate SCO Certificate</div>
                                    <div class="panel-body">
                                        <div Style="text-align: center;font-size: 15px;">
                                            <div class="confirm-msg">  												
												<?php if($status == 1){
													echo '<div style="color:red">'.$email.' is not a Stress Control Online user</div>';
												}elseif($status ==2){
													echo '<div style="color:red">'.$user_name.' has not yet completed Stress Control Online program</div>';
												}?>
                                            </div>
                                        </div>
                                        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Enter User Email </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control input-sm"  name="email" placeholder="User Email">
                                                </div>
												<div class="col-md-2 col-xs-12">
                                                    <input type="submit" class="btn btn-info waves-effect waves-light" value="Submit" name="btn-create-user"/>
                                                </div>
                                            </div>
                                            <div class="form-group m-b-0" style="text-align: center;">
                                                 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
					
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: "Please enter the email",
                        email: "Invalid Email ID"
                    }
                }
            });
        </script>     
    </body>
</html>