<?php

$presented_to = $_REQUEST['name'];
$issued_date = date('d-m-Y');

# Function to have ordinal  suffix of number
function ordinal_suffix($num){
    $num = $num % 100; // protect against large numbers
    if($num < 11 || $num > 13){
         switch($num % 10){
            case 1: return 'st';
            case 2: return 'nd';
            case 3: return 'rd';
        }
    }
    return 'th';
}

# Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');



$date_of_issued_date = date('j', strtotime($issued_date)).ordinal_suffix(date('j', strtotime($issued_date)));
$month_of_issued_date = date('F', strtotime($issued_date));
$year_of_issued_date =  date('Y', strtotime($issued_date));
$certificate_name = 'Resilience at Workplace_'.$presented_to.'.pdf';


# create new PDF document
$pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

# set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

# set margins
$pdf->SetMargins(15, 15, 15,15);

# set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


# set font
$pdf->SetFont('helvetica', '', 10);

#disabling Header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

# add a page
$pdf->AddPage();


# define some HTML content with style
$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 24pt;
        text-decoration: underline;
    }
        
    .lowercase {
        text-transform: lowercase;
    }
    .uppercase {
        text-transform: uppercase;
    }
    .capitalize {
        text-transform: capitalize;
    }
	div.title {
		color: #CC0000;
		font-family: helvetica;
		font-size: 24pt;
		text-align: center;
	}
	div.content {
		color: grey;
		font-family: helvetica;
		font-size: 14pt;
		text-align: center;
	}
	table.first{
		border-left: 2px solid #847445;
		border-right: 2px solid #847445;
		border-top: 2px solid #847445;
	}	
</style>

<table class="first myTableBg4" height="100%" cellpadding="5" cellspacing="1" width="100%" border="0" >
 
<tr>	
	<td align="center" height="50px" width="100%"></td>
 </tr>
 <tr>
	<td align="center" width="100%">
		<img class="login-logo"  src="logo.png" style="width:60px; height:60px;" alt="SilverOakHealth">
	</td>
 </tr>
   <tr>
	<td align="center" width="100%" style="font-size:16.6px;padding:15px 0 5px 0;color:#000;">
		SILVER OAK HEALTH
	</td>
 </tr>
 <tr>
	<td align="center" width="100%" style="font-size:11.1px;padding:0px 0 5px 0;color:#000;">
		EST. 2015
	</td>
 </tr>
  <tr>
	<td align="center" width="100%" style="font-size:40px;padding:15px 0 5px 0;color:#847445;">
		RESILIENCE AT WORK PLACE
	</td>
 </tr>
</table>';



// set default font subsetting mode
$pdf->setFontSubsetting(false);
$fontname = TCPDF_FONTS::addTTFfont(K_PATH_FONTS.'/Cinzel-Regular.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, 'B', 20);

// output the HTML content
$pdf->writeHTML($html, false, false, false, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$html = '
<table cellpadding="5" cellspacing="1" style="border-left:2px solid #847445;  border-right:2px solid #847445;">
  <tr>
	<td align="center" width="100%" style="font-size:18px;padding:15px 0 15px 0;color:#8D8E89;font-style:Italic">
		This certificate is presented to
	</td>
 </tr>
 </table>';
 

$fontname = TCPDF_FONTS::addTTFfont(K_PATH_FONTS.'/Lora-Italic.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, 'B', 20);
$pdf->writeHTML($html, false, false, false, false, '');

$html = '
<table cellpadding="5" cellspacing="1" style="border-left:2px solid #847445;  border-right:2px solid #847445;">
  <tr>
	<td align="center" width="100%" style="font-size:56px;padding:15px 0 15px 0;color:#847445;letter-spacing:1px;">
		'.$presented_to.'
	</td>
 </tr>
 </table>';
 
 $fontname = TCPDF_FONTS::addTTFfont(K_PATH_FONTS.'/Cinzel-Regular.ttf', 'TrueTypeUnicode', '', 32);
 $pdf->SetFont($fontname, 'B', 20);
 $pdf->writeHTML($html, false, false, false, false, '');
 
$html = '
<table cellpadding="5" cellspacing="1" style="border-left:2px solid #847445;  border-right:2px solid #847445;">
<tr>
	<td align="center" width="100%" style="font-size:14px;padding:15px 0 15px 0;color:#8D8E89;">
		For successful completion of Stress Control Online, an 8-week online Program <br/> for building coping skills
	</td>
 </tr>
 </table>';
 
 

$fontname = TCPDF_FONTS::addTTFfont(K_PATH_FONTS.'/Lora-Regular.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, 'R', 20);
$pdf->writeHTML($html, false, false, false, false, '');

$html = '
<table cellpadding="5" cellspacing="1" style="border-left:2px solid #847445;  border-right:2px solid #847445;">
    <tr>
	<td align="center" width="100%"  style="font-size:14px;padding:35px 0 15px 0;color:#8D8E89;">
		<b>Presented this '.$date_of_issued_date.' day of '.$month_of_issued_date.' in the year '.$year_of_issued_date.'</b>
	</td>
 </tr>
  <tr>
	<td align="center" width="100%" height="40px">
		
	</td>
 </tr>
</table>';
 
 

$fontname = TCPDF_FONTS::addTTFfont(K_PATH_FONTS.'/Lora-Bold.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, 'B', 20);
$pdf->writeHTML($html, false, false, false, false, '');

$html = '
<table cellpadding="5" cellspacing="1" border="0" style="border-left:2px solid #847445;  border-right:2px solid #847445;  border-bottom:2px solid #847445;">
   <tr>
	<td width ="100%">
		<table width ="100%" border="0" cellpadding="0">
			<tr>
				<td align="right" width="20%" rowspan="2">
					<br/><br/>
					<img class="login-logo" src="soh-logo.png" style="width:85px; height:62px;margin-top:50px;" alt="SilverOakHealth">
				</td>			
				<td align="center" width="55%" style="font-size:11.1px;color:#8D8E89;">
					
					<img class="login-logo" src="sara-sig.png" height="80px;"  alt="SilverOakHealth"><br/>
					Saravanan Neel <br/> COO & Head of Psychological Services<br/>							
				</td>
				<td align="left" width="0%">
					<br/><br/>
					<img  src="sco-logo.png" alt="Stresscontrolonline" width="183" height="70">
				</td>
			</tr>
		</table>
	</td>
 </tr>
 </table>'; 
 
$fontname = TCPDF_FONTS::addTTFfont(K_PATH_FONTS.'/Lora-Regular.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, 'R', 20);
$pdf->writeHTML($html, false, false, false, false, '');

# Close and output PDF document
$pdf->Output($certificate_name, "I");