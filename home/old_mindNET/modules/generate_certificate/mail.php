<?php
	require 'mindnet/aws_sdk/aws-autoloader.php';
    require 'mindnet/ses_plugin/autoloader.php';
    require 'mindnet/ses_plugin/mail_credentials.php';
    $m = new SimpleEmailServiceMessage();

## ------------------------------- EMAIL PARAMETERS ---------------------------##
    $to = 'vijays@silveroakhealth.com';
    $from = 'Stress Control Online <no-reply@stresscontrolonline.com>';
    $subject = 'EWAP';

$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">
</head>
<body style="line-height:1.6;color:#222;font-family:Open Sans;" >
<table align="center" width="600px" border="0">
	<tr>
		<td>
			<table align="center" width="600px" border="0" style="border:4px solid #0068a5;border-collapse: collapse;">
				<tr>
					<td height="380px">
						<img align="center" border="0" src="https://s3.ap-south-1.amazonaws.com/sohcdn2/landingpages/Sling+Media_Header_Image.png" alt="Sling Media" title="Image" width="593" style="line-height: inherit;outline: none;clear: both;border: 0.0px;height: auto;float: none;width: 593.0px;max-width: 593.0px;display: block;">
					</td>
				</tr>
				<tr>
					<td bgcolor="#0068a5" style="color:#fff;font-size:33px;text-align:center;padding:15px;">
						<b>EWAP - Employee Wellbeing and Assistance Program</b>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<hr></hr>			
		</td>
	</tr>
	<tr>
		<td style="font-size:14px;">
			<b>Your emotional wellbeing is of utmost importance to us</b> and it gives us great pleasure to announce <b>EWAP - Employee Wellbeing and Assistance Program (EWAP)</b> from Silver Oak Health. 

			<br/><br/>Explore the various benefits offered by the program free for all employees by logging into the confidential and personalised portal with your Employee ID.		
		</td>
	</tr>
	<tr>
		<td align="center" style="padding:30px">
			<div style="color:#fff;"><a href="https://www.silveroakhealth.com/slingmedia/" target="_BLANK" style="background-color:#0068a5;color:#fff;padding:10px 35px 10px 35px;font-size:20px;margin-top:20px; text-decoration: none;"> LOGIN TO THE PORTAL </a></div>
		</td>
	</tr>
	<tr>
		<td style="font-size:14px;">
			<b>Feeling Overwhelmed or Facing a Dilemma?</b><br/>
			This program gives you access to experienced counsellors who are trained to help you when you are in a dilemma or have difficulty making decisions related to relationships, family, work or finances. You can also call in if you are feeling overwhelmed and just need someone to talk to.		
		</td>
	</tr>
	<tr>
		<td align="center" style="padding:30px">
			<a href="tel:9035358080" style="background-color:#0068a5;color:#fff;padding:10px 20px 10px 20px;font-size:20px;margin-top:20px; text-decoration: none;"> CALL NOW : 90 35 35 80 80 </a>
		</td>
	</tr>
	<tr>
		<td style="font-size:14px;">
			<b>Online Certificate Program (Stress Control Online)</b><br/>
			This program gives you access to experienced counsellors who are trained to help you when you are in a dilemma or have difficulty making decisions related to relationships, family, work or finances. You can also call in if you are feeling overwhelmed and just need someone to talk to.		
		</td>
	</tr>
	<tr>
		<td align="center" style="padding:30px">
			<a href="https://www.youtube.com/watch?v=pJ9SgCf_8eU&list=PLB-xnZexkRCd9QONWl8MBqd33SLQyKDoi" target="_BLANK" style="background-color:#0068a5;color:#fff;padding:10px 35px 10px 35px;font-size:20px;margin-top:20px; text-decoration: none;">HOW IT WORKS VIDEO </a>
		</td>
	</tr>
	<tr>
		<td style="font-size:14px;padding-bottom:25px;">
			Employees can also participate in enriching mindfulness practices and other unique workshops around the year
		</td>
	</tr>
	<tr>
		<td bgcolor="#555555">
			<table  border="0"  width="100%">
				<tr>
					<td align="center" style="color:#fff;padding-top:10px">
						<b>LOGIN TO KNOW MORE</b>
					</td>
				</tr>
				<tr>
					<td style="color:#fff;font-size:14px;text-align:center;">
						<a href="https://www.silveroakhealth.com/slingmedia/" target="_BLANK" style="color:#fff;text-decoration:none;"> WWW.SILVEROAKHEALTH.COM/SLINGMEDIA </a>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
</table>
</body>
</html>';

    $text = ''; // plain text version [optional]
## ----------------------------------------------------------------------------##

    $m->addTo($to);
    $m->setFrom($from);
    $m->setSubject($subject);
    $m->setMessageFromString('', $html);
    $m->setSubjectCharset('ISO-8859-1');

    try {
        $ses = new SimpleEmailService($key, $secret); // Sending the message
        $ses->sendEmail($m);
    } catch (Exception $ex) {
        die("Some Error Occured. Please Try Again. If the problem persists. Send us an email at help@silveroakhealth.com");
    }
