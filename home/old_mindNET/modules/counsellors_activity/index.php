<?php
/*
  1.After the privilege set by superadmin he can choose the corporate on the slidebar then when he clicks create corporate he lands on this page.
  2.Coprporate admin  is allowed to insert the data into the given form and the credentials of new corporate is created.
  3.Mail could be sent with particular credentials.
 */

#file inclusion for various function happening in the ui
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

if (isset($_REQUEST['submit'])) {

        if (isset($_REQUEST['from_date']) && isset($_REQUEST['to_date']) && $_REQUEST['from_date'] != "" && $_REQUEST['to_date'] != "") {
            $from_date = $_REQUEST['from_date'];
            $to_date = $_REQUEST['to_date'];
            header("Location:aggregated.php?from_date=" . $from_date . "&to_date=" . $to_date);
        }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>Counsellor Activity</title>
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
              <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>
            .ewap_cmp_name{
                padding:10px 25px;
                font-size:16px;
                color:#1078c4;
            }
            .corp-list{
                padding:8px;
                width:350px;
                margin-left:25px;
            }
			.radio{
				margin-left:25px;
			}
			.dates{
				width:45%;
				float:left;
				margin-left:25px;
				
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="background-image: url('<?php echo $img_loc; ?>'); background-size:cover;background-repeat: no-repeat;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>

            <div class="container">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <!--Default Tabs-->
                        <div class="col-md-8 col-sm-12">
                            <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <form action="index.php" method="POST">
                                
                                    <div class="row" id="view_usage_date" style="margin-top:20px;">
                                        <div class="col-md-12">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td width="45%"><div class="ewap_cmp_name"><b>Select Date</b></div></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="dates">
                                                            Select From Date
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar">
                                                                    </i>
                                                                </div>
                                                                <input class="form-control input-sm" id="date" autocomplete="off" name="from_date" placeholder="DD/MM/YYYY" type="text"/>
                                                            </div>
                                                        </div>
                                                        <div class="dates">
                                                            Select To Date
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar">
                                                                    </i>
                                                                </div>
                                                                <input class="form-control input-sm" id="date" autocomplete="off" name="to_date" placeholder="DD/MM/YYYY" type="text"/>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                    </div> 
                                    <center<div class="row" id="view_usage_btn" style="margin-top:20px;display:block;margin-left:1%">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-6">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td width="45%">
                                                        <div style="padding:10px;">
                                                            <button class="btn btn-primary " name="submit" type="submit">
                                                                Show Activity
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                    </div> 
                                </section>
                            </article>
                        </form>
                    </div>
                </div>
            </div>

                                    </div>
            <!-- jQuery  -->
            <script src="../../../assets/js/jquery.min.js"></script>
            <script src="../../../assets/js/bootstrap.min.js"></script>
            <script src="../../../assets/js/detect.js"></script>
            <script src="../../../assets/js/fastclick.js"></script>
            <script src="../../../assets/js/jquery.slimscroll.js"></script>
            <script src="../../../assets/js/jquery.blockUI.js"></script>
            <script src="../../../assets/js/waves.js"></script>
            <script src="../../../assets/js/jquery.nicescroll.js"></script>
            <script src="../../../assets/js/jquery.scrollTo.min.js"></script>
            <script src="../../../assets/js/jquery.core.js"></script>
            <script src="../../../assets/js/jquery.app.js"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

            <!-- Include Date Range Picker -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

            <script>
                $(document).ready(function () {
                    var date_input = $('input[name="from_date"]'); //our date input has the name "date"
                    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'dd-mm-yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    });

                    var date_input = $('input[name="to_date"]'); //our date input has the name "date"
                    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                        format: 'dd-mm-yyyy',
                        container: container,
                        todayHighlight: true,
                        autoclose: true,
                    })
                })
            </script>
    </body>
</html>
