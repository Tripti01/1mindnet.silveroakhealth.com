<?php
	
	#file inclusion for various function happening in the ui
	require '../../if_loggedin.php';	
	include 'mindnet-host.php';
	require_once('soh-config.php');
	include 'mindnet/functions/crypto_funtions.php';
	include 'mindnet/functions/get_session_name.php';


	if(isset($_REQUEST['from_date']) &&  isset($_REQUEST['to_date']) &&  isset($_REQUEST['type']) &&  isset($_REQUEST['tid']) &&  isset($_REQUEST['tname'])){
			$from_date = $_REQUEST['from_date'];
			$to_date = $_REQUEST['to_date'];
			$type = $_REQUEST['type'];
			$tid = $_REQUEST['tid'];
			$tname = $_REQUEST['tname'];
			
			# Date to display
			$first_date = date('d-M-y',strtotime($from_date));
			$last_date = date('d-M-y',strtotime($to_date));

			# Date to calculate
			$cal_to_date = date('Y-m-d',strtotime($from_date));
			$cal_from_date = date('Y-m-d',strtotime($to_date));
	}
	
	
	# Connection to sohdbl
	$dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
	$dbh_sco->query("use sohdbl");


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>Detailed SCO Activity | Counsellor Activity</title>
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
              <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>
            .ewap_cmp_name{
                padding:10px 25px;
                font-size:16px;
                color:#1078c4;
            }
            .corp-list{
                padding:8px;
                width:350px;
                margin-left:25px;
            }
			.radio{
				margin-left:25px;
			}
			.dates{
				width:45%;
				float:left;
				margin-left:25px;
				
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>

				<div class="container">
					<div class="row">
						<div class="col-md-1"></div>
                        <div class="col-md-10 col-sm-12">
							<div class="panel panel-color panel-info" style="margin-bottom: 5px;">
								<div class="panel-body">
									<center style="padding:20px;font-size:15px;"><?php echo 'Showing activity from <b>'.$first_date.'</b> to <b>'.$last_date.'</b> for <b>'.$tname.'</b> of the type <b>'.$type; ?></b> </center>
									<p><b>SCO Activity</b></p>
									<table class="table table-striped table-bordered" style="width:100%;">
											<tr>
												<th>Counselors name</th>
												<th>User Name</th>
												<th>Session Id</th>
												<th>Date and Time</th>
											</tr>
											
										<?php	
										if($type == 'SCO Sessions'){
												$stmt02 = $dbh_sco->prepare("select user.name ,thrp.ssn,thrp.completed_on from thrp_user_ssn_completed thrp, user_login user WHERE tid=? and DATE(thrp.completed_on)>=? AND DATE(thrp.completed_on)<=? AND thrp.uid = user.uid;");
												$stmt02->execute(array($tid, $cal_to_date, $cal_from_date));
												$j=0;
												if ($stmt02->rowCount() != 0) {
													while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
														$name = decrypt($row02['name'], $encryption_key);
														$ssn_id = get_ssn_fullname($row02['ssn']);
														$date_time = date("d-M-Y H:i",strtotime($row02['completed_on']));
														
														echo '<tr>';
														echo '<td>'.$tname.'</td>';
														echo '<td>'.$name.'</td>';
														echo '<td>'.$ssn_id.'</td>';
														echo '<td>'.$date_time.'</td>';
														echo '</tr>';
														
														$j++;
													}
												} 
										}else{
												$stmt02 = $dbh_sco->prepare("select user.name ,thrp.ssn,thrp.timestamp from thrp_notes thrp, user_login user WHERE tid=? and DATE(thrp.timestamp)>=? AND DATE(thrp.timestamp)<=? AND thrp.uid = user.uid;");
												$stmt02->execute(array($tid, $cal_to_date, $cal_from_date));
												$j=0;
												if ($stmt02->rowCount() != 0) {
													while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
														$name = decrypt($row02['name'], $encryption_key);
														$ssn_id = get_ssn_fullname($row02['ssn']);
														$date_time = date("d-M-Y H:i",strtotime($row02['timestamp']));
														
														echo '<tr>';
														echo '<td>'.$tname.'</td>';
														echo '<td>'.$name.'</td>';
														echo '<td>'.$ssn_id.'</td>';
														echo '<td>'.$date_time.'</td>';
														echo '</tr>';
														
														$j++;
													}
												} 
										}?>
									</table>
								</div>
							</div>
						</div>
                    </div>
                </div>

            <!-- jQuery  -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/detect.js"></script>
            <script src="assets/js/fastclick.js"></script>
            <script src="assets/js/jquery.slimscroll.js"></script>
            <script src="assets/js/jquery.blockUI.js"></script>
            <script src="assets/js/waves.js"></script>
            <script src="assets/js/jquery.nicescroll.js"></script>
            <script src="assets/js/jquery.scrollTo.min.js"></script>
            <script src="assets/js/jquery.core.js"></script>
            <script src="assets/js/jquery.app.js"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    </body>
</html>
