<?php
    
    #file inclusion for various function happening in the ui
    require '../../if_loggedin.php';	
	include 'mindnet-host.php';

    require_once('ewap-config.php');
    require_once('soh-config.php');

    include 'mindnet/functions/crypto_funtions.php';


    if(isset($_REQUEST['from_date']) &&  isset($_REQUEST['to_date'])){
            $from_date = $_REQUEST['from_date'];
            $to_date = $_REQUEST['to_date'];
            
            # Date to display
            $first_date = date('d-M-y',strtotime($from_date));
            $last_date = date('d-M-y',strtotime($to_date));

            # Date to calculate
            $cal_to_date = date('Y-m-d',strtotime($from_date));
            $cal_from_date = date('Y-m-d',strtotime($to_date));
    }

    # Connection to sohdbl
    $dbh_sco = new PDO($dsn_sco, $sco_user, $sco_pass);
    $dbh_sco->query("use sohdbl");

    $stmt01 = $dbh_sco->prepare("SELECT name, tid from thrp_login WHERE 1");
    $stmt01->execute(array());
    if ($stmt01->rowCount() != 0) {
        $all_counsellor = array();
        while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
            $key = $row01['tid'];
            $value = decrypt($row01['name'], $encryption_key);
            $all_counsellor[$key] = $value;
        }
    }
    
    $count_tc = 0;
    $count_f2f_onsite = 0;
    $count_f2f_soh = 0;
    $count_f2f_affiliate = 0;
    $count_skype = 0;
    $count_chat = 0;
    $total_calls = 0;
    $i=0;
    $j=0;
    
    


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>Counsellor Activity</title>
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
              <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
        <style>
            .ewap_cmp_name{
                padding:10px 25px;
                font-size:16px;
                color:#1078c4;
            }
            .corp-list{
                padding:8px;
                width:350px;
                margin-left:25px;
            }
            .radio{
                margin-left:25px;
            }
            .dates{
                width:45%;
                float:left;
                margin-left:25px;
                
            }
			#table-wrapper {
			  position:relative;
			}
			#table-scroll {
			  overflow:auto;  
			  margin-top:20px;
			}
			#table-wrapper table {
			  width:100%;

			}
			#table-wrapper table * {
			  color:black;
			}
			#table-wrapper table thead th .text {
			  position:absolute;   
			  top:-20px;
			  z-index:2;
			  height:20px;
			  width:35%;
			  border:1px solid red;
			}
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
            </div>
    
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="panel panel-color panel-info" style="margin-bottom: 5px;">
                                <div class="panel-body">
                                    <center style="padding:20px;font-size:15px;"><?php echo 'Showing activity from <b>'.$first_date.'</b> to <b>'.$last_date; ?></b></center>
                                    <p><b>EWAP Activity</b></p>
									<div id="table-wrapper">
										<div id="table-scroll">
                                    <table class="table table-striped table-bordered" style="width:100%;">
                                            <tr>
                                                <th>Counselors name</th>												
                                                <th>Portal Chat Enquiry - IB</th>
												<th>SMS/WA Enquiry - IB</th>
                                                <th>F2F-Onsite</th>
                                                <th>F2F Affiliate</th>
                                                <th>F2F-SOH</th>
												<th>Video</th>
                                                <th>Email Enquiry - IB</th>
                                                <th>Email follow up - OB</th>
                                                <th>Email follow up - IB</th>
												<th>Phone No response - OB</th>
												<th>Phone reschedule - OB</th>
												<th>Phone Direct - IB</th>
												<th>Phone follow up - OB</th>
												<th>Phone Enquiry - IB</th>
												<th>Phone Follow upc- IB</th>
												<th>Phone session scheduling - IB</th>
												<th>Portal call request - IB</th>
												<th>SMS/WA no response - OB</th>
												<th>SMS/WA Session scheduling - IB</th>
												<th>Duration</th>
                                            </tr>
                                            
                                        <?php   $stmt02 = $dbh_ewap->prepare("SELECT DISTINCT(taken_by) as tid FROM call_callers_notes WHERE DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=? ORDER BY taken_at ASC");
                                                $stmt02->execute(array($cal_to_date, $cal_from_date));
                                                if ($stmt02->rowCount() != 0) {
                                                    while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
                                                        $taken_by = $row02['tid'];
														$c_101 = 0;
														$c_102 = 0;
														$c_103 = 0;
														$c_104 = 0;
														$c_105 = 0;
														$c_106 = 0;
														$c_107 = 0;
														$c_108 = 0;
														$c_109 = 0;
														$c_110 = 0;
														$c_111 = 0;
														$c_112 = 0;
														$c_113 = 0;
														$c_114 = 0;
														$c_115 = 0;
														$c_116 = 0;
														$c_117 = 0;
														$c_118 = 0;
														$c_119 = 0;
														
														$duration = 0;
														$duration_in_hrs = 0;
                                                        $stmt03 = $dbh_ewap->prepare("SELECT source, duration FROM call_callers_notes WHERE taken_by = ? AND DATE(call_callers_notes.taken_at) >=? and DATE(call_callers_notes.taken_at)<=?");
                                                        $stmt03->execute(array($taken_by, $cal_to_date, $cal_from_date));
                                                        while ($row03 = $stmt03->fetch(PDO::FETCH_ASSOC)) {
                                                            $type = $row03['source'];
															$duration = $duration + $row03['duration'] ;
                                                            
                                                            if ($type == '101'){
                                                                $c_101++;
                                                            } else if ($type == '102'){
                                                                $c_102++;
                                                            } else if ($type == '103'){
                                                                $c_103++;
                                                            } else if ($type == '104'){
                                                                $c_104++;
                                                            } else if ($type == '105'){
                                                                $c_105++;
                                                            } else if ($type == '106'){
																$c_106++;
                                                            } else if ($type == '107') {
                                                                $c_107++;
                                                            } else if ($type == '108') {
                                                                $c_108++;
                                                            } else if ($type == '109') {
                                                                $c_109++;
                                                            } else if ($type == '110') {
                                                                $c_110++;
                                                            } else if ($type == '107') {
                                                                $c_107++;
                                                            } else if ($type == '108') {
                                                                $c_108++;
                                                            } else if ($type == '109') {
                                                                $c_109++;
                                                            } else if ($type == '110') {
                                                                $c_110++;
                                                            } else if ($type == '111') {
                                                                $c_111++;
                                                            } else if ($type == '112') {
                                                                $c_112++;
                                                            } else if ($type == '113') {
                                                                $c_113++;
                                                            } else if ($type == '114') {
                                                                $c_114++;
                                                            } else if ($type == '115') {
                                                                $c_115++;
                                                            } else if ($type == '116') {
                                                                $c_116++;
                                                            } else if ($type == '117') {
                                                                $c_117++;
                                                            } else if ($type == '118') {
                                                                $c_118++;
                                                            } else if ($type == '119') {
                                                                $c_119++;
                                                            } else {
																
															}
														}
														
														$duration_in_hrs = $duration/60;
														$duration_in_hrs = number_format((float)$duration_in_hrs, 1, '.', ''). ' hrs';
														
                                                        echo '<tr>';
                                                        echo '<td>'.$all_counsellor[$taken_by].'</td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=101&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_101.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=102&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_102.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=103&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_103.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=104&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_104.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=105&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_105.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=106&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_106.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=107&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_107.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=108&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_108.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=109&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_109.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=110&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_110.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=111&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_111.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=112&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_112.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=113&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_113.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=114&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_114.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=115&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_115.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=116&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_116.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=117&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_117.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=118&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_118.'</a></td>';
                                                        echo '<td><a href="detailed_EWAP.php?tid='.$taken_by.'&type=119&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$taken_by].'" target="_BLANK">'.$c_119.'</a></td>';
                                                        
														echo '<td>'.$duration.'mins ('.$duration_in_hrs.')</td>';
                                                        echo '</tr>';
                                                        
                                                    }
                                                } ?>
                                    </table>
									</div>
									</div>
                                    <p><b>SCO Activity</b></p>
                                    <table class="table table-striped table-bordered" style="width:100%;">
                                            <tr>
                                                <th>Counselors name</th>
                                                <th>User Session Completed</th>
                                                <th>Notes Taken</th>
                                            </tr>
                                            <?php   
                                                $stmt02 = $dbh_sco->prepare("SELECT tid FROM thrp_login WHERE 1");
                                                $stmt02->execute(array());
                                                if ($stmt02->rowCount() != 0) {
                                                    $k=0;
                                                    while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
                                                        $tid = $row02['tid'];
                                                        
                                                        #count user completed session
                                                        $stmt03 = $dbh_sco->prepare("select count(uid) as count_uid from thrp_user_ssn_completed WHERE tid=? AND DATE(completed_on) >=? and DATE(completed_on)<=?");
                                                        $stmt03->execute(array($tid, $cal_to_date, $cal_from_date));
                                                        if ($stmt03->rowCount() != 0) {
                                                            $row03 = $stmt03->fetch();
                                                            $count_uid = $row03['count_uid'];
                                                        }
                                                        #count thrp notes
                                                        $stmt03 = $dbh_sco->prepare("select count(uid) as count_notes from thrp_notes WHERE tid=? AND DATE(timestamp) >=? and DATE(timestamp)<=?");
                                                        $stmt03->execute(array($tid, $cal_to_date, $cal_from_date));
                                                        if ($stmt03->rowCount() != 0) {
                                                            $row03 = $stmt03->fetch();
                                                            $count_notes = $row03['count_notes'];
                                                        }
                                                        
                                                        if($count_uid == 0 && $count_notes == 0){
                                                            #do nothing
                                                        }else{
                                                            echo '<tr>';
                                                            echo '<td>'.$all_counsellor[$tid].'</a></td>';
                                                            echo '<td><a href="detailed_SCO.php?tid='.$tid.'&type=SCO Sessions&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$tid].'" target="_BLANK">'.$count_uid.'</a></td>';
                                                            echo '<td><a href="detailed_SCO.php?tid='.$tid.'&type=SCO Notes&from_date='.$from_date.'&to_date='.$to_date.'&tname='.$all_counsellor[$tid].'" target="_BLANK">'.$count_notes.'</a></td>';
                                                            echo '</tr>';
                                                        }
                                                        $k++;
                                                    }
                                                }
                                            ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <!-- jQuery  -->
            <script src="../../../assets/js/jquery.min.js"></script>
            <script src="../../../assets/js/bootstrap.min.js"></script>
            <script src="../../../assets/js/detect.js"></script>
            <script src="../../../assets/js/fastclick.js"></script>
            <script src="../../../assets/js/jquery.slimscroll.js"></script>
            <script src="../../../assets/js/jquery.blockUI.js"></script>
            <script src="../../../assets/js/waves.js"></script>
            <script src="../../../assets/js/jquery.nicescroll.js"></script>
            <script src="../../../assets/js/jquery.scrollTo.min.js"></script>
            <script src="../../../assets/js/jquery.core.js"></script>
            <script src="../../../assets/js/jquery.app.js"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    </body>
</html>
