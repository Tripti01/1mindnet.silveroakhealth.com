<?php
require 'if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'mindnet/functions/get_emp_name.php';

                                            $dbh = new PDO($dsn, $login_user, $login_pass);
                                            $dbh->query("use mindnet");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../assets/img/logo-fav.png">
        <title>SilverOakHealth</title>
        <link rel="stylesheet" type="text/css" href="../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <div class="container-fluid">
                    <div class="navbar-header"><a href="index.php" class="navbar-brand"></a></div>
                    <div class="be-right-navbar">
                        <ul class="nav navbar-nav navbar-right be-user-nav">
                            <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                            <img src="../assets/img/avatar.png" alt="Avatar"></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li>
                                        <div class="user-info">
                                            <div class="user-name">Vijay Sharma</div>
                                        </div>
                                    </li>
                                    <li><a href="my_profile.php"><span class="icon mdi mdi-face"></span> Profile </a></li>
                                    <li><a href="my_settings.php"><span class="icon mdi mdi-settings"></span> Settings</a></li>
                                    <li><a href="logout.php"><span class="icon mdi mdi-power"></span> Logout</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div><a href="#" data-toggle="collapse" data-target="#be-navbar-collapse" class="be-toggle-top-header-menu collapsed">Wihout Sidebars</a>
                    <div id="be-navbar-collapse" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.html">Home</a></li>
                            <li class="dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                            Quick Links 
                            <span class="mdi mdi-caret-down"></span>
                            </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="my_colleagues.php"> My Colleagues </a></li>
                                    <li><a href=""> Send F & F Discount </a></li>
                                    <li><a href=""> Apply Leave</a></li>
                                    <li><a href=""> Claim Reimbursement </a></li>
                                    <li><a href=""> Check Holiday Calender </a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                           My Links
                            <span class="mdi mdi-caret-down"></span>
                            </a>
                                <ul role="menu" class="dropdown-menu">
                                    <?php include "show_my_links.php";  ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="be-content" style="background-image: url(img8.jpg); background-size: 100% 100%;">
                <div class="page-head">
                    <BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/><BR/>
                </div>
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-contrast">
                                <div class="panel-heading panel-heading-contrast"> 
                                    <div class="title">  Upcoming Holidays </div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-borderless">
                                        <tbody class="no-border-x">
                                        <?php
                                            $stmt10 = $dbh->prepare("SELECT holiday_name, holiday_date FROM soh_holiday_cal WHERE holiday_date > CURDATE() ORDER BY id LIMIT 4");
                                            $stmt10->execute();

                                             while ($row10 = $stmt10->fetch(PDO::FETCH_ASSOC))
                                                    {
                                                    $holiday_name = $row10['holiday_name'];
                                                    $holiday_date = date('d M Y', strtotime($row10['holiday_date']));
                                                    
                                                    echo "<tr>";
                                                    echo "<td>";
                                                    echo $holiday_name;
                                                    echo "<td>";
                                                    echo "<td>";
                                                    echo $holiday_date;
                                                    echo "<td>";
                                                    echo "</tr>";
                                                    }

                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                      <div class="panel panel-contrast">
                                <div class="panel-heading panel-heading-contrast"> 
                                    <div class="title"> Upcoming Birthdays </div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-borderless">
                                        <tbody class="no-border-x">
                                            <?php
                                            # Getting data for upcoming Birthdays

                                            # Select Upcoming Birthdays from Database that are due in next 30 days
                                            $stmt01 = $dbh->prepare("SELECT emp_id, DOB FROM emp_profile WHERE  DATE_ADD(DOB, INTERVAL YEAR(CURDATE())-YEAR(DOB) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(DOB),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY) ORDER BY MONTH(DOB), DAYOFMONTH(DOB);");
                                            $stmt01->execute();

                                            if ($stmt01->rowCount() == 0)
                                                {
                                                # No upcoming birthdays in the next 30 days  
                                                echo "<tr>";
                                                echo "<td>";
                                                echo "No upcoming birthdays in next 30 days.";
                                                echo "<td>";
                                                echo "</tr>";
                                                }
                                            else
                                                {
                                                #  
                                                while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC))
                                                    {
                                                    $emp_id = $row01['emp_id'];
                                                    $birth_date = date('d M', strtotime($row01['DOB']));

                                                    # Getting name for this emp_id
                                                    $stmt05 = $dbh->prepare("SELECT first_name, last_name FROM emp_login WHERE emp_id=? LIMIT 1;");
                                                    $stmt05->execute(array($emp_id));
                                                    $row05 = $stmt05->fetch();
                                                    $emp_name=$row05['first_name']." ".$row05['last_name']; 
                                                    
                                                    echo "<tr>";
                                                    echo "<td>";
                                                    echo $emp_name;
                                                    echo "<td>";
                                                    echo "<td>";
                                                    echo $birth_date;
                                                    echo "<td>";
                                                    echo "</tr>";
                                                    }
                                                }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-3">
                    <div class="panel panel-contrast">
                                <div class="panel-heading panel-heading-contrast"> 
                                    <div class="title"> Colleagues on Leave Today  </div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-borderless">
                                        <tbody class="no-border-x">
                                            <?php
                                            $stmt20 = $dbh->prepare(" SELECT emp_id FROM mindnet.emp_leave WHERE from_date <= CURDATE() AND to_date >= CURDATE() ");
                                            $stmt20->execute();

                                             while ($row20 = $stmt20->fetch(PDO::FETCH_ASSOC))
                                                    {
                                                    $emp_id= $row20['emp_id'];

                                                     # Getting name for this emp_id
                                                    $stmt05 = $dbh->prepare("SELECT first_name, last_name FROM emp_login WHERE emp_id=? LIMIT 1;");
                                                    $stmt05->execute(array($emp_id));
                                                    $row05 = $stmt05->fetch();
                                                    $emp_name=$row05['first_name']." ".$row05['last_name']; 
                                                    
                                                    echo "<tr>";
                                                    echo "<td>";
                                                    echo $emp_name;
                                                    echo "<td>";
                                                    echo "</tr>";
                                                    }

                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script src="../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>
        <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();

                //Runs prettify
                prettyPrint();
            });
        </script>
    </body>
</html>