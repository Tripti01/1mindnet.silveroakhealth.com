<?php
#including files and functions
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

//counter to fetch all employee details
$i = 0;

//to fetch all details of all employees
$stmt01 = $dbh->prepare("SELECT emp_login.emp_id AS emp_id,name,email,job_title,profile_pic,emp_dept.dept_id AS dept_id,dept_name, phone,fb,tw,li,bio FROM emp_login,emp_profile,emp_dept, dept_master, emp_contact WHERE emp_login.emp_id = emp_profile.emp_id AND emp_login.emp_id = emp_dept.emp_id AND emp_dept.dept_id = dept_master.dept_id AND emp_login.active = 1 AND emp_contact.emp_id = emp_login.emp_id ORDER BY rand()");
$stmt01->execute();
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $col_emp_id_list[$i] = $row01['emp_id'];
        $col_name[$i] = $row01['name'];
        $col_job_title[$i] = $row01['job_title'];
        $col_email_list[$i] = $row01['email'];
        $dept_list[$i] = $row01['dept_id'];
        $dept_name[$i] = $row01['dept_name'];
        $col_phone[$i] = $row01['phone'];
        $col_fb[$i] = $row01['fb'];
        $col_tw[$i] = $row01['tw'];
        $col_li[$i] = $row01['li'];
        $col_bio[$i] = $row01['bio'];
        $i++;
    }
}

//function to get profile pic for a given emp_id
function get_col_profile_pic($emp_id) {

    include 'mindnet-config.php';

    #Start database connection
    $dbh = new PDO($dsn, $login_user, $login_pass);
    $dbh->query("use mindnet");

    # Fetching user profile_pic from database 
    $stmt02 = $dbh->prepare("SELECT * from emp_profile where emp_id=? LIMIT 1");
    $stmt02->execute(array($emp_id));
    if ($stmt02->rowCount() != 0) {
        $result02 = $stmt02->fetch();
        $profile_pic = $result02['profile_pic'];
        //if profile_pic is not blank set col_profile_pic as the profile_pic else set as default_user.png
        if ($profile_pic != '') {
            $col_profile_pic = $profile_pic;
        } else {
            $col_profile_pic = 'default_user.png';
        }
    }

    echo $col_profile_pic;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title> mindNET - SilverOakHealth Intranet Portal</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="shortcut icon" href="../../../assets/images/favicon.ico">
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="../../../assets/js/modernizr.min.js"></script>
        <style>
            .card-box{
                box-sizing: border-box;
                width: 100%;
                height:300px;
            }
        </style>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <a href="index.php" class="logo"><span>mind<span>NET</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Intranet Home</h4>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <div class="user-img">
                            <img src="../../../assets/images/users/profile_pic/<?php echo $profile_pic; ?>" alt="user-img" class="img-circle img-thumbnail img-responsive">
                        </div>
                        <h5><a href="#"><?php echo $emp_name; ?></a> </h5>
                        <ul class="list-inline">
                            <li>
                                <a  data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-camera" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="account.php" >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="logout.php" class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="sidebar-menu">
                        <ul>
                            <?php
                            include '../../sidebar.php';
                            ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="content-page">
                <div class="content">
                    <div class="container">

                        <?php for ($i = 0; $i < $stmt01->rowCount(); $i++) { ?>

                            <div class="col-md-3">
                                <div class="text-center card-box" >
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12" >
                                            <div class="row m-b-10" style="text-align:right;">
                                                <?php
                                                //label color is different depending on the dept
                                                if ($dept_list[$i] == "ENGG") {
                                                    echo '<div class="label label-primary" >' . $dept_name[$i] . '</div>';
                                                } else if ($dept_list[$i] == "MRKT") {
                                                    echo '<div class="label label-success" >' . $dept_name[$i] . '</div>';
                                                } else if ($dept_list[$i] == "PSYS") {
                                                    echo '<div class="label label-warning" >' . $dept_name[$i] . '</div>';
                                                }
                                                ?>
                                            </div>
                                            <img src="../../../assets/images/users/profile_pic/<?php echo get_col_profile_pic($col_emp_id_list[$i]); ?>" style="text-align: center;width:28%;height:7%;" class="img-circle img-thumbnail img-responsive m-b-10"  alt="profile-image">

                                            <div class="row"  >
                                                <div class="text-left">
                                                    <p class="text-muted font-13" style="text-align: center;"><strong><?php echo $col_name[$i]; ?> </strong></p>
                                                </div>

                                                <div class="text-left m-b-10" style="margin-top:-2%;">
                                                    <p class="text-muted font-13" style="text-align: center;"><?php echo $col_job_title[$i]; ?> </p>
                                                </div>
                                                <div class="row m-b-10">
                                                    <?php if ($col_email_list[$i] != "") { ?>
                                                        <a href="mailto:<?php echo $col_email_list[$i]; ?> ?Subject=Hello%20again" target="_top">  <i class="fa fa-envelope fa-1x" style="color: #24478f;margin-right: 3%;" aria-hidden="true"></i></a>
                                                    <?php } else { ?>
                                                        <i class="fa fa-envelope fa-1x" style="color:#b3b3b3;margin-right: 3%;" aria-hidden="true"></i>      
                                                    <?php } ?>
                                                    <?php if ($col_phone[$i] != "") { ?>
                                                        <i class="fa fa-phone fa-1x" style="color: #24478f;margin-right: 3%;"  data-toggle="tooltip" data-placement="bottom" title="<?php echo $col_phone[$i]; ?>"  ></i>
                                                    <?php } else { ?>
                                                        <i class="fa fa-phone fa-1x" style="color:#b3b3b3;margin-right: 3%;" ></i>
                                                    <?php } ?>
                                                    <?php if ($col_fb[$i] != "") { ?>
                                                        <a href="https://www.facebook.com/<?php echo $col_fb[$i]; ?>" target="_BLANK"> <i class="fa fa-facebook fa-1x" style="color: #24478f;margin-right: 3%;" aria-hidden="true"></i></a>
                                                    <?php } else { ?> <i class="fa fa-facebook fa-1x" style="color: #b3b3b3;margin-right: 3%;" aria-hidden="true"></i> <?php } ?>
                                                    <?php if ($col_tw[$i] != "") { ?>
                                                        <a href="https://twitter.com/<?php echo $col_tw[$i]; ?>" target="_BLANK"> <i class="fa fa-twitter fa-1x" style="color: #24478f;margin-right: 3%;" aria-hidden="true"></i></a>
                                                    <?php } else { ?> <i class="fa fa-twitter fa-1x" style="color:#b3b3b3;margin-right: 3%;" aria-hidden="true"></i> <?php } ?>
                                                    <?php if ($col_li[$i] != "") { ?>
                                                        <a href="https://www.linkedin.com/in/<?php echo $col_li[$i]; ?>" target="_BLANK"> <i class="fa fa-linkedin fa-1x" style="color: #24478f;margin-right: 3%;" aria-hidden="true"></i></a>
                                                    <?php } else { ?> <i class="fa fa-linkedin fa-1x" style="color:#b3b3b3;margin-right: 3%;" aria-hidden="true"></i> <?php } ?>
                                                </div>

                                                <p class="text-muted font-13 m-b-10 box" style="text-align: center;" >
                                                    <?php echo $col_bio[$i]; ?>  </p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                        <?php } ?>

                        <div class="modal fade bs-example-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg" style="width:500px">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myLargeModalLabel">Change profile picture</h4>
                                    </div>
                                    <div class="modal-body">
                                        <iframe src="../../update_profile_pic.php" frameborder="0" scrolling="no" height="250px" width="430px"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
                <footer class="footer">
                    2016 © SilverOakHealth.
                </footer>
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>        
        <script src="../../../assets/js/jquery.min.js"></script>
        <script src="../../../assets/js/bootstrap.min.js"></script>
        <script src="../../../assets/js/detect.js"></script>
        <script src="../../../assets/js/fastclick.js"></script>
        <script src="../../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../../assets/js/jquery.blockUI.js"></script>
        <script src="../../../assets/js/waves.js"></script>
        <script src="../../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../../../assets/js/jquery.core.js"></script>
        <script src="../../../assets/js/jquery.app.js"></script>

        <script>
            $(".close").click(function () {
                window.top.location.reload();
            });
        </script>
    </body>
</html>