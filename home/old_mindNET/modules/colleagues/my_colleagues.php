<?php
#including files and functions
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

//counter to fetch all employee details
$i = 1;

//to fetch all details of all employees
$stmt01 = $dbh->prepare("SELECT emp_login.emp_id AS emp_id, first_name, last_name, email, job_title, profile_pic, emp_dept.dept_id AS dept_id,dept_name, phone,fb,tw,li,bio FROM emp_login,emp_profile,emp_dept, dept_master, emp_contact WHERE emp_login.emp_id = emp_profile.emp_id AND emp_login.emp_id = emp_dept.emp_id AND emp_dept.dept_id = dept_master.dept_id AND emp_login.active = 1 AND emp_contact.emp_id = emp_login.emp_id ORDER BY rand()");
$stmt01->execute();
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
        $col_emp_id_list[$i] = $row01['emp_id'];//to fetch list of emp_id
        $col_name[$i] = $row01['first_name'] . " " . $row01['last_name'];//to fetch list of all names
        $col_job_title[$i] = $row01['job_title'];//to fetch list of all job titles
        $col_email[$i] = $row01['email'];//to fetch list of email
        $dept_name[$i] = $row01['dept_name'];//to fetch list of dept name
        $col_phone[$i] = $row01['phone'];//to fetch list of phone numbers
        $col_fb[$i] = $row01['fb'];//to fetch list of fb id
        $col_tw[$i] = $row01['tw'];//to fetch list of twitter id
        $col_li[$i] = $row01['li'];//to fetch list of linkedin id
        $col_bio[$i] = $row01['bio'];//to fetch list of bio id
        $i++;
    }
}

$total_no_enployees = $stmt01->rowCount();//count of all employess

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
<?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="page-head">

                </div>
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="panel panel-default panel-border-color panel-border-color-primary" style="margin-bottom: 5%;">
                                <div class="panel-heading panel-heading-divider"><b>My Colleagues</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10 col-xs-12">
                            
                            <table>
                                <tr>
<?php
#Iterating throught the list of all users
for ($i = 1; $i <= $total_no_enployees; $i++) {
    #Displaying the top of the box
    echo '<td style="width:400px;padding: 20px 10px 0px 10px;">';
    echo '<div class="user-display">';
    echo '<div class="user-display-bg"></div>';
    echo '<div class="user-display-bottom">';?>
    <div class="user-display-avatar"><img src="../../../assets/img/profile_pic/<?php get_profile_pic($col_emp_id_list[$i]);?>" alt="Avatar"></div>
    <?php echo '<div class="user-display-info">';
    echo '<div class="name">' . $col_name[$i] . '</div>';
    echo '<div class="nick">' . $dept_name[$i] . '</div>';
    echo '</div>';
    echo '<div style="margin-top:20px;margin-left: 135px;">';

    #Displayimg Email and Mobile
    echo '<span class="mdi mdi-email" data-toggle="tooltip" data-placement="bottom" data-original-title="' . $col_email[$i] . '"></span> &nbsp;';
    echo '<span class="mdi mdi-smartphone-android" data-toggle="tooltip" data-placement="bottom" data-original-title="' . $col_phone[$i] . '"></span> &nbsp;';

    # Displaying facbook link if it exists.
    if (strlen($col_fb[$i]) > 1) {

        echo '<a href="' . $col_fb[$i] . '" target="_BLANK"><span class="mdi mdi-facebook-box"></span> </a>&nbsp;';
    }

    # Displaying the linkedin link 
    if (strlen($col_li[$i]) > 1) {

        echo '<a href="' . $col_li[$i] . '" target="_BLANK"><span class="mdi mdi-linkedin-box"></span> </a>&nbsp;';
    }

    # Displaying the twitter link 
    if (strlen($col_tw[$i]) > 1) {

        echo '<a href="' . $col_tw[$i] . '" target="_BLANK"><span class="mdi mdi-twitter-box"></span> </a>&nbsp;';
    }
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</td>';


    if ($i % 3 == 0) {
        echo "</tr>";
        echo "<tr>";
    }
}
?>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                //Runs prettify
                prettyPrint();
            });
        </script>
    </body>
</html>