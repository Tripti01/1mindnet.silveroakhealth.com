<?php

#checking the login details
include '../../if_loggedin.php';


#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';


#REQUEST activity_id
$activity_id = $_REQUEST['activity_id'];

# Start database transactions
$i = 0;
$corp_id=array();
$corp_name=array();


$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use sohdbl");

# Select categories from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_login WHERE 1 ORDER BY name");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row00['corp_id'];
        $corp_name[$i] = $row00['name'];
        $i++;
    }
}


#Select Marketing communication type
$stmt00 = $dbh->prepare("SELECT * FROM marketing_comm_type WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $comm_id[$i] = $row00['comm_id'];
        $comm_name[$i] = $row00['comm_type'];
        $i++;
    }
}

# Select activities from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM marketing_activity WHERE activity_id=?");
$stmt00->execute(array($activity_id));
if ($stmt00->rowCount() != 0) {
    $row00 = $stmt00->fetch();
    $ac_corp_id = $row00['corp_id'];
    $held_on = date("d-M-Y", strtotime($row00['date']));
    $ac_comm_id = $row00['comm_id'];
    $details = $row00['details'];


    $stmt02 = $dbh->prepare("SELECT name FROM corp_login WHERE corp_id=?");
    $stmt02->execute(array($ac_corp_id));
    if ($stmt02->rowCount() != 0) {
        $row02 = $stmt02->fetch();
        $ac_corp_name = decrypt($row02['name'], $encryption_key);
    }
}
if (isset($_REQUEST['corp_id']) && isset($_REQUEST['comm_id']) && isset($_REQUEST['date']) && isset($_REQUEST['details'])) {

    $activity_id = $_REQUEST['activity_id'];
    $corp_id = $_REQUEST['corp_id'];
    $comm_id = $_REQUEST['comm_id'];
    $date = str_replace('/', '-', $_REQUEST['date']);
    $held_on_date = date("Y-m-d ", strtotime($_REQUEST['date']));
    $details = $_REQUEST['details'];

    $dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
    $dbh->query("use sohdbl");

    $stmt20 = $dbh->prepare("UPDATE marketing_activity SET corp_id=?,comm_id=?,date=?, details=? WHERE activity_id=?");
    $stmt20->execute(array($corp_id, $comm_id, $held_on_date, $details, $activity_id));

    header("Location:" . $host . "/modules/marketing/activity_tracking.php?edit_status_code=2");
}

if (isset($_REQUEST['del_activity'])) {
    if (isset($_REQUEST['activity_id'])) {

        $corp_id = $_REQUEST['activity_id'];

        $dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
        $dbh->query("use sohdbl");

        $stmt20 = $dbh->prepare("DELETE FROM marketing_activity WHERE activity_id=?");
        $stmt20->execute(array($activity_id));

        header("Location:" . $host . "/modules/marketing/activity_tracking.php?edit_status_code=4");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

        <!-- App title -->
        <title>Edit Activity - Silver Oak Health</title>

        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>

        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <script src="../../../assets/js/modernizr.min.js"></script>
        <link href="https://s3-ap-southeast-1.amazonaws.com/scoreg/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../../assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/responsive.css" rel="stylesheet" type="text/css" />


        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .row{
                padding : 8px;
            }			
            .modal{
                margin-top:20px;
            }
            .modal-backdrop.in {
                filter: alpha(opacity=50);
                opacity: 0;
            }
        </style>
    </head>
    <!-- Begin page -->
    <div class="be-wrapper be-fixed-sidebar">
        <div class="be-wrapper be-nosidebar-left" >
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
<?php include '../../top_bar_nav.php'; ?>
            </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="card-box" style="margin-top:-4%;">
                    <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                        <div class="panel-body" style="background-color:#FCFCFB; margin-top:1.8%;">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST"  id="modal_role" target="_top" class="form-horizontal">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">	
                                            <div class="col-sm-6">
                                                <label  style="font-family:'Open Sans';">Corporate<span style="color:red;">*</span></label>
                                                <select id="corp-list-id" class="corp-list form-control"  name="corp_id">
                                                    <option value="0" disabled="" selected="">SELECT</option>
<?php for ($i = 0; $i < count($corp_id); $i++) { ?>
                                                        <option value="<?php echo $corp_id[$i]; ?>" <?php if ($corp_id[$i] == $ac_corp_id) {
        echo 'selected';
    } ?>><?php echo $corp_name[$i]; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label  style="font-family:'Open Sans';">Communication Type<span style="color:red;">*</span></label>
                                                <select id="corp-list-id" class="corp-list form-control"  name="comm_id">
                                                    <?php for ($i = 0; $i < count($comm_id); $i++) { ?>
                                                        <option value="<?php echo $comm_id[$i]; ?>" <?php if ($comm_id[$i] == $ac_comm_id) {
                                                        echo 'selected';
                                                    } ?>><?php echo $comm_name[$i]; ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">	
                                            <div class="col-sm-12">
                                                <label  style="font-family:'Open Sans';">Date<span style="color:red;">*</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="datepicker" placeholder="Select date here " name="date" value="<?php echo $held_on; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">	
                                            <div class="col-sm-12">
                                                <label  style="font-family:'Open Sans';">Details</label>
                                                <textarea  class="form-control"  placeholder="" name="details"><?php echo $details; ?></textarea>															
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" value="<?php echo $activity_id; ?>" name="activity_id" />
                                    <input  type="submit" name="create_activity"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                                    <button type="button" class="btn btn-danger" style="margin-left:3%;" onclick="framelocation()">Cancel</button>
                                    <button type="button" class="btn btn-info" style="margin-left:3%;float:left;background-color: #223c87 !important; border-color: #223c87 !important;"" data-toggle="modal" data-target="#del">Delete</button>
                                </div>
                            </form>

                        </div>
                        <div class="container">
                            <div class="modal fade" id="del"  role="dialog">
                                <div class="modal-dialog" style="width:400px;">
                                    <div class="modal-content" style="height:172px;margin-top:100px;background-color:#eaf0f0">
                                        <center><br/><br/>Are you sure you want to delete this?
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST"  id="modal_role"  class="form-horizontal">
                                                <input type="hidden" value="<?php echo $activity_id; ?>" name="activity_id" />
                                                <input  type="submit" name="del_activity"  class="btn btn-info" value="Yes" style="background-color: #223c87 !important; border-color: #223c87 !important;margin-top:20px" >
                                                <button type="button" class="btn btn-danger" onclick="framelocation()" style="margin-left:3%;margin-top:20px">No</button>
                                            </form></center>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- jQuery  -->
                        <script src="../../../assets/js/jquery.min.js"></script>
                        <script src="../../../assets/js/bootstrap.min.js"></script>
                        <script src="../../../assets/js/detect.js"></script>
                        <script src="../../../assets/js/fastclick.js"></script>
                        <script src="../../../assets/js/jquery.slimscroll.js"></script>
                        <script src="../../../assets/js/jquery.blockUI.js"></script>
                        <script src="../../../assets/js/waves.js"></script>
                        <!-- App js -->
                        <script src="../../../assets/js/jquery.core.js"></script>
                        <script src="../../../assets/js/jquery.app.js"></script>
                        <script src="../../../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>


                        <script>
                                                                                    var date = new Date();
                                                                                    date.setDate(date.getDate());

                                                                                    $('#datepicker').datepicker({
                                                                                        startDate: '01/01/1990'
                                                                                    });
                        </script>
                        <!--validation for the form -->
                        <script type="text/javascript">

                            $(".form-horizontal").validate({
                                rules: {
                                    corp_id: {
                                        required: true
                                    },
                                    creative_type: {
                                        required: true
                                    },
                                    date: {
                                        required: true,
                                    }

                                },
                                // Specify validation error messages
                                messages: {
                                    corp_id: {
                                        required: "Please select corporate"
                                    },
                                    activity_type: {
                                        required: "Please select activity"
                                    },
                                    date: {
                                        required: "Please enter a date",
                                    }
                                },
                                // Make sure the form is submitted to the destination defined
                                // in the "action" attribute of the form when valid
                                submitHandler: function (form) {
                                    form.submit();
                                }
                            });

                        </script>
                        <script>
                            function toplocation() {
                                window.top.location = '<?php echo $host; ?>/modules/marketing/activity_tracking.php';
                            }
                            function framelocation() {
                                window.location.href = '<?php echo $host; ?>/modules/marketing/edit_activity.php?activity_id=<?php echo $activity_id; ?>';
                                    }
                        </script>	
                        </body>
                        </html>