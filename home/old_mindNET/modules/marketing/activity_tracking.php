<?php
/*
  1.In the sidebar when view_corporates is clicked we land onto this page.
  2.All the corporates details can be seen here, there id,name and registration type are displayed with a view button in last column
  3.Clicking onto the view button takes you to the individual corporate information
 */

#checking the login details
include '../../if_loggedin.php';
include 'mindnet-host.php';

#file inclusion for various function happening in the ui
include 'ewap-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';

# Now check if from date and to date are set.
if (isset($_REQUEST['daterange'])) {
    $daterange = explode(" - ", $_REQUEST['daterange']);
    $from_date = date("Y-m-d", strtotime($daterange[0]));
    $to_date = date("Y-m-d", strtotime($daterange[1]));
} else {
    #From date and to date are not set. We need to set it to default.
    $to_date = date('Y-m-d H:i:s');
    $from_date = date("Y-m-d H:i:s", strtotime($to_date . "-7 day"));
}


# Date to display
$ui_from_date = date('d-M-y', strtotime($from_date));
$ui_to_date = date('d-M-y', strtotime($to_date));

# Status codes for error and success messages
$create_status_code = 0;
$edit_status_code = 0;

if (isset($_REQUEST['create_status_code'])) {
    $create_status_code = $_REQUEST['create_status_code'];
} else if (isset($_REQUEST['edit_status_code'])) {
    $edit_status_code = $_REQUEST['edit_status_code'];
} else {
    $create_status_code = 0;
    $edit_status_code = 0;
}


# Start database transactions
$i = 0;
$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use sohdbl");


# Select activities from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM marketing_activity WHERE DATE(date) >= ? AND DATE(date)<=? ORDER BY date ");
$stmt00->execute(array($from_date, $to_date));
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $activity_id[$i] = $row00['activity_id'];
        $ac_corp_id[$i] = $row00['corp_id'];
        $held_on[$i] = date("d-M-Y", strtotime($row00['date']));
        $details[$i] = $row00['details'];
        $ac_comm_id = $row00['comm_id'];

        $stmt03 = $dbh->prepare("SELECT comm_type FROM marketing_comm_type WHERE comm_id=?");
        $stmt03->execute(array($ac_comm_id));
        if ($stmt03->rowCount() != 0) {
            $row03 = $stmt03->fetch();
            $comm_type[$i] = $row03['comm_type'];
        }

        $stmt02 = $dbh->prepare("SELECT name FROM corp_login WHERE corp_id=?");
        $stmt02->execute(array($ac_corp_id[$i]));
        if ($stmt02->rowCount() != 0) {
            $row02 = $stmt02->fetch();
            $ac_corp_name[$i] = $row02['name'];
        }

        $i++;
    }
}

if (isset($_REQUEST['submit'])) {
    if (isset($_REQUEST['corp_id'])) {
        $corp_id = $_REQUEST['corp_id'];

        if (isset($_REQUEST['month']) && $_REQUEST['month'] != "") {
            $date_timestamp = strtotime($_REQUEST['month']);

            $month = date("F", $date_timestamp);
            $year = date("Y", $date_timestamp);
            header("Location:view_activity_report.php?period=" . $month . "&year=" . $year . "&corp_id=" . $corp_id);
        }
        if (isset($_REQUEST['from_date']) && isset($_REQUEST['to_date']) && $_REQUEST['from_date'] != "" && $_REQUEST['to_date'] != "") {
            $from_date = $_REQUEST['from_date'];
            $to_date = $_REQUEST['to_date'];
            header("Location:view_activity_report.php?from_date=" . $from_date . "&to_date=" . $to_date . "&corp_id=" . $corp_id);
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">

        <!-- App title -->
        <title>Activity Tracking - Silver Oak Health</title>
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/jqvmap/jqvmap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>

        <!-- DataTables -->
        <link href="../../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../../assets/css/components.css" rel="stylesheet" type="text/css" />
        <script src="../../../assets/js/modernizr.min.js"></script>
        <link href="https://s3-ap-southeast-1.amazonaws.com/scoreg/css/font-awesome.min.css" rel="stylesheet">
        <link href="../../../assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <script src="../../../assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .row{
                padding : 8px;
            }
            .modal{
                margin-top:20px;
            }
            input{
                margin-bottom:2%;
            }


        </style>
    </head>
    <body>
        <!-- Begin page -->
        <div class="be-wrapper be-fixed-sidebar">
            <div class="be-wrapper be-nosidebar-left" style="padding-top:20px;">
                <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                    <?php include '../../top_bar_nav.php'; ?>
                </nav>
                <div style="float:right;margin-right:20px;">
                    <button class="btn btn-primary" onclick="add()" data-toggle="modal" data-target="#activity_edit" title="edit" style="font-size:10px;">Create Activity</button>
                </div>
                <BR/><BR/><BR/><BR/><BR/>
            </div>			
            <div class="container-fluid in">
                <div class="row">
                    <div class="card-box" style="margin-top:-4%;">
                        <div class="panel panel-color panel-info" style="margin-top: 0px;margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-md-6">                        
                                    <span class="link-03" style="margin:20px 0px 0px 20px">Showing from</span> <b><?php echo $ui_from_date; ?></b> <span class="link-03">to </span> <b><?php echo $ui_to_date; ?></b>
                                </div>
                                <div  class="col-md-6">
                                    <div class="input-group mg-b-10" style="float:right">
                                        <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                            <div class="row ">
                                                <div class="col-lg-10" style="padding-right:5px">
                                                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                        <i class="fa fa-calendar"></i>&nbsp;
                                                        <span id="get_date"></span> <i class="fa fa-caret-down"></i>
                                                    </div>										
                                                </div>
                                                <div class="col-lg-1" style="padding-left:0px">
                                                    <input type="hidden" name="daterange" id="daterange" />
                                                    <button class="btn btn-sm pd-x-8 btn-primary btn-uppercase" onclick="get_daterange()" style="font-size:10px;line-height:18px !important;""> Show </button>
                                                </div>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background-color:#FCFCFB;">
                                <?php
                                if ($create_status_code == 1) {
                                    echo '<div style="color:#3AB149;text-align:center;">Activity Created Successfully!</div>';
                                } else if ($edit_status_code == 2) {
                                    echo '<div style="color:#3AB149;text-align:center;">Activity Updated Successfully!</div>';
                                } else if ($edit_status_code == 4) {
                                    echo '<div style="color:#3AB149;text-align:center;">Activity Deleted Successfully!</div>';
                                } else if ($create_status_code == 3 || $edit_status_code == 3) {
                                    echo '<div style="color:red;text-align:center;">Some Error Occurred</div>';
                                } else {
                                    echo "";
                                }
                                ?>
                                <table id="datatable-buttons" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="color:#188ae2;">Sno</th>
                                            <th style="color:#188ae2;">Corporate Name</th>
                                            <th style="color:#188ae2;">Communication Type</th>
                                            <th style="color:#188ae2;">Date</th>
                                            <th style="color:#188ae2;">Details</th>
                                            <th style="color:#188ae2;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>												
                                        <?php
                                        if (isset($ac_corp_id)) {
                                            for ($k = 0; $k < count($ac_corp_id); $k++) {
                                                ?>
                                                <!--if status_code==1 displays  corporator id name and register type-->
                                                <tr>
                                                    <td><?php echo ($k + 1); ?></td>
                                                    <td><?php echo $ac_corp_name[$k]; ?></td>
                                                    <td><?php echo $comm_type[$k]; ?></td>
                                                    <td><?php echo $held_on[$k]; ?></td>
                                                    <td><?php echo $details[$k]; ?> </td>                                               
                                                    <td><a href="edit_activity.php?activity_id=<?php echo $activity_id[$k] ?>" class="btn btn-primary" style="font-size:10px" target="_BLANK" >View & Edit</a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>

            <div class="container">
                <div class="modal fade" id="activity_edit"  role="dialog">
                    <div class="modal-dialog" style="width:900px;">
                        <div class="modal-content">
                            <iframe src="" scrolling="no" id="f_edit" width="100%" height="500px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="../../../assets/js/bootstrap.min.js"></script>
        <script src="../../../assets/js/detect.js"></script>
        <script src="../../../assets/js/fastclick.js"></script>
        <script src="../../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../../assets/js/jquery.blockUI.js"></script>
        <script src="../../../assets/js/waves.js"></script>
        <!-- Datatables-->
        <script src="../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../../../assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="../../../assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../../../assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../../../assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="../../../assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="../../../assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="../../../assets/plugins/datatables/buttons.print.min.js"></script>
        <!-- Datatable init js -->
        <script src="../../../assets/pages/datatables.init.js"></script>
        <!-- App js -->
        <script src="../../../assets/js/jquery.core.js"></script>
        <script src="../../../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>



        <script type="text/javascript">
                                                                                                        $(document).ready(function () {
                                                                                                            $('#datatable').dataTable();
                                                                                                            $('#datatable-keytable').DataTable({keys: true});
                                                                                                            $('#datatable-responsive').DataTable();
                                                                                                            $('#datatable-scroller').DataTable({ajax: "../../../assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true});
                                                                                                            var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
                                                                                                        });
                                                                                                        TableManageButtons.init();

        </script>

        <!--validation for the form -->
        <script type="text/javascript">

            $(".form-horizontal").validate({
                rules: {
                    corp_id: {
                        required: true
                    },
                    activity_type: {
                        required: true
                    },
                    date: {
                        required: true,
                    },
                    location: {
                        required: true,
                    }

                },
                // Specify validation error messages
                messages: {
                    corp_id: {
                        required: "Please select corporate"
                    },
                    activity_type: {
                        required: "Please select activity"
                    },
                    date: {
                        required: "Please enter a date",
                    },
                    location: {
                        required: "Please enter a location",
                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

        </script>


        <script>
            function edit(activity_id) {
                $("#f_edit").attr("src", "edit_activity.php?activity_id=" + activity_id);
            }
            function add() {
                $("#f_edit").attr("src", "add_activity.php");
            }
        </script>	
        <script>
            $("#view_m").click(function () {

                $("#view_usage_month").show();
                $("#view_usage_date").hide();
                $('#view_usage_btn').show();
            });
            $("#view_d").click(function () {
                $("#view_usage_date").show();
                $("#view_usage_month").hide();
                $('#view_usage_btn').show();
            });

        </script>
        <script type="text/javascript">
            $(function () {

                var start = moment().subtract(29, 'days');
                var end = moment();

                show_date = '<?php echo date("F d, Y", strtotime($from_date)); ?>';
                end_date = '<?php echo date("F d, Y", strtotime($to_date)); ?>';

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    maxDate: end,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(moment(show_date), moment(end_date));

            });
        </script>

        <script>
            function get_daterange() {
                var get_date_range = $("#get_date").text();
                $("#daterange").val(get_date_range);
            }
        </script>
    </body>
</html>