<?php
/*
  1.In the sidebar when view_corporates is clicked we land onto this page.
  2.All the corporates details can be seen here, there id,name and registration type are displayed with a view button in last column
  3.Clicking onto the view button takes you to the individual corporate information
 */

#checking the login details
include '../../if_loggedin.php';


#file inclusion for various function happening in the ui
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'soh-config.php';
include 'functions/crypto_funtions.php';


# Start database transactions
$i = 0;
$corp_id=array();
$corp_name=array();

$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use sohdbl");

# Select categories from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_login ORDER BY name");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row00['corp_id'];
        $corp_name[$i] = $row00['name'];
        $i++;
    }
}

$stmt00 = $dbh->prepare("SELECT * FROM marketing_comm_type WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $comm_id[$i] = $row00['comm_id'];
        $comm_name[$i] = $row00['comm_type'];
        $i++;
    }
}


if (isset($_REQUEST['corp_id']) && isset($_REQUEST['comm_id']) && isset($_REQUEST['date']) && isset($_REQUEST['details'])) {
    include 'mindnet/functions/token.php';

    $corp_id = $_REQUEST['corp_id'];
    $comm_id = $_REQUEST['comm_id'];
    $date = str_replace('/', '-', $_REQUEST['date']);
    $held_on_date = date("Y-m-d ", strtotime($_REQUEST['date']));
    $details = $_REQUEST['details'];

    $activity_id = create_unique_id(5);

    $dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
    $dbh->query("use sohdbl");

    $stmt20 = $dbh->prepare("INSERT INTO marketing_activity VALUES (?,?,?,?,?,?,?,?,?)");
    $stmt20->execute(array($activity_id, $corp_id, $comm_id, $held_on_date, $details, '', '', '', ''));

    header("Location:" . $host . "/modules/marketing/activity_tracking.php?create_status_code=1");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="https://s3-ap-southeast-1.amazonaws.com/sohcdn1/img/favicon.ico">

        <!-- App title -->
        <title>Add Activity - Silver Oak Health</title>

        <!-- DataTables -->
        <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

        <script src="assets/js/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiQbVEMemZgEE5gdyI50TxyIUK0Ba9PBI&sensor=false&libraries=places"></script>
        <script src="assets/js/modernizr.min.js"></script>

        <style>
            .dt-buttons {
                float: right;
            }
            div.dataTables_filter label {
                float:left;
            }
            .hr_class{
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(1, 1, 1, 0), rgba(1, 1, 1, 0.75), rgba(1, 1, 1, 0));
            }
            .row{
                padding : 8px;
            }			
            .modal{
                margin-top:20px;
            }

        </style>
    </head>
    <body class=" fixed-left" style="background-color:#fff">
        <!-- Modal to add role details -->
        <div class="container">

            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST"  id="modal_role" target="_top" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" onclick="toplocation()">&times;</button>
                    <h4 class="modal-title">Create New Activity</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label  style="font-family:'Open Sans';">Client Name<span style="color:red;">*</span></label>
                            <select id="corp-list-id" class="corp-list form-control"  name="corp_id">
                                <option value="0" disabled="" selected="">SELECT</option>
<?php
for ($i = 0; $i < count($corp_id); $i++) {
    echo '<option value="' . $corp_id[$i] . '">' . $corp_name[$i] . '</option>';
}
?>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label  style="font-family:'Open Sans';"> Communication Type<span style="color:red;">*</span></label>
                            <select id="activity-list-id" class="corp-list form-control"  name="comm_id">
                                <?php
                                for ($i = 0; $i < count($comm_id); $i++) {
                                    echo '<option value="' . $comm_id[$i] . '">' . $comm_name[$i] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label  style="font-family:'Open Sans';">Date<span style="color:red;">*</span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="datepicker" placeholder="Select date here " name="date" autocomplete=off>
                            </div>
                        </div>														
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label  style="font-family:'Open Sans';">Details</label>
                            <textarea  class="form-control"  placeholder="" name="details"></textarea>															
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input  type="submit" name="create_activity"  class="btn btn-info" value="Save" style="background-color: #223c87 !important; border-color: #223c87 !important;" >
                    <button type="button" class="btn btn-danger" onclick="toplocation()" style="margin-left:3%;">Cancel</button>
                </div>
            </form>
        </div>

        <!-- jQuery  -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->

        <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>			

        <script src="assets/plugins/moment/moment.js"></script>
        <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script>
                                                                            var date = new Date();
                                                                            date.setDate(date.getDate());

                                                                            $('#datepicker').datepicker({
                                                                                startDate: '01/01/1990'
                                                                            });
        </script>
        <!--validation for the form -->
        <script type="text/javascript">

            $(".form-horizontal").validate({
                rules: {
                    corp_id: {
                        required: true
                    },
                    activity_type: {
                        required: true
                    },
                    date: {
                        required: true,
                    },
                    location: {
                        required: true,
                    },
                    completed: {
                        required: true,
                    }

                },
                // Specify validation error messages
                messages: {
                    corp_id: {
                        required: "Please select corporate"
                    },
                    activity_type: {
                        required: "Please select  activity"
                    },
                    date: {
                        required: "Please enter date",
                    },
                    location: {
                        required: "Please enter location",
                    },
                    completed: {
                        required: "Please select status",
                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function (form) {
                    form.submit();
                }
            });

        </script>
        <script>
            function toplocation() {
                window.top.location = '<?php echo $host; ?>/modules/marketing/activity_tracking.php';
            }
        </script>	
    </body>
</html>