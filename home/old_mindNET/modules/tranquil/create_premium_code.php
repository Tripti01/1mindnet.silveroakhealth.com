<?php
//include files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet/functions/crypto_funtions.php';
include 'soh-config.php';

#---------------Database Connection tranquil---------------------#
$dbhost = "mndfapp.cgbct3oeqwvc.ap-south-1.rds.amazonaws.com";
$dbport = 3306;
$dbname = "mndfapp";

$mndfapp_dsn = "mysql:host={$dbhost};port={$dbport};dbname={$dbname}";
$mndfapp_user = "mndf_app_user";
$mndfapp_pass = "SCO@46782$";

#connection to database
$dbh_mndfapp = new PDO($mndfapp_dsn, $mndfapp_user, $mndfapp_pass);
$dbh_mndfapp->query("use mndfapp");

#---------------Database Connection tranquil---------------------#


$dbh = new PDO($dsn_sco, $sco_user, $sco_pass);
$dbh->query("use $dbname");


$i = 0;
$j = 0;
# Select categories from database and display in the form
$stmt00 = $dbh->prepare("SELECT * FROM corp_login WHERE 1");
$stmt00->execute();
if ($stmt00->rowCount() != 0) {
    $i = 0;
    while ($row00 = $stmt00->fetch(PDO::FETCH_ASSOC)) {
        $corp_id[$i] = $row00['corp_id'];
        $corp_name[$i] = decrypt($row00['name'], $encryption_key);


        $stmt01 = $dbh_mndfapp->prepare("SELECT access_code FROM corp_premium WHERE corp_id=? ORDER BY access_code ASC");
        $stmt01->execute(array($corp_id[$i]));
        if ($stmt01->rowCount() != 0) {
            while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {
                $tq_corp_name[$j] = $corp_name[$i];
                $premium_codes[$j] = $row01['access_code'];
                $j++;
            }
        }

        $i++;
    }
}

for ($i = 0; $i < count($premium_codes); $i++) {
    $stmt01 = $dbh_mndfapp->prepare("SELECT count(uid) AS user_count  FROM user_premium WHERE access_code=? ");
    $stmt01->execute(array($premium_codes[$i]));
    if ($stmt01->rowCount() != 0) {
        $row01 = $stmt01->fetch();
        $user_count[$i] = $row01['user_count'];
    } else {
        $user_count[$i] = 0;
    }
}

if (isset($_REQUEST['btn-create-user'])) {
    if (isset($_REQUEST['corp_id']) && $_REQUEST['corp_id'] != '' && isset($_REQUEST['premium_code']) && $_REQUEST['premium_code'] != '') {

        $corp_id = $_REQUEST['corp_id'];
        $premium_code = $_REQUEST['premium_code'];

        $status = 0;

        # Get partner_id and name
        $stmt = $dbh_mndfapp->prepare("SELECT * FROM corp_premium WHERE access_code=? LIMIT 1");
        $stmt->execute(array($premium_code));
        if ($stmt->rowCount() == 0) {
            #inserting the data into blog content
            $stmt20 = $dbh_mndfapp->prepare("INSERT into corp_premium VALUES(?,?,?,?,?)");
            $stmt20->execute(array('', $corp_id, $premium_code, '', ''));
            $status = 1;
            header("Location:https://app.silveroakhealth.com/mindNET/modules/tranquil/create_premium_code.php");
          
        } else {
            $status = 2; #ID already exist
        }
    } else {
        $status = 3; # Some Field is empty 
    }
} else {
    $status = 4;
# Either id or name is blank
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>Create Premium Code</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Tranquil Premium Code </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-7">
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#create_code_Modal">Create Premium Code</button><br/><br/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-8">
                                                <div Style="text-align: center;font-size: 15px;font-weight:bold;">
                                                    <?php
                                                    #displaying success or failure messages
                                                    if ($status != 0) {
                                                        switch ($status) {

                                                            case 1:
                                                                echo '<div class="alert alert-success" style="text-align:center;">';
                                                                echo "<strong>Premium code created successfully.</strong>";
                                                                echo '</div>';
                                                                break;
                                                            case 2:
                                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                                echo "<strong>Premium code Already exists.</strong>";
                                                                echo '</div>';
                                                                break;
                                                            case 3:
                                                                echo '<div class="alert alert-danger" style="text-align:center;">';
                                                                echo "<strong>Please Fill in All the Details.</strong>";
                                                                echo '</div>';
                                                            default: break;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <table width="100%">
                                                    <tr>
                                                        <th>Sno</th>
                                                        <th>Acccess Code</th>
                                                        <th>Corporate Name</th>
                                                        <th>No.of User</th>
                                                    </tr>
                                                    <?php
                                                    for ($i = 0; $i < count($tq_corp_name); $i++) {
                                                        echo "<tr>";
                                                        echo "<td>" . ($i + 1) . "</td>";
                                                        echo "<td>$premium_codes[$i]</td>";
                                                        echo "<td>$tq_corp_name[$i]</td>";
                                                        echo "<td>$user_count[$i]</td>";
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                </table>

                                                <!-- Create code Modal -->
                                                <div id="create_code_Modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Create Premium Code</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 col-xs-12 control-label"> Corporate Id </label>
                                                                        <div class="col-md-6 col-xs-12">
                                                                            <select id="corp-list-id" class="form-control input-sm"  name="corp_id">
                                                                                <option value="0" disabled="" selected="">SELECT </option>
                                                                                <?php
                                                                                for ($i = 0; $i < count($corp_id); $i++) {
                                                                                    echo '<option value="' . $corp_id[$i] . '">' . $corp_name[$i] . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>

                                                                            <span style="font-size:12px;color:#a7a7a7">If code is not sppecific to any corporate then Please Select <b><span style="color:#787676">Silver Oak Health</span></b> from the dropdown.</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 col-xs-12 control-label"> Premium Code </label>
                                                                        <div class="col-md-6 col-xs-12">
                                                                            <input type="text" class="form-control input-sm"  name="premium_code" placeholder="Premium Code">
                                                                        </div>
                                                                    </div>
                                                                    <br/><br/>
                                                                    <div class="form-group m-b-0" style="text-align: center;">
                                                                        <input type="submit" class="btn btn-info waves-effect waves-light" style="margin-top:-6%;" value="Create" name="btn-create-user"/> 
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>