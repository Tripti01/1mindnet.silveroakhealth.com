<?php

if (isset($_REQUEST['name']) && isset($_REQUEST['email']) && isset($_REQUEST['password']) && isset($_REQUEST['token']) && isset($_REQUEST['expirydate'])) {


    include 'demo_uid.php';
    include 'functions/order_id.php';
    include 'functions/assign_therapist.php';

    $emp_id = $_REQUEST['emp_id'];
    $email = $_REQUEST['email'];
    $username = $_REQUEST['name'];
    $password = $_REQUEST['password'];
    $token = $_REQUEST['token'];
    $expirydate = $_REQUEST['expirydate'];

    #---------------Database Connection---------------------#
    $dbhostCBT = "demo.cgbct3oeqwvc.ap-south-1.rds.amazonaws.com";
    $dbportCBT = 3306;
    $dbnameCBT = "sohdbl";
    $dsn1 = "mysql:host={$dbhostCBT};port={$dbportCBT};dbname={$dbnameCBT}";
    $login_user1 = "login_user";
    $login_pass1 = "Bangalore@1";
    #---------------Database Connection---------------------#

    $dbh = new PDO($dsn1, $login_user1, $login_pass1);
    $dbh->query("use sohdbl");

    #--------------User details-----------------------------#
    include 'mindnet/functions/crypto_funtions.php';
    $encryption_key = 'ab78e7997b6d5fcd55f4b5c32611b87cd923e88837b63bfdeccef819dc8ca282';
    $name = encrypt($username, $encryption_key);
    # Take a has of the password
    $hash_options = array('cost' => 11);
    $hashed_password = password_hash($password, PASSWORD_DEFAULT, $hash_options);

    $uid = create_new_uid("DEMO");

    $order_id = create_order_id();
    date_default_timezone_set("Asia/Kolkata");

    $order_done_datetime = date('Y-m-d H:i:s');
    $user_creation_time = date('Y-m-d H:i:s');

    $tracking_id = 00000000;
    $bank_ref_no = 0000;
    $failure_messag = "";
    $payment_mode = "";
    $card_name = "";
    $status_code = 0;
    $status_message = "Transaction Succesfull";
    $currency = "INR";
    $vault = "N";
    $amount = 1.0;
    $billing_name = $name;
    $retry = "null";
    $response_code = 0;
    $product = "SP";

    #Calucalting the schudle for the user future sessions, We taking time as end of the day so that the comparing date and times become reliable
    $due_datetime_SSN0 = date('Y-m-d 23:59:59'); // Session 0 is due the same day.
    $due_datetime_SSN1 = date('Y-m-d 23:59:59', strtotime("+1 week"));
    $due_datetime_SSN2 = date('Y-m-d 23:59:59', strtotime("+2 week"));
    $due_datetime_SSN3 = date('Y-m-d 23:59:59', strtotime("+3 week"));
    $due_datetime_SSN4 = date('Y-m-d 23:59:59', strtotime("+4 week"));
    $due_datetime_SSN5 = date('Y-m-d 23:59:59', strtotime("+5 week"));
    $due_datetime_SSN6 = date('Y-m-d 23:59:59', strtotime("+6 week"));
    $due_datetime_SSN7 = date('Y-m-d 23:59:59', strtotime("+7 week"));
    $due_datetime_SSN8 = date('Y-m-d 23:59:59', strtotime("+8 week"));
    $due_datetime_CBTEND = date('Y-m-d 23:59:59', strtotime("+10 week"));
    $cbt_endtime = date('Y-m-d 23:59:59', strtotime($expirydate));

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();
    try {

        # Adding payment details
        $stmt20 = $dbh->prepare("INSERT INTO order_done VALUES (?,?,?,?)");
        $stmt20->execute(array($order_id, $tracking_id, $email, $uid));

        $stmt30 = $dbh->prepare("INSERT INTO order_track VALUES (?,?,?,?,?,?,?,?)");
        $stmt30->execute(array($order_id, $email, $uid, $product, $amount, $order_done_datetime, $order_done_datetime, 1));

        $stmt35 = $dbh->prepare("INSERT INTO order_details VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt35->execute(array($order_id, $tracking_id, $bank_ref_no, "Success", $failure_messag, $payment_mode, $card_name, $status_code, $status_message, $currency, $amount, $vault, $retry, $response_code));

        # Updating user temp table
        $stmt40 = $dbh->prepare("INSERT INTO user_temp VALUES (?,?,?,?,?,?,?,?,?,?)");
        $stmt40->execute(array($email, $name, $hashed_password, $emp_id, '', '', $token, $user_creation_time, $uid, 1));

        # Inserting new rows for the user
        $stmt50 = $dbh->prepare("INSERT INTO user_login VALUES (?,?,?,?)");
        $stmt50->execute(array($email, $uid, $name, $hashed_password));

        $stmt60 = $dbh->prepare("INSERT INTO user_profile VALUES (?,?,?,?,?,?,?)");
        $stmt60->execute(array($uid, 1985, 'F', 'S', 10, 'default_user.png', 0));

        $stmt70 = $dbh->prepare("INSERT INTO user_location VALUES (?,?,?,?,?,?,?,?)");
        $stmt70->execute(array($uid, 'India', 'Karnataka', 'Bangalore Urban', '', '12.9081', '77.6476', 0));

        $stmt80 = $dbh->prepare("INSERT INTO user_time_creation VALUES (?,?)");
        $stmt80->execute(array($uid, $user_creation_time));

        $stmt90 = $dbh->prepare("INSERT INTO user_info VALUES (?,?,?,?,?)");
        $stmt90->execute(array($uid, '', '', '', ''));

        $stmt95 = $dbh->prepare("INSERT INTO user_comm_pref VALUES (?,?,?,?,?,?)");
        $stmt95->execute(array($uid, $email, '', '', '', ''));

        $stmt100 = $dbh->prepare("INSERT INTO user_progress VALUES "
                . "('$uid','1','SSN0','0','0'),"
                . "('$uid','2','SSN1','0','0'),"
                . "('$uid','3','SSN2','0','0'),"
                . "('$uid','4','SSN3','0','0'),"
                . "('$uid','5','SSN4','0','0'),"
                . "('$uid','6','SSN5','0','0'),"
                . "('$uid','7','SSN6','0','0'),"
                . "('$uid','8','SSN7','0','0'),"
                . "('$uid','9','SSN8','0','0');");
        $stmt100->execute(array());

        $stmt105 = $dbh->prepare("INSERT INTO user_time_ssn VALUES "
                . "('$uid','SSN0','',''),"
                . "('$uid','SSN1','',''),"
                . "('$uid','SSN2','',''),"
                . "('$uid','SSN3','',''),"
                . "('$uid','SSN4','',''),"
                . "('$uid','SSN5','',''),"
                . "('$uid','SSN6','',''),"
                . "('$uid','SSN7','',''),"
                . "('$uid','SSN8','','');");
        $stmt105->execute(array());

        $stmt110 = $dbh->prepare("INSERT INTO user_schedule VALUES "
                . "('$uid','SSN0','$due_datetime_SSN0'),"
                . "('$uid','SSN1','$due_datetime_SSN1'),"
                . "('$uid','SSN2','$due_datetime_SSN2'),"
                . "('$uid','SSN3','$due_datetime_SSN3'),"
                . "('$uid','SSN4','$due_datetime_SSN4'),"
                . "('$uid','SSN5','$due_datetime_SSN5'),"
                . "('$uid','SSN6','$due_datetime_SSN6'),"
                . "('$uid','SSN7','$due_datetime_SSN7'),"
                . "('$uid','SSN8','$due_datetime_SSN8'),"
                . "('$uid','CBTEND','$cbt_endtime');");
        $stmt110->execute(array());

        $stmt170 = $dbh->prepare("INSERT INTO user_type VALUES (?,?,?,?,?,?)");
        $stmt170->execute(array($uid, 'ID', '', $product, '', ''));

        ## Asssigns a therapist to the this new user
        ## In future Only assign depending upon the prduct choosen
        //$tid=get_assigned_therapist($uid);
        $stmt180 = $dbh->prepare("INSERT INTO user_therapist VALUES (?,?)");
        $stmt180->execute(array($uid, ""));


        include 'mindnet-config.php';
        include '../../../mail/send_user_created_email.php';

        $dbh1 = new PDO($dsn, $login_user, $login_pass);
        $dbh1->query("use mindnet");

        $stmt = $dbh1->prepare("SELECT email,first_name,last_name FROM emp_login WHERE emp_id=? LIMIT 1");
        $stmt->execute(array($emp_id));
        if ($stmt->rowCount() != 0) {
            $row = $stmt->fetch();
            $emp_email = $row['email'];
            $emp_name = $row['first_name'] . " " . $row['last_name'];
            send_user_created_email($emp_email, $emp_name,$username, $email, $password, $expirydate);
        }

        $dbh->commit();
        echo 'done';
    } catch (PDOException $e) {
        $dbh->rollBack();
        //echo $e->getMessage();
        echo 'not done';
    }
} else {
    echo 'not done';
}
?>