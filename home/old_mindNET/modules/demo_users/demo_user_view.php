<?php
//include files
require '../../if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'mindnet/functions/crypto_funtions.php';
include 'mindnet/functions/get_user_status.php';

# Find out the employee, only DEMO created by this emploee will be shown
# EMP id(same as ref_id) can also come URL, just in case we want to view for another employee, else take this employee id.
if (isset($_REQUEST['ref_id'])) {
    $ref_id = $_REQUEST['ref_id'];
} else {
    $ref_id = $_SESSION['emp_id'];
}

$i = 0;
$status = 0;

# Get the sort by coloumn and sort by order
if (isset($_REQUEST['sortby_col']) && isset($_REQUEST['sortby_ord'])) {
    $sortby_col = $_REQUEST['sortby_col'];
    $sortby_ord = $_REQUEST['sortby_ord'];
} else {
# Define the default sorting order
    $sortby_col = "name";
    $sortby_ord = "ASC";
}

#---------------Database Connection to demo server---------------------#
$dbhostCBT = "demo.cgbct3oeqwvc.ap-south-1.rds.amazonaws.com";
$dbportCBT = 3306;
$dbnameCBT = "sohdbl";
$dsn1 = "mysql:host={$dbhostCBT};port={$dbportCBT};dbname={$dbnameCBT}";
$login_user1 = "login_user";
$login_pass1 = "Bangalore@1";
#---------------Database Connection to demo server---------------------#
#--------------------Getting demo users------------------------------------#
$dbh = new PDO($dsn1, $login_user1, $login_pass1);
$dbh->query("use sohdbl");

$stmt01 = $dbh->prepare("SELECT uid,email,name FROM user_temp WHERE ref_id=?");
$stmt01->execute(array($ref_id));
if ($stmt01->rowCount() != 0) {
    while ($row01 = $stmt01->fetch(PDO::FETCH_ASSOC)) {

        $useremail = $row01['email'];
        $encrypted_username = $row01['name'];
        $username = decrypt($encrypted_username, $encryption_key);
        $uid = $row01['uid'];

        $stmt02 = $dbh->prepare("SELECT due_datetime FROM user_schedule WHERE uid=? and ssn=?");
        $stmt02->execute(array($uid, 'CBTEND'));
        if ($stmt02->rowCount() != 0) {
            $row02 = $stmt02->fetch();
            $end_time = $row02['due_datetime'];
            $user_endtime[$i] = date('Y-m-d H:i', strtotime($end_time));
        } else {
            $user_endtime[$i] = '';
        }
        $user_email[$i] = $useremail;
        $user_name[$i] = $username;
        $useruid[$i] = $uid;

        # Get the current status of the user
        list($user_ssn_seqn[$i], $user_ssn_status[$i]) = get_user_status($uid);

        $i++;
        $status = 1;
    }
} else {
    $status = 9;
}

$total_users = $i;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/lib/datatables/css/dataTables.bootstrap.min.css"/>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

        <style>
            .table-responsive tbody > tr > td {
                padding-top: 5px;
                padding-bottom: 5px;
            }
            .be-datatable-footer {
                padding-top: 10px;
                padding-right: 5px;
                padding-bottom: 10px;
                padding-left: 5px;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
           <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary panel-table">
                                    <div class="panel-heading panel-heading-divider" style="padding-bottom:15px;"><b>View Demo Users</b>
                                        <div style="float: right;"> <a href="demo_user_create.php"><button class="btn btn-space btn-primary"><i class="icon icon-left mdi mdi-account-add"></i>  Create Demo User</button></div></a>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th>
                                                            <B> USER NAME </B> &nbsp;
                                                        </th>
                                                        <th>
                                                            <B> USER EMAIL </B> &nbsp;
                                                        </th>
                                                        <th>
                                                            <B> USER STATUS </B> &nbsp;
                                                        </th>
                                                        <th>
                                                            <B> EXPIRY DATE </B>&nbsp;
                                                        </th>																										
                                                        <th>
                                                            EXTEND EXPIRY DATE
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    for ($k = 0; $k < $i; $k++) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $user_name[$k]; ?>													
                                                            </td>
                                                            <td>
                                                                <?php echo $user_email[$k]; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $user_ssn_status[$k]; ?>
                                                            </td>
                                                            <td> 
                                                                <span style="display:none"> <?php echo $user_endtime[$k]; ?></span>
                                                                <span>  <?php echo date('d-m-Y H:i', strtotime($user_endtime[$k])); ?> </span>																		
                                                            </td>
                                                            <td>
                                                                <div style="margin:5px 0 5px 0; ">
                                                                    <button data-toggle="modal" data-target="#expiry<?php echo $k; ?>" type="button" class="btn btn-space btn-primary btn-sm">Extend expiry date</button>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    <div id="expiry<?php echo $k; ?>" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                        <div class="modal-dialog custom-width">
                                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="form-horizontal group-border-dashed basic-form">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                                                                        <h3 class="modal-title">Extend Expiry Date</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <div>
                                                                                <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker col-sm-10">
                                                                                    <input type="text" value="<?php echo date('d-m-Y', strtotime($user_endtime[$k])); ?>" name="expiry_date<?php echo $k; ?>" class="form-control col-sm-6"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                        <input type="submit" name="btn-submit<?php echo $k; ?>" value="Update"  class="btn btn-primary"/>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (isset($_REQUEST['btn-submit' . $k])) {
                                                        $update_expiry_date = $_REQUEST['expiry_date' . $k];
                                                        $cbt_endtime = date('Y-m-d 23:59:59', strtotime($update_expiry_date));
                                                        $stmt03 = $dbh->prepare("UPDATE user_schedule set due_datetime =?  WHERE uid=? and ssn=?");
                                                        $stmt03->execute(array($cbt_endtime, $useruid[$k], 'CBTEND'));
                                                        echo '<script>
                                                          window.location.href = "' . $host . '/modules/demo_users/demo_user_view.php";
                                                          </script>';
                                                    }
                                                    ?>
                                                <?php }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>

        <script src="../../../assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
        <script src="../../../assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
        <script src="../../../../assets2/admin/layout3/scripts/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../../assets2/admin/layout3/scripts/additional-method-min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
                App.dataTables();
            });
        </script>
        <script>
            $(".close").click(function () {
                window.top.location.reload();
            });
        </script>
        
    </body>
</html>