<?php
//include files
require '../../if_loggedin.php';
include 'mindnet-config.php';

$signup_status = 0;

if (isset($_REQUEST['btn-create-user'])) {

    echo "<script src='https://s3-ap-southeast-1.amazonaws.com/sohcdn1/js/jquery.min.js' type='text/javascript'>
          </script>";

    if (isset($_REQUEST['name']) && isset($_REQUEST['email']) && isset($_REQUEST['password']) && isset($_REQUEST['rpassword'])) {


        $username = $_REQUEST['name'];
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        $rpassword = $_REQUEST['rpassword'];
        $expirydate = $_REQUEST['expiry_date'];


        #---------------Database Connection---------------------#
        $dbhostCBT = "demo.cgbct3oeqwvc.ap-south-1.rds.amazonaws.com";
        $dbportCBT = 3306;
        $dbnameCBT = "sohdbl";
        $dsn1 = "mysql:host={$dbhostCBT};port={$dbportCBT};dbname={$dbnameCBT}";
        $login_user1 = "login_user";
        $login_pass1 = "Bangalore@1";
        #---------------Database Connection---------------------#

        $dbh = new PDO($dsn1, $login_user1, $login_pass1);
        $dbh->query("use sohdbl");

        $stmt = $dbh->prepare("SELECT email,name FROM user_login WHERE email=? LIMIT 1");
        $stmt->execute(array($email));
        if ($stmt->rowCount() != 0) {
            $signup_status = 1;
        } else {

            # No row exits in table with this email, hence an account does not exits.
            # So we will create a new account here
            # Create a random token
            $token = "0000000000";

            echo " <script>
              $.post( 'success.php', { emp_id:'$emp_id', email: '$email', name :'$username', password:'$password',token: '$token', expirydate:'$expirydate' })
              .done(function( status) {
                            if(status=='done'){
              $('.confirm-msg' ).replaceWith(
              '<div class=confirm-msg style=color:#3AB149;><b>$email <b>is now stresscontrolonline user!</div>' );
              }else{
              $('.confirm-msg' ).replaceWith(
              '<div class=confirm-msg style=color:red;><b>$email <b>is not able to register!<br> Please Try again !</div>' );
              }
              });
              </script>";
        }
    } else {
        # Even after Jacript validation that all values need not be blank, Some blank values were submitted, throw a error
        echo 'Some Error Occured. Errror Code : FRDM_FRM_01';
        die();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../../../assets/css/bootstrap-datepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../../../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../assets/lib/select2/css/select2.min.css"/>

    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include '../../top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 col-xs-12">
                                <div class="panel panel-default panel-border-color panel-border-color-primary">
                                    <div class="panel-heading panel-heading-divider">Create Demo User</div>
                                    <div class="panel-body">
                                        <div Style="text-align: center;font-size: 15px;font-weight:bold;">
                                            <?php
                                            if ($signup_status == 1) {
                                                echo "<font color='red'>" . $email . " is already exist!</font>";
                                            }
                                            ?>
                                            <div class="confirm-msg">												
                                            </div>
                                        </div>
                                        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" role="form" method="POST">
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Name </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control"  name="name" placeholder="User name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label"> Email </label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control"  name="email" placeholder="User email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label">Password</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="password" id="register_password" class="form-control"  name="password" placeholder="User password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="col-md-2 col-xs-12 control-label">Re-type Password</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="password" class="form-control" id="r_register_password" name="rpassword" placeholder="Re-type password">
                                                    <div id="r_password" class="error"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-xs-12">
                                                    &nbsp;
                                                </div>
                                                <label class="control-label col-md-2 col-xs-12">Expiry Date</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control" id="datepicker" placeholder="Select date here " name="expiry_date">
                                                </div>

                                            </div>
                                            <br/><br/>
                                            <div class="form-group m-b-0" style="text-align: center;">
                                                <input type="submit" class="btn btn-info waves-effect waves-light" value="Create User" name="btn-create-user"/> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/main.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../../../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../../../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../../../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../../../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                App.formElements();
            });
        </script>
        <script>
            $(".close").click(function () {
                window.top.location.reload();
            });
        </script>
        <script>
            $(".form-horizontal").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 7
                    },
                    rpassword: {
                        required: true,
                    },
                    expiry_date: {
                        required: true,
                    }
                },
                messages: {
                    name: {
                        required: "Please enter the full name"
                    },
                    email: {
                        required: "Please enter the email",
                        email: "Invalid Email ID"
                    },
                    password: {
                        required: "Please enter the password"
                    },
                    rpassword: {
                        required: "Please enter the password"
                    },
                    expiry_date: {
                        required: "Please enter the date"
                    }
                }
            });
        </script>     
        <script type="text/javascript">
            //script to enter only alphabets in text box
            $("#r_register_password").focusout(function () {
                var pass1 = document.getElementById("register_password").value;
                var pass2 = document.getElementById("r_register_password").value;
                if (pass1 != pass2) {
                    //alert("Passwords Do not match");
                    $("#r_password").html("Please enter same password");

                } else {
                    $("#r_password").html(" ");
                }
            });

        </script>
        <script>
            var date = new Date();
            date.setDate(date.getDate());

            $('#datepicker').datepicker({
                startDate: date,
                format: 'dd-mm-yyyy'
            });
        </script>
    </body>
</html>