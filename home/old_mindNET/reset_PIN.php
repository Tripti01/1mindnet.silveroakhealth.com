<?php
#include files
include 'if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

#Start database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

# Select Details from database
$stmt01 = $dbh->prepare("SELECT * FROM emp_login WHERE email=? LIMIT 1");
$stmt01->execute(array($email));
if ($stmt01->rowCount() != 0) {
    # Fetching the records
    $result = $stmt01->fetch();
    $emp_PIN = $result['PIN'];
} else {
    $emp_PIN = '0000';
}


$PIN_status = 0;

if (isset($_REQUEST['PIN-submit-btn'])) {
    if (isset($_REQUEST['PIN']) && !empty($_REQUEST['PIN']) && $_REQUEST['PIN'] !== '' && strlen($_REQUEST['PIN']) == 4 && isset($_REQUEST['confirm_PIN']) && !empty($_REQUEST['confirm_PIN']) && $_REQUEST['confirm_PIN'] !== '' && strlen($_REQUEST['confirm_PIN']) == 4) {

        $PIN = $_REQUEST['PIN'];
        $confirm_PIN = $_REQUEST['confirm_PIN'];

        if ($PIN === $confirm_PIN) {

            $stmt10 = $dbh->prepare("UPDATE emp_login SET PIN=? WHERE emp_id=?");
            $stmt10->execute(array($PIN, $emp_id));

            # PIN is matched
            $PIN_status = 1;
        } else {
            # PIN not match
            $PIN_status = 2;
        }
    } else {
        $PIN_status = 3;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../assets/img/logo-fav.png">
        <title>Silver Oak Health mindNET</title>
        <link rel="stylesheet" type="text/css" href="../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css"/>
        <style>
            .form-control {
                height:35px;
            }
        </style>
    </head>
    <body class="be-splash-screen">
        <div class="be-wrapper be-login">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div style="text-align:center;padding-left:10px;">
                        <img src="../assets/img/logo.png" alt="logo" width="125px" height="80px" class="logo-img">
                    </div>
                    <div class="splash-container">
                        <div class="panel panel-default panel-border-color panel-border-color-primary">
                            <div class="panel-heading"><span class="splash-description">

                                    Reset PIN

                                </span></div>
                            <?php
                            if ($PIN_status == 1) {
                                echo '<B>';
                                echo 'PIN changed successfully.<a href="index.php"> Click here to go to homepage</a>';
                                echo '</B>';
                            } else if ($PIN_status == 2) {
                                echo '<div class="alert alert-danger">';
                                echo 'PIN does not match';
                                echo '</div>';
                            }
                            ?>
                            <div class="panel-body">
                                <form action="reset_PIN.php" method="POST">
                                    <div class="form-group">
                                        <input type="text" name="Current PIN" value="<?php echo $emp_PIN; ?>" placeholder="Current PIN" class="form-control" disabled>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="PIN" placeholder="New PIN" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirm_PIN" placeholder="Confirm PIN" class="form-control">
                                    </div>

                                    <div class="form-group login-submit">
                                        <button data-dismiss="modal" type="submit" name="PIN-submit-btn" class="btn btn-primary btn-xl"> RESET PIN </button>
                                    </div>
                                </form>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <script src="../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>
        <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
            });

        </script>
    </body>
</html>