<?php
session_start();
//if emp_id is not set,go to login
if (!isset($_SESSION['emp_id'])) {
    header("Location: https://app.silveroakhealth.com/mindnet/../login/");
    exit();
} else {
    //fetch details from session
    $emp_id = $_SESSION['emp_id'];
    $emp_name = $_SESSION['emp_name'];
    $emp_first_name=$_SESSION['emp_first_name'];
    $profile_pic = $_SESSION['profile_pic'];
    $prvg_list = $_SESSION['$prvg_list'];
    $email = $_SESSION['email'];
    $emp_PIN = $_SESSION['emp_PIN'];
}
?>