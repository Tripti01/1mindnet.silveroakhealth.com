<?php
include 'mindnet-host.php';
include 'mindnet-config.php';
include 'mindnet-host.php';

session_start();

$_SESSION = array();
if (isset($_COOKIE[session_name()])) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
}
unset($_SESSION['emp_id']);
session_destroy();//destroy everything in session and goto login

header('Location:' . $host . '/../login');

?>