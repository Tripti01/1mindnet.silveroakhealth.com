<?php
//include files
require 'if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

# Dynamic Background Image URL
$img_id = "img" . date('d') . ".jpg";
$img_loc = "../assets/img/gallery/bg.jpg";

#to set default dattime zone 
date_default_timezone_set("Asia/Kolkata");
//to get current year
$cur_year = date("Y");
#getting current month
$curr_month = date("F");
//to get current date
$cur_date = date("dS");
//to get current day
$cur_day = date("l");
//to get current time
$cur_time = date("H:i");


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css"/>
        <style>
            body,html{
                width:100%;
                height:100%;
            }


        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left" style="background-image: url('<?php echo $img_loc; ?>'); background-size:cover;background-repeat: no-repeat;">
           <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include 'top_bar_nav.php'; ?>
            </nav>
            <div class="be-content parallax" >
                <div class="page-head">
                  
                </div>
                <div class="main-content container-fluid" style="margin-top:18%;">
                     <div class="row">
                        <div class="col-md-8" style="text-align: right; padding-right: 40px;" >
                            <BR/><BR/><BR/><BR/><BR/>
                              <div class="row" id="lblGreetings" style="font-size:30px;font-weight:300;"></div>


                                <!--
                                <div style="font-size:70px;font-weight:500;margin-top:-6.5%;"> <?php echo $cur_time; ?> </div> 
                                <div style="font-size:30px;font-weight:300;">Today is <?php echo $cur_day; ?>,<br/><?php echo $cur_date; ?> of <?php echo $curr_month . " " . $cur_year; ?> 

                                !-->
                              
                            
                        </div>
                        <div class="col-md-4" style="border-left: 2px solid grey;">
                         <!--
                         <a href="launchpad.php"> <button class="btn btn-lg btn-space btn-primary"><i class="icon icon-left mdi mdi-apps"></i> Launchpad </button> </a>

                           <a href="../Case_Mgmt/">  <button class="btn btn-lg btn-space btn-primary"><i class="icon icon-left mdi mdi-phone"></i> Case Management </button></a>
                              <BR/>     <BR/>
                                !-->
                           
                   
                        </div>
                     </div>
                </div>
            </div>
        </div>
        <script src="../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>
        <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/lib/prettify/prettify.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();

                //Runs prettify
                prettyPrint();
            });
        </script>
        <script>
            var myDate = new Date();
            var hrs = myDate.getHours();

            var greet;

            if (hrs < 12)
                greet = 'Good Morning';
            else if (hrs >= 12 && hrs <= 15)
                greet = 'Good Afternoon';
            else if (hrs >= 15 && hrs <= 18)
                greet = 'Good Evening';
            else if (hrs >= 18 && hrs <= 24)
                greet = 'Good Evening';

            document.getElementById('lblGreetings').innerHTML =
                    greet + '<B> <?php echo $emp_first_name; ?>!<B/>';
        </script> 
    </body>
</html>