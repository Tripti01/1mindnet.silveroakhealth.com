<?php
//include files
require 'if_loggedin.php';
include 'mindnet-host.php';
include 'mindnet-config.php';

#to check if personal_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['personal_status'])) {
    $personal_status = $_REQUEST['personal_status'];
} else {
    $personal_status = 0;
}
#to check if emergency_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['emergency_status'])) {
    $emergency_status = $_REQUEST['emergency_status'];
} else {
    $emergency_status = 0;
}
#to check if social_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['social_status'])) {
    $social_status = $_REQUEST['social_status'];
} else {
    $social_status = 0;
}
#to check if profile_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['profile_status'])) {
    $profile_status = $_REQUEST['profile_status'];
} else {
    $profile_status = 0;
}
#to check if profile_status from url is passed, and perform accordingly else set all as blank
if (isset($_REQUEST['password_status'])) {
    $password_status = $_REQUEST['password_status'];
} else {
    $password_status = 0;
}

#database connection
$dbh = new PDO($dsn, $login_user, $login_pass);
$dbh->query("use mindnet");

#to fetch the dept_name for the given emp_id
$stmt00 = $dbh->prepare("SELECT dept_name FROM dept_master,emp_dept WHERE dept_master.dept_id = emp_dept.dept_id AND emp_dept.emp_id=? LIMIT 1");
$stmt00->execute(array($emp_id));
if ($stmt00->rowCount() != 0) {
    $row00 = $stmt00->fetch();
    $dept_name = $row00['dept_name']; //dept_name
}

#to fetch all basic details from emp_login,emp_profile tables
$stmt01 = $dbh->prepare("SELECT first_name,last_name,email,gender,DOB,DOJ,job_title FROM emp_login,emp_profile WHERE emp_login.emp_id=? AND emp_login.emp_id = emp_profile.emp_id LIMIT 1");
$stmt01->execute(array($emp_id));
if ($stmt01->rowCount() != 0) {
    $row01 = $stmt01->fetch();
    $first_name = $row01['first_name']; //to fetch first name
    $last_name = $row01['last_name']; //to fetch last name
    $email = $row01['email']; //to fetch email
    $gender = $row01['gender']; //to fetch gender
    $dob = $row01['DOB']; //to fetch date of birth
    $doj = $row01['DOJ']; //to fetch date of joining
    $job_title = $row01['job_title']; //to fetch job title
}

#to fetch all the list of all employees
$j = 0; //counter to fetch list of employees
$stmt02 = $dbh->prepare("SELECT first_name,last_name,emp_id FROM emp_login WHERE active=1 ");
$stmt02->execute();
if ($stmt02->rowCount() != 0) {
    while ($row02 = $stmt02->fetch(PDO::FETCH_ASSOC)) {
        $emp_name_list[$j] = $row02['first_name'] . " " . $row02['last_name']; //to fetch name of employees
        $emp_id_list[$j] = $row02['emp_id']; //to fetch the emp_id
        $j++;
    }
}

#to fetch the dr of the given emp_id 
$stmt03 = $dbh->prepare("SELECT first_name,last_name,emp_dr.dr_of_id FROM emp_dr,emp_login WHERE emp_dr.dr_of_id = emp_login.emp_id AND emp_dr.emp_id=? LIMIT 1 ");
$stmt03->execute(array($emp_id));
if ($stmt03->rowCount() != 0) {//if rowcount != 0
    $row03 = $stmt03->fetch();
    $dr_name = $row03['first_name'] . " " . $row03['last_name']; //to fetch the emp dr name
    $dr_id = $row03['dr_of_id']; //to fetch the emp dr id
} else {//if data is not set
    $dr_name = " "; //set date to blank
    $dr_id = " "; //set id to blank
}

#to fetch all social details
$stmt04 = $dbh->prepare("SELECT fb,tw,li FROM emp_contact WHERE emp_id=? LIMIT 1");
$stmt04->execute(array($emp_id));
if ($stmt04->rowCount() != 0) {
    $row04 = $stmt04->fetch();
    $facebook = $row04['fb']; //carried forward
    $twitter = $row04['tw']; //el_contant
    $linkedin = $row04['li']; //cl_constant
} else {//set all to blank
    $facebook = "";
    $twitter = "";
    $linkedin = "";
}

#to fetch all personal details for the given emp_id
$stmt09 = $dbh->prepare("SELECT phone,phone_alt,personal_email FROM emp_contact WHERE emp_id=? LIMIT 1");
$stmt09->execute(array($emp_id));
if ($stmt09->rowCount() != 0) {
    $row09 = $stmt09->fetch();
    $phone = $row09['phone']; //to fetch phone number
    $phone_alt = $row09['phone_alt']; //to fetch alternate phone number
    $personal_email = $row09['personal_email']; // to fetch a personal email
}

#to fetch address with the latest timestamp for the given emp_id
$stmt10 = $dbh->prepare("SELECT full_addr FROM emp_addr WHERE emp_id=? AND `timestamp`= (SELECT max(`timestamp`) FROM emp_addr WHERE emp_addr.emp_id=? LIMIT 1) LIMIT 1");
$stmt10->execute(array($emp_id, $emp_id));
if ($stmt10->rowCount() != 0) {
    $row10 = $stmt10->fetch();
    $addr = $row10['full_addr']; //to fetch address
} else {
    $addr = "";
}

#to fetch emergency contact details of the given emp_id
$stmt11 = $dbh->prepare("SELECT * FROM emp_emergency WHERE emp_id=? LIMIT 1");
$stmt11->execute(array($emp_id));
if ($stmt11->rowCount() != 0) {
    $row11 = $stmt11->fetch();
    $emergency_name = $row11['emergency_name']; //emergency contact1 name
    $emergency_relation = $row11['emergency_relation']; //emergency contact1 relation with employee
    $emergency_contact = $row11['emergency_contact']; //emergency contact1 contact number
    $emergency_name2 = $row11['emergency_name2']; //emergency contact2 name
    $emergency_relation2 = $row11['emergency_relation2']; //emergency contact2 relation with employee
    $emergency_contact2 = $row11['emergency_contact2']; //emergency contact2 contact number
}

#on click of edit btn- personal contact tab
if (isset($_REQUEST['personal-btn'])) {
    if (isset($_REQUEST['phone']) && !empty($_REQUEST['phone']) && isset($_REQUEST['phone_alt']) && !empty($_REQUEST['phone_alt']) && isset($_REQUEST['per_email']) && !empty($_REQUEST['per_email']) && isset($_REQUEST['new_addr']) && !empty($_REQUEST['new_addr'])) {

        $new_phone = $_REQUEST['phone'];
        $new_phone_alt = $_REQUEST['phone_alt'];
        $new_per_email = $_REQUEST['per_email'];
        $new_addr = $_REQUEST['new_addr'];
        $old_addr = $_REQUEST['addr'];
        $emp_id = $_REQUEST['emp_id'];
        date_default_timezone_set("Asia/Kolkata");
        $timestamp = date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s')));

        $dbh->beginTransaction();
        try {
#to update values in emp_contact
            $stmt18 = $dbh->prepare("UPDATE emp_contact SET phone=?,phone_alt=?,personal_email=? WHERE emp_id=?");
            $stmt18->execute(array($new_phone, $new_phone_alt, $new_per_email, $emp_id));

            //check if old address is equal to new addressor not.
            if ($old_addr != $new_addr) {
                //if old address is not equal new address, then insert into emp_addr
                $stmt19 = $dbh->prepare("INSERT INTO emp_addr VALUES (?,?,?,?,?)");
                $stmt19->execute(array('', $emp_id, $new_addr, $timestamp, ''));
            }

            $personal_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $personal_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="' . $host . '/my_profile.php?personal_status=' . $personal_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:[MYACC1000]");
    }
}

#on click of edit btn- emergency contact tab
if (isset($_REQUEST['emergency-btn'])) {
    if (isset($_REQUEST['emergency_name']) && !empty($_REQUEST['emergency_name']) && isset($_REQUEST['emergency_contact']) && !empty($_REQUEST['emergency_contact']) && isset($_REQUEST['emergency_relation']) && !empty($_REQUEST['emergency_relation']) && isset($_REQUEST['emergency_name2']) && !empty($_REQUEST['emergency_name2']) && isset($_REQUEST['emergency_contact2']) && !empty($_REQUEST['emergency_contact2']) && isset($_REQUEST['emergency_relation2']) && !empty($_REQUEST['emergency_relation2'])) {

        $new_emergency_name = $_REQUEST['emergency_name'];
        $new_emergency_phone = $_REQUEST['emergency_contact'];
        $new_emergency_relation = $_REQUEST['emergency_relation'];
        $new_emergency_name2 = $_REQUEST['emergency_name2'];
        $new_emergency_phone2 = $_REQUEST['emergency_contact2'];
        $new_emergency_relation2 = $_REQUEST['emergency_relation2'];
        $emp_id = $_REQUEST['emp_id'];

        $dbh->beginTransaction();
        try {
#to update values in emp_emergency
            $stmt20 = $dbh->prepare("UPDATE emp_emergency SET emergency_name=?,emergency_contact=?,emergency_relation=?,emergency_name2=?,emergency_contact2=?,emergency_relation2=? WHERE emp_id=?");
            $stmt20->execute(array($new_emergency_name, $new_emergency_phone, $new_emergency_relation, $new_emergency_name2, $new_emergency_phone2, $new_emergency_relation2, $emp_id));

            $emergency_status = 1; //successfull entry to database

            $dbh->commit();
        } catch (PDOException $e) {
            $dbh->rollBack();
            $emergency_status = 2; //transacion failed
        }
        echo '<script>window.top.location.href="' . $host . '/my_profile.php?emergency_status=' . $emergency_status . '"</script>';
    } else {
//  something is wrong the client side validation 
//  either the client has disabled the javascript stop the execution of the script
        die("Some empty field were submitted, Please enable your javascript if it is disabled. If this problem persists contact us at help@stresscontrolonline.com. Error Code:[MYACC1001]");
    }
}
#on click of edit btn- Social tab
if (isset($_REQUEST['social-btn'])) {

    if ($_REQUEST['facebook']) {
        $fb = $_REQUEST['facebook'];
    } else {
        $fb = " ";
    }
    if ($_REQUEST['twitter']) {
        $tw = $_REQUEST['twitter'];
    } else {
        $tw = " ";
    }
    if ($_REQUEST['linkedin']) {
        $li = $_REQUEST['linkedin'];
    } else {
        $li = "";
    }
    $emp_id = $_REQUEST['emp_id'];

    $dbh->beginTransaction();
    try {
#to update values in emp_contact
        $stmt18 = $dbh->prepare("UPDATE emp_contact SET fb=?,li=?,tw=? WHERE emp_id=?");
        $stmt18->execute(array($fb, $li, $tw, $emp_id));

        $social_status = 1; //successfull entry to database

        $dbh->commit();
    } catch (PDOException $e) {
        $dbh->rollBack();
        $social_status = 2; //transacion failed
    }
    echo '<script>window.top.location.href="' . $host . '/my_profile.php?social_status=' . $social_status . '"</script>';
}
##to change password
#on click of change password btn
if (isset($_POST['ChangePwd'])) {
    //Fetch password from database.
    $stmt9 = $dbh->prepare("SELECT password FROM emp_login WHERE emp_id=? LIMIT 1");
    $stmt9->execute(array($emp_id)) or die(print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[MYACC1103]."));
    $user_password = $stmt9->fetch();
    $db_password = $user_password['password']; //to fetch password from database
    #to get password details
    $cur_password = $_POST['cur_password'];
    $confirm_password = $_POST['password_again'];
    $newpassword = $_POST['password'];

    // password verification
    if (password_verify($cur_password, $db_password)) {
        if ($cur_password != $newpassword) {

            $hash_options = array('cost' => 11); //creating salt to be added to password
            $hashed_password = password_hash($newpassword, PASSWORD_DEFAULT, $hash_options); //hashing password
            //hashed password is updated to the database. 
            $stmt10 = $dbh->prepare("UPDATE emp_login SET password=? WHERE emp_id=?");
            $stmt10->execute(array($hashed_password, $emp_id)) or die(print_r("Some error occured. Please try again. If the error persists please contact us at help@stresscontrolonline.com.ErrorCode[MYACC1104]."));
            $password_status = 1; //password updated successfully
        } else {
            $password_status = 2; //new and old password are same
        }
    } else {
        $password_status = 3; //current password are incorrect
    }
    echo '<script>window.top.location.href="' . $host . '/my_profile.php?password_status=' . $password_status . '"</script>';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../assets/img/logo-fav.png">
        <title>mindNET</title>
        <link rel="stylesheet" type="text/css" href="../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>

        <link rel="stylesheet" type="text/css" href="../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../assets/lib/select2/css/select2.min.css"/>
        <style>
            .pull-right {
                float: right!important;
            }
            .tab_custom{
                border: 1px solid transparent;
            }
            .tbl_heading{
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-weight: 700;
                color: #666666;
            }
            .colon{
                text-align: left;
            }
            .form-start{
                margin-left: 2%;
            }
            .pass{

            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-nosidebar-left">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <?php include 'top_bar_nav.php'; ?>
            </nav>
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <!--Default Tabs-->
                        <div class="col-md-10 col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="z-index:0;"><b>My Profile</b></div>
                                <div class="tab-container" >
                                    <div class="nav tab_custom" style="z-index:1;margin-top: -4%;">
                                        <ul class="nav nav-tabs pull-right">
                                            <li id="basic_tab" class="active"><a href="#basic" data-toggle="tab">Basic</a></li>
                                            <li id="contact_tab"><a href="#contact" data-toggle="tab">Contact</a></li>
                                            <li id="social_tab"><a href="#social" data-toggle="tab">Social</a></li>
                                            <li id="profile_tab"><a href="#profile" data-toggle="tab">Profile Picture</a></li>
                                          <!--  <li id="psswrd_tab"><a href="#psswrd" data-toggle="tab">Change Password</a></li> !-->
                                          
                                        </ul>
                                    </div>
                                    <div class="tab-content" >
                                        <!--- Basic Contact Details tab--->
                                        <div id="basic" class="tab-pane active cont">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading panel-heading-divider">Basic Details</div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:1%;"></th>
                                                                        <th style="width:28%;"></th>
                                                                        <th style="width:15%;"></th>
                                                                        <th></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Name</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $first_name . " " . $last_name; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Email</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $email; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Gender</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php
                                                                                        if ($gender == "F") {
                                                                                            echo "Female";
                                                                                        } else if ($gender == "M") {
                                                                                            echo "Male";
                                                                                        }
                                                                                        ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Date of Birth</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php
                                                                                        if ($dob == "0000-00-00") {
                                                                                            echo " ";
                                                                                        } else {
                                                                                            echo date("d-m-Y", strtotime($dob));
                                                                                        }
                                                                                        ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Date of Joining</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php
                                                                                        if ($doj == "0000-00-00") {
                                                                                            echo " ";
                                                                                        } else {
                                                                                            echo date("d-m-Y", strtotime($doj));
                                                                                        }
                                                                                        ?></label></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading panel-heading-divider">Department Details</div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:1%;"></th>
                                                                        <th style="width:25%;"></th>
                                                                        <th style="width:15%;"></th>
                                                                        <th></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Department</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $dept_name; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Job Title</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $job_title; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Reports to</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $dr_name; ?></label></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--- Contact Details tab--->
                                        <div id="contact" class="tab-pane">
                                            <div class="row">
                                                <?php
                                                if (($personal_status == 0) && ($emergency_status == 0)) {
                                                    //do nothing
                                                } else if (($personal_status == 1) || ($emergency_status == 1)) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                                } else if (($personal_status == 2) || ($emergency_status == 2)) {
                                                    //transactiom failed
                                                    echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading panel-heading-divider">Personal Contact Details</div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:1%;"></th>
                                                                        <th style="width:24%;"></th>
                                                                        <th style="width:9%;"></th>
                                                                        <th style="width:38%;"></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Contact</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $phone; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Alternate Contact</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $phone_alt; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Personal Email</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $personal_email; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Address</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $addr; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    <a data-toggle="modal" data-target="#form-bp_personal" type="button" data-backdrop="static" data-keyboard="false">Edit</a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--- Personal Contact Details tab--->
                                                    <div id="form-bp_personal" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                        <div class="modal-dialog custom-width">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                                    <h3 class="modal-title">Edit Contact</h3>
                                                                </div>
                                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed personal-form">
                                                                    <div class="modal-body">
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left:.4%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Contact</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="phone" value="<?php echo $phone; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left:.4%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Alternate Contact</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="phone_alt" value="<?php echo $phone_alt; ?>"  class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left:.4%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Personal Email</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="per_email" value="<?php echo $personal_email; ?>"  class="form-control" onpaste="return false;" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left:.4%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Address</label>
                                                                                <div class="col-sm-8">
                                                                                    <textarea name="new_addr"  class="form-control"><?php echo $addr; ?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>"/>
                                                                        <input type="hidden" name="addr" value="<?php echo $addr; ?>"/>
                                                                        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                        <input type="submit" name="personal-btn" class="btn btn-primary" value="Save">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>      
                                                </div>  
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading panel-heading-divider">Emergency Contact Details</div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:7%;"></th>
                                                                        <th style="width:22%;"></th>
                                                                        <th style="width:9%;"></th>
                                                                        <th style="width:38%;"></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td colspan="4"><label class="control-label tbl_heading">Emergency Contact 1</label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Name</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_name; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Relation</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_relation; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Contact Number</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_contact; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4"><label class="control-label tbl_heading">Emergency Contact 2</label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Name</label></td>
                                                                                <td><label class="control-label tbl_heading colon">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_name2; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Relation</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_relation2; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Contact Number</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $emergency_contact2; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <a data-toggle="modal" data-target="#form-bp_emergency" type="button" data-backdrop="static" data-keyboard="false">Edit</a>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal- Form to add emergency contact details --->
                                                    <div id="form-bp_emergency" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                        <div class="modal-dialog custom-width">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                                    <h3 class="modal-title">Edit Contact</h3>
                                                                </div>
                                                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border-radius: 0px;" class="form-horizontal group-border-dashed emergency-form">
                                                                    <div class="modal-body">
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 0.1%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 tbl_heading">Emergency Contact 1</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Name</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" value="<?php echo $emergency_name; ?>" name="emergency_name" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Relation</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_relation" value="<?php echo $emergency_relation; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Contact Number</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_contact" value="<?php echo $emergency_contact; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left:0.1%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 tbl_heading">Emergency Contact 2</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Name</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_name2" value="<?php echo $emergency_name2; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Relation</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_relation2" value="<?php echo $emergency_relation2; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom:0.5%;margin-left: 2.5%;">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 tbl_heading">Contact Number</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="emergency_contact2" value="<?php echo $emergency_contact2; ?>" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>"/>
                                                                        <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                        <input type="submit" name="emergency-btn" class="btn btn-primary" value="Save">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>      
                                                </div>
                                            </div>
                                        </div>
                                        <!--- Privilage Details tab--->
                                        <div id="social" class="tab-pane">
                                            <div class="row">
                                                <?php
                                                if ($social_status == 0) {
                                                    //do nothing
                                                } else if ($social_status == 1) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Successfully made the changes.</b></div>';
                                                } else if ($social_status == 2) {
                                                    //transactiom failed
                                                    echo '<div class="alert alert-danger"><b>Transaction failed. Try Again!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>
                                                <div class="col-md-7">
                                                    <div class="panel">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table style="width:100%;margin-top: 2%;">
                                                                        <thead>
                                                                        <th style="width:1%;"></th>
                                                                        <th style="width:11%;"></th>
                                                                        <th style="width:14%;"></th>
                                                                        <th></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">FaceBook</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $facebook; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">Twitter</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $twitter; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><label class="control-label tbl_heading">LinkedIn</label></td>
                                                                                <td><label class="control-label tbl_heading">:</label></td>
                                                                                <td><label class="control-label"><?php echo $linkedin; ?></label></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td colspan="3">
                                                                                    <a data-toggle="modal" data-target="#form-bp1_social"  type="button" data-backdrop="static" data-keyboard="false">
                                                                                        Edit</a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!---Moadl - Form to fill Social Details --->
                                            <div id="form-bp1_social" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                                <div class="modal-dialog custom-width">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="padding-bottom: 20px;padding-top: 20px;">
                                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close" onClick="window.location.reload()" onClick="window.location.reload()"><span class="mdi mdi-close"></span></button>
                                                            <h3 class="modal-title">Edit Social Details </h3>
                                                        </div>
                                                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="form-horizontal group-border-dashed social-form">
                                                            <div class="modal-body" style="padding-bottom:40px;">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-3 tbl_heading">FaceBook</label>
                                                                        <div class="col-sm-8">
                                                                            <input type="text" name="facebook" placeholder="https://www.facebook.com/YourId" value="<?php echo $facebook; ?>" class="form-control ">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-3 tbl_heading">Twitter</label>
                                                                        <div class="col-sm-8">
                                                                            <input type="text" name="twitter" placeholder="https://twitter.com/YourId" value="<?php echo $twitter; ?>" class="form-control ">
                                                                        </div></div>
                                                                </div>
                                                                <div class="form-group"> 
                                                                    <div class="row">
                                                                        <label class="control-label col-sm-3 tbl_heading">LinkedIn</label>
                                                                        <div class="col-sm-8">
                                                                            <input type="text" name="linkedin" placeholder="https://www.linkedin.com/in/YourId" value="<?php echo $linkedin; ?>" class="form-control ">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>"/>
                                                                    <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                                                                    <input type="submit" value="Save" name="social-btn" class="btn btn-primary"/>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="profile" class="tab-pane">
                                            <div class="row">
                                                <?php
                                                if ($profile_status == 0) {
                                                    //do nothing
                                                } else if ($profile_status == 1) {
                                                    //successfully created employee
                                                    echo '<div class="alert alert-success"><b>Profile photo uploaded successfully!</b></div>';
                                                } else {
                                                    
                                                }
                                                ?>

                                                <div class="col-md-6">
                                                    <div  id="img-upload-msg-div">
                                                    </div>
                                                    <form role="form" id="update-logo-form"  action="javascript:void(0);" method="POST" enctype="multipart/form-data">
                                                        <div class="col-md-12 panel">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <table style="width:100%;margin-top: 2%;">
                                                                    <thead>
                                                                    <th style="width:1%;"></th>
                                                                    <th style="width:32%;"></th>
                                                                    <th style="width:16%;"></th>
                                                                    <th></th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td><label class="control-label tbl_heading">Current Profile Picture</label></td>
                                                                            <td><label class="control-label tbl_heading">:</label></td>
                                                                            <td><label class="control-label"><div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
                                                                                        <img src="../assets/img/profile_pic/<?php get_profile_pic($emp_id); ?>" alt="user-img">
                                                                                    </div></label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td><label class="control-label tbl_heading">Choose Profile Picture</label></td>
                                                                            <td><label class="control-label tbl_heading">:</label></td>
                                                                            <td><label class="control-label">
                                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;">
                                                                                    </div> 
                                                                                    <input type="file" name="image-upload-input" id="uploadImage">
                                                                                    <label style="font-size:11px;">
                                                                                        Max file size: 1 MB <BR/> (type allowed: <b> .png, .jpeg, .jpg, .gif</b>)
                                                                                    </label>
                                                                                </label>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td></td>
                                                                            <td colspan="4"><input type="hidden" name="emp_id" value="<?php echo $_SESSION['emp_id'] ?>">
                                                                                <input type="hidden" name="MAX_FILE_SIZE" value="3145728" />
                                                                                <input id="btn-save" type="submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" value="Save Profile Picture">
                                                                            </td>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="psswrd" class="tab-pane">
                                            <div class="row">
                                                <div style="text-align:center; color:#2ABB2A;font-weight:bolder">
                                                    <!--Display message when password is changed.-->
                                                    <?php if ($password_status == 1) {//Password changed successfully   ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="alert alert-success">
                                                                    <p>
                                                                        <?php echo "Password changed successfully"; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } else if ($password_status == 2) {//New password and current password are same   ?>                                                
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="alert alert-danger">
                                                                    <p>
                                                                        <?php echo "New password and current password are same."; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>																
                                                    <?php } else if ($password_status == 3) {//Current Password is Incorrect
                                                        ?>                                                
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="alert alert-danger">
                                                                    <p>
                                                                        <?php echo "Current Password is Incorrect.Please try again"; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>																
                                                        <?php
                                                    } else {
                                                        
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12" style="margin-top:2%;">
                                                                    <form id="change_pass_form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                                                        <div class="form-group">
                                                                            <div class="row pass">
                                                                                <div class="col-sm-5">
                                                                                    <label class="control-label tbl_heading">Current Password</label><font color="RED">*</font>
                                                                                </div>
                                                                                <div class="col-sm-7">
                                                                                    <input class="form-control placeholder-no-fix" id="cur_password" type="password" placeholder="Current Password" name="cur_password"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row pass">
                                                                                <div class="col-sm-5">
                                                                                    <label class="control-label tbl_heading">New Password</label><font color="RED">*</font>
                                                                                </div>
                                                                                <div class="col-sm-7">
                                                                                    <input type="password" name="password" id="password" class="form-control" placeholder="New Password">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row pass">
                                                                                <div class="col-sm-5">
                                                                                    <label class="control-label tbl_heading">Re-type New Password</label><font color="RED">*</font>
                                                                                </div>
                                                                                <div class="col-sm-7">
                                                                                    <input type="password" name="password_again" id="password_again" class="form-control" placeholder="Re-type New Password">
                                                                                    <div id="error_psswrd" class="error" style="margin-left:0%;"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="margin-top-10">
                                                                                <input type="submit" class="btn btn-primary"  name="ChangePwd" value="Change Password" >
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>
        <script src="../assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../assets/lib/jquery.nestable/jquery.nestable.js" type="text/javascript"></script>
        <script src="../assets/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
        <script src="../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../assets/lib/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
        <script src="../assets/js/app-form-elements.js" type="text/javascript"></script>
        <script src="../assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/js/additional-method-min.js" type="text/javascript"></script>
        <script src="../assets/js/bootstrap-fileinput.js" type="text/javascript"></script>
        <script>
                                                                $(document).ready(function () {
                                                                    //initialize the javascript
                                                                    App.init();
                                                                    App.formElements();
                                                                });
        </script>
        <script>
<?php if (($personal_status == 1) || ($personal_status == 2) || ($emergency_status == 1) || ($emergency_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#contact_tab").addClass("active");
                    $("#contact").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if (($social_status == 1) || ($social_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#contact_tab").removeClass("active");
                    $("#contact").removeClass("active");
                    $("#social_tab").addClass("active");
                    $("#social").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if (($profile_status == 1) || ($profile_status == 2)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#contact_tab").removeClass("active");
                    $("#contact").removeClass("active");
                    $("#social_tab").removeClass("active");
                    $("#social").removeClass("active");
                    $("#profile_tab").addClass("active");
                    $("#profile").addClass("active");
                });
<?php } ?>
        </script>
        <script>
<?php if (($password_status == 1) || ($password_status == 2) || ($password_status == 3)) { ?>
                $(document).ready(function () {
                    $("#basic_tab").removeClass("active");
                    $("#basic").removeClass("active");
                    $("#contact_tab").removeClass("active");
                    $("#contact").removeClass("active");
                    $("#social_tab").removeClass("active");
                    $("#social").removeClass("active");
                    $("#profile_tab").removeClass("active");
                    $("#profile").removeClass("active");
                    $("#psswrd_tab").addClass("active");
                    $("#psswrd").addClass("active");
                });
<?php } ?>
        </script>
        <!---Form validation---->
        <script>
            $(".personal-form").validate({
                rules: {
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    phone_alt: {
                        required: true,
                        phoneUS: true
                    },
                    per_email: {
                        required: true,
                        email: true,
                    },
                    new_addr: {
                        required: true,
                    }
                },
                messages: {
                    phone: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    phone_alt: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    per_email: {
                        required: "Please enter email",
                        email: "Invalid Email ID",
                    },
                    new_addr: {
                        required: "Please enter address",
                    }
                }
            });
        </script>
        <script>
            $(".emergency-form").validate({
                rules: {
                    emergency_name: {
                        required: true,
                    },
                    emergency_contact: {
                        required: true,
                        phoneUS: true
                    },
                    emergency_relation: {
                        required: true,
                    },
                    emergency_name2: {
                        required: true,
                    },
                    emergency_contact2: {
                        required: true,
                        phoneUS: true
                    },
                    emergency_relation2: {
                        required: true,
                    }
                },
                messages: {
                    emergency_name: {
                        required: "Please enter name",
                    },
                    emergency_contact: {
                        required: "Please enter mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    emergency_relation: {
                        required: "Please enter relation",
                    },
                    emergency_name2: {
                        required: "Please enter name",
                    },
                    emergency_contact2: {
                        required: "Please enter alternate mobile number",
                        maxlength: "Please enter a valid mobile number",
                    },
                    emergency_relation2: {
                        required: "Please enter relation",
                    },
                }
            });
        </script>
        <script>
            jQuery(document).ready(function () {
                $("#change_pass_form").validate({
                    rules: {
                        cur_password: {
                            required: true,
                        },
                        password: {
                            required: true,
                            minlength: 7
                        },
                        password_again: {
                            required: true,
                        }
                    },
                    messages: {
                        cur_password: {
                            required: "Please enter the current password"
                        },
                        password: {
                            required: "Please enter the password"
                        },
                        password_again: {
                            required: "Please enter the password"
                        }
                    }

                });
            });
        </script>
        <script>
            // form submit update-profile pic-form
            $("form#update-logo-form").submit(function (event) {

                var imgVal = $('#uploadImage').val();

                if (imgVal == '')
                {
                    $('#img-upload-msg-div').removeClass('alert alert-success');
                    $('#img-upload-msg-div').addClass('alert alert-danger');
                    $('#img-upload-msg-div').html('Please select a picture');

                } else {

                    form_dom = $(this);

                    //disable the default form submission
                    event.preventDefault();
                    //  grab all form data  
                    var formData = new FormData($(this)[0]);

                    $("#btn-save", form_dom).val("updating...");
                    $("#btn-save", form_dom).attr("disabled", "disabled");

                    //	retyped password matches
                    $.ajax({
                        url: "ajax/updatelogo_ajax.php",
                        type: "post",
                        data: formData,
                        async: true,
                        processData: false,
                        cache: false,
                        contentType: false,
                        success: function (response) {
                            if (response == 1) {
                                $('#img-upload-msg-div').removeClass('alert alert-danger');
                                $('#img-upload-msg-div').addClass('alert alert-success');
                                $('#img-upload-msg-div').html('Profile photo uploaded successfully!');
                                window.top.location.href = "<?php echo $host . '/my_profile.php'; ?>?profile_status=1";

                            } else {

                                $('#img-upload-msg-div').removeClass('alert alert-success');
                                $('#img-upload-msg-div').addClass('alert alert-danger');
                                if (response == 0) {
                                    $('#img-upload-msg-div').html('File-size limit of 1MB exceeded!');
                                } else if (response == 2) {
                                    $('#img-upload-msg-div').html('Error saving image on the server, try again!');
                                }
                            }
                            $("#btn-save", form_dom).val("Save changes");
                            $("#btn-save", form_dom).removeAttr("disabled");

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#btn-save", form_dom).val("Save changes");
                            $("#btn-save", form_dom).removeAttr("disabled");
                        }
                    });
                }
            });
        </script>  
        <script type="text/javascript">
            //script to enter only alphabets in text box
            $("#password_again").focusout(function () {

                var pass1 = document.getElementById("password").value;
                var pass2 = document.getElementById("password_again").value;

                if (pass1 != pass2) {
                    //alert("Passwords Do not match");
                    $("#error_psswrd").html("Please enter same password");

                } else {
                    $("#error_psswrd").html(" ");
                }
            });

        </script>
    </body>
</html>