<?php

require '../if_loggedin.php';
include 'mindnet-config.php';
include('../SimpleImage/src/abeautifulsite/SimpleImage.php');

if (isset($_FILES['image-upload-input']['type']) &&
        $_SESSION['emp_id'] === $_POST['emp_id']) {

    //  all the values are set
    //  form is submitted by user who is stored in our session only (used employer_identification)
    //	getting all the values
    $file = $_FILES['image-upload-input'];
    $emp_id = $_POST['emp_id'];


    //	continue the execution of the PHP script
    if ($file['size'] > 0 && $file['size'] < 3145728) {

        //	file uploaded is a valid file
        //	file uploaded is within 10MB size
        //  checking the file type of the logo uploaded by the user
        //  File types allowed are .png, .jpeg, .gif
        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
        $detectedType = exif_imagetype($file['tmp_name']);


        if (in_array($detectedType, $allowedTypes)) {

            //	A file with valid extension is submitted
            //	using pathinfo() to get the extension thr extension of the file
            $fileinfo = pathinfo($file['name']);

            #	******  !IMPORTANT  *******
            //	A unique filename is generated using function uniqid()
            //	If the filename is already used, generate another one
            while (true) {
                $uniqueID = uniqid($emp_id . "_", false);
                $target_save_folder = "../../assets/img/profile_pic/" . $uniqueID . "." . $fileinfo['extension'];
                if (!file_exists($target_save_folder)) {
                    break;
                }
            }
            $file_name = $uniqueID . "." . $fileinfo['extension'];
            #  SimpleImage is an image-manipulation PHP Library
            #  Used here to compress and reduce image size
            #  helps in saving server space and also time rendering image in HTML

            try {
                $img = new abeautifulsite\SimpleImage($file['tmp_name']);
                //	resized image's width and height is based on the aspect ratio of the original image
                $old_x = $img->get_width();
                $old_y = $img->get_height();
                $new_width = 240;
                $new_height = 240;

                if ($old_x > $old_y) {
                    $thumb_w = $new_width;
                    $thumb_h = $old_y * ($new_height / $old_x);
                }

                if ($old_x < $old_y) {
                    $thumb_w = $old_x * ($new_width / $old_y);
                    $thumb_h = $new_height;
                }

                if ($old_x === $old_y) {
                    $thumb_w = $new_width;
                    $thumb_h = $new_height;
                }

                $img->thumbnail($thumb_w, $thumb_h)->save($target_save_folder);
                //	NOW UPDATING THE TABLE WITH THE UPLOADED IMAGE'S PATH
                $dbh = new PDO($dsn, $login_user, $login_pass);
                $dbh->query("use mindnet");
                $stmt01 = $dbh->prepare("UPDATE `emp_profile` SET `profile_pic`= ? WHERE `emp_id`= ?");
                $stmt01->execute(array($file_name, $emp_id));
                $_SESSION['profile_pic'] = $file_name;
                echo '1'; //done				
            } catch (Exception $e) {
                //	If resize of image fails due to some reason
                //	just move the uploaded image to ../img/employerKeyPeoplePicture
                if (!move_uploaded_file($file['tmp_name'], $target_save_folder)) {
                    echo '2';
                    //header("HTTP/1.1 604 error saving image on the server, try again!");
                    //exit();
                }
            }
        }
    } else {

        //	file size exceeded or not a valid file
        echo '0';
        //header("HTTP/1.1 602 file-size limit of 10MB exceeded");
        //exit();
    }
} else {
    echo "3"; // file not choosen
}
?>