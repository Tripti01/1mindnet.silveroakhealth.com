<?php
include 'mindnet-config.php';
include 'mindnet-host.php';
include 'mindnet/functions/get_profile_pic.php';
# Display the Custom Links 
$prvg_list_array = $_SESSION['$prvg_list'];
?>
<!-- top nav bar . commom to all employees --->

<style>
    hr {
        display: block;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
        border-top: 1px dotted #D3D3D3;
    }
</style>

<div class="container-fluid">
    <div class="navbar-header"><a href="<?php echo $host; ?>/index.php" class="navbar-brand"></a></div>
    <div class="be-right-navbar">
        <ul class="nav navbar-nav navbar-right be-user-nav">
            <li class="dropdown">
                <a href="<?php echo $host; ?>/index.php" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                    <img src="<?php echo $host; ?>/../assets/img/profile_pic/<?php get_emp_profile_pic($emp_id); ?>" style="border-radius: 50%;" height="50" width="50" alt="Avatar"></a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <div class="user-info">
                            <div class="user-name"><?php echo $emp_name; ?></div>
                        </div>
                    </li>
                    <li><a href="<?php echo $host; ?>/my_profile.php"><span class="icon mdi mdi-face"></span> Profile </a></li>
                    <li><a href="<?php echo $host; ?>/logout.php"><span class="icon mdi mdi-power"></span> Logout</a></li>
                </ul>
            </li>
        </ul>

    </div><a href="#" data-toggle="collapse" data-target="#be-navbar-collapse" class="be-toggle-top-header-menu collapsed">Menu</a>
    <div id="be-navbar-collapse" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">

            <li><a href="<?php echo $host; ?>/index.php"><B>Home </B></a></li>
             <li class="dropdown">
                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                 <B>   Emoloyee Services </B>
                    <span class="mdi mdi-caret-down"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<?php echo $host;?>/modules/leave/index.php"> Holidays </a></li>
                  
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                    <B> My Links </B>
                    <span class="mdi mdi-caret-down"></span>
                </a>
                <ul role="menu" class="dropdown-menu">

                    <?php
                    if (in_array("SCO", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/activation_link/index.php"> SCO - Send SCO Activation Link </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/ewap_users/index.php">SCO - EWAP Create SCO User </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/generate_certificate/index.php"> SCO - Download Certificate </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/demo_users/demo_user_view.php">SCO - Demo User Management </a></li>';
                        echo "<HR/>";
                    }

                    if (in_array("ADMIN_EWAP", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/ewap_content/index.php?type=VDEO">EWAP - Add a New Content </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/ewap_content/add_article.php">EWAP - Article Management </a></li>';
                        echo "<HR/>";
                    }

                    if (in_array("ADMIN_THERAPIST", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/admin/therapistadmin/index.php">ADMIN - Therapist Management </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/admin/therapistadmin/assign_therapist.php">ADMIN - Assign/Re-Assign Therapist </a></li>';
                        echo "<HR/>";
                    }
                    if (in_array("TRANQUIL", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/tranquil/create_premium_code.php">TRANQUIL -  Premium Codes </a></li>';
                        echo "<HR/>";
                    }


                    if (in_array("PSY_NETWORK", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/psychologistnetwork/index.php">NETWORK - Affiliate Psychologist List </a></li>';
                        echo "<HR/>";
                    }

                    if (in_array("ACTIVITY_MARKETING", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/marketing/activity_tracking.php"> ACTIVITY - Marketing Tracking </a></li>';
                        echo "<HR/>";
                    }
                    if (in_array("ACTIVITY_CORPORATE", $prvg_list_array)) {
                        echo '<li><a href="' . $host . '/../mindNET/modules/admin/corporateadmin/activity_tracking.php"> ACTIVITY - Corporate Events </a></li>';
                        echo "<HR/>";
                    }
                    ?>
                </ul>
            </li>
            
            <?php
            ######  REPORTS ########
            if (in_array("REPORTS", $prvg_list_array)) {
                ?>    
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                        <B>   Reports </B>
                        <span class="mdi mdi-caret-down"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <?php
                        echo '<li><a href="' . $host . '/../mindNET/modules/counsellors_activity/index.php"> REPORT - All Counsellos Activity </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/ewap_usage_report/index.php">REPORT - EWAP Usage Report Type 1</a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/ewap_usage_report2/index.php">REPORT - EWAP Usage Report Type 2 </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/ewap_overall_usage/index.php">REPORT - EWAP Overall Usage Report </a></li>';
                        echo '<li><a href="' . $host . '/../mindNET/modules/corporate_engagement_tracker/index.php"> REPORT - Corporate Engagement Tracker </a></li>';
                        echo "<HR/>";
                        ?>
                    </ul>
                </li>
                <?php
            }
            ?>
                
            
            <?php
            ######  CORPORATE ADMIN  ########
            if (in_array("ADMIN_CORPORATE", $prvg_list_array)) {
                ?>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                        <B>   Corporate Admin </B>
                        <span class="mdi mdi-caret-down"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="https://app.silveroakhealth.com/mindNET/modules/admin/corporateadmin/">ADMIN - Corporate Management </a></li>
                    </ul>
                </li>
                <?php
            }
            ?>
                
            
            <?php
            ######  EMPLOYEE  ADMIN  ########
            if (in_array("ADMIN_EMPLOYEE", $prvg_list_array)) {
                ?>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"> 
                        <B>   Employee Admin </B>
                        <span class="mdi mdi-caret-down"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="https://app.silveroakhealth.com/mindNET/modules/emp_mgmt/view_emp.php"> HR - Employee Management </a></li>
                    </ul>
                </li>
                <?php
            }
            ?>

        </ul>
    </div>
</div>