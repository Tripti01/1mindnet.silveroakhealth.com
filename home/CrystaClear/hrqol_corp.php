<?php include "qol-config.php"?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">
    <title>Crystal Clear - HrQol Corporate Count</title>
  
    <link href="../../assets/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../../assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../../assets/lib/jqvmap/jqvmap.min.css" rel="stylesheet">
  
    <link rel="stylesheet" href="../../assets/css/dashforge.css">
    <link rel="stylesheet" href="../../assets/css/dashforge.dashboard.css">
  </head>
  <body>
    <div class="content ht-100v pd-0">
      <div class="content-header">
        <div class="content-search">
          <i data-feather="search"></i>
          <input type="search" class="form-control" placeholder="Search...">
        </div>
        <nav class="nav">
          <a href="" class="nav-link"><i data-feather="help-circle"></i></a>
          <a href="" class="nav-link"><i data-feather="grid"></i></a>
          <a href="" class="nav-link"><i data-feather="align-left"></i></a>
        </nav>
      </div><!-- content-header -->
      <div class="content-body">
        <div class="container pd-x-0">





        </div>
      </div>
    </div>

    <script src="../../assets/lib/jquery/jquery.min.js"></script>
    <script src="../../assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../assets/lib/feather-icons/feather.min.js"></script>
    <script src="../../assets/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="../../assets/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../../assets/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="../../assets/lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../assets/lib/jqvmap/maps/jquery.vmap.usa.js"></script>
    <script src="../../assets/js/dashforge.js"></script>
    <script src="../../assets/js/dashforge.aside.js"></script>
  </body>
</html>
