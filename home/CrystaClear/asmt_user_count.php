<?php


#file inclusion for various function happening in the ui
include 'if_loggedin.php';
include 'asmt/asmt-config.php';
include 'ewap-config.php';

$dbh = new PDO($dsn_asmt, $berlin_public, $berlin_pass);
$dbh->query("use berlin");


#----------------------------AYS---------------------------------------

#User Count
$stmt00 = $dbh->prepare("SELECT count(uid) AS total_user_count FROM user_type WHERE corp_id=? AND asmt_type='AYS' ");
$stmt00->execute(array($corp_id));
$row00 = $stmt00->fetch();
$total_user_count = $row00['total_user_count'];

#Anxiety Count
$stmt100 = $dbh->prepare("SELECT level FROM asmt_scale_score WHERE uid IN (SELECT uid FROM user_type WHERE corp_id=? AND asmt_type='AYS') AND scale=? ; ");
$stmt100->execute(array($corp_id,  "A"));
if ($stmt100->rowCount() != 0)
{
	$i = 0;
	while($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)){
		$level[$i] = $row100['level'];
		$i++;
	}
		
	$anxiety_count = array_count_values($level);	
}else{
	$anxiety_count = array();
}

#Depression Count
$stmt100 = $dbh->prepare("SELECT level FROM asmt_scale_score WHERE uid IN (SELECT uid FROM user_type WHERE corp_id=? AND asmt_type='AYS') AND scale=? ; ");
$stmt100->execute(array($corp_id,  "D"));
if ($stmt100->rowCount() != 0)
{
	$i = 0;
	while($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)){
		$level[$i] = $row100['level'];
		$i++;
	}
		
	$depression_count = array_count_values($level);	
}else{
	$depression_count = array();
}

#Stress Count
$stmt100 = $dbh->prepare("SELECT level FROM asmt_scale_score WHERE uid IN (SELECT uid FROM user_type WHERE corp_id=? AND asmt_type='AYS') AND scale=? ; ");
$stmt100->execute(array($corp_id,  "S"));
if ($stmt100->rowCount() != 0)
{
	$i = 0;
	while($row100 = $stmt100->fetch(PDO::FETCH_ASSOC)){
		$level[$i] = $row100['level'];
		$i++;
	}
		
	$stress_count = array_count_values($level);	
}else{
	$stress_count = array();
}


#------------------------ HRQOL--------------------------------------
$toal_asmt_done_hrqol = $toal_asmt_done_ewap = 0;

$to_date = date('Y-m-d H:i:s');
$from_date = '2000-01-01';

$postRequest = array(
    'access_token' => 'zxl7Woqkv1yEvkyWSO3ruffse2wnJAszgDVsQtC5',
    'corp_id' => $corp_id,
    'from_date' => $from_date,
    'to_date' => $to_date
    
  );


#Counting asmt data in HRQOL
$url  = "http://api.ewap.silveroakhealth.com/ios/index.php/api/get_qol_count";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT,10);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postRequest);
		$output = curl_exec($ch);
		$result = json_decode($output);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header = substr($output, 0, $header_size);
		$body = substr($output, $header_size);

		$result = json_decode($body, true);
		curl_close($ch);

		if($result['response'] == 'SUCCESS'){
			$toal_asmt_done_hrqol = $result['user_count'];
		}elseif($result['response'] == 'FAILED'){
				$message = $result['error'];
		} 

#Counting asmt data on EWAP App
$stmt02 = $dbh_ewap->prepare("SELECT COUNT(*) AS cnt FROM visitor_log WHERE corp_id=? AND type=? AND DATE(datetime)>=? AND DATE(datetime)<=?");
$stmt02->execute(array($corp_id, "ASMT", $from_date, $to_date));
if ($stmt02->rowCount() != 0) {
    $row02 = $stmt02->fetch();
    $toal_asmt_done_ewap = $row02['cnt'];
}



?>

<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="description" content="Silver Oak Health - HR Dashboard">
        <meta name="author" content="Silver Oak Health">

        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

        <title>EWAP Usage | Assessments</title>

         <!-- vendor css -->
		<link href="lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

		<!-- DashForge CSS -->
		<link rel="stylesheet" href="assets/css/dashforge.css">
		<link rel="stylesheet" href="assets/css/dashforge.demo.css">
    </head>
    <body class="page-profile">

        <?php include 'header.php'; ?>

        <div class="content content-fixed">
            <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
               	<h4 id="section7" class="mg-b-10">Assessment Analytics - "HRQoL"</h4>
               	<div class="row row-xs mg-t-10">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class=" card-body">
                                <table class="table table-sm">
                                    <thead class="thead-warning">
                                        <tr>
                                            <th scope="col" class="text-center"> Assessments <BR/> (on EWAP Portal & EWAP App) </th>
                                              <th scope="col" class="text-center"> Assessments  <BR/> (directly on HRQoL site) </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="tx-24 text-center"> <B><?php echo $toal_asmt_done_ewap; ?></B></td>
                                            <td class="tx-24 text-center"> <B><?php echo $toal_asmt_done_hrqol; ?></B></td>
                                        </tr>
                                    </tbody></table>
                            </div>
                        </div>
                    </div>
                  </div>  
                  <BR/> <BR/>             
                  <h4 id="section7" class="mg-b-10">Assessment Analytics - "Are you Stressed?"</h4>
                  <BR/>
			         <div class="row row-xs">
                    <div class="col-lg-4">           					
                        <div class="card">
                            <div class="card-body" style = "padding: 8px;">
                                <table class="table table-sm">
                                    <thead class="thead-warning">
                 							<tr>
                 								<th>Anxiety</th>
                 								<th class="text-center">No. of Employees</th>
                 								<th class="text-center">Percentage</th>
                 							</tr>
                 						</thead>
                 						<tbody>
                 							<tr>
                 								<td>Normal</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('N', $anxiety_count)) {
                 	   										 echo $anxiety_count['N'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('N', $anxiety_count)) {
                 	   										 echo round(($anxiety_count['N']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Mild</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MI', $anxiety_count)) {
                 	   										 echo $anxiety_count['MI'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MI', $anxiety_count)) {
                 	   										 echo round(($anxiety_count['MI']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Moderate</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MO', $anxiety_count)) {
                 	   										 echo $anxiety_count['MO'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MO', $anxiety_count)) {
                 	   										 echo round(($anxiety_count['MO']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Severe</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('S', $anxiety_count)) {
                 	   										 echo $anxiety_count['S'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('S', $anxiety_count)) {
                 	   										 echo round(($anxiety_count['S']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Extremely Severe</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('ES', $anxiety_count)) {
                 	   										 echo $anxiety_count['ES'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('ES', $anxiety_count)) {
                 	   										 echo round(($anxiety_count['ES']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td><B> Total </B></td>
                 								<td>
                 									<B> <?php echo $total_user_count; ?> </B>							
                 								</td>
                 								<td><B> 100% </B> </td>
                 							</tr>
                 						</tbody>
                 						</table>
                              </div>
                           </div>
           					</div>
           				    <div class="col-lg-4">
           						<div class="card">
                            <div class="card-body" style = "padding: 8px;">
                                <table class="table table-sm">
                                    <thead class="thead-warning">
                                    <tr>
                 								<th>Depression</th>
                 								<th class="text-center">No. of Employees</th>
                 								<th class="text-center">Percentage</th>
                 							</tr>
                 						</thead>
                 						<tbody>
                 							<tr>
                 								<td>Normal</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('N', $depression_count)) {
                 	   										 echo $depression_count['N'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('N', $depression_count)) {
                 	   										 echo round(($depression_count['N']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Mild</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MI', $depression_count)) {
                 	   										 echo $depression_count['MI'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MI', $depression_count)) {
                 	   										 echo round(($depression_count['MI']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Moderate</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MO', $depression_count)) {
                 	   										 echo $depression_count['MO'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MO', $depression_count)) {
                 	   										 echo round(($depression_count['MO']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Severe</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('S', $depression_count)) {
                 	   										 echo $depression_count['S'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('S', $depression_count)) {
                 	   										 echo round(($depression_count['S']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Extremely Severe</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('ES', $depression_count)) {
                 	   										 echo $depression_count['ES'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('ES', $depression_count)) {
                 	   										 echo round(($depression_count['ES']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td><B>Total</B></td>
                 								<td>
                 									<B><?php echo $total_user_count; ?>	</B>							
                 								</td>
                 								<td><B>100% </B></td>
                 							</tr>
                 						</tbody>
                 						</table>
                              </div>
                           </div>
           					</div>		
                        <div class="col-lg-4">
           						<div class="card">
                            <div class="card-body" style = "padding: 8px;">
                                <table class="table table-sm">
                                    <thead class="thead-warning">
                                    <tr>
                 								<th>Stress</th>
                 								<th class="text-center">No. of Employees</th>
                 								<th class="text-center">Percentage</th>
                 							</tr>
                 						</thead>
                 						<tbody>
                 							<tr>
                 								<td>Normal</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('N', $stress_count)) {
                 	   										 echo $stress_count['N'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('N', $stress_count)) {
                 	   										 echo round(($stress_count['N']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Mild</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MI', $stress_count)) {
                 	   										 echo $stress_count['MI'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MI', $stress_count)) {
                 	   										 echo round(($stress_count['MI']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Moderate</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MO', $stress_count)) {
                 	   										 echo $stress_count['MO'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('MO', $stress_count)) {
                 	   										 echo round(($stress_count['MO']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Severe</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('S', $stress_count)) {
                 	   										 echo $stress_count['S'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('S', $stress_count)) {
                 	   										 echo round(($stress_count['S']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td>Extremely Severe</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('ES', $stress_count)) {
                 	   										 echo $stress_count['ES'];
                 										}else{
                 											echo '0';
                 										}
                 									?>										
                 								</td>
                 								<td>
                 									<?php 
                 										if (array_key_exists('ES', $stress_count)) {
                 	   										 echo round(($stress_count['ES']/$total_user_count)*100 ) . "%" ;
                 										}else{
                 											echo '0%';
                 										}
                 									?>	
                 								</td>
                 							</tr>
                 							<tr>
                 								<td><B> Total </B></td>
                 								<td>
                 									<B> <?php echo $total_user_count; ?> </B>							
                 								</td>
                 								<td><B> 100% </B> </td>
                 							</tr>
                 						</tbody>
                 						</table>
                              </div>
                           </div>
           					</div>
           				</div>
            </div><!-- container -->
        </div><!-- content -->
		
        <script src="lib/jquery/jquery.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="lib/feather-icons/feather.min.js"></script>
		<script src="lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="lib/jqueryui/jquery-ui.min.js"></script>

		<script src="assets/js/dashforge.js"></script>
	   <script src="assets/js/nav-active.js"></script>
    </body>
</html>